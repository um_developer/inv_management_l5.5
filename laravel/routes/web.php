<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('contacts', ['uses' => 'HomeController@contacts', 'https' => true]);

//Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController',]);
Route::get('reset-password-link/{id}/{email}', 'SignupController@resetPasswordForm');

//////////////////Admin///////////////// 
Route::get('login', 'SignupController@index');
Route::post('postLogin', 'SignupController@postLogin');
Route::get('register', 'SignupController@register');
Route::post('signUpPost', 'SignupController@store');
Route::get('auth/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@itemGallery');
Route::get('/', 'HomeController@itemGallery');
Route::get('home-search', 'HomeController@itemGallerySearch');
Route::get('home-search-new', 'HomeController@itemGallerySearchNew');

//Route::get('changepassword', 'ProfileController@changepassword');
//Route::post('postchangepassword', 'ProfileController@postchangepassword');
Route::post('reset', 'SignupController@reset_password');
//Route::get('home', ['uses' => 'SignupController@index', 'https' => true]);
//Route::get('/', ['uses' => 'SignupController@index', 'https' => true]);


Route::get('dashboard', 'DashboardController@index');
Route::get('test_api', 'DashboardController@testCurl');
Route::get('tr', 'DashboardController@truncateTables');
Route::get('test-email', 'DashboardController@testEmail');

///////// VENDOR /////
Route::get('vendors', 'VendorController@index');
Route::get('vendor/create', 'VendorController@create');
Route::post('vendor/create', 'VendorController@postCreate');
Route::get('vendor/edit/{id}', 'VendorController@edit');
Route::get('vendor/detail/{id}', 'VendorController@detail');
Route::post('vendor/update', 'VendorController@postUpdate');
Route::get('vendor/delete/{id}', 'VendorController@delete');


///////// Category /////
Route::get('categories', 'CategoryController@index');
Route::get('category/create', 'CategoryController@create');
Route::post('category/create', 'CategoryController@postCreate');
Route::get('category/edit/{id}', 'CategoryController@edit');
Route::post('category/update', 'CategoryController@postUpdate');
Route::get('category/delete/{id}', 'CategoryController@delete');



///////// Items /////
Route::get('items', 'ItemController@index');
Route::get('itemts/getprice/{itemid}', 'ItemController@getItemprice');
Route::get('itemts/autogeneratecode', 'ItemController@autogeneratecode');
Route::get('itemts/generatebarcode/{itemid}', 'ItemController@generatebarcode');
Route::get('item-barcode-print/{itemid}', 'ItemController@printBarcode');

Route::get('item-gallery', 'ItemController@itemGallery');
Route::get('item-gallery-search', 'ItemController@itemGallerySearch');
Route::get('item-gallery-search-new', 'ItemController@itemGallerySearchNew');
Route::get('item/create', 'ItemController@create');
Route::post('item/create', 'ItemController@postCreate');
Route::get('item/edit/{id}', 'ItemController@edit');
Route::post('item/update', 'ItemController@postUpdate');
Route::get('item/image/delete/{id}', 'ItemController@imageDelete');
Route::get('item/delete/{id}', 'ItemController@delete');
Route::post('item/search', 'ItemController@itemListSearch');
Route::get('item/copy/{id}', 'ItemController@copy');
Route::get('item/getsubitems/{id}', 'ItemController@getsubitems');
Route::get('item/status/{status}/{id}', 'ItemController@changeStatus');
Route::get('get/modal/sub-items/{item_id}', 'CommonController@getModalSubitems');
Route::get('item/price/recalculate/{id}', 'ItemController@recalculate');
Route::get('getGalleryImages/', 'HomeController@getGalleryImages');
Route::post('item/priceUpdate', 'ItemController@priceUpdate');

Route::get('sample/create', 'ListController@createSample');
Route::post('sample/create', 'ListController@postCreateSample');
Route::get('checkApiItems', 'ItemController@getApiUpdatedItems');
Route::get('checkApiItemsLogs/{id}', 'ItemController@getApiLogs');


    Route::post('item/upcUpdate', 'ItemController@upcUpdate');
    Route::get('get/templates-categories/{templete_id}', 'CommonController@getTempleteCategories');
    Route::get('get/templates-items/{templete_id}/{cat_id}', 'CommonController@getTempleteItems');

Route::get('createSystemImagesThumbnail', 'ItemController@createSystemImagesThumbnail');

///////// Customer /////
Route::get('customers', 'CustomerController@index');
Route::get('customer/create', 'CustomerController@create');
Route::get('customer/detail/{id}', 'CustomerController@detail');
Route::post('customer/detail/{id}', 'CustomerController@detail');
Route::post('customer/create', 'CustomerController@postCreate');
Route::get('customer/edit/{id}', 'CustomerController@edit');
Route::post('customer/update', 'CustomerController@postUpdate');
Route::get('customer/delete/{id}', 'CustomerController@delete');
Route::post('customer-view', 'CustomerController@customLogin');



///////// Customer Care Managers /////
Route::get('customer-care-managers', 'ManagerController@index');
Route::get('customer-care-manager/create', 'ManagerController@create');
Route::post('customer-care-manager/create', 'ManagerController@postCreate');
Route::get('customer-care-manager/edit/{id}', 'ManagerController@edit');
Route::post('customer-care-manager/update', 'ManagerController@postUpdate');
Route::get('customer-care-manager/delete/{id}', 'ManagerController@delete');
Route::post('customer-care-manager-view', 'ManagerController@customLogin');




///
Route::get('print-catalog', 'ManagerController@printCatalog');




//Route::get('customer/login', 'CustomerController@login');
///////// Customer Payments/////
Route::get('add-new-payment', 'PaymentController@addnewpayment');
Route::get('getDetails', 'PaymentController@getDetails');
Route::post('syncPayment', 'PaymentController@addSyncPayment');

Route::get('/payments/customer', 'PaymentController@getCustomerPayments');
Route::get('/payment/edit/{id}', 'PaymentController@editPayment');
Route::post('/payments/customer/search', 'PaymentController@getCustomerPaymentsSearch');
Route::get('/payment/customer/create/{customer_id?}', 'PaymentController@createCustomerPayment');
Route::post('/payment/create', 'PaymentController@postCreate');
Route::post('/payment/update', 'PaymentController@postUpdate');

Route::get('/client/payment/create', 'PaymentController@createClientPayment');
Route::get('/getClientData', 'PaymentController@getClients');
Route::post('/client/payment/create', 'PaymentController@postCreateClient');
Route::get('/payments/vendor', 'PaymentController@getVendorPayments');
Route::post('/payments/vendor/search', 'PaymentController@getVendorPaymentsSearch');
Route::get('payment/vendor/create', 'PaymentController@createVendorPayment');
Route::get('payment/status/{id}/{status}', 'PaymentController@changeStatus');
Route::get('payment/delete/{user_type}/{id}', 'PaymentController@delete');

Route::post('/payment/create-adjust', 'PaymentController@postAdjust');
Route::get('/payment/customer/create-adjust/{customer_id?}', 'PaymentController@adjustCustomerPayment');
Route::get('/get-clients-by-customer/{customer_id}', 'PaymentController@getClientByCustomer');


///////// Purchase Order /////
Route::get('purchase-orders', 'PurchaseOrderController@index');
Route::get('purchase-order/create', 'PurchaseOrderController@create');
Route::post('purchase-order/create', 'PurchaseOrderController@postCreate');
Route::get('purchase-order/edit/{id}', 'PurchaseOrderController@edit');
Route::post('purchase-order/update', 'PurchaseOrderController@postUpdate');
Route::get('purchase-order/delete/{id}', 'PurchaseOrderController@delete');
Route::get('purchase-order/{id}', 'PurchaseOrderController@detail');
Route::get('purchase-order-print/{id}', 'PurchaseOrderController@printPage');

///////// Return vender /////
Route::get('return-vendor', 'ReturnVendorController@index');
Route::get('return-vendor/create', 'ReturnVendorController@create');
Route::post('return-vendor/create', 'ReturnVendorController@postCreate');
Route::get('return-vendor/edit/{id}', 'ReturnVendorController@edit');
Route::post('return-vendor/update', 'ReturnVendorController@postUpdate');
Route::get('return-vendor/delete/{id}', 'ReturnVendorController@delete');
Route::get('return-vendor/{id}', 'ReturnVendorController@detail');
Route::get('return-vendor-print/{id}', 'ReturnVendorController@printPage');

///////// Receive Order /////
Route::get('receive-items', 'ReceiveOrderController@index');
Route::get('receive-item/create', 'ReceiveOrderController@create');
Route::post('receive-item/create', 'ReceiveOrderController@postCreate');
Route::get('receive-item/edit/{id}', 'ReceiveOrderController@edit');
Route::post('receive-item/update', 'ReceiveOrderController@postUpdate');
Route::get('receive-item/delete/{id}', 'ReceiveOrderController@delete');
Route::get('receive-item/{id}', 'ReceiveOrderController@detail');
Route::get('receive-item-print/{id}', 'ReceiveOrderController@printPage');

Route::get('get/open-po-by-vendor/{id}', 'ReceiveOrderController@getopenPoByVendor');
Route::get('get/po-detail/{id}', 'ReceiveOrderController@getPoDetail');

Route::get('priceImpect', 'ReceiveOrderController@getPriceImpect');
Route::post('priceImpect/search', 'ReceiveOrderController@searchPriceImpect');
Route::post('priceImpectCreate', 'ReceiveOrderController@priceImpectCreate');

Route::get('activityLog', 'ActivityLogController@index');
Route::get('activityLogInvoices', 'ActivityLogController@getLog');
Route::post('activityLog/search', 'ActivityLogController@search');
Route::get('activityLogDelete', 'ActivityLogController@activityLogDelete');
Route::get('activityLogItemDelete/{id}/{type}', 'ActivityLogController@logItemDelete');
Route::post('activityLog/searching', 'ActivityLogController@searching');


///////// warehouse /////
Route::get('warehouses', 'WarehouseController@index');
Route::get('warehouse/create', 'WarehouseController@create');
Route::post('warehouse/create', 'WarehouseController@postCreate');
Route::get('warehouse/edit/{id}', 'WarehouseController@edit');
Route::post('warehouse/update', 'WarehouseController@postUpdate');
Route::get('warehouse/delete/{id}', 'WarehouseController@delete');
Route::get('warehouse/{id}', 'WarehouseController@detail');
Route::get('warehouse/inventory/{id}', 'WarehouseController@inventory');
Route::get('warehouse/inventory1/{id}', 'WarehouseController@inventory1');
Route::get('warehouse/generate/report/{id}', 'WarehouseController@generateCSVReport');
//Route::get('warehouse/acco', 'WarehouseController@aabb');
///////// Packages /////
Route::get('packages', 'PackageController@index');
Route::get('package/create-bulk', 'PackageController@createBulk');
Route::get('package/create', 'PackageController@create');
Route::post('package/create', 'PackageController@postCreate');
Route::post('package/create-bulk', 'PackageController@postCreateBulk');
Route::get('package/edit/{id}', 'PackageController@edit');
Route::post('package/update', 'PackageController@postUpdate');
Route::get('package/delete/{id}', 'PackageController@delete');
Route::get('package/{id}', 'PackageController@detail');

Route::get('package/generate/pdf/{id}', 'PackageController@generatepdf');

Route::get('get/package-by-warehouse/{id}', 'PackageController@getPackageByWarehouse');
Route::post('package/search', 'PackageController@postPackageSearch');



Route::get('package/transfer/all', 'PackageController@transferPackage');
Route::post('package/transfer/all', 'PackageController@transferPackagePost');

//Bundles///////////////////////////
Route::get('bundles', 'BundleController@index');
Route::get('bundle/status/{status}/{id}', 'BundleController@changeStatus');
// Route::get('package/create-bulk', 'PackageController@createBulk');
 Route::get('bundle/create', 'BundleController@create');
 Route::post('bundle/insert', 'BundleController@postCreate');
 Route::get('bundle/copy/{id}', 'BundleController@copy');
// Route::post('package/create-bulk', 'PackageController@postCreateBulk');
 Route::get('bundle/edit/{id}', 'BundleController@edit');
 Route::post('bundle/update', 'BundleController@postUpdate');
 Route::get('bundle/delete/{id}', 'BundleController@delete');
 Route::get('bundle/{id}', 'BundleController@detail');

// Route::get('package/generate/pdf/{id}', 'PackageController@generatepdf');

// Route::get('get/package-by-warehouse/{id}', 'PackageController@getPackageByWarehouse');
 Route::post('bundle/search', 'BundleController@postBundleSearch');



// Route::get('package/transfer/all', 'PackageController@transferPackage');
// Route::post('package/transfer/all', 'PackageController@transferPackagePost');

///////// Admin Inventory /////
//Route::get('inventory1', 'AdminInventoryController@index');
Route::get('inventory/generate/report', 'AdminInventoryController@generateCSVReport');
Route::get('inventory', 'AdminInventoryController@index1');
Route::get('inventory/item/{item_id}', 'AdminInventoryController@inventoryByItem');
Route::post('get/items-by-location', 'AdminInventoryController@getPackageByLocation');
Route::get('update-inventory-quantity/{item_id}/{amount}', 'AdminInventoryController@updateInventoryQuantity');

Route::get('inventory-logs', 'AdminInventoryController@updateInventoryLogs');



///////// Invoices /////

Route::get('invoice/getComments/{id}/{status}', 'CommonController@getComments');

Route::get('invoice/processing/{id}', 'InvoiceController@invoiceProcess');

Route::get('invoicce/deliver/{item_id}', 'InvoiceController@invoiceDeliver');
Route::get('invoices', 'InvoiceController@index');
Route::get('get-item-price/{item_id}/{user_id}/{type}', 'CommonController@getItemPrice');
Route::get('get-item-price-html/{item_id}/{user_id}/{type}', 'CommonController@getItemPriceHtml');
Route::get('get-item-price-new/{item_id}/{user_id}/{type}', 'ItemController@getItemPriceNew');
Route::get('invoices/search', 'InvoiceController@search');
Route::get('invoice/create/{customer_id?}', 'InvoiceController@create');
Route::post('invoice/create', 'InvoiceController@postCreate');

Route::post('update/deliver/status', 'CommonController@changeDeliverStatus');
Route::get('ref/order/detail/{order_id}/{show_profit?}', 'CommonController@orderDetail');
Route::get('ref/order/print/{order_id}', 'CommonController@OrderprintPage');


Route::get('invoice/edit/{id}/{customer_id?}', 'InvoiceController@edit');
Route::post('invoice/update', 'InvoiceController@postUpdate');
Route::post('invoice/modified', 'InvoiceController@postModified');
Route::post('invoice/sync/modified', 'InvoiceController@postsyncModified');
Route::get('invoice/delete/{id}', 'InvoiceController@delete');
Route::get('invoice/{id}/{show_profit?}', 'InvoiceController@detail');
Route::get('invoice-print/{id}', 'InvoiceController@printPage');
Route::get('invoice/printType/{status}/{customer_id}', 'InvoiceController@printType');
Route::get('print/bulk/{id}', 'InvoiceController@printBulkPage');
Route::get('invoice/status/{id}/{status}', 'InvoiceController@changeStatus');
Route::get('invoice/serial-item/validate/{item_id}/{start_serial}/{quantity}', 'CommonController@serialItemValidate');
Route::get('credit-memo/serial-item/validate/{item_id}/{start_serial}/{quantity}', 'CommonController@creditMemoSerialItemValidate');
Route::get('invoice/create/bulk/{package_id}/{j}', 'InvoiceController@bulkCreate');
Route::get('invoice/create/bulkbundle/{bundle_id}/{j}/{customer_id}/{type}', 'CommonController@bulkCreatebundle');
Route::get('invoice/edit/bulkbundle/{bundle_id}/{j}/{customer_id}/{type}/{qty?}', 'CommonController@bulkEditbundle');
Route::get('invoice/insert/bulk/{package_id}/{j}', 'InvoiceController@bulkAddItemInInvoice');
Route::get('invoice/insert/bulkbundle/{bundle_id}/{j}/{qty}/{customer_id}/{type}', 'CommonController@bulkAddItemInInvoicebundle');
Route::post('invoice/insertEdit/bulkbundle', 'CommonController@bulkEditItemInInvoicebundle');
Route::get('zeroprofit', 'InvoiceController@zeroprofit');
Route::get('zeropurchaseorder', 'InvoiceController@zeropurchaseorder');
Route::get('items-with-no-po', 'InvoiceController@ItemsWithNoPo');
// Route::get('invoiceskip', 'InvoiceController@invoiceskip');
Route::get('invoiceskip/{id}', 'InvoiceController@invoiceskip');

Route::get('priceImpectInvoice/{id}/{tableID}', 'InvoiceController@createPriceImpectInvoice');
// Route::get('priceImpectInvoiceAll', 'InvoiceController@createPriceImpectAllInvoice');
Route::get('invoices/priceImpect', 'InvoiceController@getPriceImpectInvoice');
Route::get('priceImpectInvoiceAll2', 'InvoiceController@createPriceImpectAllInvoice');
Route::get('priceImpectadditem', 'InvoiceController@priceImpectadditem');
Route::get('priceImpectDelete/{id}', 'InvoiceController@priceImpectDelete');
Route::get('addDummyInvoice', 'InvoiceController@addDummyInvoice');
Route::post('dummyInvoiceCreate', 'InvoiceController@dummyInvoiceCreate');
Route::get('addDummyOrder', 'InvoiceController@addDummyOrder');
Route::post('dummyOrderCreate', 'InvoiceController@dummyOrderCreate');

Route::get('item-serial-queue', 'ItemSerialController@indexQueue');
Route::get('item-serial', 'ItemSerialController@index');
Route::post('item-serial/search', 'ItemSerialController@search');
Route::get('item-serial/create', 'ItemSerialController@create');
Route::post('item-serial/create', 'ItemSerialController@postCreate');
Route::get('item-serial-queue/detail/{id}', 'ItemSerialController@queueDetail');
Route::get('item-serial/active/{serial}', 'ItemSerialController@markActive');
Route::get('item-serial/delete/{serial}', 'ItemSerialController@delete');
Route::get('/add-to-invoice/{item_id}/{quantity}/{price}', 'InvoiceController@addItemToInvoice');

///////// Credit Memo /////
Route::get('credit-memo', 'CreditMemoController@index');
Route::get('credit-memo/search', 'CreditMemoController@search');
Route::get('credit-memo/create/{customer_id?}', 'CreditMemoController@create');
Route::post('credit-memo/create', 'CreditMemoController@postCreate');
Route::get('credit-memo/edit/{id}', 'CreditMemoController@edit');
Route::post('credit-memo/update', 'CreditMemoController@postUpdate');
Route::post('credit-memo/modified', 'CreditMemoController@postModified');
Route::get('credit-memo/delete/{id}', 'CreditMemoController@delete');
Route::get('credit-memo/{id}/{show_profit?}', 'CreditMemoController@detail');
Route::get('credit-memo/status/{id}/{status}/{type}', 'CreditMemoController@changeStatus');
Route::get('credit-memo-print/{id}', 'CreditMemoController@printPage');

///////// API SERVERS /////
Route::get('api-servers', 'ApiServerController@index');
Route::get('api-server/create', 'ApiServerController@create');
Route::post('api-server/create', 'ApiServerController@postCreate');
Route::get('api-server/edit/{id}', 'ApiServerController@edit');
Route::post('api-server/update', 'ApiServerController@postUpdate');
Route::get('api-server/delete/{id}', 'ApiServerController@delete');


///////// COLORS /////
Route::get('colors', 'ColorController@index');
Route::get('color/create', 'ColorController@create');
Route::post('color/create', 'ColorController@postCreate');
Route::get('color/edit/{id}', 'ColorController@edit');
Route::post('color/update', 'ColorController@postUpdate');
Route::get('color/delete/{id}', 'ColorController@delete');

///////// Sizes /////
Route::get('sizes', 'SizeController@index');
Route::get('size/create', 'SizeController@create');
Route::post('size/create', 'SizeController@postCreate');
Route::get('size/edit/{id}', 'SizeController@edit');
Route::post('size/update', 'SizeController@postUpdate');
Route::get('size/delete/{id}', 'SizeController@delete');

///////// LIST /////
Route::get('lists', 'ListController@index');
Route::get('list/create', 'ListController@create');
Route::post('list/create', 'ListController@postCreate');
Route::get('list/edit/{id}', 'ListController@edit');
Route::post('list/update', 'ListController@postUpdate');
Route::get('list/delete/{id}', 'ListController@delete');

///////// TAGS /////
Route::get('tags', 'TagsController@index');
Route::get('tags/create', 'TagsController@create');
Route::post('tags/create', 'TagsController@postCreate');
Route::get('tags/edit/{id}', 'TagsController@edit');
Route::post('tags/update', 'TagsController@postUpdate');
Route::get('tags/delete/{id}', 'TagsController@delete');
Route::get('tags/subtags', 'TagsController@subTags');
Route::get('tags/details', 'TagsController@details');
Route::post('tags/details/create', 'TagsController@postCreateDetails');
Route::get('tags/getItems', 'TagsController@getItems');
Route::get('itemTags', 'TagsController@getItemNotReleteTags');
Route::post('itemTags', 'TagsController@itemTagListSearch');
Route::get('tag/status/{id}', 'TagsController@changeStatus');
Route::get('tags/Sorting', 'TagsController@tagsSorting');
Route::post('tags/sortOrder/update', 'TagsController@sortingUpdate');

///////// Payment Types /////
Route::get('payment-types', 'PaymentTypesController@index');
Route::get('payment-type/create', 'PaymentTypesController@create');
Route::post('payment-type/create', 'PaymentTypesController@postCreate');
Route::get('payment-type/edit/{id}', 'PaymentTypesController@edit');
Route::post('payment-type/update', 'PaymentTypesController@postUpdate');
Route::get('payment-type/delete/{id}', 'PaymentTypesController@delete');

///////// Account Types /////
Route::get('account-types', 'AccountTypesController@index');
Route::get('account-type/create', 'AccountTypesController@create');
Route::post('account-type/create', 'AccountTypesController@postCreate');
Route::get('account-type/edit/{id}', 'AccountTypesController@edit');
Route::get('account-type/detail/{id}', 'AccountTypesController@details');
Route::get('account-type/transfer', 'AccountTypesController@transfer');
Route::post('account-type/post/transfer', 'AccountTypesController@transferAmountPost');
Route::post('account-type/update', 'AccountTypesController@postUpdate');
Route::get('account-type/delete/{id}', 'AccountTypesController@delete');


///////// Types /////
Route::get('types', 'TypeController@index');
Route::get('type/create', 'TypeController@create');
Route::post('type/create', 'TypeController@postCreate');
Route::get('type/edit/{id}', 'TypeController@edit');
Route::post('type/update', 'TypeController@postUpdate');
Route::get('type/delete/{id}', 'TypeController@delete');


///////// REPORTS /////
Route::get('report/customer/', 'AdminReportsController@customer');
Route::get('report/customer-statement/', 'AdminReportsController@customerStatementReport');
Route::post('report-by-customer', 'AdminReportsController@getReportByCustomer');
Route::post('report-by-customer-statement', 'AdminReportsController@getStatementReportByCustomer');
Route::get('report/customer/sale-profit/', 'AdminReportsController@customerSaleProfitReport');
Route::post('report/customer/sale-profit-search/', 'AdminReportsController@customerSaleProfitReportSearch');
Route::get('report/item/sale-profit/', 'AdminReportsController@itemSaleProfitReport');
Route::post('report/item/sale-profit-search/', 'AdminReportsController@itemSaleProfitReportSearch');
Route::get('report/customer/balance', 'AdminReportsController@CustomerBalanceReport');
Route::get('report/vendor/balance', 'AdminReportsController@VendorBalanceReport');

Route::get('report/customer/sale-profit-new/', 'AdminReportsController@customerSaleProfitReportNew');
Route::put('report/customer/sale-profit-search-new/', 'AdminReportsController@customerSaleProfitReportSearchNew');

Route::get('report/item/sale-profit-new/', 'AdminReportsController@itemSaleProfitReportNew');
Route::post('report/item/sale-profit-search-new/', 'AdminReportsController@itemSaleProfitReportSearchNew');

Route::get('report/item/sale-profit-vendor/', 'AdminReportsController@itemSaleProfitReportVendorNew');
// Route::post('report/item/sale-profit-vendor/', 'AdminReportsController@itemSaleProfitReportVendorNew');
Route::post('report/item/sale-profit-vendor-search/', 'AdminReportsController@itemSaleProfitReportVendorNewSearch');

Route::get('report/master/', 'AdminReportsController@newCombineReport');
Route::post('report/master-search/', 'AdminReportsController@newCombineReportSearch');
Route::get('reportGetcontent', 'AdminReportsController@reportGetcontent');

/////Price Templates ///
Route::get('item-price-templates/', 'ItemPriceTemplateController@index');
Route::get('item-price-template/create', 'ItemPriceTemplateController@create');
Route::get('item-price-template/edit/{id}', 'ItemPriceTemplateController@edit');
Route::post('item-price-template/create', 'ItemPriceTemplateController@postCreate');
Route::post('item-price-template/update', 'ItemPriceTemplateController@postupdate');
Route::get('item-price-template/{id}', 'ItemPriceTemplateController@detail');
Route::post('item-price-template/price-update', 'ItemPriceTemplateController@priceUpdate');
Route::post('item-price-template/search', 'ItemPriceTemplateController@search');
Route::post('item-price-template/price-update-bulk', 'ItemPriceTemplateController@priceUpdateBulk');
Route::get('item-price-template/delete/{id}', 'ItemPriceTemplateController@delete');
Route::get('item-price-template/copy/{id}', 'ItemPriceTemplateController@copy');

Route::get('print_page_setting', 'CustomerController@printPageSetting');
Route::post('print_page_setting/update', 'CustomerController@updatePrintPageSetting');

Route::get('update_customer_cost', 'InvoiceController@calculateCustomerCost');

Route::get('invoice-recalculate-profit', 'InvoiceController@recalculateAllInvoiceProfit');
Route::get('insertClinets', 'SignupController@insertClients');

Route::get('exportPDF', 'HomeController@exportPDF');

//include("customers_routes.php");
//include("clients_routes.php");





// Route::get('/', function () {
//     return view('front.welcome');
// });
// // Authentication Routes...
// //$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
// //$this->post('login', 'Auth\LoginController@login');
// //$this->post('logout', 'Auth\LoginController@logout')->name('logout');
// //
// //// Registration Routes...
// //$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// //$this->post('register', 'Auth\RegisterController@register');
// //
// //// Password Reset Routes...
// //$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
// //$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// //$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
// //$this->post('password/reset', 'Auth\ResetPasswordController@reset');
// //Auth Routes
// Route::get('login', 'SignupController@login')->name('login');
// Route::post('login', 'SignupController@postLogin');
// Route::get('register', 'SignupController@register')->name('register');
// Route::post('register', 'SignupController@store');
// Route::post('logout', [
//     'as' => 'logout',
//     'uses' => 'Auth\LoginController@logout'
// ]);


// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

// Route::get('register/success/{id}', 'SignupController@success')->name('register/success/');
// Route::get('register/verify/{confirmation_code}', 'SignupController@confirmEmail')->name('register/verify/');

// //Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController']);
// //Auth::routes();

// Route::get('/home', 'PostController@index')->name('home');
// Route::get('setPost','PostController@create');
// Route::get('post/delete/{id}','PostController@delete');
// Route::post('post/insert','PostController@store');
// Route::get('post/edit/{id}','PostController@edit'); 
// Route::post('post/update/{id}','PostController@update'); 
// Route::get('aftab',function(){
//     return view('front/post/create');
// });

// include("routes_admin.php");
