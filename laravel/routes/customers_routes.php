<?php

Route::group(
        array('prefix' => 'customer'), function() {
    $admin = "Customers\\";

//    Route::get('/', $admin . 'HomeController@index');
    Route::get('/dashboard', 'DashboardController@customerDashboard');
    Route::get('/profile', 'CustomerProfileController@edit');
    Route::post('/profile/update', 'CustomerProfileController@postUpdate');
//    Route::get('getGalleryImages/', 'CustomerInvoiceController@getGalleryImages');

    Route::get('/invoices', 'CustomerInvoiceController@index');
    Route::get('/invoice/search', 'CustomerInvoiceController@search');
    Route::get('/invoice/create', 'CustomerInvoiceController@create');
    Route::post('/invoice/create', 'CustomerInvoiceController@postCreate');
    Route::get('/invoice/edit/{id}', 'CustomerInvoiceController@edit');
    Route::post('/invoice/update', 'CustomerInvoiceController@postUpdate');
    Route::post('/sync/invoice/update', 'CustomerInvoiceController@postSyncUpdate');
    Route::get('invoice/delete/{id}', 'CustomerInvoiceController@delete');
    Route::get('invoice/{id}', 'CustomerInvoiceController@detail');
    Route::get('invoice-print/{id}', 'CustomerInvoiceController@printPage');
    Route::get('/add-to-invoice/{item_id}/{quantity}/{price}', 'CustomerInvoiceController@addItemToInvoice');
 
    Route::get('/payments', 'CustomerPaymentController@index');
    Route::post('/my-payments/search', 'CustomerPaymentController@myPaymentSearch');
    Route::get('/payment/create', 'CustomerPaymentController@create');
    Route::get('/payment/delete/{id}', 'CustomerPaymentController@delete');
    Route::post('/payment/create', 'CustomerPaymentController@postCreate');
//    Route::get('/inventory', 'CustomerInventoryController@index');
//    Route::get('inventory/generate/report', 'CustomerInventoryController@generateCSVReport');
    Route::get('/items_list', 'CustomerInventoryController@itemList');
    Route::get('/items-list-tag-new', 'CustomerInventoryController@itemListTagSearch');
    Route::get('/itemstatus/{status}/{itemid}/{type}', 'CustomerInventoryController@itemstatus');
    Route::get('/getItemStatus/{itemid}', 'CustomerInventoryController@getItemStatus');
   
    Route::get('/credit-memo', 'CustomerCreditMemoController@index');
    Route::get('/credit-memo/search', 'CustomerCreditMemoController@search');
    Route::get('/credit-memo/create', 'CustomerCreditMemoController@create');
    Route::post('/credit-memo/create', 'CustomerCreditMemoController@postCreate');
    Route::get('/credit-memo/edit/{id}', 'CustomerCreditMemoController@edit');
    Route::post('/credit-memo/update', 'CustomerCreditMemoController@postUpdate');
    Route::get('credit-memo/delete/{id}', 'CustomerCreditMemoController@delete');
    Route::get('credit-memo/{id}', 'CustomerCreditMemoController@detail');
    Route::get('credit-memo-print/{id}', 'CustomerCreditMemoController@printPage');


    ///////// Client /////
    Route::get('clients', 'ClientController@index');
    Route::get('client/create', 'ClientController@create');
    Route::post('client/create', 'ClientController@postCreate');
    Route::get('client/edit/{id}', 'ClientController@edit');
    Route::post('client/update', 'ClientController@postUpdate');
    Route::get('client/delete/{id}', 'ClientController@delete');
    Route::post('client-view', 'ClientController@customLogin');

    Route::get('client/detail/{id}', 'ClientController@detail');
    Route::post('client/detail/{id}', 'ClientController@detail');
    Route::get('client/clientimpact/{status}/{id}/{type}', 'ClientController@clientimpact');

//////////////////ORDERS //////////////////

    Route::get('order/processing/{id}', 'CustomerOrderController@invoiceProcess');

    Route::get('/orders', 'CustomerOrderController@index');
    Route::get('/orders/search', 'CustomerOrderController@search');
    Route::get('/order/create/{client_id?}', 'CustomerOrderController@create');
    Route::post('/order/create', 'CustomerOrderController@postCreate');
    Route::get('print/bulk/orders/{id}', 'CustomerOrderController@printBulkPage');
    Route::get('/sync/order/create/{client_id?}', 'CustomerOrderController@syncCreate');
    Route::post('/sync/order/create/', 'CustomerOrderController@syncOrders');
    Route::post('/sync/order/update', 'CustomerOrderController@postSyncUpdate');
    Route::get('/order/edit/{id}/{client_id?}', 'CustomerOrderController@edit');
    Route::post('/order/update', 'CustomerOrderController@postUpdate');
    Route::post('/order/modified', 'CustomerOrderController@postModified');
    Route::get('order/delete/{id}', 'CustomerOrderController@delete');
    Route::get('order/{id}/{show_profit?}', 'CustomerOrderController@detail');
    Route::get('/order-send/{status?}/{order_id}', 'CustomerOrderController@orderSendToAdmin');
    Route::get('order-print/{id}', 'CustomerOrderController@printPage');
    Route::get('order/printType/{status}/{customer_id}', 'CustomerOrderController@printType');
    Route::get('/add-to-order/{item_id}/{quantity}/{price}', 'CustomerOrderController@addItemToOrder');
    Route::get('/zeroprofit', 'CustomerOrderController@zeroprofit');
    Route::get('/orderSkip/{id}', 'CustomerOrderController@orderSkip');

    Route::get('priceChange', 'CustomerOrderController@priceChange');
    Route::post('priceChanged', 'CustomerOrderController@priceChanged');
    Route::get('myActivity', 'CustomerOrderController@myActivity');
    ///////////// ORDER RETURNS //////////////////
    Route::get('/order-returns', 'CustomerOrderReturnsController@index');
    Route::get('/order-returns/search', 'CustomerOrderReturnsController@search');
    Route::get('/order-return/create/{client_id?}', 'CustomerOrderReturnsController@create');
    Route::post('/order-return/create', 'CustomerOrderReturnsController@postCreate');
    Route::get('/order-return/edit/{id}', 'CustomerOrderReturnsController@edit');
    Route::post('/order-return/update', 'CustomerOrderReturnsController@postUpdate');
    Route::post('/order-return/modified', 'CustomerOrderReturnsController@postModified');
    Route::get('/order-return/delete/{id}', 'CustomerOrderReturnsController@delete');
    Route::get('/order-return/{id}/{show_profit?}', 'CustomerOrderReturnsController@detail');
    Route::get('/order-return-send/{status?}/{order_id}', 'CustomerOrderReturnsController@returnOrderSendToAdmin');
    Route::get('/order-return-print/{id}', 'CustomerOrderReturnsController@printPage');


    /////////////////////////CLIENT PAYMENTS //////////////////////////////////////////
    Route::get('/client/payments', 'CustomerPaymentController@getClientPayments');
    Route::post('/client/payments/search', 'CustomerPaymentController@getClientPaymentsSearch');
    Route::get('/client/payment/create/{client_id?}', 'CustomerPaymentController@createClientPayment');
    Route::get('/client/payment/edit/{payment_id}', 'CustomerPaymentController@editClientPayment');
    Route::post('/client/payment/create', 'CustomerPaymentController@postCreateClientPayment');
    Route::post('/client/payment/update', 'CustomerPaymentController@postUpdateClientPayment');
    Route::get('/client/payment/delete/{id}', 'CustomerPaymentController@deleteClientPayment');

    Route::get('/item-gallery', 'CustomerInventoryController@itemGallery');
    Route::get('/item-gallery-search', 'CustomerInventoryController@itemGallerySearch');
    Route::post('/item-list-search', 'CustomerInventoryController@itemListSearch');
    Route::get('/item-gallery-search-new', 'CustomerInventoryController@itemGallerySearchNew');
    

    ///////// REPORTS /////
    Route::get('report/client', 'CustomerReportsController@client');
    Route::post('report-by-client', 'CustomerReportsController@getReportByClient');
    
    Route::get('report/client-statement', 'CustomerReportsController@clientStatementReport');
    Route::post('report-by-client-statement', 'CustomerReportsController@getStatementReportByClient');

    Route::get('reports', 'CustomerReportsController@myReports');
    Route::post('report-by-type', 'CustomerReportsController@getReportByType');
    
        Route::get('statement-report', 'CustomerReportsController@myStatementReport');
    Route::post('statement-report-date', 'CustomerReportsController@myStatementReportSearch');
    
    Route::get('report/client/sale-profit/', 'CustomerReportsController@clientSaleProfitReport');
    Route::post('report/client/sale-profit-search/', 'CustomerReportsController@clientSaleProfitReportSearch');
    Route::get('report/item/sale-profit/', 'CustomerReportsController@itemSaleProfitReport');
    Route::post('report/item/sale-profit-search/', 'CustomerReportsController@itemSaleProfitReportSearch');
    Route::get('report/client/balance', 'CustomerReportsController@ClientBalanceReport');

    /////Price Templates ///
    Route::get('item-price-templates/', 'CustomerItemPriceTemplateController@index');
    Route::get('item-price-template/create', 'CustomerItemPriceTemplateController@create');
    Route::get('item-price-template/edit/{id}', 'CustomerItemPriceTemplateController@edit');
    Route::post('item-price-template/create', 'CustomerItemPriceTemplateController@postCreate');
    Route::post('item-price-template/update', 'CustomerItemPriceTemplateController@postupdate');
    Route::get('item-price-template/{id}', 'CustomerItemPriceTemplateController@detail');
    Route::post('item-price-template/price-update', 'CustomerItemPriceTemplateController@priceUpdate');
    Route::post('item-price-template/search', 'CustomerItemPriceTemplateController@search');
    Route::post('item-price-template/price-update-bulk', 'CustomerItemPriceTemplateController@priceUpdateBulk');
    Route::get('item-price-template/delete/{id}', 'CustomerItemPriceTemplateController@delete');
    Route::get('item-price-template/copy/{id}', 'CustomerItemPriceTemplateController@copy');
    Route::get('get-item-price/{item_id}/{user_id}', 'CustomerItemPriceTemplateController@getItemPrice');

    Route::get('print_page_setting', 'ClientController@printPageSetting');
    Route::post('print_page_setting/update', 'ClientController@updatePrintPageSetting');
    Route::get('get/templates-categories/{templete_id}', 'CustomerInventoryController@getTempleteCategories');
    Route::get('get/templates-items/{templete_id}/{cat_id}', 'CustomerInventoryController@getTempleteItems');

    ///////// Payment Types /////
    Route::get('payment-types', 'CustomerPaymentTypesController@index');
    Route::get('payment-type/create', 'CustomerPaymentTypesController@create');
    Route::post('payment-type/create', 'CustomerPaymentTypesController@postCreate');
    Route::get('payment-type/edit/{id}', 'CustomerPaymentTypesController@edit');
    Route::post('payment-type/update', 'CustomerPaymentTypesController@postUpdate');
    Route::get('payment-type/delete/{id}', 'CustomerPaymentTypesController@delete');

    ///////// Account Types /////
    Route::get('account-types', 'CustomerAccountTypesController@index');
    Route::get('account-type/create', 'CustomerAccountTypesController@create');
    Route::post('account-type/create', 'CustomerAccountTypesController@postCreate');
    Route::get('account-type/edit/{id}', 'CustomerAccountTypesController@edit');
    Route::post('account-type/update', 'CustomerAccountTypesController@postUpdate');
    Route::get('account-type/delete/{id}', 'CustomerAccountTypesController@delete');

    Route::get('account-type/detail/{id}', 'CustomerAccountTypesController@details');
    Route::get('account-type/transfer', 'CustomerAccountTypesController@transfer');
    Route::post('account-type/post/transfer', 'CustomerAccountTypesController@transferAmountPost');
}
);
