<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Sizes extends Model {
	//
    protected $table='sizes';
    
    public function ItemSizes(){
        return $this->hasMany('App\ItemSizes');
}
}
