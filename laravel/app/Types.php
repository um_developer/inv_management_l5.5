<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Types extends Model {
	//
    protected $table='types';
    
    public function ItemTypes(){
        return $this->hasMany('App\ItemTypes');
}
}
