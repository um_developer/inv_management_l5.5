<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model {
	//
    protected $table='colors';
    
    public function ItemColors(){
        return $this->hasMany('App\ItemColors');
}
}
