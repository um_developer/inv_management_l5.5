<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Get the phone record associated with the user.
     */
    public function role() {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    public function cafe() {
        return $this->hasOne('App\Cafe', 'created_by_user', 'id');
    }

    public static function getUserById($id) {


        $result = User::select('id', 'email', 'firstName', 'lastName', 'role_id', 'status')
                ->where('id', '=', $id)
                ->get();

        return $result;


//        $result = User::where("users.deleted", '=', 0);
//        $result=$result->leftJoin('roles as r', 'r.id', '=', 'users.role_id');
//        $result=$result->leftJoin('user_codes as uc', 'uc.user_id', '=', 'users.id');
        //$result=$result->select('users.id', 'users.firstName', 'users.lastName', 'users.email', 'users.role_id', 'users.created_at', 'r.role as role', 'users.loginCount as loginCount', 'uc.expires as
    }

    

}
