<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model {

    //
    protected $table = 'items';

    public function ItemColors() {
        return $this->hasMany('App\ItemColors', 'item_id', 'id');
    }

    public function ItemSizes() {
        return $this->hasMany('App\ItemSizes', 'item_id', 'id');
    }
    
     public function ItemTypes() {
        return $this->hasMany('App\ItemTypes', 'item_id', 'id');
    }

    public function GalleryImages() {
        return $this->hasMany('App\GalleryImages', 'item_id', 'id');
    }
    public function childs() {
        return $this->hasMany(self::class,'parent_item_id','id');
    }

}
