<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\CreditMemo;
use App\CreditMemoItems;
use App\Customers;
use App\Inventory;
use App\Packages;
use App\Bundles;
use App\BundleItems;
use Illuminate\Http\Request;
use App\CustomerInventory;
use App\WarehouseInventory;
use App\ItemPriceTemplates;
use App\Payments;
use Auth;
use App\PriceTemplates;
use Session;
use Carbon\Carbon;
use App\PrintPageSetting;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;
use App\Functions\Functions;

class CreditMemoController extends AdminController {

    private $user_id = '0';
    private $customer_id = '0';

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $model = CreditMemo::where('credit_memo.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->whereBetween('credit_memo.statement_date', [$start_date, $end_date])
                ->select('credit_memo.*', 'c.name as customer_name', 'c.u_id as customer_u_id')
                ->orderBy('id', 'desc')
                ->get();

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $selected_report_type = '';
        $customers = $new_cat;

        $date = $start_date . ' - ' . $end_date;
        return view('front.credit_memo.index', compact('model', 'date','customers','selected_report_type'));
    }
 
    public function search(Request $request) {

        $date = $request->date_range;
        
        if(isset($request->customer_id)){
            $customer_id = $request->customer_id;
        }
        if(isset($request->type)){
            $selected_report_type = $request->type;
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $model = CreditMemo::where('credit_memo.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);
        if (!empty($selected_report_type)){
            
                $model = $model->where('credit_memo.status', $selected_report_type);
        }
        if (!empty($customer_id)){
                
                $model = $model->where('credit_memo.customer_id', $customer_id);
        }
        $model = $model->select('credit_memo.*', 'c.name as customer_name', 'c.u_id as customer_u_id')
                ->orderBy('id', 'desc')
                ->get();

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $customers = $new_cat;
        
        return view('front.credit_memo.index', compact('model', 'date','customers','selected_report_type'));
    }

    public function create($customer_id = '0') {

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();

//        if(count($customers) > 0 && $customer_id == '0')
//           return redirect('/credit-memo/create/'.$customers[0]->id);
//        if ($customer_id == '0')
//            $customer_id = $customers[0]->id;
        $packages = Packages::select(['id'])->where('deleted', '=', '0')->get();
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $address = '';
        $phone = '';
        $template_name = '';

//        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $new_package = [];
        foreach ($packages as $package) {
            $new_package[0] = 'Select Package';
            $new_package[$package->id] = $package->id;
        }

        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        if ($customer_id != '0') {
            $customer = Customers::where('id', $customer_id)
                    ->leftjoin('states as s', 's.code', '=', 'customers.state')
                    ->select('customers.*', 's.title as state')
                    ->first();
            $address = $customer->address1 . ' ' . $customer->address2 . ' ' . $customer->city . ' ' . $customer->state . ' ' . $customer->zip_code;
            $customer_name = $customer->name;
            $phone = $customer->phone;
            $template = PriceTemplates::where('id', $customer->selling_price_template)->first();
            $template_name = $template->title;
            $items = CommonController::getItemFormsData($customer->selling_price_template);
        } else {
            $items = CommonController::getItemFormsData('1');
        }

        //$items = CommonController::getItemFormsData('1');
        $packages = $new_package;
        $bundles = $new_bundle;
        $customers = $new_cat;
        return view('front.credit_memo.create', compact('items', 'customers', 'packages','bundles', 'customer_id', 'customer_name', 'address', 'template_name', 'phone'));
    }

    public function edit($id) {

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $packages = Packages::select(['id'])->where('deleted', '=', '0')->get();
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
       
        $address = '';
        $phone = '';
        $template_name = '';

//        $model = CreditMemo::where('credit_memo.id', '=', $id)
//                ->where('credit_memo.status', '!=', 'pending')
//                ->get();
//
//        if (count($model) > 0) {
//            return redirect('/');
//        }

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->select('credit_memo.*', 'c.id as customer_id')
                ->get();

//        $customer = Customers::where('id', '=', $model['0']->customer_id)->first();
        $customer = Customers::where('id', '=', $model['0']->customer_id)
                ->leftjoin('states as s', 's.code', '=', 'customers.state')
                ->select('customers.*', 's.title as state')
                ->first();
        $address = $customer->address1 . ' ' . $customer->address2 . ' ' . $customer->city . ' ' . $customer->state . ' ' . $customer->zip_code;
        $items = CommonController::getItemFormsData($customer->selling_price_template);
        $phone = $customer->phone;
        $template = PriceTemplates::where('id', $customer->selling_price_template)->first();
        $template_name = $template->title;

        $credit_memo_items = CreditMemoItems::where('credit_memo_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'credit_memo_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'credit_memo_items.bundle_id')
                ->where('credit_memo_items.deleted', '=', '0')
                ->select('credit_memo_items.*', 'i.name as item_name', 'i.cost as cost','b.name as bundle_name')
                ->get();
        
                $new_package = [];
        foreach ($packages as $package) {
            $new_package[0] = 'Select Package';
            $new_package[$package->id] = $package->id;
        }
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $new_cat = [];
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $customers = $new_cat;
        $packages = $new_package;
        $bundles =$new_bundle;

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.credit_memo.edit', compact('items', 'customers', 'model','bundles' ,'credit_memo_items', 'packages', 'address', 'template_name', 'phone'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $customer_id = $request->customer_id;
        $validation = array(
            'customer_id' => 'not_in:0',
        );
        $messages = array(
            'customer_id.not_in' => 'Please select any customer',
        );

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        $items = $input['num'];
        $credit_memo = new CreditMemo;
        $credit_memo->created_by = 'admin';

        if (Auth::user()->role_id == '4')
            $credit_memo->created_by = 'customer';

        $credit_memo->customer_id = $customer_id;
        $credit_memo->memo = $request->memo;
        $timestamp = strtotime($request->statement_date);
        $credit_memo->statement_date = date("Y-m-d H:i:s", $timestamp);
        $credit_memo->deleted = '1';
        $credit_memo->save();

        $total_invoice_price = 0;
        $total_quantity = 0;
        if (count($items) > 0 && isset($credit_memo->id)) {

//            if ($input['item_id_1'] != '' || $input['item_id_2'] != '') {

            for ($i = 1; $i <= count($items); $i++) {
                if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];

                    $credit_memo_items = new CreditMemoItems;
                    $credit_memo_items->credit_memo_id = $credit_memo->id;
                    $credit_memo_items->customer_id = $customer_id;
                    $credit_memo_items->item_id = $input['item_id_' . $i];
                    $credit_memo_items->item_unit_price = $input['price_' . $i];
                    $credit_memo_items->quantity = $input['quantity_' . $i];
                    $credit_memo_items->statement_date = date("Y-m-d H:i:s", $timestamp);


                    if (isset($input['item_package_id_' . $i]))
                        $credit_memo_items->package_id = $input['item_package_id_' . $i];
                    if (isset($input['item_bundle_id_' . $i])) {
                            $credit_memo_items->bundle_id = $input['item_bundle_id_' . $i];
                            $credit_memo_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                      }
    


                    if ($input['start_serial_number_' . $i] > 0) {

                        $credit_memo_items->start_serial_number = $input['start_serial_number_' . $i];
                        $credit_memo_items->end_serial_number = $input['end_serial_number_' . $i];
                        $credit_memo_items->quantity = $input['quantity_' . $i];

                        ///////// end serial num or quantity /////
//                            if ($input['end_serial_number_' . $i] > 0) {
//                                $credit_memo_items->end_serial_number = $input['end_serial_number_' . $i];
//                                $credit_memo_items->quantity = $input['end_serial_number_' . $i] - ($input['start_serial_number_' . $i]+1);
//                                
//                            } else {
//                                $credit_memo_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
//                                $credit_memo_items->quantity = $input['quantity_' . $i];
//                            }
                    }

                    $credit_memo_items->total_price = $item_total_price;
//                        $credit_memo_items->created_by = $user_id;
                    $total_invoice_price += $credit_memo_items->total_price;
                    $total_quantity += $credit_memo_items->quantity;
                    $credit_memo_items->save();
                }
            }

            if ($total_quantity == 0) {
                DB::rollBack();
                Session::flash('error', 'Item count is greater than 0.');
                return redirect()->back();
            }

            CreditMemo::where('id', '=', $credit_memo->id)->update([
                'deleted' => 0,
                'total_price' => $total_invoice_price,
                'total_quantity' => $total_quantity,
            ]);
//            } else {
//                DB::rollBack();
//                Session::flash('error', 'Credit Memo is not created. Please add atleast 1 item.');
//                return redirect()->back();
//            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Credit Memo is not created. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Credit Memo is created successfully');
        return redirect()->back();
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $user_id = Auth::user()->id;
        $customer_id = $request->customer_id;

        $credit_memo = CreditMemo::where('id', '=', $input['id'])->get();
        DB::beginTransaction();
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d", $timestamp);
        CreditMemo::where('id', '=', $input['id'])->update([
            'memo' => $input['memo'],
            'statement_date' => $statement_date
        ]);

        $credit_memo = $credit_memo[0];

        $invoice_total_price = 0;
        $total_quantity = 0;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $credit_memo_item = CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->get();
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {
                            $end_serial_num = 0;
                            $serial_quantity = 0;
                            ///////// end serial num or quantity /////
                            if ($input['end_serial_number_' . $i] > 0) {
                                $end_serial_num = $input['end_serial_number_' . $i];
                                $serial_quantity = ($input['end_serial_number_' . $i] - $input['start_serial_number_' . $i]) + 1;
                            } else {
                                $serial_quantity = $input['quantity_' . $i];
                                $end_serial_num = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            }

                            if ($total_quantity == 0) {
                                DB::rollBack();
                                Session::flash('error', 'Item count is greater than 0.');
                                return redirect()->back();
                            }

                            CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $serial_quantity,
                                'total_price' => $item_total_price,
                                'start_serial_number' => $input['start_serial_number_' . $i],
                                'end_serial_number' => $end_serial_num,
                                'statement_date' => $statement_date,
                            ]);
                        } else {
                            CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $credit_memo_items = new CreditMemoItems;
                    $credit_memo_items->credit_memo_id = $input['id'];
                    $credit_memo_items->customer_id = $customer_id;
                    $credit_memo_items->item_id = $input['item_id_' . $i];
                    $credit_memo_items->item_unit_price = $input['price_' . $i];
                    $credit_memo_items->total_price = $item_total_price;
                    $credit_memo_items->quantity = $input['quantity_' . $i];
                    $credit_memo_items->statement_date = $statement_date;

                    if (isset($input['item_package_id_' . $i]))
                        $credit_memo_items->package_id = $input['item_package_id_' . $i];
                        if (isset($input['item_bundle_id_' . $i])) {
                            $credit_memo_items->bundle_id = $input['item_bundle_id_' . $i];
                            $credit_memo_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                        }

                    if ($input['start_serial_number_' . $i] > 0) {
                        $credit_memo_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['end_serial_number_' . $i] > 0) {
                            $credit_memo_items->end_serial_number = $input['end_serial_number_' . $i];
                            $credit_memo_items->quantity = ($input['end_serial_number_' . $i] - $input['start_serial_number_' . $i]) + 1;
                            ;
                        } else {
                            $credit_memo_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $credit_memo_items->quantity = $input['quantity_' . $i];
                        }
                    }


                    $credit_memo_items->quantity = $credit_memo_items->quantity;
//                    $credit_memo_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $credit_memo_items->quantity;
                    $credit_memo_items->save();
//                    print_r($invoice_items); die;
                }
            }

            if ($invoice_total_price > 0) {
                CreditMemo::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Credit Memo is not updated. Please add atleast 1 proper item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Credit Memo is not updated. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Credit Memo is updated successfully');
        return redirect()->back();
    }

    public function postModified(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $user_id = Auth::user()->id;

        $credit_memo = CreditMemo::where('id', '=', $input['id'])->get();
        DB::beginTransaction();
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d", $timestamp);
        CreditMemo::where('id', '=', $input['id'])->update([
            'memo' => $input['memo'],
            'statement_date' => $statement_date
        ]);

        $credit_memo = $credit_memo[0];
        $customer_id = $credit_memo->customer_id;
        $invoice_total_price = 0;
        $total_quantity = 0;
        $old_total_price = $credit_memo->total_price;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $credit_memo_item = CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->first();
                    $old_quantity = $credit_memo_item->quantity;
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);

//                        print_r($credit_memo_item->item_id); die;
                        self::updateAdminInventory($credit_memo_item->item_id, $credit_memo_item->quantity, 'subtract');
                        CustomerInventory::updateInventory($credit_memo_item->item_id, $credit_memo_item->quantity, 'add', $customer_id);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {
                            //// no serial functionality
                        } else {

                            if ($old_quantity > $input['quantity_' . $i]) {
                                $new_quantity = $old_quantity - $input['quantity_' . $i];
                                self::updateAdminInventory($credit_memo_item->item_id, $new_quantity, 'subtract');
                                CustomerInventory::updateInventory($credit_memo_item->item_id, $credit_memo_item->quantity, 'add', $customer_id);
                            } elseif ($old_quantity < $input['quantity_' . $i]) {
                                $new_quantity = $input['quantity_' . $i] - $old_quantity;
                                self::updateAdminInventory($credit_memo_item->item_id, $new_quantity, 'add');
                                CustomerInventory::updateInventory($credit_memo_item->item_id, $new_quantity, 'subtract', $customer_id);
                            } else {

                               //do nothing
                            }



                            CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $new_credit_memo_items = new CreditMemoItems;
                    $new_credit_memo_items->credit_memo_id = $input['id'];
                    $new_credit_memo_items->customer_id = $customer_id;
                    $new_credit_memo_items->item_id = $input['item_id_' . $i];
                    $new_credit_memo_items->item_unit_price = $input['price_' . $i];
                    $new_credit_memo_items->total_price = $item_total_price;
                    $new_credit_memo_items->quantity = $input['quantity_' . $i];
                    $new_credit_memo_items->statement_date = $statement_date;

                    if (isset($input['item_package_id_' . $i]))
                        $new_credit_memo_items->package_id = $input['item_package_id_' . $i];

                    if ($input['start_serial_number_' . $i] > 0) {
                        $new_credit_memo_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['end_serial_number_' . $i] > 0) {
                            $new_credit_memo_items->end_serial_number = $input['end_serial_number_' . $i];
                            $new_credit_memo_items->quantity = ($input['end_serial_number_' . $i] - $input['start_serial_number_' . $i]) + 1;
                            ;
                        } else {
                            $new_credit_memo_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $new_credit_memo_items->quantity = $input['quantity_' . $i];
                        }
                    }


                    self::updateAdminInventory($input['item_id_' . $i], $new_credit_memo_items->quantity, 'add');
                    CustomerInventory::updateInventory($input['item_id_' . $i], $new_credit_memo_items->quantity, 'subtract', $customer_id);


                    $new_credit_memo_items->quantity = $new_credit_memo_items->quantity;
//                    $credit_memo_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $new_credit_memo_items->quantity;
                    $new_credit_memo_items->save();
//                    print_r($invoice_items); die;
                }
            }

//            if ($invoice_total_price > 0) {
                CreditMemo::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);


                $price_diff = 0;
                if ($old_total_price > $invoice_total_price) {

                    $price_diff = $old_total_price - $invoice_total_price;
                    PaymentController::updateCustomerBalance($customer_id, $price_diff, $credit_memo->id, 'credit_memo_pos', $statement_date);
                } elseif ($old_total_price < $invoice_total_price) {
                    $price_diff = $invoice_total_price - $old_total_price;
                    PaymentController::updateCustomerBalance($customer_id, $price_diff, $credit_memo->id, 'credit_memo', $statement_date);
                } else {
                    /// do nothing
                    Payments::where('payment_type', 'credit_memo')->where('payment_type_id', $credit_memo->id)->update(['statement_date' => $statement_date]);
                }

                self::updateProfit($credit_memo->id);
//            } else {
//                DB::rollBack();
//                Session::flash('error', 'Credit Memo is not Modified. Please add atleast 1 proper item.');
//                return redirect()->back();
//            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Credit Memo is not Modified. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Credit Memo is Modified successfully');
        return redirect()->back();
    }

    public function delete($id) {
        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->where('credit_memo.status', '!=', 'pending')
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }

        CreditMemoItems::where('credit_memo_id', '=', $id)->update(['deleted' => 1]);
        CreditMemo::where('id', '=', $id)->where('status', '=', 'pending')->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function changeStatus($id, $status, $type) {

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->where('credit_memo.status', '=', 'pending')
//                  ->orWhere('purchase_orders.deleted', '!=', '0')
                ->get();

        if (count($model) == 0) {
            return redirect('/');
        }

        $total_price = $model[0]->total_price;
        $customer_id = $model[0]->customer_id;

        if ($type == 'custom') {
            DB::rollBack();
            Session::flash('error', 'Server Error');
            return redirect()->back();
            CreditMemo::where('id', '=', $id)->where('status', '=', 'pending')->update(['status' => $status]);
            PaymentController::updateCustomerBalance($customer_id, $total_price, $id, 'credit_memo');
            Session::flash('success', 'Successfully Changed!');
            return redirect()->back();
        }

        $credit_memo_items = CreditMemoItems::where('credit_memo_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'credit_memo_items.item_id')
                ->where('credit_memo_items.deleted', '=', '0')
                ->select('credit_memo_items.*', 'i.name as item_name')
                ->get();

        DB::beginTransaction();
        if (count($credit_memo_items) > 0) {
            foreach ($credit_memo_items as $items) {
                if ($items->start_serial_number == '0') {
                    ////// approve normal items /////                    
                    if ($items->package_id != '0') {
                        if ($type == 'custom') {
                            DB::rollBack();
                            Session::flash('error', 'Package not found');
                            return redirect()->back();
                        } else {
                            $package = Packages::where('id', $items->package_id)->where('type', '!=', 'invoice')->first();
                            if (count($package) == 0) {
                                DB::rollBack();
                                Session::flash('error', 'Package# ' . $items->package_id . ' is not present or may be deleted.');
                                return redirect()->back();
                            }

                            if ($package->warehouse_id != '0') {
                                WarehouseInventory::updateInventory($package->warehouse_id, $items->item_id, $items->quantity, 'add');
                                Packages::where('id', $items->package_id)->update(['status' => 'assigned', 'invoice_id' => '0']);
                            } else {
                                self::updateAdminInventory($items->item_id, $items->quantity, 'subtract');
                                Packages::where('id', $items->package_id)->update(['status' => 'unassigned', 'invoice_id' => '0']);
                            }
                            CustomerInventory::updateInventory($items->item_id, $items->quantity, 'subtract', $customer_id);
                        }
                    } else {
                        if ($type == 'custom') {

                            self::updateAdminInventory($items->item_id, $items->quantity, 'add');
                        } else {
                            
                            
//                              self::updateAdminInventory($items->item_id, $items->quantity, 'add');
//                              CustomerInventory::updateInventory($items->item_id, $items->quantity, 'subtract', $customer_id);
                            
                            $a = CustomerInventory::where('item_id', $items->item_id)->get();
                            if (count($a) == 0 && $type != 'normal') {
                                DB::rollBack();
                                Session::flash('skip', 'inventory_mismatch');
                                return redirect()->back();
                            }
                            if ($a[0]->quantity < $items->quantity && $type != 'normal') {
                                DB::rollBack();
                                Session::flash('skip', 'inventory_mismatch');
                                return redirect()->back();
                            } else {
                                self::updateAdminInventory($items->item_id, $items->quantity, 'add');
                                CustomerInventory::updateInventory($items->item_id, $items->quantity, 'subtract', $customer_id);
                            }
                        }
                    }
                } else {
                    /////// approve serial Items /////
                    if ($type == 'custom')
                        self::updateAdminSerialInventory($items->item_id, $items->quantity, 'add', $customer_id, $items->start_serial_number, $id);

                    else {
                        $a = CommonController::serialItemValidate($items->item_id, $items->start_serial_number, $items->quantity, $customer_id);
                        if ($a == 0 && $type != 'normal') {
                            DB::rollBack();
                            Session::flash('skip', 'inventory_mismatch');
                            return redirect()->back();
                        } else {
                            self::updateAdminSerialInventory($items->item_id, $items->quantity, 'add', $customer_id, $items->start_serial_number, $id);
                            //Functions::makeSerialCurlRequest($items->start_serial_number,$items->start_serial_number+$items->quantity,'disable');
                        }
                    }
                }
            }
        }

        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        foreach ($credit_memo_items as $credit_memo_item) {

            $item = Items::where('id', $credit_memo_item->item_id)->first();

            CreditMemoItems::where('id', $credit_memo_item->id)->update([
                'item_unit_price' => $credit_memo_item->item_unit_price,
                'total_price' => $credit_memo_item->item_unit_price * $credit_memo_item->quantity,
                'item_unit_cost' => $item->cost,
                'total_cost' => $item->cost * $credit_memo_item->quantity,
                'loss' => ($credit_memo_item->item_unit_price * $credit_memo_item->quantity) - ($item->cost * $credit_memo_item->quantity)
            ]);

            $total_price = $total_price + ($credit_memo_item->item_unit_price * $credit_memo_item->quantity);
            $total_cost = $total_cost + ($item->cost * $credit_memo_item->quantity);
            $total_profit = $total_price - $total_cost;
        }


        CreditMemo::where('id', '=', $id)->where('status', '=', 'pending')->update(['status' => $status, 'total_price' => $total_price, 'total_cost' => $total_cost, 'loss' => $total_profit]);
        PaymentController::updateCustomerBalance($customer_id, $total_price, $id, 'credit_memo', $model[0]->statement_date);
        DB::commit();
        Session::flash('success', 'Successfully Changed!');
        return redirect()->back();
    }

    public function detail($id, $show_profit = 0) {

        self::updateProfit($id);
        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->where('credit_memo.deleted', '=', '0')
                ->select('credit_memo.*', 'c.id as customer_id', 'c.name as customer_name')
                ->get();

        $po_items = CreditMemoItems::where('credit_memo_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'credit_memo_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'credit_memo_items.bundle_id')
                ->where('credit_memo_items.deleted', '=', '0')
                ->select('credit_memo_items.*', 'i.name as item_name','b.name as bundle_name')
                ->get();
               
        


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        $extra = '';
        return view('front.credit_memo.detail', compact('model', 'po_items', 'extra', 'show_profit'));
    }

    public function printPage($id) {

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->leftjoin('states as s', 's.code', '=', 'c.state')
                ->where('credit_memo.deleted', '=', '0')
//                ->select('credit_memo.*', 'c.id as customer_id', 'c.name as customer_name')
                ->select('credit_memo.*', 's.title as state', 'c.id as customer_id', 'c.name as customer_name', 'c.show_image', 'c.show_serial', 'show_package_id', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'c.phone', 'c.address1')
                ->get();

        $po_items = CreditMemoItems::where('credit_memo_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'credit_memo_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'credit_memo_items.bundle_id')
                ->where('credit_memo_items.deleted', '=', '0')
                ->select('credit_memo_items.*', 'i.name as item_name','b.name as bundle_name')
                ->get();

          
        if (count($model) == 0) {
            return redirect('/');
        }

        $setting = PrintPageSetting::where('user_type', '=', 'admin')->first();

        $model = $model[0];
        $extra = '';
        return view('front.credit_memo.print', compact('model', 'po_items', 'extra', 'setting'));
    }

    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }

    function updateCustomerBalanceNOTUSED($customer_id, $amount, $item_id) {

        $customer = Customers::where('id', $customer_id)->get();
        $final_amount = $customer[0]->balance - $amount;

        Customers::where('id', $customer_id)->update(['balance' => $final_amount]);

        $payments = new Payments;
        $payments->amount = '-' . $amount;
        $payments->status = 'approved';
        $payments->user_type = 'customer';
        $payments->user_id = $customer_id;
        $payments->payment_type = 'credit_memo';
        $payments->payment_type_id = $item_id;
        $payments->transaction_type = 'negative';
        $payments->save();
    }

    function updateAdminInventory($item_id, $quantity, $type = 'add') {

        $item = Inventory::where('item_id', $item_id)->get();
        
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->save();
            }
        } else {

            if (count($item) > 0) {

                $total_quantity = $item[0]->quantity - $quantity;
                if ($total_quantity < 0) {
                    Inventory::where('item_id', '=', $item_id)->update([
                        'quantity' => $total_quantity,
                    ]);
                } else {
                    Inventory::where('item_id', '=', $item_id)->update([
                        'quantity' => $total_quantity,
                    ]);
                }
            } else {
                
                  $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = '-'.$quantity;
                $Inventory->save();
                
            }
        }
    }

    function updateAdminSerialInventory($item_id, $quantity, $type = 'add', $customer_id, $start_serial, $location_id) {
        // $items = ItemSerials::where('item_id', $item_id)->where('serial', $start_serial)->where('type', 'inventory')->get();
        if ($type == 'add') {
            ItemSerials::whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->update([
                'customer_id' => '0',
                'type' => 'inventory',
                'status' => 'inactive',
                'location' => 'credit_memo',
                'location_id' => $location_id,
            ]);
        } else {
//            ItemSerials::whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->update([
//                'customer_id' => $customer_id,
//                'type' => 'sold',
//                'status' => 'activ',
//                 'location' => 'invoice',
//                'location_id' => $location_id,
//            ]);
        }
    }

    private function updateProfit($credit_memo_id) {

        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;

        $credit_memo_items = CreditMemoItems::where('credit_memo_id', $credit_memo_id)->where('deleted', '0')->get();
        foreach ($credit_memo_items as $credit_memo_item) {

            $item = Items::where('id', $credit_memo_item->item_id)->first();

            CreditMemoItems::where('id', $credit_memo_item->id)->update([
                'item_unit_price' => $credit_memo_item->item_unit_price,
                'total_price' => $credit_memo_item->item_unit_price * $credit_memo_item->quantity,
                'item_unit_cost' => $item->cost,
                'total_cost' => $item->cost * $credit_memo_item->quantity,
                'loss' => ($credit_memo_item->item_unit_price * $credit_memo_item->quantity) - ($item->cost * $credit_memo_item->quantity)
            ]);

            $total_price = $total_price + ($credit_memo_item->item_unit_price * $credit_memo_item->quantity);
            $total_cost = $total_cost + ($item->cost * $credit_memo_item->quantity);
            $total_profit = $total_price - $total_cost;
        }


        CreditMemo::where('id', '=', $credit_memo_id)->update(['total_price' => $total_price, 'total_cost' => $total_cost, 'loss' => $total_profit]);
    }

}
