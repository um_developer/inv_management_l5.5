<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\PriceTemplates;
use App\ItemPriceTemplates;
use App\Invoices;
use App\Orders;
use App\Bundles;
use App\BundleItems;
use App\OrderItems;
use App\CreditMemo;
use App\CreditMemoItems;
use App\ReturnOrders;
use App\ReturnOrderItems;
use App\InvoiceItems;
use App\Customers;
use App\Clients;
use App\PoItems;
use App\Comments;
use App\Inventory;
use App\PurchaseOrders;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\PaymentAccountTransactions;
use App\AccountTypes;
use App\PrintPageSetting;
use App\Payments;
use Auth;
use App\GalleryImages;
use Session;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;

class CommonController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | PurchaseOrder Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';

    public function __construct() {

        $this->middleware('auth');
        $this->user_id = Auth::user()->id;
        //parent::__construct();
    }

    public static function serialItemValidate($item_id, $start_serial, $quantity, $customer_id = '0') {

        if ($customer_id == '0')
            $items = ItemSerials::where('item_id', $item_id)->where('deleted', '0')->where('serial', $start_serial)->where('type', 'inventory')->get();
        else {
            $items = ItemSerials::where('item_id', $item_id)->where('deleted', '0')->where('serial', $start_serial)->where('type', 'sold')->where('customer_id', $customer_id)->get();
        }
        if (count($items) > 0) {
            if ($customer_id == '0')
                $item_count = ItemSerials::where('item_id', $item_id)->where('deleted', '0')->where('type', 'inventory')->whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->count();
            else
                $item_count = ItemSerials::where('item_id', $item_id)->where('deleted', '0')->where('type', 'sold')->where('customer_id', $customer_id)->whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->count();

            if ($item_count == $quantity)
                return 1;
            else
                return 0;
        } else
            return 0;
    }

    public static function initilizeCreditMemoFrom() {

        $user_id = Auth::user()->id;

        $search_model = Items::where('items.deleted', '=', '0')
                ->where('serial', 'no')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->select('items.*', 'c.name as category_name')
                ->get();

        $customer = Customers::where('user_id', $user_id)->get();

        if (count($customer) > 0)
            $template = $customer[0]->sale_price_template;
        else
            $template = 'sale_price';

        $serial_items = ItemSerials::where('item_serials.deleted', '=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
                ->select('i.*')
                ->groupBy('item_id')
                ->get();

        $items = [];
        $i = 0;
        foreach ($search_model as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
//            $items[$i]['code'] = $item->code;
//            $items[$i]['sku'] = $item->sku;
            $items[$i]['id'] = $item->id;
            $items[$i]['sale_price'] = $item->$template;
            if ($items[$i]['sale_price'] < 1)
                $items[$i]['sale_price'] = $item->sale_price;
            $items[$i]['value'] = $item->name;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['serial'] = $item->serial;

            if ($item->serial) {
                $items[$i]['start_serial'] = $item->code;
            }
            $i++;
        }

        foreach ($serial_items as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
//            $items[$i]['code'] = $item->code;
//            $items[$i]['sku'] = $item->sku;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['sale_price'] = $item->$template;
            if ($items[$i]['sale_price'] < 1)
                $items[$i]['sale_price'] = $item->sale_price;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;

            $i++;
        }

        return $items;
    }

    public static function getItemListByTemplate($price_template_id) {

        $item_exist = ItemPriceTemplates::where('price_template_id', $price_template_id)->where('display', '1')->get()->toArray();
        $my_items = ItemPriceTemplates::where('price_template_id', $price_template_id)->where('display', '1')->pluck('item_id')->toArray();
//        print_r($item_exist); die;
        $new_merged = [];
        foreach ($item_exist as $item) {

            $new_merged[$item['item_id']] = $item;
        }
        $data['item_id'] = $my_items;
        $data['item_details'] = $new_merged;
        return $data;
    }

    public static function getItemFormsData($price_template_id, $template = 0) {

        $item_template = Self::getItemListByTemplate($price_template_id);

        $search_model = Items::where('items.deleted', '=', '0')
                ->where('serial', 'no')
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('items.parent_item_id', '=', '0')
                ->whereIn('items.id', $item_template['item_id'])
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->select('items.*', 'c.name as category_name')
                ->with('ItemColors.Colors', 'ItemSizes.Sizes', 'ItemTypes.Types')
                ->get();


        // $template = $customer->sale_price_template;

        $serial_items = ItemSerials::where('item_serials.deleted', '=', '0')
                ->where('item_serials.customer_id', 0)
                ->where('i.deleted', '0')
                ->where('i.parent_item_id', '=', '0')
                ->where('i.status', '1')
                ->where('i.serial', '1')
                ->where('item_serials.type', 'inventory')
                ->where('item_serials.status', 'inactive')
                ->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
                ->whereIn('i.id', $item_template['item_id'])
                ->select('i.*')
                ->groupBy('item_id')
                ->get();

        $items = [];
        $i = 0;
//        print_r($serial_items); die;
        foreach ($serial_items as $item) {

            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['id'] = $item->id;
            $items[$i]['image'] = $item->image;
//            $items[$i]['sale_price'] = $item->$template;
            $items[$i]['has_sub_item'] = $item->has_sub_item;

//            if ($items[$i]['sale_price'] < 1)
//                $items[$i]['sale_price'] = $item->sale_price;

            $items[$i]['sale_price'] = $item_template['item_details'][$item->id]['selling_price'];
            $items[$i]['value'] = $item->name;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['serial'] = $item->serial;

            if ($item->serial) {
                $items[$i]['start_serial'] = $item->code;
            }
            $i++;
        }

        foreach ($search_model as $item) {
//            print_r($item->ItemColors); die;
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['id'] = $item->id;
            $items[$i]['name'] = $item->name;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['image'] = $item->image;
            $items[$i]['has_sub_item'] = $item->has_sub_item;
//            $items[$i]['sale_price'] = $item->$template;
//            if ($items[$i]['sale_price'] < 1)
//                $items[$i]['sale_price'] = $item->sale_price;

            $items[$i]['sale_price'] = $item_template['item_details'][$item->id]['selling_price'];
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;

//            $items[$i]['ItemColors'] = $item->ItemColors->toArray();
//            $items[$i]['ItemSizes'] = $item->ItemSizes->toArray();
//            $items[$i]['ItemTypes'] = $item->ItemTypes->toArray();

            $i++;
        }

        return $items;
    }

    public function getModalSubitems($item_id) {

        $sub_items = Items::where('parent_item_id', '=', $item_id)->where('deleted', '0')->orderBy('name_2', 'ASC')->get();
        return view('front.common.ajax_modal_subitems', compact('sub_items'));
    }

    public static function updateAccountTransaction($account_type_id, $amount, $source_type, $source_id, $created_by_type, $type = 'add') {

        $user_id = Auth::user()->id;
        $account_balance = AccountTypes::where('id', $account_type_id)->first();

        $acc_previous_balance = $account_balance->amount;


        if ($type == 'add') {
            AccountTypes::where('id', $account_type_id)->increment('amount', $amount);
            $acc_updated_balance = $account_balance->amount + $amount;
        } else {
            AccountTypes::where('id', $account_type_id)->decrement('amount', $amount);
            $acc_updated_balance = $account_balance->amount - $amount;
        }

        $account_type_transaction = new PaymentAccountTransactions;
        $account_type_transaction->account_type_id = $account_type_id;

        if ($source_type == 'payment')
            $account_type_transaction->payment_id = $source_id;
        elseif ($source_type == 'transfer')
            $account_type_transaction->source_id = $source_id;
        elseif ($source_type == 'delete')
            $account_type_transaction->payment_id = $source_id;

        $account_type_transaction->source_type = $source_type;
        $account_type_transaction->previous_balance = $acc_previous_balance;
        $account_type_transaction->user_type = $created_by_type;
        $account_type_transaction->created_by = $user_id;
        $account_type_transaction->message = '';
        $account_type_transaction->updated_balance = $acc_updated_balance;
        if ($type == 'add')
            $account_type_transaction->amount = $amount;
        else
            $account_type_transaction->amount = '-' . $amount;
        $account_type_transaction->save();
    }

    public static function getItemPrice($item_id, $customer_id, $type) {

       $items = self::getItemIds($item_id);	

        if ($type == 'invoice' || $type == 'credit_memo') {
            $res = Invoices::where('invoices.status', '!=', 'pending')->where('invoices.customer_id', $customer_id)
                    ->leftjoin('invoice_items as it', 'it.invoice_id', '=', 'invoices.id')->where('invoices.deleted','!=','1');
        } elseif ($type == 'order' || $type == 'order_return') {
            $res = Orders::where('orders.status', '!=', 'pending')->where('orders.client_id', $customer_id)->where('orders.deleted','!=','1')
                    ->leftjoin('order_items as it', 'it.order_id', '=', 'orders.id');
        } elseif ($type == 'purchase_order') {

            $res = PurchaseOrders::leftjoin('po_items as it', 'it.po_id', '=', 'purchase_orders.id');
        }

        if (count($items) > 0)
            $res = $res->whereIn('it.item_id', $items);
        else
            $res = $res->where('it.item_id', $item_id);

        $res = $res->where('it.deleted','!=', '1')
//                ->where('it.customer_id', $customer_id)
                        ->select('it.item_unit_price', 'it.item_unit_cost')
                        ->orderBy('it.id', 'DESC')
                        ->get()->take(3)->toArray();

        if (count($res) == 0)
            return '0';

        $model['sale_price'] = $res;
        $model['cost'] = $res[0]['item_unit_cost'];

        return $model;
    }

    public function getItemPriceHtml($item_id, $customer_id, $type) {
        
	    $items = self::getItemIds($item_id);	

        $client_templete_price = '0';
        $invoice_template_sale_price = 'not found';
        $invoice_template_cost_price = 'not found';
        $customer_templete_cost_price = '0';

        if ($type == 'invoice' || $type == 'credit_memo') {
            $type = 'Inv';
            $res = Invoices::where('invoices.status', '!=', 'pending')->where('invoices.customer_id', $customer_id)->where('invoices.deleted','!=','1')
                            ->leftjoin('invoice_items as it', 'it.invoice_id', '=', 'invoices.id')
                            ->select('it.item_unit_price', 'it.item_unit_cost', 'invoices.id','invoices.deleted')
                            ->groupBy('invoices.id');
        } elseif ($type == 'order' || $type == 'order_return') {
            $type = 'Odr';
            $res = Orders::where('orders.status', '!=', 'pending')->where('orders.client_id', $customer_id)->where('orders.deleted','!=','1')
                            ->leftjoin('order_items as it', 'it.order_id', '=', 'orders.id')
                            ->select('it.item_unit_price', 'it.item_unit_cost', 'orders.id','orders.deleted')
                            ->groupBy('orders.id');
            $item = Items::where('id', $item_id)->first();
            $client = Clients::where('id', $customer_id)->first();
            $customer = Customers::where('id', $client->customer_id)->first();

			$item_id_2 = $item_id;
            if ($item->parent_item_id != '0')
                $item_id_2 = $item->parent_item_id;

            $item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->whereIn('item_id', $items)->first();
            $template_item_cost_order = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id', $item_id_2)->first();
            if (!empty($template_item_cost_order)) {
                $customer_templete_cost_price = $template_item_cost_order->selling_price;
            }

            $client_templete_price = 'not found';

            if (!empty($item_price)) {
                $client_templete_price = $item_price->selling_price;
                $customer_templete_cost_price = $template_item_cost_order->selling_price;
            }
        } elseif ($type == 'purchase_order') {
            $type = 'PO';
            $res = PurchaseOrders::leftjoin('po_items as it', 'it.po_id', '=', 'purchase_orders.id')
                    ->leftjoin('items as i', 'i.id', '=', 'it.item_id')
                    ->select('it.item_unit_price', 'i.cost as item_unit_cost', 'purchase_orders.id','purchase_orders.deleted')
                    ->groupBy('purchase_orders.id');
        }

		
        if (count($items) > 0)
            $res = $res->whereIn('it.item_id', $items);
        else
            $res = $res->where('it.item_id', $item_id);

        $res = $res->where('it.deleted','0')
                        ->orderBy('it.id', 'DESC')
                        ->get()->take(3)->toArray();

        if (empty($res)) {

            $new_array = [];
            $item = Items::where('id', $item_id)->first();
            $po_item = PoItems::where('item_id', $item_id)->orderBy('id', 'desc')->first();
            $po_price = 'not found';
            if (count($po_item) > 0)
                $po_price = $po_item->item_unit_price;

            if ($type == 'invoice' || $type == 'credit_memo' || $type == 'Inv') {

                $new_array[0]['item_unit_price'] = '';
                $new_array[0]['item_unit_cost'] = $po_price;
                $new_array[0]['id'] = '00';

                $res = $new_array;
            } elseif ($type == 'order' || $type == 'order_return' || $type == 'Odr') {
                $client = Clients::where('id', $customer_id)->first();
                $customer = Customers::where('id', $client->customer_id)->first();
                //if ($item->parent_item_id != '0')
                  //  $item_id = $item->parent_item_id;

                $item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$item_id)->first();
                if (!empty($item_price)) {
                    $template = PriceTemplates::where('id', $customer->selling_price_template)->first();
                    $new_array[0]['item_unit_cost'] = '*' . $item_price->selling_price . ' (' . $template->title . ')';
                }

                $new_array[0]['item_unit_price'] = '';
                $new_array[0]['item_unit_cost'] = '*0';
                $new_array[0]['id'] = '00';

                if ($new_array[0]['item_unit_cost'] == '*0')
                    $new_array[0]['item_unit_cost'] = 'Invoice not found';

                $res = $new_array;
            }
        }

        if ($type == 'invoice' || $type == 'credit_memo' || $type == 'Inv') {
            $customer = Customers::where('id', $customer_id)->first();
            $item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->whereIn('item_id', $items)->first();
            if (count($item_price) > 0) {
                $invoice_template_sale_price = $item_price->selling_price;
            }
            $item = Items::where('id', $item_id)->first();
             $invoice_template_cost_price = $item->cost;
        }

        
        $data = view('front.common.ajax_sale_price', compact('res', 'type', 'client_templete_price', 'customer_templete_cost_price', 'invoice_template_sale_price', 'invoice_template_cost_price'))->render();

        return $data;
    }

    public function getTempleteCategories($template_id) {

//        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->pluck('item_id')->toArray();
        $categories = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $template_id)
//                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
//                ->select('items.category_id', 'c.name as category_name')
                ->orderBy('items.category_id', 'desc')
                ->groupBy('items.category_id')
                ->select('c.name as category_name', 'items.category_id as category_id')
                ->get();

        $new_cat = [];
        foreach ($categories as $po) {
            $new_cat[$po->category_id] = $po->category_name;
        }
        $categories = $new_cat;
        $data = view('front.common.ajax_gallery_cat', compact('categories'))->render();
        return $data;
    }

    public function getTempleteItems($template_id, $category_id) {

//        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->pluck('item_id')->toArray();


        $drop_down_items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $template_id)
//                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1');

        if ($category_id != '0')
            $drop_down_items = $drop_down_items->where('items.category_id', $category_id);

        $drop_down_items = $drop_down_items->select('items.*')
                ->orderBy('items.id', 'desc')
                ->get();

        $new_cat = [];
        foreach ($drop_down_items as $po) {
            $new_cat[$po->id] = $po->name;
        }
        $items = $new_cat;
        $data = view('front.common.ajax_gallery_items', compact('items'))->render();
        return $data;
    }

    public function changeDeliverStatus(Request $request) {

        $req = $request->all();


        if (isset($req['type']) && !empty($req['type'])) {

            $ids = explode(",", $req['bulk_item_id']);

            if ($req['table'] == 'invoices') {
                foreach ($ids as $key => $value) {
                    if ($value != "") {

                        Invoices::where('id', $value)->update(['status' => $req['status']]);
                        $status = '';
                        if ($req['status'] == 'delivered') {
                            $status = 'delivered';
                        } elseif ($req['status'] == 'approved') {
                            $status = 'approved';
                        }

                        $comment = new Comments;
                        $comment->form_id = $value;
                        $comment->form_type = 'invoice';
                        $comment->delivered_comments = $req['delivered_comments'];
                        $comment->user_id = Auth::user()->id;
                        $comment->status_type = $status;
                        $comment->user_type = 'admin';
                        $comment->save();
                        if ($req['status'] == 'delivered') {
                            $results = Invoices::where('id', $value)->first();
                            $check = Orders::where('id', $results->order_id)->first();
                            if ($check['invoice_refrence_id'] == $value) {
                                Orders::where('id', $results->order_id)->update(['status' => 'completed']);
                            }
                        } elseif ($req['status'] == 'approved') {
                            $results = Invoices::where('id', $value)->first();
                            $check = Orders::where('id', $results->order_id)->first();
                            if ($check['invoice_refrence_id'] == $value) {
                                Orders::where('id', $results->order_id)->update(['status' => 'approved']);
                            }
                        }
                    }
                }
            } elseif ($req['table'] == 'orders') {
                foreach ($ids as $key => $value) {
                    if ($value != "") {
                        Orders::where('id', $value)->update(['status' => $req['status']]);
                        $status = '';
                        $results = Orders::where('id', $value)->first();
                        $check = Invoices::where('id', $results->invoice_refrence_id)->first();

                        if ($req['status'] == 'delivered') {
                            $status = 'delivered';
                            
                        } elseif ($req['status'] == 'approved') {
                            $status = 'approved';
                        }

                        if ($check['order_refrence_id'] == $value) {
                            Invoices::where('id', $results->invoice_id)->update(['status' =>  $status]);
                        }

                        

                        $comment = new Comments;
                        $comment->form_id = $value;
                        $comment->form_type = 'order';
                        $comment->delivered_comments = $req['delivered_comments'];
                        $comment->user_id = Auth::user()->id;
                        $comment->status_type = $status;
                        $comment->user_type = 'customer';
                        $comment->save();
                    }
                }
            }


            return redirect()->away($req['location']);
        } else {




            if (isset($req['table']) && $req['table'] == 'invoices') {
                $results = Invoices::where('id', $req['id'])->first();

                if ($results->status == 'approved' || $results->status == 'processing' || $results->status == 'completed') {
                    Invoices::where('id', $req['id'])->update(['status' => 'delivered']);
                    $comment = new Comments;
                    $comment->form_id = $req['id'];
                    $comment->form_type = 'invoice';
                    $comment->delivered_comments = $req['delivered_comments'];
                    $comment->user_id = Auth::user()->id;
                    $comment->status_type = 'delivered';
                    $comment->user_type = 'admin';
                    $comment->save();

                    $check = Orders::where('id', $results->order_id)->first();

                    if (!empty($check) && $check->invoice_refrence_id == $results->id) {

                        Orders::where('id', $results->order_id)->update(['status' => 'completed']);
                    }
                } else if ($results->status == 'delivered') {

                    Invoices::where('id', $req['id'])->update(['status' => 'approved']);
                    $comment = new Comments;
                    $comment->form_id = $req['id'];
                    $comment->form_type = 'invoice';
                    $comment->delivered_comments = $req['delivered_comments'];
                    $comment->user_id = Auth::user()->id;
                    $comment->status_type = 'approved';
                    $comment->user_type = 'admin';
                    $comment->save();

                    $check = Orders::where('id', $results->order_id)->first();
                    if (count($check) > 0 && $check->invoice_refrence_id == $req['id']) {
                        Orders::where('id', $results->order_id)->update(['status' => 'approved']);
                    }
                }
            } elseif (isset($req['table']) && $req['table'] == 'orders') {
                $results = Orders::where('id', $req['id'])->first();
                $check = Invoices::where('id', $results->invoice_refrence_id)->first();

                if ($results->status == 'approved' || $results->status == 'approved_charged' || $results->status == 'processing' || $results->status == 'completed') {
                    Orders::where('id', $req['id'])->update(['status' => 'delivered']);

                    Invoices::where('id', $results->invoice_refrence_id)->update(['status' =>  'delivered']);

                    $comment = new Comments;
                    $comment->form_id = $req['id'];
                    $comment->form_type = 'order';
                    $comment->delivered_comments = $req['delivered_comments'];
                    $comment->user_id = Auth::user()->id;
                    $comment->status_type = 'delivered';
                    $comment->user_type = 'customer';
                    $comment->save();
                } elseif ($results->status == 'delivered') {
                    Orders::where('id', $req['id'])->update(['status' => 'approved']);
                    Invoices::where('id', $results->invoice_refrence_id)->update(['status' =>  'approved']);
                    $comment = new Comments;
                    $comment->form_id = $req['id'];
                    $comment->form_type = 'order';
                    $comment->delivered_comments = $req['delivered_comments'];
                    $comment->user_id = Auth::user()->id;
                    $comment->status_type = 'approved';
                    $comment->user_type = 'customer';
                    $comment->save();
                }
            }

            return redirect()->away($req['location']);
        }
    }

    public function getComments($id, $status) {

        $user_type = '';

        if (Auth::user()->role_id == 4) {
            $user_type = 'customer';
            $form_type = '';
            if ($status == 'invoices') {
                $form_type = 'invoice';
            } else {
                $form_type = 'order';
            }
            $results = DB::table('comments')->where('form_id', $id)->where('form_type', $form_type)->where('user_type', $user_type)
                            ->leftjoin('customers as cu', 'cu.user_id', '=', 'comments.user_id')->select('comments.*', 'cu.name')->get();
        } else {

            $form_type = '';
            if ($status == 'invoices') {
                $form_type = 'invoice';
            } else {
                $form_type = 'order';
            }
            $user_type = 'admin';
            $results = DB::table('comments')->where('form_id', $id)->where('form_type', $form_type)->where('user_type', $user_type)
                            ->leftjoin('users as u', 'u.id', '=', 'comments.user_id')->select('comments.*', 'u.firstName', 'u.lastName')->get();
        }


        return $results;
    }

    public static function syncItems($sync_from, $source_id) {

        $total_quantity = 0;
        $order_id = 0;
        $invoice_id = 0;

        if ($sync_from == 'invoice') {

            $invoice = Invoices::where('id', $source_id)->first();
            OrderItems::where('order_id', $invoice->order_id)->update([
                'deleted' => '1'
            ]);

            $order_id = $invoice->order_id;
            $invoice_id = $source_id;
            self::updateInventoryForSync($invoice_id, 'reset');

            $order = Orders::where('id', '=', $invoice->order_id)->first();
            $items = InvoiceItems::where('invoice_id', $invoice->id)->where('deleted', 0)->get();
            $new_order_total_price = 0;
            $old_order_total_price = $order->total_price;

            $client = Clients::where('id', $order->client_id)->first();

            $customer_price_template_id = $client->selling_price_template;

            if ($client->selling_price_template == 0) {
                $customer_default_template_id = PriceTemplates::where('deleted', 0)->where('user_type', 'customer')->where('created_by', $invoice->customer_id)->where('is_default', '1')->first();
                $customer_price_template_id = $customer_default_template_id->id;
            }
            $selling_price = '00';

            foreach ($items as $row) {

                $item_price = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $row->item_id)->first();

                if (empty($item_price)) {

                    $item = Items::where('id', $row->item_id)->first();
                    if ($item->parent_item_id != '') {
                        $item_price = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $item->parent_item_id)->first();
                    }
                }

                $deleted_order_item = OrderItems::where('deleted', '1')->where('order_id', $order_id)->where('item_id', $row->item_id)->first();
                if (!empty($deleted_order_item))
                    $selling_price = $deleted_order_item->item_unit_price;
                else
                    $selling_price = $item_price->selling_price;

                $order_items = new OrderItems;
                $order_items->order_id = $order->id;
                $order_items->client_id = $order->client_id;
                $order_items->item_id = $row->item_id;
                $order_items->statement_date = $row->statement_date;
                $order_items->item_unit_price = $selling_price;
                $order_items->quantity = $row->quantity;
                $order_items->total_price = $order_items->item_unit_price * $order_items->quantity;

                $new_order_total_price += $order_items->total_price;
                $total_quantity += $order_items->quantity;
                $order_items->save();
            }

            Orders::where('id', '=', $order->id)->update([
                'deleted' => 0,
                'total_price' => $new_order_total_price,
                'total_quantity' => $total_quantity,
            ]);

            OrderItems::where('order_id', $order->id)->where('deleted', 1)->delete();

            $price_diff = 0;
            if ($old_order_total_price > $new_order_total_price) {
                $price_diff = $old_order_total_price - $new_order_total_price;
                PaymentController::updateClientBalance($order->client_id, $price_diff, $order->id, 'order_neg', $invoice->statement_date);
            } elseif ($old_order_total_price < $new_order_total_price) {
                $price_diff = $new_order_total_price - $old_order_total_price;
                PaymentController::updateClientBalance($order->client_id, $price_diff, $order->id, 'order', $invoice->statement_date);
            } else {
                Payments::where('payment_type', 'order')->where('payment_type_id', $order->id)->update(['statement_date' => $invoice->statement_date]);
            }

            self::updateInventoryForSync($invoice_id, 'add');
        } elseif ($sync_from == 'order') {

            $order = Orders::where('id', $source_id)->first();
            $invoice = Invoices::where('id', '=', $order->invoice_refrence_id)->first();
            $order_id = $source_id;
            $invoice_id = $order->invoice_refrence_id;

            self::updateInventoryForSync($invoice_id, 'reset');

            $customer_id = $invoice->customer_id;
            $statement_date = $invoice->statement_date;
            $old_invoice_total_price = $invoice->total_price;

            InvoiceItems::where('invoice_id', $invoice['id'])->update([
                'deleted' => '1'
            ]);

            $items = OrderItems::where('order_id', $order->id)->where('deleted', 0)->get();
            $new_order_total_price = 0;
            $total_quantity = 0;
            $old_order_total_price = $invoice['total_price'];

            $customer = Customers::where('id', $invoice['customer_id'])->first();

            $customer_price_template_id = $customer['selling_price_template'];

            if ($customer['selling_price_template'] == 0) {
                $customer_price_template_id = 1;
            }
            $new_invoice_total_price = 0;
            $selling_price = '00';

            foreach ($items as $row) {
                $selling_price = self::getItemPrice($row->item_id, $invoice['customer_id'], 'invoice');
                if ($selling_price == '0') {
                    $item_price = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $row->item_id)->first();

                    if (empty($item_price)) {

                        $item = Items::where('id', $row->item_id)->first();
                        if ($item->parent_item_id != '') {
                            $item_price = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $item->parent_item_id)->first();
                        }
                    }

                    $deleted_invoice_item = InvoiceItems::where('deleted', '1')->where('invoice_id', $invoice_id)->where('item_id', $row->item_id)->first();
                    if (!empty($deleted_invoice_item))
                        $selling_price = $deleted_invoice_item->item_unit_price;
                    else
                        $selling_price = $item_price->selling_price;
                }else
                $selling_price = $selling_price['sale_price'][0]['item_unit_price'];

                $invoice_items = new InvoiceItems;
                $invoice_items->invoice_id = $invoice['id'];
                $invoice_items->customer_id = $invoice['customer_id'];
                $invoice_items->item_id = $row->item_id;
                $invoice_items->statement_date = $row->statement_date;
                $invoice_items->item_unit_price = $selling_price;
                $invoice_items->quantity = $row->quantity;
				
				//print_r($invoice_items); die;
                $invoice_items->total_price = $invoice_items->item_unit_price * $invoice_items->quantity;

                $new_invoice_total_price += $invoice_items->total_price;
                $total_quantity += $row->quantity;

                $invoice_items->save();
            }

            Invoices::where('id', '=', $invoice['id'])->update([
                'deleted' => 0,
                'total_price' => $new_invoice_total_price,
                'total_quantity' => $total_quantity,
            ]);

            Invoices::where('id', $invoice->id)->update(['total_price' => $new_invoice_total_price]);
            InvoiceItems::where('invoice_id', $invoice['id'])->where('deleted', 1)->delete();



            $price_diff = 0;
            if ($old_invoice_total_price > $new_invoice_total_price) {

                $price_diff = $old_invoice_total_price - $new_invoice_total_price;
                PaymentController::updateCustomerBalance($customer_id, $price_diff, $invoice->id, 'invoice_neg', $statement_date);
            } elseif ($old_invoice_total_price < $new_invoice_total_price) {
                $price_diff = $new_invoice_total_price - $old_invoice_total_price;
                PaymentController::updateCustomerBalance($customer_id, $price_diff, $invoice->id, 'invoice', $statement_date);
            } else {
                Payments::where('payment_type', 'invoice')->where('payment_type_id', $invoice->id)->update(['statement_date' => $statement_date]);
            }

            self::updateInventoryForSync($invoice_id, 'add');
        }

        self::calculateSyncOrderProfit($order_id, $invoice_id);
    }

    public function orderDetail($id, $show_profit = 0) {

        //self::updateProfit($id);
        $model = Orders::where('orders.id', '=', $id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                ->where('orders.deleted', '!=', '1')
                ->select('orders.*', 'c.id as client_id', 'c.name as client_name', 'c.address1 as client_address', 'c.phone as client_phone', 'c.show_image', 'c.show_serial', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'invoices.id as invoice_refrence')
                ->get();


        $po_items = OrderItems::where('order_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'order_items.item_id')
                ->where('order_items.deleted', '=', '0')
                ->select('order_items.*', 'i.name as item_name', 'i.image as image')
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.customers.orders.detail', compact('model', 'po_items', 'show_profit'));
    }

    public function OrderprintPage($id) {

                $model = Orders::where('orders.id', '=', $id)
                        ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                        ->leftjoin('states as s', 's.code', '=', 'c.state')
                        ->leftjoin('lists as i', 'i.id', '=', 'orders.list_id')
                        ->where('orders.deleted', '!=', '1')
                        ->select('orders.*', 'c.id as client_id', 's.title as state', 'c.name as client_name', 'c.address1 as client_address1', 'c.address2 as client_address2', 'c.phone as client_phone', 'c.show_image', 'c.show_serial', 'c.balance', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'i.title as list_title')
                        ->get();
        
                        $invoice_ref_id =  $model[0]->invoice_refrence_id;
                        $invoice = Invoices::find($invoice_ref_id);

                        $customer_id = 1;
                        if(isset($invoice))
                             $customer_id =  $invoice->customer_id;


                $customer = Customers::where('id', $customer_id)
                        ->leftjoin('states as s', 's.code', '=', 'customers.state')
                        ->select('customers.*', 's.title as state')
                        ->first();
                $po_items = OrderItems::where('order_id', '=', $id)
                        ->leftjoin('items as i', 'i.id', '=', 'order_items.item_id')
                        ->leftjoin('bundles as b', 'b.id', '=', 'order_items.bundle_id')
                        ->where('order_items.deleted', '=', '0')
                        ->select('order_items.*', 'i.name as item_name', 'i.image as image', 'b.name as bundle_name')
                        ->get();
        
        
                $user_id = $customer_id;
                $setting = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id', $user_id)->first();
        
                if (count($setting) < 1) {
                    $printPageSetting = new PrintPageSetting;
                    $printPageSetting->bottom_text_1 = 'FOR ACCESSORIES VISIT BROADPOSTERS.COM';
                    $printPageSetting->bottom_text_2 = 'NOTICE: OPEN ITEMS ARE NON REFUNDABLE. THANK YOU';
                    $printPageSetting->user_type = 'customer';
                    $printPageSetting->user_id = $user_id;
                    $printPageSetting->save();
                }
        
                $setting = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id', $user_id)->first();
        
        
                if (count($model) == 0) {
                    return redirect('/');
                }
                $model = $model[0];
                return view('front.customers.orders.print', compact('model', 'po_items', 'customer', 'setting'));
            }

    public static function calculateSyncOrderProfit($order_id, $invoice_id) {

        $invoice_items = InvoiceItems::where('invoice_id', $invoice_id)->where('deleted', '0')->get();

        $orders_items = OrderItems::where('order_id', $order_id)->where('deleted', '0')->get();
        $total_order_cost = 0;
        $total_order_profit = 0;


        foreach ($invoice_items as $items) {

            $order_item = OrderItems::where('order_id', $order_id)->where('item_id', $items->item_id)->where('deleted', '0')->first();

            $item_cost = $items->item_unit_price;
            $item_price = $order_item->total_price;
            $total_item_cost = $item_cost * $order_item->quantity;
            $profit = $item_price - $total_item_cost;

            $total_order_cost += $total_item_cost;
            $total_order_profit += $profit;

            OrderItems::where('order_id', $order_id)->where('item_id', $items->item_id)->where('deleted', '0')->update([
                'item_unit_cost' => $item_cost,
                'total_cost' => $total_item_cost,
                'profit' => $profit
            ]);
        }

        Orders::where('id', $order_id)->update([
            'total_cost' => $total_order_cost,
            'profit' => $total_order_profit,
        ]);
    }

    public static function updateInventoryForSync($invoice_id, $status) {

        $invoice_items = InvoiceItems::where('invoice_id', $invoice_id)->where('deleted', '0')->get();

        if (!empty($invoice_items)) {

            foreach ($invoice_items as $item) {

                if ($status == 'reset'){
                    InvoiceController::updateAdminInventory($item->item_id, $item->quantity, 'add');

                }
                else
                    InvoiceController::updateAdminInventory($item->item_id, $item->quantity, 'subtract');


//                CustomerInventory::updateInventory($item->item_id, $item->quantity, 'subtract', $customer_id);
            }
        }
    }
    
    public function bulkAddItemInInvoicebundle($bundle_id, $j, $qty, $customer_id,$type) {

        if ($bundle_id == 'Select Bundle')
            return 0;
        $po_items1 = BundleItems::where('bundle_id', '=', $bundle_id)
                ->leftjoin('items as i', 'i.id', '=', 'bundle_items.item_id')
        //                ->leftjoin('item_price_templates as ipt', 'ipt.price_template_id', '=', '1')
                ->select('bundle_items.*', 'i.name as item_name', 'i.cost as cost', 'i.sale_price', 'i.id as item_id')
                ->get();


        $po_items = [];
        $i = 0;

        $addQty = $qty;
        foreach ($po_items1 as $item) {
            
            $items = CommonController::getItemIds($item->item_id);

            $item_sale_price = 0;
            if($type=="invoice"){
                $customer = Customers::where('id', $customer_id)->first();
                $customer_template_id = $customer->selling_price_template;
                $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();
    
                if (count($item_template) > 0)
                    $item_sale_price = $item_template->selling_price;
                }elseif($type=='order'){
                    $customer = Clients::where('id', $customer_id)->first();
                    $customer_template_id = $customer->selling_price_template;
                    $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();
        
                    if (count($item_template) > 0)
                        $item_sale_price = $item_template->selling_price;
                }
            $price = CommonController::getItemPrice($item->item_id, $customer_id, 'invoice');
            if (isset($price['sale_price']) && count($price['sale_price']) > 0) {
                $item_sale_price = $price['sale_price'][0]['item_unit_price'];
            }

            if( $item_sale_price=='0.00'){
                $item_sale_price=111;
               }

            $item->invoice_id = 0;
            $item->item_id = $item->item_id;
            $item->item_unit_price = $item_sale_price;
            $item->quantity = $item->quantity * $addQty;
            $item->customer_id = $customer_id;
            $item->total_price = $item_sale_price * $item->quantity;
            $item->item_name = $item->item_name;
            $item->cost = $item->cost;
            $item->upc_barcode = $item->upc_barcode;
            $item->sku = $item->code;
            $po_items[$i] = $item;
            $i++;
        }

        $bundle_quantity = $addQty;
//      $result['j'] = count($po_items1);
//      session(['invoice_'+$package_id => count($po_items1)]);
        return view('front.common.bundle_insert_table', compact('po_items', 'bundle_id', 'j', 'bundle_quantity','type'));
    }



    public function bulkEditItemInInvoicebundle(Request $request) {
        // dd($request->all());
        if (count($request->items_id) == 0) {
            return 0;
        }
        $bundle_id = $request->bundle_id;
        $item0 = [];
        $j = $request->j; 
        $qty = $request->qty; 
        $customer_id = $request->customer_id;
        $type = $request->type;
        foreach($request->items_id as $key => $value){
            if ($value != "") {
                $item0[] = $key;
            }
        }
        // if(!empty($items_id)){
        //     $items_id = explode(',',$items_id);
        // }else{
        //     return 0;
        // }

        $po_items1 = DB::table('items as i')->whereIn('id', $item0)
                // ->leftjoin('items as i', 'i.id', '=', 'bundle_items.item_id')
        //                ->leftjoin('item_price_templates as ipt', 'ipt.price_template_id', '=', '1')
                ->select('i.name as item_name', 'i.cost as cost', 'i.sale_price', 'i.id as item_id')
                ->get();


        $po_items = [];
        $i = 0;

        $addQty = $qty;
        foreach ($po_items1 as $item) {
            if (!empty($item)) {
                $items = CommonController::getItemIds($item->item_id);

                $item_sale_price = 0;
                if($type=="invoice"){
                    $customer = Customers::where('id', $customer_id)->first();
                    $customer_template_id = $customer->selling_price_template;
                    $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();
        
                    if (count($item_template) > 0)
                        $item_sale_price = $item_template->selling_price;
                    }elseif($type=='order'){
                        $customer = Clients::where('id', $customer_id)->first();
                        $customer_template_id = $customer->selling_price_template;
                        $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();
            
                        if (count($item_template) > 0)
                            $item_sale_price = $item_template->selling_price;
                    }
                $price = CommonController::getItemPrice($item->item_id, $customer_id, 'invoice');
                if (isset($price['sale_price']) && count($price['sale_price']) > 0) {
                    $item_sale_price = $price['sale_price'][0]['item_unit_price'];
                }
    
                if( $item_sale_price=='0.00'){
                    $item_sale_price=111;
                   }
    // dd($item);
                $item->invoice_id = 0;
                $item->item_id = $item->item_id;
                $item->item_unit_price = $item_sale_price;
                $item->quantity = $request->items_id[$item->item_id] * $addQty;
                $item->customer_id = $customer_id;
                $item->total_price = $item_sale_price * 1;
                $item->item_name = $item->item_name;
                $item->cost = $item->cost;
                $item->upc_barcode = 'Mo';
                $item->sku = 'off';
                $item->delivered_quantity = 1;
                $item->start_serial_number = 1;
                $item->end_serial_number = 1;
                $po_items[$i] = $item;
                
                $i++;
            }

        }

        $bundle_quantity = $addQty;
//      $result['j'] = count($po_items1);
//      session(['invoice_'+$package_id => count($po_items1)]);
// dd($po_items);
        $bundle_id = $bundle_id;
        return view('front.common.bundle_insert_table', compact('po_items', 'bundle_id', 'j', 'bundle_quantity','type'));
    }



    public function bulkCreatebundle($bundle_id, $j, $customer_id,$type) {

        if ($bundle_id == 'Select Bundle')
            return 0;

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $bundles = Bundles::select(['id'])->where('deleted', '=', '0')->get();

        $new_cat = [];
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $new_bundle = [];
        foreach ($bundles as $bundle) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$bundle->id] = $bundle->id;
        }

        $bundles = $new_bundle;

        $model = Bundles::where('id', '=', $bundle_id)->get();

        $po_items1 = BundleItems::where('bundle_id', '=', $bundle_id)
                ->leftjoin('items as i', 'i.id', '=', 'bundle_items.item_id')
//                ->leftjoin('item_price_templates as ipt', 'ipt.price_template_id', '=', '1')
                ->select('bundle_items.*', 'i.name as item_name', 'i.cost as cost', 'i.sale_price', 'i.id as item_id')
                ->get();


        $po_items = [];
        $i = 0;
        foreach ($po_items1 as $item) {

           $items = CommonController::getItemIds($item->item_id);
            
            $item_sale_price = 0;
            if($type=="invoice"){
            $customer = Customers::where('id', $customer_id)->first();
            $customer_template_id = $customer->selling_price_template;
            $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();

            if (count($item_template) > 0)
                $item_sale_price = $item_template->selling_price;
            }elseif($type=='order'){
                $customer = Clients::where('id', $customer_id)->first();
                $customer_template_id = $customer->selling_price_template;
                $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();
                   if (count($item_template) > 0)
                    $item_sale_price = $item_template->selling_price;
            }
            $price = CommonController::getItemPrice($item->item_id, $customer_id, $type);
            if (isset($price['sale_price']) && count($price['sale_price']) > 0) {
                $item_sale_price = $price['sale_price'][0]['item_unit_price'];
            }
           if( $item_sale_price=='0.00'){
            $item_sale_price=111;
           }
            $item->invoice_id = 0;
            $item->item_id = $item->item_id;
            $item->item_unit_price = $item_sale_price;
            $item->quantity = $item->quantity;
            $item->customer_id = $customer_id;
            $item->total_price = $item_sale_price * $item->quantity;
            $item->item_name = $item->item_name;
            $item->cost = $item->cost;
            $item->upc_barcode = $item->upc_barcode;
            $item->sku = $item->code;
            $po_items[$i] = $item;
            $i++;
        }

        $vendors = $new_cat;
        if (count($model) == 0) {
            return redirect('/');
        }
 
        return view('front.common.bundle_view_table', compact('po_items', 'bundle_id', 'j', 'customer_id','type'));
        return $po_items;
    }

    public function bulkEditbundle($bundle_id, $j, $customer_id,$type, $q) {
        $id = $bundle_id;
        $model = Bundles::where('bundles.id', '=', $id)
        ->where('bundles.deleted', '=', '0')
        ->select('bundles.*')
        ->get();

$po_items = BundleItems::where('bundle_id', '=', $id)
        ->where('bundle_items.deleted', '0')
        ->leftjoin('items as i', 'i.id', '=', 'bundle_items.item_id')
        ->select('bundle_items.*', 'i.name as item_name', 'i.code as item_sku', 'i.upc_barcode as item_upc_barcode')
        ->get();

$search_model = Items::where('items.deleted', '=', '0')
        ->where('serial', 'no')
        ->where('items.deleted', '0')
        ->where('items.status', '1')
       // ->where('items.parent_item_id', '=', '0')
        ->get();

$items = [];
$i = 0;
foreach ($search_model as $item) {
    $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
    $items[$i]['upc_barcode'] = $item->upc_barcode;
    $items[$i]['sku'] = $item->code;
    $items[$i]['id'] = $item->id;
    $items[$i]['cost'] = $item->cost;
    $items[$i]['value'] = $item->name;
    $items[$i]['serial'] = $item->serial;
    $items[$i]['inventory_quantity'] = $item->inventory_quantity;


    $i++;
}

if (count($model) == 0) {
    return redirect('/');
}
$model = $model[0];

return view('front.common.bundalUpdate', compact('model', 'po_items', 'items','q'));
//         if ($bundle_id == 'Select Bundle')
//             return 0;

//         $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
//         $bundles = Bundles::select(['id'])->where('deleted', '=', '0')->get();

//         $new_cat = [];
//         foreach ($customers as $cat) {
//             $new_cat[$cat->id] = $cat->name;
//         }

//         $new_bundle = [];
//         foreach ($bundles as $bundle) {
//             $new_bundle[0] = 'Select Bundle';
//             $new_bundle[$bundle->id] = $bundle->id;
//         }

//         $bundles = $new_bundle;

//         $model = Bundles::where('id', '=', $bundle_id)->get();

//         $po_items1 = BundleItems::where('bundle_id', '=', $bundle_id)
//                 ->leftjoin('items as i', 'i.id', '=', 'bundle_items.item_id')
// //                ->leftjoin('item_price_templates as ipt', 'ipt.price_template_id', '=', '1')
//                 ->select('bundle_items.*', 'i.name as item_name', 'i.cost as cost', 'i.sale_price', 'i.id as item_id')
//                 ->get();


//         $po_items = [];
//         $i = 0;
//         foreach ($po_items1 as $item) {

//            $items = CommonController::getItemIds($item->item_id);
            
//             $item_sale_price = 0;
//             if($type=="invoice"){
//             $customer = Customers::where('id', $customer_id)->first();
//             $customer_template_id = $customer->selling_price_template;
//             $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();

//             if (count($item_template) > 0)
//                 $item_sale_price = $item_template->selling_price;
//             }elseif($type=='order'){
//                 $customer = Clients::where('id', $customer_id)->first();
//                 $customer_template_id = $customer->selling_price_template;
//                 $item_template = ItemPriceTemplates::where('price_template_id', $customer_template_id)->whereIn('item_id', $items)->first();
//                    if (count($item_template) > 0)
//                     $item_sale_price = $item_template->selling_price;
//             }
//             $price = CommonController::getItemPrice($item->item_id, $customer_id, $type);
//             if (isset($price['sale_price']) && count($price['sale_price']) > 0) {
//                 $item_sale_price = $price['sale_price'][0]['item_unit_price'];
//             }
//            if( $item_sale_price=='0.00'){
//             $item_sale_price=111;
//            }
//             $item->invoice_id = 0;
//             $item->item_id = $item->item_id;
//             $item->item_unit_price = $item_sale_price;
//             $item->quantity = $item->quantity;
//             $item->customer_id = $customer_id;
//             $item->total_price = $item_sale_price * $item->quantity;
//             $item->item_name = $item->item_name;
//             $item->cost = $item->cost;
//             $item->upc_barcode = $item->upc_barcode;
//             $item->sku = $item->code;
//             $po_items[$i] = $item;
//             $i++;
//         }

//         $vendors = $new_cat;
//         if (count($model) == 0) {
//             return redirect('/');
//         }
//  sohaib
        // return view('front.common.bundle_view_table', compact('po_items', 'bundle_id', 'j', 'customer_id','type'));
        return $po_items;
    }

    public static function getItemIds($item_id){
        
        $item = Items::where('id',$item_id)->first();
        $items = [];
        
        if($item->parent_item_id != '0'){
            //// is sub child
             $items = Items::where('deleted', '0')->where('parent_item_id', $item->parent_item_id)->pluck('id')->toArray();
             $items[] = (int) $item->parent_item_id;
            
        }else{
            
            $items = Items::where('deleted', '0')->where('parent_item_id', $item->id)->pluck('id')->toArray();
             $items[] = (int) $item->id;
            
        }
        
        return $items;
        
    }

    public static function createOrder($item_id,$order_id,$client_id,$newPrice,$activity_id){
        if ($order_id == 0) {
            $order = new Orders;
            $order->client_id = $client_id;
            $order->memo = '';
            $order->created_by = '0';
            $order->status = 'approved';
            $order->total_price = 0;
            $order->total_quantity = 0;
            $order->deleted = 2;
            $order->activity_log_id = $activity_id;
            $timestamp = strtotime(date("Y/m/d"));
            $order->statement_date = date("Y-m-d H:i:s", $timestamp);
            $order->is_dummy = 1;
            $order->save();
            $order_id = $order->id;
        }

            $order_items = new OrderItems;
            $order_items->order_id = $order_id;
            $order_items->client_id = $client_id;
            $order_items->item_id = $item_id;
            $order_items->item_unit_price = $newPrice;
            $order_items->quantity = 1;
            $timestamp = strtotime(date("Y/m/d"));
            $order_items->statement_date = date("Y-m-d H:i:s", $timestamp);
            $order_items->total_price = $newPrice;
            $order_items->save();

            return $order_id;
    }
}
