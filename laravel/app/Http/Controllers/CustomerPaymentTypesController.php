<?php

namespace App\Http\Controllers;

use DB;
use App\Sizes;
use App\Customers;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\DirectPaymentSources;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class CustomerPaymentTypesController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      |  CustomerPaymentTypesController
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $this->customer_id = $customer[0]->id;
    }

    public function index() {

        $payment_types = DirectPaymentSources::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();
        return view('front.customers.payment_type.index', compact('payment_types'));
    }

    public function create() {

        return view('front.customers.payment_type.create');
    }

    public function edit($id) {

        $payment_type = DirectPaymentSources::where('id', '=', $id)->where('owner_type', 'customer')->where('created_by', '=', $this->customer_id)->where('deleted', '0')->get();

        if (count($payment_type) == 0) {
            return redirect('/');
        }
        $model = $payment_type[0];
        return view('front.customers.payment_type.edit', compact('model'));
    }

    public function postCreate(Request $request) {


        $validation = array(
            'title' => 'required|max:40',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }

        $size = new DirectPaymentSources;
        $size->title = $request->title;
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $this->customer_id;
        $size->save();

        if (isset($size->id)) {
            Session::flash('success', 'Payment Type is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Payment Type is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $validation = array(
            'title' => 'required|max:40',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }

        $size_id = $request->id;
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');


        DirectPaymentSources::where('id', '=', $size_id)->update($input);
        Session::flash('success', 'Payment Type has been updated.');
        return redirect()->back();
    }

    public function delete($id) {


        DirectPaymentSources::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public static function createCustomerPaymentTypes($customer_id) {


        $size = new DirectPaymentSources;
        $size->title = 'CASH';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();
        
        $size = new DirectPaymentSources;
        $size->title = 'CHECK';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();
        
        $size = new DirectPaymentSources;
        $size->title = 'MONEY ORDER';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();
        
        $size = new DirectPaymentSources;
        $size->title = 'BANK DEPOSIT';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();
    }

}
