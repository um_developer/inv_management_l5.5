<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Invoices;
use App\Orders;
use App\Clients;
use App\OrderItems;
use App\PriceTemplates;
use App\InvoiceItems;
use App\Customers;
use App\ItemPriceTemplates;
use App\Packages;
use App\Bundles;
use App\ActivityLog;
use App\BundleItems;
use App\CustomerItemCost;
use App\CustomersItemImpact;
use App\PackageItems;
use App\Inventory;
use App\WarehouseInventory;
use App\CustomerInventory;
use App\ItemPriceImpect;
use Illuminate\Http\Request;
use Auth;
use App\PrintPageSetting;
use App\Payments;
use Session;
use Carbon\Carbon;
use App\PurchaseOrders;
use App\PoItems;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;
use App\Functions\Functions;

class InvoiceController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | PurchaseOrder Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

       

        $model = Invoices::where('invoices.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                ->whereBetween('invoices.statement_date', [$start_date, $end_date])
                ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                ->orderBy('created_at', 'desc')
                ->get();

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $selected_report_type = '';
        $customers = $new_cat;
        $date = $start_date . ' - ' . $end_date;
        $invoices0 = $model;
        $selected_invoice_number = '';

       
        return view('front.invoice.index', compact('invoices0', 'date', 'customers', 'selected_report_type', 'selected_invoice_number'));
    }

    public function search(Request $request) {

        $date = $request->date_range;
        if (isset($request->customer_id)) {
            $customer_id = $request->customer_id;
        }
        if (isset($request->type)) {
            $selected_report_type = $request->type;
        }
        if (isset($request->invoice_number)) {
            $selected_invoice_number = $request->invoice_number;
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        if (!empty($selected_invoice_number)) {
            $model = Invoices::where('invoices.deleted', '=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                    ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                    ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                    ->where('invoices.id', $selected_invoice_number)
                    ->orderBy('id', 'desc')
                    ->get();
        } else {
            $model = Invoices::where('invoices.deleted', '=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                    ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                    ->whereBetween('invoices.statement_date', [$start_date, $end_date]);

            if (!empty($selected_report_type)) {

                $model = $model->where('invoices.status', $selected_report_type);
            }
            if (!empty($customer_id)) {

                $model = $model->where('invoices.customer_id', $customer_id);
            }

            $model = $model->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                    ->orderBy('id', 'desc')
                    ->get();
        }

        $customers = $new_cat;
        $invoices0 = $model;
        return view('front.invoice.index', compact('invoices0', 'date', 'customers', 'selected_report_type', 'selected_invoice_number'));
    }

    public function create($customer_id = '0') {

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $list_words = \App\Lists::where('deleted', '0')->lists('title', 'id')->toArray();
        $list_words[0] = 'Select List';
        ksort($list_words);
        $packages = Packages::select(['id'])->where('deleted', '=', '0')->get();
        // $bundles = Bundles::select(['id','name'])->where('deleted', '=', '0')->get();
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $address = '';
        $phone = '';
        $template_name = '';

        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $new_package = [];
        foreach ($packages as $package) {
            $new_package[0] = 'Select Package';
            $new_package[$package->id] = $package->id;
        }

        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            if(!empty( $getbundles->name)){
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
            }
        }
       
        if ($customer_id != '0') {
            $customer = Customers::where('id', $customer_id)
                    ->leftjoin('states as s', 's.code', '=', 'customers.state')
                    ->select('customers.*', 's.title as state')
                    ->first();
            $address = $customer->address1 . ' ' . $customer->address2 . ' ' . $customer->city . ' ' . $customer->state . ' ' . $customer->zip_code;
            $phone = $customer->phone;
            $customer_name = $customer->name;
            $template = PriceTemplates::where('id', $customer->selling_price_template)->first();
            $template_name = $template->title;
            $items = CommonController::getItemFormsData($customer->selling_price_template);
        } else {
            $items = CommonController::getItemFormsData('1');
        }

        $customers = $new_cat;
        $packages = $new_package;
        $bundles = $new_bundle;


        return view('front.invoice.create', compact('customers', 'items', 'bundles', 'packages', 'customer_id', 'customer_name', 'address', 'template_name', 'phone', 'list_words'));
    }

    public function edit($id) {

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $address = '';
        $phone = '';
        $template_name = '';

        $packages = Packages::select(['id'])->where('deleted', '=', '0')->get();
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $list_words = \App\Lists::where('deleted', '0')->lists('title', 'id')->toArray();
        $list_words[0] = 'Select List';
        ksort($list_words);

        $new_package = [];
        foreach ($packages as $package) {
            $new_package[0] = 'Select Package';
            $new_package[$package->id] = $package->id;
        }

        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            if(!empty( $getbundles->name)){
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
            }
        }

        $model = Invoices::where('invoices.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->select('invoices.*', 'c.id as customer_id')
                ->get();

        $new_cat = [];
        if (empty($model[0]->customer_id))
            $new_cat[0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        // print_r($model[0]); die;
        $temp = Invoices::where('invoices.id', '=', $id)
                        ->leftjoin('orders as o', 'o.id', '=', 'invoices.order_id')->first();
        $is_sync = 0;
        if ($temp['invoice_refrence_id'] == $id) {
            $is_sync = 1;
        } else {
            $is_sync = 0;
        }
        // dd($model);
        if (empty($model)) {
            $items = CommonController::getItemFormsData('1');
        } else {
            $customer = Customers::where('id', '=', $model['0']->customer_id)
                    ->leftjoin('states as s', 's.code', '=', 'customers.state')
                    ->select('customers.*', 's.title as state')
                    ->first();
            if (count($customer) > 0) {
                $address = $customer->address1 . ' ' . $customer->address2 . ' ' . $customer->city . ' ' . $customer->state . ' ' . $customer->zip_code;
                $template = PriceTemplates::where('id', $customer->selling_price_template)->first();
                $template_name = $template->title;
                $phone = $customer->phone;
                $items = CommonController::getItemFormsData($customer->selling_price_template);
            } else {
                $items = CommonController::getItemFormsData('1');
            }
        }

        $po_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'invoice_items.bundle_id')
                ->where('invoice_items.deleted', '=', '0')
               ->select('invoice_items.*', 'i.name as item_name', 'i.cost as cost', 'i.image as image' , 'b.name as bundle_name')
                ->get();

        $packages = $new_package;
        $bundles = $new_bundle;

        $vendors = $new_cat;
        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.invoice.edit', compact('items', 'vendors', 'model', 'bundles', 'po_items', 'packages', 'address', 'template_name', 'phone', 'is_sync', 'list_words'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $validation = array(
//            'name' => 'required|max:20',
//            'sku' => 'max:8|unique:items',
            'customer_id' => 'required|not_in:0',
        );

        $messages = array(
            'customer_id.not_in' => 'Please select any customer',
        );

//        if($request->customer_id)

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        $items = $input['num'];
        $invoice = new Invoices;
        $invoice->customer_id = $request->customer_id;
        $invoice->memo = $request->memo;
        $invoice->created_by = 'admin';
        $invoice->deleted = '1';
        $invoice->list_id = $request->list_id;
        $timestamp = strtotime($request->statement_date);
        $invoice->statement_date = date("Y-m-d H:i:s", $timestamp);
        $invoice->save();


//        $new_id = '11' . $invoice->id;
//        Invoices::where('id', '=', $invoice->id)->update(['id' => $new_id]);
//        $invoice->id = $new_id;

        $total_invoice_price = 0;
        $total_quantity = 0;
        if (count($items) > 0 && isset($invoice->id)) {

//            if ($input['item_id_1'] != '' || $input['item_id_2'] != '') {

            for ($i = 1; $i <= count($items); $i++) {
                if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];

                    $invoice_items = new InvoiceItems;
                    $invoice_items->invoice_id = $invoice->id;
                    $invoice_items->customer_id = $input['customer_id'];
                    $invoice_items->item_id = $input['item_id_' . $i];
                    $invoice_items->item_unit_price = $input['price_' . $i];
                    $invoice_items->quantity = $input['quantity_' . $i];
                    $invoice_items->statement_date = date("Y-m-d H:i:s", $timestamp);

                    if (isset($input['item_package_id_' . $i]))
                        $invoice_items->package_id = $input['item_package_id_' . $i];
                    if (isset($input['item_bundle_id_' . $i])) {
                        $invoice_items->bundle_id = $input['item_bundle_id_' . $i];
                        $invoice_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }

                    if ($input['item_type_' . $i] == 'serial') {

                        $invoice_items->start_serial_number = $input['start_serial_number_' . $i];
                        $invoice_items->end_serial_number = $input['end_serial_number_' . $i];
                        $invoice_items->is_serial = 1;
                    }

                    $invoice_items->total_price = $item_total_price;
//                    $invoice_items->created_by = $user_id;
                    $total_invoice_price += $invoice_items->total_price;
                    $total_quantity += $invoice_items->quantity;
                    $invoice_items->save();
                }
            }
            if ($total_quantity == 0) {
                DB::rollBack();
                Session::flash('error', 'Item count is greater than 0.');
                return redirect()->back();
            }

            Invoices::where('id', '=', $invoice->id)->update([
                'deleted' => 0,
                'total_price' => $total_invoice_price,
                'total_quantity' => $total_quantity,
            ]);

            self::updateProfit($invoice->id);
        } else {
            DB::rollBack();
            Session::flash('error', 'Invoice is not created. Please add atleast 1 item.');
            return redirect()->back();
        }
//        } else {
//            DB::rollBack();
//            Session::flash('error', 'Invoice is not created. Please try again.');
//            return redirect()->back();
//        }

        DB::commit();
        Session::flash('success', 'Invoice is created successfully');
        return redirect()->back();
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
        // dd($input);
        $items = $input['num'];
        // dd($items);
        $user_id = Auth::user()->id;



        DB::beginTransaction();

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d", $timestamp);

        if (isset($input['customer_id'])) {

            Invoices::where('id', '=', $input['id'])->update([
                'customer_id' => $input['customer_id'],
                'memo' => $input['memo'],
                'statement_date' => $statement_date,
                'list_id' => $request->list_id,
            ]);
        } else {

            Invoices::where('id', '=', $input['id'])->update([
                'memo' => $input['memo'],
                'statement_date' => $statement_date,
                'list_id' => $request->list_id,
            ]);
        }



        $invoice = Invoices::where('id', '=', $input['id'])->first();

        $invoice_total_price = 0;
        $total_quantity = 0;
        $is_approved = 0;
        $old_total_price = 0;

        if ($invoice->status == 'approved') {
            $is_approved = 1;
            $old_total_price = $invoice->total_price;
        }

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if po_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $po_item = InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->get();
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {

                        InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update([
                            'deleted' => 1,
                        ]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {
                            $end_serial_num = 0;
                            $serial_quantity = 0;
                            ///////// end serial num or quantity /////
                            if ($input['end_serial_number_' . $i] > 0) {
                                $end_serial_num = $input['end_serial_number_' . $i];
                                $serial_quantity = ($input['end_serial_number_' . $i] - $input['start_serial_number_' . $i]) + 1;
                            } else {
                                $serial_quantity = $input['quantity_' . $i];
                                $end_serial_num = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            }

                            if ($total_quantity == 0) {
                                DB::rollBack();
                                Session::flash('error', 'Item count is greater than 00.');
                                return redirect()->back();
                            }

                            InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $serial_quantity,
                                'total_price' => $item_total_price,
                                'start_serial_number' => $input['start_serial_number_' . $i],
                                'end_serial_number' => $end_serial_num,
                                'statement_date' => $statement_date,
                                'deleted' => 0,
                            ]);
                        } else {
                            InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                                'deleted' => 0,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_items = new InvoiceItems;
                    $invoice_items->invoice_id = $input['id'];
                    $invoice_items->customer_id = $invoice->customer_id;
                    $invoice_items->item_id = $input['item_id_' . $i];
                    $invoice_items->item_unit_price = $input['price_' . $i];
                    $invoice_items->total_price = $item_total_price;
                    $invoice_items->statement_date = $statement_date;
                    $invoice_items->quantity = $input['quantity_' . $i];

                    if (isset($input['item_package_id_' . $i]))
                        $invoice_items->package_id = $input['item_package_id_' . $i];
                    if (isset($input['item_bundle_id_' . $i])) {
                        $invoice_items->bundle_id = $input['item_bundle_id_' . $i];
                        $invoice_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }

                    if ($input['start_serial_number_' . $i] > 0) {
                        $invoice_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['item_type_' . $i] == 'serial') {
                            $invoice_items->end_serial_number = $input['end_serial_number_' . $i];
                            $invoice_items->quantity = ($input['end_serial_number_' . $i] - $input['start_serial_number_' . $i]) + 1;
                            ;
                        } else {
                            $invoice_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $invoice_items->quantity = $input['quantity_' . $i];
                        }
                    }

                    $invoice_items->quantity = $invoice_items->quantity;
//                    $invoice_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $invoice_items->quantity;
                    $invoice_items->save();
                }
            }

            if ($invoice_total_price > 0) {
                Invoices::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Invoice is not updated. Please add atleast 1 proper item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Invoice is not updated. Please try again.');
            return redirect()->back();
        }

        self::updateProfit($invoice->id);

        DB::commit();
        Session::flash('success', 'Invoice is updated successfully');
        return redirect()->back();
    }

    public function postModified(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $user_id = Auth::user()->id;

        $invoice = Invoices::where('id', '=', $input['id'])->first();
        DB::beginTransaction();

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d", $timestamp);

        Invoices::where('id', '=', $input['id'])->update([
            'memo' => $input['memo'],
            'statement_date' => $statement_date,
            'list_id' => $request->list_id,
        ]);

        $invoice_total_price = 0;
        $total_quantity = 0;
        $is_approved = 0;
        $old_total_price = 0;
        $customer_id = $invoice->customer_id;

        if ($invoice->status != 'pending') {
            $is_approved = 1;
            $old_total_price = $invoice->total_price;
        }

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if po_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $po_item = InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->first();
                    $old_quantity = $po_item->quantity;
                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];

                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {

                        InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update([
                            'deleted' => 1,
                        ]);

                        self::updateAdminInventory($po_item->item_id, $po_item->quantity, 'add');
                        CustomerInventory::updateInventory($po_item->item_id, $po_item->quantity, 'subtract', $customer_id);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {
                            //// no serial functionality
                        } else {

                            if ($old_quantity > $input['quantity_' . $i]) {
                                $new_quantity = $old_quantity - $input['quantity_' . $i];
                                self::updateAdminInventory($po_item->item_id, $new_quantity, 'add');
                                CustomerInventory::updateInventory($po_item->item_id, $po_item->quantity, 'subtract', $customer_id);
                            } elseif ($old_quantity < $input['quantity_' . $i]) {
                                $new_quantity = $input['quantity_' . $i] - $old_quantity;
                                self::updateAdminInventory($po_item->item_id, $new_quantity, 'subtract');
                                CustomerInventory::updateInventory($po_item->item_id, $new_quantity, 'add', $customer_id);
                            } else {

                                /////do nothing
                            }


                            InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                                'deleted' => 0,
                            ]);
                            self::updateCustomerCost($input['item_id_' . $i], $customer_id, $input['price_' . $i], 'invoice', $invoice->id);
                        }
                    }
                }
                ///////// if new item ////
                elseif (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_items = new InvoiceItems;
                    $invoice_items->invoice_id = $input['id'];
                    $invoice_items->customer_id = $invoice->customer_id;
                    $invoice_items->item_id = $input['item_id_' . $i];
                    $invoice_items->item_unit_price = $input['price_' . $i];
                    $invoice_items->total_price = $item_total_price;
                    $invoice_items->statement_date = $statement_date;
                    $invoice_items->quantity = $input['quantity_' . $i];

                    if (isset($input['item_package_id_' . $i]))
                        $invoice_items->package_id = $input['item_package_id_' . $i];

                    if ($input['start_serial_number_' . $i] > 0) {
                        $invoice_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['item_type_' . $i] == 'serial') {
                            $invoice_items->end_serial_number = $input['end_serial_number_' . $i];
                            $invoice_items->quantity = ($input['end_serial_number_' . $i] - $input['start_serial_number_' . $i]) + 1;
                            ;
                        } else {
                            $invoice_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $invoice_items->quantity = $input['quantity_' . $i];
                        }
                    }

                    self::updateAdminInventory($input['item_id_' . $i], $invoice_items->quantity, 'subtract');
                    CustomerInventory::updateInventory($input['item_id_' . $i], $invoice_items->quantity, 'add', $customer_id);

                    $invoice_items->quantity = $invoice_items->quantity;
//                    $invoice_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $invoice_items->quantity;
                    $invoice_items->save();
                }
            }

            Invoices::where('id', '=', $input['id'])->update([
                'deleted' => 0,
                'total_price' => $invoice_total_price,
                'total_quantity' => $total_quantity,
            ]);

            $price_diff = 0;
            if ($old_total_price > $invoice_total_price) {

                $price_diff = $old_total_price - $invoice_total_price;
                PaymentController::updateCustomerBalance($customer_id, $price_diff, $invoice->id, 'invoice_neg', $statement_date);
            } elseif ($old_total_price < $invoice_total_price) {
                $price_diff = $invoice_total_price - $old_total_price;
                PaymentController::updateCustomerBalance($customer_id, $price_diff, $invoice->id, 'invoice', $statement_date);
            } else {
                Payments::where('payment_type', 'invoice')->where('payment_type_id', $invoice->id)->update(['statement_date' => $statement_date]);
            }

            if ($invoice->order_id != '0')
                CommonController::syncItems('invoice', $invoice->id);

            self::updateProfit($invoice->id);
        } else {
            DB::rollBack();
            Session::flash('error', 'Invoice is not Modified. Please try again.');
            return redirect()->back();
        }


        DB::commit();
        Session::flash('success', 'Invoice is Modified successfully');
        return redirect()->back();
    }

    public function delete($id) {

        $model = Invoices::where('invoices.id', '=', $id)
                ->where('invoices.status', '!=', 'pending')
//                  ->orWhere('purchase_orders.deleted', '!=', '0')
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }

        Invoices::where('id', '=', $id)->where('status', '=', 'pending')->update(['deleted' => 1]);
        InvoiceItems::where('invoice_id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function changeStatus($id, $status) {

        $errors = [];
        $warnings = [];
        $i = 0;
        $model = Invoices::where('invoices.id', '=', $id)
                ->where('invoices.status', '=', 'pending')
                ->get();
        if (count($model) == 0) {
            return redirect('/');
        }

        $total_price = $model[0]->total_price;
        $customer_id = $model[0]->customer_id;

        if ($customer_id == '' || $customer_id == '0') {

            $errors[$i]['type'] = 'normal';
            $errors[$i]['message'] = 'Please select customer.';
        }

        $invoice_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->where('invoice_items.deleted', '=', '0')
                ->select('invoice_items.*', 'i.name as item_name')
                ->get();

        DB::beginTransaction();
        if (count($invoice_items) > 0) {

            foreach ($invoice_items as $items) {
                if ($items->start_serial_number == '0') {

                    if ($items->package_id != '0') {

                        $package = Packages::where('id', $items->package_id)->where('type', '!=', 'invoice')->first();
                        if (count($package) == 0) {
                            $errors[$i]['type'] = 'normal';
                            $errors[$i]['message'] = 'Package# ' . $items->package_id . ' is not present or may be deleted.';
                        }

                        if ($package->warehouse_id != '0') {
                            /////package present in warehouse inventory
                            WarehouseInventory::updateInventory($package->warehouse_id, $items->item_id, $items->quantity, 'subtract');
                        } else {
                            /////package present in admin inventory
                            $a = Inventory::where('item_id', $items->item_id)->get();

                            if (count($a) == 0 || $a[0]->quantity < $items->quantity) {
                                $errors[$i]['type'] = 'normal';
                                $errors[$i]['message'] = $items->item_name . ' quantity is not present in admin inventory. Kindly update inventory';
                            }
                            self::updateAdminInventory($items->item_id, $items->quantity, 'subtract');
                        }

                        CustomerInventory::updateInventory($items->item_id, $items->quantity, 'add', $customer_id);
                        Packages::where('id', $items->package_id)->update(['status' => 'invoice', 'invoice_id' => $id]);
                    } else {

                        self::updateAdminInventory($items->item_id, $items->quantity, 'subtract');
                        CustomerInventory::updateInventory($items->item_id, $items->quantity, 'add', $customer_id);

                        $a = Inventory::where('item_id', $items->item_id)->get();
                        if (count($a) == 0 || $a[0]->quantity < $items->quantity) {

                            $package = PackageItems::where('item_id', $items->item_id)
                                    ->leftjoin('packages as p', 'p.id', '=', 'package_items.package_id')
                                    ->where('p.status', 'unassigned')
                                    ->where('p.deleted', 0)->where('p.warehouse_id', 0)->where('p.invoice_id', 0)
                                    ->where('package_items.quantity', '>=', $items->quantity)
                                    ->get();

                            if (count($package) > 0) {
                                $warnings[$i]['message'] = $items->item_name . ' has now negative quantity. Kindly Update inventory.';
                                $warnings[$i]['type'] = 'package';
                                $warnings[$i]['package_id'] = $package[0]->id;
                            } else {
                                $warnings[$i]['type'] = 'normal';
                                $warnings[$i]['message'] = $items->item_name . ' has now negative quantity. Kindly Update inventory.';
                            }
                        }
                    }
//               echo 'aaz'; die;
                } else {
                    /////// approve serial Items /////
                    $a = CommonController::serialItemValidate($items->item_id, $items->start_serial_number, $items->quantity);

                    if ($a == 0) {
                        $errors[$i]['type'] = 'normal';
                        $errors[$i]['message'] = $items->item_name . ' quantity is not present in admin inventory. Kindly update serial inventory';
                    } else {
                        self::updateAdminSerialInventory($items->item_id, $items->quantity, 'subtract', $customer_id, $items->start_serial_number, $id);
                        // Functions::makeSerialCurlRequest($items->start_serial_number,$items->start_serial_number+$items->quantity,'active');
                    }
                }
                $i++;
            }
        }


        if (count($errors) > 0) {
            Session::flash('bulk_error', $errors);
            return redirect()->back();
        }

        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        foreach ($invoice_items as $invoice_item) {

            $item = Items::where('id', $invoice_item->item_id)->first();

            InvoiceItems::where('id', $invoice_item->id)->update([
                'item_unit_price' => $invoice_item->item_unit_price,
                'total_price' => $invoice_item->item_unit_price * $invoice_item->quantity,
                'item_unit_cost' => $item->cost,
                'total_cost' => $item->cost * $invoice_item->quantity,
                'profit' => ($invoice_item->item_unit_price * $invoice_item->quantity) - ($item->cost * $invoice_item->quantity)
            ]);

            self::updateCustomerCost($invoice_item->item_id, $customer_id, $invoice_item->item_unit_price, 'invoice', $id);

            $total_price = $total_price + ($invoice_item->item_unit_price * $invoice_item->quantity);
            $total_cost = $total_cost + ($item->cost * $invoice_item->quantity);
            $total_profit = $total_price - $total_cost;
        }

        Invoices::where('id', '=', $id)->where('status', '=', 'pending')->update(['status' => $status, 'total_price' => $total_price, 'total_cost' => $total_cost, 'profit' => $total_profit]);
        PaymentController::updateCustomerBalance($customer_id, $total_price, $id, 'invoice', $model[0]->statement_date);
        DB::commit();
        if (count($warnings) > 0) {
            Session::flash('bulk_warnings', $warnings);
        }

        Session::flash('success', 'Successfully Approved!');
        return redirect()->back();
    }

    public function detail($id, $show_profit = 0) {

        //self::updateProfit($id);

        $model = Invoices::where('invoices.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->where('invoices.deleted', '!=', '1')
                ->select('invoices.*', 'c.id as customer_id', 'c.name as customer_name', 'c.show_image', 'c.show_serial','c.print_type' ,'show_package_id', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'c.phone', 'c.address1', 'o.id as order_refrence')
                ->get();

        $po_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'invoice_items.bundle_id')
                ->where('invoice_items.deleted', '=', '0')
                ->select('invoice_items.*', 'i.name as item_name', 'i.image as image', 'b.name as bundle_name')
                ->get();

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        
        return view('front.invoice.detail', compact('model', 'po_items', 'show_profit'));
    }
    public function printType($status,$customer_id) {
        $customers = Customers::where('id', '=',$customer_id)->update([
            'print_type' =>$status,
        ]);
       
    }

    public function printPage($id) {

        $model = Invoices::where('invoices.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('states as s', 's.code', '=', 'c.state')
                ->leftjoin('lists as i', 'i.id', '=', 'invoices.list_id')
                ->where('invoices.deleted', '!=', '1')
                ->select('invoices.*', 's.title as state', 'c.id as customer_id', 'c.name as customer_name', 'c.show_image', 'c.print_type','c.balance', 'c.show_serial', 'show_package_id', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'c.phone', 'c.address1', 'i.title as list_title')
                ->get();
                $po_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                 ->leftjoin('bundles as b', 'b.id', '=', 'invoice_items.bundle_id')
                ->where('invoice_items.deleted', '=', '0')
                ->select('invoice_items.*', 'i.name as item_name', 'i.image as image', 'b.name as bundle_name')
                ->get();      
              
        if($model[0]->print_type!="Normal"){
          
        $newItemArray=[];
		$old_bundle_id = '';
		foreach($po_items as $item){
/////agr item bundle wala na ho tu
		if($item->bundle_id =='' || $item->bundle_id =='0'){
		
		$newItemArray[] = $item;
		
		}else{
		/////agr item bundle ka 1st item ho tu
		if($item->bundle_id != $old_bundle_id){
		
		$newItemArray[] = $item;
		$old_bundle_id = $item->bundle_id;
		
		
		}else{
		/////agr existing bundle ka next item ho tu
		$previous_bundle_item = $newItemArray[(count($newItemArray)-1)];
		
		$item->item_name=$item->bundle_name;
        $item->quantity = $item->quantity + $previous_bundle_item->quantity;
        $item->item_unit_cost = $item->item_unit_cost + $previous_bundle_item->item_unit_cost;
        $item->total_price = $item->total_price + $previous_bundle_item->total_price;
	    $item->total_cost = $item->total_cost + $previous_bundle_item->total_cost;
		$item->profit = $item->profit + $previous_bundle_item->profit;
         $item->item_unit_price = $item->item_unit_price + $previous_bundle_item->item_unit_price;
         $item->is_bundle =1;
		// $item->quantity = $item->quantity + $previous_bundle_item->quantity;
		
		$newItemArray[(count($newItemArray)-1)] = $item;
		
		
		}
		
		}
		
        }
        $po_items=$newItemArray;
       }
     
        $setting = PrintPageSetting::where('user_type', '=', 'admin')->first();

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
       return view('front.invoice.print', compact('model', 'po_items', 'setting'));
    }

    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }

    public static function updateAdminInventory($item_id, $quantity, $type = 'add') {
        $item = Inventory::where('item_id', $item_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->save();
            }
        } else {

            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity - $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = 0 - $quantity;
                $Inventory->save();
            }
        }
    }

    function updateAdminSerialInventory($item_id, $quantity, $type = 'add', $customer_id, $start_serial, $location_id) {
        // $items = ItemSerials::where('item_id', $item_id)->where('serial', $start_serial)->where('type', 'inventory')->get();
        if ($type == 'add') {
//            ItemSerials::whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->update([
//                'user_id' => '0',
//                'type' => 'inventory',
//                'status' => 'inactive',
//            ]);
        } else {
            ////mark serial active
            ItemSerials::active($start_serial, $quantity);
            ItemSerials::whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->update([
                'customer_id' => $customer_id,
                'type' => 'sold',
                'status' => 'active',
                'location' => 'invoice',
                'location_id' => $location_id,
            ]);
        }
    }

    
    public function bulkCreate($package_id, $j) {

        if ($package_id == 'Select Package')
            return 0;

        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $packages = Packages::select(['id'])->where('deleted', '=', '0')->get();

        $new_cat = [];
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $new_package = [];
        foreach ($packages as $package) {
            $new_package[0] = 'Select Package';
            $new_package[$package->id] = $package->id;
        }

        $packages = $new_package;

        $model = Packages::where('id', '=', $package_id)->get();

        $po_items1 = PackageItems::where('package_id', '=', $package_id)
                ->leftjoin('items as i', 'i.id', '=', 'package_items.item_id')
//                ->leftjoin('item_price_templates as ipt', 'ipt.price_template_id', '=', '1')
                ->select('package_items.*', 'i.name as item_name', 'i.cost as cost', 'i.sale_price', 'i.id as item_id')
                ->get();

        $po_items = [];
        $i = 0;
        foreach ($po_items1 as $item) {

            $item_template = ItemPriceTemplates::where('price_template_id', '1')->where('item_id', $item->item_id)->first();

            $item->invoice_id = 0;
            $item->item_id = $item->item_id;
            $item->item_unit_price = $item_template->selling_price;
            $item->quantity = $item->quantity;
            $item->customer_id = 0;
            $item->total_price = $item_template->selling_price * $item->quantity;
            $item->item_name = $item->item_name;
            $item->cost = $item->cost;
            $item->upc_barcode = $item->upc_barcode;
            $item->sku = $item->code;
            $po_items[$i] = $item;
            $i++;
        }

        $vendors = $new_cat;
        if (count($model) == 0) {
            return redirect('/');
        }

        return view('front.common.package_view_table', compact('po_items', 'package_id', 'j'));
        return $po_items;
    }

    public function bulkAddItemInInvoice($package_id, $j) {

        if ($package_id == 'Select Package')
            return 0;

        $po_items1 = PackageItems::where('package_id', '=', $package_id)
                ->leftjoin('items as i', 'i.id', '=', 'package_items.item_id')
                ->select('package_items.*', 'i.name as item_name', 'i.cost as cost', 'i.sale_price', 'i.id as item_id')
                ->get();

        $po_items = [];
        $i = 0;
        foreach ($po_items1 as $item) {

            $item_template = ItemPriceTemplates::where('price_template_id', '1')->where('item_id', $item->item_id)->first();
            $item->invoice_id = 0;
            $item->item_id = $item->item_id;
            $item->item_unit_price = $item_template->selling_price;
            $item->quantity = $item->quantity;
            $item->customer_id = 0;
            $item->total_price = $item_template->selling_price * $item->quantity;
            $item->item_name = $item->item_name;
            $item->cost = $item->cost;
            $item->upc_barcode = $item->upc_barcode;
            $item->sku = $item->code;
            $po_items[$i] = $item;
            $i++;
        }
//      $result['j'] = count($po_items1);
//      session(['invoice_'+$package_id => count($po_items1)]);
        return view('front.common.package_insert_table', compact('po_items', 'package_id', 'j'));
    }

    

    public static function getCustomerItemCost($item_id) {

        $items = CommonController::getItemIds($item_id);
        $res = PoItems::where('po.deleted', '0')->leftjoin('purchase_orders as po', 'po_items.po_id', '=', 'po.id');
        if (count($items) > 0)
            $res = $res->whereIn('item_id', $items);
        else
            $res = $res->where('item_id', $item_id);

        $res = $res->orderBy('po_items.id', 'desc')->first();

        if (empty($res)){
           return '';
        }
        return $res;
    }

    public static function updateProfit($invoice_id) {

        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        
        $invoice_items = InvoiceItems::where('invoice_id', $invoice_id)->where('deleted', '0')->get();
        foreach ($invoice_items as $invoice_item) {

            $purchase_order_id = 0;
            $item_cost = 0;
            $purchase_order_num = 0;
            $item_cost = self::getCustomerItemCost($invoice_item->item_id);

            if ($item_cost == '')
                $item_cost = $invoice_item->item_unit_price;
            else {
                $purchase_order_id = $item_cost->po_id;
                $item_cost = $item_cost->item_unit_price;
            }


            InvoiceItems::where('id', $invoice_item->id)->update([
                'item_unit_price' => $invoice_item->item_unit_price,
                'total_price' => $invoice_item->item_unit_price * $invoice_item->quantity,
                'item_unit_cost' => $item_cost,
                'purchase_order_id' => $purchase_order_id,
                'total_cost' => $item_cost * $invoice_item->quantity,
                'profit' => ($invoice_item->item_unit_price * $invoice_item->quantity) - ($item_cost * $invoice_item->quantity)
            ]);

            $total_price = $total_price + ($invoice_item->item_unit_price * $invoice_item->quantity);
            $total_cost = $total_cost + ($item_cost * $invoice_item->quantity);
            $total_profit = $total_price - $total_cost;
        }
        Invoices::where('id', '=', $invoice_id)->update(['total_price' => $total_price, 'total_cost' => $total_cost, 'profit' => $total_profit]);
    }

    public function getItemPrice($item_id, $customer_id) {

        $items = Items::where('deleted', '0')->where('parent_item_id', $item_id)->lists('id')->toArray();
        $res = Invoices::where('invoices.status', 'approved')->where('invoices.customer_id', $customer_id)
                ->leftjoin('invoice_items as it', 'it.invoice_id', '=', 'invoices.id');

        if (count($items) > 0)
            $res = $res->whereIn('it.item_id', $items);
        else
            $res = $res->where('it.item_id', $item_id);

        $res = $res->where('it.deleted', '0')->where('it.customer_id', $customer_id)
                        ->select('it.item_unit_price', 'it.item_unit_cost')
                        ->orderBy('it.id', 'DESC')
                        ->get()->take(3)->toArray();


        if (count($res) == 0)
            return '0';

        $model['sale_price'] = $res;
        $model['cost'] = $res[0]['item_unit_cost'];

        return $model;
    }

    public function getItemPriceHtml($item_id, $customer_id) {

        $items = Items::where('deleted', '0')->where('parent_item_id', $item_id)->lists('id')->toArray();
        $res = Invoices::where('invoices.status', 'approved')->where('invoices.customer_id', $customer_id)
                ->leftjoin('invoice_items as it', 'it.invoice_id', '=', 'invoices.id');

        if (count($items) > 0)
            $res = $res->whereIn('it.item_id', $items);
        else
            $res = $res->where('it.item_id', $item_id);

        $res = $res->where('it.deleted', '0')->where('it.customer_id', $customer_id)
                        ->select('it.item_unit_price', 'it.item_unit_cost')
                        ->orderBy('it.id', 'DESC')
                        ->get()->take(3)->toArray();


        $data = view('front.common.ajax_sale_price', compact('res'))->render();

        return $data;
    }

    public function calculateCustomerCost() {

        $all_customers = Customers::get();

        foreach ($all_customers as $customer) {

            $template_id = $customer->selling_price_template;
            $customer_id = $customer->id;
            $PriceTemplatesItems = ItemPriceTemplates::where('price_template_id', $template_id)->get();

            if (count($PriceTemplatesItems) > 0) {

                foreach ($PriceTemplatesItems as $items) {

                    $item_id = $items->item_id;
                    $cost = $items->selling_price;

                    self::updateCustomerCost($item_id, $customer_id, $cost, 'price_template', $template_id);
                }
            }
        }
    }

    private function updateCustomerCost($item_id, $customer_id, $cost, $type, $type_id) {

        $new_item = Items::where('id', $item_id)->first();

        $items = Items::where('deleted', '0')->where('id', $new_item->id)->lists('id')->toArray();

        if ($new_item->has_sub_item == '1') {
            $items = Items::where('deleted', '0')->where('parent_item_id', $item_id)->lists('id')->toArray();
            array_push($items, $item_id);
        }

        if ($new_item->parent_item_id != '0') {
            $items = Items::where('deleted', '0')->where('parent_item_id', $new_item->parent_item_id)->lists('id')->toArray();
            array_push($items, $new_item->parent_item_id);
        }

        foreach ($items as $item_id) {
            $rec = CustomerItemCost::where('item_id', $item_id)->where('customer_id', $customer_id)->first();

            if (count($rec) > 0) {
                CustomerItemCost::where('item_id', $item_id)->where('customer_id', $customer_id)->update(['cost' => $cost]);
            } else {

                $customerCost = new CustomerItemCost;
                $customerCost->customer_id = $customer_id;
                $customerCost->item_id = $item_id;
                $customerCost->cost = $cost;
                $customerCost->updated_from_type = $type;
                $customerCost->updated_from = $type_id;
                $customerCost->save();
            }
        }
    }

    public function addItemToInvoice($item_id, $quantity, $price) {
        if (strpos($item_id, ',') !== false) {
            $item_array = explode(",", $item_id);
            $quantity_array = explode(",", $quantity);
            $i = 0;
            foreach ($item_array as $item) {
                $res = self::innerAddItemToInvoice($item_array[$i], $quantity_array[$i], $price);
                $url = url('invoice/edit' . '/' . $res);
                $html = "<a href='" . $url . "'>" . $res . "</a>";
                $message = 'All items are successfully added in invoice# ' . $html;
                $i++;
            }
        } else {

            $res = self::innerAddItemToInvoice($item_id, $quantity, $price);
            $item = Items::where('id', $item_id)->first();
            $url = url('invoice/edit' . '/' . $res);
            $html = "<a href='" . $url . "'>" . $res . "</a>";
            $message = $item->name . ' is successfully added in invoice# ' . $html;
        }

        return $message;
    }

    private function innerAddItemToInvoice($item_id, $quantity, $price) {

        $customer_id = Auth::user()->id;
        $model = Invoices::where('invoices.deleted', '=', '0')
                ->where('customer_id', '')
                ->where('status', 'pending')
                ->orderBy('id', 'desc')
                ->first();


        $item = Items::where('id', $item_id)->first();

        if (count($model) > 0) {
            $invoice_items = new InvoiceItems;
            $invoice_items->invoice_id = $model->id;
            $invoice_items->customer_id = $customer_id;
            $invoice_items->item_id = $item_id;
            $invoice_items->item_unit_price = $price;
            $invoice_items->total_price = $quantity * $price;
            $invoice_items->quantity = $quantity;
            $invoice_items->statement_date = date("Y-m-d");
            $invoice_items->save();

            if (!isset($invoice_items->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect()->back();
            }

            Invoices::where('id', '=', $model->id)->update([
                'total_price' => $model->total_price + $price,
                'total_quantity' => $model->total_quantity + $quantity,
            ]);
            $message = $model->id;
        } else {
            DB::beginTransaction();

            $invoice = new Invoices;
            $invoice->created_by = 'admin';
            $invoice->customer_id = '';
            $invoice->total_price = $quantity * $price;
            $invoice->total_quantity = $quantity;
            $invoice->statement_date = date("Y-m-d");
            $invoice->deleted = '0';
            $invoice->save();

//            $new_id = '11' . $invoice->id;
//            Invoices::where('id', '=', $invoice->id)->update(['id' => $new_id]);
//            $invoice->id = $new_id;

            if (!isset($invoice->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect('customer/item-gallery');
            }

            $invoice_id = $invoice->id;

            $invoice_items = new InvoiceItems;
            $invoice_items->invoice_id = $invoice_id;
            $invoice_items->customer_id = '0';
            $invoice_items->item_id = $item_id;
            $invoice_items->item_unit_price = $price;
            $invoice_items->total_price = $quantity * $price;
            $invoice_items->quantity = $quantity;
            $invoice_items->save();
            DB::commit();

            if (!isset($invoice_items->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect('customer/item-gallery');
            }

            $message = $invoice_id;
        }

        return $message;
    }

    public static function approvedsyncInvoice($id, $status) {

        $errors = [];
        $warnings = [];
        $i = 0;
        $model = Invoices::where('invoices.id', '=', $id)
                ->where('invoices.status', '=', 'pending')
                ->get();
        if (count($model) == 0) {
            return redirect('/');
        }

        $total_price = $model[0]->total_price;
        $customer_id = $model[0]->customer_id;

        if ($customer_id == '' || $customer_id == '0') {

            $errors[$i]['type'] = 'normal';
            $errors[$i]['message'] = 'Please select customer.';
        }

        $invoice_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->where('invoice_items.deleted', '=', '0')
                ->select('invoice_items.*', 'i.name as item_name')
                ->get();

        DB::beginTransaction();
        if (count($invoice_items) > 0) {

            foreach ($invoice_items as $items) {
                if ($items->start_serial_number == '0') {

                    if ($items->package_id != '0') {

                        $package = Packages::where('id', $items->package_id)->where('type', '!=', 'invoice')->first();
                        if (count($package) == 0) {
                            $errors[$i]['type'] = 'normal';
                            $errors[$i]['message'] = 'Package# ' . $items->package_id . ' is not present or may be deleted.';
                        }

                        if ($package->warehouse_id != '0') {
                            /////package present in warehouse inventory
                            WarehouseInventory::updateInventory($package->warehouse_id, $items->item_id, $items->quantity, 'subtract');
                        } else {
                            /////package present in admin inventory
                            $a = Inventory::where('item_id', $items->item_id)->get();

                            if (count($a) == 0 || $a[0]->quantity < $items->quantity) {
                                $errors[$i]['type'] = 'normal';
                                $errors[$i]['message'] = $items->item_name . ' quantity is not present in admin inventory. Kindly update inventory';
                            }
                            self::updateAdminInventory($items->item_id, $items->quantity, 'subtract');
                        }

                        CustomerInventory::updateInventory($items->item_id, $items->quantity, 'add', $customer_id);
                        Packages::where('id', $items->package_id)->update(['status' => 'invoice', 'invoice_id' => $id]);
                    } else {

                        self::updateAdminInventorySync($items->item_id, $items->quantity, 'subtract');
                        CustomerInventory::updateInventory($items->item_id, $items->quantity, 'add', $customer_id);

                        $a = Inventory::where('item_id', $items->item_id)->get();
                        if (count($a) == 0 || $a[0]->quantity < $items->quantity) {

                            $package = PackageItems::where('item_id', $items->item_id)
                                    ->leftjoin('packages as p', 'p.id', '=', 'package_items.package_id')
                                    ->where('p.status', 'unassigned')
                                    ->where('p.deleted', 0)->where('p.warehouse_id', 0)->where('p.invoice_id', 0)
                                    ->where('package_items.quantity', '>=', $items->quantity)
                                    ->get();

                            if (count($package) > 0) {
                                $warnings[$i]['message'] = $items->item_name . ' has now negative quantity. Kindly Update inventory.';
                                $warnings[$i]['type'] = 'package';
                                $warnings[$i]['package_id'] = $package[0]->id;
                            } else {
                                $warnings[$i]['type'] = 'normal';
                                $warnings[$i]['message'] = $items->item_name . ' has now negative quantity. Kindly Update inventory.';
                            }
                        }
                    }
//               echo 'aaz'; die;
                } else {
                    /////// approve serial Items /////
                    $a = CommonController::serialItemValidate($items->item_id, $items->start_serial_number, $items->quantity);

                    if ($a == 0) {
                        $errors[$i]['type'] = 'normal';
                        $errors[$i]['message'] = $items->item_name . ' quantity is not present in admin inventory. Kindly update serial inventory';
                    } else {
                        self::updateAdminSerialInventory($items->item_id, $items->quantity, 'subtract', $customer_id, $items->start_serial_number, $id);
                        // Functions::makeSerialCurlRequest($items->start_serial_number,$items->start_serial_number+$items->quantity,'active');
                    }
                }
                $i++;
            }
        }


        if (count($errors) > 0) {
            Session::flash('bulk_error', $errors);
            return redirect()->back();
        }

        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        foreach ($invoice_items as $invoice_item) {

            $item = Items::where('id', $invoice_item->item_id)->first();

            InvoiceItems::where('id', $invoice_item->id)->update([
                'item_unit_price' => $invoice_item->item_unit_price,
                'total_price' => $invoice_item->item_unit_price * $invoice_item->quantity,
                'item_unit_cost' => $item->cost,
                'total_cost' => $item->cost * $invoice_item->quantity,
                'profit' => ($invoice_item->item_unit_price * $invoice_item->quantity) - ($item->cost * $invoice_item->quantity)
            ]);

            self::updateCustomerCostSync($invoice_item->item_id, $customer_id, $invoice_item->item_unit_price, 'invoice', $id);

            $total_price = $total_price + ($invoice_item->item_unit_price * $invoice_item->quantity);
            $total_cost = $total_cost + ($item->cost * $invoice_item->quantity);
            $total_profit = $total_price - $total_cost;
        }

        Invoices::where('id', '=', $id)->where('status', '=', 'pending')->update(['status' => $status, 'total_price' => $total_price, 'total_cost' => $total_cost, 'profit' => $total_profit]);
        PaymentController::updateCustomerBalance($customer_id, $total_price, $id, 'invoice', $model[0]->statement_date);
        DB::commit();
        if (count($warnings) > 0) {
            Session::flash('bulk_warnings', $warnings);
        }

        Session::flash('success', 'Successfully Approved!');
        return true;
    }

    public static function updateAdminInventorysync($item_id, $quantity, $type = 'add') {
        $item = Inventory::where('item_id', $item_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->save();
            }
        } else {

            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity - $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = 0 - $quantity;
                $Inventory->save();
            }
        }
    }

    public static function updateAdminSerialInventorysync($item_id, $quantity, $type = 'add', $customer_id, $start_serial, $location_id) {
        // $items = ItemSerials::where('item_id', $item_id)->where('serial', $start_serial)->where('type', 'inventory')->get();
        if ($type == 'add') {
//            ItemSerials::whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->update([
//                'user_id' => '0',
//                'type' => 'inventory',
//                'status' => 'inactive',
//            ]);
        } else {
            ////mark serial active
            ItemSerials::active($start_serial, $quantity);
            ItemSerials::whereBetween('serial', [$start_serial, $start_serial + ($quantity - 1)])->update([
                'customer_id' => $customer_id,
                'type' => 'sold',
                'status' => 'active',
                'location' => 'invoice',
                'location_id' => $location_id,
            ]);
        }
    }

    public static function updateCustomerCostSync($item_id, $customer_id, $cost, $type, $type_id) {

        $new_item = Items::where('id', $item_id)->first();

        $items = Items::where('deleted', '0')->where('id', $new_item->id)->lists('id')->toArray();

        if ($new_item->has_sub_item == '1') {
            $items = Items::where('deleted', '0')->where('parent_item_id', $item_id)->lists('id')->toArray();
            array_push($items, $item_id);
        }

        if ($new_item->parent_item_id != '0') {
            $items = Items::where('deleted', '0')->where('parent_item_id', $new_item->parent_item_id)->lists('id')->toArray();
            array_push($items, $new_item->parent_item_id);
        }

        foreach ($items as $item_id) {
            $rec = CustomerItemCost::where('item_id', $item_id)->where('customer_id', $customer_id)->first();

            if (count($rec) > 0) {
                CustomerItemCost::where('item_id', $item_id)->where('customer_id', $customer_id)->update(['cost' => $cost]);
            } else {

                $customerCost = new CustomerItemCost;
                $customerCost->customer_id = $customer_id;
                $customerCost->item_id = $item_id;
                $customerCost->cost = $cost;
                $customerCost->updated_from_type = $type;
                $customerCost->updated_from = $type_id;
                $customerCost->save();
            }
        }
    }

    public function printBulkPage($id) {

        $ids = explode(',', $id);

        $model = Invoices::whereIn('invoices.id', $ids)
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->where('invoices.deleted', '=', '0')
                ->select('invoices.id', 'c.name as customer_name', 'invoices.total_price', 'invoices.statement_date', 'invoices.memo', 'c.balance as balance', DB::raw('(select delivered_comments from comments where form_id = invoices.id  and form_type = "invoice" order by id desc limit 1) as delivered_comments'))
                ->get();

        if (count($model) == 0) {
            return redirect('/');
        }

        return view('front.invoice.bulk_print', compact('model'));
    }

    public function invoiceProcess($id) {

        $data = Invoices::where('id', $id)->first();
        Invoices::where('id', $id)->update(['status' => 'processing']);
        Orders::where('id', $data->order_id)->update(['status' => 'processing']);

        return redirect()->back();
    }

    public function zeroprofit() {

        $zero_profit_invoice_count = InvoiceItems::where('invoice_items.deleted', '0')
                ->where('invoice_items.customer_id', '!=', '0')
                ->where('invoice_items.profit', '=', '0.00')
                ->groupby('invoice_items.invoice_id')
                ->get();
        $ids = [];
        foreach ($zero_profit_invoice_count as $row) {
            $ids[] = $row->invoice_id;
        }

        $invoices0 = Invoices::where('invoices.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                ->whereIn('invoices.id', $ids)
                ->where('invoices.skip', 0)
                ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                ->orderBy('id', 'desc')
                ->get();

        return view('front.invoice.zero_profit_list', compact('invoices0'));
    }

    public function zeropurchaseorder() {
        $from_perchase_zero_profit_invoice_ids = InvoiceItems::where('invoice_items.deleted', '0')
                ->where('invoice_items.customer_id', '!=', '0')
                ->where('invoice_items.purchase_order_id', 0)
                ->whereDate('invoice_items.created_at', '>', '2020-09-05 16:22:54')
                ->groupby('invoice_items.invoice_id')
                ->get();

        $idss = [];
        foreach ($from_perchase_zero_profit_invoice_ids as $row) {
            $idss[] = $row->invoice_id;
        }

        $invoices0 = Invoices::where('invoices.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                ->whereIn('invoices.id', $idss)
                ->where('invoices.skip', 0)
                ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                ->orderBy('id', 'desc')
                ->get();

        foreach ($invoices0 as $invoice) {
            self::updateProfit($invoice->id);
        }

        return view('front.invoice.zero_profit_list', compact('invoices0'));
    }

    public function ItemsWithNoPo() {
        $model = InvoiceItems::where('invoice_items.deleted', '0')
                ->where('invoice_items.customer_id', '!=', '0')
                ->where('invoice_items.purchase_order_id', 0)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->whereDate('invoice_items.created_at', '>', '2020-09-05 16:22:54')
                ->where('i.status', '1')
                ->where('i.deleted', '0')
                ->groupby('invoice_items.item_id')
                ->select('i.*', 'c.name as category_name')
                ->get();


        return view('front.item.item_with_no_po', compact('model'));
    }

    public function invoiceskip($id) {
        $check = Invoices::where('id', '=', $id)->first();
        if (!empty($check)) {
            Invoices::where('id', '=', $id)->update([
                'skip' => 1,
            ]);
            Session::flash('success', 'Successfully skiped.');
            return redirect()->back();
        } else {
            Session::flash('error', 'Invoice not found.');
            return redirect()->back();
        }
    }

    public function recalculateAllInvoiceProfit() {


        $invoice_items = Invoices::where('deleted', '0')->get();
        foreach ($invoice_items as $item) {
            self::updateProfit($item->invoice_id);
        }

        echo 'done';
        die;
    }

    public function createPriceImpectInvoice($id, $tableID){
        DB::beginTransaction();        
        try{
            $customers = Customers::where('deleted', 0)->get();
            $activity = new ActivityLog;
            $activity->status = 'pending';
            $activity->type = 'single';
            $activity->customer_count = count($customers);
            $activity->save();
            $invoiceCount = 0;
            $p_i_item = ItemPriceImpect::where('status','pending')->where('id',$tableID)->first();
    
                foreach($customers as $customer){
                    $invoice_id = 0;
                    $impact = $p_i_item->impact; ////increase or descrease
                    $new_price = '';
                    $check = Items::where('id',$p_i_item->item_id)->first();
                    if($impact == 'positive'){
                        if($check->increase_effect == 1 && $customer->increase_effect == 1){
                            $type = 'invoice';
                            $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $customer->id, $type); ///get from function
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                                }
                            }
                        }
                    }
    
                    if($impact == 'negative'){
                        if($check->decrease_effect == 1 && $customer->decrease_effect == 1){
                            $type = 'invoice';
                            $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $customer->id, $type); ///get from function
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price - $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                }
                            }
                        }
                    }
    
                    if($new_price != ''){
                        if ($invoice_id == 0) {
                            $invoiceCount++;
                        }
                        $invoice_ret_id = $this->createInvoice($p_i_item->item_id,$invoice_id,$customer->id,$new_price,$activity->id); //seprate function to create invoice and invoice items and return invoice id
                        $invoice_id = 0;
                    }
                }////customer
                ActivityLog::where('id',$activity->id)->update(['invoices_count' => $invoiceCount]);    
                $this->makeDummyOrderSingle($activity->id,$tableID);
                ItemPriceImpect::where('id',$tableID)->update(['status' => 'completed','activity_log_id' => $activity->id]);
                ActivityLog::where('id',$activity->id)->update(['status' => 'completed']);
                DB::commit();
                Session::flash('success', 'Successfully created dummy invoice.');
                return redirect()->back();
            }catch(Exception $e) {
                DB::rollBack();
                Session::flash('error', $e->getMessage());
                return redirect()->back();
            }
    }

    public function makeDummyOrderSingle($activity_id, $tableID){
        $clients = Clients::where('deleted', 0)->get();
        ActivityLog::where('id',$activity_id)->update(['client_count' => count($clients)]);
        $ordersCount = 0;                
        $p_i_item = ItemPriceImpect::where('status','pending')->where('id',$tableID)->first();
    
                foreach($clients as $client){
                    $order_id = 0;

                    $impact = $p_i_item->impact; ////increase or descrease
                    $new_price = '';
                    $check = CustomersItemImpact::where('item_id',$p_i_item->item_id)->where('customer_id',$client->customer_id)->first();
                    if (!empty($check)) {
                        $check1 = Items::where('id',$p_i_item->item_id)->first();
                        if($impact == 'positive'){
                            if($check->increase_impact == 1 && $client->increase_effect == 1 && $check1->increase_effect == 1){
                                $type = 'order';
                                $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                                if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                    $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                    $new_price = $old_item_price + $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                    if(!empty($old_item_price)){
                                        $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                                    }else{
                                        $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                        $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                    }
        
                                }
                            }
                        }
        
                        if($impact == 'negative'){
                            if($check->decrese_impact == 1 && $client->decrease_effect == 1 && $check1->decrease_effect == 1){
                                $type = 'order';
                                $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                                if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                    $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                    $new_price = $old_item_price - $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                    if(!empty($old_item_price)){
                                        $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                    }else{
                                        $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                        $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                    }
                                    
                                }
                            }
                        }
                    }else{
                        $check = Items::where('id',$p_i_item->item_id)->first();
                        if($impact == 'positive'){
                            if($client->increase_effect == 1 && $check->increase_effect == 1){
                                $type = 'order';
                                $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                                if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                    $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                    $new_price = $old_item_price + $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                    if(!empty($old_item_price)){
                                        $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                                    }else{
                                        $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                        $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                    }
        
                                }
                            }
                        }
        
                        if($impact == 'negative'){
                            if($client->decrease_effect == 1 && $check->decrease_effect == 1){
                                $type = 'order';
                                $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                                if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                    $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                    $new_price = $old_item_price - $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                    if(!empty($old_item_price)){
                                        $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                    }else{
                                        $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                        $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                    }
                                    
                                }
                            }
                        }
                    }
    
                    if($new_price != ''){
                        if ($order_id == 0) {
                            $ordersCount++;
                        }
                        $order_ret_id = $this->createOrder($p_i_item->item_id,$order_id,$client->id,$new_price,$activity_id); //seprate function to create invoice and invoice items and return invoice id
                        $order_id = 0;
                    }
                }////customer

                ActivityLog::where('id',$activity_id)->update(['orders_count' => $ordersCount]);
        }


    public function getPriceImpectInvoice(){
        $invoices0 = Invoices::where('invoices.deleted', '=', '2')
        ->where('invoices.is_dummy', 1)
        ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
        ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
        ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
        ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
        ->orderBy('created_at', 'desc')
        ->get();

        return view('front.invoice.dummy', compact('invoices0'));
    }

    public function createInvoice($item_id,$invoice_id,$customer_id,$newPrice,$activity){
        
        if ($invoice_id == 0) {
            $invoice = new Invoices;
            $invoice->customer_id = $customer_id;
            $invoice->memo = '';
            $invoice->created_by = 'admin';
            $invoice->status = 'approved';
            $invoice->total_price = 0;
            $invoice->total_quantity = 0;
            $invoice->deleted = 2;
            $invoice->list_id = '';
            $invoice->activity_log_id = $activity;
            $timestamp = strtotime(date("Y/m/d"));
            $invoice->statement_date = date("Y-m-d H:i:s", $timestamp);
            $invoice->is_dummy = 1;
            $invoice->save();
            $invoice_id = $invoice->id;
        }

        $invoice_items = new InvoiceItems;
        $invoice_items->invoice_id = $invoice_id;
        $invoice_items->customer_id = $customer_id;
        $invoice_items->item_id = $item_id;
        $invoice_items->item_unit_price = $newPrice;
        $invoice_items->quantity = 1;
        $timestamp = strtotime(date("Y/m/d"));
        $invoice_items->statement_date = date("Y-m-d H:i:s", $timestamp);
        $invoice_items->total_price = $newPrice;
        $invoice_items->save();

        return $invoice_id;
    }

    public function createPriceImpectAllInvoice(){
        DB::beginTransaction();

        try{
        $customers = Customers::where('deleted', 0)->get();
        $price_impact_items = ItemPriceImpect::where('status','pending')->get();
        $activity = new ActivityLog;
        $activity->status = 'pending';
        $activity->type = 'bulk';
        $activity->customer_count = count($customers);
        $activity->save();
        $invoiceCount = 0;
            if(count($price_impact_items) > 0){
            foreach($customers as $customer){
                $invoice_id = 0;
                
                foreach($price_impact_items as $p_i_item){

                $impact = $p_i_item->impact; ////increase or descrease
                $new_price = '';
                $check = Items::where('id',$p_i_item->item_id)->first();
                if($impact == 'positive'){
                    if($check->increase_effect == 1 && $customer->increase_effect == 1){
                        $type = 'invoice';
                        $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $customer->id, $type); ///get from function
                        if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                            $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                            $new_price = $old_item_price + $p_i_item->diff_amount;
                        }else{
                            $old_item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                            if(!empty($old_item_price)){
                                $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                            }
                        }
                    }
                }

                if($impact == 'negative'){
                    if($check->decrease_effect == 1 && $customer->decrease_effect == 1){
                        $type = 'invoice';
                        $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $customer->id, $type); ///get from function
                        if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                            $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                            $new_price = $old_item_price - $p_i_item->diff_amount;
                        }else{
                            $old_item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                            if(!empty($old_item_price)){
                                $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                            }
                        }
                    }
                }

                if($new_price != ''){
                    if ($invoice_id == 0) {
                        $invoiceCount++;
                    }
                    $invoice_ret_id = $this->createInvoice($p_i_item->item_id,$invoice_id,$customer->id,$new_price,$activity->id); //seprate function to create invoice and invoice items and return invoice id
                    $invoice_id = $invoice_ret_id;
                }

                } ////price impact

            }////customer

        }
            ActivityLog::where('id',$activity->id)->update(['invoices_count' => $invoiceCount]);
            $this->makeDummyOrder($activity->id);
            ItemPriceImpect::where('status','pending')->update(['status' => 'completed','activity_log_id' => $activity->id]);
            ActivityLog::where('id',$activity->id)->update(['status' => 'completed']);
            DB::commit();
            Session::flash('success', 'Successfully created dummy invoice.');
            return redirect()->back();
        }catch(Exception $e) {
            DB::rollBack();
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    public function makeDummyOrder($activity_id){
        $clients = Clients::where('status', 1)->get();
        $price_impact_items = ItemPriceImpect::where('status','pending')->get();
        ActivityLog::where('id',$activity_id)->update(['client_count' => count($clients)]);
        $ordersCount = 0;
            if(count($price_impact_items) > 0){
            foreach($clients as $client){
                $order_id = 0;

                foreach($price_impact_items as $p_i_item){

                $impact = $p_i_item->impact; ////increase or descrease
                $new_price = '';
                $check = CustomersItemImpact::where('item_id',$p_i_item->item_id)->where('customer_id',$client->customer_id)->first();
                if (!empty($check)) {
                    if($impact == 'positive'){
                        if($check->increase_impact == 1 && $client->increase_effect == 1){
                        // if($client->increase_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                }
    
                            }
                        }
                    }
    
                    if($impact == 'negative'){
                        if($check->decrese_impact == 1 && $client->decrease_effect == 1){
                        // if($client->decrease_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price - $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                }
                                
                            }
                        }
                    }
                }else{
                    $check = Items::where('id',$p_i_item->item_id)->first();
                    if($impact == 'positive'){
                        if($client->increase_effect == 1 && $check->increase_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                }
    
                            }
                        }
                    }
    
                    if($impact == 'negative'){
                        if($client->decrease_effect == 1 && $check->decrease_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($p_i_item->item_id, $client->id, $type); ///get from function
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price - $p_i_item->diff_amount;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$p_i_item->item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = $old_item_price->selling_price - $p_i_item->diff_amount;
                                }
                                
                            }
                        }
                    }
                }


                if($new_price != ''){
                    if ($order_id == 0) {
                        $ordersCount++;
                    }
                    $order_ret_id = $this->createOrder($p_i_item->item_id,$order_id,$client->id,$new_price,$activity_id); //seprate function to create invoice and invoice items and return invoice id
                    // $ordersCount++;
                    $order_id = $order_ret_id;
                }

                } ////price impact

            }////customer

        }
        ActivityLog::where('id',$activity_id)->update(['orders_count' => $ordersCount]);
    }

    public function createOrder($item_id,$order_id,$client_id,$newPrice,$activity_id){
        if ($order_id == 0) {
            $order = new Orders;
            $order->client_id = $client_id;
            $order->memo = '';
            $order->created_by = '0';
            $order->status = 'approved';
            $order->total_price = 0;
            $order->total_quantity = 0;
            $order->deleted = 2;
            $order->activity_log_id = $activity_id;
            $timestamp = strtotime(date("Y/m/d"));
            $order->statement_date = date("Y-m-d H:i:s", $timestamp);
            $order->is_dummy = 1;
            $order->save();
            $order_id = $order->id;
        }

            $order_items = new OrderItems;
            $order_items->order_id = $order_id;
            $order_items->client_id = $client_id;
            $order_items->item_id = $item_id;
            $order_items->item_unit_price = $newPrice;
            $order_items->quantity = 1;
            $timestamp = strtotime(date("Y/m/d"));
            $order_items->statement_date = date("Y-m-d H:i:s", $timestamp);
            $order_items->total_price = $newPrice;
            $order_items->save();

            return $order_id;
    }

    public function priceImpectadditem(){
        $items = Items::where('deleted',0)->lists('name','id')->toArray();

        return view('front.receive_order.addnew', compact('items'));
    }

    public function priceImpectDelete($id){
        $check = ItemPriceImpect::where('id',$id)->first();
        if (!empty($check)) {
            ItemPriceImpect::where('id', '=', $id)->update(['deleted' => 1]);
            Session::flash('success', 'Successfully Deleted!');
            return redirect('/priceImpect');
        }else{
            Session::flash('error', 'Record Not Found!');
            return redirect('/priceImpect');
        }

    }

    public function addDummyInvoice(){
        $customers = Customers::where('deleted',0)->lists('name','id')->toArray();
        $items = Items::where('deleted',0)->lists('name','id')->toArray();

        return view('front.invoice.adddummyinvoice', compact('items','customers'));
    }

    public function dummyInvoiceCreate(Request $request){
        DB::beginTransaction();        
        try{
            $activity = new ActivityLog;
            $activity->status = 'pending';
            $activity->type = 'admin';
            $activity->customer_count = 1;
            $activity->save();
            $invoiceCount = 0;
            $item_id = $request->item_id;
            $diff = $request->diff;
            $customer = Customers::where('id',$request->customer_id)->first();
    
                $invoice_id = 0;
                $impact = '';
                if ($request->effect == 1) {
                    $impact = 'positive';
                }elseif ($request->effect == 2) {
                    $impact = 'negative';
                }

                $new_price = '';
                $check = Items::where('id',$request->item_id)->first();
                if($impact == 'positive'){
                    if($check->increase_effect == 1 && $customer->increase_effect == 1){
                        $type = 'invoice';
                        $old_item_price = CommonController::getItemPrice($item_id, $customer->id, $type); ///get from function
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $diff;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$item_id)->first();
                                    $new_price = $old_item_price->selling_price + $diff;
                                }
                            }
                        }
                    }
    
                if($impact == 'negative'){
                    if($check->decrease_effect == 1 && $customer->decrease_effect == 1){
                        $type = 'invoice';
                        $old_item_price = CommonController::getItemPrice($item_id, $customer->id, $type); ///get from function
                        if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                            $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                            $new_price = $old_item_price - $diff;
                        }else{
                            $old_item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$item_id)->first();
                            if(!empty($old_item_price)){
                                $new_price = $old_item_price->selling_price - $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$item_id)->first();
                                $new_price = $old_item_price->selling_price - $diff;
                            }
                        }
                    }
                }

            if($new_price != ''){
                if ($invoice_id == 0) {
                    $invoiceCount++;
                }
                $invoice_ret_id = $this->createInvoice($item_id,$invoice_id,$customer->id,$new_price,$activity->id);
                $invoice_id = 0;
            }

            ActivityLog::where('id',$activity->id)->update(['invoices_count' => $invoiceCount]);
            ActivityLog::where('id',$activity->id)->update(['status' => 'completed']);
            DB::commit();
            Session::flash('success', 'Successfully created dummy invoice.');
            return redirect()->back();
        }catch(Exception $e) {
                DB::rollBack();
                Session::flash('error', $e->getMessage());
                return redirect()->back();
        }
    }
    
    public function addDummyOrder(){
        $customers = Customers::where('deleted',0)->lists('name','id')->toArray();
        $items = Items::where('deleted',0)->lists('name','id')->toArray();

        return view('front.invoice.adddummyorder', compact('items','customers'));
    }

    public function dummyOrderCreate(Request $request){
        DB::beginTransaction();        
        try{
            $activity = new ActivityLog;
            $activity->status = 'pending';
            $activity->type = 'admin';
            $activity->client_count = 1;
            $activity->customer_id = $request->customer_id;
            $activity->save();
            $activity_id = $activity->id;
            if (isset($request->item_id) && $request->item_id > 0) {
                $item_id = $request->item_id;
            }else{
                Session::flash('error', 'Please Select an Item');
                return redirect()->back();
            }
            if (isset($request->diff) && $request->diff > 0) {
                $diff = $request->diff;
            }else{
                Session::flash('error', 'Please enter a valid difference');
                return redirect()->back();
            }
            if (!isset($request->client_id) || $request->client_id == 0) {
                Session::flash('error', 'Please select a client');
                return redirect()->back();
            }

            $client = Clients::where('id', $request->client_id)->first();
            $ordersCount = 0;
            $order_id = 0;
    
            $impact = '';
            if ($request->effect == 1) {
                $impact = 'positive';
            }elseif ($request->effect == 2) {
                $impact = 'negative';
            }
            $new_price = '';
            $check = CustomersItemImpact::where('item_id',$item_id)->where('customer_id',$client->customer_id)->first();
                if (!empty($check)) {
                    if($impact == 'positive'){
                        if($check->increase_impact == 1 && $client->increase_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $diff;
                                }else{
                                    $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                }
            
                            }
                        }
                    }
            
                    if($impact == 'negative'){
                        if($check->decrese_impact == 1 && $client->decrease_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price - $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price - $diff;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$item_id)->first();
                                    $new_price = $old_item_price->selling_price - $diff;
                                }
                                        
                            }
                        }
                    }
                }else{
                    if($impact == 'positive'){
                        if($client->increase_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $diff;
                                }else{
                                    $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                }
                            }
                        }
                    }
                    if($impact == 'negative'){
                        if($client->decrease_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price - $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price - $diff;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$item_id)->first();
                                    $new_price = $old_item_price->selling_price - $diff;
                                }
                            }
                        }
                    }
                }

        if($new_price != ''){
            if ($order_id == 0) {
                $ordersCount++;
            }
            $order_ret_id = $this->createOrder($item_id,$order_id,$client->id,$new_price,$activity_id);
            $order_id = 0;
        }
        ActivityLog::where('id',$activity_id)->update(['orders_count' => $ordersCount, 'status' => 'completed','client_id' => $request->client_id]);
        DB::commit();
        Session::flash('success', 'Successfully created dummy order.');
        return redirect()->back();
        }catch(Exception $e) {
            DB::rollBack();
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }
    }
}
