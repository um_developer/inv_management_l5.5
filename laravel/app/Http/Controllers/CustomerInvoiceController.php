<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\Invoices;
use App\Orders;
use App\OrderItems;
use App\Payments;
use App\Bundles;
use App\BundleItems;
use App\CustomerItemCost;
use App\InvoiceItems;
use App\Customers;
use App\PoItems;
use App\Inventory;
use App\PurchaseOrders;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\ItemPriceTemplates;
use Auth;
use Carbon\Carbon;
use App\PrintPageSetting;
use Session;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;

class CustomerInvoiceController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | PurchaseOrder Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $this->customer_id = $customer[0]->id;
        $this->show_bundle = $customer[0]->show_bundle;
    }

    public function index() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

// dd($this->customer_id);
        $model = Invoices::where('invoices.deleted', '=', '0')
                ->where('invoices.customer_id', $this->customer_id)
                ->whereBetween('invoices.statement_date', [$start_date, $end_date])
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('clients', 'clients.id', '=', 'o.client_id')
                ->select('invoices.*', 'o.id as order_refrence', 'clients.name as client_name')
                ->orderBy('invoices.id', 'desc')
                ->get();

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $selected_report_type = '';
        $date = $start_date . ' - ' . $end_date;
        $selected_invoice_number = '';
        return view('front.customers.invoice.index', compact('model', 'date', 'selected_report_type', 'selected_invoice_number'));
    }

    public function search(Request $request) {
        $date = $request->date_range;

        if (isset($request->type)) {
            $selected_report_type = $request->type;
        }
        if (isset($request->invoice_number)) {
            $selected_invoice_number = $request->invoice_number;
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }
        if (!empty($selected_invoice_number)) {
            $model = Invoices::where('invoices.deleted', '=', '0')
                    ->where('invoices.customer_id', $this->customer_id)
                    ->where('invoices.id', $selected_invoice_number)
                    ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('clients', 'clients.id', '=', 'o.client_id')
                    ->select('invoices.*', 'o.id as order_refrence', 'clients.name as client_name')
                    ->orderBy('invoices.id', 'desc')
                    ->get();
        } else {

            $model = Invoices::where('invoices.deleted', '=', '0')
                    ->where('invoices.customer_id', $this->customer_id)
                    ->whereBetween('invoices.statement_date', [$start_date, $end_date]);

            if (!empty($selected_report_type)) {

                $model = $model->where('invoices.status', $selected_report_type);
            }
            $model = $model->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('clients', 'clients.id', '=', 'o.client_id')
                    ->select('invoices.*', 'o.id as order_refrence', 'clients.name as client_name')
                    ->orderBy('invoices.id', 'desc')
                    ->get();
        }

        return view('front.customers.invoice.index', compact('model', 'date', 'selected_report_type', 'selected_invoice_number'));
    }

    public function create() {

        $customer = Customers::where('user_id', $this->user_id)->first();
        $items = CommonController::getItemFormsData($customer->selling_price_template, $customer->sale_price_template);
        $customer_id = $this->customer_id;
        $show_bundle = $this->show_bundle;
        $list_words = \App\Lists::where('deleted', '0')->pluck('title', 'id')->toArray();
        $list_words[0] = 'Select List';
        ksort($list_words);
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
        return view('front.customers.invoice.create', compact('items', 'customer_id', 'list_words','bundles','show_bundle'));
    }

    public function edit($id) {

        $model = Invoices::where('invoices.id', '=', $id)
                ->where('invoices.status', '!=', 'pending')
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }

        $show_bundle = $this->show_bundle;
        $model = Invoices::where('invoices.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->select('invoices.*', 'c.id as customer_id')
                ->get();

        $list_words = \App\Lists::where('deleted', '0')->pluck('title', 'id')->toArray();
        $list_words[0] = 'Select List';
        ksort($list_words);

        $customer = Customers::where('user_id', $this->user_id)->first();
        $items = CommonController::getItemFormsData($customer->selling_price_template, $customer->sale_price_template);

        $po_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'invoice_items.bundle_id')
                ->where('invoice_items.deleted', '=', '0')
                ->select('invoice_items.*', 'i.name as item_name', 'i.cost as cost', 'i.image as image', 'b.name as bundle_name')
                ->get();
        
        if (count($model) == 0) {
            return redirect('/');
        }

        $model = $model[0];
        $customer_id = $this->customer_id;
        $is_order_sync = $customer->is_order_sync;
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
        return view('front.customers.invoice.edit', compact('items', 'model', 'po_items', 'customer_id', 'is_order_sync', 'list_words','bundles','show_bundle'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $validation = array(
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        $items = $input['num'];
        $invoice = new Invoices;
        $invoice->created_by = 'customer';
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);
        $invoice->list_id = $request->list_id;
        $invoice->statement_date = $statement_date;
        $invoice->customer_id = $this->customer_id;
        $invoice->memo = $request->memo;
        $invoice->deleted = '1';
        $invoice->save();

//        $new_id = '11' . $invoice->id;
//        Invoices::where('id', '=', $invoice->id)->update(['id' => $new_id]);
//        $invoice->id = $new_id;

        $total_invoice_price = 0;
        $total_quantity = 0;
        if (count($items) > 0 && isset($invoice->id)) {

            if ($input['item_id_1'] != '' || isset($input['item_id_2']) && $input['item_id_2'] != '') {

                for ($i = 1; $i <= count($items); $i++) {
                    if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                        $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];

                        $invoice_items = new InvoiceItems;
                        $invoice_items->invoice_id = $invoice->id;
                        $invoice_items->customer_id = $this->customer_id;
                        $invoice_items->item_id = $input['item_id_' . $i];
                        $invoice_items->item_unit_price = $input['price_' . $i];
                        $invoice_items->quantity = $input['quantity_' . $i];
                        $invoice_items->statement_date = $statement_date;


                        if ($input['start_serial_number_' . $i] > 0) {

                            $invoice_items->start_serial_number = $input['start_serial_number_' . $i];
                            $invoice_items->end_serial_number = $input['end_serial_number_' . $i];
                            $invoice_items->quantity = $input['quantity_' . $i];
                        }
                        if (isset($input['item_bundle_id_' . $i])) {
                            $invoice_items->bundle_id = $input['item_bundle_id_' . $i];
                            $invoice_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                        }
                        $invoice_items->total_price = $item_total_price;
//                        $invoice_items->created_by = 'customer';
                        $total_invoice_price += $invoice_items->total_price;
                        $total_quantity += $invoice_items->quantity;
                        $invoice_items->save();
                    }
                }

                if ($total_quantity == 0) {
                    DB::rollBack();
                    Session::flash('error', 'Item count is greater than 0.');
                    return redirect()->back();
                }

                Invoices::where('id', '=', $invoice->id)->update([
                    'deleted' => 0,
                    'total_price' => $total_invoice_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Invoice is not created. Please add atleast 1 item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Invoice is not created. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Invoice is created successfully');
        return redirect()->back();
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $user_id = Auth::user()->id;

        $invoice = Invoices::where('id', '=', $input['id'])->get();
        DB::beginTransaction();
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);
        Invoices::where('id', '=', $input['id'])->update([
//            'customer_id' => $input['customer_id'],
            'memo' => $input['memo'],
            'statement_date' => $statement_date,
            'list_id' => $request->list_id,
        ]);

        $invoice = $invoice[0];

        $invoice_total_price = 0;
        $total_quantity = 0;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $po_item = InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->get();
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {

                            if ($total_quantity == 0) {
                                DB::rollBack();
                                Session::flash('error', 'Item count is greater than 0.');
                                return redirect()->back();
                            }

                            InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'start_serial_number' => $input['start_serial_number_' . $i],
                                'end_serial_number' => $input['end_serial_number_' . $i],
                                'statement_date' => $statement_date,
                            ]);
                        } else {
                            InvoiceItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {


                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_items = new InvoiceItems;
                    $invoice_items->invoice_id = $input['id'];
                    $invoice_items->customer_id = $this->customer_id;
                    $invoice_items->item_id = $input['item_id_' . $i];
                    $invoice_items->item_unit_price = $input['price_' . $i];
                    $invoice_items->total_price = $item_total_price;
                    $invoice_items->quantity = $input['quantity_' . $i];
                    $invoice->statement_date = $statement_date;

                    if ($input['start_serial_number_' . $i] > 0) {
                        $invoice_items->start_serial_number = $input['start_serial_number_' . $i];
                        $invoice_items->end_serial_number = $input['end_serial_number_' . $i];
                        $invoice_items->quantity = $input['quantity_' . $i];
                    }
                    if (isset($input['item_bundle_id_' . $i])) {
                        $invoice_items->bundle_id = $input['item_bundle_id_' . $i];
                        $invoice_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }

                    $invoice_items->quantity = $invoice_items->quantity;
//                    $invoice_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $invoice_items->quantity;
                    $invoice_items->save();
//                    print_r($invoice_items); die;
                }
            }

            if ($invoice_total_price > 0) {
                Invoices::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Invoice is not updated. Please add atleast 1 proper item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Invoice is not updated. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Invoice is updated successfully');
        return redirect()->back();
    }

    public function delete($id) {

        $model = Invoices::where('invoices.id', '=', $id)
                ->where('invoices.status', '!=', 'pending')
                ->where('invoices.customer_id', $this->customer_id)
//                  ->orWhere('purchase_orders.deleted', '!=', '0')
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }

        Invoices::where('id', '=', $id)->where('status', '=', 'pending')->update(['deleted' => 1]);
        InvoiceItems::where('invoice_id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id) {
        $model = Invoices::where('invoices.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                
                ->where('invoices.deleted', '=', '0')
                ->select('invoices.*', 'c.id as customer_id', 'c.name as customer_name')
                ->get();

        $po_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'invoice_items.bundle_id')
                ->where('invoice_items.deleted', '=', '0')
                ->select('invoice_items.*', 'i.name as item_name', 'i.image as image', 'b.name as bundle_name')
                ->get();
               
        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.customers.invoice.detail', compact('model', 'po_items'));
    }

    public function printPage($id) {
        $model = Invoices::where('invoices.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->where('invoices.deleted', '=', '0')
                ->leftjoin('lists as i', 'i.id', '=', 'invoices.list_id')
                ->leftjoin('states as s', 's.code', '=', 'c.state')
                ->select('invoices.*', 's.title as state', 'c.id as customer_id', 'c.name as customer_name', 'c.show_image', 'c.show_serial', 'show_package_id', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'c.phone', 'c.address1', 'i.title as list_title')
                ->get();
       
        $po_items = InvoiceItems::where('invoice_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'invoice_items.bundle_id')
                ->where('invoice_items.deleted', '=', '0')
                ->select('invoice_items.*', 'i.name as item_name', 'i.image as image', 'b.name as bundle_name')
                ->get();

        $setting = PrintPageSetting::where('user_type', '=', 'admin')->first();

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.customers.invoice.print', compact('model', 'po_items', 'setting'));
    }

    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }

    function updateCustomerBalance($customer_id, $amount, $type = 'subtract') {

        $customer = Customers::where('id', $customer_id)->get();

        if ($type == 'subtract')
            $final_amount = $customer[0]->balance - $amount;
        else
            $final_amount = $customer[0]->balance + $amount;

        Customers::where('id', $customer_id)->update(['balance' => $final_amount]);
    }

    function updateAdminInventory($item_id, $quantity, $type = 'add') {
        $item = Inventory::where('item_id', $item_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->save();
            }
        } else {
            $total_quantity = $item[0]->quantity - $quantity;
            if ($total_quantity < 0) {
                return 'false';
            } else {
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            }
        }
    }

    public function serialItemValidate($item_id, $start_serial, $quantity) {
        $items = ItemSerials::where('item_id', $item_id)->where('serial', $start_serial)->where('type', 'inventory')->get();
        if (count($items) > 0) {
            $a = ItemSerials::whereBetween('serial', [$start_serial, $start_serial + $quantity])->count();
            $quantity = $quantity + 1;
            if ($a == $quantity)
                return 1;
            else
                return 0;
        } else
            return 0;
    }

    public function addItemToInvoice($item_id, $quantity, $price) {

        if (strpos($item_id, ',') !== false) {
            $item_array = explode(",", $item_id);
            $quantity_array = explode(",", $quantity);
            $i = 0;
            foreach ($item_array as $item) {
                $res = self::innerAddItemToInvoice($item_array[$i], $quantity_array[$i], $price);
                $url = url('customer/invoice/edit' . '/' . $res);
                $html = "<a href='" . $url . "'>" . $res . "</a>";
                $message = 'All items are successfully added in invoice# ' . $html;
                $i++;
            }
        } else {

            $res = self::innerAddItemToInvoice($item_id, $quantity, $price);
            $item = Items::where('id', $item_id)->first();
            $url = url('customer/invoice/edit' . '/' . $res);
            $html = "<a href='" . $url . "'>" . $res . "</a>";
            $message = $item->name . ' is successfully added in invoice# ' . $html;
        }

        return $message;
    }

    private function innerAddItemToInvoice($item_id, $quantity, $price) {


        $model = Invoices::where('invoices.deleted', '=', '0')
                ->where('customer_id', $this->customer_id)
                ->where('status', 'pending')
                ->orderBy('id', 'desc')
                ->first();


        $item = Items::where('id', $item_id)->first();

        if (count($model) > 0) {

            $new_sale_price = CommonController::getItemPrice($item_id, $this->customer_id, 'invoice');

            if ($new_sale_price != '0')
                $price = $new_sale_price['sale_price'][0]['item_unit_price'];

            $invoice_items = new InvoiceItems;
            $invoice_items->invoice_id = $model->id;
            $invoice_items->customer_id = $this->customer_id;
            $invoice_items->item_id = $item_id;
            $invoice_items->item_unit_price = $price;
            $invoice_items->total_price = $quantity * $price;
            $invoice_items->quantity = $quantity;
            $invoice_items->statement_date = date("Y-m-d");
            $invoice_items->save();

            if (!isset($invoice_items->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect()->back();
            }

            Invoices::where('id', '=', $model->id)->update([
                'total_price' => $model->total_price + $price,
                'total_quantity' => $model->total_quantity + $quantity,
            ]);
            $message = $model->id;
        } else {
            DB::beginTransaction();

            $invoice = new Invoices;
            $invoice->created_by = 'customer';
            $invoice->customer_id = $this->customer_id;
            $invoice->total_price = $quantity * $price;
            $invoice->total_quantity = $quantity;
            $invoice->statement_date = date("Y-m-d");
            $invoice->deleted = '0';
            $invoice->save();

//            $new_id = '11' . $invoice->id;
//            Invoices::where('id', '=', $invoice->id)->update(['id' => $new_id]);
//            $invoice->id = $new_id;

            if (!isset($invoice->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect('customer/item-gallery');
            }

            $invoice_id = $invoice->id;

            $invoice_items = new InvoiceItems;
            $invoice_items->invoice_id = $invoice_id;
            $invoice_items->customer_id = $this->customer_id;
            $invoice_items->item_id = $item_id;
            $invoice_items->item_unit_price = $price;
            $invoice_items->total_price = $quantity * $price;
            $invoice_items->quantity = $quantity;
            $invoice_items->save();
            DB::commit();

            if (!isset($invoice_items->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect('customer/item-gallery');
            }

            $message = $invoice_id;
        }

        return $message;
    }
    public function getGalleryImages(Request $request) {

        $mainImages = Items::select('image')->where('id', $request->item_id)->first();
        $images = GalleryImages::select('image')->where('item_id', $request->item_id)->get();
        $allImages = [];
        if (!empty($mainImages)) {
            $allImages[]['image'] = $mainImages->image;
        }
        foreach ($images as $row) {
            $allImages[]['image'] = $row->image;
        }

        return response()->json($allImages);
    }
}
