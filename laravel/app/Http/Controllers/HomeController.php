<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Colors;
use App\Sizes;
use App\Types;
use App\ItemColors;
use App\ItemSizes;
use App\PriceTemplates;
use App\ItemPriceTemplates;
use App\ItemTypes;
use App\Categories;
use App\ApiServers;
use App\Customers;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Illuminate\Support\Arr;
use App\Tags;
use App\GalleryImages;
use Auth;
use Session;
use Excel;
use Validator,
    Input,
    Redirect;

class HomeController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Item Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    public function itemGallery() {

        
        $template = PriceTemplates::where('is_default','2')->first();
        $templaye_id = $template['id'];

        $items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $templaye_id)
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                ->orderBy('items.id', 'desc');

        $drop_down_items = $items->get();
        $items = $items->paginate(15);

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id)
                        // ->whereIn('items.id', $my_items)
                        ->where('items.deleted', '0')
                        ->where('items.parent_item_id', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
//                ->select('items.category_id', 'c.name as category_name')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->pluck('c.name as category_name', 'items.category_id')->toArray();

        $categories[0] = 'Select Category';
        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item['id']] = $item['name'];
        }

        $items_list = $all_packages;
//        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = 0;
        $item_id = 0;
        $category_id = 0;
        $template_id = 0;

        $tags = self::getTags();
       
        return view('front.home.index', compact('items', 'sale_price', 'items_list', 'item_id', 'categories', 'category_id','tags'));
    }

    public function itemGallerySearchNew(Request $request) {
// dd($request->all());
        if (isset($request->subtag) || isset($request->tag)) {
            
                $template = PriceTemplates::where('is_default','2')->first();
                $templaye_id = $template->id;
                
                $subtagCheck = 0;
        if (isset($request->subtag)) {
                $relation = DB::table('tag_item_relation')->where('tag_id',$request->subtag)->get();
                $subtagCheck = 1;
        }elseif (isset($request->tag)) {
                $relation = DB::table('tag_item_relation')->where('tag_id',$request->tag)->get();
        }

        $item_ids = [];
        foreach($relation as $row){
                $item_ids[] = $row->item_id;
        }

        $categories = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $templaye_id)
                ->where('items.parent_item_id', '0')
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->whereIn('items.id',$item_ids)
                ->orderBy('items.category_id', 'desc')
                ->groupBy('items.category_id')
                ->pluck('c.name as category_name', 'items.category_id')->toArray();
                 $categories[0] = 'Select Category';
         
                 $items = Items::where('items.deleted', '=', '0')
                         ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                         ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                         ->where('ipt.price_template_id', $templaye_id)
                         ->whereIn('items.id',$item_ids);
                         
                 $items = $items->where('items.deleted', '0')
                         ->where('items.status', '1');
                        if ($subtagCheck == 1){
                                $items = $items->where('items.parent_item_id', '0');
                        }elseif ($subtagCheck == 0 && !isset(Auth::user()->id )) {
                                $items = $items->where('items.parent_item_id', '0');
                        }

         //                       ->where('items.serial', 'no')
         $items = $items->where('ipt.display', '1')
                         ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                         ->orderBy('items.id', 'desc');
         
                 $items = $items->paginate(15);
        //  dd($items);
                     $drop_down_items = Items::where('items.deleted', '=', '0')
                             ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                             ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                             ->where('ipt.price_template_id', $templaye_id);
                 
         
                 $drop_down_items = $drop_down_items->where('items.deleted', '0')
                         ->where('items.status', '1')
                       //  ->where('items.parent_item_id', '0')
         //                       ->where('items.serial', 'no')
                         ->where('ipt.display', '1')
                         ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                         ->orderBy('items.id', 'desc');
         
         
                 $drop_down_items = $drop_down_items->get();
                 $q = $templaye_id;
                 $items->appends(['search' => $q]);
         
                 $all_packages[0] = 'All Items';
                 foreach ($drop_down_items as $item) {
                     $all_packages[$item->id] = $item->name;
                 }
         
                 $items_list = $all_packages;
         
         
                 $sale_price = 0;
                 $item_id = 0;
                 $category_id = 0;
        }else{
                $template = PriceTemplates::where('is_default','2')->first();
                $templaye_id = $template->id;
         
                 $categories = Items::where('items.deleted', '=', '0')
                                 ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                                 ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                                 ->where('ipt.price_template_id', $templaye_id)
                                ->where('items.parent_item_id', '0')
                                 ->where('items.deleted', '0')
                                 ->where('items.status', '1')
                                 ->where('ipt.display', '1')
                                 ->orderBy('items.category_id', 'desc')
                                 ->groupBy('items.category_id')
                                 ->pluck('c.name as category_name', 'items.category_id')->toArray();
                 $categories[0] = 'Select Category';
         
                 $items = Items::where('items.deleted', '=', '0')
                         ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                         ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                         ->where('ipt.price_template_id', $templaye_id);
                if ($request->item_name != '') {
                        $items = Items::where('items.deleted', '=', '0')
                                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                                ->where('ipt.price_template_id', $templaye_id)
                                ->where('items.upc_barcode',$request->item_name);
                        } 
            
                    $items = $items->where('items.deleted', '0')
                            ->where('items.status', '1')
                            ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                            ->orderBy('items.id', 'desc');
            
                    $items = $items->paginate(15);         
                 if(count($items) == 0){
                        $items = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id);
                        if ($request->item_name != '') {
                                $items = Items::where('items.deleted', '=', '0')
                                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                                        ->where('ipt.price_template_id', $templaye_id);
                                        // ->where('items.name','like','%'. $request->item_name . '%');
                                   $keyword = $request->item_name;
                                   
                                   $items = $items->where(function ($query) use($keyword) {
                                       $query->where('items.name', 'like', '%' . $keyword . '%')
                                          ->orWhere('items.upc_barcode', 'like', '%' . $keyword . '%');
                                     });
                            } 
                    
                            $items = $items->where('items.deleted', '0')
                                    ->where('items.status', '1')
                                   ->where('items.parent_item_id', '0')
                    //                       ->where('items.serial', 'no')
                                   //  ->where('ipt.display', '1')
                                    ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                                    ->orderBy('items.id', 'desc');
                    
                            $items = $items->paginate(15);
                 }

                     $drop_down_items = Items::where('items.deleted', '=', '0')
                             ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                             ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                             ->where('ipt.price_template_id', $templaye_id);
                 
         
                 $drop_down_items = $drop_down_items->where('items.deleted', '0')
                         ->where('items.status', '1')
                        // ->where('items.parent_item_id', '0')
         //                       ->where('items.serial', 'no')
                         ->where('ipt.display', '1')
                         ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                         ->orderBy('items.id', 'desc');
         
         
                 $drop_down_items = $drop_down_items->get();
                 $q = $templaye_id;
                 $items->appends(['search' => $q]);
         
                 $all_packages[0] = 'All Items';
                 foreach ($drop_down_items as $item) {
                     $all_packages[$item->id] = $item->name;
                 }
         
                 $items_list = $all_packages;
         
         
                 $sale_price = 0;
                 $item_id = 0;
                 $category_id = 0;
        }

        $tags = self::getTags();

        return view('front.home.index', compact('items', 'sale_price', 'items_list', 'item_id', 'category_id', 'categories','tags'));
    }
    
    public function itemGallerySearch(Request $request) {

       $template = PriceTemplates::where('is_default','2')->first();
        $templaye_id = $template->id;

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id)
                        ->where('items.parent_item_id', '0')
                        ->where('items.deleted', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->pluck('c.name as category_name', 'items.category_id')->toArray();
        $categories[0] = 'Select Category';

        $items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $templaye_id);

        if ($request->item_id != '0' && $request->category_id == '0') {
            $items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
                    ->where('items.id', $request->item_id);
        } elseif ($request->item_id == '0' && $request->category_id != '0') {

//            $items = Items::where('category_id', $request->category_id)->get()->toArray();

            $items = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)->where('category_id', $request->category_id);
        } elseif ($request->item_id != '0' && $request->category_id != '0') {


            $items = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)
                            ->where('items.id', $request->item_id)->where('category_id', $request->category_id);
        }

        $items = $items->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('items.parent_item_id', '0')
//                       ->where('items.serial', 'no')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                ->orderBy('items.id', 'desc');

        $items = $items->paginate(15);

        if ($request->category_id != '0') {
            $drop_down_items = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)->where('category_id', $request->category_id);
        } else {

            $drop_down_items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id);
        }

        $drop_down_items = $drop_down_items->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('items.parent_item_id', '0')
//                       ->where('items.serial', 'no')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                ->orderBy('items.id', 'desc');


        $drop_down_items = $drop_down_items->get();
        $q = $templaye_id;
        $items->appends(['search' => $q]);

        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $items_list = $all_packages;


        $sale_price = 0;
        $item_id = $request->item_id;
        $category_id = $request->category_id;
        $tags = self::getTags();

        return view('front.home.index', compact('items', 'sale_price', 'items_list', 'item_id', 'category_id', 'categories','tags'));
    }
    
    
    
    public function getGalleryImages(Request $request) {

        $mainImages = Items::select('image')->where('id', $request->item_id)->first();
        $images = GalleryImages::select('image')->where('item_id', $request->item_id)->where('deleted','0')->get();
        $allImages = [];
        if (!empty($mainImages)) {
            $allImages[]['image'] = $mainImages->image;
        }
        foreach ($images as $row) {
            $allImages[]['image'] = $row->image;
        }

        return response()->json($allImages);
    }
    
    public static function getTags(){
        
        
         $tags = Tags::where('deleted',0)->where('status',1)->where('is_subtag',0)->orderby('sorting_order','asc')->take(20)->get();
        $data = [];
       
        foreach ($tags as $row) {
            $data[$row->id] = $row;
        }
        
        foreach ($data as $row) {
                $re = DB::table('sub_tag_relation')->where('tag_id',$row->id)->get();                
                $array = [];
                foreach($re as $r){
                        $y = Tags::where('id',$r->sub_tag_id)->first();
                        array_push($array,$y);
                }
                $data[$row->id]['child'] = $array;
        }

        return $data;
        
    }

    public function exportPDF(){
        $data = Tags::take(10)->get()->toArray();
// dd($data);
        return Excel::create('TempFile', function($excel) use ($data) {

             $excel->sheet('mySheet', function($sheet) use ($data)

         {

                     $sheet->fromArray($data);

         });

        })->download("pdf");
    }

}
