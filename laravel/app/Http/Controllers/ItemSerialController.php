<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\ItemSerials;
use App\ItemSerialPackages;
use App\Categories;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;
use Exception;

class ItemSerialController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Item Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
    }

    public function indexQueue() {

        $model = ItemSerialPackages::where('item_serial_packages.deleted', '=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'item_serial_packages.item_id')
                ->select('item_serial_packages.*', 'i.name as item_name')
                ->orderBy('id', 'desc')
                ->get();
        return view('front.item_serials.index_queue', compact('model'));
    }

    public function index() {

        $items = Items::where('deleted', '=', '0')->where('serial', 'yes')
                        ->lists('name', 'id')->toArray();

        $items[0] = 'Select Item';
        ksort($items);
        $model = ItemSerials::where('item_serials.deleted', '=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
                ->select('item_serials.*', 'i.name as item_name')
                ->orderBy('id', 'desc')
                ->paginate(10);
        return view('front.item_serials.index', compact('model', 'items'));
    }

    public function queueDetail($id) {

         $items = Items::where('deleted', '=', '0')->where('serial', 'yes')
                        ->lists('name', 'id')->toArray();

        $items[0] = 'Select Item';
        ksort($items);
        $model = ItemSerials::where('item_serials.serial_package_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
                ->select('item_serials.*', 'i.name as item_name')
                ->orderBy('id', 'desc')
                ->paginate(10);

        return view('front.item_serials.index', compact('model','items'));
    }

    public function create() {

        $api_items = Items::select(['id', 'name'])->where('deleted', '=', '0')->where('serial', '=', 'yes')->get();
        $new_api_items = [];
        foreach ($api_items as $cat) {
            $new_api_items[$cat->id] = $cat->name;
        }
        $items = $new_api_items;
        return view('front.item_serials.create', compact('items'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $serial_generate = '0';
        $already_exist = 0;
        $serial_package_id = 0;
        $end_serial_number = 0;
        $validation = array(
            'item_id' => 'required',
            'start_serial' => 'required|min:1',
            'quantity' => 'required|min:1',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        DB::beginTransaction();

        $item_serial_package = new ItemSerialPackages;
        $item_serial_package->item_id = $request->item_id;
        $item_serial_package->start_serial_number = $request->start_serial;
//        $item_serial_package->end_serial_number = (int) $request->start_serial + $request->quantity;
        $item_serial_package->quantity = $request->quantity;
        $item_serial_package->created_by = $user_id;
        $item_serial_package->deleted = '1';
        $item_serial_package->save();

        $all_serials = [];

        if (isset($item_serial_package->id)) {
            $serial_package_id = $item_serial_package->id;
            for ($i = 0; $i < $request->quantity; $i++) {
                $serial = (int) $request->start_serial + $i;
                try {
                    $item_serial = new ItemSerials;
                    $item_serial->item_id = $request->item_id;
                    $item_serial->serial = $serial;
                    $item_serial->serial_package_id = $serial_package_id;
                    $item_serial->save();

                    if (isset($item_serial->id)) {
                        $end_serial_number = $serial;
                        $all_serials[] = $serial;
                        $serial_generate = 1;
                    }
                } catch (Exception $ex) {
                    $already_exist = 1;
                }
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Server error. Please try again.');
            return redirect()->back();
        }

        if ($serial_generate == '0') {
            DB::rollBack();
            Session::flash('error', 'Serials are already created. Please try again.');
            return redirect()->back();
        }

        if ($already_exist == 1 && $serial_generate == 1) {
            $diff = (int) $end_serial_number - (int) $all_serials[0];

            if ($diff == count($all_serials)) {
                $message = 'Partially Created';
            } else
                $message = 'Non Sequential';

            ItemSerialPackages::where('id', '=', $serial_package_id)->update(['deleted' => 0, 'message' => $message, 'end_serial_number' => $end_serial_number, 'start_serial_number' => $all_serials[0], 'quantity' => count($all_serials)]);
            DB::commit();
            Session::flash('success', 'Serial Numbers are partially created successfully');
            return redirect()->back();
        }

        DB::commit();
        ItemSerialPackages::where('id', '=', $serial_package_id)->update(['deleted' => 0, 'message' => 'totally Created', 'end_serial_number' => $end_serial_number]);
        Session::flash('success', 'Serial Numbers are created successfully');
        return redirect()->back();
    }

    public function delete($id) {
        ItemSerials::where('id', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function markActive($id) {

        ///CALL API
        ItemSerials::where('id', $id)->update(['status' => 'active']);

        Session::flash('success', 'Serial number is activated Successfully!');
        return redirect()->back();
    }

    public function search(Request $request) {

        $items = Items::where('deleted', '=', '0')->where('serial', 'yes')
                        ->lists('name', 'id')->toArray();

        $items[0] = 'Select Item';
        ksort($items);
        $start_serial = $request->start_serial;
        $end_serial = $request->end_serial;
        $status = $request->status;
        $item_id = $request->item_id;
        
//        print_r($item_id); die;
        $model = ItemSerials::where('item_serials.deleted', '=', '0');
        if($start_serial != '' && $end_serial == '')
            $model =  $model->where('item_serials.serial', 'LIKE', '%' . $start_serial . '%');
        
        if($start_serial != '' && $end_serial != '')
            $model =  $model->whereBetween('item_serials.serial', [$start_serial, $end_serial]);
        
        if($item_id != '0')
             $model =  $model->where('item_serials.item_id',$item_id );
        
         if($status != '0')
             $model =  $model->where('item_serials.status',$status );
        
        $model = $model->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
                ->select('item_serials.*', 'i.name as item_name')
                ->orderBy('id', 'desc')
                ->paginate(100);

        return view('front.item_serials.index', compact('model', 'search_text', 'items'));
    }

}
