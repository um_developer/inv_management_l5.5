<?php

namespace App\Http\Controllers;

use DB;
use App\Clients;
use App\Customers;
use App\users;
use App\Orders;
use App\ReturnOrders;
use App\DirectPaymentSources;
use App\AccountTypes;
use App\States;
use App\PriceTemplates;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Carbon\Carbon;
use App\PrintPageSetting;
use App\User;
use App\Payments;
use Session;
use Validator,
    Input,
    Redirect;

class ClientController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Client Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';

    public function __construct() {
        $this->middleware('auth');

        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();

        $this->customer_id = $customer[0]->id;
    }

    public function index(Request $request) {
        $status = $request->get('status');
        $pricestatus = $request->get('pricestatus');

      if($status=="" OR $status==1){
            $model = Clients::where(['clients.deleted'=> 0,'clients.status'=>1,'customer_id'=> $this->customer_id]);
      }elseif($status==0){
            $model = Clients::where(['clients.deleted'=> 0,'clients.status'=>0,'customer_id'=> $this->customer_id]);
            }
            if(isset($request->pricestatus) && $request->pricestatus=="increase"){
                $model = $model->where('clients.increase_effect', 1);
            }
            if(isset($request->pricestatus) && $request->pricestatus=="decrease"){
                $model = $model->where('clients.decrease_effect', 1);
            }  
            $model =$model->leftjoin('price_templates as pt', 'pt.id', '=', 'clients.selling_price_template')
                ->leftjoin('states as s', 's.code', '=', 'clients.state')
                ->orderBy('id', 'desc')
                ->select('clients.*','s.title as state','pt.title as template_name') 
                ->get();
            
                $active = Clients::where(['clients.deleted'=> 0,'clients.status'=>1])
                ->where('customer_id', $this->customer_id)
                ->leftjoin('price_templates as pt', 'pt.id', '=', 'clients.selling_price_template')
                ->leftjoin('states as s', 's.code', '=', 'clients.state')
                ->orderBy('id', 'desc')
                ->select('clients.*','s.title as state','pt.title as template_name') 
                ->get();
                $inactive = Clients::where(['clients.deleted'=> 0,'clients.status'=>0])
                ->where('customer_id', $this->customer_id)
                ->leftjoin('price_templates as pt', 'pt.id', '=', 'clients.selling_price_template')
                ->leftjoin('states as s', 's.code', '=', 'clients.state')
                ->orderBy('id', 'desc')
                ->select('clients.*','s.title as state','pt.title as template_name') 
                ->get();   
                $active =count(  $active );   
              
                $inactive=count(  $inactive );   
        return view('front.customers.client.index', compact('model','active','inactive','pricestatus'));
    }

    public function create() {

        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
                        ->pluck('title', 'id')->toArray();


        $states = $new_states;
        return view('front.customers.client.create', compact('states', 'templates'));
    }

    public function edit($id) {

        $model = Clients::where('u_id', '=', $id)->where('deleted', '=', '0')->get();
        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;

        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
                        ->pluck('title', 'id')->toArray();

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.customers.client.edit', compact('model', 'states', 'templates'));
    }

    public function postCreate(Request $request) {

        $user_id = $this->customer_id;
        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
//            'sale_price_template' => 'numeric|min:1|max:1000',
            'phone' => 'max:16',
        //    'password' => 'min:6|max:30',
            'email' => 'required|email|max:80',
            'phone' => 'max:16',
        );

        $messages = array(
            'sale_price_template.max' => 'The sale price percentage may not be greater than 1000.',
            'sale_price_template.min' => 'The sale price percentage may not be less than 1.',
        );

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        $client = new Clients;
        $client->name = $request->name;
        $client->email = $request->email;
//        if ($request->sale_price_template == '')
//            $client->sale_price_template = 50;
//        else
//            $client->sale_price_template = $request->sale_price_template;
      //  $client->password = $request->password;
        $client->phone = $request->phone;
        $client->address1 = $request->address1;
        $client->address2 = $request->address2;
        $client->state = $request->state;
        $client->city = $request->city;
        $client->selling_price_template = $request->selling_price_template;
        $client->zip_code = $request->zip_code;
        $client->customer_id = $this->customer_id;
        $client->note = $request->note;
        $client->customer_id = $this->customer_id;
        $client->show_unit_price = $request->show_unit_price;
        $client->show_image = $request->show_image;
        $client->show_serial = $request->show_serial;
        $client->status = $request->status;
        if (isset($request->increase_effect)) {
            $client->increase_effect = $request->increase_effect;
        }else{
            $client->increase_effect = 0;
        }
        if (isset($request->decrease_effect)) {
            $client->decrease_effect = $request->decrease_effect;
        }else{
            $client->decrease_effect = 0;
        }

//        $client->show_package_id = $request->show_package_id;
        $client->save();

        if (isset($client->id)) {
            ///Client Insert in users
         if(false){
                $user = new User;
                $user->firstName = $request->name;
                if ($request->password != '')
                    $user->password = bcrypt($request->password);
                    $user->email =  $request->email;
                $user->role_id = '7';
                $user->save();
                //'user_id' => $user->id;
         }
            $random_string = Functions::generateRandomString(6);
            $u_id = $random_string . $client->id;
            Clients::where('id', '=', $client->id)->update(['u_id' => $u_id]);
            DB::commit();
            Session::flash('success', 'Client is created successfully');
            return redirect()->back();
        } else {
            DB::rollBack();
            Session::flash('error', 'Client is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $client_id = $request->id;
        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'phone' => 'max:16',
            'u_id' => 'required',
            'email' => 'required|email|max:80',
        //    'password' => 'min:6|max:30',
//            'sale_price_template' => 'numeric|min:1|max:1000',
            'phone' => 'max:16',
        );


        $messages = array(
            'sale_price_template.max' => 'The sale price percentage may not be greater than 1000.',
            'sale_price_template.min' => 'The sale price percentage may not be less than 1.',
        );

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $client_id = $request->u_id;
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'u_id');
        array_forget($input, 'submit');

        if (!isset($input['show_unit_price']))
            $input['show_unit_price'] = 0;

        if (!isset($input['show_image']))
            $input['show_image'] = 0;

        if (!isset($input['show_serial']))
            $input['show_serial'] = 0;

        if (!isset($input['increase_effect'])){
            $input['increase_effect'] = 0;
        }
        if (!isset($input['decrease_effect'])){
            $input['decrease_effect'] = 0;
        }
//         if (!isset($input['show_package_id'])) 
//            $input['show_package_id'] = 0;

        Clients::where('u_id', '=', $client_id)->update($input);
///Client update in users
     if(false){
        $Clients = Clients::where('u_id', '=', $client_id)->get();
        $password = '';
        if ($Clients[0]->user_id != '0') {

            if ($input['password'] != '')
                $password = bcrypt($input['password']);

            User::where('id', '=', $Clients[0]->user_id)->where('role_id', '7')->update(['email' => $input['email'], 'password' => $password]);
        } else {
            $user = new User;
           // $user->FirstName = $Clients[0]->name;
            $user->email = $Clients[0]->email;
            $password = '';
            if ($Clients[0]->password != '')
                $user->password = bcrypt($Clients[0]->password);
            $user->role_id = '7';
            $user->save();

            Clients::where('u_id', '=', $client_id)->update(['user_id' => $user->id]);
        }
     }
        Session::flash('success', 'Client info has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        $client = Clients::where('id', '=', $id)->where('customer_id', $this->customer_id)->get();
        if (count($client) == 0) {
            Session::flash('error', 'Try Again later');
            return redirect()->back();
        }

        $orders = Orders::where('client_id', $id)->count();
        $return_orders = ReturnOrders::where('client_id', $id)->count();

        if ($orders != '0' || $return_orders != '0') {
            Session::flash('error', 'This client has some orders.');
            return redirect()->back();
        }

        Clients::where('id', '=', $id)->where('customer_id', $this->customer_id)->delete();
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }
    
    
    public function printPageSetting() {

        $user_id = $this->customer_id;
        $model = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id',$user_id)->first();
  
        if (count($model) < 1) {
            $printPageSetting = new PrintPageSetting;
            $printPageSetting->bottom_text_1 = 'FOR ACCESSORIES VISIT BROADPOSTERS.COM';
            $printPageSetting->bottom_text_2 = 'NOTICE: OPEN ITEMS ARE NON REFUNDABLE. THANK YOU';
            $printPageSetting->user_type = 'customer';
            $printPageSetting->user_id = $user_id;
            $printPageSetting->save();
        }

         $model = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id',$user_id)->first();
        return view('front.customers.client.print_page_setting', compact('model'));
    }

    public function updatePrintPageSetting(Request $request) {

        $input = $request->all();
                                                                                                                                                                                                                                                                                                                
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        PrintPageSetting::where('id', '=', $request->id)->update($input);
        Session::flash('success', 'Updated seccessfully.');
        return redirect()->back();
    }

    public function clientimpact($status,$id,$type){
                if($type=="increase"){
                    Clients::where(['id' =>$id])->update([
                    'increase_effect'=>$status
                ]);
                }else if($type=="decrease"){
                    Clients::where(['id' =>$id])->update([
                        'decrease_effect'=>$status
                    ]);
                }
    }
    public function detail(Request $request, $id){
        
        $client_check = Clients::where('customer_id', '=', $this->customer_id)->where('id', $id)->where('deleted', '=', '0')->get();
        if (count($client_check) == 0) {
            Session::flash('error', 'Client not exist.');
            return Redirect::to('/dashboard');
        }

    $req = $request->all();
    // dd($req);
    if (isset($req['filter']) && $req['filter'] == 1 && isset($req['date_range']) && !empty($req['date_range'])) {

            $date = explode("-",$req['date_range']);
            
            $dateS = new Carbon($date[0]);
            $dateE = new Carbon($date[1]);
            $start_date = $dateS->format('Y-m-d')." 00:00:00";
            $end_date = $dateE->format('Y-m-d')." 23:59:59";
            // $start_date = date("Y-m-d", strtotime($date[0]));
            // $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            if ($request['tab'] == 'payments') {
                $date = explode("-",$req['date_range']);
                $start_date = date("Y-m-d", strtotime($date[0]));
                $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            }else{
                $start_date = Carbon::today()->subDay(7)->toDateString();
                $end_date = Carbon::today()->addDay(1)->toDateString();
            } 
            $payment_source_id = $request->payment_source_id;
            $account_id = $request->account_id;
        $payments0 = Payments::where('payments.deleted', '=', '0')->where('payments.user_type', 'client')->where('payments.payment_type', 'direct')->where('payments.created_by', $this->customer_id)->where('modification_count', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date]);

        if ($id != '0'){
            $payments0 = $payments0->where('payments.user_id', $id);
        }
        if ($payment_source_id != '0'){
            $payments0 = $payments0->where('payments.payment_source_id', $payment_source_id);
        }
        if ($account_id != '0'){
            $payments0 = $payments0->where('payments.account_type_id', $account_id);
        }
        
        if (isset($request['type']) && $request['type'] != ''){
            $payments0 = $payments0->where('payments.status', $request['type']);
        }
        $payments0 = $payments0->select('payments.*', 'c.name as user_name', 'dps.title as payment_type_title', 'at.title as account_type_title')
               ->orderBy('payments.created_at', 'desc')
                ->get();

        $invoices = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->where('orders.created_by', $this->customer_id);
        
            $invoices = $invoices->whereBetween('orders.statement_date', [$start_date, $end_date]);

        if ($id != 0){
            $invoices = $invoices->where('orders.client_id', $id);
        }

        $invoices = $invoices->select(DB::raw("SUM(orders.total_price) as sale_amount,SUM(orders.profit) as profit_amount, orders.client_id, c.name as client_name"))
                        ->groupby('orders.client_id')->get();

        if ($request['tab'] == 'settings') {
        $date = explode("-",$req['date_range']);
        $start_date = date("Y-m-d", strtotime($date[0]));
        $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
        }else{
        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();
        }
        $invoices = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->where('orders.created_by', $this->customer_id);
            $invoices = $invoices->whereBetween('orders.statement_date', [$start_date, $end_date]);

        if ($id != 0){
            $invoices = $invoices->where('orders.client_id', $id);
        }
        $invoices = $invoices->select(DB::raw("SUM(orders.total_price) as sale_amount,SUM(orders.profit) as profit_amount, orders.client_id, c.name as client_name"))
                        ->groupby('orders.client_id')->get();


        $credit_memo = ReturnOrders::where('return_orders.deleted', '0')->where('return_orders.status', '!=', 'pending')->where('return_orders.client_id', '!=', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                ->where('return_orders.created_by', $this->customer_id);
            $credit_memo = $credit_memo->whereBetween('return_orders.statement_date', [$start_date, $end_date]);
            $credit_memo = $credit_memo->where('return_orders.client_id', $id);

        $credit_memo = $credit_memo->select(DB::raw("SUM(return_orders.total_price) as sale_amount,SUM(return_orders.loss) as loss_amount, return_orders.client_id, c.name as client_name"))
                        ->groupby('return_orders.client_id')->get();

        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->client_id] = $memo;
        }

        foreach ($invoices as $memo) {
            $new_invoice[$memo->client_id] = $memo;
        }

        $i = 0;
        if (count($invoices) >= count($credit_memo)) {
            foreach ($invoices as $invoice) {
                if (isset($new_credit_memo[$invoice->client_id])) {
                    $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->client_id]['loss_amount'];
                    $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->client_id]['sale_amount'];
                }

                $model[$i] = $invoice;
                $i++;
            }
        } else {

            foreach ($credit_memo as $invoice) {

                if (isset($new_invoice[$invoice->client_id])) {

                    $invoice['sale_amount'] = $new_invoice[$invoice->client_id]['sale_amount'];
                    $invoice['profit_amount'] = $new_invoice[$invoice->client_id]['profit_amount'];

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;
                } else {

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;

                    $invoice['sale_amount'] = '0';
                    $invoice['profit_amount'] = '0';
                }

                $model[$i] = $invoice;
                $i++;
            }

//            dd($model);
        }

        $basic_info = Clients::where('id',$id)->get();


        $clients = Clients::where('customer_id', $this->customer_id)->get();

        $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }

        $clients = $all_packages;

        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Order Payments';
        $report_type = $type;

        $client_id = $id;
        $selected_report_type = $request->report_type_id;
        $client_info = '-';

        if ($request['tab'] == 'transactions') {
            $date = explode("-",$req['date_range']);
            $start_date = date("Y-m-d", strtotime($date[0]));
            $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
        }else{
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
        }

        if ($request['tab'] == 'transactions') {
            $date = explode("-",$req['date_range']);
            $start_date = date("Y-m-d", strtotime($date[0]));
            $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            $start_date_for_array_searching = date("Y-m-d", strtotime($date[0]));
            $end_date_for_array_searching = date("Y-m-d", strtotime($date[1] . " +1 days"));
            }else{
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
            $start_date_for_array_searching = $start_date;
            $end_date_for_array_searching = $end_date;
            }
        $clients = Clients::where('customer_id', $this->customer_id)->get();
        $client_id = $id;
        $date = $request->date_range;
        $date_new = explode(" - ", $date);

        // $start_date_for_array_searching = date("Y-m-d", strtotime($date_new[0]));
        // $end_date_for_array_searching = date("Y-m-d", strtotime($date_new[1] . " +1 days"));

        $start_date = '2018-06-09 00:00:00';
        $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));


        $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }

        $clients = $all_packages;

        $client_id = $id;
        $selected_report_type = $request->report_type_id;
        $client_info = '-';


        $payments = Payments::where('payments.deleted', '=', '0')
                ->where('payments.created_by', $this->customer_id)     
                ->where('payments.status', 'approved')->where('payments.modification_count', '=', '0')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->where('payments.user_type', 'client')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id');

        if ($client_id != 0)
            $payments = $payments->where('user_id', $client_id);
        else
            $payments = $payments->where('user_id', '0');

        $payments = $payments->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->select('payments.*', 'c.name as client_name')->get();


        $new_model = [];
        $final_array = [];
        if (count($payments) > 0) {

            $new_model = $payments;
            $i = 1;
            $previous_palance = 0;
            $updated_balance = 0;
            $new_array = [];
            $final_balance = 0;
            foreach ($new_model as $item) {

                if ($i == '1') {

                    if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $updated_balance = str_replace("-", "", $item->total_modified_amount);

                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                        $updated_balance = '-' . $item->total_modified_amount;
                    else
                        $updated_balance = $item->total_modified_amount;
                } else {
                    $previous_palance = $updated_balance;
                    if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);

                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                        $updated_balance = $updated_balance - $item->total_modified_amount;
                    else
                        $updated_balance = $updated_balance + $item->total_modified_amount;
                }

                $final_balance = $updated_balance;
                $new_array[] = $item;

                $i++;
            }


            $previous_palance = 0;
            $updated_balance = $final_balance;
            $i = 1;
            foreach ($new_array as $item) {

                if ($item->transaction_type == 'negative' && $item->payment_type == 'direct')
                    $item->total_modified_amount = $item->total_modified_amount;
                else
                    $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                if ($i == '1') {
                    $updated_balance = $final_balance;

                    if ($item->payment_type == 'order')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;;
                } else {
                    $updated_balance = $previous_palance;
                    if ($item->payment_type == 'order')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;
                }

                $item->new_previous_balance = round($previous_palance, 2);
                $item->new_updated_balance = round($updated_balance, 2);
                if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                    $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                    $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                else
                    $item->new_total_modified_amount = $item->total_modified_amount;


                $final_balance = $updated_balance;
                $final_array[] = $item;
                $i++;
            }
        }

        $payments = $final_array;
        $start = $start_date_for_array_searching;
        $end = $end_date_for_array_searching;

        $result = array_filter($payments, function ($value) use ($start, $end) {
            return $start <= $value['statement_date'] && $value['statement_date'] <= $end;
        });

        $transactions = $result;

        if ($request['tab'] == 'orders') {
            $date = explode("-",$req['date_range']);
            $start_date = date("Y-m-d", strtotime($date[0]));
            $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
        }else{
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
        }

        $orders = Orders::where('orders.deleted', '=', '0')
        ->where('orders.created_by', $this->customer_id)
        ->whereBetween('orders.statement_date', [$start_date, $end_date])
        ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
        ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
        ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id');

        if (isset($request['type1']) && $request['type1'] != ""){
            $orders = $orders->where('orders.status', $request['type1']);
        }
        $orders = $orders->where('orders.client_id', $id);

        $orders = $orders->orderBy('orders.id', 'desc')
        ->select('orders.*', 'c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
        ->get();

        if ($request['tab'] == 'return_orders') {
            $date = explode("-",$req['date_range']);
            $start_date = date("Y-m-d", strtotime($date[0]));
            $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
        }else{
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
        }

        $order_returns = ReturnOrders::where('return_orders.deleted', '=', '0')
        ->where('return_orders.created_by', $this->customer_id)
        ->whereBetween('return_orders.statement_date', [$start_date, $end_date])
        ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id');
        if (isset($request['type']) && $request['type'] != ""){
    
        $order_returns = $order_returns->where('return_orders.status', $request['type1']);
    }
    if (!empty($id)){
        
        $order_returns = $order_returns->where('return_orders.client_id', $id);
    }
    
    $order_returns = $order_returns->select('return_orders.*', 'c.name as client_name', 'c.u_id as client_u_id')
        ->orderBy('id', 'desc')
        ->get();

        $clients = Clients::where('customer_id', $this->customer_id)->get();
        $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }

        $clients = $all_packages;
        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Order Payments';
        $report_type = $type;
        $report_status = $request['type'];
        if ($request['tab'] == 'orders'){
            $report_status = $request['type1'];
        }
        if ($request['tab'] == 'return_orders'){
            $report_status = $request['type1'];
        }
        

}else{

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        
        
        $payments0 = Payments::where('payments.deleted', '=', '0')->where('payments.user_type', 'client')->where('payments.payment_type', 'direct')->where('payments.created_by', $this->customer_id)->where('modification_count', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date]);

        if ($id != '0')
            $payments0 = $payments0->where('payments.user_id', $id);

        $payments0 = $payments0->select('payments.*', 'c.name as user_name', 'dps.title as payment_type_title', 'at.title as account_type_title')
               ->orderBy('payments.created_at', 'desc')
                ->get();

                $invoices = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->where('orders.created_by', $this->customer_id);
            $invoices = $invoices->whereBetween('orders.statement_date', [$start_date, $end_date]);

        if ($id != 0){
            $invoices = $invoices->where('orders.client_id', $id);
        }
        $invoices = $invoices->select(DB::raw("SUM(orders.total_price) as sale_amount,SUM(orders.profit) as profit_amount, orders.client_id, c.name as client_name"))
                        ->groupby('orders.client_id')->get();


        $credit_memo = ReturnOrders::where('return_orders.deleted', '0')->where('return_orders.status', '!=', 'pending')->where('return_orders.client_id', '!=', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                ->where('return_orders.created_by', $this->customer_id);
            $credit_memo = $credit_memo->whereBetween('return_orders.statement_date', [$start_date, $end_date]);
            $credit_memo = $credit_memo->where('return_orders.client_id', $id);

        $credit_memo = $credit_memo->select(DB::raw("SUM(return_orders.total_price) as sale_amount,SUM(return_orders.loss) as loss_amount, return_orders.client_id, c.name as client_name"))
                        ->groupby('return_orders.client_id')->get();

        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->client_id] = $memo;
        }

        foreach ($invoices as $memo) {
            $new_invoice[$memo->client_id] = $memo;
        }

        $i = 0;
        if (count($invoices) >= count($credit_memo)) {
            foreach ($invoices as $invoice) {
                if (isset($new_credit_memo[$invoice->client_id])) {
                    $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->client_id]['loss_amount'];
                    $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->client_id]['sale_amount'];
                }

                $model[$i] = $invoice;
                $i++;
            }
        } else {

            foreach ($credit_memo as $invoice) {

                if (isset($new_invoice[$invoice->client_id])) {

                    $invoice['sale_amount'] = $new_invoice[$invoice->client_id]['sale_amount'];
                    $invoice['profit_amount'] = $new_invoice[$invoice->client_id]['profit_amount'];

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;
                } else {

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;

                    $invoice['sale_amount'] = '0';
                    $invoice['profit_amount'] = '0';
                }

                $model[$i] = $invoice;
                $i++;
            }

        }

        $basic_info = Clients::where('id',$id)->get();


        $clients = Clients::where('customer_id', $this->customer_id)->get();

        $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }

        $clients = $all_packages;

        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Order Payments';
        $report_type = $type;

        $client_id = $id;
        $selected_report_type = $request->report_type_id;
        $client_info = '-';

        $client_id = $id;
        $selected_report_type = $request->report_type_id;
        $client_info = '-';


        $payments = Payments::where('payments.deleted', '=', '0')
                ->where('payments.created_by', $this->customer_id)     
                ->where('payments.status', 'approved')->where('payments.modification_count', '=', '0')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->where('payments.user_type', 'client')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id');

        if ($client_id != 0)
            $payments = $payments->where('user_id', $client_id);
        else
            $payments = $payments->where('user_id', '0');

        $payments = $payments->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->select('payments.*', 'c.name as client_name')->get();


        $new_model = [];
        $final_array = [];
        if (count($payments) > 0) {

            $new_model = $payments;
            $i = 1;
            $previous_palance = 0;
            $updated_balance = 0;
            $new_array = [];
            $final_balance = 0;
            foreach ($new_model as $item) {

                if ($i == '1') {

                    if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $updated_balance = str_replace("-", "", $item->total_modified_amount);

                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                        $updated_balance = '-' . $item->total_modified_amount;
                    else
                        $updated_balance = $item->total_modified_amount;
                } else {
                    $previous_palance = $updated_balance;
                    if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);

                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                        $updated_balance = $updated_balance - $item->total_modified_amount;
                    else
                        $updated_balance = $updated_balance + $item->total_modified_amount;
                }

                $final_balance = $updated_balance;
                $new_array[] = $item;

                $i++;
            }


            $previous_palance = 0;
            $updated_balance = $final_balance;
            $i = 1;
            foreach ($new_array as $item) {

                if ($item->transaction_type == 'negative' && $item->payment_type == 'direct')
                    $item->total_modified_amount = $item->total_modified_amount;
                else
                    $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                if ($i == '1') {
                    $updated_balance = $final_balance;

                    if ($item->payment_type == 'order')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;;
                } else {
                    $updated_balance = $previous_palance;
                    if ($item->payment_type == 'order')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;
                }

                $item->new_previous_balance = round($previous_palance, 2);
                $item->new_updated_balance = round($updated_balance, 2);
                if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                    $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                    $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                else
                    $item->new_total_modified_amount = $item->total_modified_amount;


                $final_balance = $updated_balance;
                $final_array[] = $item;
                $i++;
            }
        }

        $transactions = $payments;

        $orders = Orders::where('orders.deleted', '=', '0')
                ->where('orders.created_by', $this->customer_id)
                ->where('orders.client_id', $id)
                ->whereBetween('orders.statement_date', [$start_date, $end_date])
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id');
        if (!empty($selected_report_type)) {

            $orders = $orders->where('orders.status', $selected_report_type);
        }


        $orders = $orders->orderBy('orders.id', 'desc')
                ->select('orders.*', 'c.name as client_name','invoices.status as invoice_status','invoices.id as invoice_refrence','customers.name as customer_name')
                ->get();

        $order_returns = ReturnOrders::where('return_orders.deleted', '=', '0')
        ->where('return_orders.created_by', $this->customer_id)
        ->whereBetween('return_orders.statement_date', [$start_date, $end_date])
        ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id');        
        if (!empty($id)){
            
            $order_returns = $order_returns->where('return_orders.client_id', $id);
        }
        
        $order_returns = $order_returns->select('return_orders.*', 'c.name as client_name', 'c.u_id as client_u_id')
            ->orderBy('id', 'desc')
            ->get();

            $payment_sources = DirectPaymentSources::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();

            $all_payment_sources[0] = 'Select Payment Type';
            foreach ($payment_sources as $item) {
                $all_payment_sources[$item->id] = $item->title;
            }
            $payment_sources = $all_payment_sources;
    
            $accounts = AccountTypes::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();
    
            $all_accounts_sources[0] = 'Select Account';
            foreach ($accounts as $item) {
                $all_accounts_sources[$item->id] = $item->title;
            }
            $accounts = $all_accounts_sources;

        $sync = Customers::select('is_order_sync')->where('id',$this->customer_id)->first();
        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Order Payments';
        $report_type = $type;
        $report_status = $request['type'];
        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $date = $start_date . ' - ' . $end_date;
        $activeTab = 'info';

        return view('front.customers.client.details', compact('payments0','model','basic_info','transactions','orders','order_returns','report_type','report_status','sync','date','activeTab','accounts','payment_sources'));
}
        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();

        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $accounts = AccountTypes::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();

        $all_accounts_sources[0] = 'Select Account';
        foreach ($accounts as $item) {
            $all_accounts_sources[$item->id] = $item->title;
        }
        $accounts = $all_accounts_sources;
        $activeTab = $request['tab'];
        $date_range = $req['date_range'];
        $filter = $req['filter'];
        $sync = Customers::select('is_order_sync')->where('id',$this->customer_id)->first();
           
         return view('front.customers.client.details', compact('payments0','model','basic_info','transactions','orders','order_returns','report_type','activeTab','date_range','filter','report_status','sync','accounts','payment_sources'));
    }

}
