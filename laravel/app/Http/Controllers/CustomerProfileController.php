<?php

namespace App\Http\Controllers;

use DB;
use App\Customers;
use App\users;
use App\States;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\User;
use Session;
use Validator,
    Input,
    Redirect;

class CustomerProfileController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Customer Profile Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $model = Customers::where('deleted', '=', '0')
                ->orderBy('id', 'desc')
                ->get();
        return view('front.customer.index', compact('model'));
    }

    public function create() {

        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;
        return view('front.customer.create', compact('states'));
    }

    public function edit() {

        $user_id = Auth::user()->id;
//        print_r($user_id); die;
        $model = Customers::where('user_id', '=', $user_id)->where('deleted', '=', '0')->get();
//        print_r($model); die;
        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('customer.edit', compact('model', 'states'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;

        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'email' => 'required|email|max:50',
            'username' => 'max:50|min:3|unique:customers,username',
//              'sku' => 'max:8|min:8|unique:items,sku,'.$item_id,
            'password' => 'min:6|max:30',
            'phone' => 'max:16',
//             'phone' => 'regex:/(01)[0-9]{9}/|max:16',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        $customer = new Customers;
        $customer->name = $request->name;
        $customer->username = $request->username;
        $customer->password = $request->password;
        $customer->email = $request->email;
        $customer->sale_price_template = $request->sale_price_template;
        $customer->phone = $request->phone;
        $customer->address1 = $request->address1;
        $customer->address2 = $request->address2;
        $customer->state = $request->state;
        $customer->city = $request->city;
        $customer->zip_code = $request->zip_code;
        $customer->created_by = $user_id;
        $customer->note = $request->note;
        $customer->save();

        $user = new User;
        $user->email = $request->username;
        if ($request->password != '')
            $user->password = bcrypt($request->password);
        $user->role_id = '4';
        $user->save();

        if (isset($customer->id)) {
            $u_id = '100' . $customer->id;
            Customers::where('id', '=', $customer->id)->update(['u_id' => $u_id],['user_id' => $user->id]);
            DB::commit();
            Session::flash('success', 'Customer is created successfully');
            return redirect()->back();
        } else {
            DB::rollBack();
            Session::flash('error', 'Customer is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $customer_id = $request->id;
        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'email' => 'required|email|max:50',
            'phone' => 'max:16',
             'customer' => 'max:250',
            'u_id' => 'required',
//            'username' => 'max:50|min:3|unique:customers,username,' . $customer_id,
//            'password' => 'min:6|max:30',
//             'phone' => 'regex:/(01)[0-9]{9}/|max:16',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $customer_id = $request->u_id;
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'u_id');
        array_forget($input, 'submit');

        Customers::where('u_id', '=', $customer_id)->update($input);
        Session::flash('success', 'Customer has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        Customers::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
