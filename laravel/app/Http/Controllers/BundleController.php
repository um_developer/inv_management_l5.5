<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\Inventory;
use App\PoItems;
use App\PurchaseOrders;
use App\WarehouseInventory;
use App\Packages;
use App\PackageItems;
use App\Bundles;
use App\BundleItems;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\Warehouses;
use Session;
use Validator,
    Input,
    Redirect;
use PDF;

class BundleController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Package Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
//        echo 'dd'; die;
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
//        $this->middleware('auth');
//        if (Auth::user()->role_id != '2') {
//            Redirect::to('customer/dashboard')->send();
//            die;
//        }
    }

    public function index() {

        $items = Items::where('deleted', '=', '0')->where('status', '=', '1')->where('serial', 'no')->get();

        $all_items[0] = 'Select Item';
//        $all_items['admin'] = 'Admin Inventory';
        foreach ($items as $item) {
            $all_items[$item->id] = $item->name;
        }

        $items = $all_items;

        $model = Bundles::where('bundles.deleted', '=', '0')
                ->select('bundles.*')
                ->orderBy('id', 'desc')
                ->get();

        return view('front.bundle.index', compact('model', 'items'));
    }

    public function changeStatus($status, $id) {
        Bundles::where('id', '=', $id)->update(['is_active' => $status]);
        $message = 'Published Successfully Changed!';
        return $message;
    }

    public function postBundleSearch(Request $request) {


        $item_id = $request->item_id;

        if ($item_id == 0)
            return redirect('/bundles');

        $items = Items::where('deleted', '=', '0')->where('status', '=', '1')->where('serial', 'no')->get();

        $all_items[0] = 'Select Item';
        foreach ($items as $item) {
            $all_items[$item->id] = $item->name;
        }


        $items = $all_items;

        $model = Bundles::where('bundles.deleted', '=', '0')
                ->leftjoin('bundle_items as pi', 'pi.bundle_id', '=', 'bundles.id')
                ->where('pi.item_id', $item_id)
                ->select('bundles.*', 'pi.quantity as quantity')
                ->orderBy('id', 'desc')
                ->get();
//        print_r($model); die;
        return view('front.bundle.index', compact('model', 'items'));
    }

    public function createBulk() {

        $vendors = Vendors::select(['u_id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat[0] = 'Select Vendor';
        foreach ($vendors as $cat) {
            $new_cat[$cat->u_id] = $cat->name;
        }

        $search_model = Inventory::where('inventory.deleted', '=', '0')
                ->where('inventory.quantity', '>', '0')
                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                ->select('i.*', 'inventory.quantity as inventory_quantity')
                ->get();

        $items = [];
        $i = 0;
        foreach ($search_model as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['upc_barcode'] = $item->upc_barcode;
            $items[$i]['sku'] = $item->code;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;
            $items[$i]['inventory_quantity'] = $item->inventory_quantity;


            $i++;
        }
        $vendors = $new_cat;
        return view('front.package.create_bulk', compact('vendors', 'items'));
    }

    public function create() {

        $vendors = Vendors::select(['u_id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat[0] = 'Select Vendor';

        foreach ($vendors as $cat) {
            $new_cat[$cat->u_id] = $cat->name;
        }

        $search_model = Items::where('items.deleted', '=', '0')
                ->where('serial', 'no')
                ->where('items.deleted', '0')
                ->where('items.status', '1')
               // ->where('items.parent_item_id', '=', '0')
                ->get();

        $items = [];
        $i = 0;
        foreach ($search_model as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['upc_barcode'] = $item->upc_barcode;
            $items[$i]['sku'] = $item->code;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;
            $items[$i]['inventory_quantity'] = $item->inventory_quantity;


            $i++;
        }
        $vendors = $new_cat;
        return view('front.bundle.create', compact('vendors', 'items'));
    }

    public function copy($id) {
        $user_id = Auth::user()->id;
        $total_package_quantity = 0;
        $package_codes_initials = $user_id;
        $random_num = Functions::generateRandomString(8);
        $package_codes = $package_codes_initials . $random_num;
        DB::beginTransaction();
       $bundle=Bundles::where('id', '=', $id)->first(); 
      
       if(!empty($bundle)){
       $bundles = new Bundles;
       $bundles->name = $bundle->name." copy";
       $bundles->created_by = $user_id;
       $bundles->code = $package_codes;
       $bundles->quantity = '0';
       $bundles->type = 'single';
       $bundles->save();   
      $total_package_quantity = 0;
     $items=BundleItems::where('bundle_id',$id)->get();
     foreach ($items as $key => $value) {
                  $bundle_items = new BundleItems;
                  $bundle_items->bundle_id =  $bundles->id;
                  $bundle_items->item_id = $value->item_id;
                  $bundle_items->quantity = $value->quantity;;
                  $total_package_quantity = $total_package_quantity + $value->quantity;;
                  $bundle_items->save();
               
     }
     Bundles::where('id', '=', $bundles->id)->update([
        'quantity' => $total_package_quantity,
    ]);

    DB::commit();
    Session::flash('success', 'Bundle are created successfully');
    return redirect()->back();
     
    //   if (isset($bundle)) {
    //       for ($i = 1; $i <= count($items); $i++) {
    //           if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

    //               $bundle_items = new BundleItems;
    //               $bundle_items->bundle_id = $bundles->id;
    //               $bundle_items->item_id = $input['item_id_' . $i];
    //               $bundle_items->quantity = $input['quantity_' . $i];
    //               $total_package_quantity = $total_package_quantity + $input['quantity_' . $i];
    //               $bundle_items->save();
    //           }
    //       }

    //       if ($total_package_quantity == 0) {
    //           DB::rollBack();
    //           Session::flash('error', 'Please atleast add 1 Item');
    //           return redirect()->back();
    //       }
    //   }  
       }
    }
    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $total_package_quantity = 0;
        $validation = array(
            'name' => 'required',
        );

        $items = $input['num'];
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }


        $package_codes_initials = $user_id;

//        if ($request->vendor_id == '0') {
//            if ($request->pkg_initials == '') {
//                Session::flash('error', 'Please select any vendor or Enter 4 digit Package initial code.');
//                return redirect()->back();
//            } else
//                $package_codes_initials = $request->pkg_initials;
//        }

        $random_num = Functions::generateRandomString(8);
        $package_codes = $package_codes_initials . $random_num;
        DB::beginTransaction();

        if (count($items) > 0) {

            $bundles = new Bundles;
            $bundles->name = $input['name'];
            $bundles->created_by = $user_id;
            $bundles->code = $package_codes;
            $bundles->quantity = '0';
            $bundles->type = 'single';
            $bundles->save();


            $total_package_quantity = 0;
            if (isset($bundles->id)) {
                for ($i = 1; $i <= count($items); $i++) {
                    if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                        $bundle_items = new BundleItems;
                        $bundle_items->bundle_id = $bundles->id;
                        $bundle_items->item_id = $input['item_id_' . $i];
                        $bundle_items->quantity = $input['quantity_' . $i];
                        $total_package_quantity = $total_package_quantity + $input['quantity_' . $i];
                        $bundle_items->save();
                    }
                }

                if ($total_package_quantity == 0) {
                    DB::rollBack();
                    Session::flash('error', 'Please atleast add 1 Item');
                    return redirect()->back();
                }
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Add atleast 1 item.');
            return redirect()->back();
        }

        Bundles::where('id', '=', $bundles->id)->update([
            'quantity' => $total_package_quantity,
        ]);

        DB::commit();
        Session::flash('success', 'Bundle are created successfully');
        return redirect()->back();
    }

    function edit($id) {

        $model = Bundles::where('bundles.id', '=', $id)
                ->where('bundles.deleted', '=', '0')
                ->select('bundles.*')
                ->get();

        $po_items = BundleItems::where('bundle_id', '=', $id)
                ->where('bundle_items.deleted', '0')
                ->leftjoin('items as i', 'i.id', '=', 'bundle_items.item_id')
                ->select('bundle_items.*', 'i.name as item_name', 'i.code as item_sku', 'i.upc_barcode as item_upc_barcode')
                ->get();

       $search_model = Items::where('items.deleted', '=', '0')
                ->where('serial', 'no')
                ->where('items.deleted', '0')
                ->where('items.status', '1')
               // ->where('items.parent_item_id', '=', '0')
                ->get();

        $items = [];
        $i = 0;
        foreach ($search_model as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['upc_barcode'] = $item->upc_barcode;
            $items[$i]['sku'] = $item->code;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;
            $items[$i]['inventory_quantity'] = $item->inventory_quantity;


            $i++;
        }

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.bundle.edit', compact('model', 'po_items', 'items'));
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $total_package_quantity = 0;
        $validation = array(
            'name' => 'required',
        );

        $items = $input['num'];
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }


        $package_codes_initials = $user_id;

//        if ($request->vendor_id == '0') {
//            if ($request->pkg_initials == '') {
//                Session::flash('error', 'Please select any vendor or Enter 4 digit Package initial code.');
//                return redirect()->back();
//            } else
//                $package_codes_initials = $request->pkg_initials;
//        }

        $random_num = Functions::generateRandomString(8);
        $package_codes = $package_codes_initials . $random_num;
        DB::beginTransaction();

        if (count($items) > 0) {

            Bundles::where('id', '=', $input['id'])->update([
                'name' => $input['name'],
            ]);

            if (isset($input['id'])) {
                for ($i = 1; $i <= count($items); $i++) {
                    if (isset($input['bu_item_id_' . $i])) {
                        ////// if quantity = 0 then delete //////
                        if ($input['quantity_' . $i] == 0) {
                            BundleItems::where('id', '=', $input['item_id_' . $i])->delete();
                        } else {
                            BundleItems::where('id', '=', $input['item_id_' . $i])->update([
                                'quantity' => $input['quantity_' . $i],
                            ]);
                            $total_package_quantity = $total_package_quantity + $input['quantity_' . $i];
                        }
                    } else if ($input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                        $po_item = BundleItems::where('id', '=', $input['item_id_' . $i])->get();
                        // $inventory_item = Inventory::where('item_id', $input['item_id_' . $i])->get();

                        // if ($input['quantity_' . $i] > $inventory_item[0]->quantity) {
                        //     Session::flash('error', 'Some Item quantity is not present in our inventory. Please create purchase order again.');
                        //     return redirect()->back();
                        // }

                        $bundle_items = new BundleItems;
                        $bundle_items->bundle_id = $input['id'];
                        $bundle_items->item_id = $input['item_id_' . $i];
                        $bundle_items->quantity = $input['quantity_' . $i];
                        $total_package_quantity = $total_package_quantity + $input['quantity_' . $i];
                        $bundle_items->save();

                        // $item_inventory = Inventory::where('item_id', '=', $input['item_id_' . $i])->get();
                        // $inventory_item_current_quantity = 0;

                        // if (isset($item_inventory[0]->quantity)) {
                        //     $inventory_item_current_quantity = $item_inventory[0]->quantity;
                        //     $rem_inv_quantity = $inventory_item_current_quantity - $input['quantity_' . $i];
                        //     $total_pkg_quantity = $item_inventory[0]->package_quantity + $input['quantity_' . $i];
                        //     Inventory::where('item_id', '=', $input['item_id_' . $i])->update([
                        //         'quantity' => $rem_inv_quantity,
                        //         'package_quantity' => $total_pkg_quantity,
                        //     ]);
                        // } else {
                        //     DB::rollBack();
                        //     Session::flash('error', 'Item inventory quantity is not enough.');
                        //     return redirect()->back();
                        // }
                    }
                }
            }
        } else {
            Session::flash('error', 'Add atleast 1 item.');
            return redirect()->back();
        }

        Bundles::where('id', '=', $input['id'])->update([
            'quantity' => $total_package_quantity,
        ]);

        DB::commit();
        Session::flash('success', 'Bundle updated successfully');
        return redirect()->back();
    }

    public function delete($id) {

        $model = Bundles::where('bundles.id', '=', $id)
                ->where('status', 'unassigned')
                ->where('quantity', '!=', '0')
                ->get();

        $total_package_quantity = 0;
        if(count($model) > 0){
        $package_quantity = $model[0]->quantity;
        }
       
        $package_items = BundleItems::where('bundle_id', '=', $id)->get();
        DB::beginTransaction();
        if (count($package_items) > 0) {
            foreach ($package_items as $package_item) {

                $row = 0;
                $inventory_item = Inventory::where('item_id', $package_item->item_id)->get();
                $inventory_item_quantity = $inventory_item[0]->quantity;
                $row = $inventory_item_quantity + $package_item->quantity;
                $rem_package_quantity = $inventory_item[0]->package_quantity - $package_item->quantity;

                Inventory::where('item_id', $package_item->item_id)->update([
                    'quantity' => $row,
                    'package_quantity' => $rem_package_quantity,
                ]);

                BundleItems::where('bundle_id', '=', $id)->delete();
            }
        }

        Bundles::where('id', '=', $id)->update([
            'deleted' => '1',
        ]);
        DB::commit();
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id) {

        $model = Bundles::where('bundles.id', '=', $id)
                ->where('bundles.deleted', '=', '0')
                ->select('bundles.*')
                ->get();

        $po_items = BundleItems::where('bundle_id', '=', $id)
                ->where('bundle_items.deleted', '0')
                ->leftjoin('items as i', 'i.id', '=', 'bundle_items.item_id')
                ->select('bundle_items.*', 'i.name as item_name', 'i.code as item_sku', 'i.upc_barcode as item_upc_barcode')
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.bundle.detail', compact('model', 'po_items'));
    }

    public function transferPackage() {

        $warehouses = Warehouses::where('deleted', '=', '0')->get();
        $packages = Packages::where('deleted', '=', '0')
                ->where('status', '=', 'unassigned')
                ->get();

        $all_packages = [];
        $all_packages[0] = 'Select Packages';
        foreach ($packages as $item) {
            $all_packages[$item->id] = $item->id;
        }

        $from_warehouses = [];
        $all_warehouses = [];
        $from_warehouses[0] = 'Admin Inventory';
        foreach ($warehouses as $item) {
            $from_warehouses[$item->id] = $item->name;
        }

        foreach ($warehouses as $item) {
            $all_warehouses[$item->id] = $item->name;
        }

        $all_warehouses = $from_warehouses;

        return view('front.package.transfer', compact('all_warehouses', 'from_warehouses', 'all_packages'));
    }

   
    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }

   

    function generatepdf($package_id) {

        ///$summary = 'aamir';
        $model = Packages::where('packages.id', '=', $package_id)
                ->get();
        $summary = $model[0]->id;
        $view = view('front.package.print', ['summary' => $summary, 'i' => 0])->render();
        $package_name = 'package_' . $summary . '.pdf';
        $pdf = PDF::loadHTML($view)->setPaper('a6', 'landscape')->setWarnings(false)->save($package_name);
//     string|array $size: 'letter', 'legal', 'A4', etc. See Dompdf\Adapter\CPDF::$PAPER_SIZES
//string $orientation: 'portrait' or 'landscape'
//      PDF::load($html)
//        ->filename('/tmp/example1.pdf')
//        ->output();

        return $pdf->stream();
    }

}
