<?php

namespace App\Http\Controllers;

use DB;
use App\Tags;
use App\Items;
use App\TagItemRelation;
use App\SubtagRelation;

use App\GalleryImages;
use App\Colors;
use App\Sizes;
use App\Types;
use App\ItemColors;
use App\ItemSizes;
use App\PriceTemplates;
use App\ItemPriceTemplates;
use App\ItemTypes;
use App\Categories;
use App\ApiServers;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class TagsController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Tags Controller
      |--------------------------------------------------------------------------
      |
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
    }

    public function index() {

        $modal = Tags::where('deleted', 0)->where('is_subtag', 0)->orderby('sorting_order','asc')->get();
        return view('front.tags.index', compact('modal'));
    }

    public function subTags(){
        $modal = Tags::where('deleted', 0)->where('is_subtag', 1)->get();
        return view('front.tags.index', compact('modal'));
    }

    public function create() {

        return view('front.tags.create');
    }

    public function edit($id) {

        $tags = Tags::where('id', '=', $id)->where('deleted', 0)->get();

        if (count($tags) == 0) {
            return redirect('/');
        }
        $model = $tags[0];
        return view('front.tags.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'title' => 'required|max:100',
            'description' => 'required|max:100',
            'tag' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        try{
            $sortOrder = Tags::max('sorting_order');
            if (!empty($sortOrder)) {
                $sortOrder = $sortOrder + 1;
            }else{
                $sortOrder = 0;
            }
            $tag = new Tags;
            $tag->title = $request->title;
            $tag->description = $request->description;
            $tag->sorting_order = $sortOrder;
            $tag->is_subtag = $request->tag;
            $tag->status = 1;
            $tag->save();
            DB::commit();

            if (isset($tag->id)) {
                Session::flash('success', 'Tag is created successfully');
                return redirect()->back();
            } else {
                Session::flash('error', 'Tag is not created. Please try again.');
                return redirect()->back();
            }
        }catch(Exception $e) {
            DB::rollBack();
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $tag_id = $request->id;
        $validation = array(
            'title' => 'required|max:100',
            'description' => 'required|max:100',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        try{
            $input = $request->all();
            array_forget($input, '_token');
            array_forget($input, 'id');
            array_forget($input, 'submit');

            Tags::where('id', '=', $tag_id)->update($input);
            DB::commit();   
            Session::flash('success', 'Tag has been updated.');
            return redirect()->back();
        }catch(Exception $e) {
            DB::rollBack();
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    public function delete($id) {
        Tags::where('id', '=', $id)->update(['status' => 0, 'deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function details(Request $request){
        $template = PriceTemplates::where('is_default','2')->first();
        $templaye_id = $template['id'];
        $categories = Items::where('items.deleted', '=', '0')
        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
        ->where('ipt.price_template_id', $templaye_id)
        ->where('items.deleted', '0')
        ->where('items.parent_item_id', '0')
        ->where('items.status', '1')
        ->where('ipt.display', '1')
        ->orderBy('items.category_id', 'desc')
        ->groupBy('items.category_id')
        ->select('c.name as category_name', 'items.category_id')->get();

        if ($request->type == 'subtag') {
            $tag = Tags::where('id',$request->id)->first();
            $relatedTags = TagItemRelation::where('deleted',0)->where('tag_id',$request->id)->get();
            
            $ids = [];
            foreach ($relatedTags as $row) {
                $ids[] = $row->item_id;
            }
            $subTag = [];
            $Items = Items::select('id','name')->where('status',1)->where('deleted',0)->whereIn('id',$ids)->get();

            $modal = Items::select('id','name')->where('status',1)->where('deleted',0)->whereNotIn('id',$ids)->get();
            return view('front.tags.sample', compact('modal','tag','Items','subTag','categories'));
        }else{
            $tag = Tags::where('id',$request->id)->first();
            if ($tag->has_items == 1) {    
            $relatedTags = TagItemRelation::where('deleted',0)->where('tag_id',$request->id)->get();
            
            $ids = [];
            $subtagids = [];
            foreach ($relatedTags as $row) {
                $ids[] = $row->item_id;
                $subtagids[] = $row->tag_id;
            }
            
            $Items = Items::select('id','name')->where('status',1)->where('deleted',0)->whereIn('id',$ids)->get();
            $data = [];
            foreach($Items as $row){
                $a = [];
                $y = $row->name;
                $remove[] = "'";
                $remove[] = '"';
                $remove[] = "-";
                $FileName = str_replace( $remove, "", $y );
                
                $a['id'] = $row->id;
                $a['name'] = $FileName;
                $data[] = $a;
            }
            $Items = $data;
            $subtagsids0 = SubtagRelation::whereIn('tag_id',$subtagids)->get();
            if(count($subtagsids0) > 0){
                $ids0 = [];
                foreach($subtagsids0 as $row){
                    $ids0[] = $row->sub_tag_id;
                }
                $subTag = Tags::select('title as name','id')->whereIn('id',$ids0)->where('deleted',0)->get();
            }else{
                $subTag = [];
            }

            $modal = Items::select('id','name')->where('status',1)->where('deleted',0)->whereNotIn('id',$ids)->get();
        }else{
            $relatedTags = TagItemRelation::where('deleted',0)->where('tag_id',$request->id)->get();

            if(count($relatedTags) == 0){
                
                $relatedTags = SubtagRelation::where('deleted',0)->where('tag_id',$request->id)->get();
                $ids = [];
                $subtagids = [];
                foreach ($relatedTags as $row) {
                    $subtagids[] = $row->tag_id;
                }
                $subtagsids0 = SubtagRelation::whereIn('tag_id',$subtagids)->get();
                $ids0 = [];
                foreach($subtagsids0 as $row){
                    $ids0[] = $row->sub_tag_id;
                }
                $subTag = Tags::select('title as name','id')->whereIn('id',$ids0)->where('deleted',0)->get();
                
                $subtagsids = SubtagRelation::where('tag_id',$request->id)->get();
                $ids = [];
                foreach($subtagsids as $row){
                    $ids[] = $row->sub_tag_id;
                }
                $Items = [];
                $subTag = Tags::select('title as name','id')->whereIn('id',$ids0)->where('deleted',0)->get();
                $modal = Tags::select('id','title as name')->where('is_subtag',1)->where('deleted',0)->whereNotIn('id',$ids)->get();
                
            }else{
                $ids = [];
                $subtagids = [];
                foreach ($relatedTags as $row) {
                    $ids[] = $row->item_id;
                    $subtagids[] = $row->tag_id;
                }
                $Items = Items::select('id','name')->where('status',1)->where('deleted',0)->whereIn('id',$ids)->get();
                $data = [];
                foreach($Items as $row){
                    $a = [];
                    $y = $row->name;
                    $remove[] = "'";
                    $remove[] = '"';
                    $remove[] = "-";
                    $FileName = str_replace( $remove, "", $y );
                    
                    $a['id'] = $row->id;
                    $a['name'] = $FileName;
                    $data[] = $a;
                }
                $Items = $data;
                $subtagsids0 = SubtagRelation::whereIn('tag_id',$subtagids)->get();
                $ids0 = [];
                foreach($subtagsids0 as $row){
                    $ids0[] = $row->sub_tag_id;
                }
                $subTag = Tags::select('title as name','id')->whereIn('id',$ids0)->where('deleted',0)->get();
                
                $subtagsids = SubtagRelation::where('tag_id',$request->id)->get();
                $ids = [];
                foreach($subtagsids as $row){
                    $ids[] = $row->sub_tag_id;
                }
                $subTag = Tags::select('title as name','id')->whereIn('id',$ids0)->where('deleted',0)->get();
                $modal = Tags::select('id','title as name')->where('is_subtag',1)->where('deleted',0)->whereNotIn('id',$ids)->get();
            }

        }
            return view('front.tags.sample', compact('tag','subTag','Items','modal','categories'));
        }
    }

    public function postCreateDetails(Request $request){
        DB::beginTransaction();
        try{
            if ($request->selectedType == 2) {
                if (count($request->items) > 0) {
                    SubtagRelation::where('tag_id',$request->tagID)->delete();
                    foreach($request->items as $row){
                        $subtag = new SubtagRelation;
                        $subtag->tag_id = $request->tagID;
                        $subtag->sub_tag_id = $row;
                        $subtag->save();
                    }
                    Tags::where('id',$request->tagID)->update(['has_items' => $request->selectedType]);
                    DB::commit();
                    Session::flash('success', 'Tag is created successfully');
                    return redirect()->back();
                }else{
                    TagItemRelation::where('tag_id',$request->tagID)->delete();
                    DB::commit();
                    Session::flash('success', 'Success');
                }
            }elseif ($request->selectedType == 1) {
                
                if (count($request->items) > 0) {
                    TagItemRelation::where('tag_id',$request->tagID)->delete();
                    foreach($request->items as $row){
                        $tag = new TagItemRelation;
                        $tag->tag_id = $request->tagID;
                        $tag->item_id = $row;
                        $tag->save();
                    }
                    Tags::where('id',$request->tagID)->update(['has_items' => $request->selectedType]);
                    DB::commit();
                    Session::flash('success', 'Tag is created successfully');
                    return redirect()->back();
                }else{
                    TagItemRelation::where('tag_id',$request->tagID)->delete();
                    DB::commit();
                    Session::flash('success', 'Success');
                    return redirect()->back();
                }
            }
        }catch(Exception $e) {
            DB::rollBack();
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }

    }

    public function getItems(Request $request){
        
        if ($request->type == 2) {

            $subtagsids = SubtagRelation::where('tag_id',$request->id)->get();
            $ids = [];
            foreach($subtagsids as $row){
                $ids[] = $row->sub_tag_id;
            }
            $modal = Tags::select('id','title as name')->where('is_subtag',1)->where('deleted',0)->whereNotIn('id',$ids)->get();
            return response()->json($modal);
        }elseif ($request->type == 1) {

            $relatedTags = TagItemRelation::where('deleted',0)->where('tag_id',$request->id)->get();            
            $ids = [];
            $subtagids = [];
            foreach ($relatedTags as $row) {
                $ids[] = $row->item_id;
            }
            $modal = Items::where('status',1)->where('deleted',0)->whereNotIn('id',$ids)->get();
            $data = [];
            foreach($modal as $row){
                $a = [];
                $y = $row->name;
                $remove[] = "'";
                $remove[] = '"';
                $remove[] = "-";
                $FileName = str_replace( $remove, "", $y );
                
                $a['id'] = $row->id;
                $a['name'] = $FileName;
                $data[] = $a;
            }
            $modal = $data;
            return response()->json($modal);
        }elseif ($request->type == 'cat') {
            $relatedTags = TagItemRelation::where('deleted',0)->where('tag_id',$request->id)->get();            
            $ids = [];
            $subtagids = [];
            foreach ($relatedTags as $row) {
                $ids[] = $row->item_id;
            }
            if ($request->cat_id == 'all') {
                $modal = Items::where('status',1)->where('deleted',0)->whereNotIn('id',$ids)->get();
            }else{
                $modal = Items::where('status',1)->where('deleted',0)->where('category_id',$request->cat_id)->whereNotIn('id',$ids)->get();
            }

            $data = [];
            foreach($modal as $row){
                $a = [];
                $y = $row->name;
                $remove[] = "'";
                $remove[] = '"';
                $remove[] = "-";
                $FileName = str_replace( $remove, "", $y );
                
                $a['id'] = $row->id;
                $a['name'] = $FileName;
                $data[] = $a;
            }
            $modal = $data;
            return response()->json($modal);
        }
    }

    public function getItemNotReleteTags(){

        $tagrel = TagItemRelation::where('deleted',0)->groupby('item_id')->get();
        $ids = [];
        foreach ($tagrel as $row) {
            $ids[] = $row->item_id;
        }

        $model = Items::where('items.deleted', '=', '0')
            ->where('items.parent_item_id', '=', '0')
            ->whereNotIn('items.id', $ids)
            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
            ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
            ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
            ->select('items.*', 'i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title')
            ->orderBy('items.id', 'desc')
            ->get();

        return view('front.tags.nonTagListing', compact('model', 'categories', 'category_id'));
    }

    public function changeStatus($id){
        if ($id > 0) {
            $tagStatus = Tags::where('id', $id)->first();
            
            if ($tagStatus->status == 1) {
                Tags::where('id', $id)->update(['status' => 0]);
            }else{
                Tags::where('id', $id)->update(['status' => 1]);
            }

        }else{
            return 'Error';
        }
    }

    public function tagsSorting(){
        $modal = Tags::where('deleted', 0)->where('is_subtag', 0)->where('status',1)->orderby('sorting_order','asc')->get();
        return view('front.tags.sorting', compact('modal'));
    }

    public function sortingUpdate(Request $request){
        
        DB::beginTransaction();        
        try{
            $data = $request['data'];
            foreach($data as $key => $value){
                $position = $key;
                $id = $value;
                Tags::where('id', $id)->update(['sorting_order' => $position]);
            }
            DB::commit();
            return 'success';
        }catch(Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
}
