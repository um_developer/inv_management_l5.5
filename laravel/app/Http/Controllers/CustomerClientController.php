<?php

namespace App\Http\Controllers;

use DB;
use App\Clients;
use App\Customers;
use App\users;
use App\Orders;
use App\ReturnOrders;
use App\DirectPaymentSources;
use App\AccountTypes;
use App\States;
use App\PriceTemplates;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Carbon\Carbon;
use App\PrintPageSetting;
use App\User;
use App\Payments;
use Session;
use Validator,
    Input,
    Redirect;

class CustomerClientController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Client Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';

    public function __construct() {
        $this->middleware('auth');

        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        // $customer = Customers::where('user_id', $this->user_id)->get();
        // if ($customer[0]->client_menu == 0)
        //     Redirect::to('customer/dashboard')->send();

        // $this->customer_id = $customer[0]->id;
    }

    public function index() {

        return view('front.client.index');     
    }

}