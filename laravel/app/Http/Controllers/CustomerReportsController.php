<?php

namespace App\Http\Controllers;

use DB;
use App\Vendors;
use App\Inventory;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\WarehousePackages;
use App\WarehouseInventory;
use App\Warehouses;
use App\Customers;
use App\Clients;
use App\Payments;
use App\CreditMemo;
use App\Invoices;
use App\CreditMemoItems;
use App\Items;
use App\InvoiceItems;
use App\OrderItems;
use Carbon\Carbon;
use App\ReturnOrderItems;
use App\Orders;
use App\ReturnOrders;
use Auth;
use Session;
use App\ItemPriceTemplates;
use Validator,
    Input,
    Redirect;
use Excel;

class CustomerReportsController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Admin Inventory Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';
    private $customer_obj = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $this->customer_id = $customer[0]->id;
        $this->customer_obj = $customer[0];
    }

    public function client() {

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();

        $clients = Clients::where('customer_id', $this->customer_id)->get();
        $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }

        $model = Payments::where('payments.status', 'approved')
                ->where('payments.created_by', $this->customer_id)
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id')
                ->where('user_type', 'client')->orderBy('payments.created_at', 'desc')
                ->select('payments.*', 'c.name as client_name')
                ->get();

        $clients = $all_packages;
        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Order Payments';
        $report_type = $type;
        $client_id = '2';

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;
        $user_info = 'All Clients';

        if ($date != 'Select Date Range')
            $date_info = ' from Date Range ' . $date . '.';


        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        return view('front.reports.client', compact('model', 'clients', 'client_id', 'report_type', 'date', 'print_page_info'));
    }

    public function getReportByClient(Request $request) {

        $customer = Customers::where('user_id', $this->user_id)->get();

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();

        $clients = Clients::where('customer_id', $this->customer_id)->get();

        $date = $request->date_range;
        $date_new = explode(" - ", $date);
        $start_date = date("Y-m-d", strtotime($date_new[0]));
        $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));


        $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }

        $clients = $all_packages;

        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Order Payments';
        $report_type = $type;

        $client_id = $request->client_id;
        $selected_report_type = $request->report_type_id;
        $client_info = '-';

$clients_ids = Clients::where('customer_id',$this->customer_id)->where('deleted','0')->select('id')->get()->toArray();
        $payments = Payments::where('payments.deleted', '0')
//                ->where('payments.created_by', $this->customer_id)
              //  ->whereIn('payments.user_id', $clients_ids)
                ->where('payments.status', 'approved')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->where('payments.user_type', 'client')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id');

        if ($selected_report_type == '2')
            $payments = $payments->where('payment_type', 'direct');

        elseif ($selected_report_type == '3')
            $payments = $payments->where('payment_type', 'order');

        if ($client_id != 0)
            $payments = $payments->where('user_id', $client_id);

        $payments = $payments->orderBy('payments.created_at', 'desc')->select('payments.*', 'c.name as client_name')->get();



        $user_info = 'All Clients';
        $date_info = '.';
        if ($request->client_id != '0') {
            $client = Clients::where('id', $request->client_id)->first();
            $user_info = $client->name . '(' . $client->email . ')';
        }

        if ($date != 'Select Date Range')
            $date_info = ' from Date Range ' . $date . '.';


        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;
        $model = $payments;
        return view('front.reports.client', compact('model', 'clients', 'report_type', 'client_info', 'date', 'print_page_info'));
    }

    public function myReports() {

        $customers = '';
        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $model = Payments::where('payments.user_id', $this->customer_id)->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')->where('payments.status', 'approved')->whereBetween('payments.statement_date', [$start_date, $end_date])->where('payments.user_type', 'customer')->orderBy('payments.id', 'desc')->select('payments.*', 'c.name as customer_name')->get();

        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Invoice';

        $report_type = $type;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;

        $customer = Customers::where('id', $this->customer_id)->first();
        $customer_info = ucwords($customer->name);
        return view('front.reports.customer', compact('model', 'customers', 'customer_info', 'report_type', 'date', 'print_page_info'));
    }

    public function getReportByType(Request $request) {

        $customers = '';

        $date = $request->date_range;
        $date_new = explode(" - ", $date);
        $start_date = date("Y-m-d", strtotime($date_new[0]));
        $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));

        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Invoice';
        $report_type = $type;

        $selected_report_type = $request->report_type_id;

        if ($selected_report_type == '1')
            $payments = Payments::where('payments.user_id', $this->customer_id)->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')->where('payments.status', 'approved')->whereBetween('payments.statement_date', [$start_date, $end_date])->where('payments.user_type', 'customer')->orderBy('payments.id', 'desc')->select('payments.*', 'c.name as customer_name')->get();

        elseif ($selected_report_type == '2')
            $payments = Payments::where('payments.user_id', $this->customer_id)->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')->where('payments.status', 'approved')->whereBetween('payments.statement_date', [$start_date, $end_date])->where('payments.user_type', 'customer')->where('payment_type', 'direct')->orderBy('payments.id', 'desc')->select('payments.*')->select('payments.*', 'c.name as customer_name')->get();

        elseif ($selected_report_type == '3')
            $payments = Payments::where('payments.user_id', $this->customer_id)->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')->where('payments.status', 'approved')->whereBetween('payments.statement_date', [$start_date, $end_date])->where('payments.user_type', 'customer')->where('payment_type', 'invoice')->orderBy('payments.id', 'desc')->select('payments.*')->select('payments.*', 'c.name as customer_name')->get();

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $customer = Customers::where('id', $this->customer_id)->first();
        $customer_info = ucwords($customer->name);
        $model = $payments;
        return view('front.reports.customer', compact('model', 'customers', 'report_type', 'customer_info', 'date', 'print_page_info'));
    }

    public function clientSaleProfitReport() {

        $customers = Clients::where('deleted', '0')->where('customer_id', $this->customer_id)->get();
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        $all_packages[0] = 'Select Client';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $start_date = Carbon::today()->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $invoices = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                        ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                        ->whereBetween('orders.statement_date', [$start_date, $end_date])
                        ->where('orders.created_by', $this->customer_id)
                        ->select(DB::raw("SUM(orders.total_price) as sale_amount,SUM(orders.profit) as profit_amount, orders.client_id, c.name as client_name"))
                        ->groupby('orders.client_id')->get();


        $credit_memo = ReturnOrders::where('return_orders.deleted', '0')->where('return_orders.status', '!=', 'pending')->where('return_orders.client_id', '!=', '0')
                        ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                        ->whereBetween('return_orders.statement_date', [$start_date, $end_date])
                        ->where('return_orders.created_by', $this->customer_id)
                        ->select(DB::raw("SUM(return_orders.total_price) as sale_amount,SUM(return_orders.loss) as loss_amount, return_orders.client_id, c.name as client_name"))
                        ->groupby('return_orders.client_id')->get();

        $new_credit_memo = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->client_id] = $memo;
        }
        $i = 0;
        foreach ($invoices as $invoice) {
            if (isset($new_credit_memo[$invoice->client_id])) {
                $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->client_id]['loss_amount'];
                $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->client_id]['sale_amount'];
            }

            $model[$i] = $invoice;
            $i++;
        }

        $start_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;
        return view('front.reports.client_profit_loss', compact('model', 'customers', 'report_type', 'date', 'print_page_info'));
    }

    public function clientSaleProfitReportSearch(Request $request) {

        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 'Select Date Range' && $request->customer_id == 0)
            return redirect()->back();

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }


        $customers = Clients::where('deleted', '0')->where('customer_id', $this->customer_id)->get();

        $all_packages[0] = 'Select Client';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;
        $invoices = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->where('orders.created_by', $this->customer_id);
        if ($date != 'Select Date Range')
            $invoices = $invoices->whereBetween('orders.statement_date', [$start_date, $end_date]);

        if ($request->customer_id != 0)
            $invoices = $invoices->where('orders.client_id', $request->customer_id);

        $invoices = $invoices->select(DB::raw("SUM(orders.total_price) as sale_amount,SUM(orders.profit) as profit_amount, orders.client_id, c.name as client_name"))
                        ->groupby('orders.client_id')->get();


        $credit_memo = ReturnOrders::where('return_orders.deleted', '0')->where('return_orders.status', '!=', 'pending')->where('return_orders.client_id', '!=', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                ->where('return_orders.created_by', $this->customer_id);
        if ($date != 'Select Date Range')
            $credit_memo = $credit_memo->whereBetween('return_orders.statement_date', [$start_date, $end_date]);

        if ($request->customer_id != 0)
            $credit_memo = $credit_memo->where('return_orders.client_id', $request->customer_id);

        $credit_memo = $credit_memo->select(DB::raw("SUM(return_orders.total_price) as sale_amount,SUM(return_orders.loss) as loss_amount, return_orders.client_id, c.name as client_name"))
                        ->groupby('return_orders.client_id')->get();

        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->client_id] = $memo;
        }

        foreach ($invoices as $memo) {
            $new_invoice[$memo->client_id] = $memo;
        }

        $i = 0;
        if (count($invoices) >= count($credit_memo)) {
            foreach ($invoices as $invoice) {
                if (isset($new_credit_memo[$invoice->client_id])) {
                    $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->client_id]['loss_amount'];
                    $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->client_id]['sale_amount'];
                }

                $model[$i] = $invoice;
                $i++;
            }
        } else {

            foreach ($credit_memo as $invoice) {

                if (isset($new_invoice[$invoice->client_id])) {

                    $invoice['sale_amount'] = $new_invoice[$invoice->client_id]['sale_amount'];
                    $invoice['profit_amount'] = $new_invoice[$invoice->client_id]['profit_amount'];

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;
                } else {

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;

                    $invoice['sale_amount'] = '0';
                    $invoice['profit_amount'] = '0';
                }

                $model[$i] = $invoice;
                $i++;
            }

//            dd($model);
        }

        $user_info = 'All Clients';
        $date_info = '.';
        if ($request->customer_id != '0') {
            $client = Clients::where('id', $request->customer_id)->first();
            $user_info = $client->name . '(' . $client->email . ')';
        }

        if ($date != 'Select Date Range')
            $date_info = ' from Date Range ' . $date . '.';


        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        return view('front.reports.client_profit_loss', compact('model', 'customers', 'report_type', 'date', 'print_page_info'));
    }

    public function itemSaleProfitReport() {

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->pluck('item_id')->toArray();
        $items = Items::where('deleted', '0')->where('status', '1')->whereIn('id', $my_items)->get();
        $all_packages[0] = 'Select Item';
        foreach ($items as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $items = $all_packages;

        $start_date = Carbon::today()->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $order_items = OrderItems::where('order_items.deleted', '0')
                        ->leftjoin('orders as o', 'o.id', '=', 'order_items.order_id')
                        ->leftjoin('items as it', 'it.id', '=', 'order_items.item_id')
                        ->where('o.status', '!=', 'pending')
                        ->whereBetween('o.statement_date', [$start_date, $end_date])
                        ->where('o.created_by', $this->customer_id)
                        ->select(DB::raw("SUM(order_items.total_price) as order_sale_amount, SUM(order_items.profit) as profit_amount, SUM(order_items.quantity) as order_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('order_items.item_id')->get();


        $return_order_items = ReturnOrderItems::where('return_order_items.deleted', '0')
                        ->leftjoin('return_orders as ro', 'ro.id', '=', 'return_order_items.return_order_id')
                        ->leftjoin('items as it', 'it.id', '=', 'return_order_items.item_id')
                        ->whereBetween('ro.statement_date', [$start_date, $end_date])
                        ->where('ro.status', '!=', 'pending')
                        ->where('ro.created_by', $this->customer_id)
                        ->select(DB::raw("SUM(return_order_items.total_price) as return_order_sale_amount,SUM(return_order_items.loss) as loss_amount,SUM(return_order_items.quantity) as return_order_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('return_order_items.item_id')->get();

        $new_return_order_items = [];
        $new_order_items = [];
        $model = [];

        foreach ($return_order_items as $memo) {
            $new_return_order_items[$memo->item_id] = $memo;
        }

        foreach ($order_items as $memo) {
            $new_order_items[$memo->item_id] = $memo;
        }
        $i = 0;
        foreach ($order_items as $invoice) {
            if (isset($new_return_order_items[$invoice->item_id])) {
                $invoice['return_order_loss_amount'] = $new_return_order_items[$invoice->item_id]['loss_amount'];
                $invoice['return_order_sale_amount'] = $new_return_order_items[$invoice->item_id]['return_order_sale_amount'];
                $invoice['return_order_quantity'] = $new_return_order_items[$invoice->item_id]['return_order_quantity'];
            }

            $model[$i] = $invoice;
            $i++;
        }

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
//        print_r($model); die;

        $start_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;
        return view('front.reports.client_item_profit_loss', compact('model', 'items', 'report_type', 'date', 'print_page_info'));
    }

    public function itemSaleProfitReportSearch(Request $request) {

        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 'Select Date Range' && $request->item_id == 0)
            return redirect()->back();

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }


        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->pluck('item_id')->toArray();
        $items = Items::where('deleted', '0')->where('status', '1')->whereIn('id', $my_items)->get();
        $all_packages[0] = 'Select Item';
        foreach ($items as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $items = $all_packages;

        $invoices = OrderItems::where('order_items.deleted', '0')
                ->leftjoin('orders as o', 'o.id', '=', 'order_items.order_id')
                ->leftjoin('items as it', 'it.id', '=', 'order_items.item_id')
                ->where('o.status', '!=', 'pending')
                ->where('o.created_by', $this->customer_id);

        if ($date != 'Select Date Range')
            $invoices = $invoices->whereBetween('o.statement_date', [$start_date, $end_date]);

        if ($request->item_id != 0)
            $invoices = $invoices->where('order_items.item_id', $request->item_id);

        $invoices = $invoices->select(DB::raw("SUM(order_items.total_price) as order_sale_amount, SUM(order_items.profit) as profit_amount, SUM(order_items.quantity) as order_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('order_items.item_id')->get();

        $credit_memo = ReturnOrderItems::where('return_order_items.deleted', '0')
                ->leftjoin('return_orders as ro', 'ro.id', '=', 'return_order_items.return_order_id')
                ->leftjoin('items as it', 'it.id', '=', 'return_order_items.item_id')
                ->where('ro.status', '!=', 'pending')
                ->where('ro.created_by', $this->customer_id);

        if ($date != 'Select Date Range')
            $credit_memo = $credit_memo->whereBetween('ro.statement_date', [$start_date, $end_date]);

        if ($request->item_id != 0)
            $credit_memo = $credit_memo->where('return_order_items.item_id', $request->item_id);

        $credit_memo = $credit_memo->select(DB::raw("SUM(return_order_items.total_price) as return_order_sale_amount,SUM(return_order_items.loss) as loss_amount,SUM(return_order_items.quantity) as return_order_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('return_order_items.item_id')->get();

        $new_return_order_items = [];
        $new_order_items = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_return_order_items[$memo->item_id] = $memo;
        }

        foreach ($invoices as $memo) {
            $new_order_items[$memo->item_id] = $memo;
        }

        $i = 0;
        if (count($invoices) >= count($credit_memo)) {
            foreach ($invoices as $invoice) {
                if (isset($new_return_order_items[$invoice->item_id])) {
                    $invoice['return_order_loss_amount'] = $new_return_order_items[$invoice->item_id]['loss_amount'];
                    $invoice['return_order_sale_amount'] = $new_return_order_items[$invoice->item_id]['return_order_sale_amount'];
                    $invoice['return_order_quantity'] = $new_return_order_items[$invoice->item_id]['return_order_quantity'];
                }

                $model[$i] = $invoice;
                $i++;
            }
        } else {

            foreach ($credit_memo as $invoice) {

                if (isset($new_order_items[$invoice->item_id])) {

                    $invoice['order_sale_amount'] = $new_order_items[$invoice->item_id]['order_sale_amount'];
                    $invoice['profit_amount'] = $new_order_items[$invoice->item_id]['profit_amount'];
                    $invoice['order_quantity'] = $new_order_items[$invoice->item_id]['order_quantity'];

                    $invoice['return_order_loss_amount'] = $invoice->loss_amount;
                    $invoice['return_order_sale_amount'] = $invoice->return_order_sale_amount;
                    $invoice['return_order_quantity'] = $invoice->return_order_sale_amount;
                } else {

                    $invoice['return_order_loss_amount'] = $invoice->loss_amount;
                    $invoice['return_order_sale_amount'] = $invoice->return_order_sale_amount;
                    $invoice['return_order_quantity'] = $invoice->return_order_quantity;

                    $invoice['order_sale_amount'] = '0';
                    $invoice['order_quantity'] = '0';
                    $invoice['profit_amount'] = '0';
                }

                $model[$i] = $invoice;
                $i++;
            }
        }


        if ($date != 'Select Date Range')
            $print_page_info['top'] = 'Report of items from Date Range ' . $date;

        $print_page_info['bottom'] = '';

        return view('front.reports.client_item_profit_loss', compact('model', 'items', 'report_type', 'date', 'print_page_info'));
    }

    public function ClientBalanceReport() {
        $model = Clients::where('clients.deleted', '=', '0')
                ->where('customer_id', $this->customer_id)
                ->orderBy('balance', 'desc')
                ->get();
        return view('front.reports.client_balance', compact('model'));
    }

    public function clientStatementReport() {

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $start_date = '2018-06-09 00:00:00';
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();
        
        $all_packages = [];

        $clients = Clients::where('customer_id', $this->customer_id)->get();
        // $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }
        $model = [];


        $clients = $all_packages;

        $client_id = '0';

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;
        $user_info = 'All Clients';

        if ($date != 'Select Date Range')
            $date_info = ' from Date Range ' . $date . '.';


        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        return view('front.reports.client_statement_report', compact('model', 'clients', 'client_id', 'date', 'print_page_info'));
    }

    public function getStatementReportByClient(Request $request) {

        $customer = Customers::where('user_id', $this->user_id)->get();

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();

        $clients = Clients::where('customer_id', $this->customer_id)->get();

        $date = $request->date_range;
        $date_new = explode(" - ", $date);

        $start_date_for_array_searching = date("Y-m-d", strtotime($date_new[0]));
        $end_date_for_array_searching = date("Y-m-d", strtotime($date_new[1] . " +1 days"));

        $start_date = '2018-06-09 00:00:00';
        $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));


        $all_packages[0] = 'Select Client';
        foreach ($clients as $item) {
            $all_packages[$item->id] = ucwords($item->name) . ' (BAL: $' . $item->balance . ')';
        }

        $clients = $all_packages;

        $client_id = $request->client_id;
        $selected_report_type = $request->report_type_id;
        $client_info = '-';


        $payments = Payments::where('payments.deleted', '=', '0')
                //->where('payments.created_by', $this->customer_id)     
                ->where('payments.status', 'approved')->where('payments.modification_count', '=', '0')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->where('payments.user_type', 'client')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id');

        if ($client_id != 0)
            $payments = $payments->where('user_id', $client_id);
        else
            $payments = $payments->where('user_id', '0');

        $payments = $payments->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->select('payments.*', 'c.name as client_name')->get();


        $new_model = [];
        $final_array = [];
        if (count($payments) > 0) {

            $new_model = $payments;
            $i = 1;
            $previous_palance = 0;
            $updated_balance = 0;
            $new_array = [];
            $final_balance = 0;
            foreach ($new_model as $item) {

                if ($i == '1') {

                    if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $updated_balance = str_replace("-", "", $item->total_modified_amount);

                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                        $updated_balance = '-' . $item->total_modified_amount;
                    else
                        $updated_balance = $item->total_modified_amount;
                } else {
                    $previous_palance = $updated_balance;
                    if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);

                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                        $updated_balance = $updated_balance - $item->total_modified_amount;
                    else
                        $updated_balance = $updated_balance + $item->total_modified_amount;
                }

                $final_balance = $updated_balance;
                $new_array[] = $item;

                $i++;
            }


            $previous_palance = 0;
            $updated_balance = $final_balance;
            $i = 1;
            foreach ($new_array as $item) {

                if ($item->transaction_type == 'negative' && $item->payment_type == 'direct')
                    $item->total_modified_amount = $item->total_modified_amount;
                else
                    $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                if ($i == '1') {
                    $updated_balance = $final_balance;

                    if ($item->payment_type == 'order')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;;
                } else {
                    $updated_balance = $previous_palance;
                    if ($item->payment_type == 'order')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;
                }

                $item->new_previous_balance = round($previous_palance, 2);
                $item->new_updated_balance = round($updated_balance, 2);
                if ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                    $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                elseif ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'order_return')
                    $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                else
                    $item->new_total_modified_amount = $item->total_modified_amount;


                $final_balance = $updated_balance;
                $final_array[] = $item;
                $i++;
            }
        }

        $payments = $final_array;



        $user_info = 'All Clients';
        $date_info = '.';
        if ($request->client_id != '0') {
            $client = Clients::where('id', $request->client_id)->first();
            $user_info = $client->name . '(' . $client->email . ') ('.$client->address1.' '.$client->address2.' '.$client->city.' '.$client->state.')';
        }

        if ($date != 'Select Date Range')
            $date_info = ' from Date Range ' . $date . '.';


        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        $start = $start_date_for_array_searching;
        $end = $end_date_for_array_searching;

        $result = array_filter($payments, function ($value) use ($start, $end) {
            return $start <= $value['statement_date'] && $value['statement_date'] <= $end;
        });

        $model = $result;

        return view('front.reports.client_statement_report', compact('model', 'clients', 'client_info', 'date', 'print_page_info'));
    }

    public function myStatementReport() {

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $customer_id = $this->customer_id;
        $end_date = Carbon::today()->addDay(1)->toDateString();
        $start_date = '2018-06-09 00:00:00';

        $model = $model = Payments::where('payments.status', '!=', 'pending')->where('payments.modification_count', '=', '0')->where('payments.deleted', '=', '0')
                        ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                        ->whereBetween('payments.statement_date', [$start_date, $end_date])->where('user_type', 'customer');
        $model = $model->where('payments.user_id', $customer_id);
        $model = $model->select('payments.*', 'c.name as customer_name')
                        ->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->get();

        $new_model = [];
        if (count($model) > 0) {
            $new_model = $model;

            $i = 1;
            $previous_palance = 0;
            $updated_balance = 0;
            $new_array = [];
            $final_balance = 0;
            foreach ($new_model as $item) {

                if ($i == '1') {
                    if ($item->payment_type == 'direct') {
                        if ($item->transaction_type == 'negative')
                            $updated_balance = str_replace("-", "", $item->total_modified_amount);
                        else
                            $updated_balance = '-' . $item->total_modified_amount;
                    } else
                        $updated_balance = $item->total_modified_amount;
                } else {
                    $previous_palance = $updated_balance;
                    if ($item->payment_type == 'direct') {
                        if ($item->transaction_type == 'negative')
                            $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);
                        else
                            $updated_balance = $updated_balance - $item->total_modified_amount;
                    } else
                        $updated_balance = $updated_balance + $item->total_modified_amount;
                }

                $final_balance = $updated_balance;
                $new_array[] = $item;

                $i++;
            }

            $final_array = [];
            $previous_palance = 0;
            $updated_balance = $final_balance;
            $i = 1;
            foreach ($new_array as $item) {
                if ($item->transaction_type == 'negative' && $item->payment_type == 'direct')
                    $item->total_modified_amount = $item->total_modified_amount;
                else
                    $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                if ($i == '1') {
                    $updated_balance = $final_balance;

                    if ($item->payment_type == 'invoice')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;;
                } else {
                    $updated_balance = $previous_palance;
                    if ($item->payment_type == 'invoice')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;
                }

                $item->new_previous_balance = round($previous_palance, 2);
                $item->new_updated_balance = round($updated_balance, 2);
                if ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'credit_memo')
                    $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                elseif ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                    $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);
                else
                    $item->new_total_modified_amount = $item->total_modified_amount;


                $final_balance = $updated_balance;
                $final_array[] = $item;
                $i++;
            }
        }
        
        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $start_date_for_array_searching = date("Y-m-d", strtotime($start_date));
        $end_date_for_array_searching = date("Y-m-d", strtotime($end_date));

        $start = $start_date_for_array_searching;
        $end = $end_date_for_array_searching;

        $result = array_filter($final_array, function ($value) use ($start, $end) {
            return $start <= $value['statement_date'] && $value['statement_date'] <= $end;
        });

        $model = $result;



        $customers = '';

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;

        return view('front.reports.customer_statement_report', compact('model', 'customers', 'date', 'print_page_info', 'customer_info'));
    }

    public function myStatementReportSearch(Request $request) {


        $customers = '';
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $date = $request->date_range;

        if ($date == 'Select Date Range' && $request->item_id == 0)
            return redirect()->back();

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date_for_array_searching = date("Y-m-d", strtotime($date_new[0]));
            $end_date_for_array_searching = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
            $start_date = '2018-06-09 00:00:00';
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $customer_id = $this->customer_id;
        $selected_report_type = $request->report_type_id;
        $customer = Customers::where('id', $customer_id)->first();


        $payments = $payments = Payments::where('payments.status', '!=', 'pending')->where('payments.modification_count', '=', '0')->where('payments.deleted', '=', '0')
                        ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                        ->whereBetween('payments.statement_date', [$start_date, $end_date])->where('user_type', 'customer');

        if ($customer_id != '0')
            $payments = $payments->where('payments.user_id', $customer_id);

        $payments = $payments->select('payments.*', 'c.name as customer_name')
                        ->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->get();

        $new_model = [];
        if (count($payments) > 0) {

            $new_model = $payments;

            $i = 1;
            $previous_palance = 0;
            $updated_balance = 0;
            $new_array = [];
            $final_balance = 0;
            foreach ($new_model as $item) {

                if ($i == '1') {
                    if ($item->payment_type == 'direct') {
                        if ($item->transaction_type == 'negative')
                            $updated_balance = str_replace("-", "", $item->total_modified_amount);
                        else
                            $updated_balance = '-' . $item->total_modified_amount;
                    } else
                        $updated_balance = $item->total_modified_amount;
                } else {
                    $previous_palance = $updated_balance;
                    if ($item->payment_type == 'direct') {

                        if ($item->transaction_type == 'negative')
                            $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);
                        else
                            $updated_balance = $updated_balance - $item->total_modified_amount;
                    } else
                        $updated_balance = $updated_balance + $item->total_modified_amount;
                }

                $final_balance = $updated_balance;
                $new_array[] = $item;

                $i++;
            }

            $final_array = [];
            $previous_palance = 0;
            $updated_balance = $final_balance;
            $i = 1;
            foreach ($new_array as $item) {
                if ($item->transaction_type == 'negative' && $item->payment_type == 'direct')
                    $item->total_modified_amount = $item->total_modified_amount;
                else
                    $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);

                if ($i == '1') {
                    $updated_balance = $final_balance;

                    if ($item->payment_type == 'invoice')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;;
                } else {
                    $updated_balance = $previous_palance;
                    if ($item->payment_type == 'invoice')
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    else
                        $previous_palance = $updated_balance + $item->total_modified_amount;
                }

                $item->new_previous_balance = round($previous_palance, 2);
                $item->new_updated_balance = round($updated_balance, 2);
                if ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'credit_memo')
                    $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                elseif ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                    $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);
                else
                    $item->new_total_modified_amount = $item->total_modified_amount;


                $final_balance = $updated_balance;
                $final_array[] = $item;
                $i++;
            }
        }

        $payments = $final_array;

        $user_info = 'All Customers';
        $date_info = '.';
        $request->customer_id = $this->customer_id;
        if ($request->customer_id != '0') {
            $client = Customers::where('id', $request->customer_id)->first();
            $user_info = $client->name . '(' . $client->email . ')';
        }
        if ($date != 'Select Date Range')
            $date_info = ' from Date Range ' . $date . '.';
        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        $start = $start_date_for_array_searching;
        $end = $end_date_for_array_searching;

        $result = array_filter($payments, function ($value) use ($start, $end) {
            return $start <= $value['statement_date'] && $value['statement_date'] <= $end;
        });

        $model = $result;
        return view('front.reports.customer_statement_report', compact('model', 'customers', 'report_type', 'customer_info', 'date', 'print_page_info'));
    }

}
