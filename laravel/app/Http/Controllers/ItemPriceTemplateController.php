<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\PriceTemplates;
use App\ItemPriceTemplates;
use App\Customers;
use App\Clients;
use App\Categories;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class ItemPriceTemplateController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Payment Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $model = PriceTemplates::where('deleted', 0)->where('user_type', 'admin')
                ->orderBy('id', 'desc')
                ->get();

        return view('front.item_price_templates.index', compact('model'));
    }

    public function create() {

        return view('front.item_price_templates.create');
    }

    public function edit($id) {

        $model = PriceTemplates::where('id', '=', $id)->where('deleted', '=', '0')->first();

        if (count($model) == 0) {
            return redirect('/');
        }
        return view('front.item_price_templates.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'title' => 'required|min:1',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $template = new PriceTemplates;
        $template->title = $request->title;
        $template->description = $request->description;
        $template->price_percentage = $request->price_percentage;
        $template->created_by = $user_id;
        $template->save();

        if (isset($template->id)) {
            return redirect('/item-price-template/' . $template->id);
            Session::flash('success', 'Price template is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Price template is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $validation = array(
            'title' => 'required|min:1',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $payment_id = $request->id;
        $input = $request->all();
//        print_r($input); die;
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        PriceTemplates::where('id', '=', $payment_id)->update($input);
        Session::flash('success', 'Price template has been updated.');
        return redirect()->back();
    }

    public function delete($id) {

        $customer = Customers::where('selling_price_template', $id)->get();
        if (count($customer) > 0) {
            Session::flash('error', 'Cannot deleted because this template is using for some customer.');
            return redirect()->back();
        }
        PriceTemplates::where('id', '=', $id)->where('is_default','0')->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id) {

        $template = PriceTemplates::where('id', $id)->first();
        $template_name = ucwords($template->title);

        $template_items = Items::where('items.deleted', '=', '0')
                        ->where('items.status', '=', '1')
                        ->where('items.parent_item_id', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $id)
                        ->where('items.category_id', 1)
                        ->select('items.*', 'c.name as category_name', 'ipt.selling_price as selling_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                        ->orderBy('items.id', 'desc')
                        ->take(5)->get()->toArray();

        $all_items = Items::where('items.deleted', '=', '0')
                ->where('items.parent_item_id', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->where('items.category_id', 1)
                        ->where('items.status', '=', '1')
                        ->select('items.*', 'c.name as category_name')
                        ->orderBy('items.id', 'desc')
                        ->take(5)->get()->toArray();


        $merged = array_merge($template_items, $all_items);
        $price = array();
        foreach ($merged as $key => $row) {
            $price[$key] = $row['id'];
        }

        array_multisort($price, SORT_ASC, $merged);
        $new_merged = [];
        foreach ($merged as $item) {
            $new_merged[$item['id']] = $item;
        }
        $merged = [];
        foreach ($new_merged as $item) {
            $merged[] = $item;
        }

        $model = $merged;

        $template_id = $id;
        $categories = Categories::where('deleted', '=', '0')
                        ->pluck('name', 'id')->toArray();
        $categories[0] = 'All Categories';
        $category_id = 1;
        $display_type = '0';
        $profit_type = '0';
        $item_name = '';

        return view('front.item_price_templates.detail', compact('model', 'categories', 'category_id', 'template_id', 'display_type', 'profit_type', 'item_name', 'template_name'));
    }

    public function priceUpdate(Request $request) {

        $input = $request->all();

        $item_id = $input['item_id'];
        $price_template_id = $input['price_template_id'];
        $selling_price = $input['selling_price'];
        $profit = ($selling_price) - ($input['cost']);
        $display = $input['display'];

        self::updateItemPrice($item_id, $price_template_id, $selling_price, $profit, $display);
        $template = PriceTemplates::where('id', $input['price_template_id'])->first();

        if($template->is_default == 3){
            $image_name = '';
            $gallery = [];
            $item = Items::where('id', $item_id)->where('deleted', '0')->first();
            $type = 'edit';
            $gl_imgs = [];
        
            $g_imgs = GalleryImages::where('item_id',$item_id)->where('deleted',0)->get();
            
            $gl_imgs[] = $item->image;
            foreach($g_imgs as $row){
                $gl_imgs[] = $row->image;
            }
            $sourc = 'ItemPriceController::priceUpdate';
            ItemController::makecurlRequest($item,$selling_price,$gl_imgs,$item->code,$type,$item_id,$sourc);
        }
        $item_exist = Items::where('parent_item_id', $item_id)->where('deleted', '0')->get();
        if (count($item_exist) > 0) {
            foreach ($item_exist as $sub_item) {
                self::updateItemPrice($sub_item->id, $price_template_id, $selling_price, $profit, $display);
                if($template->is_default == 3){
                    $image_name = '';
                    $gallery = [];
                    $item = Items::where('id', $sub_item->id)->where('deleted', '0')->first();
                    $type = 'edit';
                    $gl_imgs = [];
        
                    $g_imgs = GalleryImages::where('item_id',$item_id)->where('deleted',0)->get();
                    
                    $gl_imgs[] = $item->image;
                    foreach($g_imgs as $row){
                        $gl_imgs[] = $row->image;
                    }
                    $sourc = 'ItemPriceController::priceUpdate::if (count($item_exist) > 0)';
                    ItemController::makecurlRequest($item,$selling_price,$gl_imgs,$item->code,$type,$item_id);
                }
            }
        }


        $message = 'Item# ' . $item_id . '  has been updated sucessfully';
        
        return $message;
        die;
    }

    public function search(Request $request) {

        $template = PriceTemplates::where('id', $request->id)->first();
        $template_name = ucwords($template->title);

        $model = Items::where('items.deleted', '=', '0')
                ->where('items.status', '=', '1')
                ->where('items.parent_item_id', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $request->id);
        if ($request->category_id != '0')
            $model = $model->where('items.category_id', $request->category_id);
        if ($request->profit_type == '1')
            $model = $model->where('ipt.profit', 'LIKE', '%-%');
        if ($request->display_type != '0')
            $model = $model->where('ipt.display', ($request->display_type - 1));
        if ($request->item_name != '')
            $model = $model->where('items.name', 'LIKE', "%" . $request->item_name . "%");
        $model = $model->select('items.*', 'c.name as category_name', 'ipt.selling_price as selling_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                        ->orderBy('items.id', 'desc')
                        ->get()->toArray();

        if ($request->profit_type == '0' && $request->display_type == '0') {

            $all_items = Items::where('items.deleted', '=', '0')->where('items.parent_item_id', '=', '0')
                    ->where('items.status', '=', '1')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id');
            if ($request->category_id != '0')
                $all_items = $all_items->where('items.category_id', $request->category_id);
            if ($request->item_name != '')
                $all_items = $all_items->where('items.name', 'LIKE', "%" . $request->item_name . "%");
            $all_items = $all_items->select('items.*', 'c.name as category_name')
                            ->orderBy('items.id', 'desc')
                            ->get()->toArray();


            $merged = array_merge($model, $all_items);
            $price = array();
            foreach ($merged as $key => $row) {
                $price[$key] = $row['id'];
            }

            array_multisort($price, SORT_ASC, $merged);
            $new_merged = [];
            foreach ($merged as $item) {
                $new_merged[$item['id']] = $item;
            }
            $merged = [];
            foreach ($new_merged as $item) {
                $merged[] = $item;
            }

            $model = $merged;
        } else if ($request->profit_type == '0' && $request->display_type == '1') {

            $my_items = ItemPriceTemplates::where('price_template_id', $request->id)->pluck('item_id')->toArray();
            $all_items = Items::where('items.deleted', '=', '0')->where('items.parent_item_id', '=', '0')
                    ->where('items.status', '=', '1')
                    ->where('items.parent_item_id', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->whereNotIn('items.id', $my_items);

            if ($request->category_id != '0')
                $all_items = $all_items->where('items.category_id', $request->category_id);
            if ($request->item_name != '')
                $all_items = $all_items->where('items.name', 'LIKE', "%" . $request->item_name . "%");

            $all_items = $all_items->select('items.*', 'c.name as category_name')
                            ->orderBy('items.id', 'desc')
                            ->get()->toArray();


            $merged = array_merge($model, $all_items);
//            dd($merged);
            $price = array();
            foreach ($merged as $key => $row) {
                $price[$key] = $row['id'];
            }

            array_multisort($price, SORT_ASC, $merged);
            $new_merged = [];
            foreach ($merged as $item) {
                $new_merged[$item['id']] = $item;
            }
            $merged = [];
            foreach ($new_merged as $item) {
                $merged[] = $item;
            }

            $model = $merged;
        }

        $template_id = $request->id;
        $profit_type = $request->profit_type;
        $categories = Categories::where('deleted', '=', '0')
                        ->pluck('name', 'id')->toArray();
        $categories[0] = 'All Categories';
        $category_id = $request->category_id;
        $display_type = $request->display_type;
        $item_name = $request->item_name;

        return view('front.item_price_templates.detail', compact('model', 'categories', 'category_id', 'template_id', 'display_type', 'profit_type', 'item_name', 'template_name'));
    }

    public function priceUpdateBulk(Request $request) {

        $input = $request->all();
        
        $selected_items = explode(',', $input['bulk_item_id']);
        $price_template_id = $input['bulk_template_id'];
        $selling_price = $input['bulk_selling_price'];
        $percent = $input['bulk_cost_percentage'];
        $added_amount = $input['bulk_added_amount'];
        $added_amount_in_cost = $input['bulk_added_cost_amount'];
        $bulk_display = $input['bulk_display'];
        $item_selling_price = '';

        if ($input['bulk_item_id'] == '') {

            $message = 'Please select atleast 1 item.';
            Session::flash('error', $message);
            return redirect()->back();
        }

        if ($selling_price == '' && $percent == '' && $input['bulk_display'] == '0' && $added_amount == '' && $added_amount_in_cost == '') {

            $message = 'Please do any change for update';
            Session::flash('error', $message);
            return redirect()->back();
        }

        foreach ($selected_items as $item_detail) {

            $res = explode("_", "$item_detail");
            $cost = $res[1];
            $item_id = $res[0];
            $existing_selling_price = $res[2];
            $new_selling_price = $cost * $percent;

            if ($selling_price == '')
                $item_selling_price = $new_selling_price;
            else
                $item_selling_price = $selling_price;
            
            if($added_amount != '')
                $item_selling_price = $existing_selling_price + $added_amount;
            
            if($added_amount_in_cost != '')
                $item_selling_price = $cost + $added_amount_in_cost;

            self::bulkUpdateItemPrice($item_id, $price_template_id, $item_selling_price, $bulk_display);
            $template = PriceTemplates::where('id', $price_template_id)->first();

            if($template->is_default == 3){
                $image_name = '';
                $gallery = [];
                $item0 = Items::where('id', $item_id)->where('deleted', '0')->first();
                $type = 'edit';
                $gl_imgs = [];
        
                $g_imgs = GalleryImages::where('item_id',$item_id)->where('deleted',0)->get();
                
                $gl_imgs[] = $item0->image;
                foreach($g_imgs as $row){
                    $gl_imgs[] = $row->image;
                }
                $sourc = 'ItemPriceController::priceUpdateBulk';
                ItemController::makecurlRequest($item0,$item_selling_price,$gl_imgs,$item0->code,$type,$item_id,$sourc);
            }

            $item_exist = Items::where('parent_item_id', $item_id)->where('deleted', '0')->get();
            if (count($item_exist) > 0) {
                foreach ($item_exist as $sub_item) {

                    self::bulkUpdateItemPrice($sub_item->id, $price_template_id, $item_selling_price, $bulk_display);
                    if($template->is_default == 3){
                        $image_name = '';
                        $gallery = [];
                        $item1 = Items::where('id', $sub_item->id)->where('deleted', '0')->first();
                        $type = 'edit';
                        $gl_imgs = [];
        
                        $g_imgs = GalleryImages::where('item_id',$item_id)->where('deleted',0)->get();
                        
                        $gl_imgs[] = $item1->image;
                        foreach($g_imgs as $row){
                            $gl_imgs[] = $row->image;
                        }
                        $sourc = 'ItemPriceController::priceUpdateBulk::if (count($item_exist) > 0)';
                        ItemController::makecurlRequest($item1,$item_selling_price,$gl_imgs,$item1->code,$type,$item_id,$sourc);
                    }
                }
            }
        }

        $message = 'Items has been updated sucessfully';
        Session::flash('success', $message);
        return redirect('item-price-template/' . $price_template_id);
        return redirect()->back();
    }

    public function copy($template_id) {

        $old_template = PriceTemplates::where('id', '=', $template_id)->where('deleted', '=', '0')->where('user_type', 'admin')->first();
        $user_id = Auth::user()->id;

        if ($old_template->user_type != 'admin') {
            Session::flash('error', 'Unable to copy this template.');
            return redirect()->back();
        }
        $old_template_items = ItemPriceTemplates::where('price_template_id', $template_id)->get();

        DB::beginTransaction();
        $template = new PriceTemplates;
        $template->title = $old_template->title . '_copy';
        $template->description = $old_template->description;
        $template->created_by = $old_template->created_by;
        $template->save();
        $new_template_id = $template->id;

        if (isset($template->id) && count($old_template_items) > 0) {
            foreach ($old_template_items as $items) {
                $template_items = new ItemPriceTemplates;
                $template_items->item_id = $items->item_id;
                $template_items->price_template_id = $new_template_id;
                $template_items->selling_price = $items->selling_price;
                $template_items->profit = $items->profit;
                $template_items->display = $items->display;
                $template_items->save();
            }
        } else {

            DB::rollBack();
            Session::flash('error', 'Invoice is not created. Please add atleast 1 item.');
            return redirect()->back();
        }

        DB::commit();
        $message = 'Template # ' . $template_id . ' copied successfully';
        $path = 'item-price-template/' . $new_template_id;
        Session::flash('success', $message);
        return redirect($path);
    }

    private function updateItemPrice($item_id, $price_template_id, $selling_price, $profit, $display) {

        $item_exist = ItemPriceTemplates::where('item_id', $item_id)->where('price_template_id', $price_template_id)->get();

        if (count($item_exist) == '0') {
            $template = new ItemPriceTemplates;
            $template->item_id = $item_id;
            $template->price_template_id = $price_template_id;
            $template->selling_price = $selling_price;
            $template->profit = $profit;
            $template->display = $display;
            $template->save();
        } else
            $a = ItemPriceTemplates::where('id', $item_exist[0]->id)->update(['selling_price' => $selling_price, 'display' => $display, 'profit' => $profit]);
    }

    private function bulkUpdateItemPrice($item_id, $price_template_id, $item_selling_price, $bulk_display) {

        $model = ItemPriceTemplates::where('price_template_id', $price_template_id)->where('item_id', $item_id)->first();
        $item = Items::where('id', $item_id)->first();

        $profit = $item_selling_price - $item->cost;
        if (count($model) == 0) {
            $template = new ItemPriceTemplates;
            $template->item_id = $item_id;
            $template->price_template_id = $price_template_id;
            if ($item_selling_price != '')
                $template->selling_price = $item_selling_price;
            $template->profit = $profit;
            if ($bulk_display != '0')
                $template->display = $bulk_display - 1;
            $template->save();
        } else {
            if ($bulk_display == '0') {
                ItemPriceTemplates::where('id', $model->id)->update(['selling_price' => $item_selling_price, 'profit' => $profit]);
            } else if ($item_selling_price == '')
                ItemPriceTemplates::where('id', $model->id)->update(['display' => ($bulk_display - 1)]);
            else {
                ItemPriceTemplates::where('id', $model->id)->update(['selling_price' => $item_selling_price, 'display' => ($bulk_display - 1), 'profit' => $profit]);
            }
        }
    }

}
