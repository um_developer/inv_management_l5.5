<?php

namespace App\Http\Controllers;

use DB;
use App\Sizes;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class SizeController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Categories Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
    }

    public function index() {

        $sizes = Sizes::where('deleted', '0')->get();
        return view('front.sizes.index', compact('sizes'));
    }

    public function create() {

        return view('front.sizes.create');
    }

    public function edit($id) {

        $size = Sizes::where('id', '=', $id)->where('deleted', '0')->get();

        if (count($size) == 0) {
            return redirect('/');
        }
        $model = $size[0];
        return view('front.sizes.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'title' => 'required|max:40|unique:sizes',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $size = new Sizes;
        $size->title = $request->title;
        $size->created_by = $user_id;
        $size->save();

        if (isset($size->id)) {
            Session::flash('success', 'Size is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Size is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $size_id = $request->id;
        $validation = array(
            'title' => 'required|max:40|unique:sizes,title,' . $size_id,
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }


        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        Sizes::where('id', '=', $size_id)->update($input);
        Session::flash('success', 'Size has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        Sizes::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
