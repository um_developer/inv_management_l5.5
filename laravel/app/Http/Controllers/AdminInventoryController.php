<?php

namespace App\Http\Controllers;

use DB;
use App\Vendors;
use App\Inventory;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\WarehousePackages;
use App\WarehouseInventory;
use App\Warehouses;
use Auth;
use App\InventoryUpdateLogs;
use Session;
use Validator,
    Input,
    Redirect;
use Excel;

class AdminInventoryController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Admin Inventory Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
    }

    public function index1() {

        $warehouses = Warehouses::where('deleted', '=', '0')
                ->get();

        $all_packages[0] = 'Select Warehouse';
        $all_packages['admin'] = 'Admin Inventory';
        foreach ($warehouses as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $warehouses = $all_packages;

        $model = Inventory::where('inventory.deleted', '=', '0')
               // ->where('quantity', '!=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')              
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'i.cost as ro_item_cost')
                ->groupBy('i.id')->get()
                ->toArray();

//        print_r($model); die;

        $model1 = WarehouseInventory::where('warehouse_inventory.deleted', '=', '0')
                       // ->where('quantity', '!=', '0')
                        ->leftjoin('items as i', 'i.id', '=', 'warehouse_inventory.item_id')
//                        ->leftjoin('ro_items as roi', 'roi.item_id', '=', 'warehouse_inventory.item_id')
                        ->leftjoin('warehouses as w', 'w.id', '=', 'warehouse_inventory.warehouse_id')
                        ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                        ->select('warehouse_inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'w.name as warehouse_name', 'i.cost as ro_item_cost')
                        ->groupBy('i.id')->get()->toArray();

        $merged = array_merge($model, $model1);

        $price = array();
        foreach ($merged as $key => $row) {
            $price[$key] = $row['item_name'];
        }
        array_multisort($price, SORT_ASC, $merged);
        $model = $merged;

        return view('front.inventory.index1', compact('model', 'warehouses'));
    }

    public function inventoryByItem($item_id) {

        $warehouses = Warehouses::where('deleted', '=', '0')
                ->get();

        $all_packages[0] = 'Select Warehouse';
        $all_packages['admin'] = 'Admin Inventory';
        foreach ($warehouses as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $warehouses = $all_packages;

        $model = Inventory::where('inventory.deleted', '=', '0')
                //->where('quantity', '!=', '0')
                ->where('item_id', $item_id)
                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku','i.cost as ro_item_cost')->get()
                ->toArray();

        $model1 = WarehouseInventory::where('warehouse_inventory.deleted', '=', '0')
                        //->where('quantity', '!=', '0')
                        ->where('item_id', $item_id)
                        ->leftjoin('items as i', 'i.id', '=', 'warehouse_inventory.item_id')
                        ->leftjoin('warehouses as w', 'w.id', '=', 'warehouse_inventory.warehouse_id')
                        ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                        ->select('warehouse_inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'w.name as warehouse_name','i.cost as ro_item_cost')
                        ->get()->toArray();

        $merged = array_merge($model, $model1);

        $price = array();
        foreach ($merged as $key => $row) {
            $price[$key] = $row['item_name'];
        }
        array_multisort($price, SORT_ASC, $merged);
        $model = $merged;
        return view('front.inventory.index1', compact('model', 'warehouses'));
    }

    public function index() {

        $model = Inventory::where('inventory.deleted', '=', '0')
                ->where('quantity', '!=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku')
                ->get();

        return view('front.inventory.index1', compact('model'));
    }

    public function generateCSVReport() {

        $result = Inventory::where('inventory.deleted', '=', '0')
                //->where('quantity', '!=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('inventory.id', 'i.name as item_name', 'c.name as category_name', 'i.code as item_sku', 'inventory.quantity')
                ->get();
        $file_name = "admin_inventory_report.csv";
        return Excel::create($file_name, function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download('csv');
    }

    public function getPackageByLocation(Request $request) {

        $warehouses = Warehouses::where('deleted', '=', '0')
                ->get();
        $id = $request->warehouse_id;
        $all_packages[0] = 'Select Warehouse';
        $all_packages['admin'] = 'Admin Inventory';
        $type = $request->type;
        foreach ($warehouses as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $warehouses = $all_packages;


        if ($id == 'admin') {
            $model = Inventory::where('inventory.deleted', '=', '0');

            if ($type == '0_quantity')
                $model = $model->where('inventory.quantity', '=', '0')->where('inventory.package_quantity', '=', '0');
            else
                $model = $model->where('inventory.quantity', '!=', '0');

            $model = $model->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                    ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
//                    ->leftjoin('ro_items as roi', 'roi.item_id', '=', 'inventory.item_id')
                    ->select('inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'i.cost as ro_item_cost')
                    ->groupBy('i.id')->get()
                    ->toArray();
            
//            print_r($model); die;
            
        } else if ($id == '0') {

            $model1 = Inventory::where('inventory.deleted', '=', '0');

            if ($type == '0_quantity')
                $model1 = $model1->where('inventory.quantity', '=', '0')->where('inventory.package_quantity', '=', '0');
//            else
//                $model1 = $model1->where('inventory.quantity', '!=', '0');

            $model1 = $model1->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                    ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
//                    ->leftjoin('ro_items as roi', 'roi.item_id', '=', 'inventory.item_id')
                    ->select('inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'i.cost as ro_item_cost')
                    ->groupBy('i.id')->get()
                    ->toArray();

            $model = WarehouseInventory::where('warehouse_inventory.deleted', '=', '0');
            
            if ($type == '0_quantity')
                $model = $model->where('quantity', '=', '0');
//            else
//                $model = $model->where('quantity', '!=', '0');


            $model = $model->leftjoin('items as i', 'i.id', '=', 'warehouse_inventory.item_id')
                            ->leftjoin('warehouses as w', 'w.id', '=', 'warehouse_inventory.warehouse_id')
//                            ->leftjoin('ro_items as roi', 'roi.item_id', '=', 'warehouse_inventory.item_id')
                            ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                            ->select('warehouse_inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'w.name as warehouse_name', 'i.cost as ro_item_cost')
                            ->groupBy('i.id')->get()->toArray();

            $merged = array_merge($model, $model1);
            $price = array();
            foreach ($merged as $key => $row) {
                $price[$key] = $row['item_name'];
            }
            array_multisort($price, SORT_ASC, $merged);

            $model = $merged;
        } else {
            $model = WarehouseInventory::where('warehouse_inventory.deleted', '=', '0');

            if ($type == '0_quantity')
                $model = $model->where('quantity', '=', '0');
//            else
//                $model = $model->where('quantity', '!=', '0');

            $model = $model->where('warehouse_id', $id)
                            ->leftjoin('items as i', 'i.id', '=', 'warehouse_inventory.item_id')
//                            ->leftjoin('ro_items as roi', 'roi.item_id', '=', 'warehouse_inventory.item_id')
                            ->leftjoin('warehouses as w', 'w.id', '=', 'warehouse_inventory.warehouse_id')
                            ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                            ->select('warehouse_inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'w.name as warehouse_name', 'i.cost as ro_item_cost')
                           ->groupBy('i.id') ->get()->toArray();
        }
        return view('front.inventory.index1', compact('model', 'warehouses'));
    }

    public function updateInventoryQuantity($item_id, $quantity) {

        $model = Inventory::where('inventory.deleted', '=', '0')->where('item_id', $item_id)->first();
        $user_id = Auth::user()->id;
        $old_quantity = $model->quantity;

        if (count($model) > 0) {
            Inventory::where('inventory.deleted', '=', '0')->where('item_id', $item_id)->update(['quantity' => $quantity]);

            $model = new InventoryUpdateLogs;
            $model->item_id = $item_id;
            $model->quantity = $quantity;
            $model->old_quantity = $old_quantity;
            $model->updated_by = $user_id;
            $model->save();
        }

        $message = 'Item quantity is updated.';
        return $message;
    }

    public function updateInventoryLogs() {


        $model = InventoryUpdateLogs::leftjoin('items as i', 'i.id', '=', 'inventory_update_logs.item_id')
                        ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                        ->select('inventory_update_logs.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku')->get();


        return view('front.inventory.logs', compact('model'));
    }

}
