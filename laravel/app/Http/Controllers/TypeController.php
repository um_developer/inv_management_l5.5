<?php

namespace App\Http\Controllers;

use DB;
use App\Types;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class TypeController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Categories Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
    }

    public function index() {

        $types = Types::where('deleted', '0')->get();
        return view('front.types.index', compact('types'));
    }

    public function create() {

        return view('front.types.create');
    }

    public function edit($id) {

        $type = Types::where('id', '=', $id)->where('deleted', '0')->get();

        if (count($type) == 0) {
            return redirect('/');
        }
        $model = $type[0];
        return view('front.types.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'title' => 'required|max:40|unique:types',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $type = new Types;
        $type->title = $request->title;
        $type->created_by = $user_id;
        $type->save();

        if (isset($type->id)) {
            Session::flash('success', 'Type is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Type is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $type_id = $request->id;
        $validation = array(
            'title' => 'required|max:40|unique:types,title,' . $type_id,
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        Types::where('id', '=', $type_id)->update($input);
        Session::flash('success', 'Types has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        Types::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
