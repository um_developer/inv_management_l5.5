<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\PoItems;
use App\Inventory;
use App\PurchaseOrders;
use App\VendorRoPayments;
use App\ReceiveOrders;
use App\RoItems;
use App\ItemPriceImpect;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class ReceiveOrderController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | ReceiveOrder Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
//        $this->middleware('auth');
//        if(Auth::user()->role_id != '2'){
//            Redirect::to('customer/dashboard')->send(); die;
//        }
    }

    public function index() {

        $model = ReceiveOrders::where('receive_orders.deleted', '=', '0')
                ->leftjoin('vendors as v', 'v.id', '=', 'receive_orders.vendor_id')
                ->select('receive_orders.*', 'v.name as vendor_name', 'v.u_id as vendor_u_id')
                ->orderBy('id', 'desc')
                ->get();

        return view('front.receive_order.index', compact('model'));
    }

    public function create() {

        $vendors = Vendors::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat['0'] = '--- Select Vendor ---';
        foreach ($vendors as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $vendors = $new_cat;
        return view('front.receive_order.create', compact('vendors'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $purchase_order_id = $request->po_id;
        $vendor_id = $request->vendor_id;

        $purchase_order = PurchaseOrders::where('id', '=', $input['po_id'])->where('status', '!=', 'closed')->get();

        if (count($purchase_order) == 0) {
            Session::flash('error', 'Your selected purchase order is closed now.');
            return redirect()->back();
        }

        $items = $input['num'];
        DB::beginTransaction();

        $receive_order = new ReceiveOrders;
        $receive_order->created_by = $user_id;
        $receive_order->vendor_id = $vendor_id;
        $timestamp = strtotime($request->receive_date);
        $receive_order->receive_date = date("Y-m-d H:i:s", $timestamp);
        $receive_order->po_id = $purchase_order_id;
        $receive_order->save();
//        $receive_order->id = 1;

        $total_cost = 0;
        $total_quantity = 0;
        $save_ro = 0;

        if (count($items) > 0 && isset($receive_order->id)) {
            for ($i = 1; $i <= count($items); $i++) {

                ////// if ro item quantity greater than 1 /////
                if ($input['quantity_' . $i] != '0') {

                    $po_item = PoItems::where('id', '=', $input['po_item_id_' . $i])->get();
                    $ro_items = new RoItems;
                    $ro_items->po_id = $po_item[0]->po_id;
                    $ro_items->ro_id = $receive_order->id;
                    $ro_items->po_item_id = $input['po_item_id_' . $i];
                    $ro_items->item_id = $po_item[0]->item_id;
                    $ro_items->item_unit_price = $po_item[0]->item_unit_price;
                    $ro_items->total_price = $po_item[0]->item_unit_price * $input['quantity_' . $i];
                    $ro_items->received_quantity = $input['quantity_' . $i];
                    $ro_items->created_by = $user_id;
                    $ro_items->vendor_id = $vendor_id;
                    $total_cost += $ro_items->total_price;
                    $total_quantity += $ro_items->received_quantity;
                    $ro_items->save();

                    if (isset($ro_items->id)) {
                        $row = 0;
                        $data = PoItems::where('id', '=', $input['po_item_id_' . $i])->get();
                        $row = $data[0]->delivered_quantity + $input['quantity_' . $i];
                        PoItems::where('id', '=', $po_item[0]->id)->update([
                            'delivered_quantity' => $row,
                        ]);

                        self::updateAdminInventory($po_item[0]->item_id, $input['quantity_' . $i]);
                        ///////Update item cost

                        $main_item = Items::where('id', $po_item[0]->item_id)->first();

//                         if($po_item[0]->item_unit_price  == $main_item->cost){
// echo $po_item[0]->item_id; die;
//                         }

                        if($po_item[0]->item_unit_price > $main_item->cost){

                            $item_price_impect = new ItemPriceImpect;
                            $item_price_impect->diff_amount = $po_item[0]->item_unit_price - $main_item->cost;
                            $item_price_impect->amount = $po_item[0]->item_unit_price;
                            $item_price_impect->old_price = $main_item->cost;
                            $item_price_impect->item_id = $po_item[0]->item_id;
                            $item_price_impect->impact = 'positive';
                            $item_price_impect->ro_id = $ro_items->ro_id;
                            $item_price_impect->po_id = $po_item[0]->id;
                            $item_price_impect->deleted = 0;
                            $item_price_impect->status = 'pending';
                            $item_price_impect->save();
                        }else if($po_item[0]->item_unit_price < $main_item->cost){

                            $item_price_impect = new ItemPriceImpect;
                            $item_price_impect->diff_amount = $main_item->cost - $po_item[0]->item_unit_price;
                            $item_price_impect->amount = $po_item[0]->item_unit_price;
                            $item_price_impect->old_price = $main_item->cost;
                            $item_price_impect->item_id = $po_item[0]->item_id;
                            $item_price_impect->impact = 'negative';
                            $item_price_impect->ro_id = $ro_items->ro_id;
                            $item_price_impect->po_id = $po_item[0]->id;
                            $item_price_impect->deleted = 0;
                            $item_price_impect->status = 'pending';
                            $item_price_impect->save();
                        }

                        Items::where('id', $po_item[0]->item_id)->update([
                            'cost' => $po_item[0]->item_unit_price,
                        ]);
                        
                        if($main_item->parent_item_id != '0'){
                         Items::where('parent_item_id', $main_item->parent_item_id)->update([
                            'cost' => $po_item[0]->item_unit_price,
                        ]);
                         
                        }
                    }

                    $save_ro = 1;
                }
            }

            if ($save_ro == 1) {

                if (isset($ro_items->id)) {
                    $row = 0;
                    $data = PurchaseOrders::where('id', '=', $purchase_order_id)->get();
                    $row = $data[0]->delivered_quantity + $total_quantity;
//                        print_r($row); die;
                    PurchaseOrders::where('id', '=', $purchase_order_id)->update([
                        'delivered_quantity' => $row,
                        'status' => 'open',
                    ]);

                    $data = PurchaseOrders::where('id', '=', $purchase_order_id)->get();
                    if ($data[0]->total_quantity == $data[0]->delivered_quantity) {
                        PurchaseOrders::where('id', '=', $purchase_order_id)->update([
                            'status' => 'closed',
                        ]);
                    }
                }

                ReceiveOrders::where('id', '=', $receive_order->id)->update([
                    'received_quantity' => $total_quantity,
                    'total_price' => $total_cost,
                ]);

                $vendor_ro_payment = new VendorRoPayments;
                $vendor_ro_payment->vendor_id = $receive_order->vendor_id;
                $vendor_ro_payment->ro_id = $receive_order->id;
                $vendor_ro_payment->amount = $total_cost;
                $vendor_ro_payment->save();

                self::updateVendorBalance($vendor_id, $total_cost, 'add');

                DB::commit();
            } else {
                DB::rollBack();
                Session::flash('error', 'Receive Order is not created. Please add alteast 1 quantity for any item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Purchase Order is not created. Please try again.');
            return redirect()->back();
        }

        Session::flash('success', 'Receive Order is created successfully');
        return redirect()->back();
    }

    public function detail($id) {

        $model = ReceiveOrders::where('receive_orders.id', '=', $id)
                ->leftjoin('vendors as v', 'v.id', '=', 'receive_orders.vendor_id')
                ->where('receive_orders.deleted', '=', '0')
                ->select('receive_orders.*', 'v.id as vendor_id', 'v.name as vendor_name')
                ->get();

        $po_items = RoItems::where('ro_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'ro_items.item_id')
                ->where('ro_items.deleted', '=', '0')
                ->select('ro_items.*', 'i.name as item_name')
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.receive_order.detail', compact('model', 'po_items'));
    }

    public function printPage($id) {

        $model = ReceiveOrders::where('receive_orders.id', '=', $id)
                ->leftjoin('vendors as v', 'v.id', '=', 'receive_orders.vendor_id')
                ->where('receive_orders.deleted', '=', '0')
                ->select('receive_orders.*', 'v.id as vendor_id', 'v.name as vendor_name')
                ->get();

        $po_items = RoItems::where('ro_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'ro_items.item_id')
                ->where('ro_items.deleted', '=', '0')
                ->select('ro_items.*', 'i.name as item_name')
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.receive_order.print', compact('model', 'po_items'));
    }

    public function getopenPoByVendor($vendor_id) {
        $purchase_orders = DB::table('purchase_orders')->where('vendor_id', $vendor_id)->where('status', '!=', 'closed')->where('deleted', '0')->select("id")->get();

        $new_cat = [];
        foreach ($purchase_orders as $po) {
            $new_cat[$po->id] = $po->id;
        }
        $purchase_orders = $new_cat;

        $data = view('front.common.ajax_po', compact('purchase_orders'))->render();
        return $data;
    }

    public function getPoDetail($po_id) {

        $po_items = PoItems::where('po_id', '=', $po_id)
                ->leftjoin('items as i', 'i.id', '=', 'po_items.item_id')
                ->where('po_items.deleted', '=', '0')
                ->where('po_items.quantity', '!=', 'po_items.delivered_quantity')
                ->select('po_items.*', 'i.name as item_name')
                ->get();

        $data = view('front.common.ajax_po_items', compact('po_items'))->render();
//        print_r($data);
//        die;
        return $data;
    }

    public function delete($id) {

        $model = ReceiveOrders::where('receive_orders.id', '=', $id)->get();
        $received_quantity = $model[0]->received_quantity;
        $total_price = $model[0]->total_price;
        $purchase_order_id = $model[0]->po_id;
        $vendor_id = $model[0]->vendor_id;


        DB::beginTransaction();

        $ro_items = RoItems::where('ro_id', '=', $id)->get();

        foreach ($ro_items as $ro_item) {

            $row = 0;
            $po = PoItems::where('id', '=', $ro_item->po_item_id)->get();
            $row = $po[0]->delivered_quantity - (int) $ro_item->received_quantity;

            if ($row < 0) {
                DB::rollBack();
                Session::flash('error', 'There is not enough items in purchase order items.');
                return redirect('/receive-items');
            }

            PoItems::where('id', '=', $ro_item->po_item_id)->update([
                'delivered_quantity' => $row,
            ]);
            $update = self::updateAdminInventory($ro_item->item_id, $ro_item->received_quantity, 'delete');
            self::updateVendorBalance($vendor_id, $total_price, 'subtract');
            if ($update == 'false') {
                DB::rollBack();
                Session::flash('error', 'There is not enough items in inventory.');
                return redirect('/receive-items');
            }
        }

        $data = PurchaseOrders::where('id', '=', $purchase_order_id)->get();
        $row = $data[0]->delivered_quantity - $received_quantity;
        if ($row < 0) {
            DB::rollBack();
            Session::flash('error', 'There is not enough items in purchase order.');
            return redirect('/receive-items');
        }
        PurchaseOrders::where('id', '=', $purchase_order_id)->update([
            'delivered_quantity' => $row,
            'status' => 'open',
        ]);
//
        ReceiveOrders::where('id', '=', $id)->update(['deleted' => 1]);

        $data = PurchaseOrders::where('id', '=', $purchase_order_id)->get();
        if ($data[0]->delivered_quantity == '0') {
            PurchaseOrders::where('id', '=', $purchase_order_id)->update([
                'status' => 'draft',
            ]);
        }



        DB::commit();
//        DB::rollBack();
        Session::flash('success', 'Successfully Deleted!');
        return redirect('/receive-items');
    }

    function updateAdminInventory($item_id, $quantity, $type = 'add') {
        $item = Inventory::where('item_id', $item_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->save();
            }
        } else {
            $total_quantity = $item[0]->quantity - $quantity;
            Inventory::where('item_id', '=', $item_id)->update([
                'quantity' => $total_quantity,
            ]);
        }
    }

    function updateVendorBalance($vendor_id, $amount, $type = 'subtract') {

        $vendor = Vendors::where('id', $vendor_id)->get();

        if ($type == 'subtract')
            $final_amount = $vendor[0]->balance - $amount;
        else
            $final_amount = $vendor[0]->balance + $amount;

        Vendors::where('id', $vendor_id)->update(['balance' => $final_amount]);
    }

    public function getPriceImpect(){
        $model = ItemPriceImpect::where('item_price_impact.deleted',0)->leftjoin('items','items.id','=','item_price_impact.item_id')
        ->select('item_price_impact.*','items.name')
        ->orderBy('item_price_impact.id', 'desc')->get();
        $status = '';
        return view('front.receive_order.priceImpect', compact('model','status'));
    }
    public function searchPriceImpect(Request $request){
        $model = ItemPriceImpect::where('item_price_impact.deleted',0)->where('item_price_impact.status',$request->status)->leftjoin('items','items.id','=','item_price_impact.item_id')
        ->select('item_price_impact.*','items.name')
        ->orderBy('item_price_impact.id', 'desc')->get();

        $status = $request->status;
        return view('front.receive_order.priceImpect', compact('model','status'));
    }

    public function priceImpectCreate(Request $request){

        if(count($request->item_id) == 0){
            Session::flash('error', 'Select atleast one item');
            return redirect()->back();
        }
        if(empty($request->diff)){
            Session::flash('error', 'Please add difference');
            return redirect()->back();
        }
        if(empty($request->effect)){
            Session::flash('error', 'Select Effect');
            return redirect()->back();
        }

        DB::beginTransaction();        
        try{
            if (count($request->item_id) > 0) {
                foreach ($request->item_id as $row) {
                    $item_price_impect = new ItemPriceImpect;
                    $item_price_impect->diff_amount = $request->diff;
                    $item_price_impect->item_id = $row;
                    if ($request->effect == 1) {
                        $item_price_impect->impact = 'positive';
                    }else if(($request->effect == 2)){
                        $item_price_impect->impact = 'negative';
                    }
                    $item_price_impect->deleted = 0;
                    $item_price_impect->status = 'pending';
                    $item_price_impect->save();
                }
                DB::commit();
                Session::flash('success', 'Items are Sussessfully Add in Price Impect');
                return redirect('/priceImpect');
            }else{
                Session::flash('error', 'Select atleast one item');
                DB::rollBack();
                return redirect()->back();
            }
        }catch(Exception $e) {
            DB::rollBack();
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

}
