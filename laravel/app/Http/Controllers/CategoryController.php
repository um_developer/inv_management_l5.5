<?php

namespace App\Http\Controllers;

use DB;
use App\Categories;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;


class CategoryController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Categories Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
         parent::__construct();
         if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
    }

    public function index() {

       $categories = Categories::where('deleted', '=', '0')->get();
        return view('front.category.index', compact('categories'));
    }

    public function create() {

        return view('front.category.create');
    }
    
    
    public function edit($id) {
        
        $category = Categories::where('id', '=', $id)->where('deleted', '=', '0')->get();
        
        if(count($category)==0){
            return redirect('/');
        }
        $category=$category[0];
        return view('front.category.edit', compact('category'));
    }
    
    
    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'name' => 'required|max:40',
            
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }
        
        $category = new Categories;
        $category->name = $request->name;
        $category->created_by = $user_id;
        $category->save();
        
        if(isset($category->id)){
        Session::flash('success', 'Category is created successfully');
        return redirect()->back();
        } else{
              Session::flash('error', 'Category is not created. Please try again.');
             return redirect()->back();
            
        }
    }
    
     public function postUpdate(Request $request) {

        $validation = array(
            'name' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }

        $category_id = $request->id;
        $input = $request->all();
            array_forget($input, '_token');
            array_forget($input, 'id');
             array_forget($input, 'submit');
          
            Categories::where('id', '=', $category_id)->update($input);
            Session::flash('success', 'Category has been updated.');
            return redirect()->back();
        
       
    }
    
        public function delete($id) {
        Categories::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }
    
    
    
    

   

}
