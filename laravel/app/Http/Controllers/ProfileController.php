<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\User;
use App\Countries;
use App\States;
use App\Address;
use Auth;
use Hash;
use Session;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ProfileController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        $this->middleware('auth');
        
        if(isset(Auth::user()->id)){
              $role_id = Auth::user()->role_id;
              if($role_id != '2'){
                  Auth::logout();
                  \Session::flush();
                  print view('front.customers.login'); die;
              }
 }
 
    }

    public function changepassword() {
        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
		$breadcrumb=array();
        $breadcrumb['banner_title'] = 'CHANGE PASSWORD';
		$breadcrumb['b1'] = 'Home';
		$breadcrumb['b1_link'] = url('home');
		$breadcrumb['b2'] = 'Change password';
		$breadcrumb['b2_link'] = '';
        //die('sadasd');	   
        return view('front.customers.change_password', compact('breadcrumb'))->with('user_id', $user_id);
    }

    public function postchangepassword(Request $request) {
        $user_id = Auth::user()->id;
        $rules = array(
            'old_password' => 'required|min:6',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required_with:password|min:6',
        );
//'password'              => 'min:5|confirmed|different:now_password',
//'password_confirmation' => 'required_with:password|min:5'

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            //d($request->all(),1);
            return redirect('changepassword')->withErrors($validator->errors());
            //return redirect('changepassword')->withInput();
        }

        $user = User::findOrFail($user_id);

        if (password_verify($request->old_password, $user->password)) {
        
        $data = $request->all();
        array_forget($data, 'password_confirmation');
        array_forget($data, 'old_password');
        array_forget($data, '_token');
        $data['password'] = bcrypt($request->password);
        
//         $user = User::findOrFail($user_id);
//         print_r($data['password']);
//         echo '--';
//         print_r($user->password); die;
//         
//         if($user->password != $data['password']){
//             echo 'ff'; die;
//             Session::flash('error', 'Old password is Incorrect.');
//             die;
//             // return redirect()->back();
//             return redirect()->back()->withError('Incorrect old password', 'changepassword');
//         }

        $user->update($data);
        Session::flash('success', 'Your password has been changed.');
        return redirect()->back();
        }


        Session::flash('error', 'Old password is Incorrect.');
        return redirect()->back();
        return redirect()->back()->withError('Incorrect old password', 'changepassword');
    }

    public function profile() {

        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);

		$breadcrumb=array();
        $breadcrumb['banner_title'] = 'PROFILE';
		$breadcrumb['b1'] = 'Home';
		$breadcrumb['b1_link'] = url('home');
		$breadcrumb['b2'] = 'Profile';
		$breadcrumb['b2_link'] = '';
		
        return view('front.customers.profile', compact('user','breadcrumb'))->with('user_id', $user_id);
    }

    public function updateprofile(Request $request) {

        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
      
        $rules = array(
            'firstName' => 'required|max:50',
            'lastName' => 'required|max:50',
            'city' => 'required|min:2',
            'country' => 'required|max:50',
             'state' => 'required|max:50',
            'phone' => 'required|max:16',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        } else {
            $user = User::findOrFail($user_id);

            $data = $request->all();
            $input['firstName'] = $request->firstName;
            $input['lastName'] = $request->lastName;
            array_forget($data, '_token');
            $input['state'] = $request->state;
             $input['phone'] = $request->phone;
            $input['city'] = $request->city;
            $input['country'] = $request->country;
            $input['company'] = $request->company;

            $affectedRows = User::where('id', '=', $user_id)->update($input);
            Session::flash('success', 'Your profile has been updated.');
            return redirect()->back();
        }
    }

}
