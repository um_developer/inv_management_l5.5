<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\Inventory;
use App\PoItems;
use App\PurchaseOrders;
use App\WarehouseInventory;
use App\Packages;
use App\PackageItems;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\Warehouses;
use Session;
use Validator,
    Input,
    Redirect;
use PDF;

class PackageController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Package Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
//        echo 'dd'; die;
        parent::__construct();
        if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
//        $this->middleware('auth');
//        if (Auth::user()->role_id != '2') {
//            Redirect::to('customer/dashboard')->send();
//            die;
//        }
    }

    public function index() {

        $warehouses = Warehouses::where('deleted', '=', '0')->get();
        $items = Items::where('deleted', '=', '0')->where('status', '=', '1')->where('serial', 'no')->get();

        $all_packages[0] = 'Select Warehouse';
        $all_packages['admin'] = 'Admin Inventory';
        foreach ($warehouses as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $all_items[0] = 'Select Item';
//        $all_items['admin'] = 'Admin Inventory';
        foreach ($items as $item) {
            $all_items[$item->id] = $item->name;
        }

        $warehouses = $all_packages;
        $items = $all_items;

        $model = Packages::where('packages.deleted', '=', '0')
                ->leftjoin('vendors as v', 'v.u_id', '=', 'packages.vendor_id')
                ->leftjoin('warehouses as w', 'w.id', '=', 'packages.warehouse_id')
                ->select('packages.*', 'w.name as location', 'v.name as vendor_name', 'v.u_id as vendor_u_id')
                ->orderBy('id', 'desc')
                ->get();
//        print_r($model); die;
        return view('front.package.index', compact('model', 'warehouses', 'items'));
    }

    public function postPackageSearch(Request $request) {

        $warehouse_id = $request->warehouse_id;
        $item_id = $request->item_id;

        if ($item_id == 0)
            return redirect('/packages');

        $warehouses = Warehouses::where('deleted', '=', '0')->get();
        $items = Items::where('deleted', '=', '0')->where('status', '=', '1')->where('serial', 'no')->get();

        $all_packages[0] = 'Select Warehouse';
        $all_packages['admin'] = 'Admin Inventory';
        foreach ($warehouses as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $all_items[0] = 'Select Item';
//        $all_items['admin'] = 'Admin Inventory';
        foreach ($items as $item) {
            $all_items[$item->id] = $item->name;
        }

        $warehouses = $all_packages;
        $items = $all_items;

        $model = Packages::where('packages.deleted', '=', '0')
                ->leftjoin('vendors as v', 'v.u_id', '=', 'packages.vendor_id')
                ->leftjoin('warehouses as w', 'w.id', '=', 'packages.warehouse_id')
                ->leftjoin('package_items as pi', 'pi.package_id', '=', 'packages.id')
                ->where('pi.item_id', $item_id)
                ->select('packages.*', 'w.name as location', 'v.name as vendor_name', 'v.u_id as vendor_u_id', 'pi.quantity as quantity')
                ->orderBy('id', 'desc')
                ->get();
//        print_r($model); die;
        return view('front.package.index', compact('model', 'warehouses', 'items'));
    }

    public function createBulk() {

        $vendors = Vendors::select(['u_id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat[0] = 'Select Vendor';
        foreach ($vendors as $cat) {
            $new_cat[$cat->u_id] = $cat->name;
        }

        $search_model = Inventory::where('inventory.deleted', '=', '0')
                ->where('inventory.quantity', '>', '0')
                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                ->select('i.*', 'inventory.quantity as inventory_quantity')
                ->get();

        $items = [];
        $i = 0;
        foreach ($search_model as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['upc_barcode'] = $item->upc_barcode;
            $items[$i]['sku'] = $item->code;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;
            $items[$i]['inventory_quantity'] = $item->inventory_quantity;


            $i++;
        }
        $vendors = $new_cat;
        return view('front.package.create_bulk', compact('vendors', 'items'));
    }

    public function create() {

        $vendors = Vendors::select(['u_id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat[0] = 'Select Vendor';

        foreach ($vendors as $cat) {
            $new_cat[$cat->u_id] = $cat->name;
        }

        $search_model = Inventory::where('inventory.deleted', '=', '0')
                ->where('inventory.quantity', '>', '0')
                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
                ->select('i.*', 'inventory.quantity as inventory_quantity')
                ->get();

        $items = [];
        $i = 0;
        foreach ($search_model as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['upc_barcode'] = $item->upc_barcode;
            $items[$i]['sku'] = $item->code;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;
            $items[$i]['inventory_quantity'] = $item->inventory_quantity;


            $i++;
        }
        $vendors = $new_cat;
        return view('front.package.create', compact('vendors', 'items'));
    }

    public function postCreateBulk(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;

        $validation = array(
            'quantity_per_box' => 'required',
            'pkg_initials' => 'max:4|min:4',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $total_items = $input['quantity_1'];
        $item_per_box = $input['quantity_per_box'];
        $total_items_quantity = $input['quantity_1'];


        $inventory_item = Inventory::where('item_id', $input['item_id_1'])->get();

        if (count($inventory_item) > 0) {
            if ($total_items_quantity > $inventory_item[0]->quantity) {
                Session::flash('error', 'This quantity is not present in our inventory');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Add atleast 1 item.');
            return redirect()->back();
        }
        if ($item_per_box > $total_items) {
            Session::flash('error', 'Total Item quantity must be greater than item per box.');
            return redirect()->back();
        }

        if ($item_per_box == 0 || $total_items == 0) {
            Session::flash('error', 'Quantity must be greater than 0.');
            return redirect()->back();
        }

        $boxes = $input['total_boxes'];

        $package_codes_initials = $request->vendor_id;

//        if ($request->vendor_id == '0') {
//            if ($request->pkg_initials == '') {
//                Session::flash('error', 'Please select any vendor or Enter 4 digit Package initial code.');
//                return redirect()->back();
//            } else
//                $package_codes_initials = $request->pkg_initials;
//        }


        DB::beginTransaction();
        for ($i = 0; $i < $boxes; $i++) {

            $random_num = Functions::generateRandomString(8);
            $package_codes = $package_codes_initials . $random_num;
//        print_r($package_codes); die;
            $package = new Packages;
            $package->created_by = $user_id;
            $package->vendor_id = $request->vendor_id;
            $package->code = $package_codes;
            $package->quantity = $item_per_box;
            $package->package_type = $request->package_type;
            $package->type = 'bulk';
            $package->save();

            if (isset($package->id)) {
                $package_items = new PackageItems;
                $package_items->package_id = $package->id;
                $package_items->item_id = $input['item_id_1'];
                $package_items->quantity = $item_per_box;
                $package_items->save();
            }

            $item_inventory = Inventory::where('item_id', '=', $input['item_id_1'])->get();
            $inventory_item_current_quantity = 0;

            if (isset($item_inventory[0]->quantity)) {
                $inventory_item_current_quantity = $item_inventory[0]->quantity;
                $rem_inv_quantity = $inventory_item_current_quantity - $item_per_box;
                $total_pkg_quantity = $item_inventory[0]->package_quantity + $item_per_box;
                Inventory::where('item_id', '=', $input['item_id_1'])->update([
                    'quantity' => $rem_inv_quantity,
                    'package_quantity' => $total_pkg_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Item inventory quantity is not enough.');
                return redirect()->back();
            }
        }

        DB::commit();
        Session::flash('success', 'Bulk Packages are created successfully');
        return redirect()->back();
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $total_package_quantity = 0;
        $validation = array(
            'pkg_initials' => 'max:4|min:4',
        );

        $items = $input['num'];
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }


        $package_codes_initials = $request->vendor_id;

//        if ($request->vendor_id == '0') {
//            if ($request->pkg_initials == '') {
//                Session::flash('error', 'Please select any vendor or Enter 4 digit Package initial code.');
//                return redirect()->back();
//            } else
//                $package_codes_initials = $request->pkg_initials;
//        }

        $random_num = Functions::generateRandomString(8);
        $package_codes = $package_codes_initials . $random_num;

        DB::beginTransaction();

        if (count($items) > 0) {

            $package = new Packages;
            $package->created_by = $user_id;
            $package->vendor_id = $request->vendor_id;
            $package->code = $package_codes;
            $package->quantity = '0';
            $package->package_type = $request->package_type;
            $package->type = 'single';
            $package->save();

            if (isset($package->id)) {
                for ($i = 1; $i <= count($items); $i++) {
                    if ($input['quantity_' . $i] != '0') {

                        $inventory_item = Inventory::where('item_id', $input['item_id_' . $i])->get();

                        if ($input['quantity_' . $i] > $inventory_item[0]->quantity) {
                            Session::flash('error', 'Some Item quantity is not present in our inventory. Please create purchase order again.');
                            return redirect()->back();
                        }

                        $package_items = new PackageItems;
                        $package_items->package_id = $package->id;
                        $package_items->item_id = $input['item_id_' . $i];
                        $package_items->quantity = $input['quantity_' . $i];
                        $total_package_quantity = $total_package_quantity + $input['quantity_' . $i];
                        $package_items->save();

                        $item_inventory = Inventory::where('item_id', '=', $input['item_id_' . $i])->get();
                        $inventory_item_current_quantity = 0;

                        if (isset($item_inventory[0]->quantity)) {
                            $inventory_item_current_quantity = $item_inventory[0]->quantity;
                            $rem_inv_quantity = $inventory_item_current_quantity - $input['quantity_' . $i];
                            $total_pkg_quantity = $item_inventory[0]->package_quantity + $input['quantity_' . $i];
                            Inventory::where('item_id', '=', $input['item_id_' . $i])->update([
                                'quantity' => $rem_inv_quantity,
                                'package_quantity' => $total_pkg_quantity,
                            ]);
                        } else {
                            DB::rollBack();
                            Session::flash('error', 'Item inventory quantity is not enough.');
                            return redirect()->back();
                        }
                    }
                }
            }
        } else {
            Session::flash('error', 'Add atleast 1 item.');
            return redirect()->back();
        }

        Packages::where('id', '=', $package->id)->update([
            'quantity' => $total_package_quantity,
        ]);

        DB::commit();
        Session::flash('success', 'Package are created successfully');
        return redirect()->back();
    }

    public function delete($id) {

        $model = Packages::where('packages.id', '=', $id)
                ->where('status', 'unassigned')
                ->where('quantity', '!=', '0')
                ->get();

        $total_package_quantity = 0;
        $package_quantity = $model[0]->quantity;
        $package_items = PackageItems::where('package_id', '=', $id)->get();
        DB::beginTransaction();
        if (count($package_items) > 0) {
            foreach ($package_items as $package_item) {

                $row = 0;
                $inventory_item = Inventory::where('item_id', $package_item->item_id)->get();
                $inventory_item_quantity = $inventory_item[0]->quantity;
                $row = $inventory_item_quantity + $package_item->quantity;
                $rem_package_quantity = $inventory_item[0]->package_quantity - $package_item->quantity;

                Inventory::where('item_id', $package_item->item_id)->update([
                    'quantity' => $row,
                    'package_quantity' => $rem_package_quantity,
                ]);

                PackageItems::where('package_id', '=', $id)->update([
                    'deleted' => '1',
                ]);
            }
        }

        Packages::where('id', '=', $id)->update([
            'deleted' => '1',
        ]);
        DB::commit();
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id) {

        $model = Packages::where('packages.id', '=', $id)
                ->leftjoin('vendors as v', 'v.id', '=', 'packages.vendor_id')
                ->where('packages.deleted', '=', '0')
                ->select('packages.*', 'v.id as vendor_id', 'v.name as vendor_name')
                ->get();

        $po_items = PackageItems::where('package_id', '=', $id)
                ->where('package_items.deleted', '0')
                ->leftjoin('items as i', 'i.id', '=', 'package_items.item_id')
                ->select('package_items.*', 'i.name as item_name', 'i.code as item_sku', 'i.upc_barcode as item_upc_barcode')
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.package.detail', compact('model', 'po_items'));
    }

    public function transferPackage() {

        $warehouses = Warehouses::where('deleted', '=', '0')->get();
        $packages = Packages::where('deleted', '=', '0')
                ->where('status', '=', 'unassigned')
                ->get();

        $all_packages = [];
        $all_packages[0] = 'Select Packages';
        foreach ($packages as $item) {
            $all_packages[$item->id] = $item->id;
        }

        $from_warehouses = [];
        $all_warehouses = [];
        $from_warehouses[0] = 'Admin Inventory';
        foreach ($warehouses as $item) {
            $from_warehouses[$item->id] = $item->name;
        }

        foreach ($warehouses as $item) {
            $all_warehouses[$item->id] = $item->name;
        }

        $all_warehouses = $from_warehouses;

        return view('front.package.transfer', compact('all_warehouses', 'from_warehouses', 'all_packages'));
    }

    public function getPackageByWarehouse($warehouse_id) {

        if ($warehouse_id == '0') {
            $packages = Packages::where('deleted', '=', '0')
                    ->where('status', '=', 'unassigned')
                    ->get();

            $all_packages = [];
            $all_packages[0] = 'Select Packages';
            foreach ($packages as $item) {
                $all_packages[$item->id] = $item->id;
            }
        } else {

            $packages = Packages::where('warehouse_id', '=', $warehouse_id)->where('status', '=', 'assigned')->get();
            $all_packages = [];
            $all_packages[0] = 'Select Packages';
            foreach ($packages as $item) {
                $all_packages[$item->id] = $item->id;
            }
        }

        $data = view('front.common.ajax_packages', compact('all_packages'))->render();
        return $data;
    }

    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }

    public function transferPackagePost(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;

        $validation = array(
            'warehouse_id' => 'required',
            'package_id' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $destination = $input['warehouse_t_id'];
        $from = $input['warehouse_id'];
        $package_id = $input['package_id'];
        $quantity = $input['quantity'];
        $package_id_1 = $package_id;

        if ($package_id == '0') {
            Session::flash('error', 'Please atleast select 1 package.');
            return redirect()->back();
        }

        if ($destination == $from) {
            Session::flash('error', 'Destination is same');
            return redirect()->back();
        }

        DB::beginTransaction();
        for ($i = 1; $i <= $quantity; $i++) {

            $package_items = PackageItems::where('package_id', $package_id_1)->get();
            $status = 'assigned';


            if (count($package_items) > 0) {
                foreach ($package_items as $item) {

                    if ($from != '0') { /////// from any warehouse
                        $updateWarehouseInventory = self::updateWarehouseInventory($item->item_id, $item->quantity, 'subtract', $from);

                        if ($updateWarehouseInventory != 'true') {
                            $item = Items::where('id', $item->item_id)->first();
                            Session::flash('error', 'Quantity of ' . $item->name . ' are short in the source.');
                            return redirect()->back();
                        }
                    }

                    if ($destination == 0) {
                        self::updateAdminInventory($item->item_id, $item->quantity, 'add');
                        $status = 'unassigned';
                    } else {
                        self::updateWarehouseInventory($item->item_id, $item->quantity, 'add', $destination);
                        self::updateAdminInventory($item->item_id, $item->quantity, 'subtract');
                    }
                }
            }

            Packages::where('id', $package_id_1)->update([
                'warehouse_id' => $destination,
                'status' => $status
            ]);

            $package_id_1 = $package_id + $i;
        }

        DB::commit();
        Session::flash('success', 'Package are successfully transfered.');
         return redirect('/packages');
        return redirect()->back();
    }

    function updateWarehouseInventory($item_id, $quantity, $type = 'add', $warehouse_id) {

        $item = WarehouseInventory::where('item_id', $item_id)->where('warehouse_id', $warehouse_id)->get();
        $item_inv = Inventory::where('item_id', $item_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                WarehouseInventory::where('warehouse_id', '=', $warehouse_id)->where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new WarehouseInventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->warehouse_id = $warehouse_id;
                $Inventory->save();
            }

//            $total_package_quantity = $item_inv[0]->package_quantity - $quantity;
//                Inventory::where('item_id', '=', $item_id)->update([
//                    //'quantity' => $total_quantity,
//                    'package_quantity' => $total_package_quantity,
//                ]);
        } else {
            if (count($item) > 0 && $item[0]->quantity > 0) {
                $total_quantity = $item[0]->quantity - $quantity;
                if ($total_quantity < 0) {
                    return $item_id;
                } else {
                    WarehouseInventory::where('warehouse_id', '=', $warehouse_id)->where('item_id', '=', $item_id)->update([
                        'quantity' => $total_quantity,
                    ]);
                    return 'true';
                }

//                $total_package_quantity = $item_inv[0]->package_quantity + $quantity;
//                Inventory::where('item_id', '=', $item_id)->update([
//                    //'quantity' => $total_quantity,
//                    'package_quantity' => $total_package_quantity,
//                ]);
            } else {
                return $item_id;
            }
        }
    }

    function updateAdminInventory($item_id, $quantity, $type = 'add') {
        $item = Inventory::where('item_id', $item_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {

//                $total_quantity = $item[0]->quantity + $quantity;
                $total_package_quantity = $item[0]->package_quantity + $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
//                    'quantity' => $total_quantity,
                    'package_quantity' => $total_package_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = 0;
                $Inventory->package_quantity = $quantity;
                $Inventory->save();
            }
        } else {
//            $total_quantity = $item[0]->quantity - $quantity;
            $total_package_quantity = ($item[0]->package_quantity) - $quantity;
            if ($total_package_quantity < 0) {
                return 'false';
            } else {
                Inventory::where('item_id', '=', $item_id)->update([
//                    'quantity' => $total_quantity,
                    'package_quantity' => $total_package_quantity,
                ]);
            }
        }
    }

    function generatepdf($package_id) {

        ///$summary = 'aamir';
        $model = Packages::where('packages.id', '=', $package_id)
                ->get();
        $summary = $model[0]->id;
        $view = view('front.package.print', ['summary' => $summary, 'i' => 0])->render();
        $package_name = 'package_' . $summary . '.pdf';
        $pdf = PDF::loadHTML($view)->setPaper('a6', 'landscape')->setWarnings(false)->save($package_name);
//     string|array $size: 'letter', 'legal', 'A4', etc. See Dompdf\Adapter\CPDF::$PAPER_SIZES
//string $orientation: 'portrait' or 'landscape'
//      PDF::load($html)
//        ->filename('/tmp/example1.pdf')
//        ->output();

        return $pdf->stream();
    }

}
