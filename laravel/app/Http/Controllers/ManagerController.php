<?php

namespace App\Http\Controllers;

use DB;
use App\Customers;
use App\users;
use App\PriceTemplates;
use App\States;
use App\Payments;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\User;
use Session;
use App\PrintPageSetting;
use Validator,
    Input,
    Redirect;
use Illuminate\Contracts\Auth\Guard;

class ManagerController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Customer Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
        parent::__construct();
    }

    public function index() {

        $model = User::where('deleted', '=', '0')
                ->where('role_id', '5')
                ->orderBy('id', 'desc')
                ->get();

        return view('front.manager.index', compact('model'));
    }


    public function printCatalog() {

        $model = User::where('deleted', '=', '0')
                ->where('role_id', '5')
                ->orderBy('id', 'desc')
                ->get();

        return view('front.manager.print_catalog', compact('model'));
    }


    public function create() {

        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;

        $templates = PriceTemplates::where('deleted', '=', '0')->where('user_type', 'admin')
                        ->lists('title', 'id')->toArray();
        return view('front.manager.create', compact('states', 'templates'));
    }

    public function edit($id) {

        $model = User::where('id', '=', $id)->where('deleted', '=', '0')->get();
        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
//        print_r($model); die;
        return view('front.manager.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'email' => 'required|email|max:50',
            'username' => 'max:50|min:3|unique:customers,username',
            'password' => 'min:6|max:30',
            'phone' => 'max:16',
            'company' => 'max:250',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        $customer = new Customers;
        $customer->name = $request->name;
        $customer->username = $request->username;
        $customer->password = $request->password;
        $customer->email = $request->email;
        $customer->selling_price_template = $request->selling_price_template;
        $customer->phone = $request->phone;
        $customer->address1 = $request->address1;
        $customer->address2 = $request->address2;
        $customer->state = $request->state;
        $customer->city = $request->city;
        $customer->zip_code = $request->zip_code;
        $customer->company = $request->company;
        $customer->gallery_invoice_btn = $request->gallery_invoice_btn;
        $customer->gallery_order_btn = $request->gallery_order_btn;
        $customer->client_menu = $request->client_menu;

        $customer->show_unit_price = $request->show_unit_price;
        $customer->show_image = $request->show_image;
        $customer->show_serial = $request->show_serial;
        $customer->show_package_id = $request->show_package_id;

        $customer->created_by = $user_id;
        $customer->note = $request->note;
        $customer->save();

        $user = new User;
        $user->email = $request->username;
        if ($request->password != '')
            $user->password = bcrypt($request->password);
        $user->role_id = '4';
        $user->save();

        if (!isset($user->id)) {
            DB::rollBack();
            Session::flash('error', 'Customer is not created. Please try again.');
            return redirect()->back();
        }

        if (isset($customer->id)) {
            $random_string = Functions::generateRandomString(6);
            $u_id = $random_string . $customer->id;
            Customers::where('id', '=', $customer->id)->update(['u_id' => $u_id, 'user_id' => $user->id]);
            DB::commit();
            Session::flash('success', 'Customer is created successfully');
            return redirect()->back();
        } else {
            DB::rollBack();
            Session::flash('error', 'Customer is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $customer_id = $request->id;
        $validation = array(
//            'email' => 'required|email|max:50|unique:users,email,' . $request->email,
            'password' => 'min:6|max:30',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $customer_id = $request->id;
        $input = $request->all();

        if($input['password1'] != ''){
            $password = bcrypt($input['password1']);
            array_forget($input, 'password1');
             $input['password'] = $password;
        }
        else
             array_forget($input, 'password1');
        
       

        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

User::where('id', '=', $customer_id)->update($input);
        Session::flash('success', 'User has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        $customer = Customers::where('u_id', '=', $id)->first();
        $payments = Payments::where('user_type', 'customer')->where('user_id', $customer->id)->get();

        if (count($payments) > 0) {
            Session::flash('error', 'You cannot delete this customer because this customer has some transactions.');
            return redirect()->back();
        }
        if ($customer->user_id != '0') {
//            App\User::where('id', '=', $customer->user_id)->delete();
            App\User::where('id', '=', $customer->user_id)->update(['deleted' => 1]);
        }

        Customers::where('id', '=', $id)->update(['deleted' => 1]);
        //Customers::where('u_id', '=', $id)->delete();
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function customLogin(Request $request) {

        $this->auth->logout();
        SignupController::customLogin($request->user_id, $request);
        return redirect()->back();
    }

    public function printPageSetting() {

        $model = PrintPageSetting::where('user_type', '=', 'admin')->first();
        $user_id = Auth::user()->id;

        if (count($model) < 1) {
            $printPageSetting = new PrintPageSetting;
            $printPageSetting->bottom_text_1 = 'FOR ACCESSORIES VISIT BROADPOSTERS.COM';
            $printPageSetting->bottom_text_2 = 'NOTICE: OPEN ITEMS ARE NON REFUNDABLE. THANK YOU';
            $printPageSetting->user_type = 'admin';
            $printPageSetting->user_id = $user_id;
            $printPageSetting->save();
        }

        $model = PrintPageSetting::where('user_type', '=', 'admin')->first();
        return view('front.customer.print_page_setting', compact('model'));
    }

    public function updatePrintPageSetting(Request $request) {

        $input = $request->all();

        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        PrintPageSetting::where('id', '=', $request->id)->update($input);
        Session::flash('success', 'Updated seccessfully.');
        return redirect()->back();
    }

}
