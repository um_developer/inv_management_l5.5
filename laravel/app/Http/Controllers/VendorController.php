<?php

namespace App\Http\Controllers;

use DB;
use App\Vendors;
use App\Payments;
use App\User;
use App\PurchaseOrders;
use App\ReceiveOrders;
use App\States;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;
use Excel;

class VendorController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Vendor Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        parent::__construct();
//        $this->middleware('auth');
        if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
    }

    public function index() {

        $model = Vendors::where('deleted', '=', '0')->get();
        return view('front.vendor.index', compact('model'));
    }

    public function create() {

        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;

        return view('front.vendor.create', compact('states'));
    }

    public function edit($id) {

        $model = Vendors::where('u_id', '=', $id)->where('deleted', '=', '0')->get();
        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.vendor.edit', compact('model', 'states'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'email' => 'email|max:50',
            'email_2' => 'email|max:50',
            'phone_2' => 'max:16',
            'phone' => 'max:16',
//             'phone' => 'regex:/(01)[0-9]{9}/|max:16',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $vendor = new Vendors;
        $vendor->name = $request->name;
        $vendor->email = $request->email;
        $vendor->phone = $request->phone;
        $vendor->email_2 = $request->email_2;
        $vendor->phone_2 = $request->phone_2;
        $vendor->address1 = $request->address1;
        $vendor->address2 = $request->address2;
        $vendor->state = $request->state;
        $vendor->city = $request->city;
        $vendor->zip_code = $request->zip_code;
        $vendor->created_by = $user_id;
        $vendor->note = $request->note;
        $vendor->save();

        if (isset($vendor->id)) {
            $random_string = Functions::generateRandomString(6);
            $u_id = $random_string . $vendor->id;
            Vendors::where('id', '=', $vendor->id)->update(['u_id' => $u_id]);
            Session::flash('success', 'Vendor is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Vendor is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'email' => 'email|max:50',
            'phone' => 'max:16',
            'u_id' => 'required',
            'email_2' => 'email|max:50',
            'phone_2' => 'max:16',
//             'phone' => 'regex:/(01)[0-9]{9}/|max:16',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $vendor_id = $request->u_id;
        $input = $request->all();
//        print_r($input); die;
        array_forget($input, '_token');
        array_forget($input, 'u_id');
        array_forget($input, 'submit');

        Vendors::where('u_id', '=', $vendor_id)->update($input);
        Session::flash('success', 'Vendor has been updated.');
        return redirect()->back();
    }

    public function delete($id) {

        $payments = Payments::where('user_type', 'vendor')->where('user_id', $id)->get();

        if (count($payments) > 0) {
            Session::flash('error', 'You cannot delete this vendor because this vendor has some transactions.');
            return redirect()->back();
        }

        Vendors::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id) {

        $model = Vendors::where('u_id', '=', $id)->where('deleted', '=', '0')->get();
        $purchase_orders = PurchaseOrders::where('vendor_id', $model[0]->id)->get();

        $ro_payment = ReceiveOrders::where('vendor_id', $model[0]->id)->where('deleted', '0')->sum('total_price');

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.vendor.detail', compact('model', 'purchase_orders', 'ro_payment'));
    }

    public function DownloadExcelEmail() {
        set_time_limit(360);
        $result = User::where("users.deleted", '=', 0)->where("users.role_id", '!=', 1);
        $result = $result->select('users.id', 'users.firstName', 'users.lastName', 'users.email', 'users.created_at as RegistrationDate');
        $result = $result->orderBy('id', 'desc');
        $result = $result->get();
//        print_r($result); die;
        $file_name = "users_email_data.csv";
        return Excel::create($file_name, function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download('csv');
    }

}
