<?php

namespace App\Http\Controllers;

use DB;
use App\Vendors;
use App\Payments;
use App\Customers;
use App\Clients;
use App\PurchaseOrders;
use App\ReceiveOrders;
use App\States;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\DirectPaymentSources;
use App\PaymentAccountTransactions;
use App\AccountTypes;
use Auth;
use Session;
use Carbon\Carbon;
use Validator,
    Input,
    Redirect;

class CustomerPaymentController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      |Customer Payment Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $this->customer_id = $customer[0]->id;
    }

    public function index() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_customer', '1')->get();

        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;


        $model = Payments::where('payments.deleted', '=', '0')->where('user_type', 'customer')->where('payment_type', 'direct')
                ->where('payments.user_id', $this->customer_id)
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'c.name as customer_name')
               ->orderBy('payments.created_at', 'desc')
                ->get();


        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $date = $start_date . ' - ' . $end_date;

        $user_type = 'customer';
        $payment_source_id = 0;
        return view('front.customers.payments.index', compact('model', 'user_type', 'date', 'payment_sources', 'payment_source_id'));
    }

    public function myPaymentSearch(Request $request) {

        $date = $request->date_range;
        $payment_source_id = 0; //$request->payment_source_id;

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);
            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_customer', '1')->get();

        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $model = Payments::where('payments.deleted', '=', '0')->where('user_type', 'customer')->where('payment_type', 'direct')
                ->where('payments.user_id', $this->customer_id)
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id');


        if ($payment_source_id != '0')
            $model = $model->where('payments.payment_source_id', $payment_source_id);


        $model = $model->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'c.name as customer_name')
               ->orderBy('payments.created_at', 'desc')
                ->get();


        $user_type = 'customer';
        return view('front.customers.payments.index', compact('model', 'user_type', 'date', 'payment_sources', 'payment_source_id'));
    }

    public function create() {

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_customer', '1')->lists('title', 'id');
        $accounts = AccountTypes::where('deleted', '0')->where('for_customer', '1')->lists('title', 'id');

        return view('front.customers.payments.create', compact('payment_sources', 'accounts'));
    }

    public function postCreate(Request $request) {

        $user_id = $this->customer_id;
        $validation = array(
            'amount' => 'required|min:1',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $user = Customers::where('id', $this->customer_id)->get();
        $amount = $user[0]->balance + $request->amount;

        $payment = new Payments;
        $payment->amount = $request->amount;
        $payment->user_id = $this->customer_id;
        $payment->message = $request->note;
        $payment->user_type = 'customer';
        $payment->payment_source_id = $request->payment_source_id;
        $payment->account_type_id = $request->account_id;
        $payment->total_modified_amount = $request->amount;
        $payment->status = 'pending';
        $payment->statement_date = $statement_date;
        $payment->message = $request->note;
        $payment->save();

        // Customers::where('id', $this->customer_id)->update(['balance' => $amount]);

        if (isset($payment->id)) {
            Session::flash('success', 'Payment is created successfully and waiting for admin approval');
            return redirect()->back();
        } else {
            Session::flash('error', 'Payment is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $validation = array(
            'amount' => 'required|min:1',
            'id' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $payment_id = $request->id;
        $input = $request->all();
//        print_r($input); die;
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        $input['statement_date'] = $statement_date;

        Payments::where('id', '=', $payment_id)->update($input);
        Session::flash('success', 'Payment has been updated.');
        return redirect()->back();
    }

    public function delete($id) {

        $payment = Payments::where('id', '=', $id)->where('user_id', $this->customer_id)->where('status', 'pending')->get();
        if (count($payment) == 0)
            return redirect('/');

//        $customer = Customers::where('id', $payment[0]->user_id)->get();
//        $amount = $customer[0]->balance - $payment[0]->amount;
//        $customer = Customers::where('id', $id)->update(['balance' => $amount]);

        Payments::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id) {

        $model = Vendors::where('u_id', '=', $id)->where('deleted', '=', '0')->get();
        $purchase_orders = PurchaseOrders::where('vendor_id', $model[0]->id)->get();

        $ro_payment = ReceiveOrders::where('vendor_id', $model[0]->id)->where('deleted', '0')->sum('total_price');

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.vendor.detail', compact('model', 'purchase_orders', 'ro_payment'));
    }

    public function createClientPayment($user_id = '') {

        $clients = Clients::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->where('customer_id', '=', $this->customer_id)->get();
        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->lists('title', 'id');
        $accounts = AccountTypes::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->lists('title', 'id');

        $new_users [0] = 'Select Client';
        foreach ($clients as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $clients = $new_users;
        $user_type = 'client';

        return view('front.customers.client_payments.create', compact('clients', 'user_type', 'user_id', 'payment_sources', 'accounts'));
    }

    public function editClientPayment($payment_id) {

        $clients = Clients::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->where('customer_id', '=', $this->customer_id)->get();
        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->lists('title', 'id');
        $accounts = AccountTypes::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->lists('title', 'id');
        $model = Payments::where('id', $payment_id)->where('user_type', 'client')->where('created_by', $this->customer_id)->where('is_adjusted', '0')->first();

        if (count($model) == 0)
            return redirect('/customer/dashboard');

        $new_users [0] = 'Select Client';
        foreach ($clients as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $clients = $new_users;
        $user_type = 'client';
        $user_id = $model->user_id;

        return view('front.customers.client_payments.edit', compact('clients', 'user_type', 'user_id', 'payment_sources', 'accounts', 'model'));
    }

    public function getClientPayments() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $customers = Clients::where('customer_id', '=', $this->customer_id)->where('deleted', '0')->get();
        $all_packages[0] = 'Select Client';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $clients = $all_packages;

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();

        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $accounts = AccountTypes::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();

        $all_accounts_sources[0] = 'Select Account';
        foreach ($accounts as $item) {
            $all_accounts_sources[$item->id] = $item->title;
        }
        $accounts = $all_accounts_sources;

        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();

        $model = Payments::where('payments.deleted', '=', '0')->where('user_type', 'client')->where('payment_type', 'direct')->where('payments.created_by', $this->customer_id)->where('modification_count', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->select('payments.*', 'c.name as user_name', 'dps.title as payment_type_title', 'at.title as account_type_title')
               ->orderBy('payments.created_at', 'desc')
                ->get();


        $payment_status[0] = 'Select Status';
        $payment_status['approved'] = 'Approved';
        $payment_status['pending'] = 'Pending';

        $payment_status_id = 0;
        $account_id = 0;
        $user_id = 0;
        $payment_status_id = 0;

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $date = $start_date . ' - ' . $end_date;

        $user_type = 'customer';
        return view('front.customers.client_payments.index', compact('model', 'user_type', 'clients', 'date', 'payment_status', 'payment_status_id', 'user_id', 'payment_sources', 'payment_source_id', 'accounts', 'account_id'));
    }

    public function getClientPaymentsSearch(Request $request) {

        $payment_status_id = $request->payment_status_id;
        $user_id = $request->user_id;
        $date = $request->date_range;
        $payment_source_id = $request->payment_source_id;
        $account_id = $request->account_id;

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);
            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }
        $customers = Clients::where('customer_id', '=', $this->customer_id)->where('deleted', '0')->get();
        $all_packages[0] = 'Select Client';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $clients = $all_packages;

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();

        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $accounts = AccountTypes::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();

        $all_accounts_sources[0] = 'Select Account';
        foreach ($accounts as $item) {
            $all_accounts_sources[$item->id] = $item->title;
        }
        $accounts = $all_accounts_sources;

        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();

        $model = Payments::where('payments.deleted', '=', '0')->where('payments.user_type', 'client')->where('payments.payment_type', 'direct')->where('payments.created_by', $this->customer_id)->where('modification_count', '0')
                ->leftjoin('clients as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date]);

        if ($user_id != '0')
            $model = $model->where('payments.user_id', $user_id);

        if ($payment_status_id != '0')
            $model = $model->where('payments.status', $payment_status_id);

        if ($payment_source_id != '0')
            $model = $model->where('payments.payment_source_id', $payment_source_id);

        if ($account_id != '0')
            $model = $model->where('payments.account_type_id', $account_id);

        $model = $model->select('payments.*', 'c.name as user_name', 'dps.title as payment_type_title', 'at.title as account_type_title')
               ->orderBy('payments.created_at', 'desc')
                ->get();

        $payment_status[0] = 'Select Status';
        $payment_status['approved'] = 'Approved';
        $payment_status['pending'] = 'Pending';

        $user_type = 'customer';
        return view('front.customers.client_payments.index', compact('model', 'user_type', 'clients', 'date', 'payment_status', 'payment_status_id', 'user_id', 'payment_sources', 'payment_source_id', 'accounts', 'account_id'));
    }

    public function postCreateClientPayment(Request $request) {


        $user_id = $this->customer_id;
        $payment_id = 0;
        $modification_count = 0;
        $total_modified_amount = $request->amount;
        $payment_type_id = 0;

        if (isset($request->id)) {

            $payment_id = $request->id;
            $payment = Payments::where('id', $payment_id)->first();
            $modification_count = $payment->modification_count + 1;

            $payment_new = Payments::where('payment_type_id', $payment_id)->orderBy('id', 'DECS')->first();

            if (count($payment_new) > 0)
                $modification_count = $payment_new->modification_count + 1;

            $request->amount = $request->amount - $payment->total_modified_amount;
            $request->user_id = $payment->user_id;
            $request->account_type_id = $payment->account_type_id;
            $payment_type_id = $payment_id;
        }

        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();


        $validation = array(
            'amount' => 'required|min:1',
            'user_id' => 'not_in:0',
        );

        $messages = array(
            'user_id.not_in' => 'Please select any client',
        );

        $validator = Validator::make($request->all(), $validation, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $user = Clients::where('id', $request->user_id)->first();

        $updated_balance = $user->balance - $request->amount;
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $payment = new Payments;
        $payment->amount = $request->amount;
        $payment->user_id = $request->user_id;
        $payment->created_by = $this->customer_id;
        $payment->account_type_id = $request->account_type_id;
        $payment->message = $request->note;
        $payment->user_type = 'client';
        $payment->status = 'approved';
        $payment->payment_source_id = $request->payment_source_id;
        $payment->message = $request->note;
        $payment->statement_date = $statement_date;
        $payment->modification_count = $modification_count;
        $payment->total_modified_amount = $total_modified_amount;
        $payment->payment_type_id = $payment_type_id;
        $payment->previous_balance = $user->balance;
        $payment->updated_balance = $updated_balance;
        if (strpos($request->amount, '-') !== false)
            $payment->transaction_type = 'negative';
        else
            $payment->transaction_type = 'positive';

        $payment->save();

        Clients::where('id', $request->user_id)->update(['balance' => $updated_balance]);
        CommonController::updateAccountTransaction($request->account_type_id, $request->amount, 'payment', $payment->id, 'customer');

        PaymentController::syncPayments($payment->id);

        if (isset($payment->id)) {

            if (isset($request->id))
                Session::flash('success', 'Payment is updated successfully');
            else
                Session::flash('success', 'Payment is created successfully');

            return redirect()->back();
        } else {
            Session::flash('error', 'Payment is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function deleteClientPayment($id) {

        $customer = Customers::where('user_id', $this->user_id)->get();
         $user_id = $this->customer_id;
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();

        $payment = Payments::where('id', '=', $id)->where('created_by', $this->customer_id)->first();
        if (!isset($payment->id)) {
            Session::flash('error', 'Payment not found');
            return redirect()->back();
        }
        
        if ($payment->is_adjusted == '1') {
            Payments::where('id', '=', $id)->delete();
            Session::flash('success', 'Adjusted Payment Successfully Deleted!');
            return redirect('customer/client/payments/');
        }

        DB::beginTransaction();
        $client = Clients::where('id', $payment->user_id)->first();
        $amount = $client->balance + $payment->total_modified_amount;
        $client = Clients::where('id', $payment->user_id)->update(['balance' => $amount]);
         $updated_balance = $customer[0]->balance + $payment->total_modified_amount;

        if ($payment->account_type_id != 0) {
            CommonController::updateAccountTransaction($payment->account_type_id, $payment->amount, 'delete', $payment->id, 'customer', 'subtract');
        }

        Payments::where('id', '=', $id)->update(['deleted' => 1]);
        
        
//        $payment = $payment[0];
        if (strpos($payment->total_modified_amount, '-') !== false)
             $payment->amount = str_replace("-", "", $payment->total_modified_amount);
        else
            $payment->amount = '-' . $payment->total_modified_amount;

        $payment_new = new Payments;
        $payment_new->amount = $payment->amount;
        $payment_new->user_id = $payment->user_id;
        $payment_new->message = $payment->note;
        $payment_new->statement_date = $payment->statement_date;
        $payment_new->payment_source_id = $payment->payment_source_id;
        $payment_new->account_type_id = $payment->account_type_id;
        $payment_new->modification_count = $payment->modification_count + 1;
        $payment_new->total_modified_amount = 0;
        $payment_new->user_type = $payment->user_type;
        $payment_new->status = 'approved';
        $payment_new->message = 'Deleted payment entry for PMT'. $id;
        $payment_new->previous_balance = $customer[0]->balance;
        $payment_new->updated_balance = $updated_balance;
        $payment_new->payment_type_id = $id;
        $payment_new->created_by = $user_id;
        $payment_new->deleted = '1';
        
        if (strpos($payment->amount, '-') !== false)
            $payment_new->transaction_type = 'negative';
        else
            $payment_new->transaction_type = 'positive';

        $payment_new->save();

        
        DB::commit();
        Session::flash('success', 'Successfully Deleted!');
        return redirect('customer/client/payments/');
    }

    public function postUpdateClientPayment(Request $request) {

        $validation = array(
            'amount' => 'required|min:1',
            'id' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $payment_id = $request->id;
        $payment = Payments::where('id', $payment_id)->first();

        if ($payment->total_modified_amount == $request->amount) {

            $input = $request->all();
            $timestamp = strtotime($request->statement_date);

            $input['message'] = $request->note;
            $input['statement_date'] = date("Y-m-d H:i:s", $timestamp);

            array_forget($input, '_token');
            array_forget($input, 'id');
            array_forget($input, 'submit');
            array_forget($input, 'note');
            array_forget($input, 'amount');

            Payments::where('id', '=', $payment_id)->update($input);

            PaymentController::syncPayments($payment_id);
            Session::flash('success', 'Payment has been updated.');
        } else {

            self::postCreateClientPayment($request);
        }


        return redirect()->back();
    }

}
