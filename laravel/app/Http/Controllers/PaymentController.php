<?php

namespace App\Http\Controllers;

use DB;
use App\Vendors;
use App\Payments;
use App\Customers;
use App\Clients;
use App\PurchaseOrders;
use App\ReceiveOrders;
use App\States;
use App\Invoices;
use App\Orders;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\DirectPaymentSources;
use App\AccountTypes;
use App\PaymentAccountTransactions;
use Auth;
use Carbon\Carbon;
use Session;
use Validator,
    Input,
    Redirect;

class PaymentController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Payment Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function getCustomerPayments() {

        $start_date = Carbon::today()->subDay(0)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $customers = Customers::where('deleted', '0')->get();
        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $payment_source_id = '0';
        $account_id = '0';
        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->get();


        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->get();
        $all_account_sources[0] = 'Select Account';
        foreach ($account_sources as $item) {
            $all_account_sources[$item->id] = $item->title;
        }
        $accounts = $all_account_sources;

        $model = Payments::where('payments.deleted', '=', '0')->where('user_type', 'customer')->where('payment_type', 'direct')->where('modification_count', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'c.name as user_name', 'c.id as user_id')
                ->orderBy('payments.created_at', 'desc')
                ->get();

        $payment_status[0] = 'Select Status';
        $payment_status['approved'] = 'Approved';
        $payment_status['pending'] = 'Pending';

        $payment_status_id = 0;
        $user_id = 0;

        $start_date = date('m/d/Y', strtotime("-0 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $date = $start_date . ' - ' . $end_date;
        $user_type = 'customer';
        return view('front.payments.index', compact('model', 'user_type', 'date', 'customers', 'payment_status', 'payment_status_id', 'user_id', 'payment_sources', 'payment_source_id', 'accounts', 'account_id'));
    }

    public function getCustomerPaymentsSearch(Request $request) {

        $payment_status_id = $request->payment_status_id;
        $user_id = $request->user_id;
        $date = $request->date_range;
        $payment_source_id = $request->payment_source_id;
        $account_id = $request->account_id;


        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);
            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->get();
        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->get();
        $all_account_sources[0] = 'Select Account Type';
        foreach ($account_sources as $item) {
            $all_account_sources[$item->id] = $item->title;
        }
        $accounts = $all_account_sources;

        $customers = Customers::where('deleted', '0')->get();
        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $model = Payments::where('payments.deleted', '=', '0')->where('user_type', 'customer')->where('payment_type', 'direct')->where('modification_count', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date]);

        if ($user_id != '0')
            $model = $model->where('payments.user_type', 'customer')->where('payments.user_id', $user_id);

        if ($payment_status_id != '0')
            $model = $model->where('payments.status', $payment_status_id);

        if ($payment_source_id != '0')
            $model = $model->where('payments.payment_source_id', $payment_source_id);

        if ($account_id != '0')
            $model = $model->where('payments.account_type_id', $account_id);

        $model = $model->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'c.name as user_name', 'c.id as user_id')
                ->orderBy('payments.created_at', 'desc')
                ->get();

        $payment_status[0] = 'Select Status';
        $payment_status['approved'] = 'Approved';
        $payment_status['pending'] = 'Pending';


        $user_type = 'customer';
        return view('front.payments.index', compact('model', 'user_type', 'date', 'customers', 'payment_status', 'payment_status_id', 'user_id', 'payment_sources', 'payment_source_id', 'accounts', 'account_id'));
    }

    public function getVendorPayments() {

        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }

        $payment_status_id = '0';
        $account_id = '0';
        $user_id = 0;

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $payment_status[0] = 'Select Status';
        $payment_status['approved'] = 'Approved';
        $payment_status['pending'] = 'Pending';

        $customers = Vendors::where('deleted', '0')->get();
        $all_packages[0] = 'Select Vendor';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->get();
        $all_account_sources[0] = 'Select Account Type';
        foreach ($account_sources as $item) {
            $all_account_sources[$item->id] = $item->title;
        }
        $accounts = $all_account_sources;

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->get();

        $payment_source_id = '0';
        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $model = Payments::where('payments.deleted', '=', '0')->where('user_type', 'vendor')->where('payment_type', 'direct')->where('modification_count', '0')
                ->leftjoin('vendors as v', 'v.id', '=', 'payments.user_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'v.name as user_name', 'v.id as user_id')
                ->orderBy('payments.created_at', 'desc')
                ->get();

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $date = $start_date . ' - ' . $end_date;
        $user_type = 'vendor';
        return view('front.payments.index', compact('model', 'user_type', 'date', 'customers', 'payment_status_id', 'payment_status', 'user_id', 'payment_sources', 'payment_source_id', 'accounts', 'account_id'));
    }

    public function getVendorPaymentsSearch(Request $request) {

        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }

        $payment_status_id = $request->payment_status_id;
        $user_id = $request->user_id;
        $date = $request->date_range;
        $payment_source_id = $request->payment_source_id;
        $account_id = $request->account_id;

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);
            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $payment_status[0] = 'Select Status';
        $payment_status['approved'] = 'Approved';
        $payment_status['pending'] = 'Pending';

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->get();
        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->get();
        $all_account_sources[0] = 'Select Account Type';
        foreach ($account_sources as $item) {
            $all_account_sources[$item->id] = $item->title;
        }
        $accounts = $all_account_sources;

        $customers = Vendors::where('deleted', '0')->get();
        $all_packages[0] = 'Select Vendor';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $model = Payments::where('payments.deleted', '=', '0')->where('user_type', 'vendor')->where('payment_type', 'direct')->where('modification_count', '0')
                ->leftjoin('vendors as v', 'v.id', '=', 'payments.user_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date]);

        if ($user_id != '0')
            $model = $model->where('payments.user_type', 'vendor')->where('payments.user_id', $user_id);

        if ($payment_status_id != '0')
            $model = $model->where('payments.status', $payment_status_id);
        if ($payment_source_id != '0')
            $model = $model->where('payments.payment_source_id', $payment_source_id);
        if ($account_id != '0')
            $model = $model->where('payments.account_type_id', $account_id);

        $model = $model->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'v.name as user_name', 'v.id as user_id')
                ->orderBy('payments.created_at', 'desc')
                ->get();

        $user_type = 'vendor';
        return view('front.payments.index', compact('model', 'user_type', 'date', 'customers', 'payment_status_id', 'payment_status', 'user_id', 'payment_sources', 'payment_source_id', 'accounts', 'account_id'));
    }

    public function createCustomerPayment($user_id = '') {

        $users = Customers::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->get();

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $new_users[0] = 'Select Customer';
        foreach ($users as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $users = $new_users;
        $user_type = 'customer';

        return view('front.payments.create', compact('users', 'user_type', 'user_id', 'payment_sources', 'account_sources'));
    }

    public function editPayment($payment_id) {

        $model = Payments::where('id', $payment_id)->where('is_adjusted', '0')->first();
         if (count($model) == 0)
            return redirect('/dashboard');
         
         
        if ($model->user_type == 'vendor') {
            $users = Vendors::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->get();
            $new_users[0] = 'Select Vendor';
        } else {
            $users = Customers::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->get();
            $new_users[0] = 'Select Customer';
        }

        $user_id = $model->user_id;

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        
        foreach ($users as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $users = $new_users;
        $user_type = $model->user_type;

        return view('front.payments.edit', compact('users', 'model', 'user_type', 'user_id', 'payment_sources', 'account_sources'));
    }

    public function createVendorPayment() {

        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
        $users = Vendors::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->get();
        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $new_users[0] = 'Select Vendor';
        foreach ($users as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $users = $new_users;
        $user_type = 'vendor';

        return view('front.payments.create', compact('users', 'user_type', 'payment_sources', 'account_sources'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $payment_id = 0;
        $modification_count = 0;
        $total_modified_amount = $request->amount;
        $payment_type_id = 0;

        if (isset($request->id)) {
            $payment_id = $request->id;
            $payment = Payments::where('id', $payment_id)->first();
            $modification_count = $payment->modification_count + 1;

            $payment_new = Payments::where('payment_type_id', $payment_id)->orderBy('id', 'DESC')->first();

            if (count($payment_new) > 0)
                $modification_count = $payment_new->modification_count + 1;


            $request->amount = $request->amount - $payment->total_modified_amount;
            $request->user_id = $payment->user_id;
            $request->account_type_id = $payment->account_type_id;
            $payment_type_id = $payment_id;
        }

        $validation = array(
            'amount' => 'required|min:1',
            'user_id' => 'not_in:0',
        );

        $messages = array(
            'user_id.not_in' => 'Please select any ' . $request->user_type,
        );

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }


        if ($request->user_type == 'customer')
            $user = Customers::where('id', $request->user_id)->first();
        else
            $user = Vendors::where('id', $request->user_id)->first();

        $updated_balance = $user->balance - $request->amount;

        $payment = new Payments;
        $payment->amount = $request->amount;
        $payment->user_id = $request->user_id;
        $payment->message = $request->note;
        $timestamp = strtotime($request->statement_date);
        $payment->statement_date = date("Y-m-d H:i:s", $timestamp);
        $payment->payment_source_id = $request->payment_source_id;
        $payment->account_type_id = $request->account_type_id;
        $payment->modification_count = $modification_count;
        $payment->total_modified_amount = $total_modified_amount;
        $payment->payment_type_id = $payment_type_id;

        $payment->user_type = $request->user_type;
        if (Auth::user()->role_id == 2)
            $payment->status = 'approved';
        else
            $payment->status = 'pending';
        $payment->message = $request->note;
        $payment->previous_balance = $user->balance;
        $payment->updated_balance = $updated_balance;
        $payment->created_by = $user_id;
        if (strpos($request->amount, '-') !== false)
            $payment->transaction_type = 'negative';
        else
            $payment->transaction_type = 'positive';

        $payment->save();

        if ($request->user_type == 'customer')
            Customers::where('id', $request->user_id)->update(['balance' => $updated_balance]);
        else
            Vendors::where('id', $request->user_id)->update(['balance' => $updated_balance]);


        $account_balance = AccountTypes::where('id', $request->account_type_id)->first();
        AccountTypes::where('id', $request->account_type_id)->increment('amount', $request->amount);

        $acc_previous_balance = $account_balance->amount;
        $acc_updated_balance = $account_balance->amount + $request->amount;

        $account_type_transaction = new PaymentAccountTransactions;
        $account_type_transaction->account_type_id = $request->account_type_id;
        $account_type_transaction->payment_id = $payment->id;
        $account_type_transaction->source_type = 'payment';
        $account_type_transaction->previous_balance = $acc_previous_balance;
        $account_type_transaction->user_type = 'admin';
        $account_type_transaction->created_by = $user_id;
        $account_type_transaction->message = '';
        $account_type_transaction->updated_balance = $acc_updated_balance;
        $account_type_transaction->amount = $request->amount;
        $account_type_transaction->save();


        self::syncPayments($payment->id);

        if (isset($payment->id)) {
            Session::flash('success', 'Payment is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Payment is not created. Please try again.');
            return redirect()->back();
        }
    }

    public static function syncPayments($payment_id, $type = 'direct') {

        $payment = Payments::where('id', $payment_id)->first();

        $input['statement_date'] = $payment->statement_date;
        $input['payment_source_id'] = $payment->payment_source_id;
        $input['total_modified_amount'] = $payment->total_modified_amount;
        $input['message'] = $payment->message;


        if ($type == 'direct') {

            Payments::where('payment_type_id', $payment_id)->update($input);
            if ($payment->payment_type_id != '0') {
                Payments::where('id', $payment->payment_type_id)->update($input);
                Payments::where('payment_type_id', $payment->payment_type_id)->update($input);
            }
        } elseif ($type == 'invoice' || $type == 'invoice_neg') {

            Payments::where('payment_type', 'invoice')->where('payment_type_id', $payment->payment_type_id)->update($input);
//            Payments::where('payment_type', 'invoice')->where('id', $payment->payment_type_id)->update($input);
//            Payments::where('payment_type', 'invoice')->where('payment_type_id', $payment->payment_type_id)->update($input);
        } elseif ($type == 'credit_memo' || $type == 'credit_memo_pos') {

            Payments::where('payment_type', 'credit_memo')->where('payment_type_id', $payment->payment_type_id)->update($input);
        } elseif ($type == 'order' || $type == 'order_neg') {
            Payments::where('payment_type', 'order')->where('payment_type_id', $payment->payment_type_id)->update($input);
        } elseif ($type == 'order_return' || $type == 'order_return_pos') {

            Payments::where('payment_type', 'order_return')->where('payment_type_id', $payment->payment_type_id)->update($input);
        }
    }

    public function postUpdate(Request $request) {

        $validation = array(
            'amount' => 'required|min:1',
            'id' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $payment_id = $request->id;
        $payment = Payments::where('id', $payment_id)->first();

        if ($payment->total_modified_amount == $request->amount) {

            $input = $request->all();
            $timestamp = strtotime($request->statement_date);

            $input['message'] = $request->note;
            $input['statement_date'] = date("Y-m-d H:i:s", $timestamp);

            array_forget($input, '_token');
            array_forget($input, 'id');
            array_forget($input, 'submit');
            array_forget($input, 'note');
            array_forget($input, 'amount');

            Payments::where('id', '=', $payment_id)->update($input);

            self::syncPayments($payment_id);
            Session::flash('success', 'Payment has been updated.');
        } else {

            self::postCreate($request);
        }

        return redirect()->back();
    }

    public function delete($user_type, $id) {

        $payment = Payments::where('id', '=', $id)->get();
        $user_id = Auth::user()->id;

        if ($payment[0]->is_adjusted == '1') {
            Payments::where('id', '=', $id)->delete();
            Session::flash('success', 'Adjusted Payment Successfully Deleted!');
            return redirect('payments/customer');
        }
        
        if ($payment[0]->status == 'pending') {
            Payments::where('id', '=', $id)->update(['deleted' => 1]);
            Session::flash('success', 'Payment Successfully Deleted!');
            return redirect('payments/customer');
        }
        DB::beginTransaction();


        if ($user_type == 'customer') {
            if ($payment[0]->is_adjusted == '0') {
                $customer = Customers::where('id', $payment[0]->user_id)->get();
                $updated_balance = $customer[0]->balance + $payment[0]->total_modified_amount;
                Customers::where('id', $payment[0]->user_id)->update(['balance' => $updated_balance]);
            }
        } else {
            $customer = Vendors::where('id', $payment[0]->user_id)->get();
            $updated_balance = $customer[0]->balance + $payment[0]->total_modified_amount;
            Vendors::where('id', $payment[0]->user_id)->update(['balance' => $updated_balance]);
        }

        if ($payment[0]->account_type_id != 0 && $payment[0]->status == 'approved' && $payment[0]->is_adjusted == '0') {
            CommonController::updateAccountTransaction($payment[0]->account_type_id, $payment[0]->amount, 'delete', $payment[0]->id, 'admin', 'subtract');
        }

        Payments::where('id', '=', $id)->update(['deleted' => 1]);
        $payment = $payment[0];

        if (strpos($payment->total_modified_amount, '-') !== false)
            $payment->amount = str_replace("-", "", $payment->total_modified_amount);
        else
            $payment->amount = '-' . $payment->total_modified_amount;

//        print_r($payment->amount); die;

        $payment_new = new Payments;
        $payment_new->amount = $payment->amount;
        $payment_new->user_id = $payment->user_id;
        $payment_new->message = $payment->note;
        $payment_new->statement_date = $payment->statement_date;
        $payment_new->payment_source_id = $payment->payment_source_id;
        $payment_new->account_type_id = $payment->account_type_id;
        $payment_new->modification_count = $payment->modification_count + 1;
        $payment_new->total_modified_amount = $payment->amount;
        $payment_new->user_type = $payment->user_type;
        $payment_new->status = 'approved';
        $payment_new->message = 'Deleted payment entry for PMT' . $id;
        $payment_new->previous_balance = $customer[0]->balance;
        $payment_new->updated_balance = $updated_balance;
        $payment_new->payment_type_id = $id;
        $payment_new->deleted = '1';
        $payment_new->created_by = $user_id;
        if (strpos($payment->amount, '-') !== false)
            $payment_new->transaction_type = 'negative';
        else
            $payment_new->transaction_type = 'positive';

        $payment_new->save();

        DB::commit();

        Session::flash('success', 'Successfully Deleted!');
        if ($payment->user_type == 'customer')
            return redirect('payments/customer');

        return redirect('payments/vendor');
    }

    public function detail($id) {

        $model = Vendors::where('u_id', '=', $id)->where('deleted', '=', '0')->get();
        $purchase_orders = PurchaseOrders::where('vendor_id', $model[0]->id)->get();

        $ro_payment = ReceiveOrders::where('vendor_id', $model[0]->id)->where('deleted', '0')->sum('total_price');

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.vendor.detail', compact('model', 'purchase_orders', 'ro_payment'));
    }

    public function changeStatus($id, $status) {

        $model = Payments::where('id', '=', $id)->where('user_type', 'customer')->get();
        $amount = $model[0]->amount;
        $customer_id = $model[0]->user_id;

        if (count($model) == 0 || Auth::user()->role_id == '5') {
            return redirect('/');
        }

        DB::beginTransaction();
        $user = Customers::where('id', $customer_id)->first();
        $updated_balance = $user->balance - $amount;
        Customers::where('id', $customer_id)->decrement('balance', $amount);
        Payments::where('id', '=', $id)->where('status', '=', 'pending')->update(['status' => $status, 'previous_balance' => $user->balance, 'updated_balance' => $updated_balance]);

        if ($model[0]->account_type_id != 0)
            CommonController::updateAccountTransaction($model[0]->account_type_id, $model[0]->amount, 'payment', $model[0]->id, 'admin', 'add');

        DB::commit();
        Session::flash('success', 'Successfully Changed!');

        return redirect()->to('payments/customer');
    }

    public static function updateCustomerBalance($customer_id, $amount, $form_type_id, $type, $statement_date = '') {

        $customer = Customers::where('id', $customer_id)->first();
        $payments = new Payments;

        $payments->status = 'approved';
        $payments->user_type = 'customer';
        $payments->user_id = $customer_id;
        $payments->payment_type = $type;
        $payments->payment_type_id = $form_type_id;
        $payments->statement_date = $statement_date;

        if ($type == 'invoice') {

            $invoice = \App\Invoices::where('id', $form_type_id)->first();
            $total_modified_amount = $invoice->total_price;

            $invoice_payment = Payments::where('user_type', 'customer')->where('user_id', $customer_id)->where('payment_type', 'invoice')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();

            if (count($invoice_payment) > 0) {
                $payments->modification_count = $invoice_payment->modification_count + 1;
            }

            $payments->amount = $amount;
            $payments->transaction_type = 'positive';
            $updated_balance = $customer->balance + $amount;
        } elseif ($type == 'invoice_neg') {

            $invoice = \App\Invoices::where('id', $form_type_id)->first();
            $total_modified_amount = $invoice->total_price;


            $payments->payment_type = 'invoice';
            $invoice_payment = Payments::where('user_type', 'customer')->where('user_id', $customer_id)->where('payment_type', 'invoice')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();
            if (count($invoice_payment) > 0) {
                $payments->modification_count = $invoice_payment->modification_count + 1;
            }

            $updated_balance = $customer->balance - $amount;
            $amount = '-' . $amount;
            $payments->amount = $amount;
            $payments->transaction_type = 'negative';
        } elseif ($type == 'credit_memo') {

            $credit_memo = \App\CreditMemo::where('id', $form_type_id)->first();
            $total_modified_amount = '-' . $credit_memo->total_price;

            $credit_memo_payment = Payments::where('user_type', 'customer')->where('user_id', $customer_id)->where('payment_type', 'credit_memo')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();
            if (count($credit_memo_payment) > 0) {
                $payments->modification_count = $credit_memo_payment->modification_count + 1;
            }

            $amount = '-' . $amount;
            $payments->amount = $amount;
            $payments->transaction_type = 'negative';
            $updated_balance = $customer->balance + $amount;
        } elseif ($type == 'credit_memo_pos') {

            $credit_memo = \App\CreditMemo::where('id', $form_type_id)->first();
            $total_modified_amount = '-' . $credit_memo->total_price;
            $payments->payment_type = 'credit_memo';

            $credit_memo_payment = Payments::where('user_type', 'customer')->where('user_id', $customer_id)->where('payment_type', 'credit_memo')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();
            if (count($credit_memo_payment) > 0) {
                $payments->modification_count = $credit_memo_payment->modification_count + 1;
            }

            $payments->amount = $amount;
            $payments->transaction_type = 'positive';
            $updated_balance = $customer->balance + $amount;
        }
//        print_r($total_modified_amount); die;
        $payments->previous_balance = $customer->balance;
        $payments->updated_balance = $updated_balance;
        $payments->total_modified_amount = $total_modified_amount;
        $payments->save();

        self::syncPayments($payments->id, $type);

        if (isset($payments->id))
            Customers::where('id', $customer_id)->update(['balance' => $updated_balance]);
    }

    public static function updateClientBalance($client_id, $amount, $form_type_id, $type, $statement_date = '') {

        $client = Clients::where('id', $client_id)->first();
        $total_modified_ammount = $amount;

        $payments = new Payments;

        $payments->status = 'approved';
        $payments->user_type = 'client';
        $payments->user_id = $client_id;
        $payments->payment_type = $type;
        $payments->payment_type_id = $form_type_id;
        $payments->statement_date = $statement_date;
        $payments->created_by = $client->customer_id;

        if ($type == 'order') {

            $order_payment = Payments::where('user_type', 'client')->where('user_id', $client_id)->where('payment_type', 'order')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();
            if (count($order_payment) > 0)
                $payments->modification_count = $order_payment->modification_count + 1;

            $order = \App\Orders::where('id', $form_type_id)->first();
            $total_modified_amount = $order->total_price;

            $payments->amount = $amount;
            $payments->transaction_type = 'positive';
            $updated_balance = $client->balance + $amount;
        }elseif ($type == 'order_neg') {

            $order_payment = Payments::where('user_type', 'client')->where('user_id', $client_id)->where('payment_type', 'order')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();
            if (count($order_payment) > 0)
                $payments->modification_count = $order_payment->modification_count + 1;

            $order = \App\Orders::where('id', $form_type_id)->first();
            $total_modified_amount = $order->total_price;

            $payments->payment_type = 'order';
            $updated_balance = $client->balance - $amount;
            $amount = '-' . $amount;
            $payments->amount = $amount;
            $payments->transaction_type = 'negative';
        } elseif ($type == 'order_return') {

            $order_payment = Payments::where('user_type', 'client')->where('user_id', $client_id)->where('payment_type', 'order_return')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();

            if (count($order_payment) > 0)
                $payments->modification_count = $order_payment->modification_count + 1;

            $return_order = \App\ReturnOrders::where('id', $form_type_id)->first();
            $total_modified_amount = $return_order->total_price;

            $updated_balance = $client->balance - $amount;
            $amount = '-' . $amount;
            $payments->amount = $amount;
            $payments->transaction_type = 'negative';
        }
        elseif ($type == 'order_return_pos') {

            $order_payment = Payments::where('user_type', 'client')->where('user_id', $client_id)->where('payment_type', 'order_return')->where('payment_type_id', $form_type_id)->orderBy('id', 'desc')->first();
            if (count($order_payment) > 0)
                $payments->modification_count = $order_payment->modification_count + 1;

            $payments->payment_type = 'order_return';

            $return_order = \App\ReturnOrders::where('id', $form_type_id)->first();
            $total_modified_amount = $return_order->total_price;

            $updated_balance = $client->balance + $amount;
//            $amount = '-' . $amount;
            $payments->amount = $amount;
            $payments->transaction_type = 'positive';
        }

        $payments->previous_balance = $client->balance;
        $payments->updated_balance = $updated_balance;
        $payments->total_modified_amount = $total_modified_amount;
        $payments->save();

        self::syncPayments($payments->id, $type);

        if (isset($payments->id))
            Clients::where('id', $client_id)->update(['balance' => $updated_balance]);
    }

    public function adjustCustomerPayment($user_id = '') {

        $users = Customers::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->get();

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $new_users[0] = 'Select Customer';
        foreach ($users as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $users = $new_users;
        $user_type = 'customer';
        $clients [0] = 'Select Client';
        $client_id = 0;

        return view('front.payments.adjust', compact('users', 'user_type', 'user_id', 'payment_sources', 'account_sources', 'clients', 'client_id'));
    }

    public function postAdjust(Request $request) {

        $user_id = Auth::user()->id;
        $user_type = 'customer';
        $customer_id = $request->user_id;

        if ($request->client_id != '0') {
            $user_type = 'client';
            $request->user_id = $request->client_id;
        }
        $validation = array(
            'amount' => 'required|min:1',
            'user_id' => 'not_in:0',
        );

        $messages = array(
            'user_id.not_in' => 'Please select any ' . $request->user_type,
        );

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $updated_balance = $request->amount;

        $payment = new Payments;
        $payment->amount = $request->amount;
        $payment->user_id = $request->user_id;
        $payment->message = $request->note;
        //$timestamp = strtotime($request->statement_date);
        $payment->statement_date = '2020-01-01 01:01:01'; //date("Y-m-d H:i:s", $timestamp);
        $payment->payment_source_id = '0';
        $payment->account_type_id = '0';
        $payment->modification_count = '0';
        $payment->total_modified_amount = $request->amount;
        $payment->payment_type_id = '0';
        $payment->admin_comments = 'Adjustment from Admin';
        $payment->is_adjusted = '1';
        $payment->initials = 'ADJPMT';

        $payment->user_type = $user_type;
        if (Auth::user()->role_id == 2)
            $payment->status = 'approved';
        else
            $payment->status = 'pending';
        $payment->message = $request->note;
        $payment->previous_balance = '0';
        $payment->updated_balance = $updated_balance;
        if ($request->client_id != '0')
            $payment->created_by = $customer_id;
        else
            $payment->created_by = $user_id;
        $payment->created_at = '2020-01-01 01:01:01';
        if (strpos($request->amount, '-') !== false)
            $payment->transaction_type = 'negative';
        else
            $payment->transaction_type = 'positive';

        $payment->save();


        if (isset($payment->id)) {
            if ($request->client_id != '0')
                Session::flash('success', 'Client custom payment is successfully');
            else
                Session::flash('success', 'Customer custom payment is created successfully');

            return redirect()->back();
        } else {
            Session::flash('error', 'Payment is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function getClientByCustomer($customer_id) {

        $clients = Clients::where('deleted', '0')->where('customer_id', $customer_id)->get();

        $new_cat = [];
        foreach ($clients as $po) {
            $new_cat[$po->id] = $po->name . ' (BAL: $' . $po->balance . ')';
        }
        $clients = $new_cat;
        $data = view('front.common.ajax_get_client', compact('clients'))->render();
        return $data;
    }

    public function createClientPayment() {
        $users = Customers::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->get();

        // $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        // $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $new_users[0] = 'Select Customer';
        foreach ($users as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $users = $new_users;
        $user_type = 'customer';

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_customer', '1')->lists('title', 'id');
        $account_sources = AccountTypes::where('deleted', '0')->where('for_customer', '1')->lists('title', 'id');

        return view('front.payments.client_payment_create', compact('users', 'user_type', 'payment_sources', 'account_sources'));
    }

    public function getClients(Request $request) {
        $req = $request->all();
        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $req['c_id'])->where('deleted', '=', '0')->get(); //->pluck('name', 'id');

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_customer', '1')->where('created_by', $req['c_id'])->get();
        $account_sources = AccountTypes::where('deleted', '0')->where('for_customer', '1')->where('created_by', $req['c_id'])->get();
        // $new_cat [0] = 'Select Client';
        // foreach ($clients as $cat) {
        //     $new_cat[$cat->id] = $cat->name;
        // }
        $data['clients'] = $clients;
        $data['payment_sources'] = $payment_sources;
        $data['account_sources'] = $account_sources;
        // $clients = $new_cat;
        echo json_encode($data);
    }

    public function postCreateClient(Request $request) {

        $user_id = $request->user_id;

        $payment_id = 0;
        $modification_count = 0;
        $total_modified_amount = $request->amount;
        $payment_type_id = 0;

        if (isset($request->id)) {

            $payment_id = $request->id;
            $payment = Payments::where('id', $payment_id)->first();
            $modification_count = $payment->modification_count + 1;

            $payment_new = Payments::where('payment_type_id', $payment_id)->orderBy('id', 'DECS')->first();

            if (count($payment_new) > 0)
                $modification_count = $payment_new->modification_count + 1;

            $request->amount = $request->amount - $payment->total_modified_amount;
            $request->user_id = $payment->user_id;
            $request->account_type_id = $payment->account_type_id;
            $payment_type_id = $payment_id;
        }

        $customer = Customers::where('id', $user_id)->get();

        if ($customer[0]->client_menu == 0) {
            echo 'f';
            exit();
        }


        $validation = array(
            'amount' => 'required|min:1',
            'user_id' => 'not_in:0',
        );

        $messages = array(
            'user_id.not_in' => 'Please select any client',
        );

        $validator = Validator::make($request->all(), $validation, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $user = Clients::where('id', $request->client_id)->first();

        $updated_balance = $user->balance - $request->amount;
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $payment = new Payments;
        $payment->amount = $request->amount;
        $payment->user_id = $request->client_id;
        $payment->created_by = $user_id;
        $payment->account_type_id = $request->account_type_id;
        $payment->message = $request->note;
        $payment->user_type = 'client';
        $payment->status = 'approved';
        $payment->payment_source_id = $request->payment_source_id;
        $payment->message = $request->note;
        $payment->statement_date = $statement_date;
        $payment->modification_count = $modification_count;
        $payment->total_modified_amount = $total_modified_amount;
        $payment->payment_type_id = $payment_type_id;
        $payment->previous_balance = $user->balance;
        $payment->updated_balance = $updated_balance;
        if (strpos($request->amount, '-') !== false)
            $payment->transaction_type = 'negative';
        else
            $payment->transaction_type = 'positive';

        $payment->save();

        Clients::where('id', $request->client_id)->update(['balance' => $updated_balance]);
        CommonController::updateAccountTransaction($request->account_type_id, $request->amount, 'payment', $payment->id, 'customer');

        self::syncPayments($payment->id);

        if (isset($payment->id)) {

            if (isset($request->id))
                Session::flash('success', 'Payment is updated successfully');
            else
                Session::flash('success', 'Payment is created successfully');

            return redirect()->back();
        } else {
            Session::flash('error', 'Payment is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function addnewpayment(){
        $users = Customers::select(['id', 'name', 'balance'])->where('deleted', '=', '0')->get();

        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->lists('title', 'id');
        $new_users[0] = 'Select Customer';
        foreach ($users as $user) {
            $new_users[$user->id] = $user->name . ' (BAL: $' . $user->balance . ')';
        }
        $users = $new_users;
        $user_type = 'customer';

        return view('front.payments.sync.index', compact('users', 'user_type', 'user_id', 'payment_sources', 'account_sources'));
        // return view('front.payments.sync.index');
    }

    public function getDetails(Request $request){
        $check = substr($request->val, 0, 2);
       // $val = substr($request->val, 2, 5);
        $val = $request->val;
        // dd($val);
        if($check == 11){
            $invoice = Invoices::where('invoices.deleted', '=', '0')
            ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
            ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
            ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
            ->select('invoices.*','c.id as customer_id','c.balance as customer_balance', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
            ->where('invoices.id',$val)
            ->orderBy('id', 'desc')
            ->first();
            if (isset($invoice) && !empty($invoice->order_id)) {
                $order = Orders::where('orders.deleted', '=', '0')
                ->where('orders.id', $invoice->order_id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id')
                ->orderBy('orders.id', 'desc')
                ->select('orders.*', 'c.id as client_id','c.balance as client_balance','c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
                ->first();

                if($invoice->id == $order->invoice_refrence_id){
                    $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_client', '1')->where('created_by', $invoice->customer_id)->get();
                    $account_sources = AccountTypes::where('deleted', '0')->where('for_client', '1')->where('created_by', $invoice->customer_id)->get();
                    
                    $data['single'] = 0;
                    $data['invoice'] = $invoice;
                    $data['order'] = $order;
                    $data['cli_payment_sources'] = $payment_sources;
                    $data['cli_account_sources'] = $account_sources;

                    return $data;
                }else{
                    return 0;
                }
            }else{
                if(!empty($invoice)){
                    
                    $data['single'] = 1;
                    $data['invoice'] = $invoice;
                    return $data;
                }else{
                    return 0;
                }
            }




        }elseif($check == 22){

            $order = Orders::where('orders.deleted', '=', '0')
            ->where('orders.id', $val)
            ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
            ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
            ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id')
            ->orderBy('orders.id', 'desc')
            ->select('orders.*', 'c.id as client_id','c.balance as client_balance','c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
            ->first();
            
            if(isset($order) && !empty($order->invoice_refrence_id)){
                $invoice = Invoices::where('invoices.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                ->select('invoices.*','c.id as customer_id', 'c.name as customer_name','c.balance as customer_balance', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                ->where('invoices.id',$order->invoice_refrence_id)
                ->orderBy('id', 'desc')
                ->first();

                if($invoice->id == $order->invoice_refrence_id){
                    $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_client', '1')->where('created_by', $invoice->customer_id)->get();
                    $account_sources = AccountTypes::where('deleted', '0')->where('for_client', '1')->where('created_by', $invoice->customer_id)->get();
                    $data['single'] = 0;
                    $data['invoice'] = $invoice;
                    $data['order'] = $order;
                    $data['cli_payment_sources'] = $payment_sources;
                    $data['cli_account_sources'] = $account_sources;
    
                    return $data;
                }else{
                    return 0;
                }
            }else{
                if(!empty($order)){

                    $customer = Customers::where('id',$order->created_by)->first();
                    $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_client', '1')->where('created_by', $customer->id)->get();
                    $account_sources = AccountTypes::where('deleted', '0')->where('for_client', '1')->where('created_by', $customer->id)->get();
                    $invoice['customer_name'] = $customer->name;
                    $invoice['customer_id'] = $customer->id;
                    $order['client_name'] = $order->client_name;
                    $order['client_id'] = $order->client_id;
                    $data['invoice'] = $invoice;
                    $data['order'] = $order;
                    $data['cli_payment_sources'] = $payment_sources;
                    $data['cli_account_sources'] = $account_sources;
                    $data['single'] = 0;
                    return $data;
               }else{
                   return 0;
               }
            }
        }

    }

    public function addSyncPayment(Request $request){
        // dd($request->all());
        
    if (isset($request->customer_payment) && $request->customer_payment == 1) {
        DB::beginTransaction();
        
        try {
            $user_id = Auth::user()->id;
            $payment_id = 0;
            $modification_count = 0;
            $total_modified_amount = $request->amount;
            $payment_type_id = 0;

            if (empty($request->amount)) {
                Session::flash('error', 'Please enter an amount for customer');
                return redirect()->back();
            }
            $user = Customers::where('id', $request->customer_id)->first();
    
            $updated_balance = $user->balance - $request->amount;
    
            $payment = new Payments;
            $payment->amount = $request->amount;
            $payment->user_id = $request->customer_id;
            $payment->message = $request->note;
            $timestamp = strtotime($request->statement_date);
            $payment->statement_date = date("Y-m-d H:i:s", $timestamp);
            $payment->payment_source_id = $request->payment_source_id;
            $payment->account_type_id = $request->account_type_id;
            $payment->modification_count = $modification_count;
            $payment->total_modified_amount = $total_modified_amount;
            $payment->payment_type_id = $payment_type_id;
            if (!empty($request->order_id) && !empty($request->cli_name)) {
                $payment->payment_reference = 'order #'.$request->order_id.' ('.$request->cli_name.')';
            }else{
                 $payment->payment_reference = 'invoice #'.$request->invoice_id.' ('.$request->cus_name.')';
            }
    
            $payment->user_type = 'customer';
            if (Auth::user()->role_id == 2)
                $payment->status = 'approved';
            else
                $payment->status = 'pending';
            $payment->message = $request->note;
            $payment->previous_balance = $user->balance;
            $payment->updated_balance = $updated_balance;
            $payment->created_by = Auth::user()->id;
            if (strpos($request->amount, '-') !== false)
                $payment->transaction_type = 'negative';
            else
                $payment->transaction_type = 'positive';
    
            $payment->save();
    
            Customers::where('id', $request->customer_id)->update(['balance' => $updated_balance]);
            
    
            $account_balance = AccountTypes::where('id', $request->account_type_id)->first();
            AccountTypes::where('id', $request->account_type_id)->increment('amount', $request->amount);
    
            $acc_previous_balance = $account_balance->amount;
            $acc_updated_balance = $account_balance->amount + $request->amount;
    
            $account_type_transaction = new PaymentAccountTransactions;
            $account_type_transaction->account_type_id = $request->account_type_id;
            $account_type_transaction->payment_id = $payment->id;
            $account_type_transaction->source_type = 'payment';
            $account_type_transaction->previous_balance = $acc_previous_balance;
            $account_type_transaction->user_type = 'admin';
            $account_type_transaction->created_by = Auth::user()->id;
            $account_type_transaction->message = '';
            $account_type_transaction->updated_balance = $acc_updated_balance;
            $account_type_transaction->amount = $request->amount;
            $account_type_transaction->save();
    
    
            self::syncPayments($payment->id);
            
            DB::commit();
    
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    if (isset($request->client_payment) && $request->client_payment == 1) {

        $payment_id = 0;
        $modification_count = 0;
        $total_modified_amount = $request->cli_amount;
        $payment_type_id = 0;
        if (empty($request->cli_amount)) {
            Session::flash('error', 'Please enter an amount for client');
            return redirect()->back();
        }if (empty($request->cli_payment_source_id)) {
            Session::flash('error', 'Please Make Payment Source for client');
            return redirect()->back();
        }
        if (empty($request->cli_account_type_id)) {
        Session::flash('error', 'Please Make Account type for client');
        return redirect()->back();
        }
        
        DB::beginTransaction();
        try{
            $user = Clients::where('id', $request->client_id)->first();
    
            $updated_balance = $user->balance - $request->cli_amount;
            $timestamp = strtotime($request->cli_statement_date);
            $statement_date = date("Y-m-d H:i:s", $timestamp);
    
            $payment = new Payments;
            $payment->amount = $request->cli_amount;
            $payment->user_id = $request->client_id;
            $payment->created_by = $request->customer_id;
            $payment->account_type_id = $request->cli_account_type_id;
            $payment->message = $request->cli_note;
            $payment->user_type = 'client';
            $payment->status = 'approved';
            
            $payment->payment_source_id = $request->cli_payment_source_id;
            $payment->message = $request->cli_note;
            $payment->statement_date = $statement_date;
            $payment->modification_count = $modification_count;
            $payment->total_modified_amount = $total_modified_amount;
            $payment->payment_type_id = $payment_type_id;
            $payment->previous_balance = $user->balance;
            $payment->updated_balance = $updated_balance;
            
            if (!empty($request->invoice_id) && !empty($request->cus_name)) {
                $payment->payment_reference = 'invoice #'.$request->invoice_id.' ('.$request->cus_name.')';
            }
            
            if (strpos($request->cli_amount, '-') !== false)
                $payment->transaction_type = 'negative';
            else
                $payment->transaction_type = 'positive';
    
            $payment->save();
    
            Clients::where('id', $request->client_id)->update(['balance' => $updated_balance]);
            CommonController::updateAccountTransaction($request->cli_account_type_id, $request->cli_amount, 'payment', $payment->id, 'customer');
    
            self::syncPayments($payment->id);
            
            DB::commit();
    

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
    if(!isset($request->client_payment) && !isset($request->customer_payment)){
        Session::flash('error', 'Please tick atleast one checkbox');
        return redirect()->back();
    }

    Session::flash('success', 'Payment is created successfully');
    return redirect()->back();        
        
    }

}
