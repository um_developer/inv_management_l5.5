<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\ReturnOrders;
use App\ReturnOrderItems;
use App\Customers;
use App\CreditMemo;
use App\PrintPageSetting;
use App\CreditMemoItems;
use App\ItemPriceTemplates;
use App\PriceTemplates;
use App\Clients;
use App\Inventory;
use Illuminate\Http\Request;
use App\Payments;
use Auth;
use Session;
use Carbon\Carbon;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;
use App\Functions\Functions;
use App\Bundles;
use App\BundleItems;

class CustomerOrderReturnsController extends AdminController {

    private $user_id = '0';
    private $customer_id = '0';
    private $customer_obj = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();
        $this->customer_id = $customer[0]->id;
        $this->show_bundle = $customer[0]->show_bundle;
        $this->customer_obj = $customer[0];
    }

    public function index() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $model = ReturnOrders::where('return_orders.deleted', '=', '0')
                ->where('return_orders.created_by', $this->customer_id)
                ->whereBetween('return_orders.statement_date', [$start_date, $end_date])
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                ->select('return_orders.*', 'c.name as client_name', 'c.u_id as client_u_id')
                ->orderBy('id', 'desc')
                ->get();

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->get();

        $new_cat [0] = 'Select Client';
        //$new_cat = [];
        $template_name = '';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $clients = $new_cat;
        $selected_report_type = '';
        $date = $start_date . ' - ' . $end_date;

        return view('front.customers.order_returns.index', compact('model', 'date','clients','selected_report_type'));
    }

    public function search(Request $request) {


        $date = $request->date_range;

        if(isset($request->client_id)){
            $client_id = $request->client_id;
        }
        if(isset($request->type)){
            $selected_report_type = $request->type;
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $model = ReturnOrders::where('return_orders.deleted', '=', '0')
                ->where('return_orders.created_by', $this->customer_id)
                ->whereBetween('return_orders.statement_date', [$start_date, $end_date])
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id');
            if (!empty($selected_report_type)){
            
                $model = $model->where('return_orders.status', $selected_report_type);
            }
            if (!empty($client_id)){
                
                $model = $model->where('return_orders.client_id', $client_id);
            }
            
            $model = $model->select('return_orders.*', 'c.name as client_name', 'c.u_id as client_u_id')
                ->orderBy('id', 'desc')
                ->get();

        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->get();

        $new_cat [0] = 'Select Client';
        //$new_cat = [];
        $template_name = '';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $clients = $new_cat;
        
        return view('front.customers.order_returns.index', compact('model', 'date', 'clients', 'selected_report_type'));
    }

    public function create($client_id = '0') {

        $clients = Clients::select(['id', 'name'])->where('deleted', '=', '0')->where('customer_id', '=', $this->customer_id)->get();

//        if (count($clients) > 0 && $client_id == '0')
//            return redirect('/customer/order-return/create/' . $clients[0]->id);

        $address = '';
        $phone = '';
        $template_name = '';
        $new_cat [0] = 'Select Client';
//        $new_cat = [];
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $client_sale_price_factor = 1;
        $client_name = '';
        $new_template_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($this->customer_id);
        if ($client_id != '0') {
            $client = Clients::where('id', $client_id)
                    ->leftjoin('states as s', 's.code', '=', 'clients.state')
                    ->select('clients.*', 's.title as state')
                    ->first();
            $address = $client->address1 . ' ' . $client->address2 . ' ' . $client->city . ' ' . $client->state . ' ' . $client->zip_code;
            $client_name = $client->name;
            $phone = $client->phone;
            $template = PriceTemplates::where('id', $client->selling_price_template)->first();


            if ($client->selling_price_template != '0' && $client->selling_price_template != null) {
                $template = PriceTemplates::where('id', $client->selling_price_template)->first();

                if (count($template) > 0) {
                    $template_name = $template->title;
                    $items = CommonController::getItemFormsData($client->selling_price_template);
                } else {
                    $template_name = 'Customer Default Template';
                    $items = CommonController::getItemFormsData($new_template_id);
                }
            } else {
                $template_name = 'Customer Default Template';
                $items = CommonController::getItemFormsData($new_template_id);
            }
        } else {

            $items = CommonController::getItemFormsData($new_template_id);
        }

        $clients = $new_cat;
   
  $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=  $new_bundle;
        $show_bundle=$this->show_bundle;
        return view('front.customers.order_returns.create', compact('items', 'clients', 'client_id', 'client_name', 'address', 'template_name', 'phone','bundles','show_bundle'));
    }

    public function edit($id) {

        $model = ReturnOrders::where('return_orders.id', '=', $id)
                ->where('return_orders.created_by', '=', $this->customer_id)
                ->get();
        $address = '';
        $phone = '';
        $template_name = '';

        if (count($model) == 0) {
            return redirect('/');
        }
//        d($model); die;
        $model = ReturnOrders::where('return_orders.id', '=', $id)
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                ->select('return_orders.*', 'c.id as client_id')
                ->get();


        $client_name = '';
        $new_template_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($this->customer_id);
        if ($model[0]->client_id != 0) {
            $client = Clients::where('id', $model[0]->client_id)
                    ->leftjoin('states as s', 's.code', '=', 'clients.state')
                    ->select('clients.*', 's.title as state')
                    ->first();
            $address = $client->address1 . ' ' . $client->address2 . ' ' . $client->city . ' ' . $client->state . ' ' . $client->zip_code;
            $client_name = $client->name;
            $phone = $client->phone;
            $template = PriceTemplates::where('id', $client->selling_price_template)->first();

            if ($client->selling_price_template != '0' && $client->selling_price_template != null) {
                $template = PriceTemplates::where('id', $client->selling_price_template)->first();

                if (count($template) > 0) {
                    $template_name = $template->title;
                    $items = CommonController::getItemFormsData($client->selling_price_template);
                } else {
                    $template_name = 'Customer Default Template';
                    $items = CommonController::getItemFormsData($new_template_id);
                }
            } else {
                $template_name = 'Customer Default Template';
                $items = CommonController::getItemFormsData($new_template_id);
            }
        } else {

            $items = CommonController::getItemFormsData($new_template_id);
        }

        $credit_memo_items = ReturnOrderItems::where('return_order_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'return_order_items.item_id')
                ->where('return_order_items.deleted', '=', '0')
                ->select('return_order_items.*', 'i.name as item_name', 'i.cost as cost')
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=  $new_bundle;
        $show_bundle=$this->show_bundle;
        return view('front.customers.order_returns.edit', compact('items', 'model', 'credit_memo_items', 'client_name', 'address', 'template_name', 'phone','bundles','show_bundle'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $client_id = $request->client_id;
        $validation = array(
        );

        if ($input['client_id'] == '0') {
            Session::flash('error', 'Please select client.');
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);
        DB::beginTransaction();
        $items = $input['num'];
        $return_order = new ReturnOrders;
        $return_order->created_by = $this->customer_id;
        $return_order->client_id = $client_id;
        $return_order->memo = $request->memo;
        $return_order->statement_date = $statement_date;
        $return_order->deleted = '1';

        $return_order->save();

        $total_invoice_price = 0;
        $total_quantity = 0;
        if (count($items) > 0 && isset($return_order->id)) {

            for ($i = 1; $i <= count($items); $i++) {
                if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];

                    $credit_memo_items = new ReturnOrderItems;
                    $credit_memo_items->return_order_id = $return_order->id;
                    $credit_memo_items->client_id = $client_id;
                    $credit_memo_items->item_id = $input['item_id_' . $i];
                    $credit_memo_items->item_unit_price = $input['price_' . $i];
                    $credit_memo_items->quantity = $input['quantity_' . $i];
                    $credit_memo_items->statement_date = $statement_date;


                    if ($input['start_serial_number_' . $i] > 0) {

                        $credit_memo_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['end_serial_number_' . $i] > 0) {
                            $credit_memo_items->end_serial_number = $input['end_serial_number_' . $i];
                            $credit_memo_items->quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                            ;
                        } else {
                            $credit_memo_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $credit_memo_items->quantity = $input['quantity_' . $i];
                        }
                    }
                    if (isset($input['item_bundle_id_' . $i])) {
                        $credit_memo_items->bundle_id = $input['item_bundle_id_' . $i];
                        $credit_memo_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }

                    $credit_memo_items->total_price = $item_total_price;
//                        $credit_memo_items->created_by = $user_id;
                    $total_invoice_price += $credit_memo_items->total_price;
                    $total_quantity += $credit_memo_items->quantity;
                    $credit_memo_items->save();
                }
            }

            if ($total_quantity == 0) {
                DB::rollBack();
                Session::flash('error', 'Item count is greater than 0.');
                return redirect()->back();
            }

            ReturnOrders::where('id', '=', $return_order->id)->update([
                'deleted' => 0,
                'total_price' => $total_invoice_price,
                'total_quantity' => $total_quantity,
            ]);
//            } else {
//                DB::rollBack();
//                Session::flash('error', 'Credit Memo is not created. Please add atleast 1 item.');
//                return redirect()->back();
//            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Return Order is not created. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Return Order is created successfully');
        return redirect('customer/order-return/' . $return_order->id);
        return redirect()->back();
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $client_id = $request->client_id;

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $return_order = ReturnOrders::where('id', '=', $input['id'])->first();
        DB::beginTransaction();
        ReturnOrders::where('id', '=', $input['id'])->update([
            'memo' => $input['memo'],
            'statement_date' => $statement_date,
        ]);

        $invoice_total_price = 0;
        $total_quantity = 0;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $credit_memo_item = ReturnOrderItems::where('id', '=', $input['po_item_id_' . $i])->get();
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        ReturnOrderItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {
                            $end_serial_num = 0;
                            $serial_quantity = 0;
                            ///////// end serial num or quantity /////
                            if ($input['end_serial_number_' . $i] > 0) {
                                $end_serial_num = $input['end_serial_number_' . $i];
                                $serial_quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                            } else {
                                $serial_quantity = $input['quantity_' . $i];
                                $end_serial_num = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            }

                            if ($total_quantity == 0) {
                                DB::rollBack();
                                Session::flash('error', 'Item count is greater than 0.');
                                return redirect()->back();
                            }

                            ReturnOrderItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $serial_quantity,
                                'total_price' => $item_total_price,
                                'start_serial_number' => $input['start_serial_number_' . $i],
                                'end_serial_number' => $end_serial_num,
                                'statement_date' => $statement_date,
                            ]);
                        } else {
                            ReturnOrderItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $return_order_items = new ReturnOrderItems;
                    $return_order_items->return_order_id = $input['id'];
                    $return_order_items->client_id = $client_id;
                    $return_order_items->item_id = $input['item_id_' . $i];
                    $return_order_items->item_unit_price = $input['price_' . $i];
                    $return_order_items->total_price = $item_total_price;
                    $return_order_items->quantity = $input['quantity_' . $i];
                    $return_order_items->statement_date = $statement_date;

                    if (isset($input['item_package_id_' . $i]))
                        $return_order_items->package_id = $input['item_package_id_' . $i];

                    if ($input['start_serial_number_' . $i] > 0) {
                        $return_order_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['end_serial_number_' . $i] > 0) {
                            $return_order_items->end_serial_number = $input['end_serial_number_' . $i];
                            $return_order_items->quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                            ;
                        } else {
                            $return_order_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $return_order_items->quantity = $input['quantity_' . $i];
                        }
                    }

                    if (isset($input['item_bundle_id_' . $i])) {
                        $return_order_items->bundle_id = $input['item_bundle_id_' . $i];
                        $return_order_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }
                    $return_order_items->quantity = $return_order_items->quantity;
//                    $credit_memo_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $return_order_items->quantity;
                    $return_order_items->save();
//                    print_r($invoice_items); die;
                }
            }

            if ($invoice_total_price > 0) {
                ReturnOrders::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Return Order is not updated. Please add atleast 1 proper item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Return Order is not updated. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Return Order is updated successfully');
        return redirect()->back();
    }

    public function postModified(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $client_id = $request->client_id;

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $return_order = ReturnOrders::where('id', '=', $input['id'])->first();
        DB::beginTransaction();
        ReturnOrders::where('id', '=', $input['id'])->update([
            'memo' => $input['memo'],
            'statement_date' => $statement_date,
        ]);

        $invoice_total_price = 0;
        $total_quantity = 0;
        $old_total_price = $return_order->total_price;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $credit_memo_item = ReturnOrderItems::where('id', '=', $input['po_item_id_' . $i])->first();
                    $old_quantity = $credit_memo_item->quantity;
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        ReturnOrderItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {

                            //// no serial functionality
                        } else {
                            ReturnOrderItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $return_order_items = new ReturnOrderItems;
                    $return_order_items->return_order_id = $input['id'];
                    $return_order_items->client_id = $client_id;
                    $return_order_items->item_id = $input['item_id_' . $i];
                    $return_order_items->item_unit_price = $input['price_' . $i];
                    $return_order_items->total_price = $item_total_price;
                    $return_order_items->quantity = $input['quantity_' . $i];
                    $return_order_items->statement_date = $statement_date;

                    if (isset($input['item_package_id_' . $i]))
                        $return_order_items->package_id = $input['item_package_id_' . $i];

                    if ($input['start_serial_number_' . $i] > 0) {
                        $return_order_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['end_serial_number_' . $i] > 0) {
                            $return_order_items->end_serial_number = $input['end_serial_number_' . $i];
                            $return_order_items->quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                            ;
                        } else {
                            $return_order_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $return_order_items->quantity = $input['quantity_' . $i];
                        }
                    }


                    $return_order_items->quantity = $return_order_items->quantity;
//                    $credit_memo_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $return_order_items->quantity;
                    $return_order_items->save();
//                    print_r($invoice_items); die;
                }
            }

//            if ($invoice_total_price > 0) {
                ReturnOrders::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);

                $price_diff = 0;
                if ($old_total_price > $invoice_total_price) {

                    $price_diff = $old_total_price - $invoice_total_price;
                    PaymentController::updateClientBalance($return_order->client_id, $price_diff, $return_order->id, 'order_return_pos', $statement_date);
                } elseif ($old_total_price < $invoice_total_price) {
                    $price_diff = $invoice_total_price - $old_total_price;
                    PaymentController::updateClientBalance($return_order->client_id, $price_diff, $return_order->id, 'order_return', $statement_date);
                } else {
                    /// do nothing
                    Payments::where('payment_type', 'order_return')->where('payment_type_id', $return_order->id)->update(['statement_date' => $statement_date]);
                }

                self::updateProfit($return_order->id);
//            } else {
//                DB::rollBack();
//                Session::flash('error', 'Return Order is not Modified. Please add atleast 1 proper item.');
//                return redirect()->back();
//            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Return Order is not Modified. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Return Order is Modified successfully');
        return redirect()->back();
    }

    public function delete($id) {
        $model = ReturnOrders::where('return_orders.id', '=', $id)
                ->where('return_orders.status', '!=', 'pending')
                ->where('return_orders.created_by', '=', $this->customer_id)
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }

        ReturnOrderItems::where('id', '=', $id)->update(['deleted' => 1]);
        ReturnOrders::where('id', '=', $id)->where('status', '=', 'pending')->where('created_by', '=', $this->customer_id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id, $show_profit = 0) {

        self::updateProfit($id);
        $model = ReturnOrders::where('return_orders.id', '=', $id)
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                ->where('return_orders.deleted', '=', '0')
                ->select('return_orders.*', 'c.id as client_id', 'c.name as client_name')
                ->get();

        $po_items = ReturnOrderItems::where('return_order_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'return_order_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'return_order_items.bundle_id')
                ->where('return_order_items.deleted', '=', '0')
                ->select('return_order_items.*', 'i.name as item_name','b.name as bundle_name')
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        $extra = '';
        return view('front.customers.order_returns.detail', compact('model', 'po_items', 'extra', 'show_profit'));
    }

    public function printPage($id) {

        $model = ReturnOrders::where('return_orders.id', '=', $id)
                ->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                ->leftjoin('states as s', 's.code', '=', 'c.state')
                ->where('return_orders.deleted', '=', '0')
                ->select('return_orders.*', 'c.id as client_id', 's.title as state', 'c.name as client_name', 'c.address1 as client_address1', 'c.address2 as client_address2', 'c.phone as client_phone', 'c.show_image', 'c.show_serial', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code')
                //  ->select('return_orders.*', 'c.id as client_id', 'c.name as client_name')
                ->get();

        $customer = Customers::where('user_id', $this->user_id)
                ->leftjoin('states as s', 's.code', '=', 'customers.state')
                ->select('customers.*', 's.title as state')
                ->first();

        $po_items = ReturnOrderItems::where('return_order_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'return_order_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'return_order_items.bundle_id')
                ->where('return_order_items.deleted', '=', '0')
                ->select('return_order_items.*', 'i.name as item_name','b.name as bundle_name')
                ->get();


        if (count($model) == 0)
            return redirect('/');

        $user_id = $this->customer_id;
        $setting = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id', $user_id)->first();

        if (count($setting) < 1) {
            $printPageSetting = new PrintPageSetting;
            $printPageSetting->bottom_text_1 = 'FOR ACCESSORIES VISIT BROADPOSTERS.COM';
            $printPageSetting->bottom_text_2 = 'NOTICE: OPEN ITEMS ARE NON REFUNDABLE. THANK YOU';
            $printPageSetting->user_type = 'customer';
            $printPageSetting->user_id = $user_id;
            $printPageSetting->save();
        }

        $setting = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id', $user_id)->first();

        $model = $model[0];
        $extra = '';
        return view('front.customers.order_returns.print', compact('model', 'po_items', 'extra', 'customer', 'setting'));
    }

    public function returnOrderSendToAdmin($status, $order_id) {

        $return_order = ReturnOrders::where('return_orders.deleted', '=', '0')
                ->where('id', $order_id)
                ->where('status', 'pending')
                ->first();

        $invoice_total_price = 0;

        $return_order_items = ReturnOrderItems::where('deleted', '0')
                ->where('return_order_id', $return_order->id)
                ->get();
        if (count($return_order_items) == 0) {

            Session::flash('error', 'Some error occured.');
            return redirect()->back();
        }

        $client = Clients::where('id', $return_order->client_id)->first();
        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        foreach ($return_order_items as $return_order_item) {

            $item_cost = CustomerOrderController::getClientItemCost($this->customer_obj->selling_price_template, $return_order_item->item_id);

            ReturnOrderItems::where('id', $return_order_item->id)->update([
                'item_unit_price' => $return_order_item->item_unit_price,
                'total_price' => $return_order_item->total_price,
                'item_unit_cost' => $item_cost,
                'total_cost' => $item_cost * $return_order_item->quantity,
                'loss' => $return_order_item->total_price - ($item_cost * $return_order_item->quantity)
            ]);

            $total_price = $total_price + $return_order_item->total_price;
            $total_cost = $total_cost + ($item_cost * $return_order_item->quantity);
            $total_profit = $total_price - $total_cost;
        }

        $message = 'Return Order has been charged to client Successfully.';
        $status_type = 'approved_charged';


        if ($status == 'credit-memo') {
            DB::beginTransaction();
            $credit_memo = new CreditMemo;
            $credit_memo->created_by = 'customer';
            $credit_memo->customer_id = $this->customer_id;
            $credit_memo->return_order_id = $return_order->id;
            $credit_memo->memo = $return_order->memo;
            $credit_memo->total_price = $return_order->total_price;
            $credit_memo->total_quantity = $return_order->total_quantity;
            $credit_memo->statement_date = $return_order->statement_date;
            $credit_memo->deleted = '0';
            $credit_memo->save();

            if (!isset($credit_memo->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect()->back();
            }

            $credit_memo_id = $credit_memo->id;
            foreach ($return_order_items as $item) {

                $customer = Customers::where('id', $this->customer_id)->first();
                $my_item_id = $item->item_id;
                $new_item = Items::where('id', $item->item_id)->first();
                if ($new_item->parent_item_id != '0')
                    $item->item_id = $new_item->parent_item_id;

                $item_exist = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id', $item->item_id)->first();
                $selling_price = 0;
                if (count($item_exist) > 0)
                    $selling_price = $item_exist->selling_price;

                $invoice_items = new CreditMemoItems;
                $invoice_items->credit_memo_id = $credit_memo_id;
                $invoice_items->customer_id = $this->customer_id;
                $invoice_items->item_id = $my_item_id;
                $invoice_items->quantity = $item->quantity;
                $invoice_items->item_unit_price = $selling_price;
                $invoice_items->statement_date = $return_order->statement_date;
                $invoice_items->total_price = $invoice_items->item_unit_price * $invoice_items->quantity;
                $invoice_total_price = $invoice_total_price + $invoice_items->total_price;

                $invoice_items->save();
            }

            CreditMemo::where('id', $credit_memo_id)->update(['total_price' => $invoice_total_price]);
            $message = 'Return Order has been credited to client and send to Admin Successfully.';
            $status_type = 'approved';
        }


        PaymentController::updateClientBalance($return_order->client_id, $return_order->total_price, $return_order->id, 'order_return', $return_order->statement_date);
        ReturnOrders::where('id', $return_order->id)->update(['status' => $status_type, 'total_price' => $total_price, 'total_cost' => $total_cost, 'loss' => $total_profit]);
        DB::commit();
        Session::flash('success', $message);
        return redirect()->back();
    }

    private function updateProfit($return_order_id) {

        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;

        $return_order_items = ReturnOrderItems::where('return_order_id', $return_order_id)->where('deleted', '0')->get();

        foreach ($return_order_items as $return_order_item) {

            $item_cost = CustomerOrderController::getClientItemCost($this->customer_id, $return_order_item->item_id);

            ReturnOrderItems::where('id', $return_order_item->id)->update([
                'item_unit_price' => $return_order_item->item_unit_price,
                'total_price' => $return_order_item->total_price,
                'item_unit_cost' => $item_cost,
                'total_cost' => $item_cost * $return_order_item->quantity,
                'loss' => $return_order_item->total_price - ($item_cost * $return_order_item->quantity)
            ]);

            $total_price = $total_price + $return_order_item->total_price;
            $total_cost = $total_cost + ($item_cost * $return_order_item->quantity);
            $total_profit = $total_price - $total_cost;
        }

        ReturnOrders::where('id', $return_order_id)->update(['total_price' => $total_price, 'total_cost' => $total_cost, 'loss' => $total_profit]);
    }

}
