<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\PoItems;
use App\Irems;
use App\PurchaseOrders;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\InvoiceItems;
use App\Invoices;
use App\Bundles;
use App\BundleItems;
use App\Http\Controllers\ItemController;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class PurchaseOrderController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | PurchaseOrder Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct() {
//        $this->middleware('auth');
//        if(Auth::user()->role_id != '2'){
//            Redirect::to('customer/dashboard')->send(); die;
//        }
     
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
    }

    public function index() {

        $model = PurchaseOrders::where('purchase_orders.deleted', '=', '0')
                ->leftjoin('vendors as v', 'v.id', '=', 'purchase_orders.vendor_id')
                ->select('purchase_orders.*', 'v.name as vendor_name', 'v.u_id as vendor_u_id')
                ->orderBy('id', 'desc')
                ->get();
        return view('front.purchase_order.index', compact('model'));
    }

    public function create() {

        $vendors = Vendors::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        foreach ($vendors as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $search_model = Items::where('items.deleted', '=', '0')
                ->where('serial', 'no')
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '=', '0')
                ->where('items.status', '1')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->select('items.*', 'c.name as category_name')
//                 ->with('ItemColors.Colors', 'ItemSizes.Sizes', 'ItemTypes.Types')
                ->get();

        $items = [];
        $i = 0;
        foreach ($search_model as $item) {
            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['code'] = $item->code;
            $items[$i]['name'] = $item->name;
            $items[$i]['has_sub_item'] = $item->has_sub_item;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;

//            $items[$i]['ItemColors'] = $item->ItemColors->toArray();
//            $items[$i]['ItemSizes'] = $item->ItemSizes->toArray();
//            $items[$i]['ItemTypes'] = $item->ItemTypes->toArray();


            $i++;
        }

//        print_r($items); die;
        $vendors = $new_cat;
       
      $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
        return view('front.purchase_order.create', compact('vendors', 'items','bundles'));
    }

    public function edit($id) {

        $vendors = Vendors::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $model = PurchaseOrders::where('purchase_orders.id', '=', $id)
                ->where('purchase_orders.status', '!=', 'draft')
//                  ->orWhere('purchase_orders.deleted', '!=', '0')
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }


        $new_cat = [];
        foreach ($vendors as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $search_model = Items::where('items.deleted', '=', '0')
                ->where('serial', 'no')
                ->where('items.parent_item_id', '=', '0')
                ->where('items.status', '1')
                ->where('items.deleted', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->select('items.*', 'c.name as category_name')
                ->get();
//       print_r($model); die;
        $items = [];
        $i = 0;
        foreach ($search_model as $item) {

            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->upc_barcode;
            $items[$i]['code'] = $item->code;
            $items[$i]['name'] = $item->name;
            $items[$i]['has_sub_item'] = $item->has_sub_item;
            $items[$i]['id'] = $item->id;
            $items[$i]['cost'] = $item->cost;
            $items[$i]['value'] = $item->name;
            $items[$i]['serial'] = $item->serial;

            $i++;
        }

        $model = PurchaseOrders::where('purchase_orders.id', '=', $id)
                ->leftjoin('vendors as v', 'v.id', '=', 'purchase_orders.vendor_id')
                ->select('purchase_orders.*', 'v.id as vendor_id')
                ->get();

        $po_items = PoItems::where('po_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'po_items.item_id')
                ->where('po_items.deleted', '=', '0')
                ->select('po_items.*', 'i.name as item_name')
                ->get();

        $vendors = $new_cat;
        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
        return view('front.purchase_order.edit', compact('items', 'vendors', 'model', 'po_items','bundles'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        // echo '<pre>'; print_r($input);echo '</pre>'; die;
        $user_id = Auth::user()->id;
        $validation = array(
//            'name' => 'required|max:20',
//            'sku' => 'max:8|unique:items',
//            'sale_price' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $items = $input['num'];
       // dd($input);
        $purchase_order = new PurchaseOrders;
        $purchase_order->created_by = $user_id;
        $purchase_order->vendor_id = $request->vendor_id;
        $purchase_order->tracking_info = $request->tracking_info;
        $purchase_order->created_by = $user_id;
        $purchase_order->deleted = '1';
        $purchase_order->save();
//        $purchase_order->id = 00;

        $total_cost = 0;
        $total_quantity = 0;

        if (count($items) > 0 && isset($purchase_order->id)) {

            if ($input['item_id_1'] != '' || $input['item_id_2'] != '') {

                for ($i = 1; $i <= count($items); $i++) {

                    if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {
                        $po_items = new PoItems;
                        $po_items->po_id = $purchase_order->id;
                        $po_items->vendor_id = $input['vendor_id'];
                        $po_items->item_id = $input['item_id_' . $i];
                        $po_items->item_unit_price = $input['price_' . $i];
                        $po_items->total_price = $input['total_' . $i];
                        if (isset($input['item_bundle_id_' . $i])) {
                            $po_items->bundle_id = $input['item_bundle_id_' . $i];
                            $po_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                        }
                        $po_items->quantity = $input['quantity_' . $i];
                        $po_items->created_by = $user_id;
                        $total_cost += $po_items->total_price;
                        $total_quantity += $po_items->quantity;
                        $po_items->save();
                        
                        
                       // self::updateItemCostFromPO($input['item_id_' . $i]);
                        
                         $purchase_order_id = $purchase_order->id;
                         $old_profit  = 0;
                        $invoice_items = InvoiceItems::where('item_id', $input['item_id_' . $i])->where('purchase_order_id', '0')->get();
                        
//                        echo count($invoice_items); die;
                        if (!empty($invoice_items)) {
                            foreach ($invoice_items as $item) {

                                $old_profit = $item->profit;
                                $new_profit = $item->total_price - ($input['price_' . $i] *$item->quantity);
                                
                                $profit_diff = $old_profit - $new_profit;
                                
                                InvoiceItems::where('id', $item->id)->update([
                                    'item_unit_cost' => $input['price_' . $i],
                                    'total_cost' => $input['price_' . $i] * $item->quantity ,
                                    'purchase_order_id' => $purchase_order_id,
                                     'profit' => $item->total_price - ($input['price_' . $i] *$item->quantity) ,
                                ]);
                                
                                Invoices::where('id',$item->invoice_id)->increment('total_cost', $input['price_' . $i]);
                                Invoices::where('id',$item->invoice_id)->increment('profit', $profit_diff);
                                
                            }
                        }

                        
                    }
                }

                if ($total_cost > 0) {
                    PurchaseOrders::where('id', '=', $purchase_order->id)->update([
                        'deleted' => 0,
                        'total_cost' => $total_cost,
                        'total_quantity' => $total_quantity,
                    ]);
                } else {
                    Session::flash('error', 'Purchase Order is not created. Please add atleast 1 proper item.');
                    return redirect()->back();
                }
            } else {
                Session::flash('error', 'Purchase Order is not created. Please add atleast 1 item.');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Purchase Order is not created. Please try again.');
            return redirect()->back();
        }

        Session::flash('success', 'Purchase Order is created successfully');
        return redirect()->back();
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
    
        // echo '<pre>'; print_r($input); echo '</pre>'; die();
        $items = $input['num'];
        $user_id = Auth::user()->id;

        $purchase_order = PurchaseOrders::where('id', '=', $input['id'])->get();

        PurchaseOrders::where('id', '=', $input['id'])->update([
            'vendor_id' => $input['vendor_id'],
            'tracking_info' => $input['tracking_info'],
        ]);

        $purchase_order = $purchase_order[0];

        $total_cost = 0;
        $total_quantity = 0;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if po_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {
                    $total_cost += $input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $po_item = PoItems::where('id', '=', $input['po_item_id_' . $i])->get();
              
                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0) {
                        PoItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        PoItems::where('id', '=', $input['po_item_id_' . $i])->update([
                            'item_unit_price' => $input['price_' . $i],
                            'quantity' => $input['quantity_' . $i],
                            'total_price' => $input['total_' . $i],
                        ]);
                    }
                    // self::updateItemCostFromPO($input['item_id_' . $i]);
                }
                ///////// if new item ////
                else if(isset($input['item_id_' . $i]) &&  $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {
                    // echo 'in1'; die();
                    $po_items = new PoItems;
                    $po_items->po_id = $input['id'];
                    $po_items->vendor_id = $input['vendor_id'];
                    $po_items->item_id = $input['item_id_' . $i];
                    $po_items->item_unit_price = $input['price_' . $i];
                    $po_items->total_price = $input['total_' . $i];
                    $po_items->quantity = $input['quantity_' . $i];
                    $po_items->created_by = $user_id;
                    $total_cost += $po_items->total_price;
                    $total_quantity += $po_items->quantity;
                    if (isset($input['item_bundle_id_' . $i])) {
                        $po_items->bundle_id = $input['item_bundle_id_' . $i];
                        $po_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }
                    $po_items->save();
                    
                   //  self::updateItemCostFromPO($input['item_id_' . $i]);
                }
               
                $purchase_order_id = $input['id'];
              //  $invoice_items = InvoiceItems::where('item_id', $input['item_id_' . $i])->where('profit', '0.00')->get();
              if(isset($input['item_id_' . $i])){
                $invoice_items = InvoiceItems::where('item_id', $input['item_id_' . $i])->where('purchase_order_id', '0')->get();
                        if (!empty($invoice_items)) {
                            foreach ($invoice_items as $item) {

                                $old_profit = $item->profit;
                                $new_profit = $item->total_price - ($input['price_' . $i] *$item->quantity);
                                
                                $profit_diff = $old_profit - $new_profit;
                                
                                InvoiceItems::where('id', $item->id)->update([
                                    'item_unit_cost' => $input['price_' . $i],
                                    'total_cost' => $input['price_' . $i] * $item->quantity ,
                                    'purchase_order_id' => $purchase_order_id,
                                     'profit' => $item->total_price - ($input['price_' . $i] *$item->quantity) ,
                                ]);
                                
                                Invoices::where('id',$item->invoice_id)->increment('total_cost', $input['price_' . $i]);
                                Invoices::where('id',$item->invoice_id)->increment('profit', $profit_diff);
                                
                            }
                        }
                    }  
            }

            if ($total_cost > 0) {
                PurchaseOrders::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_cost' => $total_cost,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                Session::flash('error', 'Purchase Order is not updated. Please add atleast 1 proper item.');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Purchase Order is not created. Please try again.');
            return redirect()->back();
        }

        Session::flash('success', 'Purchase Order is updated successfully');
        return redirect()->back();
    }

    public function delete($id) {

        $model = PurchaseOrders::where('purchase_orders.id', '=', $id)
                ->where('purchase_orders.status', '!=', 'draft')
//                  ->orWhere('purchase_orders.deleted', '!=', '0')
                ->get();

        if (count($model) > 0) {
            Session::flash('error', 'This purchase order is deleted or invalid.');
            return redirect()->back();
        }

          PurchaseOrders::where('id', '=', $id)->where('status', '=', 'draft')->update(['deleted' => 1]);
        $po_items = PoItems::where('po_id', '=', $id)->get();
        if(!empty($po_items)){
            PoItems::where('po_id', '=', $id)->update(['deleted' => 1]);
        }
        $invoice_items = InvoiceItems::where('purchase_order_id',  $id)->groupby('invoice_id')->get();
      
        if(!empty($invoice_items)){
            foreach ($invoice_items as $key => $item) {
              // echo  $item->invoice_id;
              InvoiceController::updateProfit($item->invoice_id);
              //$this->ItemController->updateProfit($item->invoice_id);
            }
        }
      
          Session::flash('success', 'Successfully Deleted!');
         return redirect()->back();
    }
    
    public function detail($id) {

        $model = PurchaseOrders::where('purchase_orders.id', '=', $id)
                ->leftjoin('vendors as v', 'v.id', '=', 'purchase_orders.vendor_id')
                ->where('purchase_orders.deleted', '=', '0')
                ->select('purchase_orders.*', 'v.id as vendor_id', 'v.name as vendor_name')
                ->get();

        $po_items = PoItems::where('po_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'po_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'po_items.bundle_id')
                ->where('po_items.deleted', '=', '0')
                ->select('po_items.*', 'i.name as item_name','b.name as bundle_name')
                ->get();
               


                if (count($model) == 0) {
                    Session::flash('error', 'This purchase order is deleted or invalid.');
                    return redirect()->back();
                }
        $model = $model[0];
        return view('front.purchase_order.detail', compact('model', 'po_items'));
    }

    public function printPage($id) {

        $model = PurchaseOrders::where('purchase_orders.id', '=', $id)
                ->leftjoin('vendors as v', 'v.id', '=', 'purchase_orders.vendor_id')
                ->where('purchase_orders.deleted', '=', '0')
                ->select('purchase_orders.*', 'v.id as vendor_id', 'v.name as vendor_name')
                ->get();

        $po_items = PoItems::where('po_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'po_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'po_items.bundle_id')
                ->where('po_items.deleted', '=', '0')
                ->select('po_items.*', 'i.name as item_name','b.name as bundle_name')
                ->get();
          

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.purchase_order.print', compact('model', 'po_items'));
    }

    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }
    
    private function updateItemCostFromPO($item_id){
        
        $new_item = Items::where('id', $item_id)->first();
                    $latest_po_price = PoItems::where('deleted','0')->where('item_id',$item_id)->orderBy('id','desc')->first();
                    $latest_po_item_price = $latest_po_price->item_unit_price;
                     DB::table('items')
                                    ->where('id', $item_id)
                                    ->update(['cost' => $latest_po_item_price]);

                    if ($new_item->has_sub_item == '1') {

                        DB::table('items')
                                ->where('id', $item_id)
                                ->update(['cost' => $latest_po_item_price]);

                        DB::table('items')
                                ->where('parent_item_id', $item_id)
                                ->update(['cost' => $latest_po_item_price]);
                    }

                    if ($new_item->parent_item_id != '0') {

                        DB::table('items')
                                ->where('parent_item_id', $new_item->parent_item_id)
                                ->update(['cost' => $latest_po_item_price]);

                        DB::table('items')
                                ->where('id', $new_item->parent_item_id)
                                ->update(['cost' => $latest_po_item_price]);
                    }
        
        
    }

}
