<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\User;
use App\States;
use App\Address;
use App\Content;
use App\Role;
use App\Countries;
use App\Functions\Functions;
use Auth;
use Session;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\RedirectResponse;
use App\Clients;

class SignupController extends Controller {

   // use AuthenticatesAndRegistersUsers;

    protected $loginPath = 'login';

    public function __construct(Guard $auth) {

        $this->auth = $auth;
        //$this->registrar = $registrar;
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function index() {

        return view('front.customers.login');
    }

    public function forgot_password() {
        return view('front.customers.forgot');
    }

    public function reset_password(Request $request) {


        $validation = array(
            'email' => 'required|email',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect('forgot')->withErrors($validator->errors());
        }

        $user = User::where("email", "=", $request->email)->get();


        if (count($user) == 1) {

            $user = $user[0];
//            $token = md5($user->email.$user->id);
//            $link = url('/reset-password-link') . '/' . $user->id . "/" . $token;
//            $mail_content = "Please click on " . $link . " to reset your password.";
//            
            // print_r($user); die;
            if ($user->role_id == '2') {

                $password = substr(md5(microtime()), rand(0, 26), 7);
                //$user = $user[0];
                $replaces['NAME'] = $user->firstName . ' ' . $user->lastName;
                $replaces['PASSWORD'] = $password;
                $affectedRows = User::where('id', '=', $user->id)->update(array('password' => bcrypt($password)));
                $content = Content::where('code', '=', 'forgot_password')->get();
                $template = Functions::setEmailTemplate($content, $replaces);
                $mail = Functions::sendEmail($request->email, $template['subject'], $template['body']);
                \Session::flash('success', 'Your new password has been emailed.');
            } else {
                \Session::flash('success', 'Email not found.');
            }
        } else {
            \Session::flash('success', 'Email not found.');
        }

        return redirect('forgot');
    }

    public function resetPasswordForm($id, $email) {
        $user = User::where("id", "=", $id)->get();

        if (md5($user[0]->email . $user[0]->id) == $email) {

//            print_r("MATCH"); die;
            return View::make('resetPassword', ['user_id' => $id, 'errors' => "0"]);
        } else
            print "Link is not valid. Please resend link.";

        die;
    }

    public function postLogin(Request $request) {


        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if ($this->auth->attempt($credentials, $request->has('remember'))) {

            $model = User::where('email', '=', $credentials['email'])
                    ->where('status', '=', '1')
//                    -> where('role_id', '=', '2')
                    ->get();

            if (count($model) == 0) {

                $this->auth->logout();
                \Session::flush();
                return redirect('/login')
                                ->withInput($request->only('email', 'remember'))
                                ->withErrors([
                                    'email' => $this->getFailedLoginMessage(),
                ]);
            }
            if ($model[0]->role_id == '4')
                return redirect()->intended('customer/dashboard');
            
            if ($model[0]->role_id == '5')
                return redirect()->intended('customers');

                if ($model[0]->role_id == '7')  
                return redirect()->intended('client/dashboard');
            return redirect()->intended('/dashboard');
        }

        return redirect('/login')
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => $this->getFailedLoginMessage(),
        ]);
    }

    public static function customLogin($id, $request) {

        $user = User::where('id', $id)->first();
        Auth::login($user);
        if ($user->role_id == '4')
            return redirect()->intended('customer/dashboard');

        return redirect()->intended('/dashboard');
    }

    public static function insertClients() {
        $user = Clients::limit(50)->get();
        echo count( $user);
       if(isset( $user ) && !empty( $user )){
           foreach ($user as $key => $value) {
            $userlist = User::where('email', $value->email)->first();
             if(empty($userlist)){
                $user = new User;
                $user->firstName = $value->name;
                $user->password = bcrypt($value->name);
                $user->email =  $value->email;
                $user->role_id = '7';
                $user->save();
             }
           }
       }

    }
}
