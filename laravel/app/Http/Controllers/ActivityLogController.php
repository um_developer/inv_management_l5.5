<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\ActivityLog;
use App\Invoices;
use App\Orders;
use App\Customers;
use App\Clients;
use App\ItemPriceImpect;
use Session;
use Validator,
    Input,
    Redirect;

class ActivityLogController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Activity Log Controller
      |--------------------------------------------------------------------------
      |
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
    }

    public function index(){
        $modal = ActivityLog::where('deleted',0)->orderBy('id', 'desc')->get();
        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $customers = $new_cat;
        return view('front.activityLog.index', compact('modal','customers'));
    }

    public function searching(Request $request){

        if (isset($request->customer_id) && $request->customer_id > 0) {
            $customer_id = $request->customer_id;
            $modal = ActivityLog::where('deleted',0)->where('customer_id',$customer_id)->orderBy('id', 'desc')->get();
        }else{
            $modal = ActivityLog::where('deleted',0)->orderBy('id', 'desc')->get();
        }
        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $customers = $new_cat;            
        return view('front.activityLog.index', compact('modal','customers'));
    }

    public function getLog(Request $request) {
        if ($request->type =='invoices') {
            $modal = Invoices::where('invoices.activity_log_id',$request->id)->where('invoices.deleted', '!=', '1')
            ->leftjoin('customers','customers.id','=','invoices.customer_id')
            ->select('invoices.*','customers.name as c_name')
			->orderBy('id', 'desc')
            ->get();
        }else if ($request->type =='orders') {
            $modal = Orders::where('orders.activity_log_id',$request->id)->where('orders.deleted', '!=', '1')
            ->leftjoin('clients','clients.id','=','orders.client_id')
            ->select('orders.*','clients.name as c_name')
			->orderBy('id', 'desc')
            ->get();
        }
        $status = '';
        $type = $request->type;
        $log_id = $request->id;
        return view('front.activityLog.logs', compact('modal','status','type','log_id'));
    }

    public function activityLogDelete(Request $request){
// dd($request->id);

        Orders::where('activity_log_id', '=', $request->id)->update(['deleted' => 1]);
        Invoices::where('activity_log_id', '=', $request->id)->update(['deleted' => 1]);
        ItemPriceImpect::where('activity_log_id', '=', $request->id)->update(['status' => 'pending', 'activity_log_id' => 0]);
        ActivityLog::where('id', '=', $request->id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully deleted.');
        return redirect()->back();
        
    }

    public function logItemDelete($id, $type){
        if ($type == 'invoices') {
            Invoices::where('id', '=', $id)->update(['deleted' => 1]);
        }elseif($type == 'orders'){
            Orders::where('id', '=', $id)->update(['deleted' => 1]);
        }
        Session::flash('success', 'Successfully deleted.');
        return redirect()->back();
    }


}
