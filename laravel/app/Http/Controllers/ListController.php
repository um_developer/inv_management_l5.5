<?php

namespace App\Http\Controllers;

use DB;
use App\Lists;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class ListController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Categories Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }
    }

    public function index() {

        $sizes = Lists::where('deleted', '0')->get();
        return view('front.lists.index', compact('sizes'));
    }

    public function create() {

        return view('front.lists.create');
    }

    public function edit($id) {

        $list = Lists::where('id', '=', $id)->where('deleted', '0')->get();

        if (count($list) == 0) {
            return redirect('/');
        }
        $model = $list[0];
        return view('front.lists.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'title' => 'required|max:40|unique:lists',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $list = new Lists;
        $list->title = $request->title;
//        $list->created_by = $user_id;
        $list->save();

        if (isset($list->id)) {
            Session::flash('success', 'List is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'List is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $list_id = $request->id;
        $validation = array(
            'title' => 'required|max:40|unique:lists,title,' . $list_id,
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }


        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        Lists::where('id', '=', $list_id)->update($input);
        Session::flash('success', 'List has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        Lists::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }
    
     public function createSample() {

        return view('front.lists.sample');
    }
    
     public function postCreateSample(Request $request) {

         $input = $request->all();
         
         print_r($input); die;
    }

}
