<?php

namespace App\Http\Controllers;

use DB;
use App\Sizes;
use App\Customers;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\AccountTypes;
use App\PaymentAccountTransactions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;

class CustomerAccountTypesController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      |  CustomerPaymentTypesController
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $this->customer_id = $customer[0]->id;
    }

    public function index() {

        $payment_types = AccountTypes::where('deleted', '0')->where('owner_type', 'customer')->where('created_by', $this->customer_id)->get();
        return view('front.customers.account_type.index', compact('payment_types'));
    }

    public function create() {

        return view('front.customers.account_type.create');
    }

    public function edit($id) {

        $payment_type = AccountTypes::where('id', '=', $id)->where('owner_type', 'customer')->where('created_by', '=', $this->customer_id)->where('deleted', '0')->get();

        if (count($payment_type) == 0) {
            return redirect('/');
        }
        $model = $payment_type[0];
        return view('front.customers.account_type.edit', compact('model'));
    }

    public function postCreate(Request $request) {


        $validation = array(
            'title' => 'required|max:40',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }

        $size = new AccountTypes;
        $size->title = $request->title;
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $this->customer_id;
        $size->save();

        if (isset($size->id)) {
            Session::flash('success', 'Account is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Account is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $validation = array(
            'title' => 'required|max:40',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }

        $size_id = $request->id;
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');


        AccountTypes::where('id', '=', $size_id)->update($input);
        Session::flash('success', 'Payment Type has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        
       $payment_type = AccountTypes::where('id', '=', $id)->where('owner_type', 'customer')->where('created_by', '=', $this->customer_id)->where('deleted', '0')->get();
       
       if (count($account_detail) == 0)
            return redirect('/');

        if ($account_detail->amount == 0) {
            AccountTypes::where('id', '=', $id)->update(['deleted' => 1]);
            Session::flash('success', 'Successfully Deleted!');
            return redirect()->back();
        } else {
            Session::flash('error', $account_detail->title . ' balance is greater then 0.');
            return redirect()->back();
        }
    }

    public static function createClientAccountTypes($customer_id) {


        $size = new AccountTypes;
        $size->title = 'CASH';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();

        $size = new AccountTypes;
        $size->title = 'CHECK';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();

        $size = new AccountTypes;
        $size->title = 'MONEY ORDER';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();

        $size = new AccountTypes;
        $size->title = 'BANK DEPOSIT';
        $size->owner_type = 'customer';
        $size->for_client = '1';
        $size->created_by = $customer_id;
        $size->save();
    }
    
    public function details($id) {

        $account_detail = AccountTypes::where('id', '=', $id)->where('owner_type','customer')->where('created_by',$this->customer_id)->first();
        
        if(count($account_detail) == 0)
            return redirect('/');

        $model = PaymentAccountTransactions::where('payment_account_transactions.account_type_id', $id)->where('payment_account_transactions.user_type', 'customer')
                ->leftjoin('payments as p', 'p.id', '=', 'payment_account_transactions.payment_id')
                ->leftjoin('account_types as at', 'at.id', '=', 'payment_account_transactions.source_id')
                ->leftjoin('clients as c', 'c.id', '=', 'p.user_id')
                ->select('payment_account_transactions.*', 'p.user_type as user_type_new', 'c.name as customer_name','at.title as account_title','p.modification_count as modification_count','p.payment_type_id as payment_type_id')
                 ->orderBy('payment_account_transactions.id','DESC')->get();

        return view('front.customers.account_type.detail', compact('model', 'account_detail'));
    }

    public function transfer() {

        $account_types = AccountTypes::where('deleted', '0')->where('owner_type','customer')->where('created_by',$this->customer_id)->pluck('title', 'id');
        return view('front.customers.account_type.transfer', compact('account_types'));
    }

    public function transferAmountPost(Request $request) {

        $input = $request->all();
        $user_id = $this->customer_id;
        
        $validation = array(
            'source_id' => 'required',
            'destination_id' => 'required',
            'amount' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $destination_id = $input['destination_id'];
        $source_id = $input['source_id'];
        $amount = $input['amount'];


        if ($amount <= 0) {
            Session::flash('error', 'Please enter amount greater than 0.');
            return redirect()->back();
        }

        if ($destination_id == $source_id) {
            Session::flash('error', 'Source and Destination is same.');
            return redirect()->back();
        }
        
        $source_account_balance = AccountTypes::where('id', $source_id)->first();
        $destination_account_balance = AccountTypes::where('id', $source_id)->first();
        
         if ($source_account_balance->amount < $amount) {
            Session::flash('error', 'Amount is insufficient in your Account.');
            return redirect()->back();
        }

        DB::beginTransaction();

        AccountTypes::where('id', $source_id)->decrement('amount', $amount);
        AccountTypes::where('id', $destination_id)->increment('amount', $amount);

        $account_type_transaction = new PaymentAccountTransactions;
        $account_type_transaction->account_type_id = $source_id;
        $account_type_transaction->payment_id = 0;
        $account_type_transaction->source_id = $destination_id;
        $account_type_transaction->source_type = 'transfer';
        $account_type_transaction->previous_balance = $source_account_balance->amount;
        $account_type_transaction->user_type = 'customer';
        $account_type_transaction->created_by = $user_id;
        $account_type_transaction->message = '';
        $account_type_transaction->updated_balance = $source_account_balance->amount - $amount;
        $account_type_transaction->amount = '-' . $amount;
        $account_type_transaction->save();

        
        $account_type_transaction = new PaymentAccountTransactions;
        $account_type_transaction->account_type_id = $destination_id;
        $account_type_transaction->payment_id = 0;
        $account_type_transaction->source_id = $source_id;
        $account_type_transaction->source_type = 'customer';
        $account_type_transaction->previous_balance = $destination_account_balance->amount;
        $account_type_transaction->user_type = 'admin';
        $account_type_transaction->created_by = $user_id;
        $account_type_transaction->message = '';
        $account_type_transaction->updated_balance = $destination_account_balance->amount + $amount;
        $account_type_transaction->amount = $amount;
        $account_type_transaction->save();
        
        DB::commit();
        Session::flash('success', 'Amount is successfully transfered.');
        return redirect()->back();
    }

}
