<?php

namespace App\Http\Controllers;

use DB;
use App\Warehouses;
use App\User;
use App\WarehousePackages;
use App\WarehouseInventory;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;
use Excel;

class WarehouseController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Warehouse Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
//        $this->middleware('auth');
        parent::__construct();
        if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
    }

    public function index() {

        $model = Warehouses::where('deleted', '=', '0')->get();
        return view('front.warehouse.index', compact('model'));
    }

    public function create() {

        return view('front.warehouse.create');
    }

    public function edit($id) {

        $model = Warehouses::where('id', '=', $id)->where('deleted', '=', '0')->get();

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.warehouse.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'name' => 'required|max:100',
//             'phone' => 'regex:/(01)[0-9]{9}/|max:16',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $warehouses = new Warehouses;
        $warehouses->name = $request->name;
        $warehouses->phone = $request->phone;
        $warehouses->address = $request->address;
        $warehouses->save();

        if (isset($warehouses->id)) {
            Session::flash('success', 'Warehouse is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'Warehouse is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $validation = array(
            'name' => 'required|max:100',
            'id' => 'required',
//             'phone' => 'regex:/(01)[0-9]{9}/|max:16',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $warehouse_id = $request->id;
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        Warehouses::where('id', '=', $warehouse_id)->update($input);
        Session::flash('success', 'Warehouse has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        Warehouses::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function inventory($warehouse_id) {

        $warehouse = Warehouses::where('id', $warehouse_id)->get();
        $model = WarehouseInventory::where('warehouse_inventory.deleted', '=', '0')
                ->where('quantity', '!=', '0')
                ->where('warehouse_id', '=', $warehouse_id)
                ->leftjoin('items as i', 'i.id', '=', 'warehouse_inventory.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('warehouse_inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku')
                ->get();
//       print_r($model); die;
        return view('front.warehouse.inventory', compact('model', 'warehouse'));
    }

    public function inventory1($warehouse_id) {

        $result = Warehouses::where('deleted', '=', '0')->get();
//        $result = WarehouseInventory::where('warehouse_inventory.deleted', '=', '0')
//                 ->where('quantity','!=','0')
//                 ->where('warehouse_id','=',$warehouse_id)
//                ->leftjoin('items as i','i.id','=','warehouse_inventory.item_id')
//               ->leftjoin('categories as c','c.id','=','i.category_id')
//               ->select('warehouse_inventory.*','c.name as category_name','i.name as item_name','i.sku as item_sku')
//               ->get();
        $type = 'csv';

        return downloadExcel('users_data', function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download($type);
    }

    public function generateCSVReport($warehouse_id) {
//        $warehouse_id = 1;
        $result = WarehouseInventory::where('warehouse_inventory.deleted', '=', '0')
                ->where('quantity', '!=', '0')
                ->where('warehouse_id', '=', $warehouse_id)
                ->leftjoin('items as i', 'i.id', '=', 'warehouse_inventory.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('i.name as item_name', 'c.name as category_name', 'i.code as item_sku', 'warehouse_inventory.quantity')
                ->get();

//       print_r($result); die;
        $file_name = "warehouse_" . $warehouse_id . "_inventory.csv";
        return Excel::create($file_name, function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download('csv');
    }

}
