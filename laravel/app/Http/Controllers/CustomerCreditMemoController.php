<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\Invoices;
use App\CreditMemo;
use App\CreditMemoItems;
use App\InvoiceItems;
use App\Customers;
use App\PoItems;
use App\Inventory;
use App\Bundles;
use App\BundleItems;
use App\CustomerInventory;
use App\PurchaseOrders;
use Illuminate\Http\Request;
use App\PrintPageSetting;
use App\Functions\Functions;
use Auth;
use Carbon\Carbon;
use Session;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;

class CustomerCreditMemoController extends AdminController {

    private $user_id = '0';
    private $customer_id = '0';
    private $customer_obj = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->first();
        $this->customer_id = $customer->id;
        $this->show_bundle =$customer->show_bundle;
        $this->customer_obj = $customer;

    }

    public function index() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $model = CreditMemo::where('credit_memo.deleted', '=', '0')
                ->where('customer_id', $this->customer_id)
                ->whereBetween('credit_memo.statement_date', [$start_date, $end_date])
                ->orderBy('id', 'desc')
                ->get();


        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
 
         $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $selected_report_type = '';
        $customers = $new_cat;

        $date = $start_date . ' - ' . $end_date;
        return view('front.credit_memo.index', compact('model', 'date','customers','selected_report_type'));
    }

    public function search(Request $request) {

        $date = $request->date_range;

        if(isset($request->type)){
            $selected_report_type = $request->type;
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }


        $model = CreditMemo::where('credit_memo.deleted', '=', '0')
                ->where('customer_id', $this->customer_id)
                ->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);
        if (!empty($selected_report_type)){
            
            $model = $model->where('credit_memo.status', $selected_report_type);
        }
        
        $model = $model->orderBy('id', 'desc')
                ->get();
                
        $customers = Customers::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $customers = $new_cat;
        return view('front.credit_memo.index', compact('model', 'date','customers','selected_report_type'));
    }

    public function create() {

        //$items = CommonController::initilizeCreditMemoFrom();

        $customer = Customers::where('user_id', $this->user_id)->first();
        $items = CommonController::getItemFormsData($customer->selling_price_template, $customer->sale_price_template);
         $customer_id = $this->customer_id;
         $show_bundle = $this->show_bundle;
  $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
        return view('front.credit_memo.create', compact('items','customer_id','bundles','show_bundle'));
    }

    public function edit($id) {

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->where('credit_memo.status', '!=', 'pending')
                ->get();
                $show_bundle = $this->show_bundle;
        if (count($model) > 0) {
            return redirect('/');
        }

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->select('credit_memo.*', 'c.id as customer_id')
                ->get();

        //$items = CommonController::initilizeCreditMemoFrom();
        $customer = Customers::where('user_id', $this->user_id)->first();
        $items = CommonController::getItemFormsData($customer->selling_price_template, $customer->sale_price_template);

        $credit_memo_items = CreditMemoItems::where('credit_memo_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'credit_memo_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'credit_memo_items.bundle_id')
                ->where('credit_memo_items.deleted', '=', '0')
                ->select('credit_memo_items.*', 'i.name as item_name', 'i.cost as cost', 'b.name as bundle_name')
                ->get();
          

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        $customer_id = $this->customer_id;
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
        return view('front.credit_memo.edit', compact('items', 'model', 'credit_memo_items','customer_id','bundles','show_bundle'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $validation = array(
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        DB::beginTransaction();
        $items = $input['num'];
        $credit_memo = new CreditMemo;
        $credit_memo->created_by = 'admin';

        if (Auth::user()->role_id == '4')
            $credit_memo->created_by = 'customer';

        $credit_memo->customer_id = $this->customer_id;
        $timestamp = strtotime($request->statement_date);
        $credit_memo->statement_date = date("Y-m-d H:i:s", $timestamp);
        $credit_memo->memo = $request->memo;
        $credit_memo->deleted = '1';
        $credit_memo->save();

        $total_invoice_price = 0;
        $total_quantity = 0;
        if (count($items) > 0 && isset($credit_memo->id)) {

            if ($input['item_id_1'] != '' || $input['item_id_2'] != '') {

                for ($i = 1; $i <= count($items); $i++) {
                    if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                        $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];

                        $credit_memo_items = new CreditMemoItems;
                        $credit_memo_items->credit_memo_id = $credit_memo->id;
                        $credit_memo_items->customer_id = $this->customer_id;
                        $credit_memo_items->item_id = $input['item_id_' . $i];
                        $credit_memo_items->item_unit_price = $input['price_' . $i];
                        $credit_memo_items->quantity = $input['quantity_' . $i];
                        $credit_memo_items->statement_date = date("Y-m-d H:i:s", $timestamp);


                        if ($input['start_serial_number_' . $i] > 0) {

                            $credit_memo_items->start_serial_number = $input['start_serial_number_' . $i];
                            $credit_memo_items->end_serial_number = $input['end_serial_number_' . $i];
                            $credit_memo_items->quantity = $input['quantity_' . $i];
                        }

                        if (isset($input['item_bundle_id_' . $i])) {
                            $credit_memo_items->bundle_id = $input['item_bundle_id_' . $i];
                            $credit_memo_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                        }
                        $credit_memo_items->total_price = $item_total_price;
//                        $credit_memo_items->created_by = $user_id;
                        $total_invoice_price += $credit_memo_items->total_price;
                        $total_quantity += $credit_memo_items->quantity;
                        $credit_memo_items->save();
                    }
                }

                if ($total_quantity == 0) {
                    DB::rollBack();
                    Session::flash('error', 'Item count is greater than 0.');
                    return redirect()->back();
                }

                CreditMemo::where('id', '=', $credit_memo->id)->update([
                    'deleted' => 0,
                    'total_price' => $total_invoice_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Credit Memo is not created. Please add atleast 1 item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Credit Memo is not created. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Credit Memo is created successfully');
        return redirect()->back();
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $user_id = Auth::user()->id;

        $credit_memo = CreditMemo::where('id', '=', $input['id'])->get();
        DB::beginTransaction();
        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d", $timestamp);
        CreditMemo::where('id', '=', $input['id'])->update([
//            'customer_id' => $input['customer_id'],
            'memo' => $input['memo'],
            'statement_date' => $statement_date,
        ]);

        $credit_memo = $credit_memo[0];

        $invoice_total_price = 0;
        $total_quantity = 0;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $credit_memo_item = CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->get();
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {

                            if ($total_quantity == 0) {
                                DB::rollBack();
                                Session::flash('error', 'Item count is greater than 0.');
                                return redirect()->back();
                            }

                            CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'start_serial_number' => $input['start_serial_number_' . $i],
                                'end_serial_number' => $input['end_serial_number_' . $i],
                                'statement_date' => $statement_date,
                            ]);
                        } else {
                            CreditMemoItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {


                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $credit_memo_items = new CreditMemoItems;
                    $credit_memo_items->credit_memo_id = $input['id'];
                    $credit_memo_items->customer_id = $this->customer_id;
                    $credit_memo_items->item_id = $input['item_id_' . $i];
                    $credit_memo_items->item_unit_price = $input['price_' . $i];
                    $credit_memo_items->total_price = $item_total_price;
                    $credit_memo_items->quantity = $input['quantity_' . $i];
                    $credit_memo_items->statement_date = $statement_date;

                    if ($input['start_serial_number_' . $i] > 0) {
                        $credit_memo_items->start_serial_number = $input['start_serial_number_' . $i];
                        $credit_memo_items->end_serial_number = $input['end_serial_number_' . $i];
                        $credit_memo_items->quantity = $input['quantity_' . $i];
                    }

                    if (isset($input['item_bundle_id_' . $i])) {
                        $credit_memo_items->bundle_id = $input['item_bundle_id_' . $i];
                        $credit_memo_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }
                    $credit_memo_items->quantity = $credit_memo_items->quantity;
//                    $credit_memo_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $credit_memo_items->quantity;
                    $credit_memo_items->save();
//                    print_r($invoice_items); die;
                }
            }

            if ($invoice_total_price > 0) {
                CreditMemo::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Credit Memo is not updated. Please add atleast 1 proper item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Credit Memo is not updated. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Credit Memo is updated successfully');
        return redirect()->back();
    }

    public function delete($id) {

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->where('credit_memo.status', '!=', 'pending')
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }

        CreditMemoItems::where('credit_memo_id', '=', $id)->update(['deleted' => 1]);
        CreditMemo::where('id', '=', $id)->where('status', '=', 'pending')->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function detail($id) {

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->where('credit_memo.deleted', '=', '0')
                ->select('credit_memo.*', 'c.id as customer_id', 'c.name as customer_name')
                ->get();

        $po_items = CreditMemoItems::where('credit_memo_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'credit_memo_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'credit_memo_items.bundle_id')
                ->where('credit_memo_items.deleted', '=', '0')
                ->select('credit_memo_items.*', 'i.name as item_name', 'b.name as bundle_name')
                ->get();
               
        $show_profit = '0';
        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.credit_memo.detail', compact('model', 'po_items', 'show_profit'));
    }

    public function printPage($id) {

        $model = CreditMemo::where('credit_memo.id', '=', $id)
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                ->where('credit_memo.deleted', '=', '0')
                ->leftjoin('states as s', 's.code', '=', 'c.state')
                ->select('credit_memo.*', 's.title as state', 'c.id as customer_id', 'c.name as customer_name', 'c.show_image', 'c.show_serial', 'show_package_id', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'c.phone', 'c.address1')
                ->get();



        $po_items = CreditMemoItems::where('credit_memo_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'credit_memo_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'credit_memo_items.bundle_id')
                ->where('credit_memo_items.deleted', '=', '0')
                ->select('credit_memo_items.*', 'i.name as item_name', 'b.name as bundle_name')
                ->get();

            
        if (count($model) == 0) {
            return redirect('/');
        }

        $setting = PrintPageSetting::where('user_type', '=', 'admin')->first();
        $model = $model[0];
        return view('front.credit_memo.print', compact('model', 'po_items', 'setting'));
    }

    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }

    function updateCustomerBalance($customer_id, $amount, $type = 'subtract') {

        $customer = Customers::where('id', $customer_id)->get();

        if ($type == 'subtract')
            $final_amount = $customer[0]->balance - $amount;
        else
            $final_amount = $customer[0]->balance + $amount;

        Customers::where('id', $customer_id)->update(['balance' => $final_amount]);
    }

    function updateAdminInventory($item_id, $quantity, $type = 'add') {
        $item = Inventory::where('item_id', $item_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new Inventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->save();
            }
        } else {
            $total_quantity = $item[0]->quantity - $quantity;
            if ($total_quantity < 0) {
                return 'false';
            } else {
                Inventory::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            }
        }
    }

//    public function serialItemValidate($item_id, $start_serial, $quantity) {
//        $items = ItemSerials::where('item_id', $item_id)->where('serial', $start_serial)->where('type', 'inventory')->get();
//        if (count($items) > 0) {
//           $a= ItemSerials::whereBetween('serial', [$start_serial, $start_serial+$quantity])->count();
//           $quantity= $quantity+1;
//           if($a == $quantity)
//                return 1;
//           else
//               return 0;
//        } else 
//            return 0;
//        
//    }
//    public function initilizeFrom(){
//        
//         $search_model = Inventory::where('inventory.deleted', '=', '0')
//                ->where('quantity', '!=', '0')
//                ->leftjoin('items as i', 'i.id', '=', 'inventory.item_id')
//                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
//                ->select('i.*', 'c.name as category_name')
//                ->get();
//
//         $customer = Customers::where('user_id',$this->user_id)->get();
//        $template = $customer[0]->sale_price_template;
//        
//         $serial_items = ItemSerials::where('item_serials.deleted', '=', '0')
//                ->where('item_serials.customer_id', $this->customer_id)
//                ->where('item_serials.type', 'sold')
//                ->where('item_serials.status', 'inactive')
//                ->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
//                ->select('i.*')
//                ->groupBy('item_id')
//                ->get();
//
//        $items = [];
//        $i = 0;
//        foreach ($search_model as $item) {
//            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->sku;
//            $items[$i]['code'] = $item->code;
//            $items[$i]['sku'] = $item->sku;
//            $items[$i]['id'] = $item->id;
//            $items[$i]['sale_price'] = $item->sale_price;
//            $items[$i]['value'] = $item->name;
//            $items[$i]['cost'] = $item->cost;
//            $items[$i]['serial'] = $item->serial;
//
//            if ($item->serial) {
//                $items[$i]['start_serial'] = $item->code;
//            }
//            $i++;
//        }
//        
//        foreach ($serial_items as $item) {
//            $items[$i]['label'] = $item->name . '-' . $item->code . '-' . $item->sku;
//            $items[$i]['code'] = $item->code;
//            $items[$i]['sku'] = $item->sku;
//            $items[$i]['id'] = $item->id;
//            $items[$i]['cost'] = $item->cost;
//            $items[$i]['sale_price'] = $item->$template;
//           if($items[$i]['sale_price'] < 1)
//                $items[$i]['sale_price'] = $item->sale_price;
//            $items[$i]['value'] = $item->name;
//            $items[$i]['serial'] = $item->serial;
//
//            $i++;
//        }
//        
//        return $items;
//        
//    }
}
