<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\GalleryImages;
use App\Colors;
use App\Sizes;
use App\Types;
use App\ItemColors;
use App\ItemSizes;
use App\PriceTemplates;
use App\ItemPriceTemplates;
use App\ItemTypes;
use App\Categories;
use App\ApiServers;
use App\Tags;
use App\ApiActivityLog;
use App\TagItemRelation;
use File;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Illuminate\Support\Arr;
use Image;
use App\Customers;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;


class ItemController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Item Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        // parent::__construct();
        // if (Auth::user()->role_id != '2') {
        //     Redirect::to('customers')->send();
        //     die;
        // }
    }
    public function getItemprice($itemid) {

        $model = Items::where(['items.deleted'=> '0','items.id'=>$itemid])
                ->where('items.parent_item_id', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
                ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
                ->select('items.*', 'i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title')
                ->orderBy('items.id', 'desc')
//                ->take(30)
                ->first();
echo $model->cost;
           }     
           public function getsubitems($itemid) {

            $model = Items::where(['items.deleted'=> '0'])
                    ->where('items.parent_item_id', '=',$itemid)
                    ->select('items.name','items.id','items.upc_barcode')
                    ->get();
    return view('front.item.sub_item_modal', compact('model'));
               }          
    public function index() {
        
        $model = Items::where('items.deleted', '=', '0')
                ->where('items.parent_item_id', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
                ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
                // ->with([
                //     'GalleryImages' => function ($q) {
                //         return $q->where('deleted', 0);
                //     },
                // ])
                ->select('items.*', 'i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title')
                ->orderBy('items.id', 'desc')
            //    ->take(10)
                ->get();

        $categories = Categories::where('deleted', '=', '0')
                        ->pluck('name', 'id')->toArray();
        $categories[0] = 'Select Category';
        $category_id = 0;

        foreach ($model as $item) {

            $sub_item_quantity = 0;
            if ($item->serial == 'no') {

                $a = \App\WarehouseInventory::where('item_id', $item->id)
                        ->select(DB::raw('sum(quantity) as num'))
                        ->get();

                if (count($a) > 0)
                    $item->warehouse_quantity = $a[0]->num;
                else
                    $item->warehouse_quantity = 0;
            }else {
                $item_quantity = \App\ItemSerials::where('item_id', $item->id)->where('type', 'inventory')->where('status', 'inactive')->count();
                $item->warehouse_quantity = $item_quantity;
            }

            if ($item->has_sub_item == '1') {
                $sub_items = Items::where('parent_item_id', $item->id)->where('deleted', 0)->select('id')->get()->toArray();
                $inventory = \App\Inventory::whereIn('item_id', $sub_items)->select(DB::raw('sum(quantity) as quantity , sum(package_quantity) as package_quantity'))->get();

                if (count($inventory) > 0)
                    $sub_item_quantity = $inventory[0]->quantity + $inventory[0]->package_quantity;
            }
            $item->sub_item_quantity = $sub_item_quantity;
        }

        return view('front.item.index', compact('model', 'categories', 'category_id'));
    }

    public function autogeneratecode() {
       $code = Functions::generateRandomcode();
       $model = Items::where(['items.deleted'=> '0'])
       ->where('items.upc_barcode', '=', $code)
       ->select('items.name','items.id','items.upc_barcode')
       ->get();
       
       if(count($model)==0){
      return $code;
       }else{
      return $code = Functions::generateRandomcode();
       }
    }
    public function generatebarcode($itemid) {
        
                        $model = Items::where(['items.deleted'=> '0'])
                                ->where('items.parent_item_id', '=',$itemid)
                                ->select('items.name','items.id','items.upc_barcode')
                                ->get();
                       
                       if(empty($model) || count($model)==0){
                        $model = Items::where(['items.deleted'=> '0'])
                        ->where('items.id', '=',$itemid)
                        ->select('items.name','items.id','items.upc_barcode')
                        ->get();
                       }  
                             
                return view('front.item.barcodeUpdateModal', compact('model'));
                           }  
        
                           public function printBarcode($itemid) {
                            $model = Items::where(['items.deleted'=> '0'])
                            ->where('items.id', '=',$itemid)
                            ->select('items.name','items.id','items.upc_barcode','items.code')
                            ->first();
                            return view('front.item.printitembarcode', compact('model'));
                           }  
    public function itemListSearch(Request $request) {
        if ($request->category_id == '0' && $request->status == "" &&  $request->pricestatus=="") {

            return redirect('/items');
        }

        $model = Items::where('items.deleted', '=', '0')
                ->where('items.parent_item_id', '=', '0');
        if (isset($request->status) && !empty($request->status) && $request->category_id != '0' && $request->pricestatus=="") {
            $model = $model->where(['items.status' => $request->status, 'items.category_id' => $request->category_id]);
        } else {
            if ($request->status != "") {
                $model = $model->where('items.status', $request->status);
            }
            if ($request->category_id != '0') {

                $model = $model->where('items.category_id', $request->category_id);
            }
            if(isset($request->pricestatus) && $request->pricestatus=="increase"){
                $model = $model->where('items.increase_effect', 1);
            }
            if(isset($request->pricestatus) && $request->pricestatus=="decrease"){
                $model = $model->where('items.decrease_effect', 1);
            }
            
        }

        $model = $model->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
                ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
                // ->with([
                //     'GalleryImages' => function ($q) {
                //         return $q->where('deleted', 0);
                //     },
                // ])
                ->select('items.*', 'i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title')
                ->orderBy('items.id', 'desc')
                ->get();
        $categories = Categories::where('deleted', '=', '0')
                        ->pluck('name', 'id')->toArray();
        $categories[0] = 'Select Category';
        $category_id = $request->category_id;
        $status = $request->status;
        $pricestatus = $request->pricestatus;

        foreach ($model as $item) {
            $sub_item_quantity = 0;
            if ($item->serial == 'no') {

                $a = \App\WarehouseInventory::where('item_id', $item->id)
                        ->select(DB::raw('sum(quantity) as num'))
                        ->get();

                if (count($a) > 0)
                    $item->warehouse_quantity = $a[0]->num;
                else
                    $item->warehouse_quantity = 0;
            }else {
                $item_quantity = \App\ItemSerials::where('item_id', $item->id)->where('type', 'inventory')->where('status', 'inactive')->count();
                $item->warehouse_quantity = $item_quantity;
            }

            if ($item->has_sub_item == '1') {
                $sub_items = Items::where('parent_item_id', $item->id)->where('deleted', 0)->select('id')->get()->toArray();
                $inventory = \App\Inventory::whereIn('item_id', $sub_items)->select(DB::raw('sum(quantity) as quantity , sum(package_quantity) as package_quantity'))->get();

                if (count($inventory) > 0)
                    $sub_item_quantity = $inventory[0]->quantity + $inventory[0]->package_quantity;
            }
            $item->sub_item_quantity = $sub_item_quantity;
        }
        return view('front.item.index', compact('model', 'categories', 'category_id', 'status','pricestatus'));
    }

    public function create() {

        $categories = Categories::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $sizes = Sizes::where('deleted', '0')->pluck('title', 'id');
        $types = Types::where('deleted', '0')->pluck('title', 'id');
        $colors = Colors::where('deleted', '0')->pluck('title', 'id');
        $api_servers = ApiServers::select(['id', 'title'])->get();

        // $tagrel = TagItemRelation::where('deleted',0)->groupby('tag_id')->get();
        // $ids = [];
        // foreach ($tagrel as $row) {
        //     $ids[] = $row->tag_id;
        // }
        // $tags = Tags::where('deleted', 0)->whereNotIn('id',$ids)->lists('title', 'id');
        // $tags = Tags::where('deleted', 0)->where('is_subtag', 0)->lists('title', 'id');
        $tags = Tags::select('id','title')->where('deleted',0)->where('is_subtag',0)->get();
        $data1 = [];
       
        foreach ($tags as $row) {
            $data1[$row->id] = $row;
        }
        
        foreach ($data1 as $row) {
                $re = DB::table('sub_tag_relation')->where('tag_id',$row->id)->get();                
               
                $array = [];
                foreach($re as $r){
                        $y = Tags::select('id','title')->where('id',$r->sub_tag_id)->first();
                        array_push($array,$y);
                }
                $data1[$row->id]['child'] = $array;
        }

        $tags = $data1;

        $new_cat = [];
        $new_api_servers[0] = 'No';

        foreach ($categories as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        foreach ($api_servers as $api) {
            $new_api_servers[$api->id] = $api->title;
        }

        $categories = $new_cat;
        $api_servers = $new_api_servers;
        return view('front.item.create', compact('categories', 'api_servers', 'colors', 'sizes', 'types', 'tags'));
    }

    public function edit($id) {

//        $model = Items::where('id',$id)->with('ItemColors.Colors','ItemSizes.Sizes','ItemTypes.Types')->get();
//        print_r($model); die;
        $model = Items::where('id', '=', $id)->where('deleted', '=', '0')->get();
        $colors = Colors::where('deleted', '0')->pluck('title', 'id');
        $sizes = Sizes::where('deleted', '0')->pluck('title', 'id');
        $types = Types::where('deleted', '0')->pluck('title', 'id');
        $categories = Categories::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $api_servers = ApiServers::select(['id', 'title'])->get();

        // $tags = Tags::where('deleted', 0)->where('is_subtag', 0)->lists('title', 'id');
        $subitems = Items::where('parent_item_id', '=', $id)->where('deleted', '=', '0')->get();
//        print_r($subitems); die;
        $new_api_servers[0] = 'No';

        foreach ($categories as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        foreach ($api_servers as $api) {
            $new_api_servers[$api->id] = $api->title;
        }

        $categories = $new_cat;
        $api_servers = $new_api_servers;

        $gameColorModel = ItemColors::where('item_id', $id)->get();
        $gameSizeModel = ItemSizes::where('item_id', $id)->get();
        $gameTypeModel = ItemTypes::where('item_id', $id)->get();
        $tagRelationModal = TagItemRelation::where('item_id', $id)->get();

        $galleryimages = GalleryImages::where('item_id', $id)->where('deleted', '=', '0')->get();

        $item_colors = array();
        $item_sizes = array();
        $item_types = array();
        $item_tags = array();

        foreach ($gameColorModel as $gc) {
            $item_colors[] = $gc->color_id;
        }

        foreach ($gameSizeModel as $gc) {
            $item_sizes[] = $gc->size_id;
        }

        foreach ($gameTypeModel as $gc) {
            $item_types[] = $gc->type_id;
        }

        foreach ($tagRelationModal as $it) {
            $item_tags[] = $it->tag_id;
        }
        $item_tags = Tags::whereIn('id',$item_tags)->get();

        if (count($model) == 0) {
            return redirect('/');
        }
        $tags = Tags::select('id','title')->where('deleted',0)->where('is_subtag',0)->get();
        $data1 = [];
       
        foreach ($tags as $row) {
            $data1[$row->id] = $row;
        }
        
        foreach ($data1 as $row) {
                $re = DB::table('sub_tag_relation')->where('tag_id',$row->id)->get();                
                $array = [];
                foreach($re as $r){
                        $y = Tags::select('id','title')->where('id',$r->sub_tag_id)->first();
                        array_push($array,$y);
                }
                $data1[$row->id]['child'] = $array;
        }

        $tags = $data1;

        $model = $model[0];
        return view('front.item.edit', compact('model', 'categories', 'api_servers', 'colors', 'item_colors', 'sizes', 'item_sizes', 'types', 'item_types', 'subitems', 'galleryimages', 'tags', 'item_tags'));
    }

    public function postCreate(Request $request) {
// dd($request);
        $user_id = Auth::user()->id;
        $validation = array(
            'name' => 'required|max:200',
//            'upc_barcode' => 'required|integer|between:2,20',
            'upc_barcode' => 'required|min:2|max:25',
            // 'code' => 'required|max:190|min:4|unique:items',
//            'sale_price' => 'required',
            'cost' => 'required',
//             'image' => 'mimes:jpeg,bmp,png,gif',
        );


//        https://laravel.io/forum/09-16-2014-validator-greater-than-other-field

        $messages = array();

//         $messages = array(
//                'code.required' => 'You have to enter a valid time.',
//                'code.date_format' => 'The time does not match the format minutes:seconds.',
//            );

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

//        $sku = $request->sku;
//        if ($request->sku == '')
//            $sku = Functions::generateRandomString(8);

DB::beginTransaction();
try{
        $image_name = '';

        if (Input::hasFile('image')) {
            $image = Input::file('image');
            $image_name = rand(111, 999) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/items');
            $image->move($destinationPath, $image_name);
            $destinatio = $destinationPath . '/' . $image_name;
            $thumbpath = $destinationPath . '/thumbnail/' . $image_name;
            // $this->createThumbnail($destinatio, $thumbpath, 50, 50, $background=false);
//            $model->image = S3::storeImage($fileName, $file, 'uploads/games/', 'uploads/games/thumbnail/', 400, 400);
        }


        $last_item = Items::where('parent_item_id', '0')->orderBy('id', 'desc')->first();
        $last_item_sku = $last_item->code;
        $last_item_sku = ltrim($last_item_sku, 'S');
        $new_item_sku = 'S' . ($last_item_sku + 1);
        //  $new_item_sku = 'S1000';

        $item = new Items;
        $item->name = $new_item_sku . ' ' . $request->name;
        $item->category_id = $request->category_id;
        $item->upc_barcode = $request->upc_barcode;
        $item->code = $new_item_sku;
        $item->cost = $request->cost;
        $item->description = $request->description;
        $item->serial = $request->serial;
        $item->api = $request->api;
        $item->created_by = $user_id;
        $item->image = $image_name;
        $item->save();

        $item_id = $item->id;
        $gallery = [];
        if ($request->hasFile('gallery_image')) {
            $files = $request->file('gallery_image');
            foreach ($files as $file) {
                $filename = rand(111, 999) . time() . '.' . $file->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/items/gallery');
                $file->move($destinationPath, $filename);
                $image = new GalleryImages;
                $image->item_id = $item_id;
                $image->image = $filename;
                $image->sort_order = 0;
                $image->save();
                $gallery[] = $filename;
            }
        }

        if (isset($request->item_colors)) {
            ItemColors::where('item_id', $item_id)->delete();
            if (count($request->item_colors) > 0) {
                foreach ($request->item_colors as $color) {

                    $item_color = new ItemColors;
                    $item_color->item_id = $item_id;
                    $item_color->color_id = $color;
                    $item_color->save();
                }
            }
            array_forget($input, 'item_colors');
        }

        if (isset($request->item_sizes)) {
            ItemSizes::where('item_id', $item_id)->delete();
            if (count($request->item_sizes) > 0) {
                foreach ($request->item_sizes as $size) {

                    $item_size = new ItemSizes;
                    $item_size->item_id = $item_id;
                    $item_size->size_id = $size;
                    $item_size->save();
                }
            }
            array_forget($input, 'item_sizes');
        }

        if (isset($request->item_types)) {
            ItemTypes::where('item_id', $item_id)->delete();
            if (count($request->item_types) > 0) {
                foreach ($request->item_types as $type) {

                    $item_type = new ItemTypes;
                    $item_type->item_id = $item_id;
                    $item_type->type_id = $type;
                    $item_type->save();
                }
            }
            array_forget($input, 'item_types');
        }

        if (isset($request->tags)) {
            if (count($request->tags) > 0) {
                foreach ($request->tags as $tag) {

                    $tagItemRelation = new TagItemRelation;
                    $tagItemRelation->item_id = $item_id;
                    $tagItemRelation->tag_id = $tag;
                    $tagItemRelation->save();
                }
            }
            array_forget($input, 'tags');
        }
// dd($request->all());
        $itemcheck = Items::where('id', '!=', $item_id)->where('upc_barcode',$request->upc_barcode)->first();
        $barcodeexist="";
        if(!empty($itemcheck)){
        $barcodeexist="Upc/Barcode already exist on $itemcheck->name";
        }
        if (isset($item->id)) {
            self::createSubItems($item->id, $request->item_colors, $request->item_sizes, $request->item_types);

            self::addItemPriceInTemplate($item->id, $request->cost);
            $type = 'add';
            $price = 0;
            DB::commit();
            $gl_imgs = [];
            $g_imgs = GalleryImages::where('item_id',$item->id)->where('deleted',0)->get();
            
            $gl_imgs[] = $item->image;
            foreach($g_imgs as $row){
                $gl_imgs[] = $row->image;
            }
            $sourc = 'ItemController::posrCreated';
            $this->makecurlRequest($request,$price,$gl_imgs,$item->code,$type,$item->id,$sourc);
            
            Session::flash('success', 'Item is created successfully. '. $barcodeexist.'');
            return redirect()->back();
        } else {
            Session::flash('error', 'Item is not created. Please try again.');
            return redirect()->back();
        }

    }catch(Exception $e) {
        DB::rollBack();
        Session::flash('error', $e->getMessage());
        return redirect()->back();
    }
    }

    public static function makecurlRequest($request,$price,$gallery,$sku,$type,$item_id,$sourc){
        
        $API_URL = \Config::get('apiCreds.API_URL');
        //'https://bmaxlive.testpinless.com/items';

        if ($type == 'delete') {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $API_URL.'/deleteitem',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array(
                    'category' => '8',
                    'subcategory' => '0',
                    'parent' => '0',
                    'name' => $request->name,
                    'price_type' => 'Fixed',
                    'displayprice' => '1pc ($22.00)',
                    'price' => $request->cost,
                    'orgpirce' => $request->cost,
                    'check_cost' => $request->cost,
                    'details' => $request->description,
                    'Disclaimer' => 'S-123 K38 Wireless True Headset&nbsp;',
                    'quantity_left' => '0',
                    'status' => 'I',
                    'sku' => $sku
                    ),
                    CURLOPT_HTTPHEADER => array(
                        'api-key: 513e856b-24b4-415b-9297-01865b6fc40f'
                    ),
            ));
            $r = array(
                'category' => '8',
                'subcategory' => '0',
                'parent' => '0',
                'name' => $request->name,
                'price_type' => 'Fixed',
                'displayprice' => '1pc ($22.00)',
                'price' => $request->cost,
                'orgpirce' => $request->cost,
                'check_cost' => $request->cost,
                'details' => $request->description,
                'Disclaimer' => 'S-123 K38 Wireless True Headset&nbsp;',
                'quantity_left' => '0',
                'status' => 'I',
                'sku' => $sku
            );
            $act = 'delete';
            $r = json_encode($r);
            $response = curl_exec($curl);

        $check1 = ApiActivityLog::where('item_id',$item_id)->first();
        if (empty($check1)) {
            $log = new ApiActivityLog;
            $log->user_id = Auth::user()->id;
            $log->item_id = $item_id;
            $log->item_name = $request->name;
            $log->source = $sourc.' Api URL => '.$API_URL;
            $log->send = $r;
            $log->receive = json_encode($response);
            $log->action = $act;
            $log->save();
        }else{
            ApiActivityLog::where('item_id', '=', $item_id)->update([
                'item_name' => $request->name,
                'source' => $sourc.' Api URL => '.$API_URL,
                'send' => $r,
                'receive' => json_encode($response),
                'action' => $act
                ]);
        }
            curl_close($curl);

            return true;
        }
        $colors = [];
        if (count($request->item_colors) > 0) {
            foreach($request->item_colors as $color){
                $tmp = Colors::where('id',$color)->first();
                $colors[] = $tmp->title;
            }
        }

        if (count($colors) == 0) {
            $colors[] = 'null';
        }
        $sizes = [];
        if (count($request->item_sizes) > 0) {
            foreach($request->item_sizes as $size){
                $tmp = Sizes::where('id',$size)->first();
                $sizes[] = $tmp->title;
            }
        }

        if (count($sizes) == 0) {
            $sizes[] = 'null';
        }
        $cat = Categories::where('id',$request->category_id)->first();
        $tag = [];
        $subtag = [];
        if (count($request->tags) > 0) {
            foreach($request->tags as $row){
                $tmp = Tags::where('id',$row)->first();
                if($tmp->is_subtag == 0){
                    $tag[] = $tmp->title;
                }else{
                    $subtag[] = $tmp->title;
                }
            }
        }

        if (count($tag) == 0) {
            $tag[] = 'null';
        }
        if (count($subtag) == 0) {
            $subtag[] = 'null';
        }
        $images = [];
		
        if (count($gallery) > 0) {
            foreach($gallery as $row){
				if(!empty($row) && $row != null && $row != 'null' && $row != ''){ 
                $images[] = asset('laravel/public/uploads/items').'/'.$row;
				}
            }
        }
        if (count($images) > 0) 
			$images = implode(', ', $images);
		else
            $images = '';
		
        $curl = curl_init();
        if ($type == 'add') {
            curl_setopt_array($curl, array(
                CURLOPT_URL => $API_URL.'/add',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array(
                    'category' => '8',
                    'subcategory' => '0',
                    'parent' => '0',
                    'name' => $request->name,
                    'price_type' => 'Fixed',
                    'displayprice' => '1pc ($22.00)',
                    'price' => $price,
                    'orgpirce' => $request->cost,
                    'check_cost' => $request->cost,
                    'details' => $request->description,
                    'Disclaimer' => 'S-123 K38 Wireless True Headset&nbsp;',
                    'image' => $images,
                    'color' => implode(', ', $colors),
                    'size' => implode(', ', $sizes),
                    'quantity_left' => '0',
                    'status' => 'I',
                    'product_tag_name' => implode(', ', $tag),
                    'product_subtag_name' => implode(', ', $subtag),
                    'sku' => $sku
                    ),
                    CURLOPT_HTTPHEADER => array(
                        'api-key: 513e856b-24b4-415b-9297-01865b6fc40f'
                    ),
            ));
            $r = array(
                'category' => '8',
                'subcategory' => '0',
                'parent' => '0',
                'name' => $request->name,
                'price_type' => 'Fixed',
                'displayprice' => '1pc ($22.00)',
                'price' =>  $price,
                'orgpirce' => $request->cost,
                'check_cost' => $request->cost,
                'details' => $request->description,
                'Disclaimer' => 'S-123 K38 Wireless True Headset&nbsp;',
                'image' => $images,
                'color' => implode(', ', $colors),
                'size' => implode(', ', $sizes),
                'quantity_left' => '0',
                'status' => 'I',
                'product_tag_name' => implode(', ', $tag),
                'product_subtag_name' => implode(', ', $subtag),
                'sku' => $sku
            );
            $act = 'add';
        }
        if ($type == 'edit') {

          curl_setopt_array($curl, array(
                CURLOPT_URL => $API_URL.'/edit',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array(
                    'category' => '8',
                    'subcategory' => '0',
                    'parent' => '0',
                    'name' => $request->name,
                    'price_type' => 'Fixed',
                    'displayprice' => '1pc ($22.00)',
                    'price' =>  $price,
                    'orgpirce' => $request->cost,
                    'check_cost' => $request->cost,
                    'details' => $request->description,
                    'Disclaimer' => 'S-123 K38 Wireless True Headset&nbsp;',
                    'image' => $images,
                    'color' => implode(', ', $colors),
                    'size' => implode(', ', $sizes),
                    'quantity_left' => '0',
                    'status' => 'I',
                    'product_tag_name' => implode(', ', $tag),
                    'product_subtag_name' => implode(', ', $subtag),
                    'sku' => $sku
                    ),
                    CURLOPT_HTTPHEADER => array(
                        'api-key: 513e856b-24b4-415b-9297-01865b6fc40f'
                    ),
            ));
            $r = array(
                'category' => '8',
                'subcategory' => '0',
                'parent' => '0',
                'name' => $request->name,
                'price_type' => 'Fixed',
                'displayprice' => '1pc ($22.00)',
                'price' =>  $price,
                'orgpirce' => $request->cost,
                'check_cost' => $request->cost,
                'details' => $request->description,
                'Disclaimer' => 'S-123 K38 Wireless True Headset&nbsp;',
                'image' => $images,
                'color' => implode(', ', $colors),
                'size' => implode(', ', $sizes),
                'quantity_left' => '0',
                'status' => 'I',
                'product_tag_name' => implode(', ', $tag),
                'product_subtag_name' => implode(', ', $subtag),
                'sku' => $sku
            );
            $act = 'edit';
        }
        $r = json_encode($r);

        $response = curl_exec($curl);
        $check1 = ApiActivityLog::where('item_id',$item_id)->first();
        if (empty($check1)) {
            $log = new ApiActivityLog;
            $log->user_id = Auth::user()->id;
            $log->item_id = $item_id;
            $log->item_name = $request->name;
            $log->source = $sourc.' Api URL => '.$API_URL;
            $log->send = $r;
            $log->receive = json_encode($response);
            $log->action = $act;
            $log->save();
        }else{
            ApiActivityLog::where('item_id', '=', $item_id)->update([
                'item_name' => $request->name,
                'source' => $sourc.' Api URL => '.$API_URL,
                'send' => $r,
                'receive' => json_encode($response),
                'action' => $act
                ]);
        }

        curl_close($curl);
        return true;
        // dd($response);
    }

    public function getApiUpdatedItems(){
        $API_URL = \Config::get('apiCreds.API_URL');
        //'https://bmaxlive.testpinless.com/items';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $API_URL.'/getitems',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => array(),
                CURLOPT_HTTPHEADER => array(
                    'api-key: 513e856b-24b4-415b-9297-01865b6fc40f'
                ),
        ));
        $response = curl_exec($curl);
        return view('front.item.temp', compact('response','API_URL'));

        curl_close($curl);
    }

    public function getApiLogs($id){
        $records = ApiActivityLog::where('item_id',$id)->first();
        if (!empty($records)) {
            return view('front.item.apiActivity', compact('records'));
        }else{
            return 404;
        }

    }

    public function createSystemImagesThumbnail() {
        $results = Items::all();

        foreach ($results as $row) {
            $destinationPath = public_path('/uploads/items');
            $destinatio = $destinationPath . '/' . $row['image'];

            $thumbpath = $destinationPath . '/thumbnail/' . $row['image'];
            if (file_exists($destinatio)) {
                if (!file_exists($thumbpath)) {
                    // $this->createThumbnail($destinatio, $thumbpath, 50, 50, $background=false);
                }
            } else {

                echo 'Not found in folder= ' . $row['image'];
                echo '</br>';
            }
        }

        echo 'Completed';
        echo '</br>';
    }

    public function imageDelete($id) {
        if (GalleryImages::where('id', '=', $id)->update(['deleted' => 1])) {
            return $id;
        } else {
            return 0;
        }
    }

    public function postUpdate(Request $request) {

        $item_id = $request->id;

        $validation = array(
            'name' => 'required|max:200',
            'upc_barcode' => 'required|min:2|max:25',
//            'sale_price' => 'required',
//            'code' => 'min:2|max:190|unique:items,code,' . $item_id,
            'id' => 'required',
            'cost' => 'required',
            'image' => 'mimes:jpeg,bmp,png,gif',
        );

        $image_name = '';
        $input = $request->all();
        DB::beginTransaction();
        try{
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        if (Input::hasFile('image')) {
            $image = Input::file('image');
            $image_name = rand(111, 999) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/items');
            $image->move($destinationPath, $image_name);
            $destinatio = $destinationPath . '/' . $image_name;
            $thumbpath = $destinationPath . '/thumbnail/' . $image_name;
            //    $this->createThumbnail($destinatio, $thumbpath, 50, 50, $background=false);
            $privious_image = Items::select('image')->where('id', '=', $item_id)->first();

            // unlink($destinationPath.'/thumbnail/'.$privious_image['image']);
            // unlink($destinationPath.'/'.$privious_image['image']);
//            Image::make($fileName, $file, 'uploads/items/', 'uploads/items/thumbnail/', 100, 100);
            $input['image'] = $image_name;
        } else {
            array_forget($input, 'image');
        }
        $gallery = [];
        if ($request->hasFile('gallery_image')) {
            $files = $request->file('gallery_image');
            foreach ($files as $file) {
                $filename = rand(111, 999) . time() . '.' . $file->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/items/gallery');
                $file->move($destinationPath, $filename);
                $image = new GalleryImages;
                $image->item_id = $item_id;
                $image->image = $filename;
                $image->sort_order = 0;
                $image->save();
                $gallery[] = $filename;
            }
        }
        array_forget($input, 'gallery_image');

        if (isset($request->item_colors)) {
            ItemColors::where('item_id', $request->id)->delete();
            if (count($request->item_colors) > 0) {
                foreach ($request->item_colors as $color) {

                    $item_color = new ItemColors;
                    $item_color->item_id = $request->id;
                    $item_color->color_id = $color;
                    $item_color->save();
                }
            }
            array_forget($input, 'item_colors');
        } else
            ItemColors::where('item_id', $request->id)->delete();

        if (isset($request->item_sizes)) {
            ItemSizes::where('item_id', $request->id)->delete();
            if (count($request->item_sizes) > 0) {
                foreach ($request->item_sizes as $size) {

                    $item_size = new ItemSizes;
                    $item_size->item_id = $request->id;
                    $item_size->size_id = $size;
                    $item_size->save();
                }
            }
            array_forget($input, 'item_sizes');
        } else
            ItemSizes::where('item_id', $request->id)->delete();

        if (isset($request->item_types)) {
            ItemTypes::where('item_id', $item_id)->delete();
            if (count($request->item_types) > 0) {
                foreach ($request->item_types as $type) {

                    $item_type = new ItemTypes;
                    $item_type->item_id = $item_id;
                    $item_type->type_id = $type;
                    $item_type->save();
                }
            }
            array_forget($input, 'item_types');
        } else
            ItemTypes::where('item_id', $item_id)->delete();

        
        if(isset($request->deletedTag)){
            if (count($request->deletedTag) > 0) {
                TagItemRelation::where('item_id', $item_id)->whereIn('tag_id',$request->deletedTag)->delete();
            }
            array_forget($input, 'deletedTag');
        }

        if (isset($request->tags)) {
            if (count($request->tags) > 0) {
                foreach ($request->tags as $tag) {
                    $tagItemRelation = new TagItemRelation;
                    $tagItemRelation->item_id = $item_id;
                    $tagItemRelation->tag_id = $tag;
                    $tagItemRelation->save();
                }
            }
            array_forget($input, 'tags');
        }
        if (!isset($request->increase_effect)) {
            $input['increase_effect'] = 0;
        }
        if (!isset($request->decrease_effect)) {
            $input['decrease_effect'] = 0;
        }

        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        Items::where('id', '=', $item_id)->update($input);
        DB::commit();
        $item = Items::where('id', '=', $item_id)->first();
        $itemcheck = Items::where('id', '!=', $item_id)->where('upc_barcode',$input['upc_barcode'])->first();
        $barcodeexist="";
        if(!empty($itemcheck)){
           $barcodeexist="Upc/Barcode already exist on $itemcheck->name";
        }
        self::createSubItems($item_id, $request->item_colors, $request->item_sizes, $request->item_types);
        $type = 'edit';
        $price = 0;
        $gl_imgs = [];
        
        $g_imgs = GalleryImages::where('item_id',$item_id)->where('deleted',0)->get();
        
        $gl_imgs[] = $item->image;
        foreach($g_imgs as $row){
            $gl_imgs[] = $row->image;
        }
        $sourc = 'ItemController::postUpdated';
        $this->makecurlRequest($request,$price,$gl_imgs,$item->code,$type,$item_id,$sourc);
        
        Session::flash('success', 'Item has been updated. '. $barcodeexist.'');
        return redirect()->back();
    }catch(Exception $e) {
        DB::rollBack();
        Session::flash('error', $e->getMessage());
        return redirect()->back();
    }
    }

    public function delete($id) {
        $item = Items::where('id', '=', $id)->first();
        Items::where('id', '=', $id)->update(['deleted' => 1]);
        Items::where('parent_item_id', '=', $id)->update(['deleted' => 1]);
        TagItemRelation::where('item_id', $id)->delete();
        $type = 'delete';
        $image_name = '';
        $gallery = '';
        $price = 0;
        $sourc = 'ItemController::delete';
        $this->makecurlRequest($item,$price,$gallery,$item->code,$type,$id,$sourc);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function changeStatus($status, $id) {

        $item = Items::where('id', '=', $id)->first();
        if ($item->status == 1) {
            Items::where('id', '=', $id)->update(['status' => '0']);
            Items::where('parent_item_id', '=', $id)->update(['status' => '0']);
            $message = 'Published Successfully Changed!';
        } else {
            Items::where('id', '=', $id)->update(['status' => '1']);
            Items::where('parent_item_id', '=', $id)->update(['status' => '1']);
            $message = 'Published Successfully Changed!';
        }

        return $message;
    }

    public function copy($item_id) {

        $old_item = Items::where('id', $item_id)->first();
        $old_sub_items = Items::where('parent_item_id', $item_id)->where('deleted', '0')->get();
        
         $last_item = Items::where('parent_item_id', '0')->orderBy('id', 'desc')->first();
        $last_item_sku = $last_item->code;
        $last_item_sku = ltrim($last_item_sku, 'S');
        $new_item_sku = 'S' . ($last_item_sku + 1);

        $item = new Items;
        $item->name = $old_item->name . '-copy';
        $item->category_id = $old_item->category_id;
        $item->upc_barcode = $old_item->upc_barcode;
        $item->code = $new_item_sku;
        $item->cost = $old_item->cost;
        $item->description = $old_item->description;
        $item->serial = $old_item->serial;
        $item->api = $old_item->api;
        $item->created_by = $old_item->created_by;
        $item->image = $old_item->image;

        $item->parent_item_id = $old_item->parent_item_id;
        $item->has_sub_item = $old_item->has_sub_item;
        $item->type_id = $old_item->type_id;
        $item->color_id = $old_item->color_id;
        $item->size_id = $old_item->size_id;
        $item->name_2 = $old_item->name_2;
        $item->save();

        $new_item_id = $item->id;
//        $new_item_code = $old_item->code . '-' . $new_item_id;
//        Items::where('id', '=', $new_item_id)->update(['code' => $new_item_code]);

        $item_colors = ItemColors::where('item_id', $item_id)->get();
        if (count($item_colors) > 0) {
            foreach ($item_colors as $item) {
                $item_color = new ItemColors;
                $item_color->item_id = $new_item_id;
                $item_color->color_id = $item->color_id;
                $item_color->save();
            }
        }

        $item_types = ItemTypes::where('item_id', $item_id)->get();
        if (count($item_types) > 0) {
            foreach ($item_types as $item) {
                $item_type = new ItemTypes;
                $item_type->item_id = $new_item_id;
                $item_type->type_id = $item->type_id;
                $item_type->save();
            }
        }

        $item_sizes = ItemSizes::where('item_id', $item_id)->get();
        if (count($item_sizes) > 0) {
            foreach ($item_sizes as $item) {
                $item_size = new ItemSizes;
                $item_size->item_id = $new_item_id;
                $item_size->size_id = $item->size_id;
                $item_size->save();
            }
        }

        foreach ($old_sub_items as $old_item) {

            $item = new Items;
            $item->name = $old_item->name . '-copy';
            $item->category_id = $old_item->category_id;
            $item->upc_barcode = $old_item->upc_barcode;
            $item->code = $old_item->code . '-copy';
            $item->cost = $old_item->cost;
            $item->description = $old_item->description;
            $item->serial = $old_item->serial;
            $item->api = $old_item->api;
            $item->created_by = $old_item->created_by;
            $item->image = $old_item->image;

            $item->parent_item_id = $new_item_id;
            $item->has_sub_item = $old_item->has_sub_item;
            $item->type_id = $old_item->type_id;
            $item->color_id = $old_item->color_id;
            $item->size_id = $old_item->size_id;
            $item->name_2 = $old_item->name_2;
            $item->save();

            $new_sub_item_id = $item->id;
            $new_item_code = $old_item->code . '-' . $new_item_id;
            Items::where('id', '=', $new_sub_item_id)->update(['code' => $new_item_code]);
        }

        $message = 'Item # ' . $new_item_id . ' copied successfully';
        $path = 'item/edit/' . $new_item_id;
        Session::flash('success', $message);
        return redirect($path);
    }

    public function Combinations($arrays, $i = 0) {
//        https://stillat.com/blog/2018/04/11/laravel-5-creating-combinations-of-elements-with-crossjoin
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }

        // get combinations from subsequent arrays
        $tmp = self::combinations($arrays, $i + 1);

        $result = array();

        // concat each array from tmp with each element from $arrays[$i]
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                        array_merge(array($v), $t) :
                        array($v, $t);
            }
        }

        return $result;
    }

    private function createSubItems($item_id, $selected_item_colors, $selected_item_sizes, $selected_item_types) {

        $user_id = Auth::user()->id;

        if (count($selected_item_colors) == 0)
            $selected_item_colors[] = 0;

        if (count($selected_item_sizes) == 0)
            $selected_item_sizes[] = 0;

        if (count($selected_item_types) == 0)
            $selected_item_types[] = 0;

        $num_of_combinations = Arr::crossJoin($selected_item_types, $selected_item_sizes, $selected_item_colors);
//        dd($num_of_combinations); die;
        $master_item = Items::where('id', $item_id)->first();

        Items::where('parent_item_id', '=', $item_id)->update(['deleted' => '1']);
        Items::where('id', '=', $item_id)->update(['has_sub_item' => '0']);

//        print_r($num_of_combinations); die;

        foreach ($num_of_combinations as $item) {

            $sub_item_name = '_SUB';
            $type_id = '0';
            $color_id = '0';
            $size_id = '0';

            if ($item[0] != '0') {
                $type_id = $item[0];
                $type_title = Types::where('id', $type_id)->select('title')->first();
                $sub_item_name = $sub_item_name . '-' . $type_title->title;
            }

            if ($item[1] != '0') {
                $size_id = $item[1];
                $size_title = Sizes::where('id', $size_id)->select('title')->first();
                $sub_item_name = $sub_item_name . '-' . $size_title->title;
            }

            if ($item[2] != '0') {
                $color_id = $item[2];
                $color_title = Colors::where('id', $color_id)->select('title')->first();
                $sub_item_name = $sub_item_name . '-' . $color_title->title;
            }

            $sub_item_exist = Items::where('parent_item_id', $item_id);

            if ($type_id != '0')
                $sub_item_exist = $sub_item_exist->where('type_id', $type_id);
            if ($color_id != '0')
                $sub_item_exist = $sub_item_exist->where('color_id', $color_id);
            if ($size_id != '0')
                $sub_item_exist = $sub_item_exist->where('size_id', $size_id);

            $sub_item_exist = $sub_item_exist->first();

//            print_r($sub_item_exist); die;

            if (count($sub_item_exist) > 0 && $sub_item_name != '_SUB') {

                $old_item['name'] = $master_item->name . $sub_item_name;
                $old_item['name_2'] = $sub_item_name;
                $old_item['category_id'] = $master_item->category_id;
                // $old_item['upc_barcode'] = $master_item->upc_barcode;
                $old_item['code'] = $master_item->code . $sub_item_name;
                $old_item['cost'] = $master_item->cost;
                $old_item['description'] = $master_item->description;
                $old_item['serial'] = $master_item->serial;
                $old_item['api'] = $master_item->api;
                $old_item['created_by'] = $user_id;
                $old_item['image'] = $master_item->image;
                $old_item['parent_item_id'] = $master_item->id;
                $old_item['type_id'] = $type_id;
                $old_item['color_id'] = $color_id;
                $old_item['size_id'] = $size_id;
                $old_item['deleted'] = '0';
                Items::where('id', '=', $sub_item_exist->id)->update($old_item);
            } else {

                if ($type_id == 0 && $color_id == 0 && $size_id == 0) {

                    //do Nothing
                } else {

                    $item_new = new Items;
                    $item_new->name = $master_item->name . $sub_item_name;
                    $item_new->name_2 = $sub_item_name;
                    $item_new->category_id = $master_item->category_id;
                    // $item_new->upc_barcode = $master_item->upc_barcode;
                    $item_new->code = $master_item->code . $sub_item_name;
                    $item_new->cost = $master_item->cost;
                    $item_new->description = $master_item->description;
                    $item_new->serial = $master_item->serial;
                    $item_new->api = $master_item->api;
                    $item_new->created_by = $user_id;
                    $item_new->image = $master_item->image;
                    $item_new->parent_item_id = $master_item->id;
                    $item_new->type_id = $type_id;
                    $item_new->color_id = $color_id;
                    $item_new->deleted = '0';
                    $item_new->size_id = $size_id;
                    $item_new->save();
                }
            }
        }

        $items_sub = Items::where('parent_item_id', '=', $item_id)->where('deleted', 0)->get();

        if (count($items_sub) > 0)
            Items::where('id', '=', $item_id)->update(['has_sub_item' => '1']);
    }

    private function addItemPriceInTemplate($item_id, $item_cost) {

        $admin_price_templates = PriceTemplates::where('user_type', 'admin')->get();
        foreach ($admin_price_templates as $price_template) {

            $price_template_id = $price_template->id;
            $item_selling_price = $item_cost * $price_template->price_percentage;
            $profit = $item_selling_price - $item_cost;
            $entry = ItemPriceTemplates::where('item_id', $item_id)->where('price_template_id', $price_template_id)->first();
            if (count($entry) > 0) {
                ItemPriceTemplates::where('item_id', '=', $item_id)->where('price_template_id', $price_template_id)->update(['selling_price' => $item_selling_price, 'profit' => $profit]);
            } else {
                $template = new ItemPriceTemplates;
                $template->item_id = $item_id;
                $template->price_template_id = $price_template_id;
                $template->selling_price = $item_selling_price;
                $template->profit = $profit;
                $template->display = 1;
                $template->save();
            }
        }


        $customers = Customers::get();

        foreach ($customers as $customer) {

            $p_template = ItemPriceTemplates::where('item_id', '=', $item_id)->where('price_template_id', $customer->selling_price_template)->first();

            $item_cost_for_customer = $p_template->selling_price;

            $customer_created_price_template = PriceTemplates::where('user_type', 'customer')->where('created_by', $customer->id)->get();
            if (count($customer_created_price_template) > 0) {

                foreach ($customer_created_price_template as $price_template) {

                    $price_template_id = $price_template->id;
                    $item_selling_price = $item_cost_for_customer * $price_template->price_percentage;
                    $profit = $item_selling_price - $item_cost_for_customer;
                    $entry = ItemPriceTemplates::where('item_id', $item_id)->where('price_template_id', $price_template_id)->first();
                    if (count($entry) > 0) {
                        ItemPriceTemplates::where('item_id', '=', $item_id)->where('price_template_id', $price_template_id)->update(['selling_price' => $item_selling_price, 'profit' => $profit]);
                    } else {
                        $template = new ItemPriceTemplates;
                        $template->item_id = $item_id;
                        $template->price_template_id = $price_template_id;
                        $template->selling_price = $item_selling_price;
                        $template->profit = $profit;
                        $template->display = 1;
                        $template->save();
                    }
                }
            }
        }
    }

    public function itemGallery() {

        $templaye_id = 1;
        $customer_id = 1;
        $t_ids = [];
        $templates = PriceTemplates::where('deleted', '=', '0')->where('user_type', 'admin')->get();
        
        $model = Items::where('items.deleted', '=', '0')->where('items.status', '=', '1')
                ->where('items.parent_item_id', '=', '0')
                
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
                ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $templaye_id)
                ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
                ->where('price_templates.deleted',0)
                ->where('price_templates.user_type','admin')
                ->select('items.*', 'i.quantity as inventory_quantity', 'ipt.selling_price as sale_price', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName')
                ->orderBy('items.id', 'desc');
        
        $drop_down_items = []; //$model->get();

        $model = $model->paginate(15);

        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item['id']] = $item['name'];
        }

        $items_list = $all_packages;

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id)
                        ->where('items.deleted', '0')
                        ->where('items.parent_item_id', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->pluck('c.name as category_name', 'items.category_id')->toArray();

        $category_id = 0;

        foreach ($model as $item) {

            if ($item->serial == 'no') {

                $a = \App\WarehouseInventory::where('item_id', $item->id)
                        ->select(DB::raw('sum(quantity) as num'))
                        ->get();

                if (count($a) > 0)
                    $item->warehouse_quantity = $a[0]->num;
                else
                    $item->warehouse_quantity = 0;
            }else {
                $item_quantity = \App\ItemSerials::where('item_id', $item->id)->where('type', 'inventory')->where('status', 'inactive')->count();
                $item->warehouse_quantity = $item_quantity;
            }
        }

        $item_id = 0;
        $category_id = 0;
        $template_id = 1;
        $invoice_btn = 0;
        $order_btn = 0;
        $items = $model;
        $tags = HomeController::getTags();
        $customers = Customers::select(['id', 'name','selling_price_template'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $customerTemplate = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
            $customerTemplate[$cat->id] = $cat->selling_price_template;
        }
        $customers = $new_cat;
        $new_tmp = [];
        $new_tmp [0] = 'All Templates';
        foreach ($templates as $tmp) {
            $new_tmp[$tmp->id] = $tmp->title;
        }
        $templates = $new_tmp;
        
        return view('front.item.item_gallery_listing', compact('items', 'categories', 'category_id', 'items_list', 'item_id', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','customers','customerTemplate'));
//         return view('front.customers.inventory.item_gallery_listing', compact('items', 'sale_price', 'items_list', 'item_id', 'categories', 'category_id', 'invoice_btn', 'order_btn', 'templates', 'template_id'));
    }

    public function itemGallerySearch(Request $request) {
// dd($request->all());
        $templaye_id = 1;

        $item_id = 0;
        $category_id = 0;
        $template_id = 0;
        $invoice_btn = 0;
        $order_btn = 0;

        if ($request->item_id == '0' && $request->category_id == '0' && $request->selling_price_template == '0')
            return redirect('/customer/item-gallery');

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id)
                        ->where('items.parent_item_id', '0')
                        ->where('items.deleted', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
//                ->select('items.category_id', 'c.name as category_name')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->pluck('c.name as category_name', 'items.category_id')->toArray();
        $categories[0] = 'Select Category';

        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'admin')
                        ->get();
                        // ->lists('title', 'id')->toArray();

        $items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $templaye_id);

        if ($request->item_id != '0' && $request->category_id == '0') {
            $items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
                    ->where('items.id', $request->item_id);
        } elseif ($request->item_id == '0' && $request->category_id != '0') {
//            $items = Items::where('category_id', $request->category_id)->get()->toArray();

            $items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
                    ->where('category_id', $request->category_id);
        } elseif ($request->item_id != '0' && $request->category_id != '0') {


            $items = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)
                            ->where('items.id', $request->item_id)->where('category_id', $request->category_id);
        }

        if ($request->selling_price_template != '0') {

            $items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
                    ->where('items.parent_item_id', '0');

            if ($request->category_id != '0')
                $items = $items->where('category_id', $request->category_id);

            if ($request->item_id != '0')
                $items = $items->where('items.id', $request->item_id);
        }

        $items = $items->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('items.parent_item_id', '0')
//                       ->where('items.serial', 'no')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                ->orderBy('items.id', 'desc');

        $items = $items->paginate(15);

        if ($request->category_id != '0') {
            $drop_down_items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
                    ->where('category_id', $request->category_id);
        } else {

            $drop_down_items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id);
        }

        $drop_down_items = $drop_down_items->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('items.parent_item_id', '0')
//                       ->where('items.serial', 'no')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                ->orderBy('items.id', 'desc');


        $drop_down_items = $drop_down_items->get();
        $q = $templaye_id;
        $items->appends(['search' => $q]);

        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $items_list = $all_packages;

        $item_id = $request->item_id;
        $category_id = $request->category_id;
        $template_id = $request->selling_price_template;
        
        $tags = HomeController::getTags();
        $customers = Customers::select(['id', 'name','selling_price_template'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $customerTemplate = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
            $customerTemplate[$cat->id] = $cat->selling_price_template;
        }
        $customers = $new_cat;
        $new_tmp = [];
        $new_tmp [0] = 'All Templates';
        foreach ($templates as $tmp) {
            $new_tmp[$tmp->id] = $tmp->title;
        }
        $templates = $new_tmp;
        return view('front.item.item_gallery_listing', compact('items', 'items_list', 'item_id', 'category_id', 'categories', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','customers','customerTemplate'));
    }

    public function itemGallerySearchNew(Request $request) {

        if (isset($request->customers_id) && $request->customers_id > 0) {
            $customer_id = $request->customers_id;
            $cu = Customers::where('id', $customer_id)->first();

            $templaye_id = $cu->selling_price_template;
        }else{
            if (isset($request->template) && $request->template > 0) {
                $templaye_id = $request->template;
            }else{
                $templaye_id = 0;
            }
            $customer_id = 0;
        }

        $item_ids = [];
        if (isset($request->subtag) || isset($request->tag)) {
            $subtagCheck = 0;
            if (isset($request->subtag)) {
                    $relation = DB::table('tag_item_relation')->where('tag_id',$request->subtag)->get();
                    $subtagCheck = 1;
            }elseif (isset($request->tag)) {
                    $relation = DB::table('tag_item_relation')->where('tag_id',$request->tag)->get();
            }

            foreach($relation as $row){
                    $item_ids[] = $row->item_id;
            }
            return $this->getTagItemsSearch($request, $item_ids, $subtagCheck);
        }else{
            $templates = PriceTemplates::where('deleted', '=', '0')->where('user_type', 'admin')->get();

            $model = Items::where('items.deleted', '=', '0')->where('items.status', '=', '1')
            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
            ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
            ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
            ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
            ->where('price_templates.deleted',0)
            ->where('price_templates.user_type','admin')
            ->select('items.*','i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title'
            ,'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName'
            );
    
            if ($templaye_id > 0) {
                $model = $model->where('ipt.price_template_id', $templaye_id);
            }
            $model = $model->orderBy('items.id', 'desc');
            $drop_down_items = '';
            if ($request->item_name != '') {
                $model = $model->where('items.upc_barcode',$request->item_name);
            } 
            $model = $model->paginate(15);
    
            if(count($model) == 0){
                $model = Items::where('items.deleted', '=', '0')->where('items.status', '=', '1')
                ->where('items.parent_item_id', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
                ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
                ->where('price_templates.deleted',0)
                ->where('price_templates.user_type','admin');
                if ($templaye_id > 0) {
                    $model = $model->where('ipt.price_template_id', $templaye_id);
                }
                $model = $model->select('items.*','i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title'
                ,'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName'
                )
                ->orderBy('items.id', 'desc');
    
                $drop_down_items = []; //$model->get();
    
                if ($request->item_name != '') {
                    $keyword = $request->item_name;
                    
                    $model = $model->where(function ($query) use($keyword) {
                        $query->where('items.name', 'like', '%' . $keyword . '%')
                            ->orWhere('items.upc_barcode', 'like', '%' . $keyword . '%');
                    });
                } 
    
                if (count($item_ids) > 0) {
                    $model = $model->whereIn('items.id',$item_ids);
                }
    
                $model = $model->paginate(15);
            }
    
    
            $items_list = []; //$all_packages;
    
            $categories = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)
                            ->where('items.deleted', '0')
                            ->where('items.parent_item_id', '0')
                            ->where('items.status', '1')
                            ->where('ipt.display', '1');
                            if (count($item_ids) > 0) {
                                $categories = $categories->whereIn('items.id',$item_ids);
                            }
                            $categories = $categories->orderBy('items.category_id', 'desc')
                            ->groupBy('items.category_id')
                            ->pluck('c.name as category_name', 'items.category_id')->toArray();
            $category_id = 0;
    
    
            foreach ($model as $item) {
    
                if ($request->item_name != '' && strlen($request->item_name) == 3 ) {
    
                    $type = 'invoice';
                    $old_item_price = CommonController::getItemPrice($item->id, $customer_id, $type);
    
                    if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                        $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                        $item->sale_price = $old_item_price;
                    }
                }
    
                if ($item->serial == 'no') {
    
                    $a = \App\WarehouseInventory::where('item_id', $item->id)
                            ->select(DB::raw('sum(quantity) as num'))
                            ->get();
    
                    if (count($a) > 0)
                        $item->warehouse_quantity = $a[0]->num;
                    else
                        $item->warehouse_quantity = 0;
                }else {
                    $item_quantity = \App\ItemSerials::where('item_id', $item->id)->where('type', 'inventory')->where('status', 'inactive')->count();
                    $item->warehouse_quantity = $item_quantity;
                }
    
            }
    
            $item_id = 0;
            $category_id = 0;
            $template_id = $templaye_id;
            $invoice_btn = 0;
            $order_btn = 0;
            $items = $model;
    
            $tags = HomeController::getTags();
            $customers = Customers::select(['id', 'name','selling_price_template'])->where('deleted', '=', '0')->get();
            $new_cat = [];
            $customerTemplate = [];
            $new_cat [0] = 'Select Customer';
            foreach ($customers as $cat) {
                $new_cat[$cat->id] = $cat->name;
                $customerTemplate[$cat->id] = $cat->selling_price_template;
            }
            $customers = $new_cat;
            $new_tmp = [];
            $new_tmp [0] = 'All Templates';
            foreach ($templates as $tmp) {
                $new_tmp[$tmp->id] = $tmp->title;
            }
            $templates = $new_tmp;
            return view('front.item.item_gallery_listing', compact('items', 'categories', 'category_id', 'items_list', 'item_id', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','customers','customerTemplate'));
        }

    }

    public function getTagItemsSearch($request, $item_ids, $subtagCheck){
        
        $template = PriceTemplates::where('is_default','2')->first();
        $templaye_id = $template->id;
        $model = Items::where('items.deleted', '=', '0')->where('items.status', '=', '1');
        if ($subtagCheck == 1) {
            $model = $model->where('items.parent_item_id', '=', '0');
        }

        $model = $model->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
        ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
        ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
        ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
        ->where('price_templates.deleted',0)
        ->where('price_templates.user_type','admin');
        if ($templaye_id > 0) {
            $model = $model->where('ipt.price_template_id', $templaye_id);
        }
        $model = $model->select('items.*','i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title'
        ,'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName'
        )
        ->orderBy('items.id', 'desc');

        $drop_down_items = [];

        if (count($item_ids) > 0) {
            $model = $model->whereIn('items.id',$item_ids);
        }
        
        $model = $model->groupBy('items.id')->paginate(15);

        $items_list = []; //$all_packages;

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id)
                        ->where('items.deleted', '0')
                        ->where('items.parent_item_id', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1');
                        if (count($item_ids) > 0) {
                            $categories = $categories->whereIn('items.id',$item_ids);
                        }
                        $categories = $categories->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->pluck('c.name as category_name', 'items.category_id')->toArray();
        $category_id = 0;

        $customer_id = $request->customer_id;
        foreach ($model as $item) {

            if ($request->item_name != '' && strlen($request->item_name) == 3 ) {

                $type = 'invoice';
                $old_item_price = CommonController::getItemPrice($item->id, $customer_id, $type);

                if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                    $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                    $item->sale_price = $old_item_price;
                }
            }

            if ($item->serial == 'no') {

                $a = \App\WarehouseInventory::where('item_id', $item->id)
                        ->select(DB::raw('sum(quantity) as num'))
                        ->get();

                if (count($a) > 0)
                    $item->warehouse_quantity = $a[0]->num;
                else
                    $item->warehouse_quantity = 0;
            }else {
                $item_quantity = \App\ItemSerials::where('item_id', $item->id)->where('type', 'inventory')->where('status', 'inactive')->count();
                $item->warehouse_quantity = $item_quantity;
            }

        }

        $item_id = 0;
        $category_id = 0;
        $template_id = $request->template;
        $invoice_btn = 0;
        $order_btn = 0;
        $items = $model;

        $tags = HomeController::getTags();
        $templates = PriceTemplates::where('deleted', '=', '0')->where('user_type', 'admin')->get();
        $customers = Customers::select(['id', 'name','selling_price_template'])->where('deleted', '=', '0')->get();
        $new_cat = [];
        $customerTemplate = [];
        $new_cat [0] = 'Select Customer';
        foreach ($customers as $cat) {
            $new_cat[$cat->id] = $cat->name;
            $customerTemplate[$cat->id] = $cat->selling_price_template;
        }
        $customers = $new_cat;

        $item_name = $request->item_name;
        $new_tmp = [];
        $new_tmp [0] = 'All Templates';
        foreach ($templates as $tmp) {
            $new_tmp[$tmp->id] = $tmp->title;
        }
        $templates = $new_tmp;
        return view('front.item.item_gallery_listing', compact('items', 'categories', 'category_id', 'items_list', 'item_id', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','customers','customerTemplate','item_name','template_id','customer_id'));
    }
    public function getTempleteCategories($template_id) {

//        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        $categories = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $template_id)
//                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
//                ->select('items.category_id', 'c.name as category_name')
                ->orderBy('items.category_id', 'desc')
                ->groupBy('items.category_id')
                ->select('c.name as category_name', 'items.category_id as category_id')
                ->get();

        $new_cat = [];
        foreach ($categories as $po) {
            $new_cat[$po->category_id] = $po->category_name;
        }
        $categories = $new_cat;
        $data = view('front.common.ajax_gallery_cat', compact('categories'))->render();
        return $data;
    }

    public function getTempleteItems($template_id, $category_id) {

//        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();


        $drop_down_items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $template_id)
//                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1');

        if ($category_id != '0')
            $drop_down_items = $drop_down_items->where('items.category_id', $category_id);

        $drop_down_items = $drop_down_items->select('items.*')
                ->orderBy('items.id', 'desc')
                ->get();

        $new_cat = [];
        foreach ($drop_down_items as $po) {
            $new_cat[$po->id] = $po->name;
        }
        $items = $new_cat;
        $data = view('front.common.ajax_gallery_items', compact('items'))->render();
        return $data;
    }

    function createThumbnail($filepath, $thumbpath, $thumbnail_width, $thumbnail_height, $background = false) {
        list($original_width, $original_height, $original_type) = getimagesize($filepath);
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);

        if ($original_type === 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        } else if ($original_type === 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        } else if ($original_type === 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        } else {
            return false;
        }

        $old_image = $imgcreatefrom($filepath);
        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height); // creates new image, but with a black background
        // figuring out the color for the background
        if (is_array($background) && count($background) === 3) {
            list($red, $green, $blue) = $background;
            $color = imagecolorallocate($new_image, $red, $green, $blue);
            imagefill($new_image, 0, 0, $color);
            // apply transparent background only if is a png image
        } else if ($background === 'transparent' && $original_type === 3) {
            imagesavealpha($new_image, TRUE);
            $color = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
            imagefill($new_image, 0, 0, $color);
        }

        imagecopyresampled($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
        $imgt($new_image, $thumbpath);
        return file_exists($thumbpath);
    }

    public function recalculate($item_id) {

        $item = Items::where('id', $item_id)->first();
        self::addItemPriceInTemplate($item_id, $item->cost);

        Session::flash('success', 'Price Updated in all templates');
        return redirect()->back();
    }

    public function priceUpdate(Request $request) {
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        if (Items::where('id', '=', $request->id)->update($input)) {
            return 'Cost updated';
        }
    }

    public function getItemPriceNew($item_id, $id, $type){
        $new_price = 0;
        if($type = 'invoice'){
            $customer = Customers::where('id', $id)->first();
            $old_item_price = CommonController::getItemPrice($item_id, $id, $type);
            
            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                
                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                $new_price = $old_item_price;
            }else{

                $old_item_price = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id',$item_id)->first();
                if(!empty($old_item_price)){
                    $new_price = $old_item_price->selling_price;
                }else{
                    $new_price = 0;
                }
            }
        }else if($type = 'order'){
            $client = Clients::where('id', $id)->first();
            $old_item_price = CommonController::getItemPrice($item_id, $id, $type);
            
            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                
                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                $new_price = $old_item_price;
            }else{
    
                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                if(!empty($old_item_price)){
                    $new_price = $old_item_price->selling_price;
                }else{
                    $new_price = 0;
                }
            }
        }
        
        return $new_price;
    }

    // public function upcUpdate(Request $request){
    //     if (!empty($request->code) && $request->id > 0) {
    //         $item = Items::where('id', '=', $request->id)->first();
    //         if ($item->parent_item_id > 0) {
    //             if(Items::where('id', '=', $request->id)->update(['upc_barcode' => $request->code])){
    //              $itemcheck = Items::where('id', '!=', $request->id)->where('upc_barcode',$request->code)->first();
    //             $barcodeexist="";
    //             if(!empty($itemcheck)){
    //            return $barcodeexist="Upc/Barcode already exist on $itemcheck->name";
    //             }else{
    //                 return '1';
    //             } 
    //             }else{
    //                 return '2';
    //             }
                
    //         }
    //     }else{
    //         return '3';
    //     }
    // }
    public function upcUpdate(Request $request){
        // if (!empty($request->code) && $request->id > 0) {
            $item = Items::where('id', '=', $request->id)->first();
            if ($item->parent_item_id > 0) {
                if(Items::where('id', '=', $request->id)->update(['upc_barcode' => $request->code])){
                 $itemcheck = Items::where('id', '!=', $request->id)->where('upc_barcode',$request->code)->first();
                $barcodeexist="";
                if(!empty($itemcheck) && !empty($request->code)){
               return $barcodeexist="Upc/Barcode already exist on $itemcheck->name";
                }else{
                    return '1';
                } 
                }else{
                    return '2';
                }
                
            }
        // }else{
        //     return '3';
        // }
    }

}
