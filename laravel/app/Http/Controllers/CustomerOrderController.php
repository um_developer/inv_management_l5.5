<?php

namespace App\Http\Controllers;

use DB;
use App\Items;
use App\Vendors;
use App\Invoices;
use App\InvoiceItems;
use App\PriceTemplates;
use App\CustomerItemCost;
use App\ItemPriceTemplates;
use App\Bundles;
use App\BundleItems;
use App\Orders;
use App\PrintPageSetting;
use App\OrderItems;
use App\Clients;
use App\Customers;
use App\PoItems;
use App\Inventory;
use App\Payments;
use App\PurchaseOrders;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\CustomerInventory;
use Session;
use Carbon\Carbon;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;
use App\ActivityLog;
use App\CustomersItemImpact;

class CustomerOrderController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | CustomerOrderController Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user_id = '0';
    private $customer_id = '0';
    private $customer_obj = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->get();
        if ($customer[0]->client_menu == 0)
            Redirect::to('customer/dashboard')->send();
        $this->customer_id = $customer[0]->id;
        $this->customer_obj = $customer[0];
        $this->show_bundle = $customer[0]->show_bundle;
    }

    public function index() {

        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();
        $sync = Customers::select('is_order_sync')->where('id', $this->customer_id)->first();

        $model = Orders::where('orders.deleted', '=', '0')
                ->where('orders.created_by', $this->customer_id)
                ->whereBetween('orders.statement_date', [$start_date, $end_date])
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id')
                ->orderBy('orders.id', 'desc')
                ->select('orders.*', 'c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
                ->get();
// dd($model);
        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->get();

        $new_cat [0] = 'Select Client';
        //$new_cat = [];
        $template_name = '';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $clients = $new_cat;
        $selected_report_type = '';
        $date = $start_date . ' - ' . $end_date;
        $orders = $model;
        $selected_order_number = '';
        return view('front.customers.orders.index', compact('orders', 'date', 'clients', 'selected_report_type', 'sync', 'selected_order_number'));
    }

    public function search(Request $request) {

        $date = $request->date_range;

        if (isset($request->client_id)) {
            $client_id = $request->client_id;
        }
        if (isset($request->type)) {
            $selected_report_type = $request->type;
        }
        if (isset($request->order_number)) {
            $selected_order_number = $request->order_number;
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }
        $model = '';

        if (!empty($selected_order_number)) {
            $model = Orders::where('orders.deleted', '=', '0')
                    ->where('orders.created_by', $this->customer_id)
                    ->where('orders.id', $selected_order_number)
                    ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                    ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id')
                    ->orderBy('orders.id', 'desc')
                    ->select('orders.*', 'c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
                    ->get();
        } else {
            $model = Orders::where('orders.deleted', '=', '0')
                    ->where('orders.created_by', $this->customer_id)
                    ->whereBetween('orders.statement_date', [$start_date, $end_date])
                    ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                    ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id');
            if (!empty($selected_report_type)) {

                $model = $model->where('orders.status', $selected_report_type);
            }
            if (!empty($client_id)) {

                $model = $model->where('orders.client_id', $client_id);
            }

            $model = $model->orderBy('orders.id', 'desc')
                    ->select('orders.*', 'c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
                    ->get();
        }


        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->get();

        $new_cat [0] = 'Select Client';
        //$new_cat = [];
        $template_name = '';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $clients = $new_cat;
        $sync = Customers::select('is_order_sync')->where('id', $this->customer_id)->first();
        $orders = $model;
        return view('front.customers.orders.index', compact('orders', 'date', 'clients', 'selected_report_type', 'sync', 'selected_order_number'));
    }

    public function create($client_id = '0') {

        $is_sync = DB::table('customers')->select('is_order_sync')->where('user_id', Auth::user()->id)->first();
        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->get();

        $list_words = \App\Lists::where('deleted', '0')->lists('title', 'id')->toArray();
        $list_words[0] = 'Select List';
        ksort($list_words);

        $address = '';
        $phone = '';
        $new_cat [0] = 'Select Client';
        $template_name = '';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $client_sale_price_factor = 1;
        $client_name = '';
        $new_template_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($this->customer_id);
        if ($client_id != '0') {
            $client = Clients::where('id', $client_id)
                    ->leftjoin('states as s', 's.code', '=', 'clients.state')
                    ->select('clients.*', 's.title as state')
                    ->first();
            $address = $client->address1 . ' ' . $client->address2 . ' ' . $client->city . ' ' . $client->state . ' ' . $client->zip_code;
            $client_name = $client->name;
            $phone = $client->phone;
            if ($client->selling_price_template != '0' && $client->selling_price_template != null) {
                $template = PriceTemplates::where('id', $client->selling_price_template)->first();

                if (count($template) > 0) {
                    $template_name = $template->title;
                    $items = CommonController::getItemFormsData($client->selling_price_template);
                } else {
                    $template_name = 'Customer Default Template';
                    $items = CommonController::getItemFormsData($new_template_id);
                }
            } else {
                $template_name = 'Customer Default Template';
                $items = CommonController::getItemFormsData($new_template_id);
            }
        } else {

            $items = CommonController::getItemFormsData($new_template_id);
        }

        $clients = $new_cat;
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
      $show_bundle=$this->show_bundle;
        return view('front.customers.orders.create', compact('items', 'clients', 'client_id', 'client_name', 'address', 'template_name', 'phone', 'is_sync', 'list_words','bundles','show_bundle'));
    }

    public function edit($id, $client_id = '') {


        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->get();

        $list_words = \App\Lists::where('deleted', '0')->lists('title', 'id')->toArray();
        $list_words[0] = 'Select List';
        ksort($list_words);

        $client_name = '';
//        $client_id = '0';
        $template_name = '';
        $address = '';
        $phone = '';
        $template_name = '';
        $new_cat [0] = 'Select Client';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $model = Orders::where('orders.id', '=', $id)
                ->where('orders.created_by', '=', $this->customer_id)
                ->get();


        if (count($model) == 0) {
            return redirect('/');
        }

        $model = Orders::where('orders.id', '=', $id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->select('orders.*', 'c.id as client_id')
                ->get();

        $client_sale_price_factor = 1;

        $new_template_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($this->customer_id);

        if ($model[0]->client_id != 0) {

            $client = Clients::where('id', $model[0]->client_id)
                    ->leftjoin('states as s', 's.code', '=', 'clients.state')
                    ->select('clients.*', 's.title as state')
                    ->first();
            $address = $client->address1 . ' ' . $client->address2 . ' ' . $client->city . ' ' . $client->state . ' ' . $client->zip_code;
            $client_name = $client->name;
            $phone = $client->phone;

            if ($client->selling_price_template != '0' && $client->selling_price_template != null) {
                $template = PriceTemplates::where('id', $client->selling_price_template)->first();

                if (count($template) > 0) {
                    $template_name = $template->title;
                    $items = CommonController::getItemFormsData($client->selling_price_template);
                } else {
                    $template_name = 'Customer Default Template';
                    $items = CommonController::getItemFormsData($new_template_id);
                }
            } else {
                $template_name = 'Customer Default Template';
                $items = CommonController::getItemFormsData($new_template_id);
            }
        } else {
            $items = CommonController::getItemFormsData($new_template_id);
        }



        if ($client_id != '') {
            $my_client = Clients::where('id', $client_id)->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->first();
            $po_items = self::getTemplateItemPrice($id, $client_id, $my_client->selling_price_template);

            if ($this->customer_obj->is_order_sync == '1') {
                self::ApprovedOrderAndSyncInvoice($id);
                return redirect('customer/order/edit/' . $id);
            }

            Session::flash('success', 'Client name is updated in this order');
            return redirect('customer/order/edit/' . $id);
        } else {

            $po_items = OrderItems::where('order_id', '=', $id)
                    ->leftjoin('items as i', 'i.id', '=', 'order_items.item_id')
                    ->leftjoin('bundles as b', 'b.id', '=', 'order_items.bundle_id')
                    ->where('order_items.deleted', '=', '0')
                    ->select('order_items.*', 'i.name as item_name', 'i.cost as cost', 'i.image as image', 'b.name as bundle_name')
                    ->get();
                   
        }

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        $clients = $new_cat;

        if ($model->client_id != 0)
            $client_id = $model->client_id;




            $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
            $new_bundle = [];
            foreach ($bundles as $getbundles) {
                $new_bundle[0] = 'Select Bundle';
                $new_bundle[$getbundles->id] = $getbundles->name;
            }
            $bundles=$new_bundle;
            $show_bundle=$this->show_bundle;
        return view('front.customers.orders.edit', compact('items', 'model', 'po_items', 'client_name', 'clients', 'client_id', 'address', 'template_name', 'phone', 'list_words','bundles','show_bundle'));
    }

    public function postCreate(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $validation = array(
        );

        if ($input['client_id'] == '0') {
            Session::flash('error', 'Please select client.');
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        DB::beginTransaction();
        $items = $input['num'];
        $order = new Orders;
        $order->created_by = $this->customer_id;
        $order->list_id = $request->list_id;
        $order->client_id = $input['client_id'];
        $order->memo = $request->memo;
        $order->status = 'pending';
        $order->statement_date = $statement_date;
        $order->deleted = '1';

        $order->save();

//        $new_id = '22' . $order->id;
//        Orders::where('id', '=', $order->id)->update(['id' => $new_id]);
//        $order->id = $new_id;

        $total_invoice_price = 0;
        $total_quantity = 0;
        if (count($items) > 0 && isset($order->id)) {

            if ($input['item_id_1'] != '' || $input['item_id_2'] != '') {

                for ($i = 1; $i <= count($items); $i++) {
                    if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                        $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];

                        $order_items = new OrderItems;
                        $order_items->order_id = $order->id;
                        $order_items->client_id = $input['client_id'];
                        $order_items->item_id = $input['item_id_' . $i];
                        $order_items->item_unit_price = $input['price_' . $i];
                        $order_items->quantity = $input['quantity_' . $i];
                        $order_items->statement_date = $statement_date;


                        if ($input['start_serial_number_' . $i] > 0) {

                            $order_items->start_serial_number = $input['start_serial_number_' . $i];

                            ///////// end serial num or quantity /////
                            if ($input['end_serial_number_' . $i] > 0) {
                                $order_items->end_serial_number = $input['end_serial_number_' . $i];
                                $order_items->quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                                ;
                            } else {
                                $order_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                                $order_items->quantity = $input['quantity_' . $i];
                            }
                        }
                        if (isset($input['item_bundle_id_' . $i])) {
                            $order_items->bundle_id = $input['item_bundle_id_' . $i];
                            $order_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                        }

                        $order_items->total_price = $item_total_price;
                    //    $order_items->created_by = $user_id;
                        $total_invoice_price += $order_items->total_price;
                        $total_quantity += $order_items->quantity;
                        $order_items->save();
                    }
                }

                if ($total_quantity == 0) {
                    DB::rollBack();
                    Session::flash('error', 'Item count is greater than 0.');
                    return redirect()->back();
                }

                Orders::where('id', '=', $order->id)->update([
                    'deleted' => 0,
                    'total_price' => $total_invoice_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Order is not created. Please add atleast 1 item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Order is not created. Please try again.');
            return redirect()->back();
        }

        $path = 'customer/order/' . $order->id;

        DB::commit();
//         return redirect($path);
        Session::flash('success', 'Order is created successfully');
        return redirect($path);
        return redirect()->back();
    }

    public function postUpdate(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $user_id = Auth::user()->id;

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $order = Orders::where('id', '=', $input['id'])->get();
        if (isset($request->client_id) && $request->client_id == 0 && $request->client_id != '') {
            Session::flash('error', 'Order is not updated. Please select any Client..');
            return redirect()->back();
        }
        DB::beginTransaction();
        if (isset($request->client_id)) {
            Orders::where('id', '=', $input['id'])->update([
                'client_id' => $input['client_id'],
                'memo' => $input['memo'],
                'statement_date' => $statement_date,
                'list_id' => $input['list_id']
            ]);
        } else {

            Orders::where('id', '=', $input['id'])->update([
                'memo' => $input['memo'],
                'statement_date' => $statement_date,
                'list_id' => $input['list_id']
            ]);
        }

        $order = $order[0];

        $invoice_total_price = 0;
        $total_quantity = 0;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];
                    $po_item = OrderItems::where('id', '=', $input['po_item_id_' . $i])->get();
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($input['quantity_' . $i] == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        OrderItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {
                            $end_serial_num = 0;
                            $serial_quantity = 0;
                            ///////// end serial num or quantity /////
                            if ($input['end_serial_number_' . $i] > 0) {
                                $end_serial_num = $input['end_serial_number_' . $i];
                                $serial_quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                            } else {
                                $serial_quantity = $input['quantity_' . $i];
                                $end_serial_num = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            }

                            if ($total_quantity == 0) {
                                DB::rollBack();
                                Session::flash('error', 'Item count is greater than 0.');
                                return redirect()->back();
                            }

                            OrderItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $serial_quantity,
                                'total_price' => $item_total_price,
                                'start_serial_number' => $input['start_serial_number_' . $i],
                                'end_serial_number' => $end_serial_num,
                                'statement_date' => $statement_date,
                            ]);
                        } else {
                            OrderItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {


                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $order_items = new OrderItems;
                    $order_items->order_id = $input['id'];
                    $order_items->client_id = $order->client_id;
                    $order_items->item_id = $input['item_id_' . $i];
                    $order_items->item_unit_price = $input['price_' . $i];
                    $order_items->total_price = $item_total_price;
                    $order_items->quantity = $input['quantity_' . $i];
                    $order_items->statement_date = $statement_date;

                    if ($input['start_serial_number_' . $i] > 0) {
                        $order_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['end_serial_number_' . $i] > 0) {
                            $order_items->end_serial_number = $input['end_serial_number_' . $i];
                            $order_items->quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                            ;
                        } else {
                            $order_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $order_items->quantity = $input['quantity_' . $i];
                        }
                    }

                    if (isset($input['item_bundle_id_' . $i])) {
                        $order_items->bundle_id = $input['item_bundle_id_' . $i];
                        $order_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }
                    $order_items->quantity = $order_items->quantity;
//                    $order_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $order_items->quantity;
                    $order_items->save();
//                    print_r($invoice_items); die;
                }
            }

            if ($invoice_total_price > 0) {
                Orders::where('id', '=', $input['id'])->update([
                    'deleted' => 0,
                    'total_price' => $invoice_total_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                DB::rollBack();
                Session::flash('error', 'Order is not updated. Please add atleast 1 proper item.');
                return redirect()->back();
            }
        } else {
            DB::rollBack();
            Session::flash('error', 'Order is not updated. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Order is updated successfully');
        return redirect('customer/order/' . $order->id);
//        return redirect()->back();
    }

    public function postModified(Request $request) {

        $input = $request->all();
        $items = $input['num'];
        $user_id = Auth::user()->id;


        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        $order = Orders::where('id', '=', $input['id'])->get();
        if (isset($request->client_id) && $request->client_id == 0 && $request->client_id != '') {
            Session::flash('error', 'Order is not updated. Please select any Client..');
            return redirect()->back();
        }
        DB::beginTransaction();
        if (isset($request->client_id)) {
            Orders::where('id', '=', $input['id'])->update([
                'client_id' => $input['client_id'],
                'memo' => $input['memo'],
                'statement_date' => $statement_date,
                'list_id' => $input['list_id'],
            ]);
        } else {

            Orders::where('id', '=', $input['id'])->update([
                'memo' => $input['memo'],
                'statement_date' => $statement_date,
                'list_id' => $input['list_id'],
            ]);
        }

        $order = $order[0];

        $invoice_total_price = 0;
        $total_quantity = 0;
        $old_total_price = $order->total_price;

        if (count($items) > 0) {

            for ($i = 1; $i <= count($items); $i++) {

                //// if inivoice_item is already exist //////
                if (isset($input['po_item_id_' . $i])) {

                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $invoice_total_price += $item_total_price; //$input['total_' . $i];
                    $total_quantity += $input['quantity_' . $i];

                    $po_item = OrderItems::where('id', '=', $input['po_item_id_' . $i])->first();
                    $old_quantity = $po_item->quantity;
                    $serial_diff = $input['start_serial_number_' . $i] - $input['end_serial_number_' . $i];

                    ////// if quantity = 0 then delete //////
                    if ($item_total_price == 0 || $serial_diff > 0 && $input['start_serial_number_' . $i] > 0) {
                        OrderItems::where('id', '=', $input['po_item_id_' . $i])->update(['deleted' => '1',]);
                    } else {
                        if ($input['start_serial_number_' . $i] > 0) {

                            //// no serial functionality
                        } else {
                            OrderItems::where('id', '=', $input['po_item_id_' . $i])->update([
                                'item_unit_price' => $input['price_' . $i],
                                'quantity' => $input['quantity_' . $i],
                                'total_price' => $item_total_price,
                                'statement_date' => $statement_date,
                            ]);
                        }
                    }
                }
                ///////// if new item ////
                else if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {


                    $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];
                    $order_items = new OrderItems;
                    $order_items->order_id = $input['id'];
                    $order_items->client_id = $order->client_id;
                    $order_items->item_id = $input['item_id_' . $i];
                    $order_items->item_unit_price = $input['price_' . $i];
                    $order_items->total_price = $item_total_price;
                    $order_items->quantity = $input['quantity_' . $i];
                    $order_items->statement_date = $statement_date;

                    if ($input['start_serial_number_' . $i] > 0) {
                        $order_items->start_serial_number = $input['start_serial_number_' . $i];

                        ///////// end serial num or quantity /////
                        if ($input['end_serial_number_' . $i] > 0) {
                            $order_items->end_serial_number = $input['end_serial_number_' . $i];
                            $order_items->quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                            ;
                        } else {
                            $order_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                            $order_items->quantity = $input['quantity_' . $i];
                        }
                    }

                    if (isset($input['item_bundle_id_' . $i])) {
                        $order_items->bundle_id = $input['item_bundle_id_' . $i];
                        $order_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                    }
                    $order_items->quantity = $order_items->quantity;
//                    $order_items->created_by = $user_id;
                    $invoice_total_price += $item_total_price;
                    $total_quantity += $order_items->quantity;
                    $order_items->save();
//                    print_r($invoice_items); die;
                }
            }

//            if ($invoice_total_price > 0) {
            Orders::where('id', '=', $input['id'])->update([
                'deleted' => 0,
                'total_price' => $invoice_total_price,
                'total_quantity' => $total_quantity,
            ]);


            $price_diff = 0;
            if ($old_total_price > $invoice_total_price) {

                $price_diff = $old_total_price - $invoice_total_price;
                PaymentController::updateClientBalance($order->client_id, $price_diff, $order->id, 'order_neg', $statement_date);
            } elseif ($old_total_price < $invoice_total_price) {
                $price_diff = $invoice_total_price - $old_total_price;
                PaymentController::updateClientBalance($order->client_id, $price_diff, $order->id, 'order', $statement_date);
            } else {
                Payments::where('payment_type', 'order')->where('payment_type_id', $order->id)->update(['statement_date' => $statement_date]);
            }

            if ($order->invoice_refrence_id != '0'){
                CommonController::syncItems('order', $order->id);
            }
            else
                self::updateProfit($order->id);
        } else {
            DB::rollBack();
            Session::flash('error', 'Order is not Modified. Please try again.');
            return redirect()->back();
        }

        DB::commit();
        Session::flash('success', 'Order is Modified successfully');
        return redirect('customer/order/' . $order->id);
//        return redirect()->back();
    }

    public function delete($id) {

        $model = Orders::where('orders.id', '=', $id)
                ->where('orders.status', '!=', 'pending')
                ->where('orders.created_by', '=', $this->customer_id)
                ->get();

        if (count($model) > 0) {
            return redirect('/');
        }

        Orders::where('id', '=', $id)->where('status', '=', 'pending')->update(['deleted' => 1]);
        OrderItems::where('order_id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }
    public function printType($status,$client_id) {
        $customers = Clients::where('id', '=',$client_id)->update([
            'print_type' =>$status,
        ]);
       
    }
    public function detail($id, $show_profit = 0) {

        $model = Orders::where('orders.id', '=', $id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                ->where('orders.deleted', '=', '0')
                ->select('orders.*', 'c.id as client_id', 'c.name as client_name','c.print_type', 'c.address1 as client_address', 'c.phone as client_phone', 'c.show_image', 'c.show_serial', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'invoices.id as invoice_refrence')
                ->get();

        $po_items = OrderItems::where('order_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'order_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'order_items.bundle_id')
                ->where('order_items.deleted', '=', '0')
                ->select('order_items.*', 'i.name as item_name', 'i.image as image', 'b.name as bundle_name')
                ->get();
               

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.customers.orders.detail', compact('model', 'po_items', 'show_profit'));
    }

    public function printPage($id) {

        $model = Orders::where('orders.id', '=', $id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('states as s', 's.code', '=', 'c.state')
                ->leftjoin('lists as i', 'i.id', '=', 'orders.list_id')
                ->where('orders.deleted', '=', '0')
                ->select('orders.*', 'c.id as client_id', 's.title as state', 'c.name as client_name','c.print_type' ,'c.address1 as client_address1', 'c.address2 as client_address2', 'c.phone as client_phone', 'c.show_image', 'c.show_serial', 'c.balance', 'show_unit_price', 'c.email', 'c.city', 'c.zip_code', 'i.title as list_title')
                ->get();

        $customer = Customers::where('user_id', $this->user_id)
                ->leftjoin('states as s', 's.code', '=', 'customers.state')
                ->select('customers.*', 's.title as state')
                ->first();

            $po_items = OrderItems::where('order_id', '=', $id)
                ->leftjoin('items as i', 'i.id', '=', 'order_items.item_id')
                ->leftjoin('bundles as b', 'b.id', '=', 'order_items.bundle_id')
                ->where('order_items.deleted', '=', '0')
                ->select('order_items.*', 'i.name as item_name', 'i.image as image', 'b.name as bundle_name')
                ->get();

                if($model[0]->print_type!="Normal"){
                $newItemArray=[];
                $old_bundle_id = '';
                
                
                foreach($po_items as $item){
        /////agr item bundle wala na ho tu
                if($item->bundle_id =='' || $item->bundle_id =='0'){
                
                $newItemArray[] = $item;
                
                }else{
                /////agr item bundle ka 1st item ho tu
                if($item->bundle_id != $old_bundle_id){
                
                $newItemArray[] = $item;
                $old_bundle_id = $item->bundle_id;
                
                
                }else{
                /////agr existing bundle ka next item ho tu
                $previous_bundle_item = $newItemArray[(count($newItemArray)-1)];
                
                $item->item_name=$item->bundle_name;
                $item->quantity = $item->quantity + $previous_bundle_item->quantity;
                $item->item_unit_cost = $item->item_unit_cost + $previous_bundle_item->item_unit_cost;
                $item->total_price = $item->total_price + $previous_bundle_item->total_price;
                $item->total_cost = $item->total_cost + $previous_bundle_item->total_cost;
                $item->profit = $item->profit + $previous_bundle_item->profit;
                 $item->item_unit_price = $item->item_unit_price + $previous_bundle_item->item_unit_price;
                 $item->is_bundle =1;
                // $item->quantity = $item->quantity + $previous_bundle_item->quantity;
                
                $newItemArray[(count($newItemArray)-1)] = $item;
                
                
                }
                
                }
                
                }
                $po_items=$newItemArray;
               }


        $user_id = $this->customer_id;
        $setting = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id', $user_id)->first();

        if (count($setting) < 1) {
            $printPageSetting = new PrintPageSetting;
            $printPageSetting->bottom_text_1 = 'FOR ACCESSORIES VISIT BROADPOSTERS.COM';
            $printPageSetting->bottom_text_2 = 'NOTICE: OPEN ITEMS ARE NON REFUNDABLE. THANK YOU';
            $printPageSetting->user_type = 'customer';
            $printPageSetting->user_id = $user_id;
            $printPageSetting->save();
        }

        $setting = PrintPageSetting::where('user_type', '=', 'customer')->where('user_id', $user_id)->first();


        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.customers.orders.print', compact('model', 'po_items', 'customer', 'setting'));
    }

    public function autocomplete(Request $request) {
        $term = $request->term;
        $data = Items::where('code', 'LIKE', '%' . $term . '%')
                ->take(10)
                ->get();
        $result = array();
        foreach ($data as $key => $v) {
            $result[] = ['value' => $value->item];
        }
        return response()->json($results);
    }

    public function serialItemValidate($item_id, $start_serial, $quantity) {
        $items = ItemSerials::where('item_id', $item_id)->where('serial', $start_serial)->where('type', 'inventory')->get();
        if (count($items) > 0) {
            $a = ItemSerials::whereBetween('serial', [$start_serial, $start_serial + $quantity])->count();
            $quantity = $quantity + 1;
            if ($a == $quantity)
                return 1;
            else
                return 0;
        } else
            return 0;
    }

    public function orderSendToAdmin($status, $order_id) {

        $order = Orders::where('orders.deleted', '=', '0')
                ->where('id', $order_id)
                ->where('status', 'pending')
                ->first();

        if (count($order) == 0) {

            Session::flash('error', 'Some error occured.');
            return redirect()->back();
        }

        $invoice_total_price = 0;

        $order_items = OrderItems::where('deleted', '0')
                ->where('order_id', $order->id)
                ->get();
//        print_r($order_items); die;
        if (count($order_items) == 0) {

            Session::flash('error', 'Some error occured.');
            return redirect()->back();
        }

        DB::beginTransaction();

        $client = Clients::where('id', $order->client_id)->first();
        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        foreach ($order_items as $order_item) {

            $item_cost = self::getClientItemCost($this->customer_id, $order_item->item_id);

            if ($item_cost == '') {
                $customer = Customers::where('id', $this->customer_id)->first();
                $customer_price_template_id = $customer['selling_price_template'];

                if ($customer['selling_price_template'] == 0)
                    $customer_price_template_id = 1;

                $item_cost = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $row->item_id)->first();

                if (empty($item_price)) {

                    $item = Items::where('id', $row->item_id)->first();
                    if ($item->parent_item_id != '') {
                        $item_cost = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $item->parent_item_id)->first();
                    }
                }
            }

            OrderItems::where('id', $order_item->id)->update([
                'item_unit_price' => $order_item->item_unit_price,
                'total_price' => $order_item->total_price,
                'item_unit_cost' => $item_cost,
                'total_cost' => $item_cost * $order_item->quantity,
                'profit' => $order_item->total_price - ($item_cost * $order_item->quantity)
            ]);

            $total_price = $total_price + $order_item->total_price;
            $total_cost = $total_cost + ($item_cost * $order_item->quantity);
            $total_profit = $total_price - $total_cost;

            CustomerInventory::updateInventory($order_item->item_id, $order_item->quantity, 'subtract', $this->customer_id);
        }

        $message = 'Order has been charged Successfully.';
        $status_type = 'approved_charged';

        if ($status == 'invoice') {

            $invoice = new Invoices;
            $invoice->created_by = 'customer';
            $invoice->customer_id = $this->customer_id;
            //$invoice->order_id = $order->id;
            $invoice->memo = $order->memo;
            $invoice->list_id = $order->list_id;
            $invoice->total_price = $order->total_price;
            $invoice->total_quantity = $order->total_quantity;
            $invoice->statement_date = $order->statement_date;
            $invoice->deleted = '0';
            $invoice->save();

//            $new_id = '11' . $invoice->id;
//            Invoices::where('id', '=', $invoice->id)->update(['id' => $new_id]);
//            $invoice->id = $new_id;

            if (!isset($invoice->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect()->back();
            }

            $invoice_id = $invoice->id;
            foreach ($order_items as $item) {

                $customer = Customers::where('id', $this->customer_id)->first();
                $my_item_id = $item->item_id;
                $new_item = Items::where('id', $item->item_id)->first();
                if ($new_item->parent_item_id != '0')
                    $item->item_id = $new_item->parent_item_id;

                $item_exist = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id', $item->item_id)->first();

                $selling_price = 0;
                if (count($item_exist) > 0)
                    $selling_price = $item_exist->selling_price;

                $item_cost = self::getClientItemCost($this->customer_id, $item->item_id);

                $invoice_items = new InvoiceItems;
                $invoice_items->invoice_id = $invoice_id;
                $invoice_items->customer_id = $this->customer_id;
                $invoice_items->item_id = $my_item_id;
                $invoice_items->quantity = $item->quantity;
                $invoice_items->item_unit_price = $item_cost;
                $invoice_items->statement_date = $order->statement_date;
                $invoice_items->total_price = $invoice_items->item_unit_price * $invoice_items->quantity;
                $invoice_total_price = $invoice_total_price + $invoice_items->total_price;
                $invoice_items->save();
            }

            Invoices::where('id', $invoice_id)->update(['total_price' => $invoice_total_price]);
            $message = 'Order has been charged to client and send to Admin Successfully.';
            $status_type = 'approved';
        }


        $order = Orders::where('id', $order_id)->first();
        if ($total_price == '0')
            $total_price = $order->total_price;

        PaymentController::updateClientBalance($order->client_id, $order->total_price, $order->id, 'order', $order->statement_date);
        Orders::where('id', $order->id)->update(['status' => $status_type, 'total_price' => $total_price, 'total_cost' => $total_cost, 'profit' => $total_profit]);

        self::updateProfit($order->id);

        DB::commit();
        Session::flash('success', $message);
        return redirect('customer/order/create');

        return redirect()->back();
    }

    public function addItemToOrder($item_id, $quantity, $price) {


        if (strpos($item_id, ',') !== false) {
            $item_array = explode(",", $item_id);
            $quantity_array = explode(",", $quantity);
            $i = 0;
            foreach ($item_array as $item) {
                $res = self::innerAddItemToOrder($item_array[$i], $quantity_array[$i], $price);

                $url = url('customer/order/edit' . '/' . $res);
                $html = "<a href='" . $url . "'>" . $res . "</a>";
                $message = 'All items are successfully added in order# ' . $html;
                $i++;
            }
        } else {

            $res = self::innerAddItemToOrder($item_id, $quantity, $price);
            $item = Items::where('id', $item_id)->first();
            $url = url('customer/order/edit' . '/' . $res);
            $html = "<a href='" . $url . "'>" . $res . "</a>";
            $message = $item->name . ' is successfully added in order# ' . $html;
        }


        return $message;
    }

    private function innerAddItemToOrder($item_id, $quantity, $price) {

        $model = Orders::where('orders.deleted', '=', '0')
                ->where('client_id', 0)
                ->where('created_by', $this->customer_id)
                ->where('status', 'pending')
                ->orderBy('id', 'desc')
                ->first();

        $item = Items::where('id', $item_id)->first();

        if (count($model) > 0) {
            $order_items = new OrderItems;
            $order_items->order_id = $model->id;
            $order_items->client_id = $model->client_id;
            $order_items->item_id = $item_id;
            $order_items->quantity = $quantity;
            $order_items->item_unit_price = $price;
            $order_items->statement_date = date("Y-m-d");
            $order_items->total_price = $quantity * $price;
            $order_items->save();

            if (!isset($order_items->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect()->back();
            }

            Orders::where('id', '=', $model->id)->update([
                'total_price' => $model->total_price + ($quantity * $price),
                'total_quantity' => $model->total_quantity + $quantity,
            ]);
            $message = $model->id;
        } else {
            DB::beginTransaction();

            $order = new Orders;
            $order->created_by = $this->customer_id;
            $order->client_id = '0';
            $order->total_price = $quantity * $price;
            $order->total_quantity = $quantity;
            $order->statement_date = date("Y-m-d");
            $order->deleted = '0';
            $order->save();

//            $new_id = '22' . $order->id;
//            Orders::where('id', '=', $order->id)->update(['id' => $new_id]);
//            $order->id = $new_id;

            if (!isset($order->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect('customer/item-gallery');
            }

            $order_id = $order->id;

            $order_items = new OrderItems;
            $order_items->order_id = $order->id;
            $order_items->client_id = '0';
            $order_items->item_id = $item_id;
            $order_items->item_unit_price = $price;
            $order_items->total_price = $price * $quantity;
            $order_items->quantity = $quantity;
            $order_items->save();
            DB::commit();

            if (!isset($order_items->id)) {
                Session::flash('error', 'Some error occured.');
                return redirect('customer/item-gallery');
            }

            $message = $order_id;
        }
        return $message;
    }

    private function getTemplateItemPrice($order_id, $client_id, $price_template_id) {

        $customer_template = PriceTemplates::where('user_type', 'customer')->where('created_by', $this->customer_id)->where('is_default', '1')->first();
        $customer_default_template_id = $customer_template->id;
        $item_unit_price = 10;
        $cost = '5';
        $total_price = 0;


        if ($price_template_id == null || $price_template_id == '0')
            $price_template_id = $customer_default_template_id;
//         print_r($price_template_id); die;
        $po_items = OrderItems::where('order_id', '=', $order_id)
                ->leftjoin('items as i', 'i.id', '=', 'order_items.item_id')
                ->where('order_items.deleted', '=', '0')
                ->select('order_items.*', 'i.name as item_name', 'i.cost as cost', 'i.image as image')
                ->get();


        $new_po_items = [];

        foreach ($po_items as $items) {

            $new_sale_price = CommonController::getItemPrice($items->item_id, $client_id, 'order');

            if ($new_sale_price != '0') {
                $item_unit_price = $new_sale_price['sale_price'][0]['item_unit_price'];
            } else {
                $new_item = Items::where('id', $items->item_id)->first();
                if ($new_item->parent_item_id != '0')
                    $items->item_id = $new_item->parent_item_id;

                $item_exist = ItemPriceTemplates::where('price_template_id', $price_template_id)->where('item_id', $items->item_id)->first();
                if (count($item_exist) > 0) {
                    $item_unit_price = $item_exist->selling_price;
                } else {
                    $item_exist = ItemPriceTemplates::where('price_template_id', $customer_default_template_id)->where('item_id', $items->item_id)->first();
                    if (count($item_exist) > 0)
                        $item_unit_price = $item_exist->selling_price;
                    else
                        $item_unit_price = 0;
                }
            }

            $items->item_unit_price = $item_unit_price;
            $items->total_price = $items->item_unit_price * $items->quantity;
            $new_po_items[] = $items;

            $total_price = $total_price + $items->total_price;

            OrderItems::where('id', $items->id)->update([
                'item_unit_price' => $item_unit_price,
                'total_price' => $items->total_price,
//                    'total_quantity' => $items->total_prsice,
            ]);

            Orders::where('id', $order_id)->update([
                'client_id' => $client_id,
                'total_price' => $total_price,
            ]);
        }



//        return redirect()->back();
        return $new_po_items;
    }

    public static function getClientItemCost($customer_id, $item_id) {

        $item_cost = '';
        $items = Items::where('deleted', '0')->where('parent_item_id', $item_id)->lists('id')->toArray();
        $res = InvoiceItems::where('i.customer_id', $customer_id)->where('invoice_items.deleted', '0')->where('i.deleted', '0')->where('i.status', '!=', 'pending')
                ->leftjoin('invoices as i', 'i.id', '=', 'invoice_items.invoice_id');

        if (count($items) > 0)
            $res = $res->whereIn('item_id', $items);
        else
            $res = $res->where('item_id', $item_id);

        $res = $res->orderBy('invoice_items.id', 'desc')->first();

        if (empty($res)) {

            $customer = Customers::where('id', $customer_id)->first();
            $customer_price_template_id = $customer['selling_price_template'];

            if ($customer['selling_price_template'] == 0)
                $customer_price_template_id = 1;

            $item_cost = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $item_id)->first();

            if (empty($item_cost)) {

                $item = Items::where('id', $item_id)->first();
                if ($item->parent_item_id != '') {
                    $item_cost = ItemPriceTemplates::where('price_template_id', $customer_price_template_id)->where('item_id', $item->parent_item_id)->first();
                }
            }

            return $item_cost->selling_price;
        }


        return $res->item_unit_price;
    }

    private function updateProfit($order_id) {

        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        $order_items = OrderItems::where('order_id', $order_id)->where('deleted', '0')->get();

        foreach ($order_items as $order_item) {

            $item_cost = self::getClientItemCost($this->customer_id, $order_item->item_id);
            if ($item_cost == '')
                $item_cost = $order_item->item_unit_price;


            OrderItems::where('id', $order_item->id)->update([
                'item_unit_price' => $order_item->item_unit_price,
                'total_price' => $order_item->total_price,
                'item_unit_cost' => $item_cost,
                'total_cost' => $item_cost * $order_item->quantity,
                'profit' => $order_item->total_price - ($item_cost * $order_item->quantity)
            ]);

            $total_price = $total_price + $order_item->total_price;
            $total_cost = $total_cost + ($item_cost * $order_item->quantity);
            $total_profit = $total_price - $total_cost;
        }

        Orders::where('id', $order_id)->update(['total_price' => $total_price, 'total_cost' => $total_cost, 'profit' => $total_profit]);
    }

    public function syncCreate($client_id = '0') {

        $clients = Clients::select(['id', 'name'])->where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->get();

        $list_words = \App\Lists::where('deleted', '0')->lists('title', 'id')->toArray();
        $list_words[0] = 'Select List';
        ksort($list_words);

        $address = '';
        $phone = '';
        $new_cat [0] = 'Select Client';
        //$new_cat = [];
        $template_name = '';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }

        $client_sale_price_factor = 1;
        $client_name = '';
        $new_template_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($this->customer_id);
        if ($client_id != '0') {
            $client = Clients::where('id', $client_id)
                    ->leftjoin('states as s', 's.code', '=', 'clients.state')
                    ->select('clients.*', 's.title as state')
                    ->first();
            $address = $client->address1 . ' ' . $client->address2 . ' ' . $client->city . ' ' . $client->state . ' ' . $client->zip_code;
            $client_name = $client->name;
            $phone = $client->phone;
            if ($client->selling_price_template != '0' && $client->selling_price_template != null) {
                $template = PriceTemplates::where('id', $client->selling_price_template)->first();

                if (count($template) > 0) {
                    $template_name = $template->title;
                    $items = CommonController::getItemFormsData($client->selling_price_template);
                } else {
                    $template_name = 'Customer Default Template';
                    $items = CommonController::getItemFormsData($new_template_id);
                }
            } else {
                $template_name = 'Customer Default Template';
                $items = CommonController::getItemFormsData($new_template_id);
            }
        } else {

            $items = CommonController::getItemFormsData($new_template_id);
        }

        $clients = $new_cat;
        $bundles = Bundles::select(['id', 'name'])->where('deleted', '=', '0')->get();
        $new_bundle = [];
        foreach ($bundles as $getbundles) {
            $new_bundle[0] = 'Select Bundle';
            $new_bundle[$getbundles->id] = $getbundles->name;
        }
        $bundles=$new_bundle;
        $show_bundle=$this->show_bundle;
        return view('front.customers.orders.syne_create', compact('items', 'clients', 'client_id', 'client_name', 'address', 'template_name', 'phone', 'list_words','bundles','show_bundle'));
    }

    // start


    public function syncOrders(Request $request) {

        $input = $request->all();
        $user_id = Auth::user()->id;
        $validation = array(
        );

        if ($input['client_id'] == '0') {
            Session::flash('error', 'Please select client.');
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $timestamp = strtotime($request->statement_date);
        $statement_date = date("Y-m-d H:i:s", $timestamp);

        DB::beginTransaction();
        $items = $input['num'];
        $order = new Orders;
        $order->created_by = $this->customer_id;
        $order->client_id = $input['client_id'];
        $order->list_id = $input['list_id'];
        $order->memo = $request->memo;
        $order->status = 'pending';
        $order->statement_date = $statement_date;
        $order->deleted = '1';

        $order->save();

//        $new_id = '22' . $order->id;
//        Orders::where('id', '=', $order->id)->update(['id' => $new_id]);
//        $order->id = $new_id;

        $total_invoice_price = 0;
        $total_quantity = 0;
        if (count($items) > 0 && isset($order->id)) {

            if ($input['item_id_1'] != '' || $input['item_id_2'] != '') {

                for ($i = 1; $i <= count($items); $i++) {
                    if (isset($input['item_id_' . $i]) && $input['item_id_' . $i] != '' && $input['quantity_' . $i] != '0') {

                        $item_total_price = $input['quantity_' . $i] * $input['price_' . $i];

                        $order_items = new OrderItems;
                        $order_items->order_id = $order->id;
                        $order_items->client_id = $input['client_id'];
                        $order_items->item_id = $input['item_id_' . $i];
                        $order_items->item_unit_price = $input['price_' . $i];
                        $order_items->quantity = $input['quantity_' . $i];
                        $order_items->statement_date = $statement_date;


                        if ($input['start_serial_number_' . $i] > 0) {

                            $order_items->start_serial_number = $input['start_serial_number_' . $i];

                            ///////// end serial num or quantity /////
                            if ($input['end_serial_number_' . $i] > 0) {
                                $order_items->end_serial_number = $input['end_serial_number_' . $i];
                                $order_items->quantity = $input['end_serial_number_' . $i] - $input['start_serial_number_' . $i];
                                ;
                            } else {
                                $order_items->end_serial_number = $input['start_serial_number_' . $i] + $input['quantity_' . $i];
                                $order_items->quantity = $input['quantity_' . $i];
                            }
                        }

                        if (isset($input['item_bundle_id_' . $i])) {
                            $order_items->bundle_id = $input['item_bundle_id_' . $i];
                            $order_items->bundle_quantity = $input['item_bundle_quantity_' . $i];
                        }

                        $order_items->total_price = $item_total_price;
//                        $order_items->created_by = $user_id;
                        $total_invoice_price += $order_items->total_price;
                        $total_quantity += $order_items->quantity;
                        $order_items->save();
                    }
                }



                $a = Orders::where('id', '=', $order->id)->update([
                    'deleted' => 0,
                    'total_price' => $total_invoice_price,
                    'total_quantity' => $total_quantity,
                ]);
            } else {
                Session::flash('error', 'Order is not created. Please add atleast 1 item.');
                return redirect()->back();
            }
        } else {

            Session::flash('error', 'Order is not created. Please try again.');
        }

        $order = Orders::where('orders.deleted', '=', '0')
                ->where('id', $order->id)
                ->where('status', 'pending')
                ->first();


        self::ApprovedOrderAndSyncInvoice($order->id);
        return redirect()->back();
    }

// end

    public function printBulkPage($id) {

        $ids = explode(',', $id);

        $model = Orders::whereIn('orders.id', $ids)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->where('orders.deleted', '=', '0')
                ->select('orders.id', 'c.name as customer_name', 'orders.total_price', 'orders.statement_date', 'orders.memo', 'c.balance as balance', DB::raw('(select delivered_comments from comments where form_id = orders.id  and form_type = "order" order by id desc limit 1) as delivered_comments'))
                ->get();


        $setting = PrintPageSetting::where('user_type', '=', 'admin')->first();

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model;
        return view('front.invoice.bulk_print', compact('model', 'setting'));
    }

    public function invoiceProcess($id) {

        $data = Orders::where('id', $id)->first();
        Orders::where('id', $id)->update(['status' => 'processing']);
        Invoices::where('id', $data->invoice_refrence_id)->update(['status' => 'processing']);

        return redirect()->back();
    }

    private function ApprovedOrderAndSyncInvoice($order_id) {

        $invoice_total_price = 0;

        $order = Orders::where('id', $order_id)->first();
        $order_items = OrderItems::where('deleted', '0')
                ->where('order_id', $order->id)
                ->get();
        if (count($order_items) == 0) {

            Session::flash('error', 'Some error occured.');
            return redirect()->back();
        }


        $client = Clients::where('id', $order->client_id)->first();
        $total_price = 0;
        $total_cost = 0;
        $total_profit = 0;
        foreach ($order_items as $order_item) {

            $item_cost = self::getClientItemCost($this->customer_id, $order_item->item_id);

            OrderItems::where('id', $order_item->id)->update([
                'item_unit_price' => $order_item->item_unit_price,
                'total_price' => $order_item->total_price,
                'item_unit_cost' => $item_cost,
                'total_cost' => $item_cost * $order_item->quantity,
                'profit' => $order_item->total_price - ($item_cost * $order_item->quantity)
            ]);

            $total_price = $total_price + $order_item->total_price;
            $total_cost = $total_cost + ($item_cost * $order_item->quantity);
            $total_profit = $total_price - $total_cost;
        }

        $message = 'Order has been charged Successfully.';
        $status_type = 'approved_charged';

        $invoice = new Invoices;
        $invoice->created_by = 'customer';
        $invoice->customer_id = $this->customer_id;
        $invoice->order_id = $order->id;
        $invoice->memo = $order->memo;
        $invoice->list_id = $order->list_id;
        $invoice->total_price = $order->total_price;
        $invoice->total_quantity = $order->total_quantity;
        $invoice->statement_date = $order->statement_date;
        $invoice->deleted = '0';
        $invoice->save();

//        $new_id = '11' . $invoice->id;
//        Invoices::where('id', '=', $invoice->id)->update(['id' => $new_id]);
//        $invoice->id = $new_id;

        if (!isset($invoice->id)) {
            Session::flash('error', 'Some error occured.');
            return redirect()->back();
        }

        $invoice_id = $invoice->id;
        $customer = Customers::where('id', $this->customer_id)->first();

        foreach ($order_items as $item) {

            $my_item_id = $item->item_id;
            $new_item = Items::where('id', $item->item_id)->first();
            if ($new_item->parent_item_id != '0')
                $item->item_id = $new_item->parent_item_id;

            $selling_price = CommonController::getItemPrice($item->item_id, $this->customer_id, 'invoice');
            if ($selling_price == '0') {
                $item_exist = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('item_id', $item->item_id)->first();

                $selling_price = 0;
                if (count($item_exist) > 0)
                    $selling_price = $item_exist->selling_price;
            } else
                $selling_price = $selling_price['sale_price'][0]['item_unit_price'];

            $invoice_items = new InvoiceItems;
            $invoice_items->invoice_id = $invoice_id;
            $invoice_items->customer_id = $this->customer_id;
            $invoice_items->item_id = $my_item_id;
            $invoice_items->quantity = $item->quantity;
            $invoice_items->item_unit_price = $selling_price;
            $invoice_items->statement_date = $order->statement_date;
            $invoice_items->total_price = $invoice_items->item_unit_price * $invoice_items->quantity;
            $invoice_total_price = $invoice_total_price + $invoice_items->total_price;
            $invoice_items->save();
        }

        Invoices::where('id', $invoice_id)->update(['total_price' => $invoice_total_price]);
        $message = 'Order has been charged to client and send to Admin Successfully.';
        $status_type = 'approved';



        $order = Orders::where('id', $order->id)->first();
        if ($total_price == '0')
            $total_price = $order->total_price;

        PaymentController::updateClientBalance($order->client_id, $order->total_price, $order->id, 'order', $order->statement_date);
        Orders::where('id', $order->id)->update(['status' => $status_type, 'total_price' => $total_price, 'total_cost' => $total_cost, 'profit' => $total_profit, 'invoice_refrence_id' => $invoice_id]);
        $status = 'approved';

        if (InvoiceController::approvedsyncInvoice($invoice_id, $status)) {
            ////Calculate Profit after creation of their invoice
            CommonController::calculateSyncOrderProfit($order->id, $invoice_id);

            DB::commit();
            Session::flash('success', $message);
            return redirect('customer/order/create');
        } else {
            DB::rollBack();
            Session::flash('error', 'Something went wrong');
            return redirect()->back();
        }
    }

    public function zeroprofit() {

        $zero_profit_order_count = OrderItems::where('order_items.deleted', '0')
                ->where('order_items.profit', '=', 0.00)
                ->groupby('order_items.order_id')
                ->get();
        $ids = [];
        foreach ($zero_profit_order_count as $row) {
            $ids[] = $row->order_id;
        }

        $orders = Orders::where('orders.deleted', '=', '0')
                ->where('orders.created_by', $this->customer_id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id')
                ->whereIn('orders.id', $ids)
                ->where('orders.skip', 0)
                ->orderBy('orders.id', 'desc')
                ->select('orders.*', 'c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
                ->get();

        return view('front.customers.orders.zero_profit_list', compact('orders'));
    }

    public function orderSkip($id) {

        $check = Orders::where('id', '=', $id)->where('orders.created_by', $this->customer_id)->first();
        if (!empty($check)) {
            Orders::where('id', '=', $id)->update([
                'skip' => 1,
            ]);

            Session::flash('success', 'Successfully skiped.');
            return redirect()->back();
        } else {
            Session::flash('error', 'Order not found.');
            return redirect()->back();
        }
    }

    public function priceChange(){
        $clients = Clients::where('customer_id', '=', $this->customer_id)->where('deleted', '=', '0')->lists('name','id')->toArray();
        $items = Items::where('deleted',0)->lists('name','id')->toArray();
        
        return view('front.customers.orders.adddummyorder', compact('items','clients'));
    }

    public function priceChanged(Request $request){
        
        DB::beginTransaction();
        try{
            $activity = new ActivityLog;
            $activity->status = 'pending';
            $activity->type = 'customer';
            $activity->client_count = 1;
            $activity->save();
            $activity_id = $activity->id;
            if (isset($request->item_id) && $request->item_id > 0) {
                $item_id = $request->item_id;
            }else{
                Session::flash('error', 'Please Select an Item');
                return redirect()->back();
            }
            if (isset($request->diff) && $request->diff > 0) {
                $diff = $request->diff;
            }else{
                Session::flash('error', 'Please enter a valid difference');
                return redirect()->back();
            }
            if (!isset($request->client_id) || $request->client_id == 0) {
                Session::flash('error', 'Please select a client');
                return redirect()->back();
            }

            $client = Clients::where('id', $request->client_id)->first();
            $ordersCount = 0;
            $order_id = 0;
    
            $impact = '';
            if ($request->effect == 1) {
                $impact = 'positive';
            }elseif ($request->effect == 2) {
                $impact = 'negative';
            }
            $new_price = '';
            $check = CustomersItemImpact::where('item_id',$item_id)->where('customer_id',$client->customer_id)->first();
            
            if (!empty($check)) {
            
                $check1 = Items::where('id',$item_id)->first();
                    if($impact == 'positive'){
                        if($check->increase_impact == 1 && $client->increase_effect == 1 && $check1->increase_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $diff;
                                }else{
                                    $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                }
            
                            }
                        }
                    }
            
                    if($impact == 'negative'){
                        if($check->decrese_impact == 1 && $client->decrease_effect == 1 && $check1->decrease_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price - $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price - $diff;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$item_id)->first();
                                    $new_price = $old_item_price->selling_price - $diff;
                                }
                                        
                            }
                        }
                    }
                }else{
                    $check = Items::where('id',$item_id)->first();
                    
                    if($impact == 'positive'){
                        if($client->increase_effect == 1 && $check->increase_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price + $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price + $diff;
                                }else{
                                    $old_item_price = 0;//ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$p_i_item->item_id)->first();
                                    $new_price = 0; //$old_item_price->selling_price + $p_i_item->diff_amount;
                                }
                            }
                        }
                    }
                    if($impact == 'negative'){
                        if($client->decrease_effect == 1 && $check->decrease_effect == 1){
                            $type = 'order';
                            $old_item_price = CommonController::getItemPrice($item_id, $client->id, $type);
                            if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                                $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                                $new_price = $old_item_price - $diff;
                            }else{
                                $old_item_price = ItemPriceTemplates::where('price_template_id', $client->selling_price_template)->where('item_id',$item_id)->first();
                                if(!empty($old_item_price)){
                                    $new_price = $old_item_price->selling_price - $diff;
                                }else{
                                    $old_item_price = ItemPriceTemplates::where('price_template_id', '1')->where('item_id',$item_id)->first();
                                    $new_price = $old_item_price->selling_price - $diff;
                                }
                            }
                        }
                    }
                }

        if($new_price != ''){
            if ($order_id == 0) {
                $ordersCount++;
            }
            $order_ret_id = CommonController::createOrder($item_id,$order_id,$client->id,$new_price,$activity_id);
            $order_id = 0;
        }
        ActivityLog::where('id',$activity_id)->update(['orders_count' => $ordersCount, 'customer_id' => $this->customer_id, 'status' => 'completed','client_id' => $request->client_id]);
        DB::commit();
        Session::flash('success', 'Successfully Price Changed.');
        return redirect()->back();
        }catch(Exception $e) {
            DB::rollBack();
            Session::flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    public function myActivity(){
        $modal = ActivityLog::where('activity_log.deleted',0)->where('activity_log.customer_id',$this->customer_id)
        ->leftjoin('clients','clients.id','=','activity_log.client_id')
        ->leftjoin('orders','orders.activity_log_id','=','activity_log.id')
        ->leftjoin('order_items','order_items.order_id','=','orders.id')
        ->leftjoin('items','items.id','=','order_items.item_id')
        ->select('activity_log.*','clients.name','items.name as item_name','order_items.total_price as item_price')->orderBy('id', 'desc')->get();

        return view('front.customers.orders.activityLog', compact('modal'));
    }

}
