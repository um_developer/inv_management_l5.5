<?php

namespace App\Http\Controllers;

use DB;
use App\Sizes;
use Illuminate\Http\Request;
use App\Functions\Functions;
use App\DirectPaymentSources;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;


class PaymentTypesController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      |  ControllerPaymentTypesController
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
         parent::__construct();
         if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
    }

    public function index() {

       $payment_types = DirectPaymentSources::where('deleted','0')->where('owner_type','admin')->get();
        return view('front.payment_type.index', compact('payment_types'));
    }

    public function create() {

        return view('front.payment_type.create');
    }
    
    
    public function edit($id) {
        
        $payment_type = DirectPaymentSources::where('id', '=', $id)->where('deleted','0')->get();
        
        if(count($payment_type)==0){
            return redirect('/');
        }
        $model=$payment_type[0];
        return view('front.payment_type.edit', compact('model'));
    }
    
    
    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'title' => 'required|max:40',
//            'code' => 'required|max:40',
            
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }
        
        $size = new DirectPaymentSources;
        $size->title = $request->title;
         $size->for_customer = $request->for_customer;
         $size->for_admin = $request->for_admin;
        $size->created_by = $user_id;
        $size->save();
        
        if(isset($size->id)){
        Session::flash('success', 'Payment Type is created successfully');
        return redirect()->back();
        } else{
              Session::flash('error', 'Payment Type is not created. Please try again.');
             return redirect()->back();
            
        }
    }
    
     public function postUpdate(Request $request) {

        $validation = array(
            'title' => 'required|max:40',
//            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'vendor_create');
        }

        $size_id = $request->id;
        $input = $request->all();
            array_forget($input, '_token');
            array_forget($input, 'id');
             array_forget($input, 'submit');
             
              if (!isset($input['for_admin']))
            $input['for_admin'] = 0;
              
               if (!isset($input['for_customer']))
            $input['for_customer'] = 0;
          
            DirectPaymentSources::where('id', '=', $size_id)->update($input);
            Session::flash('success', 'Payment Type has been updated.');
            return redirect()->back();
        
       
    }
    
        public function delete($id) {
        DirectPaymentSources::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
