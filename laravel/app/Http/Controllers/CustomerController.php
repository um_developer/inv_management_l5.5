<?php

namespace App\Http\Controllers;

use DB;
use App\Customers;
use App\users;
use App\PriceTemplates;
use App\States;
use App\Payments;
use App\DirectPaymentSources;
use App\AccountTypes;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\Invoices;
use App\CreditMemo;
use App\User;
use Session;
use Carbon\Carbon;
use App\PrintPageSetting;
use Validator,
    Input,
    Redirect;
use Illuminate\Contracts\Auth\Guard;

class CustomerController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Customer Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
        parent::__construct();
    }

    public function index(Request $request) {
         $status = $request->get('status');
         $pricestatus = $request->get('pricestatus');
         if($status=="" OR $status==1){
        $model = Customers::where(['customers.deleted'=>0 ,'customers.status'=>1]);
         }elseif($status==0){
            $model = Customers::where(['customers.deleted'=>0 ,'customers.status'=>0]);
             }
           if(isset($request->pricestatus) && $request->pricestatus=="increase"){
                $model = $model->where('customers.increase_effect', 1);
            }
            if(isset($request->pricestatus) && $request->pricestatus=="decrease"){
                $model = $model->where('customers.decrease_effect', 1);
            }  
         $model = $model->leftjoin('states as s', 's.code', '=', 'customers.state')
                ->leftjoin('price_templates as pt', 'pt.id', '=', 'customers.selling_price_template')
                ->orderBy('customers.id', 'desc')
                ->select('customers.*', 's.title as state', 'pt.title as template_name')
                ->get();
                $active = Customers::where(['customers.deleted'=>0 ,'customers.status'=>1])
                ->leftjoin('states as s', 's.code', '=', 'customers.state')
                ->leftjoin('price_templates as pt', 'pt.id', '=', 'customers.selling_price_template')
                ->orderBy('customers.id', 'desc')
                ->select('customers.*', 's.title as state', 'pt.title as template_name')
                ->count();
               
                $inactive = Customers::where(['customers.deleted'=>0,'customers.status'=>0])
                ->leftjoin('states as s', 's.code', '=', 'customers.state')
                ->leftjoin('price_templates as pt', 'pt.id', '=', 'customers.selling_price_template')
                ->orderBy('customers.id', 'desc')
                ->select('customers.*', 's.title as state', 'pt.title as template_name')
                ->count();


        return view('front.customer.index', compact('model','active','inactive','pricestatus'));
    }

    public function create() {

        $states = States::select(['title', 'code'])->get();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;

        $templates = PriceTemplates::where('deleted', '=', '0')->where('user_type', 'admin')
                        ->pluck('title', 'id')->toArray();
        return view('front.customer.create', compact('states', 'templates'));
    }

    public function edit($id) {

        $model = Customers::where('u_id', '=', $id)->where('deleted', '=', '0')->get();
        $states = States::select(['title', 'code'])->get();
        $templates = PriceTemplates::where('deleted', '=', '0')->where('user_type', 'admin')
                        ->pluck('title', 'id')->toArray();
        $new_states = [];
        foreach ($states as $state) {
            $new_states[$state->code] = $state->title;
        }
        $states = $new_states;

        if (count($model) == 0) {
            return redirect('/');
        }
        $model = $model[0];
        return view('front.customer.edit', compact('model', 'states', 'templates'));
    }

    public function detail(Request $request, $customer_id) {

        if (isset($request['filter']) && isset($request['date_range']) && !empty($request['date_range'])) {

            $date = explode("-", $request['date_range']);
            $dateS = new Carbon($date[0]);
            $dateE = new Carbon($date[1]);
            if (isset($request['type'])) {
                $selected_report_type = $request['type'];
            }
            if ($request['tab'] == 'invoices') {
                $start_date = date("Y-m-d", strtotime($date[0]));
                $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            } else {
                $start_date = Carbon::today()->subDay(7)->toDateString();
                $end_date = Carbon::today()->addDay(1)->toDateString();
            }

            $model = Invoices::where('invoices.deleted', '=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                    ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                    ->whereBetween('invoices.statement_date', [$start_date, $end_date]);
// ->whereBetween('statement_date', [$dateS->format('Y-m-d')." 00:00:00", $dateE->format('Y-m-d')." 23:59:59"]);
            if (!empty($selected_report_type && $request['tab'] == 'invoices')) {

                $model = $model->where('invoices.status', $selected_report_type);
            }
            if (!empty($customer_id)) {

                $model = $model->where('invoices.customer_id', $customer_id);
            }

            $model = $model->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                    ->orderBy('id', 'desc')
                    ->get();
            $invoices0 = $model;

            if ($request['tab'] == 'credit_memo') {

                $start_date = date("Y-m-d", strtotime($date[0]));
                $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            } else {

                $start_date = Carbon::today()->subDay(7)->toDateString();
                $end_date = Carbon::today()->addDay(1)->toDateString();
            }

            $credit_memo0 = CreditMemo::where('credit_memo.deleted', '=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                    ->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);

            if (isset($request['type']) && $request['tab'] == 'credit_memo' && $request['type'] != 'delivered' && $request['type'] != '') {

                $credit_memo0 = $credit_memo0->where('credit_memo.status', $selected_report_type);
            }
            if (!empty($customer_id)) {

                $credit_memo0 = $credit_memo0->where('credit_memo.customer_id', $customer_id);
            }
            $credit_memo0 = $credit_memo0->select('credit_memo.*', 'c.name as customer_name', 'c.u_id as customer_u_id')
                    ->orderBy('id', 'desc')
                    ->get();

            if ($request['tab'] == 'payments') {
                $start_date = date("Y-m-d", strtotime($date[0]));
                $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            } else {
                $start_date = Carbon::today()->subDay(7)->toDateString();
                $end_date = Carbon::today()->addDay(1)->toDateString();
            }
            $payment_source_id = $request->payment_source_id;
            $account_id = $request->account_id;
            $payments0 = Payments::where('payments.deleted', '=', '0')->where('user_type', 'customer')->where('payment_type', 'direct')->where('modification_count', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                    ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                    ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                    ->whereBetween('payments.statement_date', [$start_date, $end_date]);

            if ($customer_id != '0') {
                $payments0 = $payments0->where('payments.user_type', 'customer')->where('payments.user_id', $customer_id);
            }
            if ($payment_source_id != '0') {
                $payments0 = $payments0->where('payments.payment_source_id', $payment_source_id);
            }
            if ($account_id != '0') {
                $payments0 = $payments0->where('payments.account_type_id', $account_id);
            }

            if (isset($request['type']) && $request['tab'] == 'payments' && $request['type'] != 'delivered' && $request['type'] != '') {
                $payments0 = $payments0->where('payments.status', $request['type']);
            }

            $payments0 = $payments0->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'c.name as user_name', 'c.id as user_id')
                    ->orderBy('payments.created_at', 'desc')
                    ->get();



            if ($request['tab'] == 'settings') {
                $start_date = date("Y-m-d", strtotime($date[0]));
                $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            } else {
                $start_date = Carbon::today()->subDay(7)->toDateString();
                $end_date = Carbon::today()->addDay(1)->toDateString();
            }

            $invoices = Invoices::where('invoices.deleted', '0')->where('invoices.status', '!=', 'pending')->where('invoices.customer_id', '!=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id');

            $invoices = $invoices->whereBetween('invoices.statement_date', [$start_date, $end_date]);

            $invoices = $invoices->where('invoices.customer_id', $customer_id);


            $invoices = $invoices->select(DB::raw("SUM(invoices.total_price) as sale_amount,SUM(invoices.profit) as profit_amount, invoices.customer_id, c.name as customer_name"))
                            ->groupby('invoices.customer_id')->get();

            $credit_memo = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id');

            $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);

            if ($customer_id != 0)
                $credit_memo = $credit_memo->where('credit_memo.customer_id', $customer_id);

            $credit_memo = $credit_memo->select(DB::raw("SUM(credit_memo.total_price) as sale_amount,SUM(credit_memo.loss) as loss_amount, credit_memo.customer_id, c.name as customer_name"))
                            ->groupby('credit_memo.customer_id')->get();

            $new_credit_memo = [];
            $new_invoice = [];
            $model = [];

            foreach ($credit_memo as $memo) {
                $new_credit_memo[$memo->customer_id] = $memo;
            }

            foreach ($invoices as $memo) {
                $new_invoice[$memo->customer_id] = $memo;
            }

            $i = 0;
            if (count($invoices) >= count($credit_memo)) {
                foreach ($invoices as $invoice) {
                    if (isset($new_credit_memo[$invoice->customer_id])) {
                        $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->customer_id]['loss_amount'];
                        $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->customer_id]['sale_amount'];
                    }

                    $model[$i] = $invoice;
                    $i++;
                }
            } else {

                foreach ($credit_memo as $invoice) {

                    if (isset($new_invoice[$invoice->customer_id])) {

                        $invoice['sale_amount'] = $new_invoice[$invoice->customer_id]['sale_amount'];
                        $invoice['profit_amount'] = $new_invoice[$invoice->customer_id]['profit_amount'];

                        $invoice['credit_memo_loss'] = $invoice->loss_amount;
                        $invoice['credit_memo_sale'] = $invoice->sale_amount;
                    } else {

                        $invoice['credit_memo_loss'] = $invoice->loss_amount;
                        $invoice['credit_memo_sale'] = $invoice->sale_amount;

                        $invoice['sale_amount'] = '0';
                        $invoice['profit_amount'] = '0';
                    }

                    $model[$i] = $invoice;
                    $i++;
                }
            }

            if ($request['tab'] == 'transactions') {
                $start_date = date("Y-m-d", strtotime($date[0]));
                $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            } else {
                $start_date = Carbon::today()->subDay(7)->toDateString();
                $end_date = Carbon::today()->addDay(1)->toDateString();
            }

            $transactions = Payments::where('payments.status', '!=', 'pending')->where('payments.deleted', '=', '0')
                    ->whereBetween('payments.statement_date', [$start_date, $end_date])
                    ->where('payments.user_type', 'customer')
                    ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id');

            if (isset($request['report_type_id'])) {
                if ($request['report_type_id'] == '2') {
                    $transactions = $transactions->where('payment_type', 'direct');
                } else if ($request['report_type_id'] == '3') {
                    $transactions = $transactions->where('payment_type', 'invoice');
                }
            }


            $transactions = $transactions->where('payments.user_id', $customer_id)->select('payments.*', 'c.name as customer_name')->orderBy('payments.created_at', 'desc')->get();

            $customer = Customers::where('customers.id', $customer_id)->where('customers.deleted', '=', '0')->leftjoin('states as s', 's.code', '=', 'customers.state')
                    ->leftjoin('price_templates as pt', 'pt.id', '=', 'customers.selling_price_template')
                    ->orderBy('customers.id', 'desc')
                    ->select('customers.*', 's.title as state', 'pt.title as template_name')
                    ->get();
            $cus = $customer;
            $customers = Customers::where('deleted', '0')->get();
            $all_packages[0] = 'Select Customer';
            foreach ($customers as $item) {
                $all_packages[$item->id] = $item->name . ' (BAL: $' . $item->balance . ')';
            }


            // sksk
            if ($request['tab'] == 'statement') {
                $start_date = date("Y-m-d", strtotime($date[0]));
                $end_date = date("Y-m-d", strtotime($date[1] . " +1 days"));
            } else {
                $start_date = Carbon::today()->subDay(7)->toDateString();
                $end_date = Carbon::today()->addDay(1)->toDateString();
            }
            $customer = Customers::where('id', $customer_id)->first();


            $payments = $payments = Payments::where('payments.status', '!=', 'pending')->where('payments.modification_count', '=', '0')->where('payments.deleted', '=', '0')
                            ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                            ->whereBetween('payments.statement_date', [$start_date, $end_date])->where('user_type', 'customer');

            if ($customer_id != '0') {
                $payments = $payments->where('payments.user_id', $customer_id);
            }
            $payments = $payments->select('payments.*', 'c.name as customer_name')
                            ->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->get();

            $new_model = [];
            $final_array = [];
            if (count($payments) > 0) {
                $new_model = $payments;

                $i = 1;
                $previous_palance = 0;
                $updated_balance = 0;
                $new_array = [];
                $final_balance = 0;
                foreach ($new_model as $item) {

                    if ($i == '1') {
                        if ($item->payment_type == 'direct') {

                            if ($item->transaction_type == 'negative')
                                $updated_balance = str_replace("-", "", $item->total_modified_amount);
                            else
                                $updated_balance = '-' . $item->total_modified_amount;
                        } else
                            $updated_balance = $item->total_modified_amount;
                    } else {
                        $previous_palance = $updated_balance;
                        if ($item->payment_type == 'direct') {

                            if ($item->transaction_type == 'negative')
                                $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);
                            else
                                $updated_balance = $updated_balance - $item->total_modified_amount;
                        } else
                            $updated_balance = $updated_balance + $item->total_modified_amount;
                    }

                    $final_balance = $updated_balance;
                    $new_array[] = $item;

                    $i++;
                }

//            print_r($final_balance); die;
                $previous_palance = 0;
                $updated_balance = $final_balance;
//            dd($new_array);
                $i = 1;
                foreach ($new_array as $item) {

                    if ($item->transaction_type == 'negative' && $item->payment_type == 'direct')
                        $item->total_modified_amount = $item->total_modified_amount;
                    else
                        $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);


                    if ($i == '1') {
                        $updated_balance = $final_balance;

                        if ($item->payment_type == 'invoice')
                            $previous_palance = $updated_balance - $item->total_modified_amount;
                        else
                            $previous_palance = $updated_balance + $item->total_modified_amount;;
                    } else {
                        $updated_balance = $previous_palance;
                        if ($item->payment_type == 'invoice')
                            $previous_palance = $updated_balance - $item->total_modified_amount;
                        else
                            $previous_palance = $updated_balance + $item->total_modified_amount;
                    }

                    $item->new_previous_balance = round($previous_palance, 2);
                    $item->new_updated_balance = round($updated_balance, 2);
                    if ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'credit_memo')
                        $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);
                    else
                        $item->new_total_modified_amount = $item->total_modified_amount;


                    $final_balance = $updated_balance;
                    $final_array[] = $item;
                    $i++;
                }
            }


            $statement = $final_array;


            // jnxknkxj
            $customers = $all_packages;
            $type[1] = 'Complete Transaction';
            $type[2] = 'Payments History';
            $type[3] = 'Invoice Payments';
            $report_type = $type;
            $selected_report_type = $request['type'];
        }else {

            // $invoices0 = Invoices::where('customer_id',$customer_id)->where('deleted',0)->get();
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();

            $invoices0 = Invoices::where('invoices.customer_id', $customer_id)->where('invoices.deleted', '=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                    ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                    ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                    ->whereBetween('invoices.statement_date', [$start_date, $end_date])
                    ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                    ->orderBy('id', 'desc')
                    ->get();

            $credit_memo0 = CreditMemo::where('customer_id', $customer_id)->where('deleted', 0)->whereBetween('statement_date', [$start_date, $end_date])->get();
            $payments0 = Payments::where('payments.deleted', '=', '0')->where('user_type', 'customer')->where('payment_type', 'direct')->where('modification_count', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                    ->leftjoin('direct_payment_sources as dps', 'dps.id', '=', 'payments.payment_source_id')
                    ->leftjoin('account_types as at', 'at.id', '=', 'payments.account_type_id')
                    ->whereBetween('payments.statement_date', [$start_date, $end_date]);

            if ($customer_id != '0') {
                $payments0 = $payments0->where('payments.user_type', 'customer')->where('payments.user_id', $customer_id);
            }
            $payments0 = $payments0->select('payments.*', 'dps.title as payment_type_title', 'at.title as account_type_title', 'c.name as user_name', 'c.id as user_id')
                    ->orderBy('payments.created_at', 'desc')
                    ->get();
            // $payments0 = Payments::where('user_type','customer')->where('user_id',$customer_id)->where('status','approved')->where('payment_type','direct')->where('modification_count','0')
            // ->whereBetween('statement_date', [$start_date, $end_date])->get();



            $invoices = Invoices::where('invoices.deleted', '0')->where('invoices.status', '!=', 'pending')->where('invoices.customer_id', '!=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id');

            $invoices = $invoices->whereBetween('invoices.statement_date', [$start_date, $end_date]);

            if ($customer_id != 0)
                $invoices = $invoices->where('invoices.customer_id', $customer_id);

            $invoices = $invoices->select(DB::raw("SUM(invoices.total_price) as sale_amount,SUM(invoices.profit) as profit_amount, invoices.customer_id, c.name as customer_name"))
                            ->groupby('invoices.customer_id')->get();


            $credit_memo = CreditMemo::where('credit_memo.deleted', '=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                    ->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);
            if (!empty($customer_id)) {
                $credit_memo = $credit_memo->where('credit_memo.customer_id', $customer_id);
            }
            $credit_memo = $credit_memo->select('credit_memo.*', 'c.name as customer_name', 'c.u_id as customer_u_id')
                    ->orderBy('id', 'desc')
                    ->get();


            $new_credit_memo = [];
            $new_invoice = [];
            $model = [];

            foreach ($credit_memo as $memo) {
                $new_credit_memo[$memo->customer_id] = $memo;
            }

            foreach ($invoices as $memo) {
                $new_invoice[$memo->customer_id] = $memo;
            }

            $i = 0;
            if (count($invoices) >= count($credit_memo)) {
                foreach ($invoices as $invoice) {
                    if (isset($new_credit_memo[$invoice->customer_id])) {
                        $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->customer_id]['loss_amount'];
                        $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->customer_id]['sale_amount'];
                    }

                    $model[$i] = $invoice;
                    $i++;
                }
            } else {

                foreach ($credit_memo as $invoice) {

                    if (isset($new_invoice[$invoice->customer_id])) {

                        $invoice['sale_amount'] = $new_invoice[$invoice->customer_id]['sale_amount'];
                        $invoice['profit_amount'] = $new_invoice[$invoice->customer_id]['profit_amount'];

                        $invoice['credit_memo_loss'] = $invoice->loss_amount;
                        $invoice['credit_memo_sale'] = $invoice->sale_amount;
                    } else {

                        $invoice['credit_memo_loss'] = $invoice->loss_amount;
                        $invoice['credit_memo_sale'] = $invoice->sale_amount;

                        $invoice['sale_amount'] = '0';
                        $invoice['profit_amount'] = '0';
                    }

                    $model[$i] = $invoice;
                    $i++;
                }
            }


            // $customer = Customers::where('id', $customer_id)->first();

            $transactions = Payments::where('payments.status', '!=', 'pending')->where('payments.deleted', '=', '0')
                    ->whereBetween('payments.statement_date', [$start_date, $end_date])
                    ->where('payments.user_type', 'customer')
                    ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id');

            if ($customer_id != '0')
                $transactions = $transactions->where('payments.user_id', $customer_id);

            $transactions = $transactions->select('payments.*', 'c.name as customer_name')->orderBy('payments.created_at', 'desc')->get();


            $customer = Customers::where('customers.id', $customer_id)->where('customers.deleted', '=', '0')->leftjoin('states as s', 's.code', '=', 'customers.state')
                    ->leftjoin('price_templates as pt', 'pt.id', '=', 'customers.selling_price_template')
                    ->orderBy('customers.id', 'desc')
                    ->select('customers.*', 's.title as state', 'pt.title as template_name')
                    ->get();

            $cus = $customer;

            $customers = Customers::where('deleted', '0')->get();
            $all_packages[0] = 'Select Customer';
            foreach ($customers as $item) {
                $all_packages[$item->id] = $item->name . ' (BAL: $' . $item->balance . ')';
            }


// sksk

            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
            $customer = Customers::where('id', $customer_id)->first();


            $payments = $payments = Payments::where('payments.status', '!=', 'pending')->where('payments.modification_count', '=', '0')->where('payments.deleted', '=', '0')
                            ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                            ->whereBetween('payments.statement_date', [$start_date, $end_date])->where('user_type', 'customer');

            if ($customer_id != '0')
                $payments = $payments->where('payments.user_id', $customer_id);

            $payments = $payments->select('payments.*', 'c.name as customer_name')
                            ->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->get();

            $new_model = [];
            $final_array = [];
            if (count($payments) > 0) {
                $new_model = $payments;

                $i = 1;
                $previous_palance = 0;
                $updated_balance = 0;
                $new_array = [];
                $final_balance = 0;
                foreach ($new_model as $item) {

                    if ($i == '1') {
                        if ($item->payment_type == 'direct') {

                            if ($item->transaction_type == 'negative')
                                $updated_balance = str_replace("-", "", $item->total_modified_amount);
                            else
                                $updated_balance = '-' . $item->total_modified_amount;
                        } else
                            $updated_balance = $item->total_modified_amount;
                    } else {
                        $previous_palance = $updated_balance;
                        if ($item->payment_type == 'direct') {

                            if ($item->transaction_type == 'negative')
                                $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);
                            else
                                $updated_balance = $updated_balance - $item->total_modified_amount;
                        } else
                            $updated_balance = $updated_balance + $item->total_modified_amount;
                    }

                    $final_balance = $updated_balance;
                    $new_array[] = $item;

                    $i++;
                }

//            print_r($final_balance); die;
                $previous_palance = 0;
                $updated_balance = $final_balance;
//            dd($new_array);
                $i = 1;
                foreach ($new_array as $item) {

                    if ($item->transaction_type == 'negative' && $item->payment_type == 'direct')
                        $item->total_modified_amount = $item->total_modified_amount;
                    else
                        $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);


                    if ($i == '1') {
                        $updated_balance = $final_balance;

                        if ($item->payment_type == 'invoice')
                            $previous_palance = $updated_balance - $item->total_modified_amount;
                        else
                            $previous_palance = $updated_balance + $item->total_modified_amount;;
                    } else {
                        $updated_balance = $previous_palance;
                        if ($item->payment_type == 'invoice')
                            $previous_palance = $updated_balance - $item->total_modified_amount;
                        else
                            $previous_palance = $updated_balance + $item->total_modified_amount;
                    }

                    $item->new_previous_balance = round($previous_palance, 2);
                    $item->new_updated_balance = round($updated_balance, 2);
                    if ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'credit_memo')
                        $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                    elseif ($item->payment_type == 'direct' && $item->transaction_type == 'negative')
                        $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);
                    else
                        $item->new_total_modified_amount = $item->total_modified_amount;


                    $final_balance = $updated_balance;
                    $final_array[] = $item;
                    $i++;
                }
            }


            $statement = $final_array;


            // jnxknkxj
            $customers = $all_packages;
            $type[1] = 'Complete Transaction';
            $type[2] = 'Payments History';
            $type[3] = 'Invoice Payments';
            $report_type = $type;
            $selected_report_type = $request['type'];
            // end if
        }
        $payment_sources = DirectPaymentSources::where('deleted', '0')->where('for_admin', '1')->get();
        $all_payment_sources[0] = 'Select Payment Type';
        foreach ($payment_sources as $item) {
            $all_payment_sources[$item->id] = $item->title;
        }
        $payment_sources = $all_payment_sources;

        $account_sources = AccountTypes::where('deleted', '0')->where('for_admin', '1')->get();
        $all_account_sources[0] = 'Select Account Type';
        foreach ($account_sources as $item) {
            $all_account_sources[$item->id] = $item->title;
        }
        $accounts = $all_account_sources;

        $date['accounts'] = $accounts;
        $date['payment_sources'] = $payment_sources;


        $date['statement'] = $statement;
        $date['customers'] = $customers;
        $date['report_type'] = $report_type;
        $cu[] = $cus;

        $date['customer'] = $cu;
        $date['transactions'] = $transactions;
        $date['invoices0'] = $invoices0;
        $date['credit_memos0'] = $credit_memo0;
        $date['payments0'] = $payments0;
        $date['model'] = $model;
        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $date = $start_date . ' - ' . $end_date;
        $activeTab = 'info';

        if ($request['filter'] == '1') {
            $date_range = $request['date_range'];
            $filter = $request['filter'];
            $data['filter'] = $filter;
            $data['date_range'] = $date_range;
            $activeTab = $request['tab'];
            $data['activeTab'] = $activeTab;
            return view('front.customer.detail', compact('invoices0', 'credit_memo0', 'payments0', 'filter', 'activeTab', 'date_range', 'model', 'transactions', 'customer', 'report_type', 'customers', 'selected_report_type', 'statement', 'payment_sources', 'accounts'));
        }
        // $selected_report_type = '';

        return view('front.customer.detail', compact('invoices0', 'credit_memo0', 'payments0', 'model', 'transactions', 'customer', 'report_type', 'customers', 'selected_report_type', 'date', 'statement', 'activeTab', 'payment_sources', 'accounts'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'email' => 'email|max:80',
            'username' => 'max:50|min:3|unique:customers,username',
            'password' => 'min:6|max:30',
            'phone' => 'max:16',
            'company' => 'max:250',
            'email_2' => 'email|max:50',
            'phone_2' => 'max:16',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        DB::beginTransaction();
        $customer = new Customers;
        $customer->name = $request->name;
        $customer->username = $request->username;
        $customer->password = $request->password;
        $customer->email = $request->email;
        $customer->email_2 = $request->email_2;
        $customer->selling_price_template = $request->selling_price_template;
        $customer->phone = $request->phone;
        $customer->phone_2 = $request->phone_2;
        $customer->address1 = $request->address1;
        $customer->address2 = $request->address2;
        $customer->state = $request->state;
        $customer->city = $request->city;
        $customer->zip_code = $request->zip_code;
        $customer->company = $request->company;
        $customer->gallery_invoice_btn = $request->gallery_invoice_btn;
        $customer->gallery_order_btn = $request->gallery_order_btn;
        $customer->client_menu = $request->client_menu;

        $customer->show_unit_price = $request->show_unit_price;
        $customer->show_image = $request->show_image;
        $customer->show_serial = $request->show_serial;
        $customer->show_package_id = $request->show_package_id;
        if (!empty($request->show_bundle)){
        $customer->show_bundle =$request->show_bundle;
          }else{
            $customer->show_bundle =0;
        }
        if (!empty($request->is_order_sync))
            $customer->is_order_sync = $request->is_order_sync;
        else
            $customer->is_order_sync = 0;

        if (!empty($request->increase_effect)){
            $customer->increase_effect = $request->increase_effect;
        }else{
            $customer->increase_effect = 0;
        }
        if (!empty($request->decrease_effect)){
            $customer->decrease_effect = $request->decrease_effect;
        }else{
            $customer->decrease_effect = 0;
        }
        $customer->created_by = $user_id;
        $customer->note = $request->note;
        $customer->status = $request->status;
        $customer->save();

        $user = new User;
        $user->email = $request->username;
        if ($request->password != '')
            $user->password = bcrypt($request->password);
        $user->role_id = '4';
        $user->save();

        if (!isset($user->id)) {
            DB::rollBack();
            Session::flash('error', 'Customer is not created. Please try again.');
            return redirect()->back();
        }

        if (isset($customer->id)) {
            $random_string = Functions::generateRandomString(6);
            $u_id = $random_string . $customer->id;
            Customers::where('id', '=', $customer->id)->update(['u_id' => $u_id, 'user_id' => $user->id]);

            CustomerPaymentTypesController::createCustomerPaymentTypes($customer->id);
            CustomerAccountTypesController::createClientAccountTypes($customer->id);

            DB::commit();
            $new_template_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($customer->id);

            Session::flash('success', 'Customer is created successfully');
            return redirect()->back();
        } else {
            DB::rollBack();
            Session::flash('error', 'Customer is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $customer_id = $request->id;
        $validation = array(
            'name' => 'required|max:100',
            'address1' => 'max:400',
            'address2' => 'max:400',
            'zip_code' => 'max:20',
            'city' => 'max:30',
            'email' => 'email|max:80',
            'phone' => 'max:16',
            'u_id' => 'required',
            'username' => 'max:50|min:3|unique:customers,username,' . $customer_id,
            'password' => 'min:6|max:30',
            'company' => 'max:250',
            'email_2' => 'email|max:50',
            'phone_2' => 'max:16',
//             'phone' => 'regex:/(01)[0-9]{9}/|max:16',
        );


        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

        $customer_id = $request->u_id;
        $input = $request->all();

        if (!isset($input['gallery_invoice_btn']))
            $input['gallery_invoice_btn'] = 0;

        if (!isset($input['gallery_order_btn']))
            $input['gallery_order_btn'] = 0;

        if (!isset($input['client_menu']))
            $input['client_menu'] = 0;

        if (!isset($input['show_unit_price']))
            $input['show_unit_price'] = 0;

        if (!isset($input['show_image']))
            $input['show_image'] = 0;

        if (!isset($input['show_serial']))
            $input['show_serial'] = 0;

        if (!isset($input['show_package_id']))
            $input['show_package_id'] = 0;

        if (!isset($input['is_order_sync']))
            $input['is_order_sync'] = 0;

        if (!isset($input['increase_effect'])){
            $input['increase_effect'] = 0;
        }
        if (!isset($input['decrease_effect'])){
            $input['decrease_effect'] = 0;
        }
        if (!isset($input['show_bundle'])){
            $input['show_bundle'] = 0;
        }
       


        array_forget($input, '_token');
        array_forget($input, 'u_id');
        array_forget($input, 'submit');

        Customers::where('u_id', '=', $customer_id)->update($input);

        $customer = Customers::where('u_id', '=', $customer_id)->get();
        $password = '';
        if ($customer[0]->user_id != '0') {

            if ($input['password'] != '')
                $password = bcrypt($input['password']);

            User::where('id', '=', $customer[0]->user_id)->where('role_id', '4')->update(['email' => $input['username'], 'password' => $password]);
        } else {
            $user = new User;
            $user->email = $customer[0]->username;
            $password = '';
            if ($customer[0]->password != '')
                $user->password = bcrypt($customer[0]->password);
            $user->role_id = '4';
            $user->save();

            Customers::where('u_id', '=', $customer_id)->update(['user_id' => $user->id]);
        }
        Session::flash('success', 'Customer has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        $customer = Customers::where('u_id', '=', $id)->first();
        $payments = Payments::where('user_type', 'customer')->where('user_id', $customer->id)->get();

        if (count($payments) > 0) {
            Session::flash('error', 'You cannot delete this customer because this customer has some transactions.');
            return redirect()->back();
        }
        if ($customer->user_id != '0') {
//            App\User::where('id', '=', $customer->user_id)->delete();
            User::where('id', '=', $customer->user_id)->update(['deleted' => 1]);

        }

        Customers::where('u_id', '=', $id)->update(['deleted' => 1]);
        //Customers::where('u_id', '=', $id)->delete();
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function customLogin(Request $request) {

        $this->auth->logout();
        SignupController::customLogin($request->user_id, $request);
        return redirect()->back();
    }

    public function printPageSetting() {

        $model = PrintPageSetting::where('user_type', '=', 'admin')->first();
        $user_id = Auth::user()->id;

        if (count($model) < 1) {
            $printPageSetting = new PrintPageSetting;
            $printPageSetting->bottom_text_1 = 'FOR ACCESSORIES VISIT BROADPOSTERS.COM';
            $printPageSetting->bottom_text_2 = 'NOTICE: OPEN ITEMS ARE NON REFUNDABLE. THANK YOU';
            $printPageSetting->user_type = 'admin';
            $printPageSetting->user_id = $user_id;
            $printPageSetting->save();
        }

        $model = PrintPageSetting::where('user_type', '=', 'admin')->first();
        return view('front.customer.print_page_setting', compact('model'));
    }

    public function updatePrintPageSetting(Request $request) {

        $input = $request->all();

        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        PrintPageSetting::where('id', '=', $request->id)->update($input);
        Session::flash('success', 'Updated seccessfully.');
        return redirect()->back();
    }

}
