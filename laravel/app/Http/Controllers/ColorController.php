<?php

namespace App\Http\Controllers;

use DB;
use App\Colors;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;


class ColorController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Categories Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
         parent::__construct();
         if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
    }

    public function index() {

       $colors = Colors::where('deleted','0')->get();
        return view('front.colors.index', compact('colors'));
    }

    public function create() {

        return view('front.colors.create');
    }
    
    
    public function edit($id) {
        
        $color = Colors::where('id', '=', $id)->where('deleted','0')->get();
        
        if(count($color)==0){
            return redirect('/');
        }
        $model=$color[0];
        return view('front.colors.edit', compact('model'));
    }
    
    
    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;
        $validation = array(
            'title' => 'required|max:40|unique:colors',
            'code' => 'required|max:40',
            
            
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        
        $color = new Colors;
        $color->title = $request->title;
         $color->code = $request->code;
        $color->created_by = $user_id;
        $color->save();
        
        if(isset($color->id)){
        Session::flash('success', 'Color is created successfully');
        return redirect()->back();
        } else{
              Session::flash('error', 'Color is not created. Please try again.');
             return redirect()->back();
            
        }
    }
    
     public function postUpdate(Request $request) {

          $color_id = $request->id;
        $validation = array(
            'title' => 'required|max:40|unique:colors,title,' . $color_id,
            'code' => 'required|max:40',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }

       
        $input = $request->all();
            array_forget($input, '_token');
            array_forget($input, 'id');
             array_forget($input, 'submit');
          
            Colors::where('id', '=', $color_id)->update($input);
            Session::flash('success', 'Color has been updated.');
            return redirect()->back();
        
       
    }
    
        public function delete($id) {
        Colors::where('id', '=', $id)->update(['deleted' => 1]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
