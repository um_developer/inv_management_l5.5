<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Post;
use Auth;
use App\Repositories\PostRepository;

class PostController extends Controller {

    protected $repo;

    public function __construct(Post $post) {
        $this->repo = new PostRepository($post);
    }

    public function index() {
        
        $data['posts'] = $this->repo->paginate(5);
        return view('front.home',$data);
    }

    public function create() {
        return view('front/post/create');
    }

    public function update(Request $request, $id) {
        $rules = [
            'title' => 'min:2|max:20',
            'description' => 'min:2'
        ];
        $msg = [
            'title.min' => 'Title must be at least 2 characters long',
            'title.max' => 'Title must be at most 20 characters long',
            'descrition.min' => 'Description must be at least 2 characters long',
            'descrition.max' => 'Description must be at most 20 characters long'
        ];
        $validator = Validator::make($request->all(), $rules, $msg);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $this->repo->update($request->only($this->repo->getModel()->fillable), $id);

        \Session::flash('success', 'Successfully Updated!');
        return redirect()->back();
    }

    public function edit($id) {
        $data['post'] = $this->repo->show($id);
        return view('front.posts.edit', $data);
    }

    public function store(Request $request) {
        $rules = [
            'title' => 'min:2|max:20',
            'description' => 'min:2'
        ];
        $msg = [
            'title.min' => 'Title must be at least 2 characters long',
            'title.max' => 'Title must be at most 20 characters long',
            'descrition.min' => 'Description must be at least 2 characters long',
            'descrition.max' => 'Description must be at most 20 characters long'
        ];
        $validator = Validator::make($request->all(), $rules, $msg);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $this->repo->create($request->only($this->repo->getModel()->fillable));
        \Session::flash('success', 'Successfully Posted!');
        return redirect()->back();
    }

    public function delete($id) {
        $this->repo->delete($id);
        \Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
