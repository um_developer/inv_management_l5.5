<?php

namespace App\Http\Controllers;

use DB;
use App\ApiServers;
use App\users;
use App\States;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use App\User;
use Session;
use Validator,
    Input,
    Redirect;

class ApiServerController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | ApiServer Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
         parent::__construct();
         if(Auth::user()->role_id != '2'){
            Redirect::to('customers')->send(); die;
        }
    }

    public function index() {

        $model = ApiServers::orderBy('id', 'desc')
                ->get();
        return view('front.api_server.index', compact('model'));
    }

    public function create() {
        return view('front.api_server.create');
    }

    public function edit($id) {

        $model = ApiServers::where('id', '=', $id)->first();
 
        if (count($model) == 0) {
            return redirect('/');
        }
        return view('front.api_server.edit', compact('model'));
    }

    public function postCreate(Request $request) {

        $user_id = Auth::user()->id;

        $validation = array(
            'title' => 'required|max:100',
            'username' => 'max:100',
            'url' => 'required|max:100',
            'password' => 'required|min:2|max:100',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        $api_server = new ApiServers;
        $api_server->title = $request->title;
        $api_server->username = $request->username;
        $api_server->password = $request->password;
        $api_server->url = $request->url;
        $api_server->save();

        if (isset($api_server->id)) {
            Session::flash('success', 'API server is created successfully');
            return redirect()->back();
        } else {
            Session::flash('error', 'API server is not created. Please try again.');
            return redirect()->back();
        }
    }

    public function postUpdate(Request $request) {

        $api_server = $request->id;
        $validation = array(
            'title' => 'required|max:100',
            'username' => 'max:100',
            'url' => 'required|max:100',
            'password' => 'required|min:2|max:100',
            'id' => 'required',
        );

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'form');
        }
        $api_server = $request->id;
        $input = $request->all();
        array_forget($input, '_token');
        array_forget($input, 'id');
        array_forget($input, 'submit');

        ApiServers::where('id', '=', $api_server)->update($input);
        Session::flash('success', 'API Server has been updated.');
        return redirect()->back();
    }

    public function delete($id) {
        $customer =  ApiServers::where('id', '=', $id)->get();
        ApiServers::where('id', '=', $id)->delete();
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
