<?php

namespace App\Http\Controllers;

use App\CreditMemo;
use App\CreditMemoItems;
use App\Customers;
use App\InvoiceItems;
use App\Invoices;
use App\Items;
use App\Payments;
use App\Vendors;
use Carbon\Carbon;
use App\RoItems;
use DB;
use Illuminate\Http\Request;
use Redirect;

class AdminReportsController extends AdminController {
    /*
      |--------------------------------------------------------------------------
      | Admin Inventory Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function customer() {

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $start_date = Carbon::today()->subDay(7)->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $customers = Customers::where('deleted', '0')->get();

        $model = $payments = Payments::where('payments.status', '!=', 'pending')->where('payments.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])->where('user_type', 'customer')->orderBy('payments.created_at', 'desc')
                ->select('payments.*', 'c.name as customer_name')
                ->get();

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name . ' (BAL: $' . $item->balance . ')';
        }

        $customers = $all_packages;

        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Invoice Payments';

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;

        $report_type = $type;
        $model = [];
        return view('front.reports.customer', compact('model', 'customers', 'report_type', 'date', 'print_page_info', 'customer_info'));
    }

    public function getReportByCustomer(Request $request) {

        $customers = Customers::where('deleted', '0')->get();
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $date = $request->date_range;

        if ($date == 'Select Date Range' && $request->item_id == 0) {
            return redirect()->back();
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name . ' (BAL: $' . $item->balance . ')';
        }

        $customers = $all_packages;

        $type[1] = 'Complete Transaction';
        $type[2] = 'Payments History';
        $type[3] = 'Invoice Payments';
        $report_type = $type;

        $customer_id = $request->customer_id;
        $selected_report_type = $request->report_type_id;
        $customer = Customers::where('id', $customer_id)->first();

        $payments = Payments::where('payments.status', '!=', 'pending')->where('payments.deleted', '=', '0')
                ->whereBetween('payments.statement_date', [$start_date, $end_date])
                ->where('payments.user_type', 'customer')
                ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id');

        if ($selected_report_type == '2') {
            $payments = $payments->where('payment_type', 'direct');
        } elseif ($selected_report_type == '3') {
            $payments = $payments->where('payment_type', 'invoice');
        }

        if ($customer_id != '0') {
            $payments = $payments->where('payments.user_id', $customer_id);
        }

        $payments = $payments->select('payments.*', 'c.name as customer_name')
                        ->orderBy('payments.created_at', 'desc')->get();
// dd($payments);
        $user_info = 'All Customers';
        $date_info = '.';
        if ($request->customer_id != '0') {
            $client = Customers::where('id', $request->customer_id)->first();
            $user_info = $client->name . '(' . $client->email . ')';
        }
        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        $model = $payments;
        return view('front.reports.customer', compact('model', 'customers', 'report_type', 'customer_info', 'date', 'print_page_info'));
    }

    public function customerSaleProfitReport() {

        $customers = Customers::where('deleted', '0')->get();
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $start_date = Carbon::today()->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $invoices = Invoices::where('invoices.deleted', '0')->where('invoices.status', '!=', 'pending')->where('invoices.customer_id', '!=', '0')
                        ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                        // ->whereBetween('invoices.statement_date', [$start_date, $end_date])
                        ->whereDate('invoices.created_at', '=', Carbon::today()->toDateString())
                        ->select(DB::raw("SUM(invoices.total_price) as sale_amount,SUM(invoices.profit) as profit_amount, invoices.customer_id, c.name as customer_name"))
                        ->groupby('invoices.customer_id')->get();

        $credit_memo = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                        ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                        ->whereDate('credit_memo.created_at', '=', Carbon::today()->toDateString())
                        //  ->whereBetween('credit_memo.statement_date', [$start_date, $end_date])
                        ->select(DB::raw("SUM(credit_memo.total_price) as sale_amount,SUM(credit_memo.loss) as loss_amount, credit_memo.customer_id, c.name as customer_name"))
                        ->groupby('credit_memo.customer_id')->get();

        $new_credit_memo = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->customer_id] = $memo;
        }
        $i = 0;
        foreach ($invoices as $invoice) {
            if (isset($new_credit_memo[$invoice->customer_id])) {
                $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->customer_id]['loss_amount'];
                $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->customer_id]['sale_amount'];
            }

            $model[$i] = $invoice;
            $i++;
        }

        $start_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;
        return view('front.reports.customer_profit_loss', compact('model', 'customers', 'report_type', 'date', 'print_page_info'));
    }

    public function customerSaleProfitReportSearch(Request $request) {

        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 'Select Date Range' && $request->customer_id == 0) {
            return redirect()->back();
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $customers = Customers::where('deleted', '0')->get();

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;
        $invoices = Invoices::where('invoices.deleted', '0')->where('invoices.status', '!=', 'pending')->where('invoices.customer_id', '!=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id');
        if ($date != 'Select Date Range') {
            $invoices = $invoices->whereBetween('invoices.statement_date', [$start_date, $end_date]);
        }

        if ($request->customer_id != 0) {
            $invoices = $invoices->where('invoices.customer_id', $request->customer_id);
        }

        $invoices = $invoices->select(DB::raw("SUM(invoices.total_price) as sale_amount,SUM(invoices.profit) as profit_amount, invoices.customer_id, c.name as customer_name"))
                        ->groupby('invoices.customer_id')->get();

        $credit_memo = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id');
        if ($date != 'Select Date Range') {
            $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);
        }

        if ($request->customer_id != 0) {
            $credit_memo = $credit_memo->where('credit_memo.customer_id', $request->customer_id);
        }

        $credit_memo = $credit_memo->select(DB::raw("SUM(credit_memo.total_price) as sale_amount,SUM(credit_memo.loss) as loss_amount, credit_memo.customer_id, c.name as customer_name"))
                        ->groupby('credit_memo.customer_id')->get();

        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->customer_id] = $memo;
        }

        foreach ($invoices as $memo) {
            $new_invoice[$memo->customer_id] = $memo;
        }

        $i = 0;
        if (count($invoices) >= count($credit_memo)) {
            foreach ($invoices as $invoice) {
                if (isset($new_credit_memo[$invoice->customer_id])) {
                    $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->customer_id]['loss_amount'];
                    $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->customer_id]['sale_amount'];
                }

                $model[$i] = $invoice;
                $i++;
            }
        } else {

            foreach ($credit_memo as $invoice) {

                if (isset($new_invoice[$invoice->customer_id])) {

                    $invoice['sale_amount'] = $new_invoice[$invoice->customer_id]['sale_amount'];
                    $invoice['profit_amount'] = $new_invoice[$invoice->customer_id]['profit_amount'];

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;
                } else {

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;

                    $invoice['sale_amount'] = '0';
                    $invoice['profit_amount'] = '0';
                }

                $model[$i] = $invoice;
                $i++;
            }
        }

        $user_info = 'All Customers';
        $date_info = '.';
        if ($request->customer_id != '0') {
            $client = Customers::where('id', $request->customer_id)->first();
            $user_info = $client->name . '(' . $client->email . ')';
        }
        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        return view('front.reports.customer_profit_loss', compact('model', 'customers', 'report_type', 'date', 'print_page_info'));
    }

    public function itemSaleProfitReport() {

        $items = Items::where('deleted', '0')->get();
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        $all_packages[0] = 'Select Item';
        foreach ($items as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $items = $all_packages;

        $customers = Customers::where('deleted', '0')->get();

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $start_date = Carbon::today()->toDateString();
        $end_date = Carbon::today()->addDay(1)->toDateString();

        $invoice_items = InvoiceItems::where('invoice_items.deleted', '0')
                        ->leftjoin('invoices as i', 'i.id', '=', 'invoice_items.invoice_id')
                        ->leftjoin('items as it', 'it.id', '=', 'invoice_items.item_id')
                        ->whereBetween('i.statement_date', [$start_date, $end_date])
                        ->where('i.status', '!=', 'pending')
                        ->select(DB::raw("SUM(invoice_items.total_price) as invoice_sale_amount, SUM(invoice_items.profit) as profit_amount, SUM(invoice_items.quantity) as invoice_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('invoice_items.item_id')->get();

        $credit_memo_items = CreditMemoItems::where('credit_memo_items.deleted', '0')
                        ->leftjoin('credit_memo as c', 'c.id', '=', 'credit_memo_items.credit_memo_id')
                        ->leftjoin('items as it', 'it.id', '=', 'credit_memo_items.item_id')
                        ->whereBetween('c.statement_date', [$start_date, $end_date])
                        ->where('c.status', '!=', 'pending')
                        ->select(DB::raw("SUM(credit_memo_items.total_price) as credit_memo_sale_amount,SUM(credit_memo_items.loss) as loss_amount,SUM(credit_memo_items.quantity) as credit_memo_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('credit_memo_items.item_id')->get();

        $new_credit_memo_items = [];
        $new_invoice_items = [];
        $model = [];

        foreach ($credit_memo_items as $memo) {
            $new_credit_memo_items[$memo->item_id] = $memo;
        }

        foreach ($invoice_items as $memo) {
            $new_invoice_items[$memo->item_id] = $memo;
        }
        $i = 0;
        foreach ($invoice_items as $invoice) {
            if (isset($new_credit_memo_items[$invoice->item_id])) {
                $invoice['credit_memo_loss_amount'] = $new_credit_memo_items[$invoice->item_id]['loss_amount'];
                $invoice['credit_memo_sale_amount'] = $new_credit_memo_items[$invoice->item_id]['credit_memo_sale_amount'];
                $invoice['credit_memo_quantity'] = $new_credit_memo_items[$invoice->item_id]['credit_memo_quantity'];
            }

            $model[$i] = $invoice;
            $i++;
        }

        $start_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;
        return view('front.reports.item_profit_loss', compact('model', 'items', 'report_type', 'date', 'print_page_info', 'customers'));
    }

    public function itemSaleProfitReportSearch(Request $request) {

        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 'Select Date Range' && $request->item_id == 0) {
            return redirect()->back();
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $items = Items::where('deleted', '0')->get();
        $all_packages[0] = 'Select Item';
        foreach ($items as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $items = $all_packages;

        $customers = Customers::where('deleted', '0')->get();

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        $invoices = InvoiceItems::where('invoice_items.deleted', '0')
                ->leftjoin('invoices as i', 'i.id', '=', 'invoice_items.invoice_id')
                ->leftjoin('items as it', 'it.id', '=', 'invoice_items.item_id')
                ->where('i.status', '!=', 'pending');
        if ($date != 'Select Date Range') {
            $invoices = $invoices->whereBetween('i.statement_date', [$start_date, $end_date]);
        }

        if ($request->item_id != 0) {
            $invoices = $invoices->where('invoice_items.item_id', $request->item_id);
        }

        $invoices = $invoices->select(DB::raw("SUM(invoice_items.total_price) as invoice_sale_amount, SUM(invoice_items.profit) as profit_amount, SUM(invoice_items.quantity) as invoice_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('invoice_items.item_id')->get();

        $credit_memo = CreditMemoItems::where('credit_memo_items.deleted', '0')
                ->leftjoin('credit_memo as c', 'c.id', '=', 'credit_memo_items.credit_memo_id')
                ->leftjoin('items as it', 'it.id', '=', 'credit_memo_items.item_id')
                ->where('c.status', '!=', 'pending');
        if ($date != 'Select Date Range') {
            $credit_memo = $credit_memo->whereBetween('c.statement_date', [$start_date, $end_date]);
        }

        if ($request->item_id != 0) {
            $credit_memo = $credit_memo->where('credit_memo_items.item_id', $request->item_id);
        }

        $credit_memo = $credit_memo->select(DB::raw("SUM(credit_memo_items.total_price) as credit_memo_sale_amount,SUM(credit_memo_items.loss) as loss_amount,SUM(credit_memo_items.quantity) as credit_memo_quantity, it.name as item_name, it.id as item_id"))
                        ->groupby('credit_memo_items.item_id')->get();

        $new_credit_memo_items = [];
        $new_invoice_items = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo_items[$memo->item_id] = $memo;
        }

        foreach ($invoices as $memo) {
            $new_invoice_items[$memo->item_id] = $memo;
        }

        $i = 0;
        if (count($invoices) >= count($credit_memo)) {
            foreach ($invoices as $invoice) {
                if (isset($new_credit_memo_items[$invoice->item_id])) {
                    $invoice['credit_memo_loss_amount'] = $new_credit_memo_items[$invoice->item_id]['loss_amount'];
                    $invoice['credit_memo_sale_amount'] = $new_credit_memo_items[$invoice->item_id]['credit_memo_sale_amount'];
                    $invoice['credit_memo_quantity'] = $new_credit_memo_items[$invoice->item_id]['credit_memo_quantity'];
                }

                $model[$i] = $invoice;
                $i++;
            }
        } else {

            foreach ($credit_memo as $invoice) {

                if (isset($new_invoice_items[$invoice->item_id])) {

                    $invoice['invoice_sale_amount'] = $new_invoice_items[$invoice->item_id]['invoice_sale_amount'];
                    $invoice['profit_amount'] = $new_invoice_items[$invoice->item_id]['profit_amount'];

                    $invoice['credit_memo_loss_amount'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale_amount'] = $invoice->credit_memo_sale_amount;
                } else {

                    $invoice['credit_memo_loss_amount'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale_amount'] = $invoice->credit_memo_sale_amount;

                    $invoice['invoice_sale_amount'] = '0';
                    $invoice['profit_amount'] = '0';
                }

                $model[$i] = $invoice;
                $i++;
            }
        }

        if ($date != 'Select Date Range') {
            $print_page_info['top'] = 'Report of items from Date Range ' . $date;
        }

        $print_page_info['bottom'] = '';

        return view('front.reports.item_profit_loss', compact('model', 'items', 'report_type', 'date', 'print_page_info', 'customers'));
    }

    public function CustomerBalanceReport() {

        $model = Customers::where('customers.deleted', '=', '0')
                ->orderBy('balance', 'desc')
                ->get();

        return view('front.reports.customer_balance', compact('model'));
    }

    public function VendorBalanceReport() {

        $model = Vendors::where('deleted', '=', '0')
                ->orderBy('balance', 'desc')
                ->get();

        return view('front.reports.vendor_balance', compact('model'));
    }

    public function customerStatementReport() {

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $customer_id = 0;
        $end_date = Carbon::today()->addDay(1)->toDateString();
        $start_date = '2018-06-09 00:00:00';

        $customers = Customers::where('deleted', '0')->get();
        $customer_id = 0;
        $model = [];

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name . ' (BAL: $' . $item->balance . ')';
        }

        $customers = $all_packages;

        $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
        $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));

        $date = $start_date . ' - ' . $end_date;

        return view('front.reports.customer_statement_report', compact('model', 'customers', 'date', 'print_page_info', 'customer_info'));
    }

    public function getStatementReportByCustomer(Request $request) {

        $customers = Customers::where('deleted', '0')->get();
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $date = $request->date_range;

        if ($date == 'Select Date Range' && $request->item_id == 0) {
            return redirect()->back();
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date_for_array_searching = date("Y-m-d", strtotime($date_new[0]));
            $end_date_for_array_searching = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
//            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $start_date = '2018-06-09 00:00:00';
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name . ' (BAL: $' . $item->balance . ')';
        }

        $customers = $all_packages;

        $customer_id = $request->customer_id;
        $selected_report_type = $request->report_type_id;
        $customer = Customers::where('id', $customer_id)->first();

        $payments = $payments = Payments::where('payments.status', '!=', 'pending')->where('payments.modification_count', '=', '0')->where('payments.deleted', '=', '0')
                        ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                        ->whereBetween('payments.statement_date', [$start_date, $end_date])->where('user_type', 'customer');

        if ($customer_id != '0') {
            $payments = $payments->where('payments.user_id', $customer_id);
        }

        $payments = $payments->select('payments.*', 'c.name as customer_name')
                        ->orderBy('statement_date', 'desc')->orderBy('payments.id', 'desc')->get();

        $new_model = [];
        $final_array = [];
        if (count($payments) > 0) {
            $new_model = $payments;

            $i = 1;
            $previous_palance = 0;
            $updated_balance = 0;
            $new_array = [];
            $final_balance = 0;
            foreach ($new_model as $item) {

                if ($i == '1') {
                    if ($item->payment_type == 'direct') {

                        if ($item->transaction_type == 'negative') {
                            $updated_balance = str_replace("-", "", $item->total_modified_amount);
                        } else {
                            $updated_balance = '-' . $item->total_modified_amount;
                        }
                    } else {
                        $updated_balance = $item->total_modified_amount;
                    }
                } else {
                    $previous_palance = $updated_balance;
                    if ($item->payment_type == 'direct') {

                        if ($item->transaction_type == 'negative') {
                            $updated_balance = $updated_balance - str_replace("-", "-", $item->total_modified_amount);
                        } else {
                            $updated_balance = $updated_balance - $item->total_modified_amount;
                        }
                    } else {
                        $updated_balance = $updated_balance + $item->total_modified_amount;
                    }
                }

                $final_balance = $updated_balance;
                $new_array[] = $item;

                $i++;
            }

//            print_r($final_balance); die;
            $previous_palance = 0;
            $updated_balance = $final_balance;
//            dd($new_array);
            $i = 1;
            foreach ($new_array as $item) {

                if ($item->transaction_type == 'negative' && $item->payment_type == 'direct') {
                    $item->total_modified_amount = $item->total_modified_amount;
                } else {
                    $item->total_modified_amount = str_replace("-", "", $item->total_modified_amount);
                }

                if ($i == '1') {
                    $updated_balance = $final_balance;

                    if ($item->payment_type == 'invoice') {
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    } else {
                        $previous_palance = $updated_balance + $item->total_modified_amount;
                    }
                } else {
                    $updated_balance = $previous_palance;
                    if ($item->payment_type == 'invoice') {
                        $previous_palance = $updated_balance - $item->total_modified_amount;
                    } else {
                        $previous_palance = $updated_balance + $item->total_modified_amount;
                    }
                }

                $item->new_previous_balance = round($previous_palance, 2);
                $item->new_updated_balance = round($updated_balance, 2);
                if ($item->payment_type == 'direct' && $item->transaction_type == 'positive' || $item->payment_type == 'credit_memo') {
                    $item->new_total_modified_amount = '-' . $item->total_modified_amount;
                } elseif ($item->payment_type == 'direct' && $item->transaction_type == 'negative') {
                    $item->new_total_modified_amount = str_replace("-", "", $item->total_modified_amount);
                } else {
                    $item->new_total_modified_amount = $item->total_modified_amount;
                }

                $final_balance = $updated_balance;
                $final_array[] = $item;
                $i++;
            }
        }

        $payments = $final_array;

        $user_info = 'All Customers';
        $date_info = '.';
        if ($request->customer_id != '0') {
            $client = Customers::where('id', $request->customer_id)->first();
            $user_info = $client->name . '(' . $client->email . ') ('.$client->address1.' '.$client->address2.' '.$client->city.' '.$client->state.')';
        }
        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Report of ' . $user_info . $date_info;

        $start = $start_date_for_array_searching;
        $end = $end_date_for_array_searching;

        $result = array_filter($payments, function ($value) use ($start, $end) {
            return $start <= $value['statement_date'] && $value['statement_date'] <= $end;
        });

        $model = $result;
        return view('front.reports.customer_statement_report', compact('model', 'customers', 'report_type', 'customer_info', 'date', 'print_page_info'));
    }

    public function customerSaleProfitReportNew($date = 0, $selected_type = 'items') {

        $customers = Customers::where('deleted', '0')->get();
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';
        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        if ($date == 0) {
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
        } else {
            $date_new = explode(" - ", $date);
            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }
        $array = [];
        if ($selected_type == 'items') {


            $invoices = self::getInvoiceData($start_date, $end_date);
            $credit_memo = self::getCreditMemoData($start_date, $end_date);

            $new_credit_memo = [];
            $new_invoice = [];
            $model = [];

            foreach ($credit_memo as $memo) {
                $new_credit_memo[$memo->item_id] = $memo;
            }

            foreach ($invoices as $memo) {

                $new_invoice[$memo->item_id] = $memo;
            }

            $i = 0;
            foreach ($invoices as $invoice) {
                if (isset($new_credit_memo[$invoice->item_id])) {
                    $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                    $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];

                    unset($new_credit_memo[$invoice->item_id]);
                } else {

                    $invoice['credit_memo_loss'] = '0';
                    $invoice['credit_memo_sale'] = '0';
                }

                $model[$i] = $invoice;
                array_push($array, $model[$i]);

                $i++;
            }

            if (count($new_credit_memo) > 0) {
                foreach ($new_credit_memo as $invoice) {

                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;
                    $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                    $invoice['sale_amount'] = '0';
                    $invoice['profit_amount'] = '0';

                    $model[$i] = $invoice;

                    array_push($array, $model[$i]);
                    $i++;
                }
            }
        }

        if ($selected_type == 'invoice') {
            $array = [];

            $invoices = Invoices::where('invoices.deleted', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                    ->where('invoices.status', '!=', 'pending');

            $invoices = $invoices->whereBetween('invoices.statement_date', [$start_date, $end_date]);

            $invoices = $invoices->select(
                            'invoices.total_price as sale_amount', 'invoices.profit as profit_amount', 'invoices.customer_id', 'c.name as customer_name', 'invoices.id as invoice_id', 'invoices.statement_date'
                    )
                    ->get();

            $credit_memo = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                    ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id');

            $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);

            $credit_memo = $credit_memo->select(
                            'credit_memo.total_price as sale_amount', 'credit_memo.loss as profit_amount', 'credit_memo.customer_id', 'c.name as customer_name', 'credit_memo.id', 'credit_memo.statement_date'
                    )
                    ->get();
//            print_r($credit_memo); die;
            $i = 0;

            foreach ($invoices as $invoice) {
                if ($invoice->customer_id != null) {
                    $model[$i] = $invoice;
                    array_push($array, $model[$i]);
                    $i++;
                }
            }
            foreach ($credit_memo as $invoice) {
                if ($invoice->customer_id != null) {
                    $model[$i] = $invoice;
                    array_push($array, $model[$i]);
                    $i++;
                }
            }
        }

        if ($date == 0) {
            $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
            $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
            $date = $start_date . ' - ' . $end_date;
        } else {

            $date_new = explode(" - ", $date);
            $start_date = date("m/d/Y", strtotime($date_new[0]));
            $end_date = date("m/d/Y", strtotime($date_new[1]));
            $date = $start_date . ' - ' . $end_date;
        }
        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Customer Sale/Profit report (' . $selected_type . ') ' . $date_info;

        $date = $start_date . ' - ' . $end_date;
        $selected_customer = array();

        return view('front.reports.customer_profit_loss-new', compact('array', 'customers', 'report_type', 'date', 'print_page_info', 'selected_customer', 'selected_type'));
    }

    public function customerSaleProfitReportSearchNew(Request $request) {

        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 'Select Date Range' && count($request->customer_id) == 0) {
            return redirect()->back();
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        if (count($request->customer_id) == 0) {
            return $this->customerSaleProfitReportNew($request->date_range, $request->selected_type);
        }

        $customers = Customers::where('deleted', '0')->get();

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;
        $array = [];
        $array1 = [];
        $array2 = [];
        if ($request->selected_type == 'items') {
            foreach ($request->customer_id as $row) {

//                $invoices = InvoiceItems::where('invoice_items.deleted', '0')->where('invoice_items.customer_id', '!=', '0')
//                        ->leftjoin('customers as c', 'c.id', '=', 'invoice_items.customer_id')
//                        ->leftjoin('invoices as in', 'in.id', '=', 'invoice_items.invoice_id')
//                        ->leftjoin('items', 'items.id', '=', 'invoice_items.item_id')
//                        ->where('in.status', '!=', 'pending')
//                        ->where('in.deleted', '0');

                $invoices = self::getInvoiceData($start_date, $end_date, $row);
                $credit_memo = self::getCreditMemoData($start_date, $end_date, $row);

//                if ($date != 'Select Date Range') {
//                    $invoices = $invoices->whereBetween('in.statement_date', [$start_date, $end_date]);
//                }
//                $invoices = $invoices->where('invoice_items.customer_id', $row);
//
//                $invoices = $invoices->select(DB::raw("SUM(invoice_items.total_price) as sale_amount,SUM(invoice_items.profit) as profit_amount, invoice_items.customer_id, c.name as customer_name, items.name as item_name, invoice_items.item_id as item_id"))
//                                ->groupby('invoice_items.item_id')->get();
//
//                $credit_memo = CreditMemoItems::where('credit_memo_items.deleted', '0')
//                        ->where('credit_memo_items.customer_id', '!=', '0')
//                        ->leftjoin('credit_memo', 'credit_memo.id', '=', 'credit_memo_items.credit_memo_id')
//                        ->leftjoin('customers as c', 'c.id', '=', 'credit_memo_items.customer_id')
//                        ->leftjoin('items', 'items.id', '=', 'credit_memo_items.item_id')
//                        ->where('credit_memo.status', '!=', 'pending')
//                        ->where('credit_memo.deleted', '0');
//
//                $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);
//
//                $credit_memo = $credit_memo->where('credit_memo_items.customer_id', $row);
//
//                $credit_memo = $credit_memo->select(DB::raw("SUM(credit_memo_items.total_price) as sale_amount,SUM(credit_memo_items.loss) as loss_amount, credit_memo_items.customer_id, c.name as customer_name, items.name as item_name, credit_memo_items.item_id as item_id"))
//                                ->groupby('credit_memo_items.item_id')->get();
//   dd($credit_memo);
                $new_credit_memo = [];
                $new_invoice = [];
                $model = [];

                foreach ($credit_memo as $memo) {
                    $new_credit_memo[$memo->item_id] = $memo;
                }

                foreach ($invoices as $memo) {

                    $new_invoice[$memo->item_id] = $memo;
                }

                $i = 0;
                foreach ($invoices as $invoice) {
                    if (isset($new_credit_memo[$invoice->item_id])) {
                        $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                        $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];

                        unset($new_credit_memo[$invoice->item_id]);
                    } else {

                        $invoice['credit_memo_loss'] = '0';
                        $invoice['credit_memo_sale'] = '0';
                    }

                    $model[$i] = $invoice;
                    array_push($array, $model[$i]);

                    $i++;
                }

//
                if (count($new_credit_memo) > 0) {
                    foreach ($new_credit_memo as $invoice) {

                        $invoice['credit_memo_loss'] = $invoice->loss_amount;
                        $invoice['credit_memo_sale'] = $invoice->sale_amount;
                        $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                        $invoice['sale_amount'] = '0';
                        $invoice['profit_amount'] = '0';

                        $model[$i] = $invoice;

                        array_push($array, $model[$i]);
                        $i++;
                    }
                }
            }
        }

        if ($request->selected_type == 'invoice') {

            foreach ($request->customer_id as $row) {

                $invoices = Invoices::where('invoices.deleted', '0')
                        ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                        ->where('invoices.status', '!=', 'pending');

                $invoices = $invoices->whereBetween('invoices.statement_date', [$start_date, $end_date]);

                $invoices = $invoices->where('invoices.customer_id', $row);

                $invoices = $invoices->select(
                                'invoices.total_price as sale_amount', 'invoices.profit as profit_amount', 'invoices.customer_id', 'c.name as customer_name', 'invoices.id as invoice_id', 'invoices.statement_date'
                        )
                        ->get();

                $credit_memo = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                        ->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id');

                if ($date != 'Select Date Range') {
                    $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);
                }
                if ($row != 0) {
                    $credit_memo = $credit_memo->where('credit_memo.customer_id', $row);
                }

                $credit_memo = $credit_memo->select(
                                'credit_memo.total_price as sale_amount', 'credit_memo.loss as profit_amount', 'credit_memo.customer_id', 'c.name as customer_name', 'credit_memo.id', 'credit_memo.statement_date'
                        )
                        ->get();

                $i = 0;

                foreach ($invoices as $invoice) {
                    if ($invoice->customer_id != null) {
                        $model[$i] = $invoice;
                        array_push($array, $model[$i]);
                        $i++;
                    }
                }
                foreach ($credit_memo as $invoice) {
                    if ($invoice->customer_id != null) {
                        $model[$i] = $invoice;
                        array_push($array, $model[$i]);
                        $i++;
                    }
                }
            }
        }
        $selected_type = $request->selected_type;
        $selected_customer = $request->customer_id;
        // dd($array);
        $name = 'statement_date';
        usort($array, function ($a, $b) use (&$name) {
            return $a[$name] - $b[$name];
        });

        if ($date == 0) {
            $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
            $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
            $date = $start_date . ' - ' . $end_date;
        }
        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Customer Sale/Profit report (' . $selected_type . ') ' . $date_info;

        return view('front.reports.customer_profit_loss-new', compact('array', 'customers', 'report_type', 'date', 'print_page_info', 'selected_customer', 'selected_type'));
    }

    public function date_compare($a, $b) {
        $t1 = strtotime($a['datetime']);
        $t2 = strtotime($b['datetime']);
        return $t1 - $t2;
    }

    public function itemSaleProfitReportNew($date = 0, $selected_type = 'customer') {

        $items = Items::where('deleted', '0')->get();
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        $all_packages[0] = 'Select Item';
        foreach ($items as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $items = $all_packages;

        $customers = Customers::where('deleted', '0')->get();

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;

        if ($date == 0) {
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
        } else {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $invoices = self::getInvoiceData($start_date, $end_date);
        $credit_memo = self::getCreditMemoData($start_date, $end_date);

        $array = [];
        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->item_id] = $memo;
        }

        foreach ($invoices as $memo) {

            $new_invoice[$memo->item_id] = $memo;
        }

        $i = 0;
        foreach ($invoices as $invoice) {
            $invoice['invoice_sale_amount'] = $invoice->sale_amount;
            if (isset($new_credit_memo[$invoice->item_id])) {
                $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];


                unset($new_credit_memo[$invoice->item_id]);
            } else {

                $invoice['credit_memo_loss'] = '0';
                $invoice['credit_memo_sale'] = '0';
            }

            $model[$i] = $invoice;
            array_push($array, $model[$i]);

            $i++;
        }

        if (count($new_credit_memo) > 0) {
            foreach ($new_credit_memo as $invoice) {

                $invoice['credit_memo_sale_amount'] = $invoice->sale_amount;

                $invoice['credit_memo_loss'] = $invoice->loss_amount;
                $invoice['credit_memo_sale'] = $invoice->sale_amount;
                $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                $invoice['invoice_sale_amount'] = '0';
                $invoice['profit_amount'] = '0';

                $model[$i] = $invoice;

                array_push($array, $model[$i]);
                $i++;
            }
        }



        if ($selected_type == 'vendor') {
            $sold_items1 = [];
            foreach ($array as $item) {

                $vendor_name = 'NOT FOUND';
                $vendor_id = '0';
                $ro_item = RoItems::where('item_id', $item->item_id)->where('ro_items.deleted', 0)
                        ->leftjoin('vendors as v', 'v.id', '=', 'ro_items.vendor_id')
                        ->orderBy('ro_items.id', 'DESC')
                        ->select('v.*')
                        ->first();
                if (count($ro_item) > 0) {
                    $vendor_name = $ro_item->name;
                    $vendor_id = $ro_item->id;
                }
                $item->vendor_name = $vendor_name;
                $item->vendor_id = $vendor_id;
                $sold_items1[] = $item;
            }
            $array = $sold_items1;
        }

        if ($date == 0) {
            $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
            $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
            $date = $start_date . ' - ' . $end_date;
        }


        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Item Sale/Profit report (' . $selected_type . ') ' . $date_info;
//        print_r($print_page_info); die;
        $selected_items0 = array();

        return view('front.reports.item_profit_loss_new', compact('model', 'items', 'report_type', 'date', 'print_page_info', 'customers', 'array', 'selected_items0', 'selected_type'));
    }

    public function itemSaleProfitReportSearchNew(Request $request) {

        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 'Select Date Range' && count($request->item_id) == 0) {
            return redirect()->back();
        }

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        if (count($request->item_id) == 0) {

            return $this->itemSaleProfitReportNew($date, $request->selected_type);
        }

        $items = Items::where('deleted', '0')->get();
        $all_packages[0] = 'Select Item';
        foreach ($items as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $items = $all_packages;

        $customers = Customers::where('deleted', '0')->get();

        $all_packages[0] = 'Select Customer';
        foreach ($customers as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $customers = $all_packages;
        $array = [];

        foreach ($request->item_id as $row) {

            $invoices = self::getInvoiceData($start_date, $end_date, $row);
            $credit_memo = self::getCreditMemoData($start_date, $end_date, $row);

            $new_credit_memo = [];
            $new_invoice = [];
            $model = [];

            foreach ($credit_memo as $memo) {
                $new_credit_memo[$memo->item_id] = $memo;
            }

            foreach ($invoices as $memo) {

                $new_invoice[$memo->item_id] = $memo;
            }

            $i = 0;
            foreach ($invoices as $invoice) {
                 $invoice['invoice_sale_amount'] = $invoice->sale_amount;
                if (isset($new_credit_memo[$invoice->item_id])) {
                    $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                    $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];

                    unset($new_credit_memo[$invoice->item_id]);
                } else {

                    $invoice['credit_memo_loss'] = '0';
                    $invoice['credit_memo_sale'] = '0';
                }

                $model[$i] = $invoice;
                array_push($array, $model[$i]);

                $i++;
            }

            if (count($new_credit_memo) > 0) {
                foreach ($new_credit_memo as $invoice) {
 $invoice['credit_memo_sale_amount'] = $invoice->sale_amount;
                    $invoice['credit_memo_loss'] = $invoice->loss_amount;
                    $invoice['credit_memo_sale'] = $invoice->sale_amount;
                    $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                    $invoice['sale_amount'] = '0';
                    $invoice['profit_amount'] = '0';

                    $model[$i] = $invoice;

                    array_push($array, $model[$i]);
                    $i++;
                }
            }
        }


        if ($request->selected_type == 'vendor') {
            $sold_items1 = [];
            foreach ($array as $item) {

                $vendor_name = 'NOT FOUND';
                $vendor_id = '0';
                $ro_item = RoItems::where('item_id', $item->item_id)->where('ro_items.deleted', 0)
                        ->leftjoin('vendors as v', 'v.id', '=', 'ro_items.vendor_id')
                        ->orderBy('ro_items.id', 'DESC')
                        ->select('v.*')
                        ->first();
                if (count($ro_item) > 0) {
                    $vendor_name = $ro_item->name;
                    $vendor_id = $ro_item->id;
                }
                $item->vendor_name = $vendor_name;
                $item->vendor_id = $vendor_id;
                $sold_items1[] = $item;
            }
            $array = $sold_items1;
        }

        if ($date == 0) {
            $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
            $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
            $date = $start_date . ' - ' . $end_date;
        }


        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['bottom'] = '';
        $selected_items0 = $request->item_id;
        $selected_type = $request->selected_type;
        $print_page_info['top'] = 'Item Sale/Profit report (' . $selected_type . ') ' . $date_info;
        return view('front.reports.item_profit_loss_new', compact('model', 'items', 'report_type', 'date', 'print_page_info', 'customers', 'array', 'selected_items0', 'selected_type'));
    }

    public function itemSaleProfitReportVendorNew($date = 0) {

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 0) {
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
        } else {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $vendors = Vendors::where('deleted', '0')->get();

        $all_packages[0] = 'Select Vendor';
        foreach ($vendors as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $vendors = $all_packages;


        ////////////////////////////


        $sold_items = [];
        $invoices = self::getInvoiceData($start_date, $end_date);
        $credit_memo = self::getCreditMemoData($start_date, $end_date);

        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->item_id] = $memo;
        }

        foreach ($invoices as $memo) {

            $new_invoice[$memo->item_id] = $memo;
        }

        $i = 0;
        foreach ($invoices as $invoice) {
            if (isset($new_credit_memo[$invoice->item_id])) {
                $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];
                $invoice['credit_memo_quantity'] = $new_credit_memo[$invoice->item_id]['credit_memo_quantity'];

                unset($new_credit_memo[$invoice->item_id]);
            } else {

                $invoice['credit_memo_loss'] = '0';
                $invoice['credit_memo_sale'] = '0';
                $invoice['credit_memo_quantity'] = '0';
            }

            $model[$i] = $invoice;
            array_push($sold_items, $model[$i]);

            $i++;
        }

        if (count($new_credit_memo) > 0) {
            foreach ($new_credit_memo as $invoice) {

                $invoice['credit_memo_loss'] = $invoice->loss_amount;
                $invoice['credit_memo_sale'] = $invoice->sale_amount;
                $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                $invoice['sale_amount'] = '0';
                $invoice['profit_amount'] = '0';
                $invoice['invoice_quantity'] = '0';

                $model[$i] = $invoice;

                array_push($sold_items, $model[$i]);
                $i++;
            }
        }

        $sold_items1 = [];

        foreach ($sold_items as $item) {

            $vendor_name = 'NOT FOUND';
            $vendor_id = '0';
            $inventory_count = 0;
            $rem_po_quantity = 0;
            $ro_item = RoItems::where('item_id', $item->item_id)->where('ro_items.deleted', 0)
                    ->leftjoin('vendors as v', 'v.id', '=', 'ro_items.vendor_id')
                    ->orderBy('ro_items.id', 'DESC')
                    ->select('v.*')
                    ->first();
            if (count($ro_item) > 0) {
                $vendor_name = $ro_item->name;
                $vendor_id = $ro_item->id;
            }


            $inventory = \App\Inventory::where('item_id', $item->item_id)->first();

            if (count($inventory) > 0)
                $inventory_count = $inventory->quantity + $inventory->package_quantity;

            $purchase_order = \App\PoItems::where('item_id', $item->item_id)
                            ->where('deleted', 0)->where('vendor_id', $vendor_id)
                            ->select(DB::raw("SUM(quantity) as quantity, SUM(delivered_quantity) as delivered_quantity"))
                            ->groupBy('item_id')->first();

            if (count($purchase_order) > 0)
                $rem_po_quantity = $purchase_order->quantity - $purchase_order->delivered_quantity;


            $item->vendor_name = $vendor_name;
            $item->vendor_id = $vendor_id;
            $item->inventory = $inventory_count;
            $item->rem_po_quantity = $rem_po_quantity;
            $sold_items1[] = $item;
        }

        //////////////////////////////////

        $sold_items2 = [];
        $name = 'vendor_id';
        usort($sold_items1, function ($a, $b) use (&$name) {
            return $a[$name] - $b[$name];
        });

        foreach ($sold_items1 as $item) {
//            if ($item->vendor_id != '0')
            $sold_items2[] = $item;
        }

        $array = $sold_items2;



        if ($date == 0) {
            $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
            $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
            $date = $start_date . ' - ' . $end_date;
        }

        $selected_vendor = array();


        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Vendor Sale/Profit report ' . $date_info;

        $print_page_info['bottom'] = '';
        return view('front.reports.customer_profit_loss-vendor-new', compact('array', 'vendors', 'report_type', 'date', 'print_page_info', 'selected_vendor', 'selected_type'));
    }

    public function itemSaleProfitReportVendorNewSearch(Request $request) {
        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        if (count($request->vendor_id) == 0) {
            return $this->itemSaleProfitReportVendorNew($date);
        }

        $vendors = Vendors::where('deleted', '0')->get();

        $all_packages[0] = 'Select Vendor';
        foreach ($vendors as $item) {
            $all_packages[$item->id] = $item->name;
        }
        $vendors = $all_packages;

        ////////////////////////////


        $sold_items = [];
        $invoices = InvoiceItems::where('invoice_items.deleted', '0')->where('invoice_items.customer_id', '!=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoice_items.customer_id')
                ->leftjoin('invoices as in', 'in.id', '=', 'invoice_items.invoice_id')
                ->leftjoin('items', 'items.id', '=', 'invoice_items.item_id')
                ->where('in.status', '!=', 'pending')
                ->where('in.deleted', '0');

        $invoices = $invoices->whereBetween('in.statement_date', [$start_date, $end_date]);

        $invoices = $invoices->select(DB::raw("SUM(invoice_items.total_price) as sale_amount,SUM(invoice_items.profit) as profit_amount,SUM(invoice_items.quantity) as invoice_quantity, invoice_items.customer_id, c.name as customer_name, items.name as item_name, invoice_items.item_id as item_id"))
                        ->groupby('invoice_items.item_id')->get();

        $credit_memo = CreditMemoItems::where('credit_memo_items.deleted', '0')
                ->where('credit_memo_items.customer_id', '!=', '0')
                ->leftjoin('credit_memo', 'credit_memo.id', '=', 'credit_memo_items.credit_memo_id')
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo_items.customer_id')
                ->leftjoin('items', 'items.id', '=', 'credit_memo_items.item_id')
                ->where('credit_memo.status', '!=', 'pending')
                ->where('credit_memo.deleted', '0');

        $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);

        $credit_memo = $credit_memo->select(DB::raw("SUM(credit_memo_items.total_price) as sale_amount,SUM(credit_memo_items.loss) as loss_amount,SUM(credit_memo_items.quantity) as credit_memo_quantity, credit_memo_items.customer_id, c.name as customer_name, items.name as item_name, credit_memo_items.item_id as item_id"))
                        ->groupby('credit_memo_items.item_id')->get();

        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->item_id] = $memo;
        }

        foreach ($invoices as $memo) {

            $new_invoice[$memo->item_id] = $memo;
        }

        $i = 0;
        foreach ($invoices as $invoice) {
            if (isset($new_credit_memo[$invoice->item_id])) {
                $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];
                $invoice['credit_memo_quantity'] = $new_credit_memo[$invoice->item_id]['credit_memo_quantity'];

                unset($new_credit_memo[$invoice->item_id]);
            } else {

                $invoice['credit_memo_loss'] = '0';
                $invoice['credit_memo_sale'] = '0';
                $invoice['credit_memo_quantity'] = '0';
            }

            $model[$i] = $invoice;
            array_push($sold_items, $model[$i]);

            $i++;
        }

        if (count($new_credit_memo) > 0) {
            foreach ($new_credit_memo as $invoice) {

                $invoice['credit_memo_loss'] = $invoice->loss_amount;
                $invoice['credit_memo_sale'] = $invoice->sale_amount;
                $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                $invoice['sale_amount'] = '0';
                $invoice['profit_amount'] = '0';
                $invoice['invoice_quantity'] = '0';

                $model[$i] = $invoice;

                array_push($sold_items, $model[$i]);
                $i++;
            }
        }

        $sold_items1 = [];

        foreach ($sold_items as $item) {

            $vendor_name = 'NOT FOUND';
            $vendor_id = '0';
            $inventory_count = 0;
            $rem_po_quantity = 0;
            $ro_item = RoItems::where('item_id', $item->item_id)->where('ro_items.deleted', 0)
                    ->leftjoin('vendors as v', 'v.id', '=', 'ro_items.vendor_id')
                    ->orderBy('ro_items.id', 'DESC')
                    ->select('v.*')
                    ->first();
            if (count($ro_item) > 0) {
                $vendor_name = $ro_item->name;
                $vendor_id = $ro_item->id;
            }


            $inventory = \App\Inventory::where('item_id', $item->item_id)->first();

            if (count($inventory) > 0)
                $inventory_count = $inventory->quantity + $inventory->package_quantity;

            $purchase_order = \App\PoItems::where('item_id', $item->item_id)
                            ->where('deleted', 0)->where('vendor_id', $vendor_id)
                            ->select(DB::raw("SUM(quantity) as quantity, SUM(delivered_quantity) as delivered_quantity"))
                            ->groupBy('item_id')->first();

            if (count($purchase_order) > 0)
                $rem_po_quantity = $purchase_order->quantity - $purchase_order->delivered_quantity;


            $item->vendor_name = $vendor_name;
            $item->vendor_id = $vendor_id;
            $item->inventory = $inventory_count;
            $item->rem_po_quantity = $rem_po_quantity;
            $sold_items1[] = $item;
        }

        //////////////////////////////////

        $sold_items2 = [];
        $name = 'vendor_id';
        usort($sold_items1, function ($a, $b) use (&$name) {
            return $a[$name] - $b[$name];
        });

        foreach ($sold_items1 as $item) {
            if (in_array($item->vendor_id, $request->vendor_id))
                $sold_items2[] = $item;
        }

        $array = $sold_items2;
        $selected_vendor = $request->vendor_id;

        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Vendor Sale/Profit report ' . $date_info;
        return view('front.reports.customer_profit_loss-vendor-new', compact('array', 'vendors', 'report_type', 'date', 'print_page_info', 'selected_vendor', 'selected_type'));
    }

    private function getInvoiceData($start_date, $end_date, $customer_id = 0) {

        $invoices = InvoiceItems::where('invoice_items.deleted', '0')
                ->where('invoice_items.customer_id', '!=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoice_items.customer_id')
                ->leftjoin('invoices as in', 'in.id', '=', 'invoice_items.invoice_id')
                ->leftjoin('items', 'items.id', '=', 'invoice_items.item_id')
                ->where('in.status', '!=', 'pending')
                ->where('in.deleted', '0');
        $invoices = $invoices->whereBetween('in.statement_date', [$start_date, $end_date]);

        if ($customer_id != 0)
            $invoices = $invoices->where('invoice_items.customer_id', $customer_id);

        $invoices = $invoices->select(DB::raw("SUM(invoice_items.total_price) as sale_amount,SUM(invoice_items.profit) as profit_amount,SUM(invoice_items.quantity) as invoice_quantity, invoice_items.customer_id, c.name as customer_name, items.name as item_name, invoice_items.item_id as item_id"))
                        ->groupby('invoice_items.item_id')->get();

        return $invoices;
    }

    private function getCreditMemoData($start_date, $end_date, $customer_id = 0) {

        $credit_memo = CreditMemoItems::where('credit_memo_items.deleted', '2')
                ->where('credit_memo.customer_id', '!=', '0')
                ->leftjoin('credit_memo', 'credit_memo.id', '=', 'credit_memo_items.credit_memo_id')
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo_items.customer_id')
                ->leftjoin('items', 'items.id', '=', 'credit_memo_items.item_id')
                ->where('credit_memo.status', '!=', 'pending')
                ->where('credit_memo.deleted', '0');

        $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);
        if ($customer_id != 0)
            $credit_memo = $credit_memo->where('credit_memo_items.customer_id', $customer_id);

        $credit_memo = $credit_memo->select(DB::raw("SUM(credit_memo_items.total_price) as sale_amount,SUM(credit_memo_items.loss) as loss_amount, credit_memo_items.customer_id, c.name as customer_name, items.name as item_name, credit_memo_items.item_id as item_id,credit_memo.id as credit_memo_id"))
                        ->groupby('credit_memo_items.item_id')->get();

        return $credit_memo;
    }


    public function newCombineReport($date = 0) {

        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date == 0) {
            $start_date = Carbon::today()->subDay(7)->toDateString();
            $end_date = Carbon::today()->addDay(1)->toDateString();
        } else {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        
        $invoices = self::getInvoiceData($start_date, $end_date);
        $credit_memo = self::getCreditMemoData($start_date, $end_date);

        $sold_items = [];
        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) { $new_credit_memo[$memo->item_id] = $memo; }
        foreach ($invoices as $memo) { $new_invoice[$memo->item_id] = $memo; }

        $i = 0;
        foreach ($invoices as $invoice) {
            if (isset($new_credit_memo[$invoice->item_id])) {
                $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];
                $invoice['credit_memo_quantity'] = $new_credit_memo[$invoice->item_id]['credit_memo_quantity'];

                unset($new_credit_memo[$invoice->item_id]);
            } else {

                $invoice['credit_memo_loss'] = '0';
                $invoice['credit_memo_sale'] = '0';
                $invoice['credit_memo_quantity'] = '0';
            }

            $model[$i] = $invoice;
            array_push($sold_items, $model[$i]);

            $i++;
        }

        if (count($new_credit_memo) > 0) {
            foreach ($new_credit_memo as $invoice) {

                $invoice['credit_memo_loss'] = $invoice->loss_amount;
                $invoice['credit_memo_sale'] = $invoice->sale_amount;
                $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                $invoice['sale_amount'] = '0';
                $invoice['profit_amount'] = '0';
                $invoice['invoice_quantity'] = '0';

                $model[$i] = $invoice;

                array_push($sold_items, $model[$i]);
                $i++;
            }
        }

        $sold_items1 = [];

        foreach ($sold_items as $item) {

            $vendor_name = 'NOT FOUND';
            $vendor_id = '0';
            $inventory_count = 0;
            $rem_po_quantity = 0;
            $ro_item = RoItems::where('item_id', $item->item_id)->where('ro_items.deleted', 0)
                    ->leftjoin('vendors as v', 'v.id', '=', 'ro_items.vendor_id')
                    ->orderBy('ro_items.id', 'DESC')
                    ->select('v.*')
                    ->first();
            if (count($ro_item) > 0) {
                $vendor_name = $ro_item->name;
                $vendor_id = $ro_item->id;
            }


            $inventory = \App\Inventory::where('item_id', $item->item_id)->first();

            if (count($inventory) > 0)
                $inventory_count = $inventory->quantity + $inventory->package_quantity;

            $purchase_order = \App\PoItems::where('item_id', $item->item_id)
                            ->where('deleted', 0)->where('vendor_id', $vendor_id)
                            ->select(DB::raw("SUM(quantity) as quantity, SUM(delivered_quantity) as delivered_quantity"))
                            ->groupBy('item_id')->first();

            if (count($purchase_order) > 0)
                $rem_po_quantity = $purchase_order->quantity - $purchase_order->delivered_quantity;


            $item->vendor_name = $vendor_name;
            $item->vendor_id = $vendor_id;
            $item->inventory = $inventory_count;
            $item->rem_po_quantity = $rem_po_quantity;
            $sold_items1[] = $item;
        }

        //////////////////////////////////

        $sold_items2 = [];
        $name = 'vendor_id';
        usort($sold_items1, function ($a, $b) use (&$name) {
            return $a[$name] - $b[$name];
        });

        foreach ($sold_items1 as $item) {
//            if ($item->vendor_id != '0')
            $sold_items2[] = $item;
        }

        $array = $sold_items2;


        if ($date == 0) {
            $start_date = date('m/d/Y', strtotime("-7 day", strtotime(date("m/d/Y"))));
            $end_date = date('m/d/Y', strtotime("+0 day", strtotime(date("m/d/Y"))));
            $date = $start_date . ' - ' . $end_date;
        }

        $selected_vendor = array();


        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Vendor Sale/Profit report ' . $date_info;

        $print_page_info['bottom'] = '';
        return view('front.reports.customer_profit_loss-vendor-new1', compact('array', 'report_type', 'date', 'print_page_info', 'selected_vendor', 'selected_type'));
    }

    public function newCombineReportSearch(Request $request) {
        $date = $request->date_range;
        $print_page_info['top'] = '';
        $print_page_info['bottom'] = '';

        if ($date != 'Select Date Range') {
            $date_new = explode(" - ", $date);

            $start_date = date("Y-m-d", strtotime($date_new[0]));
            $end_date = date("Y-m-d", strtotime($date_new[1] . " +1 days"));
        }

        $input = $request->all();
        print_r($input); die;

        if (count($request->type) == '') {
            return $this-> newCombineReport($date);
        }

       
        ////////////////////////////


        $sold_items = [];
        $invoices = InvoiceItems::where('invoice_items.deleted', '0')->where('invoice_items.customer_id', '!=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoice_items.customer_id')
                ->leftjoin('invoices as in', 'in.id', '=', 'invoice_items.invoice_id')
                ->leftjoin('items', 'items.id', '=', 'invoice_items.item_id')
                ->where('in.status', '!=', 'pending')
                ->where('in.deleted', '0');

        $invoices = $invoices->whereBetween('in.statement_date', [$start_date, $end_date]);

        $invoices = $invoices->select(DB::raw("SUM(invoice_items.total_price) as sale_amount,SUM(invoice_items.profit) as profit_amount,SUM(invoice_items.quantity) as invoice_quantity, invoice_items.customer_id, c.name as customer_name, items.name as item_name, invoice_items.item_id as item_id"))
                        ->groupby('invoice_items.item_id')->get();

        $credit_memo = CreditMemoItems::where('credit_memo_items.deleted', '0')
                ->where('credit_memo_items.customer_id', '!=', '0')
                ->leftjoin('credit_memo', 'credit_memo.id', '=', 'credit_memo_items.credit_memo_id')
                ->leftjoin('customers as c', 'c.id', '=', 'credit_memo_items.customer_id')
                ->leftjoin('items', 'items.id', '=', 'credit_memo_items.item_id')
                ->where('credit_memo.status', '!=', 'pending')
                ->where('credit_memo.deleted', '0');

        $credit_memo = $credit_memo->whereBetween('credit_memo.statement_date', [$start_date, $end_date]);

        $credit_memo = $credit_memo->select(DB::raw("SUM(credit_memo_items.total_price) as sale_amount,SUM(credit_memo_items.loss) as loss_amount,SUM(credit_memo_items.quantity) as credit_memo_quantity, credit_memo_items.customer_id, c.name as customer_name, items.name as item_name, credit_memo_items.item_id as item_id"))
                        ->groupby('credit_memo_items.item_id')->get();

        $new_credit_memo = [];
        $new_invoice = [];
        $model = [];

        foreach ($credit_memo as $memo) {
            $new_credit_memo[$memo->item_id] = $memo;
        }

        foreach ($invoices as $memo) {

            $new_invoice[$memo->item_id] = $memo;
        }

        $i = 0;
        foreach ($invoices as $invoice) {
            if (isset($new_credit_memo[$invoice->item_id])) {
                $invoice['credit_memo_loss'] = $new_credit_memo[$invoice->item_id]['loss_amount'];
                $invoice['credit_memo_sale'] = $new_credit_memo[$invoice->item_id]['sale_amount'];
                $invoice['credit_memo_quantity'] = $new_credit_memo[$invoice->item_id]['credit_memo_quantity'];

                unset($new_credit_memo[$invoice->item_id]);
            } else {

                $invoice['credit_memo_loss'] = '0';
                $invoice['credit_memo_sale'] = '0';
                $invoice['credit_memo_quantity'] = '0';
            }

            $model[$i] = $invoice;
            array_push($sold_items, $model[$i]);

            $i++;
        }

        if (count($new_credit_memo) > 0) {
            foreach ($new_credit_memo as $invoice) {

                $invoice['credit_memo_loss'] = $invoice->loss_amount;
                $invoice['credit_memo_sale'] = $invoice->sale_amount;
                $invoice['credit_memo_quantity'] = $invoice->credit_memo_quantity;

                $invoice['sale_amount'] = '0';
                $invoice['profit_amount'] = '0';
                $invoice['invoice_quantity'] = '0';

                $model[$i] = $invoice;

                array_push($sold_items, $model[$i]);
                $i++;
            }
        }

        $sold_items1 = [];

        foreach ($sold_items as $item) {

            $vendor_name = 'NOT FOUND';
            $vendor_id = '0';
            $inventory_count = 0;
            $rem_po_quantity = 0;
            $ro_item = RoItems::where('item_id', $item->item_id)->where('ro_items.deleted', 0)
                    ->leftjoin('vendors as v', 'v.id', '=', 'ro_items.vendor_id')
                    ->orderBy('ro_items.id', 'DESC')
                    ->select('v.*')
                    ->first();
            if (count($ro_item) > 0) {
                $vendor_name = $ro_item->name;
                $vendor_id = $ro_item->id;
            }


            $inventory = \App\Inventory::where('item_id', $item->item_id)->first();

            if (count($inventory) > 0)
                $inventory_count = $inventory->quantity + $inventory->package_quantity;

            $purchase_order = \App\PoItems::where('item_id', $item->item_id)
                            ->where('deleted', 0)->where('vendor_id', $vendor_id)
                            ->select(DB::raw("SUM(quantity) as quantity, SUM(delivered_quantity) as delivered_quantity"))
                            ->groupBy('item_id')->first();

            if (count($purchase_order) > 0)
                $rem_po_quantity = $purchase_order->quantity - $purchase_order->delivered_quantity;


            $item->vendor_name = $vendor_name;
            $item->vendor_id = $vendor_id;
            $item->inventory = $inventory_count;
            $item->rem_po_quantity = $rem_po_quantity;
            $sold_items1[] = $item;
        }

        //////////////////////////////////

        $sold_items2 = [];
        $name = 'vendor_id';
        usort($sold_items1, function ($a, $b) use (&$name) {
            return $a[$name] - $b[$name];
        });

        foreach ($sold_items1 as $item) {
            if (in_array($item->vendor_id, $request->vendor_id))
                $sold_items2[] = $item;
        }

        $array = $sold_items2;
        $selected_vendor = $request->vendor_id;

        if ($date != 'Select Date Range') {
            $date_info = ' from Date Range ' . $date . '.';
        }

        $print_page_info['top'] = 'Vendor Sale/Profit report ' . $date_info;
        return view('front.reports.customer_profit_loss-vendor-new1', compact('array', 'vendors', 'report_type', 'date', 'print_page_info', 'selected_vendor', 'selected_type','customers','items'));
    }

    public function reportGetcontent(Request $request){

        if ($request->type == 'customer') {
            $customers = Customers::where('deleted', '0')->get();
            $data[0]['id'] = '';
            $data[0]['name'] = 'Select Customer';
            foreach($customers as $row){
                $a = [];
                $y = $row->name;
                $remove[] = "'";
                $remove[] = '"';
                $remove[] = "-";
                $FileName = str_replace( $remove, "", $y );
                
                $a['id'] = $row->id;
                $a['name'] = $FileName;
                $data[] = $a;
            }
            $customers = $data;
            return $customers;
        }elseif ($request->type == 'invoice') {
            $invoices = Invoices::where('deleted', '0')->orderBy('id','DESC')->get();
           
            $data[0]['id'] = '';
            $data[0]['name'] = 'Select Invoice';
            foreach($invoices as $row){
                $a = [];
                $y = $row->id;
                $remove[] = "'";
                $remove[] = '"';
                $remove[] = "-";
                $FileName = str_replace( $remove, "", $y );
                
                $a['id'] = $row->id;
                $a['name'] = 'Invoice# '.$FileName;
                $data[] = $a;
            }
            $invoices = $data;
            return $invoices;

        }elseif ($request->type == 'items') {
            $items = Items::where('deleted', '0')->get();
            $data[0]['id'] = '';
            $data[0]['name'] = 'Select Item';
            foreach($items as $row){
                $a = [];
                $y = $row->name;
                $remove[] = "'";
                $remove[] = '"';
                $remove[] = "-";
                $FileName = str_replace( $remove, "", $y );
                
                $a['id'] = $row->id;
                $a['name'] = $FileName;
                $data[] = $a;
            }
            $items = $data;
            return $items;
        }





    }

}
