<?php

namespace App\Http\Controllers;

use DB;
use App\CustomerInventory;
use App\Items;
use App\Customers;
use App\Categories;
use App\Clients;
use App\Invoices;
use Auth;
use Session;
use Validator,
    Input,
    Redirect;
use App\ItemSerials;
use App\ItemSerialPackages;
use Illuminate\Http\Request;
use Excel;
use App\ItemPriceTemplates;
use App\PriceTemplates;
use App\CustomersItemImpact;

class CustomerInventoryController extends AdminController {

    private $user_id = '0';
    private $customer_id = '0';
    private $customer_obj = '0';

    public function __construct() {
        $this->middleware('auth');
        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $this->user_id = Auth::user()->id;
        $customer = Customers::where('user_id', $this->user_id)->first();
        $this->customer_id = $customer->id;
        $this->customer_obj = $customer;
    }

    public function index() {

        $model = CustomerInventory::where('customer_inventory.deleted', '=', '0')
                ->where('quantity', '!=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'customer_inventory.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('customer_inventory.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'i.image', 'i.upc_barcode as item_upc_barcode')
                ->get();

        $serial_inventory = ItemSerials::where('item_serials.deleted', '=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
                ->where('type', 'sold')
                ->where('customer_id', $this->customer_id)
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('item_serials.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'i.image', 'i.upc_barcode as item_upc_barcode', 'i.name as item_name', DB::raw('COUNT(item_serials.item_id) as quantity'))
                ->groupBy('item_serials.item_id')
                ->orderBy('id', 'desc')
                ->get();

        $finalResult = $model->merge($serial_inventory);
        $model = $finalResult;
        return view('front.customers.inventory.index', compact('model'));
    }

    public function generateCSVReport() {

        $result = CustomerInventory::where('customer_inventory.deleted', '=', '0')
                ->where('quantity', '!=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'customer_inventory.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('customer_inventory.id', 'i.name as item_name', 'c.name as category_name', 'i.code as item_sku', 'customer_inventory.quantity')
                ->get();


        $serial_inventory = ItemSerials::where('item_serials.deleted', '=', '0')
                ->leftjoin('items as i', 'i.id', '=', 'item_serials.item_id')
                ->where('type', 'sold')
                ->where('customer_id', $this->customer_id)
//                ->leftjoin('items as i','i.id','=','item_serials.item_id')
                ->leftjoin('categories as c', 'c.id', '=', 'i.category_id')
                ->select('item_serials.*', 'c.name as category_name', 'i.name as item_name', 'i.code as item_sku', 'i.name as item_name', DB::raw('COUNT(item_serials.item_id) as quantity'))
                ->groupBy('item_serials.item_id')
                ->orderBy('id', 'desc')
                ->get();

        $finalResult = $result->merge($serial_inventory);
        $result = $finalResult;

        $file_name = "customer_inventory_report.csv";
        return Excel::create($file_name, function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result) {
                        $sheet->fromArray($result);
                    });
                })->download('csv');
    }

    public function itemList() {

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        $model1 = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
//                ->leftjoin('customer_item_cost as cic', 'cic.item_id', '=', 'items.id')
//                ->where('cic.customer_id', $this->customer_id)
                ->where('ipt.price_template_id', $this->customer_obj->selling_price_template)
                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.selling_price as template_item_price')
                ->orderBy('items.id', 'desc')
                ->get();

        $ids = [];
        foreach ($model1 as $row) {
            $ids[] = $row->id;
        }

        $model2 = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $this->customer_obj->selling_price_template)
                ->whereIn('items.id', $my_items)
                ->whereNotIn('items.id', $ids)
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit', 'ipt.selling_price as template_item_price')
                ->orderBy('items.id', 'desc')
                ->get();
        $model = $model1->merge($model2);
        
        

        $categories = Categories::where('deleted', '=', '0')
                        ->lists('name', 'id')->toArray();
        $categories[0] = 'Select Category';
        $category_id = 0;
        
        $model_new = [];
        foreach ($model as $item) {
            $price = CommonController::getItemPrice($item->id, $this->customer_id, 'invoice');
            $item['cost'] = $item->template_item_price;
            if (isset($price['sale_price']) && count($price['sale_price']) > 0) {
                $cost = $price['sale_price'][0]['item_unit_price'];
                $item['cost'] = $cost;
            }
            $model_new[] = $item;
        }
        $customer_id=Auth::user()->id;
        // foreach ($model as $item) {
        // $getrecord=CustomersItemImpact::where(['customer_id'=>$customer_id,'item_id'=>$item->id])->first();
        // if(!empty($getrecord)){
        //   if($item->id==$getrecord->item_id){  
        // $item['increase_impact']=$getrecord->increase_impact;
        // $item['decrese_impact']=$getrecord->decrese_impact;
        //   }
        // }else{
        // $item['increase_impact']= 1;
        // $item['decrese_impact']=1;
        // }
        // $model_new[] = $item;
        // }
        // $model = $model_new;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = $customer[0]->sale_price_template;
// sohaib
        $tags = HomeController::getTags();
        return view('front.customers.inventory.item_list', compact('model', 'sale_price', 'category_id', 'categories','tags'));
    }
    public function getItemStatus($itemid) {
        $customer_id=Auth::user()->id;
        $getrecord = CustomersItemImpact::where(['customer_id' =>$customer_id,'item_id' => $itemid])->first();
        if(!empty($getrecord)){
        $item['increase_impact']=$getrecord->increase_impact;
        $item['decrese_impact']=$getrecord->decrese_impact;
        $item['id']=$itemid;
        }else{
        $item['increase_impact']= 1;
        $item['decrese_impact']=1;
        $item['id']=$itemid;
        }
        return view('front.customers.inventory.impactmodal', compact('item'));
    }
    public function itemstatus($status,$itemid,$type) {
    $customer_id=Auth::user()->id;
    $getitem = CustomersItemImpact::where(['customer_id' =>$customer_id,'item_id' => $itemid])->first();
    if(!empty($getitem)){
        if($type=="increase"){
            CustomersItemImpact::where(['customer_id' =>$customer_id,'item_id' => $itemid])->update([
            'increase_impact'=>$status
        ]);
        }else if($type=="decrease"){
            CustomersItemImpact::where(['customer_id' =>$customer_id,'item_id' => $itemid])->update([
                'decrese_impact'=>$status
            ]);
        }
    }else{
       
            $CustomersItemImpact=new CustomersItemImpact();
            $CustomersItemImpact->customer_id=$customer_id;
            $CustomersItemImpact->item_id= $itemid;
            if($type=="increase"){
            $CustomersItemImpact->increase_impact=$status;
            }else if($type=="decrease"){
            $CustomersItemImpact->decrese_impact=$status;
            }
            $CustomersItemImpact->save();
       
       
    }
    // $user = CustomersItemImpact::updateOrCreate(['customer_id' =>$customer_id,'item_id' => $itemid], [ 
    //     'increase_impact'=>$status
    // ]);
    // $user->save();
    }

    public function itemListTagSearch(Request $request){
        $item_ids = [];
        if (isset($request->subtag) || isset($request->tag)) {
               
            if (isset($request->subtag)) {
                    $relation = DB::table('tag_item_relation')->where('tag_id',$request->subtag)->get();
            }elseif (isset($request->tag)) {
                    $relation = DB::table('tag_item_relation')->where('tag_id',$request->tag)->get();
            }

            
            foreach($relation as $row){
                    $item_ids[] = $row->item_id;
            }
        }

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        $final = [];
        if (count($item_ids) > 0) {
            foreach($item_ids as $row){
                if (in_array($row,$my_items)) {
                    $final[] = $row;
                }
            }
        }
        $model1 = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
//                ->leftjoin('customer_item_cost as cic', 'cic.item_id', '=', 'items.id')
//                ->where('cic.customer_id', $this->customer_id)
                ->where('ipt.price_template_id', $this->customer_obj->selling_price_template)
                ->whereIn('items.id', $final)
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.selling_price as template_item_price')
                ->orderBy('items.id', 'desc')
                ->get();

        $ids = [];
        foreach ($model1 as $row) {
            $ids[] = $row->id;
        }

        $model2 = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $this->customer_obj->selling_price_template)
                ->whereIn('items.id', $final)
                ->whereNotIn('items.id', $ids)
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit', 'ipt.selling_price as template_item_price')
                ->orderBy('items.id', 'desc')
                ->get();
        $model = $model1->merge($model2);
        
        

        $categories = Categories::where('deleted', '=', '0')
                        ->lists('name', 'id')->toArray();
        $categories[0] = 'Select Category';
        $category_id = 0;
        
        $model_new = [];
        foreach ($model as $item) {
            $price = CommonController::getItemPrice($item->id, $this->customer_id, 'invoice');
            $item['cost'] = $item->template_item_price;
            if (isset($price['sale_price']) && count($price['sale_price']) > 0) {
                $cost = $price['sale_price'][0]['item_unit_price'];
                $item['cost'] = $cost;
            }
            $model_new[] = $item;
        }
        $customer_id=Auth::user()->id;
        foreach ($model as $item) {
        $getrecord=CustomersItemImpact::where(['customer_id'=>$customer_id,'item_id'=>$item->id])->first();
        if(!empty($getrecord)){
          if($item->id==$getrecord->item_id){  
        $item['increase_impact']=$getrecord->increase_impact;
        $item['decrese_impact']=$getrecord->decrese_impact;
          }
        }else{
        $item['increase_impact']= 1;
        $item['decrese_impact']=1;
        }
        $model_new[] = $item;
        }
        $model = $model_new;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = $customer[0]->sale_price_template;
// sohaib
        $tags = HomeController::getTags();
        return view('front.customers.inventory.item_list', compact('model', 'sale_price', 'category_id', 'categories','tags'));
    }
    public function itemListSearch(Request $request) {

        if ($request->category_id == '0' &&  $request->pricestatus=="")
            return redirect('/customer/items_list');

        $categories = Categories::where('deleted', '=', '0')
                        ->lists('name', 'id')->toArray();
        $categories[0] = 'Select Category';
        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        $model1 = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
//                ->leftjoin('customer_item_cost as cic', 'cic.item_id', '=', 'items.id')
//                ->where('cic.customer_id', $this->customer_id)
                ->where('ipt.price_template_id', $this->customer_obj->selling_price_template)
                ->whereIn('items.id', $my_items)
                ->where('items.category_id', $request->category_id)
                ->where('items.deleted', '0')
                // ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit', 'ipt.selling_price as template_item_price')
                ->orderBy('items.id', 'desc')
                ->get();

        $ids = [];
        foreach ($model1 as $row) {
            $ids[] = $row->id;
        }

        $model2 = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $this->customer_obj->selling_price_template)
                ->whereIn('items.id', $my_items)
                ->whereNotIn('items.id', $ids)
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit', 'ipt.selling_price as template_item_price')
                ->orderBy('items.id', 'desc')
                ->get();
        $model = $model1->merge($model2);

        $categories = Categories::where('deleted', '=', '0')
                        ->lists('name', 'id')->toArray();
        $categories[0] = 'Select Category';
        $category_id = 0;

        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = $customer[0]->sale_price_template;
        
         $model_new = [];
        foreach ($model as $item) {
            $price = CommonController::getItemPrice($item->id, $this->customer_id, 'invoice');
            $item['cost'] = $item->template_item_price;
            if (isset($price['sale_price']) && count($price['sale_price']) > 0) {
                $cost = $price['sale_price'][0]['item_unit_price'];
                $item['cost'] = $cost;
            }
            $model_new[] = $item;
        }
        $model_newitem="";
        $customer_id=Auth::user()->id;
        foreach ($model as $item) {
            if($request->pricestatus=="increase"){
        $getrecord=CustomersItemImpact::where(['customer_id'=>$customer_id,'item_id'=>$item->id,'increase_impact'=>1])->first();
            }
           if($request->pricestatus=="decrease"){
            $getrecord=CustomersItemImpact::where(['customer_id'=>$customer_id,'item_id'=>$item->id,'decrese_impact'=>1])->first();
           }
        if(!empty($getrecord)){
          if($item->id==$getrecord->item_id){  
        $item['increase_impact']=$getrecord->increase_impact;
        $item['decrese_impact']=$getrecord->decrese_impact;
          }
          $model_newitem[] = $item;
        }
        
       
        }
        // $model_newitem="";
        // foreach ($model_new as $item) {
        //     if($request->pricestatus=="increase"){
        //        if(isset($item->increase_impact) && $item->increase_impact==1){
        //         $model_newitem[] = $item;
        //        } 
        //     }
        // }
        if(isset($request->pricestatus) && $request->pricestatus!=""){
            $model = $model_newitem;
        }else{
            $model = $model_new;
        }
        $pricestatus=$request->pricestatus;
        $tags = HomeController::getTags();
        return view('front.customers.inventory.item_list', compact('sale_price', 'model', 'item_id', 'category_id','pricestatus', 'categories','tags'));
    }

    public function itemGallery() {

        $customer = Customers::where('user_id', $this->user_id)->get();
        $invoice_btn = $customer[0]->gallery_invoice_btn;
        $order_btn = $customer[0]->gallery_order_btn;
        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
                        ->lists('title', 'id')->toArray();

        if (count($templates) == 0)
            $templaye_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($this->customer_id);
        else
            $templaye_id = key($templates);

        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
                        ->get(); //->lists('title', 'id')->toArray();

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        $items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
                ->where('price_templates.deleted',0)
                ->where('price_templates.user_type','customer')
                ->where('ipt.price_template_id', $templaye_id)
                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.parent_item_id', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName')
                ->orderBy('items.id', 'desc');

        $drop_down_items = $items->get();
        // $items = $items->take(10)->get();
        $items = $items->paginate(15);

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id)
                        ->whereIn('items.id', $my_items)
                        ->where('items.deleted', '0')
                        ->where('items.parent_item_id', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
//                ->select('items.category_id', 'c.name as category_name')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->lists('c.name as category_name', 'items.category_id')->toArray();


//        $categories = Categories::where('deleted', '=', '0')->lists('name', 'id')->toArray();
        $categories[0] = 'Select Category';
        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item['id']] = $item['name'];
        }

        $items_list = $all_packages;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = $customer[0]->sale_price_template;
        $item_id = 0;
        $category_id = 0;
        
        $template_id = $templates[0]->id;
        $tags = HomeController::getTags();
        $clients = Clients::where('deleted', '=', '0')->where('customer_id', $this->customer_id)->get();
        $new_cat = [];
        $clientTemplate = [];
        $new_cat [0] = 'Select Client';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
            $clientTemplate[$cat->id] = $cat->selling_price_template;
        }

        $clients = $new_cat;
        // $templates
        $new_tmp = [];
        $new_tmp [0] = 'Select Template';
        foreach ($templates as $tmp) {
            $new_tmp[$tmp->id] = $tmp->title;
        }
        $templates = $new_tmp;
        
        return view('front.customers.inventory.item_gallery_listing', compact('items', 'sale_price', 'items_list', 'item_id', 'categories', 'category_id', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','clients','clientTemplate'));
    }

    public function itemGallerySearch(Request $request) {

        $customer = Customers::where('user_id', $this->user_id)->get();
//        if ($customer[0]->client_menu == 0)
//            Redirect::to('customer/dashboard')->send();

        $invoice_btn = $customer[0]->gallery_invoice_btn;
        $order_btn = $customer[0]->gallery_order_btn;

        if ($request->item_id == '0' && $request->category_id == '0' && $request->selling_price_template == '0')
            return redirect('/customer/item-gallery');

        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
                        ->get(); //->lists('title', 'id')->toArray();

        $templaye_id = $request->selling_price_template;

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id)
                        ->whereIn('items.id', $my_items)
                        ->where('items.parent_item_id', '0')
                        ->where('items.deleted', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
//                ->select('items.category_id', 'c.name as category_name')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->lists('c.name as category_name', 'items.category_id')->toArray();
        $categories[0] = 'Select Category';

//        $items_list = Items::where('items.deleted', '=', '0')
//                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
//                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
//                ->where('ipt.price_template_id', $this->customer_obj->selling_price_template)
//                ->whereIn('items.id', $my_items)
//                ->where('items.parent_item_id', '0')
//                ->where('items.deleted', '0')
////                        ->where('items.serial', 'no')
//                ->where('items.status', '1')
//                ->where('ipt.display', '1');
//
//        if ($request->category_id != '0')
//            $items_list = $items_list->where('category_id', $request->category_id);
//
//        $items_list = $items_list->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
//                        ->orderBy('items.id', 'desc')
//                        ->limit(200)->get();

        $items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $templaye_id)
                ->whereIn('items.id', $my_items);

        if ($request->item_id != '0' && $request->category_id == '0') {
            $items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
                    ->where('items.id', $request->item_id);
        } elseif ($request->item_id == '0' && $request->category_id != '0') {
//            $items = Items::where('category_id', $request->category_id)->get()->toArray();

            $items = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)
                            ->whereIn('items.id', $my_items)->where('category_id', $request->category_id);
        } elseif ($request->item_id != '0' && $request->category_id != '0') {


            $items = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)
                            ->where('items.id', $request->item_id)->where('category_id', $request->category_id);
        }

        if ($request->selling_price_template != '0') {
            $customer = Customers::where('id', $this->customer_id)->first();
            $my_items = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('display', '1')->lists('item_id')->toArray();

            $items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
//                    ->where('items.serial', 'no')
                    // ->where('items.parent_item_id', '0')
                    ->whereIn('items.id', $my_items);

            if ($request->category_id != '0')
                $items = $items->where('category_id', $request->category_id);

            if ($request->item_id != '0')
                $items = $items->where('items.id', $request->item_id);
        }

        $items = $items->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('items.parent_item_id', '0')
//                       ->where('items.serial', 'no')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                ->orderBy('items.id', 'desc');

        $items = $items->paginate(15);



        if ($request->category_id != '0') {
            $drop_down_items = Items::where('items.deleted', '=', '0')
                            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                            ->where('ipt.price_template_id', $templaye_id)
                            ->whereIn('items.id', $my_items)->where('category_id', $request->category_id);
        } else {

            $drop_down_items = Items::where('items.deleted', '=', '0')
                    ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                    ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                    ->where('ipt.price_template_id', $templaye_id)
                    ->whereIn('items.id', $my_items);
        }

        $drop_down_items = $drop_down_items->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('items.parent_item_id', '0')
//                       ->where('items.serial', 'no')
                ->where('ipt.display', '1')
                ->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit')
                ->orderBy('items.id', 'desc');


        $drop_down_items = $drop_down_items->get();
        $q = $templaye_id;
        $items->appends(['search' => $q]);

        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item->id] = $item->name;
        }

        $items_list = $all_packages;

        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = $customer[0]->sale_price_template;
        $item_id = $request->item_id;
        $category_id = $request->category_id;
        $template_id = $request->selling_price_template;

        $tags = HomeController::getTags();
        $clients = Clients::select(['id', 'name'])->where('deleted', '=', '0')->where('customer_id', $this->customer_id)->get();
        $new_cat = [];
        $new_cat [0] = 'Select Client';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
        }
        $clients = $new_cat;
        $new_tmp = [];
        $new_tmp [0] = 'Select Template';
        foreach ($templates as $tmp) {
            $new_tmp[$tmp->id] = $tmp->title;
        }
        $templates = $new_tmp;
        return view('front.customers.inventory.item_gallery_listing', compact('items', 'sale_price', 'items_list', 'item_id', 'category_id', 'categories', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','clients'));
    }

    public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    public function getTempleteCategories($template_id) {

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        $categories = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $template_id)
                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1')
//                ->select('items.category_id', 'c.name as category_name')
                ->orderBy('items.category_id', 'desc')
                ->groupBy('items.category_id')
                ->select('c.name as category_name', 'items.category_id as category_id')
                ->get();

        $new_cat = [];
        foreach ($categories as $po) {
            $new_cat[$po->category_id] = $po->category_name;
        }
        $categories = $new_cat;
        $data = view('front.common.ajax_gallery_cat', compact('categories'))->render();
        return $data;
    }

    public function getTempleteItems($template_id, $category_id) {

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();


        $drop_down_items = Items::where('items.deleted', '=', '0')
                ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                ->where('ipt.price_template_id', $template_id)
                ->whereIn('items.id', $my_items)
                ->where('items.deleted', '0')
                ->where('items.status', '1')
                ->where('ipt.display', '1');

        if ($category_id != '0')
            $drop_down_items = $drop_down_items->where('items.category_id', $category_id);

        $drop_down_items = $drop_down_items->select('items.*')
                ->orderBy('items.id', 'desc')
                ->get();

        $new_cat = [];
        foreach ($drop_down_items as $po) {
            $new_cat[$po->id] = $po->name;
        }
        $items = $new_cat;
        $data = view('front.common.ajax_gallery_items', compact('items'))->render();
        return $data;
    }

    public function itemGallerySearchNew(Request $request) {

        $item_ids = [];
        if (isset($request->subtag) || isset($request->tag)) {
            $subtagCheck = 0;
            if (isset($request->subtag)) {
                    $relation = DB::table('tag_item_relation')->where('tag_id',$request->subtag)->get();
                    $subtagCheck = 1;
            }elseif (isset($request->tag)) {
                    $relation = DB::table('tag_item_relation')->where('tag_id',$request->tag)->get();
            }            
            foreach($relation as $row){
                    $item_ids[] = $row->item_id;
            }
        }

        $customer = Customers::where('user_id', $this->user_id)->get();

        $invoice_btn = $customer[0]->gallery_invoice_btn;
        $order_btn = $customer[0]->gallery_order_btn;
        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
                        ->lists('title', 'id')->toArray();
            $client_id = 0;
        if (isset($request->client_id) && $request->client_id > 0) {
            $client_id = $request->client_id;
            $cli_data = Clients::where('id',$request->client_id)->first();
            $templaye_id = $cli_data->selling_price_template;
        }else if(isset($request->selling_price_template) && $request->selling_price_template > 0){
            $templaye_id = $request->selling_price_template;
        }else{
            if (count($templates) == 0){
                $templaye_id = CustomerItemPriceTemplateController::getCustomerDefaultTemplateId($this->customer_id);
            }else{
                $templaye_id = key($templates);
            }
        }

        $templates = PriceTemplates::where('deleted', '=', '0')
                        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
                        ->get(); //->lists('title', 'id')->toArray();

        $my_items = ItemPriceTemplates::where('price_template_id', $this->customer_obj->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
        
        $final = [];
        if (count($item_ids) > 0) {
            foreach($item_ids as $row){
                if (in_array($row,$my_items)) {
                    $final[] = $row;
                }
            }
        }
        if (isset($request->subtag) || isset($request->tag)) {
            return $this->getTagItemsSearch($request, $item_ids, $final, $templaye_id, $invoice_btn, $order_btn, $client_id, $my_items, $subtagCheck);
        }
        $items = Items::where('items.deleted', '=', '0')
        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
        ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
        ->where('price_templates.deleted',0)
        ->where('price_templates.user_type','customer')
        ->where('ipt.price_template_id',$templaye_id);
        $items = $items->whereIn('items.id', $my_items);
        $items = $items->where('items.deleted', '0');
        if ($request->item_name != '') {
            $items = $items->where('items.upc_barcode',$request->item_name);
        }

        $items = $items->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName')
                ->orderBy('items.id', 'desc');

        $drop_down_items = [];
        $items = $items->paginate(15);

        if(count($items) == 0){
            $items = Items::where('items.deleted', '=', '0')
            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
            ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
            ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
            ->where('price_templates.deleted',0)
            ->where('price_templates.user_type','customer')
            ->where('ipt.price_template_id', $templaye_id);
            if (count($final) > 0) {
                $items = $items->whereIn('items.id', $final);
            }else{
                $items = $items->whereIn('items.id', $my_items);
            }
            $items = $items->where('items.deleted', '0')
            ->where('items.parent_item_id', '0');
            // ->where('items.status', '1');
            // ->where('ipt.display', '1');

            if ($request->item_name != '') {
                $keyword = $request->item_name;
                $items = $items->where(function ($query) use($keyword) {
                    $query->where('items.name', 'like', '%' . $keyword . '%')
                        ->orWhere('items.upc_barcode', 'like', '%' . $keyword . '%');
                });
            }

            $items = $items->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName')
                    ->orderBy('items.id', 'desc');

            $drop_down_items = $items->get();
            $items = $items->paginate(15);
        }

        foreach($items as $item){
            if ($request->item_name != '' && strlen($request->item_name) > 2 && $client_id > 0) {
                $type = 'order';
                $old_item_price = CommonController::getItemPrice($item->id, $client_id, $type);
                if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                    $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                    $item->sale_price = $old_item_price;
                } 
            }
        }

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id);
                        if (count($final) > 0) {
                            $categories = $categories->whereIn('items.id', $final);
                        }else{
                            $categories = $categories->whereIn('items.id', $my_items);
                        }
                        $categories = $categories->where('items.deleted', '0')
                        ->where('items.parent_item_id', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->lists('c.name as category_name', 'items.category_id')->toArray();

        $categories[0] = 'Select Category';
        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item['id']] = $item['name'];
        }

        $items_list = $all_packages;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = $customer[0]->sale_price_template;
        $item_id = 0;
        $category_id = 0;
        $template_id = $templaye_id;
        $tags = HomeController::getTags();
        $clients = Clients::where('deleted', '=', '0')->where('customer_id', $this->customer_id)->get();
        $new_cat = [];
        
        $clientTemplate = [];
        $new_cat [0] = 'Select Client';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
            $clientTemplate[$cat->id] = $cat->selling_price_template;
        }
        $clients = $new_cat;
        $new_tmp = [];
        $new_tmp [0] = 'Select Template';
        foreach ($templates as $tmp) {
            $new_tmp[$tmp->id] = $tmp->title;
        }
        $templates = $new_tmp;
        return view('front.customers.inventory.item_gallery_listing', compact('items', 'sale_price', 'items_list', 'item_id', 'categories', 'category_id', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','clients','clientTemplate'));
    }

    public function getTagItemsSearch($request, $item_ids, $final, $templaye_id, $invoice_btn, $order_btn, $client_id, $my_items, $subtagCheck){
        $items = Items::where('items.deleted', '=', '0')->where('items.status', '=', '1')
        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
        ->leftjoin('price_templates', 'price_templates.id', '=', 'ipt.price_template_id')
        ->where('price_templates.deleted',0)
        ->where('price_templates.user_type','customer')
        ->where('ipt.price_template_id', $templaye_id);
        if (count($final) > 0) {
            $items = $items->whereIn('items.id', $final);
        }else{
            $items = $items->whereIn('items.id', $my_items);
        }
        $items = $items->where('items.deleted', '0');
        if ($subtagCheck == 1) {
            $items = $items->where('items.parent_item_id', '=', '0');
        }

        $items = $items->select('items.*', 'c.name as category_name', 'ipt.selling_price as sale_price', 'ipt.id as item_template_price_id', 'ipt.display as display', 'ipt.profit as profit','ipt.price_template_id as tmplt_id','price_templates.title as tmpltName')
                ->orderBy('items.id', 'desc');

        $drop_down_items = $items->get();
        $items = $items->paginate(15);
        foreach($items as $item){
            if ($request->item_name != '' && strlen($request->item_name) > 2 && $client_id > 0) {
                $type = 'order';
                $old_item_price = CommonController::getItemPrice($item->id, $client_id, $type);
                if ($old_item_price != '0' && $old_item_price['sale_price'][0]['item_unit_price'] != '0.00') {
                    $old_item_price = $old_item_price['sale_price'][0]['item_unit_price'];
                    $item->sale_price = $old_item_price;
                } 
            }
        }

        $categories = Items::where('items.deleted', '=', '0')
                        ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
                        ->leftjoin('item_price_templates as ipt', 'ipt.item_id', '=', 'items.id')
                        ->where('ipt.price_template_id', $templaye_id);
                        if (count($final) > 0) {
                            $categories = $categories->whereIn('items.id', $final);
                        }else{
                            $categories = $categories->whereIn('items.id', $my_items);
                        }
                        $categories = $categories->where('items.deleted', '0')
                        ->where('items.parent_item_id', '0')
                        ->where('items.status', '1')
                        ->where('ipt.display', '1')
                        ->orderBy('items.category_id', 'desc')
                        ->groupBy('items.category_id')
                        ->lists('c.name as category_name', 'items.category_id')->toArray();

        $categories[0] = 'Select Category';
        $all_packages[0] = 'All Items';
        foreach ($drop_down_items as $item) {
            $all_packages[$item['id']] = $item['name'];
        }

        $items_list = $all_packages;
        $customer = Customers::where('user_id', $this->user_id)->get();
        $sale_price = $customer[0]->sale_price_template;
        $item_id = 0;
        $category_id = 0;
        $template_id = $request->template;
        $client_id = $request->client_id;
        $item_name = $request->item_name;
        $tags = HomeController::getTags();
        $templates = PriceTemplates::where('deleted', '=', '0')
        ->where('user_type', 'customer')->where('created_by', $this->customer_id)
        ->get();
        $clients = Clients::select(['id', 'name'])->where('deleted', '=', '0')->where('customer_id', $this->customer_id)->get();
        $new_cat = [];
        $clientTemplate = [];
        $new_cat [0] = 'Select Client';
        foreach ($clients as $cat) {
            $new_cat[$cat->id] = $cat->name;
            $clientTemplate[$cat->id] = $cat->selling_price_template;
        }
        $clients = $new_cat;
        $new_tmp = [];
        $new_tmp [0] = 'Select Template';
        foreach ($templates as $tmp) {
            $new_tmp[$tmp->id] = $tmp->title;
        }
        $templates = $new_tmp;
        return view('front.customers.inventory.item_gallery_listing', compact('items', 'sale_price', 'items_list', 'item_id', 'categories', 'category_id', 'invoice_btn', 'order_btn', 'templates', 'template_id','tags','clients','item_name','template_id','client_id','clientTemplate'));
    }

}
