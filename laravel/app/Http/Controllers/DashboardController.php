<?php

namespace App\Http\Controllers;

use DB;
use App\Customers;
use App\Clients;
use App\Vendors;
use App\ItemPriceTemplates;
use App\Categories;
use App\PurchaseOrders;
use App\ReceiveOrders;
use App\Items;
use App\Invoices;
use App\InvoiceItems;
use App\CreditMemo;
use App\Payments;
use App\Orders;
use App\OrderItems;
use App\ReturnOrders;
use App\TagItemRelation;
use App\ItemPriceImpect;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Auth;
use Carbon\Carbon;
use Validator,
    Input,
    Redirect;

class DashboardController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Dashboard Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth');
    }

    public function testCurl() {

        $a = Functions::makeSerialCurlRequest('', '', '');
        print_r($a);
        die;
    }

    public function index() {

        if (Auth::user()->role_id == '4') {
            Redirect::to('customer/dashboard')->send();
            die;
        }

        if (Auth::user()->role_id != '2') {
            Redirect::to('customers')->send();
            die;
        }

        $vendor = Vendors::where('deleted', '=', '0')->count();
        $customer = Customers::where('deleted', '=', '0')->count();
        $customer_balance = Customers::where('deleted', '=', '0')->sum('balance');
        $vendor_balance = Vendors::where('deleted', '=', '0')->sum('balance');

        $po = PurchaseOrders::where('deleted', '=', '0')->where('status', '=', 'open')->count();
        $ro = ReceiveOrders::where('deleted', '=', '0')->count();
        $items = Items::where('deleted', '=', '0')->count();
        $invoice = Invoices::where('deleted', '=', '0')->count();
        $credit_memo = CreditMemo::where('deleted', '=', '0')->count();


        $pending_invoice = Invoices::where('invoices.deleted', '=', '0')->where('invoices.status', 'pending')->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                        ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id')
                        ->orderBy('invoices.id', 'desc')->get();

        $pending_credit_memo = CreditMemo::where('credit_memo.deleted', '=', '0')->where('credit_memo.status', 'pending')->leftjoin('customers as c', 'c.id', '=', 'credit_memo.customer_id')
                        ->select('credit_memo.*', 'c.name as customer_name', 'c.u_id as customer_u_id')
                        ->orderBy('credit_memo.id', 'desc')->get();
        $pending_payments = Payments::where('payments.deleted', '=', '0')->where('user_type', 'customer')->where('payment_type', 'direct')->where('payments.status', 'pending')
                ->leftjoin('customers as c', 'c.id', '=', 'payments.user_id')
                ->select('payments.*', 'c.name as user_name', 'c.id as user_id')
                ->orderBy('id', 'desc')
                ->get();

        $invoices_all = Invoices::where('invoices.deleted', '0')->where('invoices.status', '!=', 'pending')->where('invoices.customer_id', '!=', '0')
                ->select(DB::raw("SUM(invoices.total_price) as invoice_sale_amount,SUM(invoices.profit) as profit_amount"))
                ->first();


        $credit_memo_all = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                ->select(DB::raw("SUM(credit_memo.total_price) as credit_memo_sale_amount,SUM(credit_memo.loss) as loss_amount"))
                ->first();

        $invoice_today = Invoices::where('invoices.deleted', '0')->where('invoices.status', '!=', 'pending')->where('invoices.customer_id', '!=', '0')
                ->whereDate('invoices.statement_date', '=', Carbon::today()->toDateString())
                ->select(DB::raw("SUM(invoices.total_price) as invoice_sale_amount,SUM(invoices.profit) as profit_amount"))
                ->first();

        $credit_memo_today = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                ->whereDate('credit_memo.statement_date', '=', Carbon::today()->toDateString())
                ->select(DB::raw("SUM(credit_memo.total_price) as credit_memo_sale_amount,SUM(credit_memo.loss) as loss_amount"))
                ->first();

        $invoice_monthly = Invoices::where('invoices.deleted', '0')->where('invoices.status', '!=', 'pending')->where('invoices.customer_id', '!=', '0')
                ->whereMonth('invoices.statement_date', '=', Carbon::today()->month)
                ->select(DB::raw("SUM(invoices.total_price) as invoice_sale_amount,SUM(invoices.profit) as profit_amount"))
                ->first();

        $credit_memo_monthly = CreditMemo::where('credit_memo.deleted', '0')->where('credit_memo.status', '!=', 'pending')->where('credit_memo.customer_id', '!=', '0')
                ->whereMonth('credit_memo.statement_date', '=', Carbon::today()->month)
                ->select(DB::raw("SUM(credit_memo.total_price) as credit_memo_sale_amount,SUM(credit_memo.loss) as loss_amount"))
                ->first();

        $delivered_orders = Invoices::where('invoices.deleted', '=', '0')->where('invoices.status', 'approved')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                 ->leftjoin('orders', 'orders.id', '=', 'invoices.order_id')
                 ->leftjoin('clients as cl', 'cl.id', '=', 'orders.client_id')
                ->select('invoices.*', 'c.name as customer_name','c.city' ,'c.state','c.u_id as customer_u_id','cl.name as client_name','cl.city as cl_city','cl.state as cl_state')
                ->orderBy('invoices.id', 'desc')
                ->get();

        $zero_profit_invoice_ids = InvoiceItems::where('invoice_items.deleted', '0')
                ->where('invoice_items.customer_id', '!=', '0')
                ->where('invoice_items.profit', '=', '0.00')
                ->groupby('invoice_items.invoice_id')
                ->get();

        $ids = [];
        foreach ($zero_profit_invoice_ids as $row) {
            $ids[] = $row->invoice_id;
        }

        $zero_profit_invoice_count = Invoices::where('invoices.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                ->where('invoices.skip', 0)
                ->whereIn('invoices.id', $ids)
                ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                ->orderBy('id', 'desc')
                ->get();

        $from_perchase_zero_profit_invoice_ids = InvoiceItems::where('invoice_items.deleted', '0')
                ->where('invoice_items.customer_id', '!=', '0')
                ->where('invoice_items.purchase_order_id', 0)
                ->whereDate('invoice_items.created_at', '>', '2020-09-05 16:22:54')
                ->groupby('invoice_items.invoice_id')
                ->get();
        if (count($from_perchase_zero_profit_invoice_ids) > 0) {
            foreach ($from_perchase_zero_profit_invoice_ids as $invoice1) {
                InvoiceController::updateProfit($invoice1->invoice_id);
            }
        }
        $from_perchase_zero_profit_invoice_ids1 = InvoiceItems::where('invoice_items.deleted', '0')
                ->where('invoice_items.customer_id', '!=', '0')
                 ->leftjoin('items as i', 'i.id', '=', 'invoice_items.item_id')
                ->where('i.status', '1')
                ->where('i.deleted', '0')
                ->where('invoice_items.purchase_order_id', 0)
                ->whereDate('invoice_items.created_at', '>', '2020-09-05 16:22:54')
                ->groupby('invoice_items.item_id')
                ->get();

        $no_po_items = count($from_perchase_zero_profit_invoice_ids1);

        $idss = [];
        foreach ($from_perchase_zero_profit_invoice_ids as $row) {
            $idss[] = $row->invoice_id;
        }

        $from_perchase_zero_profit_invoice_count = Invoices::where('invoices.deleted', '=', '0')
                ->leftjoin('customers as c', 'c.id', '=', 'invoices.customer_id')
                ->leftjoin('orders as o', 'o.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('clients as cl', 'cl.id', '=', 'o.client_id')
                ->whereIn('invoices.id', $idss)
                ->where('invoices.skip', 0)
                ->select('invoices.*', 'c.name as customer_name', 'c.u_id as customer_u_id', 'o.id as order_refrence', 'cl.name as client_name')
                ->orderBy('id', 'desc')
                ->get();

        $total_profit = $invoices_all->profit_amount - $credit_memo_all->loss_amount;
        $total_sales = $invoices_all->invoice_sale_amount - $credit_memo_all->credit_memo_sale_amount;

        $daily_profit = $invoice_today->profit_amount - $credit_memo_today->loss_amount;
        $daily_sales = $invoice_today->invoice_sale_amount - $credit_memo_today->credit_memo_sale_amount;

        $month_profit = $invoice_monthly->profit_amount - $credit_memo_monthly->loss_amount;
        $month_sales = $invoice_monthly->invoice_sale_amount - $credit_memo_monthly->credit_memo_sale_amount;

        $tagrel = TagItemRelation::where('deleted',0)->groupby('item_id')->get();
        $ids = [];
        foreach ($tagrel as $row) {
            $ids[] = $row->item_id;
        }

        $itemcount = Items::where('items.deleted', '=', '0')
            ->where('items.parent_item_id', '=', '0')
            ->whereNotIn('items.id', $ids)
            ->leftjoin('categories as c', 'c.id', '=', 'items.category_id')
            ->leftjoin('api_servers as s', 's.id', '=', 'items.api')
            ->leftjoin('inventory as i', 'i.item_id', '=', 'items.id')
            ->select('items.*', 'i.quantity as inventory_quantity', 'i.package_quantity as package_quantity', 'c.name as category_name', 's.title as api_title')
            ->orderBy('items.id', 'desc')
            ->get();

        $priceImpect = ItemPriceImpect::where('deleted',0)->where('status','pending')->count();
        
       return view('front.customers.dashboard', compact('customer_balance', 'vendor_balance', 'vendor', 'customer', 'po', 'ro', 'items', 'invoice', 'credit_memo', 'pending_invoice', 'pending_payments', 'pending_credit_memo', 'daily_sales', 'daily_profit', 'month_sales', 'month_profit', 'total_profit', 'total_sales', 'delivered_orders', 'zero_profit_invoice_count', 'from_perchase_zero_profit_invoice_count', 'no_po_items','itemcount','priceImpect'));
    }

    public function customerDashboard() {

//         if(Auth::user()->role_id != '2'){
//            Redirect::to('customers')->send(); die;
//        }

        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $user_id = Auth::user()->id;
        if (Auth::user()->role_id != '4') {
            Redirect::to('dashboard')->send();
            die;
        }

        $customer = Customers::where('user_id', $user_id)->first();
        $invoice_count = Invoices::where('deleted', '=', '0')
                ->where('customer_id', $customer->id)
                ->count();

        $credit_memo_count = CreditMemo::where('deleted', '=', '0')
                ->where('customer_id', $customer->id)
                ->count();



        $my_items = ItemPriceTemplates::where('price_template_id', $customer->selling_price_template)->where('display', '1')->lists('item_id')->toArray();
//         print_r(count($my_items)); die;
        $item_count = count($my_items);

        $balance = Customers::where('user_id', $user_id)->select('balance')->get();

        $client_balance = Clients::where('customer_id', $customer->id)->sum('balance');

//         print_r($client_balance);die;


        $pending_orders = Orders::where('orders.deleted', '=', '0')->where('orders.status', 'pending')->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                        ->where('orders.created_by', $customer->id)
                        ->select('orders.*', 'c.name as client_name', 'c.u_id as client_id')
                        ->orderBy('orders.id', 'desc')->get();

        $pending_return_orders = ReturnOrders::where('return_orders.deleted', '=', '0')->where('return_orders.status', 'pending')->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                        ->where('return_orders.created_by', $customer->id)
                        ->select('return_orders.*', 'c.name as client_name', 'c.u_id as client_id')
                        ->orderBy('return_orders.id', 'desc')->get();

        $order_count = Orders::where('deleted', '=', '0')
                ->where('created_by', $customer->id)
                ->count();

        $return_order_count = ReturnOrders::where('deleted', '=', '0')
                ->where('created_by', $customer->id)
                ->count();

//        $customer = Customers::where('user_id', $user_id)->get();
        $sale_price = $customer->sale_price_template;


        $orders_all = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                ->where('orders.created_by', $customer->id)
                ->select(DB::raw("SUM(orders.total_price) as orders_sale_amount,SUM(orders.profit) as profit_amount"))
                ->first();

        $return_order_all = ReturnOrders::where('return_orders.deleted', '0')->where('return_orders.status', '!=', 'pending')->where('return_orders.client_id', '!=', '0')
                ->where('return_orders.created_by', $customer->id)
                ->select(DB::raw("SUM(return_orders.total_price) as return_orders_sale_amount,SUM(return_orders.loss) as loss_amount"))
                ->first();

        $orders_daily = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                ->where('orders.created_by', $customer->id)
                ->whereDate('orders.statement_date', '=', Carbon::today()->toDateString())
                ->select(DB::raw("SUM(orders.total_price) as orders_sale_amount,SUM(orders.profit) as profit_amount"))
                ->first();

        $return_order_daily = ReturnOrders::where('return_orders.deleted', '0')->where('return_orders.status', '!=', 'pending')->where('return_orders.client_id', '!=', '0')
                ->whereDate('return_orders.statement_date', '=', Carbon::today()->toDateString())
                ->where('return_orders.created_by', $customer->id)
                ->select(DB::raw("SUM(return_orders.total_price) as return_orders_sale_amount,SUM(return_orders.loss) as loss_amount"))
                ->first();

        $orders_monthly = Orders::where('orders.deleted', '0')->where('orders.status', '!=', 'pending')->where('orders.client_id', '!=', '0')
                ->where('orders.created_by', $customer->id)
                ->whereMonth('orders.statement_date', '=', Carbon::today()->month)
                ->select(DB::raw("SUM(orders.total_price) as orders_sale_amount,SUM(orders.profit) as profit_amount"))
                ->first();

        $return_order_monthly = ReturnOrders::where('return_orders.deleted', '0')->where('return_orders.status', '!=', 'pending')->where('return_orders.client_id', '!=', '0')
                ->whereMonth('return_orders.statement_date', '=', Carbon::today()->month)
                ->where('return_orders.created_by', $customer->id)
                ->select(DB::raw("SUM(return_orders.total_price) as return_orders_sale_amount,SUM(return_orders.loss) as loss_amount"))
                ->first();

        $total_profit = $orders_all->profit_amount - $return_order_all->loss_amount;
        $total_sales = $orders_all->orders_sale_amount - $return_order_all->return_orders_sale_amount;

        $daily_profit = $orders_daily->profit_amount - $return_order_daily->loss_amount;
        $daily_sales = $orders_daily->orders_sale_amount - $return_order_daily->return_orders_sale_amount;

        $month_profit = $orders_monthly->profit_amount - $return_order_monthly->loss_amount;
        $month_sales = $orders_monthly->orders_sale_amount - $return_order_monthly->return_orders_sale_amount;

        $approved_orders = Orders::where('orders.deleted', '0')->where('orders.status', '=', 'approved')->where('orders.client_id', '!=', '0')
                ->where('orders.created_by', $customer->id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->select('orders.*', 'c.name as client_name', 'c.u_id as client_id')
                ->get();

        $zero_profit_order_ids = OrderItems::where('order_items.deleted', '0')
                ->where('order_items.profit', '=', '0.00')
                ->groupby('order_items.order_id')
                ->get();
        $ids = [];
        foreach ($zero_profit_order_ids as $row) {
            $ids[] = $row->order_id;
        }
        $zero_profit_order_count = Orders::where('orders.deleted', '=', '0')
                ->where('orders.created_by', $customer->id)
                ->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                ->leftjoin('invoices', 'orders.invoice_refrence_id', '=', 'invoices.id')
                ->leftjoin('customers', 'customers.id', '=', 'invoices.customer_id')
                ->whereIn('orders.id', $ids)
                ->where('orders.skip', 0)
                ->orderBy('orders.id', 'desc')
                ->select('orders.*', 'c.name as client_name', 'invoices.status as invoice_status', 'invoices.id as invoice_refrence', 'customers.name as customer_name')
                ->get();

        return view('front.customers.customer_dashboard', compact('client_balance', 'invoice_count', 'balance', 'credit_memo_count', 'pending_orders', 'pending_return_orders', 'sale_price', 'item_count', 'order_count', 'return_order_count', 'daily_sales', 'daily_profit', 'month_sales', 'month_profit', 'total_sales', 'total_profit', 'approved_orders', 'zero_profit_order_count'));
    }

    public function customerManagerDashboard() {


        if (!isset(Auth::user()->id))
            Redirect::to('login')->send();

        $user_id = Auth::user()->id;
        if (Auth::user()->role_id != '4') {
            Redirect::to('dashboard')->send();
            die;
        }

        $customer = Customers::where('user_id', $user_id)->first();
        $invoice_count = Invoices::where('deleted', '=', '0')
                ->where('customer_id', $customer->id)
                ->count();

        $credit_memo_count = CreditMemo::where('deleted', '=', '0')
                ->where('customer_id', $customer->id)
                ->count();

        $item_count = Items::where('deleted', '=', '0')->count();

        $balance = Customers::where('user_id', $user_id)->select('balance')->get();

        $pending_orders = Orders::where('orders.deleted', '=', '0')->where('orders.status', 'pending')->leftjoin('clients as c', 'c.id', '=', 'orders.client_id')
                        ->where('orders.created_by', $customer->id)
                        ->select('orders.*', 'c.name as client_name', 'c.u_id as client_id')
                        ->orderBy('orders.id', 'desc')->get();

        $pending_return_orders = ReturnOrders::where('return_orders.deleted', '=', '0')->where('return_orders.status', 'pending')->leftjoin('clients as c', 'c.id', '=', 'return_orders.client_id')
                        ->select('return_orders.*', 'c.name as client_name', 'c.u_id as client_id')
                        ->orderBy('return_orders.id', 'desc')->get();

        $order_count = Orders::where('deleted', '=', '0')
                ->where('created_by', $customer->id)
                ->count();

        $return_order_count = ReturnOrders::where('deleted', '=', '0')
                ->where('created_by', $customer->id)
                ->count();

        $customer = Customers::where('user_id', $user_id)->get();
        $sale_price = $customer[0]->sale_price_template;


        return view('front.customers.customer_dashboard', compact('invoice_count', 'balance', 'credit_memo_count', 'pending_orders', 'pending_return_orders', 'sale_price', 'item_count', 'order_count', 'return_order_count'));
    }

    public function truncateTables() {

        print_r('a');
        die;
        DB::table('test_t')->truncate();
        DB::table('credit_memo')->truncate();
        DB::table('credit_memo_items')->truncate();
        DB::table('items')->truncate();

        DB::table('purchase_orders')->truncate();
        DB::table('po_items')->truncate();

        DB::table('receive_orders')->truncate();
        DB::table('ro_items')->truncate();

        DB::table('warehouse')->truncate();
        DB::table('warehouse_inventory')->truncate();

        DB::table('inventory')->truncate();
        DB::table('customer_inventory')->truncate();

        DB::table('invoices')->truncate();
        DB::table('invoice_items')->truncate();

        DB::table('packages')->truncate();
        DB::table('packages_items')->truncate();

        DB::table('item_serials')->truncate();
        DB::table('item_serial_packages')->truncate();

        DB::table('payments')->truncate();
        DB::table('vendor_ro_payments')->truncate();

        DB::table('test_t')->truncate();
        print_r($a);
        die;
    }

    public function testEmail() {

        phpinfo();
        die;
        $a = Functions::sendEmail('aims4aamir@yahoo.com', 'test', 'body');
        print_r($a);
        die;
    }

}
