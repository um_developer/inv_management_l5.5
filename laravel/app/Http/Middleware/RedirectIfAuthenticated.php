<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;

class RedirectIfAuthenticated {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
        
	public function handle($request, Closure $next)
	{
		if ($this->auth->check())
		{
//                    return new RedirectResponse(url('/home'));
                    
                    $role_id = Auth::user()->role_id;
                    if($role_id == '2')
			return new RedirectResponse(url('/'));
                    elseif($role_id == '3'){
                             return new RedirectResponse(url('/technician/home'));
                    }
                    elseif($role_id == '4'){
                         return new RedirectResponse(url('/customer/dashboard'));
                    }
		}

		return $next($request);
	}

}
