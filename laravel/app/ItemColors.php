<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemColors extends Model {
	//
    protected $table='item_colors';
    
    public function Items(){
        return $this->belongsTo('App\Items', 'item_id');
}

    public function Colors(){
        return $this->belongsTo('App\Colors', 'color_id', 'id');
}
    
}
