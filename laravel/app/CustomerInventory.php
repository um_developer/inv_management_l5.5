<?php

namespace App;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class CustomerInventory extends Model {

    protected $table = 'customer_inventory';

    public static function updateInventory($item_id, $quantity, $type = 'add', $customer_id) {
        $user_id = Auth::user()->id;
        $item = Self::where('item_id', $item_id)->get();

        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Self::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new CustomerInventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = $quantity;
                $Inventory->customer_id = $customer_id;
                $Inventory->created_by = $user_id;
                $Inventory->save();
            }
        } else {

            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity - $quantity;
                Self::where('item_id', '=', $item_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new CustomerInventory;
                $Inventory->item_id = $item_id;
                $Inventory->quantity = '-' . $quantity;
                $Inventory->customer_id = $customer_id;
                $Inventory->created_by = $user_id;
                $Inventory->save();
            }
        }
    }

}
