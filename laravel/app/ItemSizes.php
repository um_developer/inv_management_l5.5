<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSizes extends Model {
	//
    protected $table='item_sizes';
    
    public function Items(){
        return $this->belongsTo('App\Items', 'item_id');
}

    public function Sizes(){
        return $this->belongsTo('App\Sizes', 'size_id', 'id');
}
    
}
