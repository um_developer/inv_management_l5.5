<?php namespace App;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class WarehouseInventory extends Model {

      protected $table='warehouse_inventory';

      public static function updateInventory($warwhouse_id,$item_id, $quantity, $type = 'add') {
          $user_id = Auth::user()->id;
        $item = Self::where('item_id', $item_id)->where('warehouse_id',$warwhouse_id)->get();
        if ($type == 'add') {
            if (count($item) > 0) {
                $total_quantity = $item[0]->quantity + $quantity;
                Self::where('item_id', '=', $item_id)->where('warehouse_id',$warwhouse_id)->update([
                    'quantity' => $total_quantity,
                ]);
            } else {

                $Inventory = new WarehouseInventory;
                $Inventory->item_id = $item_id;
                $Inventory->warehouse_id = $warwhouse_id;
                $Inventory->quantity = $quantity;
                 $Inventory->created_by = $user_id;
                $Inventory->save();
            }
        } else {
            $total_quantity = $item[0]->quantity - $quantity;
            if ($total_quantity < 0) {
                return 'false';
            } else {
                Self::where('item_id', '=', $item_id)->where('warehouse_id',$warwhouse_id)->update([
                    'quantity' => $total_quantity,
                ]);
            }
        }
    }
      
      
}
