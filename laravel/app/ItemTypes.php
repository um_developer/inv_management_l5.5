<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTypes extends Model {
	//
    protected $table='item_types';
    
    public function Items(){
        return $this->belongsTo('App\Items', 'item_id');
}

    public function Types(){
        return $this->belongsTo('App\Types', 'type_id', 'id');
}
    
}
