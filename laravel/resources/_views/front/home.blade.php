@extends('front.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('front.commons.errors')
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <div class="form-group col-sm-12">
                        <input type="text" name="temp" id ="temp" class="form-control" value="" placeholder="What's In Your Mind?"/>
                    </div>
                </div>
            </div>
            @include('front.posts.create')
            @include('front.posts.index')
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        
    jQuery('.delete').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
    jQuery('.edit').click(function ()
    {
        $('#closemodal').attr('href', $(this).data('link'));
    });
    
    

        $('#temp').on('click', function () {
            $('#amoos').modal('show');
        });

        document.getElementById('amoos').addEventListener('click', function (e) {
            if (e.target.class != 'modal') {
                if (document.getElementById('description').value != "") {
                    document.getElementById('temp').value = document.getElementById('description').value;
                } else {
                    document.getElementById('temp').value = "";
                }
            }
        });
    }
</script>
@endsection
