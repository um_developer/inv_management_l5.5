<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="user_id" value="<?php echo Auth::user()->id ?>">

<div class="form-group col-sm-12">
    <input type="text" name="title" id ="title" class="form-control" value="{{old('title')}}" placeholder="Title"/>
</div>
<div class="form-group col-sm-12">
    <textarea type="text" name="description" id="description" class="form-control" style="min-width: 100%" rows="3" placeholder="Body" >
        {{ (isset($post))? $post->description: old('description')}}
    </textarea>
</div>
<br>
<!--<div class="form-group col-sm-12">
<input type="text" name="tags" class="form-control" data-role="tagsinput" placeholder="Tags" value="">
</div>-->
<button type="submit" class="btn btn-success btn-flat "> 
    Post
</button>
<button type="button" class="btn btn-danger btn-flat " class="close" data-dismiss="modal" onclick="this.form.reset()"> 
    Cancel
</button>
