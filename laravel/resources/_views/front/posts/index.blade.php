@if(!empty($posts))
@foreach($posts as $post)
<div class="panel panel-default">
    @if(isset(Auth::user()->id))
    @if($post->user_id ==Auth::user()->id)
    <button type="button" class="delete pull-right" data-toggle="modal" data-target="#myModal" data-link="post/delete/<?php echo $post->id ?>">
        &times;
    </button>
<!--    <button type="button" class="edit pull-right" href ="{{url("post/update/<?php echo $post->id ?>)}}">
        &times;
    </button>-->
    <a onclick="a(this)" id ='1'>Edit</a>
<script>
//        function a() {
//            $('#<?php echo $post->id; ?>').css('display', 'none');
//            $('#f<?php echo $post->id; ?>').css('display', 'block');
//        }
//        function a2() {
//            $('#<?php echo $post->id; ?>').css('display', 'block');
//            $('#f<?php echo $post->id; ?>').css('display', 'none');
//        }
function a(obj){
//    alert(obj.nextElementSibling.id);
    var post = obj.nextElementSibling;
    var edit = post.nextElementSibling;
    $('#'+post.id).css('display', 'none');
    $('#'+edit.id).css('display', 'block');
    
    
}
</script>
    @endif
    @endif
    <div id="{{$post->id}}">
        <div class="panel-heading">
            {{$post->title}}
        </div>
        <div class="panel-body">
            <?php echo $post->description ?>
        </div>
    </div>
    <div style="display: none" id="f{{$post->id}}">
        <form action="{{url('post/update/'.$post->id)}}" method="post">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="panel-heading">
                <input type="text" name="title" id ="title" value="{{$post->title}}" placeholder="Title"/>
            </div>
            <div class="panel-body">
                <input type="text" name="description" id ="description" value="{{$post->description}}"/>
            </div>
            <button type="submit"> 
                Update
            </button>
            <button type="button" onclick = location.reload()> 
                Cancel
            </button>
        </form>
    </div>
</div>
@include('front.commons.delete_modal')
@endforeach
{{ $posts->links() }}
@endif
