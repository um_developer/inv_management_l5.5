<div class="modal modal-vcntr fade bs-modal-sign-in" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" id="amoos">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
<!--                
                <form method="post" action="{{url('post/edit')}}" class="form">-->
                    
                {!! Form::model($post, ['class' => 'form','url' => ['post/update', $post->id], 'method' => 'post']) !!}
                    @include('front.posts.form')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
