<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title><?php echo Config('params.site_name'); ?></title>
        <meta name="description" content="<?php echo Config('params.meta_description'); ?>">
        <meta name="keywords" content="<?php echo Config('params.keywords'); ?>" />
        <link rel="icon" type="image/png" href="{{ asset('front/images/favicon.png')}}">

        <!-- Bootstrap --><link href="{{ asset('front/css/bootstrap.min.css')}}" rel="stylesheet">

<!--        <link rel="stylesheet" href="{{ asset('front/css/stylized.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/style.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/style-extra.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/colorized.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/animate.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/slidenav.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/swiper.min.css')}}">-->

        <!-- jQuery -->
        <!--[if (!IE)|(gt IE 8)]><!-->
        <script src="{{ asset('front/js/jquery-2.2.4.min.js')}}"></script>
        <!--<![endif]-->

        <script src="{{ asset('front/js/countries.js')}}"></script>
        <!--[if lte IE 8]>
          <script src="js/jquery1.9.1.min.js"></script>
        <![endif]-->

        <!--browser selector-->
        <script src="{{ asset('front/js/css_browser_selector.js')}}" type="text/javascript"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->


    </head>
    <body class="transition">

        <div id="body-wrapper" class="container">
            @include('front/common/header')

            <main id="page-content">
                @yield('content')

            </main>

            @include('front/common/footer')



        </div><!--body-wrapper-->

        <!--Bootstrap-->
        <script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
        <!--./Bootstrap-->

        <!--Major Scripts-->
        <script src="{{ asset('front/js/viewportchecker.js')}}"></script>
        <script src="{{ asset('front/js/kodeized.js')}}"></script>

        <script src="{{ asset('front/js/swiper.jquery.min.js')}}"></script>

        <script src="{{ asset('front/js/customized.js')}}"></script>



        <!--./Major Scripts-->



    </body>
</html>
