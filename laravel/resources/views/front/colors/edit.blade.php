@extends('customer_template_2')
@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Update Color</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'color/update', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
                    <label for="exampleInputEmail1">Title</label>
                    {!! Form::text('title', $model->title , array('placeholder'=>"Color Name *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Color</label>
                    <input type="text" required="required" name="code" type="text" class="form-control demo" value="{{ $model->code }}">
                </div>
            </div>

            <link rel="stylesheet" href="{{ asset('adminlte/dist/css/jquery.minicolors.css')}}"/>
             <script src="{{ asset('adminlte/dist/js/jquery.minicolors.min.js') }}"></script>


                <script type="text/javascript">

        $(document).ready(function () {

                $('.demo').each(function () {
                        $(this).minicolors({
                                control: $(this).attr('data-control') || 'hue',
                                defaultValue: $(this).attr('data-defaultValue') || '',
                                format: $(this).attr('data-format') || 'hex',
                                keywords: $(this).attr('data-keywords') || '',
                                inline: $(this).attr('data-inline') === 'true',
                                letterCase: $(this).attr('data-letterCase') || 'lowercase',
                                opacity: $(this).attr('data-opacity'),
                                position: $(this).attr('data-position') || 'bottom',
                                swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
                                change: function (value, opacity) {
                                        if (!value)
                    return;
                                        if (opacity)
                    value += ', ' + opacity;
                                        if (typeof console === 'object') {
                                                console.log(value);
                                        }
                                },
                                theme: 'bootstrap'
                        });

                });

        });


                </script>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>


</section>

@endsection