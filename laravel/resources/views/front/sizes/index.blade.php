@extends('customer_template')

@section('content')

<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Size Listing</h3>
                <a href="{{ url('size/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create New Size</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Size ID</th>
                            <th>Title</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($sizes) >0)
                        @foreach($sizes as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                              <td>
                                <a href="{{ url('size/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('size/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
        });
    });
</script>
@endsection



