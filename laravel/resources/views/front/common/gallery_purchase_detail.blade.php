<div class="modal modal--game-upload fade" id="galleryModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Item Details</strong></h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <div id="quan_div" class="quan_div">
                        Add Quantity
                        <input class="form-control" type="number" min="1" max="100"  value="" id="gallery_item_quantity" placeholder="0" maxlength="4" name="gallery_item_quantity" >
                    </div>
                    <input type="hidden" name="item_id" id="item_id" value=""/>
                    <input type="hidden" name="item_name" id="item_name" value="" />
                    <input type="hidden" name="item_quantity" id="item_quantity" value=""/>
                    <input type="hidden" name="type" id="type" value=""/>
                    <input type="hidden" name="gallery_item_price" id="gallery_item_price" value=""/>
 
                    <div class="table-responsive">
                        <input type="hidden" id="detail_row_id" value="" >
                        <table class="table table-striped" id="subitem_body">
                            <thead>
                            </thead>
                            <tbody></tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="updateQuantity()">Next</button>
                <button type="button" id="no_subitems" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
            </div>

        </div> <!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>

    $(document).on("click", ".open-AddBookDialog", function () {
        var item_id = $(this).data('id');
        var type = $(this).data('type');
        var gallery_item_price = $(this).data('price');

        $(".modal-body #item_id").val(item_id);
        $(".modal-body #type").val(type);
        $(".modal-body #gallery_item_price").val(gallery_item_price);

    });

    function updateQuantity() {

        updateNewSubitem();
        var item_id = $(".modal-body #item_id").val();
        var type = $(".modal-body #type").val();
        var quantity = $(".modal-body #gallery_item_quantity").val();
        var new_quantity = $(".modal-body #item_quantity").val();
        var gallery_item_price = $(".modal-body #gallery_item_price").val();
        if (gallery_item_price == '') {
            gallery_item_price = 0.00;
        }
        console.log(gallery_item_price);
// alert(gallery_item_price);
        if (new_quantity == '') {

            new_quantity = quantity;
        }

        if (type == 'invoice') {

            addToInvoice(item_id, new_quantity, gallery_item_price);
        } else {

            addToOrder(item_id, new_quantity, gallery_item_price);
        }
        $('#galleryModal').modal('toggle');
    }

    function subItemsModal(item_id) {

        $(subitem_body).find('tbody').html('');
        $(".modal-body #gallery_item_quantity").val('');
        $(".modal-body #item_quantity").val('');
        $(".modal-body #item_id").val(item_id);

        $.ajax({
            url: "<?php echo url('get/modal/sub-items'); ?>" + '/' + item_id,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                if (response != 0) {
                    $(subitem_body).find('tbody').html('');
                    $(subitem_body).find('tbody').append(response);

                    $("#quan_div").hide();
                } else {
//                    alert(response);
                    $("#quan_div").show();
                    $(subitem_body).find('tbody').html('');
                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });

    }

    function updateNewSubitem() {

        var f_item_id = '';
        var f_quantity = '';

        var item_id = $(".modal-body #item_id").val();
        var sub_items_count = document.getElementById('sub_item_count');

        if (sub_items_count != null) {
            sub_items_count = sub_items_count.value;

            if (sub_items_count > 0) {
                for (var i = 1; i <= sub_items_count; i++) {

                    var item_quantity = document.getElementById('sub_item_count_' + i).value;
                    var item_id = document.getElementById('sub_item_id_' + i).value;
                    var item_name = document.getElementById('sub_item_name_' + i).value;
                    if (item_quantity != '') {
                        if (f_item_id == '') {
                            f_item_id = item_id;
                        } else {
                            f_item_id = f_item_id + ',' + item_id;
                        }

                        if (f_quantity == '') {
                            f_quantity = item_quantity;
                        } else {
                            f_quantity = f_quantity + ',' + item_quantity;
                        }

                    }
                }
            }
        }

        $(".modal-body #item_quantity").val(f_quantity);
        if (f_item_id == '') {
            $(".modal-body #item_id").val(item_id);
        } else {
            $(".modal-body #item_id").val(f_item_id);
        }

    }
</script>