<div class="modal" id="modal_bundle1">
    <div class="modal-dialog" style="width: 95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross1bundle1"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Bundle Detail</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive col-md-12" style=" height: 400px;
                        display: inline-block;
                        overflow: auto; ">
                    <table class="table table-bordered table-striped" id="item_body">
                        <!-- <thead>
                            <tr>
                             <th>S#.</th> 
                                <th></th>
                                <th  style="width: 100px;"></th>
                                <th style="width: 100px;"></th>
                                <th style="width: 100px;"></th>
                            </tr>
                        </thead> -->
                        <tbody></tbody> 
                    </table>
                </div>
            </div>
            <div class="modal-footer">

                <div class="form-group">

                    <div class="col-sm-3">
                        <label for="exampleInputEmail1">  </label>
                    </div>

                    <div class="col-sm-4">
                        <label for="exampleInputEmail1"> Insert Bundle Quantity </label>
                    </div>

                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="Quentity" value="1" min="1" id="getQty1">
                    </div>
                </div>

                <button type="button" id="no1bundle1" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <a id="closemodal" class="btn btn-success pull-right" onclick="addBundle1()">Add Bundle</a>

            </div>
        </div>

    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</div>
<script type="text/javascript">

    $('#getQty1').on('keyup', function () {
        if (this.value <= 0) {
            document.getElementById('getQty1').value = 1;
        }
    });
    var cross = document.getElementById("cross1bundle1");
    var no = document.getElementById("no1bundle1");
    cross.onclick = function () {
        var modal = document.getElementById('modal_bundle1');
        modal.style.display = "none";
    }
    no.onclick = function () {
        var modal = document.getElementById('modal_bundle1');
        modal.style.display = "none";
    }


    function addBundle1() {

        var b_id = document.getElementById('bundle_id0').value;
        
        var customer_id = document.getElementById('customer').value;
        
        var type = 'invoice'
        // document.getElementById('type').value;
        
        var qty = document.getElementById('getQty1').value;
        // alert(qty);
        var j = 1;
        // document.getElementById('j').value;
        var items_id = clickArray
// alert(items_id)

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            url: "<?php echo url('invoice/insertEdit/bulkbundle/'); ?>",
            type: 'post',
            data: {items_id: items_id, j:j, qty:qty, customer_id:customer_id, type:type, bundle_id:b_id},
            success: function (response) {
                console.log('sohaib')
                if (response != 0) {
                    $(item_body).find('tbody').append(response);
                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });
        document.getElementById('getQty').value = 1;
        var modal = document.getElementById('modal_bundle1');
        modal.style.display = "none";
    }
</script>