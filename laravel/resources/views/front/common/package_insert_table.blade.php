<?php
$i = $j;
?>
@if(count($po_items)>0 && isset($po_items[0]))
@foreach($po_items as $items_1)
<?php
$delivered_items = $items_1->delivered_quantity;
$validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
?>
<tr id="item_{{ $i }}">
    <td><input class="form-control" type="text" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
        <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
        <input type="hidden" readonly="readonly" name="item_type_{{ $i }}" id="item_type_{{ $i }}"  value="">
        <input type="hidden" name="num[]" id="num[]">
        <input type="hidden" readonly="readonly" name="item_image_{{ $i }}" id="item_image_{{ $i }}">
    <input type="hidden" name="package_id" id="package_id" value="{{ $package_id }}">
        <input type="hidden" id="item_package_id_{{ $i }}" name="item_package_id_{{ $i }}" value="{{ $package_id }}"></td>
    
    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                            <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>

    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"readonly="readonly"  onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
    @if(Auth::user()->role->role == 'client')
    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="calculatePrice('{{ $i }}')" style="width: 150px;" onchange="updatePriceCheck('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
    @else
    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="calculatePrice('{{ $i }}')" style="width: 150px;" onchange="updatePriceCheck('{{ $i }}')" type="number"  readonly="readonly" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
    @endif

    <td><input class="form-control" min ="0" step="any" type="number" style="width: 150px;" name="total_{{ $i }}" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>
    <td><button onclick="showItemImage(this)" id="img_button_{{ $i }}" class="btn btn-success btn-sm" type="button"><i class="fa fa-image"></i></button><input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Package# {{ $package_id }}" onclick="deletePackageRow({{ $package_id }})"></td>

</tr>

<?php $i++; ?>
<script>
j = '{{ $i }}';
</script>
@endforeach

@endif
