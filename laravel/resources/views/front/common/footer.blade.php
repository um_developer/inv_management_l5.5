<footer>
    @if(1==2)
		<section class="ftr-area ftr--blind" id="footer">
			<div class="container">
			
				<div class="ftr__box col-sm-3 ftr__logo">
					<!--<h4><img src="{{ asset('front/images/ftr-logo.png')}}"></h4>-->
<!--					<div class="cont">
					Sample text
					</div>					-->
				</div>
				<div class="ftr__box ftr__nav col-sm-3 dotlist listview clrlist">
					<h4>SITEMAP</h4>
					<ul>
						<li><a href="{{ url('home') }}">Home</a></li>
						
					</ul>
				</div>
				
				<div class="ftr__box ftr__nav col-sm-3 dotlist listview clrlist">
					<h4>SUPPORT</h4>
					<ul>
						<li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                                                <li><a href="{{ url('terms-and-conditions') }}">Terms and Conditions</a></li>
                                               
						
					</ul>
				</div>
				
<!--				<div class="ftr__box ftr__nav col-sm-3 clrlist listview list-icon">
					<h4>CONTACT US</h4>
					<ul>
						<li><i class="fa icon"><img src="{{ asset('front/images/ftricon1.png')}}" alt="" /></i> <a href="#"><span>Abuja Office, 12 Sheik Ismail<br>Idris  Street, Near NNPC Mega<br>&nbsp; Station, First Avenue, Gwarinmpa,<br>&nbsp; Abuja, FCT</span></a></li>
						<li><i class="fa icon"><img src="{{ asset('front/images/ftricon2.png')}}" alt="" /></i> <a href="tel:23409099601329"><span>+234 (0) 90 99 60 1329</span></a></li>
						<li><i class="fa icon"><img src="{{ asset('front/images/ftricon3.png')}}" alt="" /></i> <a href="mailto:info@medicarehealthsystems.com"><span>info@medicarehealthsystems.com </span></a></li>
					</ul>
				</div>-->
		
			</div>
		</section>
		
		@endif
		<section class="bottom-area">
			<div class="container">
				
				<div class="fl"> &copy; Comhar Design. All rights reserved.</div>
				
				
			</div>
		</section>
		
		<a href="" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
		
		
	<script>
		 <?php
		 if($user = Auth::user())
		 { 
		   echo 'jQuery("body").addClass("login");';
		 }else{
		   echo 'jQuery("body").addClass("logout");';
		  }
		 ?>  

	</script>
</footer>
