<?php $i = 1; ?>
@if(count($po_items)>0)
@foreach($po_items as $items_1)
<?php 
$remaining_quantity = $items_1->quantity - $items_1->delivered_quantity; 
?>
@if($remaining_quantity != 0)
<tr>
    <td><input type="hidden" name="num[]" id="num[]"><input type="hidden" readonly="readonly" name="po_item_id_{{ $i }}" id="po_item_id_{{ $i }}"  value="{{$items_1->id}}">
        {{$items_1->item_name}}</td>
    <td>{{$remaining_quantity}}</td>
    <td>{{$items_1->item_unit_price}}</td>
    <td class="col-sm-2"><input class="form-control" onkeypress="calculatePrice('{{ $i }}')" onchange="calculatePrice('{{ $i }}')" type="number" step="0.01" name="quantity_{{ $i }}" min ="0" max ="{{ $remaining_quantity }}" id="quantity_{{ $i }}" value="0" ></td>
</tr>

<?php $i++; ?>
@endif
@endforeach
@endif
 <script type="text/javascript">
     var chk = 0;
      function calculatePrice(i) {
                    var quantity = parseFloat($("#quantity_" + i).val());
                    //chk = chk + quantity;
                    if(quantity >0){
                    $('.cr_rc_btn').prop('disabled', false);
                    }

                }
                
                </script>