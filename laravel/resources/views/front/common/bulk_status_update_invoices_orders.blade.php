     <div class="modal small fade" id="view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title text-Black">Add Comments</h4>
        </div>
        <form id="myform" method="post" action="{{ url('update/deliver/status') }}">

        <div class="modal-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" id="updateStatusDeliver">
            <input type="hidden" name="table" id="table1">
            <input type="hidden" name="location" id="location">
            <!--<label>Comments</label>-->
            <div class="form-group col-sm-12">
            
            <textarea rows="2" id="comments" cols="5" name="delivered_comments"  style="width: 100%;height: 100px;"></textarea>
        </div>
        </div>
        </form>

        <div class="modal-footer">
            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <input type="submit" form="myform" class="btn btn-primary pull-right" value="Update Status"/>
        </div>
      </div>
    </div>
</div>



    <div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Comments</h4>
      </div>
      <div class="modal-body">
        <p id="bookId"></p>
        <!-- <input type="text" name="bookId" value=""/> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- bulk -->
<div class="modal" id="BulkUpdateModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Bulk Price Update</h4>
            </div>
            <div class="modal-body">
                <p>
        <form id="myform1" method="post" action="{{ url('update/deliver/status') }}">
                    
                <div class="box-body">
                    <div class="row">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" id="updateStatusDeliver">
                            <input type="hidden" name="location" id="location1">
                            <input type="hidden" name="type" value="bulk">
                            <input type="hidden" name="table" id="table">
                        <div class="form-group col-sm-12">
                            Comments
                            <textarea class="form-control" rows="5" cols="5" name="delivered_comments" id="comments" required="required" style="width: 86%;height: 100px;"></textarea>
                            <input class="form-control" type="hidden" id="bulk_item_id" name="bulk_item_id" >
                        </div>
                        <div class="form-group col-sm-6">
                           <select id="status" name="status" class="form-control">
                                <option value="delivered">Delivered</option>
                                <option value="approved">Approved</option>
                            </select>
                        </div>

                    </div>
                </div>
            </form>

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="no" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <input type="submit" form="myform1" class="btn btn-primary pull-right" value="Update Status"/>
                <!-- <input type="submit" id="btnSubmit" class="btn btn-primary" name="submit" value="Update Selected Items"> -->

            </div>
        </div>

    </div>

</div>


<!-- bulk Print -->
<div class="modal" id="BulkPrintModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Bulk Price Update</h4>
            </div>
            <div class="modal-body">
                <p>
        <form id="myform2" method="get" action="{{ url('print/bulk') }}">
                    
                <div class="box-body">
                    <div class="row">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group col-sm-12">

                            <input class="form-control" type="hidden" id="bulk_print_item_id" name="bulk_item_id" >
                        </div>
                    </div>
                </div>
            </form>

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="no" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <input type="submit" form="myform2" class="btn btn-primary pull-right btn_print" value="Update Status"/>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('front/js/jquery.printPage.js')}}"></script>
            <script>
$(document).ready(function () {
$(".btn_print").printPage();
});
</script>
<script type="text/javascript">

        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });



    function showBulkUpdate(type) {
        
        if (type == 'invoicePrint' || type == 'orderPrint') {
            if (gameCheck()) {
                getPrintValueUsingClass(type);
            }
        }else if (type == 2) {
            if (gameCheck()) {
                $('#BulkUpdateModal').modal('show');
                getValueUsingClass();
            }
        }
    }

    $(document).ready(function () {

        $("#bulk_submit").submit(function (e) {
            $("#btnSubmit").attr("disabled", true);
            return true;

        });
    });
    
    function getValueUsingClass(){
        // var chkArray = [];
        // $(".chk:checked").each(function() {
        // chkArray.push($(this).val());
        // });
        var chkArray = $('#selectedids').val();
        var selected;
        selected = chkArray;
        // selected = chkArray.join(',');
        if (selected.length > 0){
            $('#bulk_item_id').val(selected);
        } else{
            alert("Please at least check one of the checkbox"); 
        }
    }

    function getPrintValueUsingClass(type){

        var chkArray = $('#selectedids').val();
        // $(".chk:checked").each(function() {
        // chkArray.push($(this).val());
        // });

        if (type == 'invoicePrint') {
            if (chkArray.length > 0){
                var url = "{{ url('print/bulk/') }}" +'/'+  chkArray
                $('#phiddenbtn').attr({ href: url });
                document.getElementById('phiddenbtn').click();
            } else{
                 alert("Please at least check one of the checkbox"); 
            }
        }else if (type == 'orderPrint') {
            if (chkArray.length > 0){
                var url = "{{ url('customer/print/bulk/orders/') }}" +'/'+  chkArray
                $('#phiddenbtn').attr({ href: url });
                document.getElementById('phiddenbtn').click();
            } else{
                 alert("Please at least check one of the checkbox"); 
            }
        }
        
    }

</script>