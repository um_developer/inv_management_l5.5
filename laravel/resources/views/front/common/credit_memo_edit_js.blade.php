<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
@include('front/common/suggestion_box')
<script type="text/javascript">
var loc = window.location.href;
var is_order = loc.includes("order-return");
$(function () {

//            Bootstrap DateTimePicker v4
$('#datetimepicker4').datetimepicker({
format: 'MM/DD/YYYY'
        });
});
var user_type = "{{ Auth::user()->role->role }}";
var j = {{ count($credit_memo_items) + 1 }}
$(document).ready(function(){
//    InsertNewRow();
var my_date = "{{ $model->statement_date }}";
$('#datetimepicker4').data("DateTimePicker").date(moment(my_date).format('MM/DD/YYYY'));
calculatePrice(1);
});
function updateQuantityStatus(i){
$("#quantity_" + i).prop("readonly", true);
$("#quantity_" + i).val('0');
}

function updatePriceCheck(i){
var cost = parseFloat($("#price_" + i).attr('min'));
var item_row = 'price_' + i;
document.getElementById(item_row).style.background = '#FFFFFF';
if (parseFloat($("#price_" + i).val()) >= cost) {
calculatePrice(i);
} else {
calculatePrice(i);
document.getElementById(item_row).style.background = '#F08080';
//    alert('Sale price should not be less than item cost.');
//    $("#price_" + i).val(cost);
}
}

function calculatePrice(i){

var price = ($("#price_" + i).val());
var quantity = parseFloat($("#quantity_" + i).val());
var grand_total = 0;
if (quantity == '' || quantity == '0'){
$("#total_" + i).val('0');
} else{
var total = price * quantity;
total = parseFloat(total).toFixed(2);
$("#total_" + i).val(total);
}

var my_data_table = document.getElementById("item_body");
var k = my_data_table.rows.length - 2;
for (var a = 1; a <= k; a++) {
grand_total = grand_total + parseFloat($("#total_" + a).val());
}
grand_total = parseFloat(grand_total).toFixed(2);
$('#footer_total').html(grand_total);
}

function deleteRow(r, a = 0) {

var i = r.parentNode.parentNode.rowIndex;
if (a != '0'){

$("#quantity_" + i).val(0);
$("#total_" + i).val(0);
$("#quantity_" + i).prop("readonly", true);
var item_row = "item_" + i;
document.getElementById(item_row).style.display = 'none';
}
else{

$("#quantity_" + i).val(0);
$("#total_" + i).val(0);
$("#quantity_" + i).prop("readonly", true);
var item_row = "item_" + i;
document.getElementById(item_row).style.display = 'none';
//    document.getElementById("item_body").deleteRow(i);
}
}

function getDatableData(){
var aTable = $('#item_body').DataTable();
var w = aTable.cell(0, 2).nodes().to$().find('input').val()
        alert(w);
var x = document.getElementById("item_body").rows[1].cells[4];
console.log(x);
alert(x);
alert(document.getElementById("item_body").rows[0].cells.innerHTML);
}

function InsertNewRow() {

var i = window.j;
if (user_type == 'client') {
var scntDiv = '<tr id = item_' + i + '><td><input type="text" title= "" name="item_name_' + i + '" class="form-control" id="item_name_' + i + '"  autocomplete="on" placeholder="Enter item name"><input type="hidden" readonly="readonly" name="item_id_' + i + '" id="item_id_' + i + '"><input type="hidden" readonly="readonly" name="item_type_' + i + '" id="item_type_' + i + '"><input type="hidden" name="num[]" id="num[]"></td><td><input type="number" min ="0" name="start_serial_number_' + i + '" class="form-control" readonly="readonly" id="start_serial_number_' + i + '"></td><td><input type="number" min ="0" readonly="readonly" name="end_serial_number_' + i + '" class="form-control" id="end_serial_number_' + i + '"></td><td><input type="number" name="quantity_' + i + '" class="form-control" min ="0" readonly="readonly" id="quantity_' + i + '"></td><td><input type="number" min ="0" name="price_' + i + '" class="form-control" style="width: 150px;" readonly="readonly" step=any id="price_' + i + '"></td><td><input type="text" name="total_' + i + '" class="form-control" min ="0" readonly="readonly"  style="width: 150px;" id="total_' + i + '"></td><td><input type="button" class="btn btn-danger btn-sm" id="delete_btn_' + i + '" name="delete_btn_' + i + '" value="Delete" onclick="deleteRow(this)"></td></tr>';
} else {
var scntDiv = '<tr id = item_' + i + '><td><input type="text" title= "" name="item_name_' + i + '" class="form-control" id="item_name_' + i + '"  autocomplete="on" placeholder="Enter item name"><input type="hidden" readonly="readonly" name="item_id_' + i + '" id="item_id_' + i + '"><input type="hidden" readonly="readonly" name="item_type_' + i + '" id="item_type_' + i + '"><input type="hidden" name="num[]" id="num[]"></td><td><input type="number" min ="0" name="start_serial_number_' + i + '" class="form-control" readonly="readonly" id="start_serial_number_' + i + '"></td><td><input type="number" min ="0" readonly="readonly" name="end_serial_number_' + i + '" class="form-control" id="end_serial_number_' + i + '"></td><td><input type="number" name="quantity_' + i + '" class="form-control" min ="0" readonly="readonly" id="quantity_' + i + '"></td><td><input type="number" min ="0" name="price_' + i + '" readonly="readonly" class="form-control" style="width: 150px;" readonly="readonly"  step=any id="price_' + i + '"></td><td><input type="text" name="total_' + i + '" class="form-control" min ="0" readonly="readonly"  style="width: 150px;" id="total_' + i + '"></td><td><input type="button" class="btn btn-danger btn-sm" id="delete_btn_' + i + '" name="delete_btn_' + i + '" value="Delete" onclick="deleteRow(this)"></td></tr>';
}
var item_array = <?php echo json_encode($items, JSON_PRETTY_PRINT) ?>;
$("#item_body").append(scntDiv);
$(function ($) {
$('#item_name_' + i).autocomplete({
source: item_array,
        select: function (event, ui) {

        $("#item_id_" + i).val(ui.item.id);
//        $("#price_" + i).val(ui.item.sale_price);
        $("#quantity_" + i).val('1');
        $("#start_serial_number_" + i).val('0'); // save selected id to hidden input    
        $("#end_serial_number_" + i).val('0'); // save selected id to hidden input    
        $("#item_type_" + i).val(''); // save selected id to hidden input  
        $("#price_" + i).prop("min", ui.item.cost);
        document.getElementById("item_name_" + i).title = ui.item.name;
        if (ui.item.serial == 'yes') {
        $("#start_serial_number_" + i).val('0'); // save selected id to hidden input    
        $("#end_serial_number_" + i).val('0'); // save selected id to hidden input    
        $("#start_serial_number_" + i).prop("readonly", false);
        $("#end_serial_number_" + i).prop("readonly", false);
        $("#quantity_" + i).val('0');
        $("#quantity_" + i).prop("readonly", true);
        if (user_type == 'client') {
        $("#price_" + i).prop("readonly", false);
        } else {
        $("#price_" + i).prop("readonly", true);
        }

        $("#item_type_" + i).val('serial');
        } else {
        if (ui.item.has_sub_item != '0') {
        subItemsModal(i);
        }
        if (user_type == 'client') {
        $("#price_" + i).prop("readonly", false);
        } else {
        $("#price_" + i).prop("readonly", true);
        }
        $("#quantity_" + i).prop("readonly", false);
        $("#start_serial_number_" + i).prop("readonly", true);
        $("#end_serial_number_" + i).prop("readonly", true);
        }
        $("#num[0]").val(ui.item.id);
        calculatePrice(i);
        getItemPrice(i, ui.item.sale_price);
        }
});
$("#price_" + i).on('blur', function () {

var cost = parseFloat($("#price_" + i).attr('min'));
var item_row = 'price_' + i;
document.getElementById(item_row).style.background = '#FFFFFF';
if (parseFloat($("#price_" + i).val()) >= cost) {
calculatePrice(i);
} else {
//                alert('Sale price should not be less than item cost.');
document.getElementById(item_row).style.background = '#F08080';
}

});
//        $("#end_serial_number_" + i).on('blur', function (e) {
//
//            var end_serial_num = parseInt($("#end_serial_number_" + i).val());
//            var start_serial_num = parseInt($("#start_serial_number_" + i).val());
//
//            if (end_serial_num >= start_serial_num) {
//                quantity = end_serial_num - start_serial_num;
//                quantity = quantity + 1;
//                $("#quantity_" + i).val(quantity);
//
//                if (e.keyCode != '9') {
//                    calculatePrice(i);
//                }
//            } else {
//                alert('End serail number should be greater than start serail number.')
//            }
//        });

$("#price_" + i).on('keyup change ', function (e) {

if (e.keyCode == '9') {
var price = $("#price_" + i).val();
getItemPriceHTML(i, price);
}
});
$("#total_" + i).on('keyup change ', function (e) {

if (e.keyCode == '9') {
var suggestion_box = document.getElementById("suggestion_box");
suggestion_box.style.display = "none";
}
});
$("#end_serial_number_" + i).on('blur', function (e) {

var end_serial_num = parseInt($("#end_serial_number_" + i).val());
var start_serial_num = parseInt($("#start_serial_number_" + i).val());
 if ($("#start_serial_number_" + i).val() != 0) {
if (end_serial_num >= start_serial_num) {
quantity = end_serial_num - start_serial_num;
quantity = quantity + 1;
$("#quantity_" + i).val(quantity);
if (e.keyCode != '9') {
calculatePrice(i);
}
} else {

alert('End serail number should be greater than start serail number.')
        }
    }
});
$("#item_name_" + i).on('blur', function () {
if ($("#item_id_" + i).val() == '' || $("#item_name_" + i).val() == '') {
//                document.getElementById("item_body").deleteRow(i);
$("#item_name_" + i).val('');
$("#item_id_" + i).val('');
$("#quantity_" + i).val('');
$("#price_" + i).val('');
$("#total_" + i).val('');
//                InsertNewRow();
//                var modal = document.getElementById('myModal');
//                modal.style.display = "block";
}
});
$("#quantity_" + i).on('blur', function (e) {
if (e.keyCode != '9' && $("#item_type_" + i).val() != 'serial') {
$("#end_serial_number_" + i).prop("readonly", true);
if ($("#quantity_" + i).val() == '' && $("#item_name_" + i).val() != '' || $("#quantity_" + i).val() == '0' && $("#item_name_" + i).val() != '') {
alert('Enter valid quantity');
$("#quantity_" + i).val('1');
}
}
});
$("#quantity_" + i).on('keyup change', function (e) {
if (e.keyCode != '9' && $("#item_type_" + i).val() != 'serial') {

$("#end_serial_number_" + i).prop("readonly", true);
if ($("#start_serial_number_" + i).val() != 0) {
var end_serial = '';
end_serial = parseInt($("#start_serial_number_" + i).val()) + (parseInt($("#quantity_" + i).val() - 1));
$("#end_serial_number_" + i).val(end_serial);
}

if ($("#quantity_" + i).val() == '' || $("#quantity_" + i).val() == '0') {
//                    alert('1');
//                    $("#quantity" + i).val('1');

} else {

calculatePrice(i);
}

}
});
$("#delete_btn_" + i).on('keyup change', function (e) {
if (e.keyCode == '9') {
var my_data_table = document.getElementById("item_body");
var i = my_data_table.rows.length - 1;
var item_type = $("#item_type_" + i).val();
if (item_type == 'serial') {
validateSerialItem();
} else {
InsertNewRow();
}
}
});
});
window.j++;
return false;
}

$('#add_more').on('click', function () {

var suggestion_box = document.getElementById("suggestion_box");
suggestion_box.style.display = "none";
var my_data_table = document.getElementById("item_body");
var i = my_data_table.rows.length - 1;
var item_type = $("#item_type_" + i).val();
InsertNewRow();
//    if (item_type == 'serial') {
//    validateSerialItem();
//    } else {
//    InsertNewRow();
//    }
});
$(function () {
$('#item_body').DataTable({
"paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false
        });
});
function validateSerialItem(type = 'new') {

$('#submit_btn').prop('disabled', true);
var my_data_table = document.getElementById("item_body");
var i = my_data_table.rows.length - 1;
var item_type = $("#item_type_" + i);
$("#invoice_form").submit();
}


function getItemPrice(i, price) {

var item_id = $("#item_id_" + i).val();
var customer_id = $("#client_id").val();

if (!customer_id) {
customer_id = "{{ $model->customer_id }}";
}
if (!customer_id) {
customer_id = $("#client_id2").val();
}

//alert(customer_id);

var type = 'credit_memo';
if (is_order) {
type = 'order_return';
}
my_url = "<?php echo url('get-item-price/'); ?>" + '/' + item_id + '/' + customer_id + '/' + type;
$.ajax({
url: my_url,
        type: 'get',
//            dataType: 'html',
        success: function (response) {

        if (response != '0') {
        $("#price_" + i).val(response['sale_price'][0]['item_unit_price']);
        } else {
        $("#price_" + i).val(price);
        }
        calculatePrice(i);
        },
        error: function (xhr, status, response) {
        alert(response);
        }
});
}

function getItemPriceHTML(i, price) {

hideSuggestionPopup();
var item_id = $("#item_id_" + i).val();
var customer_id = $("#client_id").val();
if (!customer_id) {
customer_id = "{{ $model->customer_id }}";
}
if (!customer_id) {
customer_id = $("#client_id2").val();
}

var type = 'credit_memo';
if (is_order) {
type = 'order_return';
}
my_url = "<?php echo url('get-item-price-html/'); ?>" + '/' + item_id + '/' + customer_id + '/' + type;
$.ajax({
url: my_url,
        type: 'get',
        dataType: 'html',
        success: function (response) {

        if (response != '0') {
        var suggestion_box = document.getElementById("suggestion_box");
        suggestion_box.style.display = "block";
        var suggestion_box_text = document.getElementById("suggestion_box_text");
        suggestion_box_text.innerHTML = response;
//                alert(response);
        }
        },
        error: function (xhr, status, response) {
        alert(response);
        }
});
}

function showSuggestionPopup(i){
updatePriceCheck(i);
var keycode = (window.event) ? event.keyCode : e.keyCode;
if (keycode == '9') {
getItemPriceHTML(i);
}

}

function hideSuggestionPopup(){

var suggestion_box = document.getElementById("suggestion_box");
suggestion_box.style.display = "none";
}

</script>