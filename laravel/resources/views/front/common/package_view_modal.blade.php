 <div class="modal" id="modal_package">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button"  id="cross1"  class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Package Detail</h4>
                </div>
                <div class="modal-body">
                     <div class="table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        <tr>
            <!--              <th>S#.</th>-->
                            <th>Item Name</th>
                            <th  style="width: 100px;">QTY</th>
                            <th style="width: 100px;">Unit Price</th>
                            <th style="width: 100px;">Total</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="no1" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <a id="closemodal" class="btn btn-success" onclick="addPackage()">Add Package</a>
                  
                </div>
            </div>

        </div>

    </div>
 <script type="text/javascript">
        var cross = document.getElementById("cross1");
        var no = document.getElementById("no1");
        cross.onclick = function () {
            var modal = document.getElementById('modal_package');
            modal.style.display = "none";
        }
        no.onclick = function () {
            var modal = document.getElementById('modal_package');
            modal.style.display = "none";
        }
        
        
        function addPackage(){
            
            var package_id = document.getElementById('package_id_insert').value;
            var j = document.getElementById('j').value;
            
//            var f = "{{ \Session::get('invoice_22') }}";
//            console.log(f);
//            console.log(j);
           
            $.ajax({
            url: "<?php echo url('invoice/insert/bulk/'); ?>" + '/' + package_id +'/' +j,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                if (response != 0) {
                   $(item_body).find('tbody').append(response);
//                    j++;
                } 
//                
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });
        
         var modal = document.getElementById('modal_package');
             modal.style.display = "none";
            
            
        }
        </script>