 <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button"  id="cross"  class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Item not found</h4>
                </div>
                <div class="modal-body">
                    <p>This item is not find in your item list. 
    Would you like to add it now? If not, go back and select any existing item.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="no" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                     <a id="closemodal" class="btn btn-primary" href="{{ url('item/create/') }}">Create Item</a>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>

        </div>

    </div>
 <script type="text/javascript">
        var cross = document.getElementById("cross");
        var no = document.getElementById("no");
        cross.onclick = function () {
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }
        no.onclick = function () {
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }
        
        </script>