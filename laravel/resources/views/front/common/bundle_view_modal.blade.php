<div class="modal" id="modal_bundle">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross1bundle"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Bundle Detail</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style=" height: 400px;
    display: inline-block;
    overflow: auto; ">
                    <table class="table table-bordered table-striped" id="item_body">
                        <thead>
                            <tr>
                <!--              <th>S#.</th>-->
                                <th>Item Name</th>
                                <th  style="width: 100px;">QTY</th>
                                <th style="width: 100px;">Unit Price</th>
                                <th style="width: 100px;">Total</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer">

                <div class="form-group">

                    <div class="col-sm-3">
                        <label for="exampleInputEmail1">  </label>
                    </div>

                    <div class="col-sm-4">
                        <label for="exampleInputEmail1"> Insert Bundle Quantity </label>
                    </div>

                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="Quentity" value="1" min="1" id="getQty">
                    </div>
                </div>

                <button type="button" id="no1bundle" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <a id="closemodal" class="btn btn-success pull-right" onclick="addBundle()">Add Bundle</a>

            </div>
        </div>

    </div>

</div>
<script type="text/javascript">
    $('#getQty').on('keyup', function () {
        if (this.value <= 0) {
            document.getElementById('getQty').value = 1;
        }
    });
    var cross = document.getElementById("cross1bundle");
    var no = document.getElementById("no1bundle");
    cross.onclick = function () {
        var modal = document.getElementById('modal_bundle');
        modal.style.display = "none";
    }
    no.onclick = function () {
        var modal = document.getElementById('modal_bundle');
        modal.style.display = "none";
    }


    function addBundle() {

        var bundle_id = document.getElementById('bundle_id_insert').value;
        var customer_id = document.getElementById('customer').value;
        var type = document.getElementById('type').value;
        var qty = document.getElementById('getQty').value;
        var j = document.getElementById('j').value;

//            var f = "{{ \Session::get('invoice_22') }}";
//            console.log(f);
//            console.log(j);

        $.ajax({
            url: "<?php echo url('invoice/insert/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j + '/' + qty + '/' + customer_id+ '/' + type,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                if (response != 0) {
                    $(item_body).find('tbody').append(response);
//                    j++;
                }
//                
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });
        document.getElementById('getQty').value = 1;
        var modal = document.getElementById('modal_bundle');
        modal.style.display = "none";


    }
</script>