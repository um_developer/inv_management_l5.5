

<style>
    .modal-dialog{
        overflow-y: initial !important
    }
    .new-modal-body{
        height: 520px;
        overflow-y: auto;
    }
</style>


<div class="modal" id="modal_item_detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross_subitems"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Choose your Sub Item</h4>
            </div>
            <div class="modal-body new-modal-body">
                <div class="table-responsive">
                    <!--<input id="detail_item_id" value="aamm" >-->
                    <input type="hidden" id="detail_row_id" value="" >
                    <table class="table table-striped" id="subitem_body">
                        <thead>
                        </thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="no_subitems" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <a id="closemodal" class="btn btn-success" onclick="updateNewSubitem()">Select</a>

            </div>
        </div>

    </div>

</div>
<script type="text/javascript">
    var cross = document.getElementById("cross_subitems");
    var no = document.getElementById("no_subitems");
    var y = 0;
    
    cross.onclick = function () {
        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "none";
        var item_row = "item_" + y;
        document.getElementById(item_row).style.display = 'none';
        $('#item_id_'+y).val('');
        var text = $('#footer_total').text()        
        var a1 = $("#total_" + y).val();
        var grand_total0 = text - parseFloat($("#total_" + y).val());
        grand_total0 = parseFloat(grand_total0).toFixed(2);
        $('#footer_total').html(grand_total0);
        $('#total_'+y).val('0');
        $('#total_'+ y).val('0');
        InsertNewRow();
    } 
    no.onclick = function () {
        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "none";
        var item_row = "item_" + y;
        document.getElementById(item_row).style.display = 'none';
        // deleteRow(r)
        $('#item_id_'+y).val('');
        var text = $('#footer_total').text()        
        var a1 = $("#total_" + y).val();
        var grand_total0 = text - parseFloat($("#total_" + y).val());
        grand_total0 = parseFloat(grand_total0).toFixed(2);
        $('#footer_total').html(grand_total0);
        $('#total_'+y).val('0');
        $('#total_'+ y).val('0');
        InsertNewRow();
    }

    function subItemsModal(r) {
 y = r;
//        var i = r.parentNode.parentNode.rowIndex;
        var i = r;
        var item_id = $("#item_id_" + i).val();
        
        $(subitem_body).find('tbody').html('LOADING DATA ...');

        $.ajax({
            url: "<?php echo url('get/modal/sub-items'); ?>" + '/' + item_id,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                if (response != 0) {
                    $(subitem_body).find('tbody').html('');
                    $(subitem_body).find('tbody').append(response);
                } else {
                    $(subitem_body).find('tbody').html('This item has no subitems.');
                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });

        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "block";
        $("#detail_row_id").val(i);

    }

    function updateNewSubitem() {

        var sub_items_count = document.getElementById('sub_item_count').value;
        var total_quantity = 0;

        for (var i = 1; i <= sub_items_count; i++) {

            var item_quantity = document.getElementById('sub_item_count_' + i).value;
            var item_id = document.getElementById('sub_item_id_' + i).value;
            var item_name = document.getElementById('sub_item_name_' + i).value;
            total_quantity = total_quantity + item_quantity;
            if (item_quantity != '') {
                insertSubitemsRow(item_id, item_quantity, item_name);
            }


        }

        var row_id = document.getElementById('detail_row_id').value;
        var radioValue = $("input[name='subitem_name']:checked").val();
        var radioId = $("input[name='subitem_name']:checked").attr('id');

        if (radioValue) {
            $("#item_name_" + row_id).val(radioValue);
            $("#item_id_" + row_id).val(radioId);
            document.getElementById("item_name_" + row_id).title = radioValue;
        }

        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "none";

        $("#quantity_" + row_id).focus();
        
        if(total_quantity == '0'){
        var item_row = "item_" + y;
        document.getElementById(item_row).style.display = 'none';
        InsertNewRow();
    }
    }


    function insertSubitemsRow(item_id, item_quantity, item_name) {


        InsertNewRow();

        var i = window.j - 2;
        $("#item_id_" + i).val(item_id);

        if ($("#price_" + i).val() == '') {
            $("#price_" + i).val(window.parent_item_price);
        } else {
            window.parent_item_price = $("#price_" + i).val();
        }

        $("#quantity_" + i).val(item_quantity);
        $("#item_name_" + i).val(item_name);
        document.getElementById("item_name_" + i).title = item_name;

        var total = window.parent_item_price * item_quantity;
        total = parseFloat(total).toFixed(2);
        $("#total_" + i).val(total);

        $("#start_serial_number_" + i).prop("readonly", true);
        $("#end_serial_number_" + i).prop("readonly", true);
        $("#quantity_" + i).prop("readonly", false);
        $("#start_serial_number_" + i).val('0'); // save selected id to hidden input    
        $("#end_serial_number_" + i).val('0'); // save selected id to hidden input    

        if (user_type == 'client' || is_order) {
            $("#price_" + i).prop("readonly", false);
        } else {
            $("#price_" + i).prop("readonly", true);
        }
        
         getItemPrice(i, $("#price_" + i).val());

    }
</script>