<?php $type = 'image1'; ?>

<div class="modal modal--game-upload fade" id="imgModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Item Image Gallery</strong></h4>
            </div>

            <div class="modal-body">
               
                @if($type == 'image')
                <img id="img_display" src="{{ asset('laravel/public/uploads/items/no-image.png')}}" alt="Smiley face" width="455"> 
                @else
                <div class="modal-gallery-wrapper">

                    <div class="Thumbs swiper-container">
                        <div class="swiper-wrapper" id="imageGalleryThumbnail">
                               
                        </div>
                        <div class="tswiper-button-prev"></div>
                        <div class="tswiper-button-next"></div>
                    </div>

                     <div class="Gallery swiper-container">
                        <div class="swiper-wrapper" id="imageGalleryDisplay">

                            
                        </div>
                    </div> 

                </div>
                @endif

<!--                <div class="modal-product-desc">
                    <h4>Description</h4>
                    <p>Product description goes here</p>
                </div>-->

            </div>

        </div> <!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
    function showItemImage(r) {

        $('#imgModal').modal('show');
        var i = r.parentNode.parentNode.rowIndex;

        if ($("#item_image_" + i).val() == '') {
            var image_path = "{{ asset('laravel/public/uploads/items/no-image.png')}}";
        } else {
            var image_path = "{{ asset('laravel/public/uploads/items')}}" + "/" + $("#item_image_" + i).val();
        }

        $("#img_display").attr("src", image_path);

    }

    function showItemGallery(image_path,item_id) {

        $('#imgModal').modal('show');
        
         $("#imageGalleryThumbnail").html('');
                $("#imageGalleryDisplay").html('');  
                
//console.log(data);
//                var main_image = "{{ asset('laravel/public/uploads/items')}}" + "/" + image_path;
//                var main_image2 = "<div class='swiper-slide'><img src='" + main_image + "' /></div>"
//                $("#imageGalleryThumbnail").append(main_image2);
//                $("#imageGalleryDisplay").append(main_image2);
                
        $.ajax({
            url: "{{ url('getGalleryImages')}}",
            data: { "item_id": item_id },
            type: "get",
            success: function(data){
               
                $("#imgModal").addClass("loaded");

            $.each(data, function( index, value ) {
                
                if (index == 0) {
                    var image_path = "{{ asset('laravel/public/uploads/items')}}";                    
                }else{
                    var image_path = "{{ asset('laravel/public/uploads/items/gallery')}}";
                }

                var image = image_path+'/'+value.image
                var option = "<div class='swiper-slide'><img src='"+image+"' /></div>"
                     
                     
                $("#imageGalleryThumbnail").append(option);
                $("#imageGalleryDisplay").append(option);

             });
             
            }
        });
    

      //  $('#imgModal').modal('show');
         productModalGallery();

            $(window).on('shown.bs.modal', function() { 
                $('#imgModal').modal('show');
                setTimeout(function () { 

                    var galleryTop = new Swiper('.Gallery', {
                        direction: 'vertical',
                        spaceBetween: 10
                    });
            
                    var galleryThumbs = new Swiper('.Thumbs', {
                        direction: 'vertical',
                        spaceBetween: 10,
                        centeredSlides: true,
                        slidesPerView: 9,
                        touchRatio: 1,
                        slideToClickedSlide: true,
                        navigation: {
                            nextEl: '.tswiper-button-next',
                            prevEl: '.tswiper-button-prev',
                        },
                    });
            
                    galleryTop.params.control = galleryThumbs;
                    galleryThumbs.params.control = galleryTop;
            
            
                }, 1000 );
            // alert('shown');
            });

    }
    
    
    function showItemModal(image) {
      
       
        $("#imgModal").addClass("loaded");
        $('#imgModal').modal('show');
         if(image == ''){
            var image_path = "{{ asset('laravel/public/uploads/items/no-image.png')}}";
            }else{
                var image_path = "{{ asset('laravel/public/uploads/items')}}"+"/"+image;
            }
        $("#img_display").attr("src", image_path);  
    }

</script>
