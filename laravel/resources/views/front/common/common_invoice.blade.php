        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Invoices</h3>
                <a href="{{ url('invoice/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Invoice</a>
                <button class="btn btn-primary pull-right" onclick="showBulkUpdate(2)" style="margin-right: 5px;">Bulk Delivery Update</button>
                <button class="btn btn-primary pull-right" onclick="showBulkUpdate('invoicePrint')" style="margin-right: 5px;">Bulk Print</button>
                <a href="" id="phiddenbtn" class="btn_print"></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
                <div class="col-md-12">

                <table id="vendors3" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onclick="CheckllidsForprint();" id="checkAll">Bulk Operations</th>
                            <th>Invoice ID</th>
                            <th>Customer Name</th>
                            <th>Memo</th>
                            <th>Amount</th>
                            <th>Order Refrence</th>
                            <th>Profit</th>
                            <th>Status</th>
                            <th>Statement Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($invoices0) > 0)
                        @foreach($invoices0 as $item)
                        <tr>
                            @if($item->status == 'approved' || $item->status == 'delivered' || $item->status == 'processing')
                            <td><input class="chk" type="checkbox" id="{{'check_box_'.$item['id']}}" value="{{ $item['id']}}" name="checks" onclick="SelectallidsForprint({{ $item['id']}});"></td>
                            @else
                            <td></td>
                            @endif
                            <td><a href="{{ url('invoice/'.$item->id) }}">{{$item->id}}</a></td>
                            @if($item->status != 'delivered')
                            <td><a href="{{ url('customer/edit/'.$item->customer_u_id) }}">{{$item->customer_name}}</a></td>
                            @else
                            <td>{{$item->customer_name}}</td>
                            @endif
                            <td>{{$item->memo}}</td>
                            <td>{{$item->total_price}}</td>
                            <td>
                            @if($item->order_refrence > 0)
                            <a href="{{ url('ref/order/detail/'.$item->order_refrence) }}">
                                {{$item->client_name}}
                                ( Order # {{$item->order_refrence}} )</a>
                            @endif
                            </td>
                            <td>{{$item->profit}}</td>
                            @if($item->status == 'pending')
                            <td><a class="label label-warning btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Pending</a></td>
                            @elseif($item->status == 'approved')
                            <td><a class="label label-success btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Approved</a></td>
                            @elseif($item->status == 'delivered')
                            <td><a href="#my_modal" class="btn btn-primary btn-xs" data-toggle="modal" data-book-id="{{$item->id}}" class="label label-primary btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Delivered</a></td>
                            @elseif($item->status == 'processing')
                            <td><a class="label label-warning btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Processing</a></td>
                            @else
                            <td><a class="label label-danger btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Rejected</a></td>
                            @endif
                            <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                            <td>
                                <a href="{{ url('invoice/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                <a href="{{ url('invoice/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                 @if($item->status == 'pending')
                                <a href="{{ url('invoice/delete/'.$item->id) }}"  onclick="return confirm('Are you sure you want to delete this Invoice?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                @endif

                                @if($item->status == 'approved' && $item->order_refrence > 0)
                                    <a href="{{ url('invoice/processing/'.$item->id) }}"class="btn btn-primary">Process</a>
                                @elseif($item->status == 'processing')
                                <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-primary deliver">Deliver</a>
                                @elseif($item->status == 'approved')
                                <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-primary deliver">Deliver</a>
                                @elseif($item->status == 'delivered')
                                <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-danger deliver"><i class="fa fa-undo" aria-hidden="true"></i>
                                </a>
                                @endif
                            </td>
                        </tr>

                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

                </div></div>
            </div>
            <!-- /.box-body -->
        </div>
        <input type="hidden" id="selectedids" name="artistslist" value="" />
@include('front/common/bulk_status_update_invoices_orders')
@include('front/common/dataTable_js')

<script>
var allids = [];
 
function checkValue(value,arr){
  var status = 'Not exist';
 
  for(var i=0; i<arr.length; i++){
    var name = arr[i];
    if(name == value){
      status = 'Exist';
      break;
    }
  }

  return status;
}

function CheckllidsForprint(){
    
    setTimeout(function(){ 
        var chkArray = [];
        var unchkArray = [];
        var values = $("#selectedids").val().split(",");

        $(".chk:checked").each(function() {
        chkArray.push($(this).val());
        });
        if (chkArray == '') {
            $("input:checkbox:not(:checked)").each(function() {
                unchkArray.push($(this).val());
            });
            
            var newValue = [];
            var values = $("#selectedids").val().split(",");
            for ( var i = 0 ; i < unchkArray.length ; i++ )
            {
                var ck = checkValue(unchkArray[i], values)
                if(ck != 'Exist'){
                    newValue = newValue + values[i] + ",";
                }
            }
            $("#selectedids").val( newValue );
            newValue = '';
            return true;
        }
        allids = values
        allids.push(chkArray);
        var selected = allids.join(',');
        if(selected != ''){
            var selected = selected.replace(",,", ",");
        }
        $('#selectedids').val(selected);
        allids = '';
        chkArray = ''
    }, 1000);
        
}

    function SelectallidsForprint(val) {
        
        var values = $("#selectedids").val().split(",");
        var newValue = [];

                var ck = checkValue(val, values)
                
                if(ck == 'Exist'){
                    for ( var i = 0 ; i < values.length ; i++ )
                    {
                        if ( val != values[i] )
                        {
                            newValue = newValue + values[i] + ",";
                        }
                    }
                    
                    if(newValue != ''){
                        var newValue = newValue.replace(",,", ",");
                    }
                    
                    $("#selectedids").val( newValue );
                }else{
                    var values = $("#selectedids").val().split(",");

                    if (values != "") {
                        values.push(val);                        

                            for ( var i = 0 ; i < values.length ; i++ )
                            {
                                if ( values[i] != "" )
                                {
                                    newValue = newValue + values[i] + ",";
                                }
                            }

                        $("#selectedids").val( newValue );
                    
                    }else{
                        $("#selectedids").val( val );
                    }
                }

    }
    $('#my_modal').on('show.bs.modal', function(e) {

        var bookId = $(e.relatedTarget).data('book-id');
        var status = 'invoices';
        var url = "<?php echo url('invoice/getComments'); ?>" + '/' + bookId + '/' + status;
                $("#bookId").empty();
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'html',
                    success: function (response) {
                        var array = JSON.parse(response);
                        var clients = array
                        
                        clients.forEach(function(object) {
                               var option = '<div class="row"><div class="col-md-6" style="text-transform: capitalize;">By[ '+object.firstName+' '+object.lastName+' ] On '+object.created_at+'</br>'+object.status_type+': '+object.delivered_comments+'</div></div><hr>'; 
                             $("#bookId").append(option); 
                        });

                    },
                    error: function (xhr, status, response) {
                    }
                });
    });

    $('.deliver').click(function(){
        var id=$(this).data('id');
        var currentLocation = window.location;
        document.getElementById("updateStatusDeliver").value = id;
        document.getElementById("location").value = currentLocation.href;
        document.getElementById("table1").value = 'invoices';
    })

    function gameCheck() {
        var chkArray = $('#selectedids').val();
        var checked = chkArray.length;
    if (!checked) {
        alert('Please select atleast 1 item.');

    return false;
    }
    var currentLocation = window.location;
    document.getElementById("location1").value = currentLocation.href;
    document.getElementById("table").value = 'invoices';
    return true;
    }

    function printCheck() {
    checked = $("input[class=chkp]:checked").length;
    if (!checked) {
        alert('Please select atleast 1 item.');

    return false;
    }

    return true;
    }



//    $(function () {
//        $('#vendors').DataTable({
//            "paging": true,
//            "lengthChange": false,
//            "searching": true,
//            "ordering": false,
//            "info": false,
//            "pageLength": {{Config::get('params.default_list_length')}},
//            "autoWidth": false,            
//            dom: 'Bfrtip',
//            buttons: [
//            {
//            extend: 'excelHtml5',
//                    text: 'Export To Excel',
//                    title: 'Report',
//                   
//            },
//            {
//            extend: 'pdfHtml5',
//                    text: 'Export To PDF',
//                    title: 'Report',
//                    
//            }
//            ]
//        });
//    });


        $(document).ready(function () {

            $('#customer_id').on('select2:select', function (e) {});
            $('#customer_id').select2({
              language: {
                noResults: function (params) {
                  alert("Customer not found. Please create this customer.");
                }
              }
            });
        });
        $(function () {
                                        $('#vendors3').DataTable({
                                        "paging": true,
                                                "lengthChange": false,
                                                "searching": true,
                                                "ordering": false,
                                                "info": false,
                                                "pageLength": {{Config::get('params.default_list_length')}},
                                                "autoWidth": false,
                                                dom: 'Bfrtip',
                                                buttons: [
                                                {
                                                extend: 'excelHtml5',
                                                        text: 'Export To Excel',
                                                        title: 'Invoices',
                                                },
                                                {
                                                extend: 'pdfHtml5',
                                                        text: 'Export To PDF',
                                                        title: 'Invoices',
                                                }
                                                ]
                                        });
                                        });
</script>