<div class="modal" id="BulkUpdateModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Bulk Price Update</h4>
            </div>
            <div class="modal-body">
                <p>
                    @if(Auth::user()->role_id == 4)
                    {!! Form::open(array( 'class' => '', 'id' => 'bulk_submit','url' => 'customer/item-price-template/price-update-bulk', 'method' => 'post')) !!}
                    @else
                    {!! Form::open(array( 'class' => '', 'id' => 'bulk_submit','url' => 'item-price-template/price-update-bulk', 'method' => 'post')) !!}
                    @endif
                <div class="box-body">
                    <div class="row">

                        <div class="form-group col-sm-6">
                            Enter New Selling Price
                            <input class="form-control samevalu" type="text" id="bulk_selling_price" placeholder="00" maxlength="10" name="bulk_selling_price" >
                            <h5>OR<br></h5>
                            Enter X times Cost
                            <input class="form-control samevalu" type="text" id="bulk_cost_percentage" placeholder="0" maxlength="4" name="bulk_cost_percentage" >
                             <h5>OR<br></h5>
                            Enter Fixed Added Amount to Selling Price
                            <input class="form-control samevalu" type="text" id="bulk_added_amount" placeholder="0" maxlength="4" name="bulk_added_amount" >
                            
                             <h5>OR<br></h5>
                            Enter Fixed Added Amount to Cost Price
                            <input class="form-control samevalu" type="text" id="bulk_added_cost_amount" placeholder="0" maxlength="4" name="bulk_added_cost_amount" >
                             
                            <input class="form-control" type="hidden" id="bulk_item_id" name="bulk_item_id" >
                            {!! Form::hidden('bulk_template_id', $template_id ) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            Select Display status
                            {!!   Form::select('bulk_display', array('0' => 'Do Not Update','1' => 'No','2' => 'Yes'), 0, array('class' => 'form-control' ))  !!}

                        </div>

                    </div>
                </div>

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="no" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <input type="submit" id="btnSubmit" class="btn btn-primary" name="submit" value="Update Selected Items">

            </div>
        </div>

    </div>

</div>
<script type="text/javascript">

    function gameCheck() {
    checked = $("input[class=chk]:checked").length;
    if (!checked) {
        alert('Please select atleast 1 item.');
//    $("#error_msg2").removeClass("hidden");
//    setTimeout(function(){ $("#error_msg2").addClass("hidden"); }, 5000);
    return false;
    }

    return true;
    }

    function showBulkUpdate() {

        if (gameCheck()) {
            $('#BulkUpdateModal').modal('show');
            getValueUsingClass();
        }
    }
    $(document).ready(function () {
        $('.samevalu').on('keyup', function() {
 
            $('.samevalu').not(this).val("");
           
            });

        $("#bulk_submit").submit(function (e) {
            $("#btnSubmit").attr("disabled", true);
            return true;

        });
    });
</script>
