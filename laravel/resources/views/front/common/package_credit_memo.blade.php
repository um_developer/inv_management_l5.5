 
<div class="col-sm-4">
    <label for="exampleInputEmail1">Select Package #</label>
    {!!   Form::select('package_id', $packages, Request::input('package_id'), array('class' => 'form-control select2 select_package','id' => 'package_id' ))  !!}

</div>


<script type="text/javascript">
    var j = 1;
    $(document).ready(function () {
        $('.select_package').on('change', function () {
            var data = $(".select_package option:selected").text();
            loadPackageAjax(data, j);
        })

        @if (1 == 2)
        var customer_select = document.getElementById('customer_id');
        customer_select.addEventListener('change', function () {
            var table = $('#item_body').DataTable();
            table.clear().draw();
            InsertNewRow();
        }, false);
        @endif
    });

    $(function () {
        $('.select2').select2()
    });

    function loadPackageAjax(package_id, j) {

        $.ajax({
            url: "<?php echo url('invoice/create/bulk/'); ?>" + '/' + package_id + '/' + j,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                if (response != 0) {
                    var modal = document.getElementById('modal_package');
                    modal.style.display = "block";
                    $(modal_package).find('tbody').empty();
                    $(modal_package).find('tbody').append(response);
                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });

    }

    function deletePackageRow(r) {

        $("#item_body").find('input[name="package_id"]').each(function () {
            if ($(this).val() == r) {
//                $(this).closest("tr").remove();
                
                var id = $(this).closest("tr")[0].id;
            var row_id = id.substr(id.length - 1); // => "1"
            $("#quantity_" + row_id).val(0);
            $(this).closest("tr")[0].style.display = 'none';
            }
        });
    }

    function deletePackageRowEdit(r) {

    $("#item_body").find('input[name="package_id"]').each(function(){
    if ($(this).val() == r){
    var id = $(this).closest("tr")[0].id;
            var row_id = id.substr(id.length - 1); // => "1"
            $("#quantity_" + row_id).val(0);
            $(this).closest("tr")[0].style.display = 'none';
            }
    });
            }
</script>