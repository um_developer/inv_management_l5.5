
<section id="form1" style="width: 100%;">
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Edit Bundle</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'bundle/update', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="bundle_id0" name="id" value="{{ $model->id }}">
        <input type="hidden" name="qw" id="qwe" value="{{ $q }}">
        <input type="hidden" id="selecteditems0" name="selecteditems0[]" value="">
        <div class="box-body">
         
            <div class="form-group">
            
            <div class="col-sm-4">
                <label for="exampleInputEmail1">Bundle Name</label>
             
            <input type="text"  name="name"  class = 'form-control' value="{{ $model->name }}">
            </div>

            </div>
            <a href="#" id="add_more1" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>

            @include('front/common/suggestion_box')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body1">
                    <thead>
                        <tr>

                            <th>Item Name</th>
                            <th>SKU/Item Code</th>
                            <th>UPC/Barcode</th>
                            {{-- <th>Remaining Qty</th> --}}
                            <th>Total Qty</th>
                           <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @if(count($po_items)>0)
                        @foreach($po_items as $items_1)
                        <?php
                       // dump($items_1);
                        $delivered_items = $items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
                        ?>
                        @if(1==1)
                        <tr id="item00_{{ $i }}">
                            <td><input class="form-control" type="text" name="item_name1_{{ $i }}" title="{{$items_1->item_name}}" id="item_name1_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id1_{{ $i }}" id="item_id01_{{ $i }}"  value="{{$items_1->id}}">
                                <input type="hidden" readonly="readonly" name="bu_item_id1_{{ $i }}" id="bu_item_id1_{{ $i }}"  value="{{$items_1->bundle_id}}">
                                <input type="hidden" name="num[]" id="num[]"></td>
                           
                            <td><input class="form-control"readonly="readonly" name="sku1_{{$i}}"  value="{{$items_1->item_sku}}"></td>
                            <td><input class="form-control"  name="upc_barcode1_{{$i}}"  readonly="readonly" value="{{$items_1->item_upc_barcode}}"></td>
                            <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" onchange="calculatePrice('{{ $i }}')" name="quantity1_{{ $i }}" id="quantity1_{{ $i }}" value="{{$items_1->quantity}}" step="0.01"></td>
                            <td> <input id="delete_btn1_{{ $i }}" name="delete_btn1_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Item" onclick="deleteRow1(this,{{ $items_1->id }})"></td></td>
                        </tr>


                        <?php $i++; ?>
                        @endif
                        @endforeach

                        @endif

                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div>
            <div class="row" hidden="true">

                <div class="col-xs-8"></div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Total Amount for Purchase Order</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Total:</th>
                                <td id="po_total">$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

            <script type="text/javascript">

                                var user_type = "{{ Auth::user()->role->role }}";
                                var is_order = false;
                                var j = {{ count($po_items) + 1 }}
                                clickArray = [];
                                clickArray1 = [];
                                $(document).ready(function(){
                                    InsertNewRow1();
                                    for (s = 1; s < j - 1; s++) {
                                        var r = '#item_id01_'+s
                                        var t = '#quantity1_'+s
                                        var a = $(r).val();
                                        var b = $(t).val();
                                        if (a  && b ) {
                                            // console.log('true')
                                            clickArray[a] = b;
                                        }

                                    }
                                    // console.log(clickArray)
                                });
                                function myFunction(item, index) {
                                    console.log(index + ":" + item);
                                } 
                                function calculatePrice(i){

                                    var price = ($("#price_" + i).val());
                                    var quantity = parseFloat($("#quantity1_" + i).val());

                                    var total = price * quantity;
                                    total = parseFloat(total).toFixed(2);
                                    $("#total_" + i).val(total);
                                }

                                function getDatableData(){
                                var aTable = $('#item_body1').DataTable();
                                var w = aTable.cell(0, 2).nodes().to$().find('input').val()
                                        alert(w);
                                var x = document.getElementById("item_body1").rows[1].cells[4];
                                console.log(x);
//                                alert(x);
                                alert(document.getElementById("item_body1").rows[0].cells.innerHTML);
                                }

    function InsertNewRow1() {
    var i = window.j;
    var scntDiv = '<tr id="item00_'+i+'"><td><input type="text" name="item_name00_' + i + '" class="form-control" id="item_name00_' + i + '"  autocomplete="on" placeholder="Enter item name1"><input type="hidden" readonly="readonly" name="item_id00_' + i + '" id="item_id01_' + i + '"><input type="hidden" name="num[]" id="num[]"></td><td><input type="text" readonly="readonly" name="sku1_' + i + '" class="form-control" id="sku1_' + i + '"></td><td><input type="text" name="upc_barcode1_' + i + '" class="form-control" readonly="readonly" id="upc_barcode1_' + i + '"></td><td><input type="hidden" readonly="readonly"  name="r_quantity1_' + i + '" class="form-control" id="r_quantity1_' + i + '"><input type="number" name="quantity1_' + i + '" class="form-control"   min ="1" id="quantity1_' + i + '" onchange="chnageQ(this,'+i+')"></td><td><input type="button" class="btn btn-danger btn-sm" id="delete_btn1_' + i + '" name="delete_btn1_' + i + '" value="Delete item" onclick="deleteRow1(this,'+i+')"></td></tr>';
    var item_array = <?php echo json_encode($items, JSON_PRETTY_PRINT) ?>;

    $("#item_body1").append(scntDiv);
    $(function ($) {

        $('#item_name00_' + i).autocomplete({
            source: item_array,
            select: function (event, ui) {

                $("#item_id1_" + i).val(ui.item.id);
                $("#sku1_" + i).val(ui.item.sku);
                $("#upc_barcode1_" + i).val(ui.item.upc_barcode);
                $("#r_quantity1_" + i).val(ui.item.inventory_quantity);
                $("#num[0]").val(ui.item.id);
                $("#quantity1_" + i).val('1');
                $('#item_id01_'+i).val(ui.item.id);
                if(typeof clickArray[ui.item.id] === 'undefined') {
                    clickArray[ui.item.id] = 1;
                }
            }
        });
    });
    window.j++;
    return false;
}
// var removeVal = $('#item_id01_'+i).val();
// quantity1_
// item_id01_
    // $('#quantity1_').on('focusout', function() {
    
    // });

                                $('#add_more1').on('click', function () {
                                InsertNewRow1();

                                });
                                $(function () {
                                $('#item_body1').DataTable({
                                "paging": false,
                                        "lengthChange": false,
                                        "searching": false,
                                        "ordering": false,
                                        "info": false,
                                        "autoWidth": false
                                });
                                });
                                function chnageQ(r, a = 0) {
                                        var i = r.parentNode.parentNode.rowIndex;
                                        var qun = $('#quantity1_'+i).val()
                                        var it = $('#item_id01_'+i).val()
                                        clickArray[it] = qun;
                                        // console.log(clickArray)
                                }
                                function deleteRow1(r, a = 0) {

                                    var i = r.parentNode.parentNode.rowIndex;
                                    if (a != '0'){
                                        $("#quantity1_" + i).val(0);
                                        $("#total1_" + i).val(0);
                                        $("#quantity1_" + i).prop("readonly", true);
                                        var item_row = "item00_" + i;
                                        document.getElementById(item_row).style.display = 'none';
                                        }
                                        else{
                                        $("#quantity1_" + i).val(0);
                                        $("#total1_" + i).val(0);
                                        $("#quantity1_" + i).prop("readonly", true);
                                        var item_row = "item00_" + i;
                                        document.getElementById(item_row).style.display = 'none';
                                        //document.getElementById("item_body").deleteRow(i);
                                    }
                                    var removeVal = $('#item_id01_'+i).val();
                                    delete clickArray[removeVal]
                                }

                                function getItemPrice(i, price) {

                                var item_id = $("#item_id1_" + i).val();
                                var customer_id = $("#vendor_id").val();
                                var type = 'purchase_order';
                                my_url = "<?php echo url('get-item-price/'); ?>" + '/' + item_id + '/' + customer_id + '/' + type;
                                $.ajax({
                                url: my_url,
                                        type: 'get',
//            dataType: 'html',
                                        success: function (response) {

                                        if (response != '0') {
                                        $("#price_" + i).val(response['sale_price'][0]['item_unit_price']);
                                        } else {
                                        $("#price_" + i).val(price);
                                        }
                                        calculatePrice(i);
                                        },
                                        error: function (xhr, status, response) {
                                        alert(response);
                                        }
                                });
                                }

                                function getItemPriceHTML(i, price) {

                                var item_id = $("#item_id_" + i).val();
                                var customer_id = $("#vendor_id").val();
                                var type = 'purchase_order';
                                my_url = "<?php echo url('get-item-price-html/'); ?>" + '/' + item_id + '/' + customer_id + '/' + type;
                                $.ajax({
                                url: my_url,
                                        type: 'get',
                                        dataType: 'html',
                                        success: function (response) {

                                        if (response != '0') {
                                        var suggestion_box = document.getElementById("suggestion_box");
                                        suggestion_box.style.display = "block";
                                        var suggestion_box_text = document.getElementById("suggestion_box_text");
                                        suggestion_box_text.innerHTML = response;
//                alert(response);
                                        }
                                        },
                                        error: function (xhr, status, response) {
                                        alert(response);
                                        }
                                });
                                }

                                function showSuggestionPopup(i){
                                calculatePrice(i);
                                var keycode = (window.event) ? event.keyCode : e.keyCode;
                                if (keycode == '9') {
                                getItemPriceHTML(i);
                                }

                                }

                                function hideSuggestionPopup(){

                                var suggestion_box = document.getElementById("suggestion_box");
                                suggestion_box.style.display = "none";
                                }

            </script>
        </div>
        <!-- /.box-body -->
        <!-- <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update Bundle">
        </div> -->
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

</section>
