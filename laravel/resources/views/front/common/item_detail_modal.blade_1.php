

<style>
.modal-dialog{
      overflow-y: initial !important
}
.new-modal-body{
  height: 520px;
  overflow-y: auto;
}
</style>


<div class="modal" id="modal_item_detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross_subitems"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Choose your Sub Item</h4>
            </div>
            <div class="modal-body new-modal-body">
                <div class="table-responsive">
                    <!--<input id="detail_item_id" value="aamm" >-->
                    <input type="hidden" id="detail_row_id" value="" >
                    <table class="table table-striped" id="subitem_body">
                        <thead>
                        </thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="no_subitems" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <a id="closemodal" class="btn btn-success" onclick="updateNewSubitem()">Select</a>

            </div>
        </div>

    </div>

</div>
<script type="text/javascript">
    var cross = document.getElementById("cross_subitems");
    var no = document.getElementById("no_subitems");
    cross.onclick = function () {
        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "none";
    }
    no.onclick = function () {
        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "none";
    }

    function subItemsModal(r) {

//        var i = r.parentNode.parentNode.rowIndex;
        var i = r;
        var item_id = $("#item_id_" + i).val();
        $(subitem_body).find('tbody').html('LOADING DATA ...');

        $.ajax({
            url: "<?php echo url('get/modal/sub-items'); ?>" + '/' + item_id,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                if (response != 0) {
                    $(subitem_body).find('tbody').html('');
                    $(subitem_body).find('tbody').append(response);
                }else{
                     $(subitem_body).find('tbody').html('This item has no subitems.');
                 }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });

        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "block";
        $("#detail_row_id").val(i);

    }

    function updateNewSubitem() {

        var row_id = document.getElementById('detail_row_id').value;
        var radioValue = $("input[name='subitem_name']:checked").val();
        var radioId = $("input[name='subitem_name']:checked").attr('id');

        if (radioValue) {
            $("#item_name_" + row_id).val(radioValue);
            $("#item_id_" + row_id).val(radioId);
            document.getElementById("item_name_" + row_id).title = radioValue;
        }

        var modal = document.getElementById('modal_item_detail');
        modal.style.display = "none";
        
       $("#quantity_" + row_id).focus();
    }
</script>