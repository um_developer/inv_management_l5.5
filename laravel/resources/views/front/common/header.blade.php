<?php

use App\Categories;

?>
	<header>
	
		<section class="hdr-top bdr-grid bg-white">
			<div class="container0">
				
<!--				<div class="top__contactinfo col-sm-7 clrlist bg-white p0">
					<ul>
						<li><a href="tel:23409099601329"><i class="fa fa-phone"></i> <span>+1111111111</span></a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> <span>Office address</span></a></li>
						<li><a href="#"><i class="fa fa-clock-o"></i> <span>MON - SAT : 8 A.M - 9.00 P.M</span></a></li>
						
					</ul>
				</div>-->
				
				
				<div class="top__social col-sm-5 clrlist text-right bg-white p0">
				
					<ul>
                                            <?php
                                if (isset(Auth::user()->id)) {
                                    ?>
<!--						<li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>-->
                                                <?php  } ?>
                                                </ul>
				</div>
				
			</div>
		</section>
	
	<section class="hdr-area hdr-nav  cross-toggle navbar-overide">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="{{ url('home') }}"><img src="{{ asset('front/images/logo.png')}}" alt="" class="broken-image"/></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  
					  <?php
                                if (isset(Auth::user()->id)) {
                                    ?>
					  <a class="btn btn-primary btn-login fr" href="{{url('auth/logout')}}">LOGOUT</a>
					  <a class="btn btn-primary btn-login fr" href="{{ url('dashboard') }}" >Dashboard</a>
					  <a class="btn btn-primary btn-login fr" href="{{ url('changepassword') }}" >Change Password</a>
					  <a class="btn btn-primary btn-login fr" href="{{ url('profile') }}" >Profile</a>
                                          
					  <?php }else{ ?>
						 <a class="btn btn-primary btn-login fr" href="{{url('login')}}">Client Login</a> 
                                                  <a class="btn btn-primary btn-login fr" href="{{url('technician/login')}}">Technician Login</a> 
                                                   <a class="btn btn-primary btn-login fr" href="{{url('reviewer/login')}}">Reviewer Login</a> 
					<?php  } ?>
					  <ul class="nav navbar-nav navbar-main fr">
					  
						<li><a href="{{ url('home') }}">Home</a></li>
                                                <li><a href="{{ url('/') }}">BIM</a></li>
                                                <li><a href="{{ url('/') }}">Manufacturing</a></li>
                                                <li><a href="{{ url('/') }}">Construction</a></li>
                                                <li><a href="{{ url('/') }}">Drawing management</a></li>
						<!--<li><a href="{{ url('/') }}">Contact Us</a></li>-->
					  </ul>
					  
					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>
