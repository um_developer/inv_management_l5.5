@if(!empty($res))
<?php
$show_type = 'Invoice';
if($type == 'Odr'){
   $show_type = 'Order'; 
}
if($type == 'PO'){
    $show_type = 'Purchase Order';
}

?>
@if($res[0]['item_unit_cost'] != '0' && $res[0]['item_unit_cost'] != '0.00')
@if(Auth::user()->role_id == '4' &&  $type == 'Inv')
My template cost Price =  {{ $invoice_template_sale_price }}<br>
@else
@if($customer_templete_cost_price != '0')
Cost-From Tmpl = {{ $customer_templete_cost_price }}<br>
@endif
@if($type == 'Odr')
Cost-From Invoice = {{ $res[0]['item_unit_cost'] }}<br>
@else
My Item List Cost Price = {{ $invoice_template_cost_price }}<br>
My Purchase Order Price = {{ $res[0]['item_unit_cost'] }}<br>
My Template sale Price = {{ $invoice_template_sale_price }}<br>

@endif
@endif
@endif
@if($type == 'Odr')
Sale-From Tmpl = {{ $client_templete_price }} <br>
@endif

@if($res[0]['item_unit_price'] != '')

@if($type == 'PO')
Last item price sold<br>
@endif

<?php $i = 1; ?>
@foreach($res as $a)
<?php 
$extra = '';
$inititls = $type;
if($a['deleted'] == '2'){
    $extra = 'Prc_Chg';
    $inititls  = '';
}

?>
{{ $inititls.$extra .' # '.$a['id'].' = $'. $a['item_unit_price'] }}<br>
<?php $i++; ?>
@endforeach
@else
@if($type == 'Odr')
Sale-from Order = not found
@else
My {{ $show_type }} Price = not found
@endif
@endif
@else
There is no Item price.
@endif