        <?php
        $client = \App\Customers::where('user_id', Auth::user()->id)->select('client_menu')->first();
        ?>
        @if($client->client_menu == 1)
        <div class="btn-group" style="margin-bottom: 10px">
            <button onclick="location.href = '{{ url('/customer/clients') }}';" type="button" class="btn btn-primary">My Clients</button>
            <button onclick="location.href = '{{ url('/customer/orders') }}';" type="button" class="btn btn-primary">Orders</button>
            <button onclick="location.href = '{{ url('/customer/order-returns') }}';" type="button" class="btn btn-primary">Order Returns</button>
            <button onclick="location.href = '{{ url('/customer/client/payments') }}';" type="button" class="btn btn-primary">Payments</button>

        </div>

        <div class="btn-group" style="margin-bottom: 10px">
            <button onclick="location.href = '{{ url('customer/report/client-statement') }}';" type="button" class="btn btn-info">Statement Report</button>
            <button onclick="location.href = '{{ url('/customer/report/client/sale-profit') }}';" type="button" class="btn btn-info">Sale /Profit Report</button>
            <button onclick="location.href = '{{ url('/customer/report/item/sale-profit') }}';" type="button" class="btn btn-info">Item Sale /Profit Report</button>
            <button onclick="location.href = '{{ url('/customer/report/client/balance') }}';" type="button" class="btn btn-info">Balance Report</button>
        </div>


        <div class="btn-group" style="margin-bottom: 10px">
            <button onclick="location.href = '{{ url('customer/print_page_setting') }}';" type="button" class="btn btn-default">Settings</button>
            <button onclick="location.href = '{{ url('/customer/payment-types') }}';" type="button" class="btn btn-default">Payment Type</button>
            <button onclick="location.href = '{{ url('/customer/account-types') }}';" type="button" class="btn btn-default">Accounts</button>
            <button onclick="location.href = '{{ url('/customer/priceChange') }}';" type="button" class="btn btn-default" style="display:block;">Price Change</button>
            <button onclick="location.href = '{{ url('/customer/myActivity') }}';" type="button" class="btn btn-default" style="display:block;">Price Change Log</button>
        </div>
        <br>
        @endif
