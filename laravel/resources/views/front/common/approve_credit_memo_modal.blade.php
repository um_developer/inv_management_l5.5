<style>
    
    .modal.modal-alert .modal-dialog .modal-content {
  border-radius: 6px;
  padding: 0;
}
.modal.modal-alert .modal-dialog .modal-content .modal-header {
  padding: 9px 15px;
  border-bottom: 1px solid #eee;
  background-color: #0480be;
  -webkit-border-top-left-radius: 3px;
  -webkit-border-top-right-radius: 3px;
  -moz-border-radius-topleft: 3px;
  -moz-border-radius-topright: 3px;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.modal.modal-alert .modal-dialog .modal-content .modal-body {
  padding: 10px 0;
}
.modal.modal-alert .modal-dialog .modal-content .modal-footer {
  padding-top: 15px;
}
</style> 

<div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button"  id="cross"  class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-red">Alert</h4>
                </div>
                <div class="modal-body text-red">
                    <p>Items are not avaliable in customer Inventory. 
            Click on "Approve" button to proceed further.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="no" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                     <a id="closemodal" class="btn btn-success" href="{{ url('credit-memo/status/').'/'.$model->id.'/approved/normal' }}">Approve</a>
                </div>
            </div>

        </div>

    </div>
 <script type="text/javascript">
        var cross = document.getElementById("cross");
        var no = document.getElementById("no");
        cross.onclick = function () {
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }
        no.onclick = function () {
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }
        
        </script>