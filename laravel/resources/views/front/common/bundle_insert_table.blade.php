<?php
$i = $j;
?>
@if(count($po_items)>0 && isset($po_items[0]))
@foreach($po_items as $items_1)
<?php
$delivered_items = $items_1->delivered_quantity;
$validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';

$bundle = \App\Bundles::where('id',$bundle_id)->first();
?>
<tr id="item_{{ $i }}">
    <td><input class="form-control" type="text" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name .'('.$bundle->name.' QTY: '.$bundle_quantity.')'}}">
        <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
        <input type="hidden" readonly="readonly" name="item_type_{{ $i }}" id="item_type_{{ $i }}"  value="">
        <input type="hidden" name="num[]" id="num[]">
        <input type="hidden" readonly="readonly" name="item_image_{{ $i }}" id="item_image_{{ $i }}">
    <input type="hidden" name="bundle_id" id="bundle_id" value="{{ $bundle_id }}">
    <input type="hidden" id="item_bundle_id_{{ $i }}" name="item_bundle_id_{{ $i }}" value="{{ $bundle_id }}"></td>
    <input type="hidden" name="item_bundle_quantity_{{ $i }}" id="item_bundle_quantity_{{ $i }}" value="{{ $bundle_quantity }}">
    @if($type!="purchase_order")
    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
    @endif
    <td><input {{ $validation_num }} readonly="readonly"  onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
    @if(Auth::user()->role->role == 'client')
    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" readonly="readonly" onkeyup="getItemPriceHTML('{{ $i }}')" style="width: 150px;" onchange="getItemPriceHTML('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
    @else
    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" readonly="readonly" onkeyup="getItemPriceHTML('{{ $i }}')" style="width: 150px;" onchange="getItemPriceHTML('{{ $i }}')" type="number"  readonly="readonly" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
    @endif

    <td><input class="form-control" min ="0" step="any" type="number" style="width: 150px;" name="total_{{ $i }}" onkeyup="hideBlueBox()" onChange="hideBlueBox()" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>
    <td>
        {{-- <button onclick="showItemImage(this)" id="img_button_{{ $i }}" class="btn btn-success btn-sm" type="button"><i class="fa fa-image"></i> --}}
        </button>
        <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Bundle# {{ $bundle_id }}" onclick="deletebundleRow({{ $bundle_id }})">
        <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete item" onclick="deleteRow(this)">
        @if(1 == 1)
         <input id="edit_btn_{{ $i }}" name="edit_btn_{{ $i }}" class="btn btn-primary btn-sm" type="button" value="Edit Bundle# {{ $bundle_id }}" onclick="editBundle(<?php echo $bundle_id; echo ','; echo $bundle_quantity; ?>)">
        @endif
        </td>

</tr>

<?php $i++; ?>
<script>
j = '{{ $i }}';
function hideBlueBox(){
     var suggestion_box = document.getElementById("suggestion_box");
                suggestion_box.style.display = "none";
}
</script>
@endforeach

@endif
