<div class="modal" id="modal_profit_new">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="cross_subitems"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">{{ $modal_title }} Details </h4>
            </div>
            <div class="modal-body new-modal-body">
                <div class="table-responsive">
                    <!--<input id="detail_item_id" value="aamm" >-->
                    <input type="hidden" id="detail_row_id" value="" >
                    <table class="table table-striped" id="subitem_body">
                        <thead>
                        </thead>
                        <tbody>
                            <tr> 
                            <th>Total Sale</th>
                            <td>{{ $model->total_price }}</td>
                        </tr>
                            
                            <tr>
                            <th>Total Cost</th>
                            <td>{{ $model->total_cost }}</td>
                        </tr>
                        
                        <tr>
                            <th>Total {{ $modal_title }}</th>
                            @if($modal_title == 'Profit')
                            <td>{{ $model->profit }}</td>
                            @else
                            <td>{{ $model->loss }}</td>
                            @endif
                        </tr>
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="no_subitems" class="btn btn-default " data-dismiss="modal">Close</button>
              

            </div>
        </div>

    </div>

</div>
<script type="text/javascript">
    var cross = document.getElementById("cross_subitems");
    var no = document.getElementById("no_subitems");
    cross.onclick = function () {
        var modal = document.getElementById('modal_profit_new');
        modal.style.display = "none";
    }
    no.onclick = function () {
        var modal = document.getElementById('modal_profit_new');
        modal.style.display = "none";
    }

</script>