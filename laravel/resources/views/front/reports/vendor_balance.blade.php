@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<div class = "table table-responsive" >
    <section id="form" class="mt30 mb30 col-sm-12 p0">
        <div class="row">

            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
            @endif

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Vendors Balance Report</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="vendors" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Customer Name</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(count($model) >0)
                            <?php
                            $total_balance = 0;
                            $i = 1;
                            ?>
                            @foreach($model as $item)
                            @if($item->balance != '0')
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->balance}}</td>
                            </tr>
                            <?php
                            $i++;
                            $total_balance += $item->balance;
                            ?>
                            @endif
                            @endforeach
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Total Balance: {{ $total_balance }}</th>

                            </tr>
                            @endif


                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>			
    </section>
</div>
@include('front/common/dataTable_js')
<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
         "pageLength": {{Config::get('params.reports_list_length')}},
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'Vendor Balance Report',

                },
                {
                    extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'Vendor Balance Report',

                }
            ]
        });
    });
</script>
@endsection



