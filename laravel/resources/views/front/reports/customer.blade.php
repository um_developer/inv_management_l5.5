@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

@section('content')
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        @if($customers != '')
                        <h3 class="box-title">Filter by Customer</h3>
                        @else
                        <h3 class="box-title">Filter by Transaction Type</h3>
                        @endif
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    @if($customers != '')
                    {!! Form::open(array( 'class' => '','url' => 'report-by-customer', 'method' => 'post')) !!}
                    @else
                    {!! Form::open(array( 'class' => '','url' => 'customer/report-by-type', 'method' => 'post')) !!}
                    @endif
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-sm-4">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div>
                            </div>

                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>

                            @if($customers != '')
                            <div class="form-group col-sm-4">

                                {!!  Form::select('customer_id', $customers, Request::input('customer_id'), array('class' => 'form-control select2','id' => 'customer_id' ))  !!}
                            </div>
                            @endif
                            <div class="form-group col-sm-4">

                                {!!  Form::select('report_type_id', $report_type, Request::input('report_type_id'), array('class' => 'form-control','id' => 'report_type_id' ))  !!}
                            </div>
                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                @if($customers != '')
                                <a href="{{ URL::to('report/customer') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                                @else
                                <a href="{{ URL::to('customer/reports') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                                @endif
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>





        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Records</h3>
                <!--<a href="{{ URL::to('inventory/generate/report') }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>-->

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Transaction ID</th>
                                    <th>Customer Name</th>
                                    <th>Start Bal</th>
                                    <th>Amount</th>
                                    <th>End Bal</th>
                                    <th>Transaction Type</th>
                                    <th>Transaction Details </th>
                                    <!--<th>Transaction Date</th>-->
                                    <th>Statement Date</th>

                                </tr>
                            </thead>
                            <tbody>

                                @if(count($model) >0)
                                <?php $i = 1; ?>
                                @foreach($model as $item)
                                <tr>

                                    <td>{{ $item->id }}</td>
                                    
                                    <td>{{ $item->customer_name }}</td>
                                    
                                    <td>{{ $item->previous_balance }}</td>
                                    
                                    @if (strpos($item->amount, '-') !== false || $item->payment_type == 'invoice')
                                    <td>{{ str_replace("-", "", $item->amount) }}</td>
                                     @elseif($item->payment_type == 'credit_memo' && $item->transaction_type == 'positive')
                                     <td>{{ $item->amount }}</td>
                                     @else
                                     <td>{{ '-'.$item->amount }}</td>
                                     @endif

                                    <td>{{ $item->updated_balance }}</td>
                                    
                                    @if($item->payment_type == 'direct')
                                    <td>Payment</td>
                                    @else
                                    <td>{{ ucfirst(str_replace("_"," ",$item->payment_type)) }}</td>
                                    @endif
                                    
                                    @if($item->payment_type == 'direct')
                                    @if($item->modification_count > 0)
                                    <td>PMT{{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</td>
                                    @else
                                     <td>{{$item->initials . $item->id}}</td>
                                    @endif
                                    @elseif($item->payment_type == 'invoice')
                                    @if($item->modification_count > 0)
                                    <td> <a href="{{ url('invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</a></td>
                                    @else
                                    <td> <a href="{{ url('invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }}</a></td>
                                    @endif
                                    @else

                                    @if($item->modification_count > 0)
                                    <td> <a href="{{ url('credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</a></td>
                                    @else
                                    <td> <a href="{{ url('credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }}</a></td>
                                    @endif
                                    @endif

                                    <td><?php echo date("d M Y", strtotime($item['statement_date'])); ?></td>

                                </tr>
                                <?php $i++; ?>
                                @endforeach

                                @endif


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>

                    </div></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')

<script>


    
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "pageLength": {{Config::get('params.reports_list_length')}},
            dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'customer_transactions',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
            },
            {
            extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'customer_transactions',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
            }
            ]
    });
    });
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    });
</script>

@endsection



