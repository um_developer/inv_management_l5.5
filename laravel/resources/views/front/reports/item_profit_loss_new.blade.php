@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

@section('content')
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    {!! Form::open(array( 'class' => '','url' => 'report/item/sale-profit-search-new', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-3">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div></div>


                            <div class="form-group col-sm-4">

                                {!!  Form::select('item_id', $items, Request::input('item_id'), array('multiple'=>'multiple', 'name'=>'item_id[]','class' => 'form-control select2','id' => 'item_id' ))  !!}
                            </div>
                            <div class="form-group col-md-4">
                            <select id="type" name="selected_type" class="form-control">
                                    
                                    <option value="customer" <?php echo ($selected_type == 'customer') ? 'selected' : ''; ?>>Customer</option>
                                    <?php if($selected_type == 'vendor'){  ?>
                                        <option value="vendor" selected="selected">Vendor</option>
                                    <?php }else{ ?>
                                        <option value="vendor">Vendor</option>
                                    <?php } ?>
                                </select>
                               
                            </div>

                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
        $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>


                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('report/item/sale-profit-new/') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>



        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Item Sales and Profit Report New</h3>
                <!--<a href="{{ URL::to('inventory/generate/report') }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>-->

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Item ID</th>
                                    <th>Item Name</th>
                                    @if($selected_type == 'vendor')
                                    <th>Vendor Name</th>
                                    @else
                                    <th>Customer Name</th>
                                    @endif
                                    <th>Sale Amount</th>
                                    <th>Profit Amount</th>
                                    <th>Total Item Quantity Sold</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $total_sale_amount = 0;
                                $total_profit = 0;
                                $total_quantity = 0;
                                ?>

                                @if(count($array) >0)
                                <?php $i = 1; ?>
                                @foreach($array as $item)
                                <?php

                                $f_sale = $item->invoice_sale_amount - $item->credit_memo_sale_amount;
                                $f_profit = $item->profit_amount - $item->credit_memo_loss_amount;
                                $f_quantity = $item->invoice_quantity - $item->credit_memo_quantity;
                                ?>
                                <tr>
                                <td>{{ $item->item_id }}</td>
                                    <td>{{ $item->item_name }}</td>
                                     @if($selected_type == 'vendor')
                                    <td>{{ $item->vendor_name }}</td>
                                    @else
                                     <td>{{ $item->customer_name }}</td>
                                    @endif
                                    <td>{{ $f_sale }}</td>
                                    <td>{{ $f_profit }}</td>
                                    <td>{{ $f_quantity }}</td>
                                </tr>
                                <?php
                                $i++;
                                $total_sale_amount = $total_sale_amount + $f_sale;
                                $total_profit = $total_profit + $f_profit;
                                $total_quantity = $total_quantity + $f_quantity;
                                
                                ?>
                                @endforeach
                                <tr>
                                <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Total Sale ${{ $total_sale_amount }}</th>
                                    <th>Total Profit ${{ $total_profit }}</th>
                                    <th>Total Quantity {{ $total_quantity }}</th>
                                </tr>
                                @endif


                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>

                    </div></div>
            </div>

            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": false,
         "pageLength": {{Config::get('params.reports_list_length')}},
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'Item Sale / Profit Report',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
                },
                {
                    extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'Item Sale / Profit Report',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
                }
            ]
        });
    });

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
    });

</script>

@endsection



