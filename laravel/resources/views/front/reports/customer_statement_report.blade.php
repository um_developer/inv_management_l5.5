@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')
<?php 
$base_url = '';
if(Auth::user()->role_id == 4 ){
  $base_url = 'customer/';  
}

?>
@section('content')
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        @if($customers != '')
                        <h3 class="box-title">Filter by Customer</h3>
                        @else
                        <h3 class="box-title">Filter by Date</h3>
                        @endif
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    @if($customers != '')
                    {!! Form::open(array( 'class' => '','url' => 'report-by-customer-statement', 'method' => 'post')) !!}
                    @else
                    {!! Form::open(array( 'class' => '','url' => 'customer/statement-report-date', 'method' => 'post')) !!}
                    @endif
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-sm-4">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div>
                            </div>

                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>

                            @if($customers != '')
                            <div class="form-group col-sm-4">

                                {!!  Form::select('customer_id', $customers, Request::input('customer_id'), array('class' => 'form-control select2','id' => 'customer_id' ))  !!}
                            </div>
                            @endif

                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                @if($customers != '')
                                <a href="{{ URL::to('report/customer-statement') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                                @else
                                <a href="{{ URL::to('customer/statement-report') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                                @endif
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>





        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Customer Statement Report</h3>
                <!--<a href="{{ URL::to('inventory/generate/report') }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>-->

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>S.#</th>
                                    <th>Transaction ID</th>
                                    <th>Customer Name</th>
                                    <th>Start Bal</th>
                                    <th>Amount</th>
                                    <th>End Bal</th>
                                    <th>Transaction Type</th>
                                    <th>Transaction Details </th>
                                    <!--<th>Transaction Date</th>-->
                                    <th>Payment Reference</th>
                                    <th>Statement Date</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $check = true; $invoice_scan_id = 000; ?>
                                @if(count($model) >0)
                                <?php $i = 1;
                                ?>
                                @foreach($model as $item)
                                
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->customer_name }}</td> 
                                    <td>{{number_format((float)$item->new_previous_balance, 2, '.', '')  }}
                                    </td>
                                     <td>{{ $item->new_total_modified_amount }}</td>
                                    <td>
                                        {{number_format((float)$item->new_updated_balance, 2, '.', '')  }}
                                    
                                    </td>
                                    @if($item->payment_type == 'direct')
                                    <td>Payment</td>
                                    @else
                                    <td>{{ ucfirst(str_replace("_"," ",$item->payment_type)) }}</td>
                                    @endif
                                    
                                    @if($item->payment_type == 'direct')
                                    @if($item->modification_count > 0)
                                    <td>PMT{{ $item->payment_type_id }}</td>
                                    @else
                                     <td>{{$item->initials . $item->id}}</td>
                                    @endif
                                    @elseif($item->payment_type == 'invoice')
                                    <?php
                                    if ($check == true) {
                                        $invoice_scan_id = $item->payment_type_id;
                                        $check = false;
                                    }
                                        
                                    ?>
                                    @if($item->modification_count > 0)
                                    <td> <a href="{{ url($base_url.'invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }}</a></td>
                                    @else
                                    <td> <a href="{{ url($base_url.'invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }}</a></td>
                                    @endif
                                    @else

                                    @if($item->modification_count > 0)
                                    <td> <a href="{{ url($base_url.'credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }}</a></td>
                                    @else
                                    <td> <a href="{{ url($base_url.'credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }}</a></td>
                                    @endif

                                    @endif
                                    <td>{{$item->payment_reference}}</td>
                                    <td><?php echo date("d M Y", strtotime($item['statement_date'])); ?></td>

                                </tr>

                                <?php $i++; ?>

                                @endforeach

                                @endif


                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>

                    </div></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')

<script>
    var a= 0;
    $(document).ready(function () {
        
//        table.fnSort( [ [0,'desc'] ] ); // Sort by first column descending
//    $('#vendors').DataTable({
//    "order": [[ 0, "desc" ]]
//    });
    });
    
    $(function () {

        function getBase64FromImageUrl(url) {
    var img = new Image();
		img.crossOrigin = "anonymous";
    img.onload = function () {
        var canvas = document.createElement("canvas");
        canvas.width =this.width;
        canvas.height =this.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    };
    img.src = url;
	}



            $('#vendors').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": false,
                    "info": false,
                    "autoWidth": false,
                    "pageLength": {{Config::get('params.reports_list_length')}},
                    dom: 'Bfrtip',
                    "buttons": [
                        {
                        extend: 'excelHtml5',
                                text: 'Export To Excel',
                                title: 'customer_statement_report',
                                messageTop: "{{ $print_page_info['top'] }}",
                                messageBottom: "{{ $print_page_info['bottom'] }}",
                        },
                        // {
                        // extend: 'pdfHtml5',
                        //         text: 'Export To PDF',
                        //         title: 'customer_statement_report',
                        //         messageTop: "{{ $print_page_info['top'] }}",
                        //         messageBottom: "{{ $print_page_info['bottom'] }}",
                        // },
				{
					text: 'Export Custom PDF',
					extend: 'pdfHtml5',
                    // title: 'Customer Statement Report',
					filename: 'customer_statement_report_new',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
					//orientation: 'landscape', //portrait
					// pageSize: 'A5', //A3 , A5 , A6 , legal , letter
					exportOptions: {
						columns: ':visible',
						search: 'applied',
						order: 'applied'
					},
					customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'/'+(now.getMonth()+1)+'/'+now.getFullYear();
						 
                         var newLogo ="data:image/png;base64,"+ "{{  DNS1D::getBarcodePNG( $invoice_scan_id, 'C39') }}"
                          


						
					 
                     var logo = newLogo;
						doc.pageMargins = [10,60,10,10];
						// Set the font size fot the entire document
						doc.defaultStyle.fontSize = 7;
						// Set the fontsize for the table header
						doc.styles.tableHeader.fontSize = 7;
						// Create a header object with 3 columns
						// Left side: Logo
						// Middle: brandname
						// Right side: A document title
						doc['header']=(function() {
							return {
								columns: [
									{
										image: logo,
										width: 70,
                                        height:100,
                                       
									},
                                    {
										// alignment: 'left',
										fontSize: 10,
										text: '  {{  $invoice_scan_id }}'
                                       
									},
									{
										alignment: 'center',
										text: 'Customer Statement Report',
										fontSize: 14,
										//  margin: [10,0]
									},

                                    {
										alignment: 'center',
										// italics: true,
										text: 'Print Date: '+jsDate,
										fontSize: 7,
										//  margin: [50,0]
									},
									
								],
								margin: 20
							}
						});
						// Create a footer object with 2 columns
						// Left side: report creation date
						// Right side: current page and total pages
						doc['footer']=(function(page, pages) {
							return {
								columns: [
									{
										alignment: 'left',
										text: ['Created on: ', { text: jsDate.toString() }]
									},
									{
										alignment: 'right',
										text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
									}
								],
								margin: 20
							}
						});
						// Change dataTable layout (Table styling)
						// To use predefined layouts uncomment the line below and comment the custom lines below
						// doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return .5; };
						objLayout['vLineWidth'] = function(i) { return .5; };
						objLayout['hLineColor'] = function(i) { return '#aaa'; };
						objLayout['vLineColor'] = function(i) { return '#aaa'; };
						objLayout['paddingLeft'] = function(i) { return 4; };
						objLayout['paddingRight'] = function(i) { return 4; };
						doc.content[0].layout = objLayout;
				}
				}
                ]
            });


    });
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    });
        $(document).ready(function () {
            $('#customer_id').select2({
              language: {
                noResults: function (params) {
                  alert("Customer not found. Please create this customer.");
                }
              }
            });
        });
</script>

 
@endsection



