@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

@section('content')
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    {!! Form::open(array( 'class' => '','url' => 'report/item/sale-profit-search', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-3">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div></div>


                            <div class="form-group col-sm-3">

                                {!!  Form::select('item_id', $items, Request::input('item_id'), array('class' => 'form-control select2','id' => 'customer_id' ))  !!}
                            </div>
                            @if(1==2)
                             <div class="form-group col-sm-3">

                                {!!  Form::select('customer_id', $customers, Request::input('customer_id'), array('class' => 'form-control select2','id' => 'customer_id' ))  !!}
                            </div>
                            @endif
                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
        $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>


                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('report/item/sale-profit/') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>





        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Item Sales and Profit Report</h3>
                <!--<a href="{{ URL::to('inventory/generate/report') }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>-->

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Sale Amount</th>
                                    <th>Profit Amount</th>
                                    <th>Total Item Quantity Sold</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $total_sale_amount = 0;
                                $total_profit = 0;
                                $total_quantity = 0;
                                ?>

                                @if(count($model) >0)
                                <?php $i = 1; ?>
                                @foreach($model as $item)
                                <?php
                                $f_sale = $item->invoice_sale_amount - $item->credit_memo_sale_amount;
                                $f_profit = $item->profit_amount - $item->credit_memo_loss_amount;
                                $f_quantity = $item->invoice_quantity - $item->credit_memo_quantity;
                                ?>
                                <tr>
                                    <td>{{ $item->item_name }}</td>
                                    <td>{{ $f_sale }}</td>
                                    <td>{{ $f_profit }}</td>
                                    <td>{{ $f_quantity }}</td>
                                </tr>
                                <?php
                                $i++;
                                $total_sale_amount = $total_sale_amount + $f_sale;
                                $total_profit = $total_profit + $f_profit;
                                $total_quantity = $total_quantity + $f_quantity;
                                ?>
                                @endforeach
                                <tr>
                                    <th></th>
                                    <th>Total Sale ${{ $total_sale_amount }}</th>
                                    <th>Total Profit ${{ $total_profit }}</th>
                                    <th>Total Quantity {{ $total_quantity }}</th>
                                </tr>
                                @endif


                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>

                    </div></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
         "pageLength": {{Config::get('params.reports_list_length')}},
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'Item Sale / Profit Report',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
                },
                {
                    extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'Item Sale / Profit Report',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
                }
            ]
        });
    });

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
    });

</script>

@endsection



