@extends('customer_c_template')

@section('content')
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                 @include('front/common/client_common_tabs')
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filter by Client</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>

                    {!! Form::open(array( 'class' => '','url' => 'customer/report-by-client', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-sm-4">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div>
                            </div>

                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>

                            <div class="form-group col-sm-4">

                                {!!  Form::select('client_id', $clients, Request::input('client_id'), array('class' => 'form-control','id' => 'client_id' ))  !!}
                            </div>

                            <div class="form-group col-sm-4">

                                {!!  Form::select('report_type_id', $report_type, Request::input('report_type_id'), array('class' => 'form-control','id' => 'report_type_id' ))  !!}
                            </div>
                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('customer/report/client') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>





        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Records</h3>
                <!--<a href="{{ URL::to('inventory/generate/report') }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>-->

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Transaction ID</th>
                                    <th>Client Name</th>
                                    <th>Start Bal</th>
                                    <th>Amount</th>
                                    <th>End Bal</th>
                                    <th>Transaction Type</th>
                                    <th>Transaction Details </th>
                                    <!--<th>Transaction Date</th>-->
                                    <th>Statement Date</th>

                                </tr>
                            </thead>
                            <tbody>

                                @if(count($model) >0)
                                <?php $i = 1; ?>
                                @foreach($model as $item)
                                <tr>

                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->client_name }}</td>
                                    <td>{{ $item->previous_balance }}</td>
                                    
                                     
                                     @if (strpos($item->amount, '-') !== false && $item->payment_type == 'order')
                                    <td>{{ str_replace("-", "", $item->amount) }}</td>
                                     @else
                                     <td>{{ $item->amount }}</td>
                                     @endif
                                     
                                    <td>{{ $item->updated_balance }}</td>
                                    @if($item->payment_type == 'direct')
                                    <td>Payment</td>
                                    @else
                                    <td>{{ ucfirst(str_replace("_"," ",$item->payment_type)) }}</td>
                                    @endif
                                    @if($item->payment_type == 'direct')
                                    @if($item->modification_count > 0)
                                    <td>PMT{{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</td>
                                    @else
                                    <td>{{$item->initials  . $item->id }}</td>
                                    @endif
                                    @elseif($item->payment_type == 'order')
                                    @if($item->modification_count > 0)
                                    <td> <a href="{{ url('customer/order/'.$item->payment_type_id) }}" target="_blank">Order# {{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</a></td>
                                    @else
                                    <td> <a href="{{ url('customer/order/'.$item->payment_type_id) }}" target="_blank">Order# {{ $item->payment_type_id }}</a></td>
                                    @endif
                                    @else
                                    @if($item->modification_count > 0)
                                    <td> <a href="{{ url('customer/order-return/'.$item->payment_type_id) }}" target="_blank">Return Order# {{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</a></td>
                                    @else 
                                    <td> <a href="{{ url('customer/order-return/'.$item->payment_type_id) }}" target="_blank">Return Order# {{ $item->payment_type_id }}</a></td>
                                    @endif
                                    @endif
@if(1==2)
                                    <td><?php echo date("d M Y", strtotime($item['created_at'])); ?></td>
                                    @endif
                                    <td><?php echo date("d M Y", strtotime($item['statement_date'])); ?></td>

                                </tr>
                                <?php $i++; ?>
                                @endforeach

                                @endif


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>


                    </div></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')

<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "pageLength": {{Config::get('params.reports_list_length')}},
            dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'Client Transactions',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
            },
            {
            extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'Client Transactions',
                    messageTop: "{{ $print_page_info['top'] }}",
                    messageBottom: "{{ $print_page_info['bottom'] }}",
            }
            ]
    });
    });
</script>

@endsection



