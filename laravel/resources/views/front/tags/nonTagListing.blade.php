@extends('customer_template')

@section('content')
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<?php  $type0 = basename(Request::url()); ?>
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif

<section id="form" class="mt30 mb30 col-sm-12 p0">
<?php if (1 == 2) {?>
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

                {!! Form::open(array( 'class' => '','url' => 'itemTags', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">

                    <div class="form-group col-sm-6">
                        {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control ','id' => 'category_id' ))  !!}
                    </div>
                    <div class="form-group col-sm-6">
                    <select class ='form-control' name="status" >
                    <option value="" <?php if(isset($status) && $status == ""){ ?> selected <?php }?>>Select Status</option> 
                    <option value="1" <?php if(isset($status) && $status == 1){ ?> selected  <?php }?>>Active</option>
                    <option value="0"<?php if(isset($status) && $status == 0){ ?> selected  <?php }?>>inactive </option>
                    </select>
                    </div>

                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('itemTags') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>
<?php } ?>
<style>
    .modal-lg {
  max-width: 900px;
}
</style>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Non Tag Items Listing</h3>
                <a href="{{ url('/tags') }}" class="btn btn-primary pull-right" style="margin-right: 15px;"><i class="fa fa-plus"></i> Back to Tags</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>IMG</th>
                            <th>ID</th>
                            <th>SKU/Item Code</th>
                            <th>Item Name</th>
                             <th>Action</th>
                            <!-- <th>Serial</th>
                            <th>UPC/Barcode</th>
                            <th>Cost</th>
                            <th>QTY</th>
                            <th>State</th> -->
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        <?php foreach($model as $item){ ?>
                        <tr>
                            <?php
                            $quantity = 0;
                            $quantity = $item->warehouse_quantity + $item->inventory_quantity + $item->package_quantity + $item->sub_item_quantity;
                            ?>
                            @if($item->image == '')
                            <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                            @else
                             <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>
                            @endif
                            <td>{{$item->id}}</td>
                            <td>{{$item->code}}</td>
                            <td>{{$item->name}}</td>
                             <td><a href="{{ url('item/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                            <!-- <td>{{$item->serial}}</td>
                            <td>{{$item->upc_barcode}}</td>
                            <td>{{$item->cost}}</td>
                            <td>{{$quantity}}</a></td>
                            <td>
                                @if($item->status == 1)
                                    <input type="checkbox" checked data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @else
                                    <input type="checkbox" data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @endif
                            </td> -->
                        </tr>
                        <?php } ?>
                        @endif
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
                </div></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>



<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@include('front/common/dataTable_js')
<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export To Excel',
                title: 'item_list',
                exportOptions: {
                    columns: [1,2,3,4,5,6,7,8,9,10]
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Export To PDF',
                title: 'item_list',
               exportOptions: {
                    columns: [1,2,3,4,5,6,7,8,9]
                }
            }
        ]
        });
    });
    
    function changeItemStatus(item_id) {

        $.ajax({
            url: "<?php echo url('item/status/'); ?>" + '/0/' + item_id,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                console.log(response);
                if (response == 1) {
//                    var modal = document.getElementById('myModal');
//                    modal.style.display = "block";
//                    alert(response);

                } else if (response == 0) {

                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });
    }
</script>
<script>
  $(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'Active',
      off: 'Disabled'
    });
  })

  $(document).ready(function () {
            $('#category_id').on('select2:select', function (e) {});
            $('#category_id').select2({
              language: {
                noResults: function (params) {
                  alert("This category is not existing");
                }
              }
            });
        });
</script>

@include('front/common/image_modal')

@endsection