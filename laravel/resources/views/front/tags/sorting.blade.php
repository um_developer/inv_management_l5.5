@extends('customer_template')

@section('content')

<section id="form" class="mt30 mb30 col-sm-12 p0">
<style>
#tags td 
{
    text-align: center; 
    vertical-align: middle;
}
</style>
    <div class="row">
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <?php  $type = basename(Request::url()); ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tags Sorting Order</h3>
            </div>
            
                <div class="box-body">
                    <ul id="sortable" >
                        @if(count($modal) > 0)
                        @foreach($modal as $row)
                            <li class="ui-state-default getSortOrder" id="<?php echo $row->id; ?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php echo $row->title ?></li>
                        @endforeach
                        @endif
                    </ul>
                </div>

        </div>
    </div>			
</section>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );

  $( function() {
  $( "#sortable" ).sortable({

    stop: function(event, ui) {
      var itemOrder = $('#sortable').sortable("toArray");
      mydata = [];
      for (var i = 0; i < itemOrder.length; i++) {
        mydata[i] = itemOrder[i];
      }

        $.ajax({
            url: "<?php echo url('tags/sortOrder/update'); ?>",
            type: 'post',
            data: {
                "_token": "{{ csrf_token() }}",
                "data": mydata
            },
            success: function (response) {
                console.log(response);
                
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });
    }
  });

});
  </script>

@endsection



