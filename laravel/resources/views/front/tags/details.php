@extends('customer_template')

@section('content')
<!-- <?php
// $required = 'required';
?> -->
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Tags.. </h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'sample/create', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Title</label>
                    {!! Form::text('title', Request::input('title') , array('placeholder'=>"Title *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                </div>
            </div>



            <div class="tags-area">
                <div class="row">

                <div class="col-md-12" id="wrapper">
                    <div  class=" col-md-6">
                        <h4>All Tags <button class="btn btn-primary" id="moveAll">Move All</button> <button class="btn btn-primary" id="moveSelected">Move Selected</button> </h4> 
                        <div id="origin" class="tagbox">
                            <label class="draggable">
                                <input value="tag1" type="checkbox" >    
                                Tag #1
                            </label>
                            <label class="draggable">
                                <input value="tag1" type="checkbox"  >    
                                Tag #2
                            </label>
                            <label class="draggable">
                                <input value="tag1" type="checkbox"  >    
                                Tag #3
                            </label>
                            <label class="draggable">
                                <input value="tag1" type="checkbox"  >    
                                Tag #4
                            </label>
                            <label class="draggable">
                                <input value="tag1" type="checkbox"  >    
                                Tag #5
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h4>Selected Tags <button class="btn btn-primary" id="restoreAll">Restore All</button> </h4>
                        <div id="drop"  class="tagbox"></div>
                    </div>
                </div>
                </div>
            
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Create">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>



</section>


@endsection