@extends('customer_template')

@section('content')

<section id="form" class="mt30 mb30 col-sm-12 p0">
<style>
#tags td 
{
    text-align: center; 
    vertical-align: middle;
}
</style>
    <div class="row">
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <?php  $type = basename(Request::url()); ?>
        <div class="box">
            <div class="box-header">
                @if($type == 'tags')
                    <h3 class="box-title">Tags Listing</h3>
                @else
                    <h3 class="box-title">Sub-Tags Listing</h3>
                @endif
                <a href="{{ url('tags/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create New Tag</a>
                @if($type == 'tags')
                    <a href="{{ url('tags/subtags/') }}" class="btn btn-primary pull-right" style="margin-right: 30px;"><i class="fa fa-list"></i> Sub Tags Listing</a>
                @else
                    <a href="{{ url('tags/') }}" class="btn btn-primary pull-right" style="margin-right: 30px;"><i class="fa fa-list"></i> Tags Listing</a>
                @endif
                <a href="{{ url('itemTags/') }}" class="btn btn-primary pull-right" style="margin-right: 30px;"><i class="fa fa-list"></i> Non Tags Items</a>
                @if($type == 'tags')
                    <a href="{{ url('tags/Sorting/') }}" class="btn btn-primary pull-right" style="margin-right: 30px;"><i class="fa fa-list"></i> Sort Order</a>
                @endif
            </div>
            <div class="box-body">
                <table id="tags" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            @if($type == 'tags')
                            <th>Tag Name</th>
                            @else
                            <th>Sub-Tag Name</th>
                            @endif
                            <th>Description</th>
                            <th>Image</th>
                            <th>Date Created</th>
                            <th>Status</th>
                            <th>Items/Sub-Tags</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($modal) > 0)
                        <?php $i = 1; ?>
                        @foreach($modal as $item)
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->description}}</td>
                            <td>No Image</td>
                            <td>{{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y g:i A') }}</td>
                            <td>
                              @if($item->status == 1)
                                <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}')" checked data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @else
                               <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}')" data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @endif
                            </td>
                            <td>
                                <?php if ($item->has_items == 1) { ?>
                                    Items
                                <?php }elseif ($item->has_items == 2) { ?>
                                    Sub-Tags
                                <?php } ?>
                            </td>
                            <td>
                            @if($type == 'tags')
                                <a href="{{ url('tags/details/?type=tag&id='.$item->id) }}" class="btn btn-primary"><i class="fa fa-info-circle"></i></a>
                            @else
                            <a href="{{ url('tags/details/?type=subtag&id='.$item->id) }}" class="btn btn-primary"><i class="fa fa-info-circle"></i></a>
                            @endif
                                <a href="{{ url('tags/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('tags/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                            <?php $i++; ?>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
    </div>			
</section>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
    $(function () {
        $('#tags').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
        });
    });
function changeItemStatus(tag_id) {

$.ajax({
    url: "<?php echo url('tag/status/'); ?>" + '/' + tag_id,
    type: 'get',
    success: function (response) {},
    error: function (xhr, status, response) {
        alert(response);
    }
});

}
$(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'Active',
      off: 'Disabled'
    });
  })
</script>
@endsection



