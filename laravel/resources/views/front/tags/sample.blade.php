

@extends('customer_template')
@section('content')
<?php
  $required = 'required';
  ?>
<section id="form">
  <div class="box box-primary">
  <div class="box-header with-border text-center">
    <h3 class="box-title">Tags.. </h3>
  </div>
  <!-- /.box-header -->
  @if (session('success'))
  <div class="alert alert-success">
    {{ session('success') }}
  </div>
  @endif
  @if (session('error'))
  <div class="alert alert-danger">
    {{ session('error') }}
  </div>
  @endif
  @if (count($errors->form) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->form->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif 
  {!! Form::open(array( 'class' => 'form-horizontal','url' => 'tags/details/create', 'method' => 'post')) !!}
  <input type="hidden"  name="tagID" value="<?php echo $tag->id; ?>">
  <div class="box-body">
    <div class="form-group">
      <div class="col-sm-12">
        <label for="exampleInputEmail1">Title</label>
        {!! Form::text('title', $tag->title , array('placeholder'=>"Title *",'maxlength' => 100,'class' => 'form-control', 'readonly') ) !!}
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-12">
        <label for="exampleInputEmail1">Description</label>
        {!! Form::text('title', $tag->description , array('placeholder'=>"Description *",'maxlength' => 100,'class' => 'form-control', 'readonly') ) !!}
      </div>
    </div>
    <?php if($tag->is_subtag == 0){ ?>
    <div class="form-group">
      <div class="col-sm-12">
        <label for="exampleInputEmail1">Select An option</label>
        <?php if ($tag->has_items == 1) { ?>
        <input type="radio" name="type" class="type" value="1" checked /> Item
        <?php }else{ ?>
        <input type="radio" name="type" class="type" value="1" autocomplete="off"/> Item
        <?php } ?>
        <?php if ($tag->has_items == 2) { ?>
        <input type="radio" name="type" class="type" value="2" checked /> Sub-Tag
        <?php }else{ ?>
        <input type="radio" name="type" class="type" value="2" autocomplete="off" /> Sub-Tag
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <?php if($tag->is_subtag == 1){ ?>
    <div class="tags-area">
      <div class="row">
        <input type="hidden" id="selectedType" name="selectedType" value="1">
        <div class="col-md-12" id="wrapper">
          <div  class="col-md-5" style="margin-left: 4%">
            <div class="col-md-7 row">
              <a class="btn btn-primary " id="selectAllOrigin">Select All</a>
            </div>
            <select class="btn  btn-default col-md-offset-1 col-md-4" id="cat0">
              <option value="all" selected="selected">All</option>
              @foreach($categories as $row)
              <option value="{{$row->category_id}}">{{$row->category_name}}</option>
              @endforeach
            </select>
            <div id="origin" class="tagbox col-md-12">
              <div class="form-group col-md-12">
                <input class="form-control" type="search" id="searchTag1"  placeholder="Search..." />
              </div>
              <?php if(count($modal) > 0){ ?>
              <select id="select1" class="selected_opt" name="items[]" multiple>
                <?php foreach($modal as $row){ ?>
                <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                <?php } ?>
              </select>
              <?php }else{ ?>
              <select id="select1" name="items[]" multiple autocomplete="off"></select>
              <?php } ?>
            </div>
          </div>
          <div class="tags__center col-md-1">
            <a class="btn btn-primary" id="add">&#8702;</a>
            <br><br>
            <a  class="btn btn-primary" id="remove">&#8701;</a>
          </div>
          <div class="col-md-5">
            <a class="btn btn-primary" id="selectAllDrop">Select All</a>
            <input class="btn btn-primary" type="button" value="Up">
            <input  class="btn btn-primary" type="button" value="Down">
            <div id="drop" class="tagbox col-md-12">
              <div class="form-group col-md-12">
                <input class="form-control" type="search" id="searchTag2" placeholder="Search..." />
              </div>
              <?php if (count($Items) > 0) { ?>
              <select id="select2" name="items[]" multiple autocomplete="off">
                <?php foreach($Items as $row){ ?>
                <option value="<?php echo $row->id; ?>" selected="selected"><?php echo $row->name; ?></option>
                <?php } ?>
              </select>
              <?php }else{ ?>
              <select id="select2" name="items[]" multiple autocomplete="off"></select>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <?php }elseif($tag->is_subtag == 0){ ?>
      <div class="tags-area">
        <div class="row">
          <input type="hidden" id="selectedType" name="selectedType" value="1">
          <div class="col-md-12" id="wrapper">
            <div  class="col-md-5" style="margin-left: 4%">
              <div class="col-md-7 row">
                <a class="btn btn-primary " id="selectAllOrigin">Select All</a>
              </div>
              <select class="btn  btn-default col-md-offset-1 col-md-4" id="cat0">
                <option value="all">All</option>
                @foreach($categories as $row)
                <option value="{{$row->category_id}}">{{$row->category_name}}</option>
                @endforeach
              </select>
              <div id="origin" class="tagbox myclass0  col-md-12">
                <div class="form-group col-md-12">
                  <input class="form-control" type="search" id="searchTag1"  placeholder="Search..." />
                </div>
                <?php if(count($modal) > 0){ ?>
                <select id="select1" class="selected_opt" name="items[]" multiple>
                  <?php foreach($modal as $row){ ?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                  <?php } ?>
                </select>
                <?php } ?>
              </div>
            </div>
            <div class="tags__center col-md-1">
              <a class="btn btn-primary" id="add">&#8702;</a>
              <br><br>
              <a  class="btn btn-primary" id="remove">&#8701;</a>
            </div>
            <div class="col-md-5">
              <a class="btn btn-primary" id="selectAllDrop">Select All</a>
              <input class="btn btn-primary" type="button" value="Up">
              <input  class="btn btn-primary" type="button" value="Down">
              <!-- <select class="btn  btn-default col-md-offset-1 col-md-4">
                <option value="1">Category 1</option>
                <option value="2">Category 2</option>
                <option value="3">Category 3</option>
                <option value="4">Category 4</option>
                </select>
                -->
              <div id="drop"  class="tagbox col-md-12">
                <div class="form-group col-md-12">
                  <input class="form-control" type="search" id="searchTag2" placeholder="Search..." />
                </div>
                <select class="selected_t" id="select2" name="items[]" multiple>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <input type="submit" name="submit" id="createTagBtn" class="btn btn-primary pull-right" value="Create">
    </div>
    <!-- /.box-footer -->
    {!! Form::close() !!} 
  </div>
</section>
<script>
  $(document).ready(function () {
    $('#cat0').on('change', function() {
            var cat_id = this.value;
            var tag_id = '<?php echo $tag->id; ?>';
            $.ajax({
              url: "<?php echo url('tags/getItems'); ?>" + '?type=cat&cat_id=' +cat_id+ '&id='+ tag_id,
              type: 'get',
              success: function (response) {
                      $(".selected_opt").empty();
                      response.forEach(function(entry) {
                          var content = "<option value='"+entry.id+"'>"+entry.name+"</option>"
                          $(".selected_opt").append(content);
                      });
              },
              error: function (xhr, status, response) {
                  console.log(response);
              }
          });
          });
      var t = '<?php echo $tag->is_subtag; ?>';
  
      if (t == 0) {
          var ty = '<?php echo $tag->has_items; ?>';
      
      if (ty == 1) {
          var sub = '<?php echo json_encode($Items); ?>';
          document.getElementById("cat0").style.display = "block";
      }else{
          var sub = '<?php echo json_encode($subTag); ?>';
          document.getElementById("cat0").style.display = "none";
      }
  
      if (sub.length > 0) {
          var obj = JSON.parse(sub);
          $(".selected_t").empty();
  
          obj.forEach(function(entry) {
              var content0 = "<option value='"+entry.id+"' selected='selected'>"+entry.name+"</option>"
              $(".selected_t").append(content0);
          });        
      }
  
         $('.type').click(function () {
          var type = $(this).val();
             var id = '<?php echo $tag->id; ?>';
             $('#selectedType').val(type);
             var a = '<?php echo $tag->is_subtag ?>';
             if (a == 0) {
                  if (type == 2) {
                    document.getElementById("cat0").style.display = "none";
                      <?php if (isset($subTag)) {  ?>
                      var sub = '<?php echo json_encode($subTag); ?>';
                      var obj = JSON.parse(sub);
                      $(".selected_t").empty();
                      $(".selected_t").val("");
                      obj.forEach(function(entry) {
                          var content0 = "<option value='"+entry.id+"' selected='selected'>"+entry.name+"</option>"
                          $(".selected_t").append(content0);
                      });
                     <?php } ?>
  
                  }else{
                    document.getElementById("cat0").style.display = "block";
                      var sub = '<?php echo json_encode($Items); ?>';
                      var obj = JSON.parse(sub);
                      $(".selected_t").empty();
                      $(".selected_t").val("");   
                      obj.forEach(function(entry) {
                          var content0 = "<option value='"+entry.id+"' selected='selected'>"+entry.name+"</option>"
                          $(".selected_t").append(content0);
                      });
                  }
             }else{}
  
  
             $.ajax({
              url: "<?php echo url('tags/getItems'); ?>" + '?type=' + type + '&id=' +id,
              type: 'get',
              success: function (response) {
                      $(".selected_opt").empty();
                      response.forEach(function(entry) {
                          var content = "<option value='"+entry.id+"'>"+entry.name+"</option>"
                          $(".selected_opt").append(content);
                      });
              },
              error: function (xhr, status, response) {
                  console.log(response);
              }
          });
  
  
  
         });
      }
     });
</script>
@endsection

