@extends('customer_template')

@section('content')

<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ $account_detail->title }} (Balance: {{ $account_detail->amount }})</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Transaction ID</th>
                            <th>User Name</th>
                            <th>Start Bal</th>
                            <th>Amount</th>
                            <th>End Bal</th>
                            <th>Transaction Detail</th>
                            <th>Transaction Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                         <?php $i = 1; ?>
                        @foreach($model as $item)
                        <tr>
                            <td>{{$i}}</td>
                              <td>{{$item->customer_name}}</td>
                               <td>{{$item->previous_balance}}</td>
                            <td>{{$item->amount}}</td>
                             <td>{{$item->updated_balance}}</td>
                            @if($item->source_type == 'payment')
                             @if($item->modification_count == 0)
                              <td>From PMT{{ $item->payment_id }}</td>
                              @else
                               <td> PMT{{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</td>
                               @endif
                             @elseif($item->source_type == 'delete')
                            <td>Delete From Payment ID {{ $item->payment_id }}</td>
                            @else
                            @if(strpos($item->amount, '-') !== false)
                            <td>Transfer to {{ $item->account_title }}</td>
                            @else
                            <td>Credit from {{ $item->account_title }}</td>
                            @endif
                            @endif
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                        </tr>
                         <?php $i++; ?>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>
@include('front/common/dataTable_js')
<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: "{{ $account_detail->title . ' Transactions' }}",
                    
            },
            {
            extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: "{{ $account_detail->title . ' Transactions' }}",
                   
            }
            ]
    });
    });
</script>
@endsection



