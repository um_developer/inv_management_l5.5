@extends('customer_template_2')
@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Update Account</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'account-type/update', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
                    <label for="exampleInputEmail1">Title</label>
                    {!! Form::text('title', $model->title , array('placeholder'=>"Type Name *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Show For Admin</label>
                    {!! Form::checkbox('for_admin',1,$model->for_admin, array('id'=>'for_admin','class' => 'flat-red')) !!}
                </div> 

                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Show For Customer</label>
                    {!! Form::checkbox('for_customer',1,$model->for_customer, array('id'=>'for_customer','class' => 'flat-red')) !!}
                </div> 


            </div> 

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

    <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js')}}"></script>	
    <script>

//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-purple',
    radioClass: 'iradio_flat-purple'
});
    </script>
</section>

@endsection