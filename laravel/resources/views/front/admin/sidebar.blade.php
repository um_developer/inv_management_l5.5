<!-- Left side column. contains the logo and sidebar -->

<?php

use App\Categories;
use App\Functions\Functions;
use App\Cart;
?>
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <style>
                /*li.treeview.multi-links a {
                    padding: 12px 30px 12px 15px;
                }*/

                .sidebar-menu>li>a.agnle_anchor {
                    position: absolute;
                    right: 0px;
                    left: auto;
                    top: 0;
                    border: 0;
                    padding: 0;
                    z-index: 99;
                    width: 30px;
                }
                li.treeview.multi-links {
                    position: relative;
                }
                li.multi-links>a:nth-child(2) {
                    min-height:40px;    
                }
            </style>



            @if(Auth::user()->role->role == 'client')



            <li><a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li><a href="{{ url('/customer-care-managers') }}"><i class="fa fa-dashboard"></i> <span>Customer Care Manager</span></a></li>
            
            <li><a href="{{ url('payment/customer/create-adjust/') }}"><i class="fa fa-dashboard"></i> <span>Adjust Statement</span></a></li>
            
            <li><a href="{{ url('item-gallery/') }}"><i class="fa fa-dashboard"></i> <span>Item Gallery</span></a></li>

            <li><a href="{{ url('add-new-payment/') }}"><i class="fa fa-dashboard"></i> <span>Add New Payment</span></a></li>
            
            <li><a href="{{ url('lists/') }}"><i class="fa fa-dashboard"></i> <span>List</span></a></li>

            <li><a href="{{ url('tags') }}"><i class="fa fa-dashboard"></i> <span>Tags</span></a></li>
<li><a href="{{ url('priceImpect/') }}"><i class="fa fa-dashboard"></i> <span>Update Price From RO</span></a></li>
            <li class="treeview multi-links active">
                <a href="{{ url('/vendors') }}">
                    <i class="fa fa-user"></i> <span>Vendor</span>
                </a>
                <a class="agnle_anchor" href="{{ url('') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu menu-open" style="">
                    <li><a href="{{ url('/vendors') }}"><i class="fa fa-table"></i>Vendors Listing</a></li>
                    <li><a href="{{ url('/purchase-orders') }}"><i class="fa fa-circle-o"></i>Purchase Orders</a></li>
                    <li><a href="{{ url('payments/vendor') }}"><i class="fa fa-table"></i>Vendor Payments</a></li>
                    {{-- <li><a href="{{ url('return-vendor') }}"><i class="fa fa-table"></i>Return Vendor</a></li> --}}
                    <li><a href="{{ url('receive-items') }}"><i class="fa fa-circle-o"></i>Receive Items</a></li>
                    <li><a href="{{ url('/inventory') }}"><i class="fa fa-circle-o"></i>Current Inventory</a></li>
                     <li><a href="{{ url('/inventory-logs') }}"><i class="fa fa-circle-o"></i>Update Inventory Logs</a></li>
                    <li><a href="{{ url('warehouses') }}"><i class="fa fa-table"></i>Warehouse List</a></li>

                </ul>
            </li>


            <li class="treeview multi-links active">
                <a href="{{ url('/customers') }}">
                    <i class="fa fa-user"></i> <span>Customers</span>
                </a>
                <a class="agnle_anchor" href="{{ url('') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu menu-open" style="">
                    <li><a href="{{ url('/customers') }}"><i class="fa fa-table"></i>Customer Listing</a></li>
                    <li><a href="{{ url('/invoices') }}"><i class="fa fa-table"></i>Invoices</a></li>
                    <li><a href="{{ url('/credit-memo') }}"><i class="fa fa-table"></i>Credit Memo</a></li>
                    <li><a href="{{ url('/payments/customer') }}"><i class="fa fa-table"></i>Customer Payments</a></li>
                    <li><a href="{{ url('/payment-types') }}"><i class="fa fa-table"></i>Payment Types</a></li>
                    <li><a href="{{ url('/account-types') }}"><i class="fa fa-table"></i>Accounts</a></li>
                    <li><a href="{{ url('report/customer-statement') }}"><i class="fa fa-table"></i>Customer Statement Report</a></li>
                    <li><a href="{{ url('/print_page_setting') }}"><i class="fa fa-table"></i>Settings</a></li>

                </ul>
            </li>


            <li class="treeview multi-links active">
                <a href="{{ url('/items') }}">
                    <i class="fa fa-user"></i> <span>Items</span>
                </a>
                <a class="agnle_anchor" href="{{ url('') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu menu-open" style="">
                    <li><a href="{{ url('/items') }}"><i class="fa fa-table"></i>Items Listing</a></li>
                    <li><a href="{{ url('/item-serial-queue') }}"><i class="fa fa-table"></i>Items Serials Queue</a></li>
                    <li><a href="{{ url('/item-serial') }}"><i class="fa fa-table"></i>Items Serials</a></li>
                    <li><a href="{{ url('/categories') }}"><i class="fa fa-table"></i>Item Categories</a></li>
                    <li><a href="{{ url('/item-price-templates') }}"><i class="fa fa-table"></i>Price Templates</a></li>
                    @if(1==1)
                    <li><a href="{{ url('/colors') }}"><i class="fa fa-table"></i>Items Colors</a></li>
                     <li><a href="{{ url('/sizes') }}"><i class="fa fa-table"></i>Items Sizes</a></li>
                      <li><a href="{{ url('/types') }}"><i class="fa fa-table"></i>Items Types</a></li>
                    @endif
                    <li><a href="{{ url('/api-servers') }}"><i class="fa fa-table"></i>API Server</a></li>

                </ul>
            </li>

            <li class="treeview multi-links active">
                <a href="{{ url('bundles') }}">
                    <i class="fa fa-user"></i> <span>Bundles</span>
                </a>
                <a class="agnle_anchor" href="{{ url('/bundles') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu menu-open" style="">
                    <li hidden=""><a href="{{ url('bundles') }}"><i class="fa fa-table"></i>Bundles</a></li>
                    <li><a href="{{ url('bundle/create') }}"><i class="fa fa-table"></i>Create Bundle</a></li>
                    {{-- <li><a href="{{ url('package/create-bulk') }}"><i class="fa fa-table"></i>Create Bulk Packages</a></li>
                    <li><a href="{{ url('package/transfer/all') }}"><i class="fa fa-table"></i>Transfer Packages</a></li> --}}

                </ul>
            </li>
            <li class="treeview multi-links active">
                <a href="{{ url('packages') }}">
                    <i class="fa fa-user"></i> <span>Packages</span>
                </a>
                <a class="agnle_anchor" href="{{ url('/packages') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu menu-open" style="">
                    <li hidden=""><a href="{{ url('packages') }}"><i class="fa fa-table"></i>Packages</a></li>
                    <li><a href="{{ url('package/create') }}"><i class="fa fa-table"></i>Create Packages</a></li>
                    <li><a href="{{ url('package/create-bulk') }}"><i class="fa fa-table"></i>Create Bulk Packages</a></li>
                    <li><a href="{{ url('package/transfer/all') }}"><i class="fa fa-table"></i>Transfer Packages</a></li>

                </ul>
            </li>

            <li class="treeview multi-links active">
                <a href="{{ url('report/customer') }}">
                    <i class="fa fa-user"></i> <span>Reports</span>
                </a>
                <a class="agnle_anchor" href="{{ url('/report/customer') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu menu-open" style="">
                    <li><a href="{{ url('report/customer') }}"><i class="fa fa-table"></i>Customer Transactions</a></li>
                     
                    <li><a href="{{ url('report/customer/sale-profit') }}"><i class="fa fa-table"></i>Customer Sale / Profit</a></li>     
                    <li><a href="{{ url('report/item/sale-profit') }}"><i class="fa fa-table"></i>Items Sale / Profit</a></li>
                    <li><a href="{{ url('report/customer/sale-profit-new') }}"><i class="fa fa-table"></i>Customer Sale / Profit New</a></li>
                    <li><a href="{{ url('report/item/sale-profit-new') }}"><i class="fa fa-table"></i>Items Sale / Profit New</a></li>
                    <li><a href="{{ url('report/item/sale-profit-vendor') }}"><i class="fa fa-table"></i>Vendor Sale/Profit Report New</a></li>
                     <li><a href="{{ url('report/customer/balance') }}"><i class="fa fa-table"></i>Customer Balance Report</a></li>
                      <li><a href="{{ url('report/vendor/balance') }}"><i class="fa fa-table"></i>Vendor Balance Report</a></li>
                      <li><a href="{{ url('report/master') }}"><i class="fa fa-table"></i>Combine Report NEW</a></li>

                </ul>
            </li>

            <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-dashboard"></i> <span>Logout</span></a></li>
        </ul>
        @endif

        @if(Auth::user()->role->role == 'customer_manager')

        <li class="treeview multi-links active">
            <a href="{{ url('/customers') }}">
                <i class="fa fa-user"></i> <span>Customers</span>
            </a>
            <a class="agnle_anchor" href="{{ url('') }}">
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu menu-open" style="">
                <li><a href="{{ url('/customers') }}"><i class="fa fa-table"></i>Customer Listing</a></li>
                <li><a href="{{ url('/invoices') }}"><i class="fa fa-table"></i>Invoices</a></li>
                <li><a href="{{ url('/credit-memo') }}"><i class="fa fa-table"></i>Credit Memo</a></li>
                <li><a href="{{ url('/payments/customer') }}"><i class="fa fa-table"></i>Customer Payments</a></li>

            </ul>
        </li>


        <li class="treeview multi-links active">
            <a href="{{ url('report/customer') }}">
                <i class="fa fa-user"></i> <span>Reports</span>
            </a>
            <a class="agnle_anchor" href="{{ url('/report/customer') }}">
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu menu-open" style="">
                <li><a href="{{ url('report/customer') }}"><i class="fa fa-table"></i>Customer</a></li>
            </ul>
            
           
        </li>

        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-dashboard"></i> <span>Logout</span></a></li>
        </ul>

        @endif
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>