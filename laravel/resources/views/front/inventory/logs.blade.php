@extends('customer_template')

@section('content')
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div id="alert_message"  style="display: none" class="alert alert-success" role="alert">
        </div>
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Inventory Logs</h3>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Item ID</th>
                            <th>Item Name</th>
                            <th>Category Name</th>
                            <th>SKU/Item Code</th>
                            <th>Old Quantity</th>
                            <th>New Quantity</th>
                            <th> Date</th>
                            
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        <?php $i = 1; ?>
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item['item_id']}}</td>
                            <td>{{$item['item_name']}}</td>
                            <td>{{$item['category_name']}}</td>
                            <td>{{$item['item_sku']}}</td>
                             <td>{{$item['old_quantity']}}</td>
                              <td>{{$item['quantity']}}</td>
                            <td><?php echo date("d M Y", strtotime($item['updated_at'])); ?></td>
                            
                        </tr>
                        <?php $i++; ?>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')

<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'Inventory Logs'
            },
            {
            extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'Inventory Logs'
            }
            ]
    });
    });</script>

<script type="text/javascript">

    $(document).ready(function () {
    var warehouse_select1 = document.getElementById('warehouse_id');
    warehouse_select1.addEventListener('change', function () {
    if (warehouse_select1.value != '0') {
    // updateList(warehouse_select1.value);
    }
    }, false);
    });
    function updateList(warehouse_id) {

    var url = "<?php echo url('get/items-by-location'); ?>" + '/' + warehouse_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            alert(response);
            $("select[name='package_id'").html('');
            $("select[name='package_id'").html(response);
            },
            error: function (xhr, status, response) {
            }
    });
    }

    function updateQuantity(item_id, i, p_quantity){

    document.getElementById("alert_message").innerHTML = '';
    document.getElementById("alert_message").style.display = "none";
    var new_amount = document.getElementById("item_new_quantity_" + i).value;
    var final_quantity = parseFloat(new_amount) + parseFloat(p_quantity);
    document.getElementById("item_quantity_" + i).innerHTML = final_quantity;
    
    var url = "<?php echo url('update-inventory-quantity'); ?>" + '/' + item_id + '/' + new_amount;
    alert(url);
    $.ajax({
    url: url,
            type: 'get',
//            dataType: 'html',
            success: function (response) {
                
                console.log(response);

            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }


</script>
@endsection



