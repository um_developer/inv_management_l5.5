@extends('customer_template')

@section('content')
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div id="alert_message"  style="display: none" class="alert alert-success" role="alert">
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filter by Location</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>

                    {!! Form::open(array( 'class' => '','url' => 'get/items-by-location', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">



                            <div class="form-group col-sm-4">

                                {!!  Form::select('warehouse_id', $warehouses, Request::input('warehouse_id'), array('class' => 'form-control','id' => 'warehouse_id' ))  !!}
                            </div>
                            
                             <div class="form-group col-sm-2">
                                {!!   Form::select('type', array('' => 'Show All','negative' => 'Negative','0_quantity' => '0 Quantity'),Request::input('type'), array('class' => 'form-control' ))  !!}
                            </div>
                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <!--                        <div class="form-group col-sm-6">
                                                        <button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-search"></i> Search</button>
                                                    </div>-->
                            <div class=" form-group col-sm-2">
                                <a href="{{ URL::to('inventory') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-2">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>





        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Inventory</h3>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Item ID</th>
                            <th>Item Name</th>
                            <th>Location</th>
                            <th>Category Name</th>
                            <th>SKU/Item Code</th>
                            <th>Quantity</th>
                            <th>Cost</th>
                            <th>Total Cost</th>
                            <th>Updated Date</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        <?php $i = 1; ?>
                        @foreach($model as $item)
                        
                        <?php
                            if (!isset($item['package_quantity'])) {
                                $item['package_quantity'] = 0;
                            }
                            $total_quantity = $item['quantity'] + $item['package_quantity'];
                            ?>
                        @if(Request::input('type') == 'negative')
                        @if($total_quantity < 0)
                        
                        <tr>
                            <td>{{$item['item_id']}}</td>
                            <td>{{$item['item_name']}}</td>
                            @if(isset($item['warehouse_name']))
                            <td>{{$item['warehouse_name']}}</td>
                            @else
                            <td>Admin</td>
                            @endif
                            <td>{{$item['category_name']}}</td>
                            <td>{{$item['item_sku']}}</td>
                            
                            <td id="{{ 'item_quantity_'.$i }}">{{ $total_quantity }}</td>
                             <td>{{ $item['ro_item_cost'] }}</td>
                             <td>{{ $item['ro_item_cost'] * $total_quantity }}</td>
                            <td><?php echo date("d M Y", strtotime($item['updated_at'])); ?></td>
                            @if(!isset($item['warehouse_name']))

                            <td>
                                <div class="input-group input-group-sm">
                                    <input id="{{ 'item_new_quantity_'.$i }}" type="text" style="width: 54px" class="form-control" placeholder="00">
                                    <span class="input-group-btn">
                                        <button onclick="updateQuantity('{{ $item['item_id'] }}','{{ $i }}','{{ $item['package_quantity'] }}')" type="button" class="btn btn-info btn-flat">Update Quantity</button>
                                    </span>
                                </div>
                                @else
                            <td></td>
                            @endif

                        </tr>
                        
                        @endif
                        @else
                        <tr>
                            <td>{{$item['item_id']}}</td>
                            <td>{{$item['item_name']}}</td>
                            @if(isset($item['warehouse_name']))
                            <td>{{$item['warehouse_name']}}</td>
                            @else
                            <td>Admin</td>
                            @endif
                            <td>{{$item['category_name']}}</td>
                            <td>{{$item['item_sku']}}</td>
                            
                            <td id="{{ 'item_quantity_'.$i }}">{{ $total_quantity }}</td>
                            <td>{{ $item['ro_item_cost'] }}</td>
                             <td>{{ $item['ro_item_cost'] * $total_quantity }}</td>
                            <td><?php echo date("d M Y", strtotime($item['updated_at'])); ?></td>
                            @if(!isset($item['warehouse_name']))

                            <td>
                                <div class="input-group input-group-sm">
                                    <input id="{{ 'item_new_quantity_'.$i }}" type="text" style="width: 54px" class="form-control" placeholder="00">
                                    <span class="input-group-btn">
                                        <button onclick="updateQuantity('{{ $item['item_id'] }}','{{ $i }}','{{ $item['package_quantity'] }}')" type="button" class="btn btn-info btn-flat">Update Quantity</button>
                                    </span>
                                </div>
                                @else
                            <td></td>
                            @endif

                        </tr>
                        <?php $i++; ?>
                        @endif
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')

<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'Inventory Report',
                    exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8]
                }
            },
            {
            extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'Inventory Report',
                     exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8]
                }
            }
            ]
    });
    });</script>

<script type="text/javascript">

    $(document).ready(function () {
    var warehouse_select1 = document.getElementById('warehouse_id');
    warehouse_select1.addEventListener('change', function () {
    if (warehouse_select1.value != '0') {
    // updateList(warehouse_select1.value);
    }
    }, false);
    });
    function updateList(warehouse_id) {

    var url = "<?php echo url('get/items-by-location'); ?>" + '/' + warehouse_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            alert(response);
            $("select[name='package_id'").html('');
            $("select[name='package_id'").html(response);
            },
            error: function (xhr, status, response) {
            }
    });
    }

    function updateQuantity(item_id, i, p_quantity){

    document.getElementById("alert_message").innerHTML = '';
    document.getElementById("alert_message").style.display = "none";
    var new_amount = document.getElementById("item_new_quantity_" + i).value;
    var final_quantity = parseFloat(new_amount) + parseFloat(p_quantity);
    document.getElementById("item_quantity_" + i).innerHTML = final_quantity;
    
    var url = "<?php echo url('update-inventory-quantity'); ?>" + '/' + item_id + '/' + new_amount;
    $.ajax({
    url: url,
            type: 'get',
//            dataType: 'html',
            success: function (response) {
                
                console.log(response);

            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
//            document.getElementById("alert_message").style.display = "none";
//            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }


</script>
@endsection



