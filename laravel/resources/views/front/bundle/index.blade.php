@extends('customer_template')
<?php use App\Functions\Functions;?>
@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filter by Item</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>

                    {!! Form::open(array( 'class' => '','url' => 'bundle/search', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-sm-4">

                                {!!  Form::select('item_id', $items, Request::input('item_id'), array('class' => 'form-control','id' => 'item_id' ))  !!}
                            </div>
                            @if(1==2)
                            <div class="form-group col-sm-4">
                                {!!  Form::select('warehouse_id', $warehouses, Request::input('warehouse_id'), array('class' => 'form-control','id' => 'warehouse_id' ))  !!}
                            </div>
                            @endif
                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <!--                        <div class="form-group col-sm-6">
                                                        <button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-search"></i> Search</button>
                                                    </div>-->
                            <div class=" form-group col-sm-2">
                                <a href="{{ URL::to('bundles') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-2">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Bundles</h3>
                <a href="{{ url('bundle/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Bundle</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bundle Code</th>
                            <th>Bundle Name</th>
                            <th>Item Quantity</th>
                            <th>Created Date</th>
                            <th>State</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->name}}</td>
                            <td>
                               
                                {{ $item->quantity }}
                            </td>
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            <td>
                                @if($item->is_active == 1)
                                  <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}','0')" checked data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                  @else
                                 <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}','1')" data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                  @endif
                            </td>
                            <td>
                                <a href="{{ url('bundle/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                 <a href="{{ url('bundle/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a> 
                                 <a href="{{ url('bundle/copy/'.$item->id) }}" onclick="return confirm('Are you sure you want to copy item # {{ $item->id }}.')" class="btn btn-primary"><i class="fa fa-copy"></i></a>
                                @if($item->status == 'unassigned')
                                <a href="{{ url('bundle/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                                @endif


                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
     function changeItemStatus(item_id,status) {

$.ajax({
    url: "<?php echo url('bundle/status/'); ?>" + '/'+status+'/' + item_id,
    type: 'get',
    dataType: 'html',
    success: function (response) {
        console.log(response);
        if (response == 1) {
//                    var modal = document.getElementById('myModal');
//                    modal.style.display = "block";
//                    alert(response);

        } else if (response == 0) {

        }
    },
    error: function (xhr, status, response) {
        alert(response);
    }
});



}
      $(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'Active',
      off: 'Disabled'
    });
  })
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
        });
    });
</script>
@endsection



