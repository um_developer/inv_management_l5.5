@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Edit Bundle</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'bundle/update', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <input type="hidden"  name="id" value="{{ $model->id }}">
        <div class="box-body">

            <div class="form-group">
            
            <div class="col-sm-4">
                <label for="exampleInputEmail1">Bundle Name</label>
             
            <input type="text"  name="name"  class = 'form-control' value="{{ $model->name }}">
            </div>

            </div>
            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>

            @include('front/common/suggestion_box')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        <tr>

                            <th>Item Name</th>
                            <th>SKU/Item Code</th>
                            <th>UPC/Barcode</th>
                            {{-- <th>Remaining Qty</th> --}}
                            <th>Total Qty</th>
                           <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @if(count($po_items)>0)
                        @foreach($po_items as $items_1)
                        <?php
                       // dump($items_1);
                        $delivered_items = $items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
                        ?>
                        @if(1==1)
                        <tr id="item_{{ $i }}">
                            <td><input class="form-control" type="text" name="item_name_{{ $i }}" title="{{$items_1->item_name}}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->id}}">
                                <input type="hidden" readonly="readonly" name="bu_item_id_{{ $i }}" id="bu_item_id_{{ $i }}"  value="{{$items_1->bundle_id}}">
                                <input type="hidden" name="num[]" id="num[]"></td>
                           
                            <td><input class="form-control"readonly="readonly" name="sku_{{$i}}"  value="{{$items_1->item_sku}}"></td>
                            <td><input class="form-control"  name="upc_barcode_{{$i}}"  readonly="readonly" value="{{$items_1->item_upc_barcode}}"></td>
                            <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}" step="0.01"></td>
                            <td> <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Item" onclick="deleteRow(this,{{ $items_1->id }})"></td></td>
                        </tr>


                        <?php $i++; ?>
                        @endif
                        @endforeach

                        @endif

                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div>
            <div class="row" hidden="true">

                <div class="col-xs-8"></div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Total Amount for Purchase Order</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Total:</th>
                                <td id="po_total">$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

            <script type="text/javascript">

                                var user_type = "{{ Auth::user()->role->role }}";
                                var is_order = false;
                                var j = {{ count($po_items) + 1 }}
                                $(document).ready(function(){
                                InsertNewRow();
                                });
                                function calculatePrice(i){

                                var price = ($("#price_" + i).val());
                                var quantity = parseFloat($("#quantity_" + i).val());
                                // alert(price);
                                var total = price * quantity;
                                total = parseFloat(total).toFixed(2);
                                $("#total_" + i).val(total);
                                }

                                function getDatableData(){
                                var aTable = $('#item_body').DataTable();
                                var w = aTable.cell(0, 2).nodes().to$().find('input').val()
                                        alert(w);
                                var x = document.getElementById("item_body").rows[1].cells[4];
                                console.log(x);
//                                alert(x);
                                alert(document.getElementById("item_body").rows[0].cells.innerHTML);
                                }

                             function InsertNewRow() {
    var i = window.j;
    var scntDiv = '<tr id="item_'+i+'"><td><input type="text" name="item_name_' + i + '" class="form-control" id="item_name_' + i + '"  autocomplete="on" placeholder="Enter item name"><input type="hidden" readonly="readonly" name="item_id_' + i + '" id="item_id_' + i + '"><input type="hidden" name="num[]" id="num[]"></td><td><input type="text" readonly="readonly" name="sku_' + i + '" class="form-control" id="sku_' + i + '"></td><td><input type="text" name="upc_barcode_' + i + '" class="form-control" readonly="readonly" id="upc_barcode_' + i + '"></td><td><input type="hidden" readonly="readonly"  name="r_quantity_' + i + '" class="form-control" id="r_quantity_' + i + '"><input type="number" name="quantity_' + i + '" class="form-control"   min ="1" id="quantity_' + i + '"></td><td><input type="button" class="btn btn-danger btn-sm" id="delete_btn_' + i + '" name="delete_btn_' + i + '" value="Delete item" onclick="deleteRow(this,'+i+')"></td></tr>';
    var item_array = <?php echo json_encode($items, JSON_PRETTY_PRINT) ?>;

    $("#item_body").append(scntDiv);
    $(function ($) {
        $('#item_name_' + i).autocomplete({
            source: item_array,
            select: function (event, ui) {

                $("#item_id_" + i).val(ui.item.id);
                $("#sku_" + i).val(ui.item.sku);
                $("#upc_barcode_" + i).val(ui.item.upc_barcode);
                $("#r_quantity_" + i).val(ui.item.inventory_quantity);
                $("#num[0]").val(ui.item.id);
                $("#quantity_" + i).val('1');
               // $("#quantity_" + i).attr('max', ui.item.inventory_quantity);


            }
        });


    });
    window.j++;
    return false;
}

                                $('#add_more').on('click', function () {
                                InsertNewRow();
                                // getDatableData();
                                });
                                $(function () {
                                $('#item_body').DataTable({
                                "paging": false,
                                        "lengthChange": false,
                                        "searching": false,
                                        "ordering": false,
                                        "info": false,
                                        "autoWidth": false
                                });
                                });
                                function deleteRow(r, a = 0) {

                                    var i = r.parentNode.parentNode.rowIndex;
                                    if (a != '0'){

                                    $("#quantity_" + i).val(0);
                                    $("#total_" + i).val(0);
                                    $("#quantity_" + i).prop("readonly", true);
                                    var item_row = "item_" + i;
                                    document.getElementById(item_row).style.display = 'none';
                                    }
                                    else{
                                    $("#quantity_" + i).val(0);
                                    $("#total_" + i).val(0);
                                    $("#quantity_" + i).prop("readonly", true);
                                    var item_row = "item_" + i;
                                    document.getElementById(item_row).style.display = 'none';
                                    //document.getElementById("item_body").deleteRow(i);
                                    }
                                    }

                                function getItemPrice(i, price) {

                                var item_id = $("#item_id_" + i).val();
                                var customer_id = $("#vendor_id").val();
                                var type = 'purchase_order';
                                my_url = "<?php echo url('get-item-price/'); ?>" + '/' + item_id + '/' + customer_id + '/' + type;
                                $.ajax({
                                url: my_url,
                                        type: 'get',
//            dataType: 'html',
                                        success: function (response) {

                                        if (response != '0') {
                                        $("#price_" + i).val(response['sale_price'][0]['item_unit_price']);
                                        } else {
                                        $("#price_" + i).val(price);
                                        }
                                        calculatePrice(i);
                                        },
                                        error: function (xhr, status, response) {
                                        alert(response);
                                        }
                                });
                                }

                                function getItemPriceHTML(i, price) {

                                var item_id = $("#item_id_" + i).val();
                                var customer_id = $("#vendor_id").val();
                                var type = 'purchase_order';
                                my_url = "<?php echo url('get-item-price-html/'); ?>" + '/' + item_id + '/' + customer_id + '/' + type;
                                $.ajax({
                                url: my_url,
                                        type: 'get',
                                        dataType: 'html',
                                        success: function (response) {

                                        if (response != '0') {
                                        var suggestion_box = document.getElementById("suggestion_box");
                                        suggestion_box.style.display = "block";
                                        var suggestion_box_text = document.getElementById("suggestion_box_text");
                                        suggestion_box_text.innerHTML = response;
//                alert(response);
                                        }
                                        },
                                        error: function (xhr, status, response) {
                                        alert(response);
                                        }
                                });
                                }

                                function showSuggestionPopup(i){
                                calculatePrice(i);
                                var keycode = (window.event) ? event.keyCode : e.keyCode;
                                if (keycode == '9') {
                                getItemPriceHTML(i);
                                }

                                }

                                function hideSuggestionPopup(){

                                var suggestion_box = document.getElementById("suggestion_box");
                                suggestion_box.style.display = "none";
                                }

            </script>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update Bundle">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

</section>

@endsection