@extends('customer_template')

@section('content')
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-hdd-o"></i>Return vendor Order # {{ $model->id }}
            <small class="pull-right">Created Date: <?php echo date("d M Y", strtotime($model->created_at)); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <b>Order # {{ $model->id }}</b><br>
          <br>
          <b>Vendor Name:</b> {{ $model->vendor_name }}<br>
          <b>Modified Date:</b> <?php echo date("d M Y", strtotime($model->updated_at)); ?><br>
<!--          <b>Account:</b> 968-34567-->
 <br>
  <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          
          <br>
         @if($model->status == 'closed')
          <b class = "btn btn-danger btn-xs">Order is {{ $model->status }}</b><br>
          @elseif($model->status == 'open')
          <b class = "btn btn-primary btn-xs">Order is {{ $model->status }}</b><br>
        @else
        <b class = "btn btn-success btn-xs">Order is in {{ $model->status }} mode</b><br>
         @endif
          <b>Delivered Quantity:</b> {{ $model->delivered_quantity }}<br>
          
          <!--<b>Order Modified Date:</b> <?php echo date("d M Y", strtotime($model->updated_at)); ?><br>-->
<!--          <b>Account:</b> 968-34567-->
 <br>
  <br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
        <tr>
              <th>Name</th>
<!--              <th>Start Serial #</th>
               <th>End Serial #</th>-->
              <th>QTY</th>
              <th>Delivered QTY</th>
              <th>Unit Price</th>
               <th>Total</th>
            </tr>
            </thead>
            <tbody>
                 @if(count($rv_items)>0)
                @foreach($rv_items as $items_1)
                
				 <?php 
         if($items_1->bundle_name != ''){
              $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
             $items_1->item_name = $items_1->item_name . $bundle_detail;
         }
         ?>
            <tr>
              <td>{{ $items_1->item_name }}</td>
<!--              <td>{{ $items_1->start_serial_number }}</td>
              <td>{{ $items_1->end_serial_number }}</td>-->
              <td>{{ $items_1->quantity }}</td>
              <td>{{ $items_1->delivered_quantity }}</td>
              <td>{{ $items_1->item_unit_price }}</td>
              <td>{{ $items_1->total_price }}</td>
            </tr>
             
                @endforeach
                @endif
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
<!--          <p class="lead">Payment Methods:</p>
        

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>-->
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!--<p class="lead">Amount Due 2/22/2014</p>-->

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Total Quantity:</th>
                <td>{{ $model->total_quantity }}</td>
              </tr>
              <tr>
                <th>Total Cost</th>
                 <td>{{ $model->total_cost }}</td>
              </tr>
             
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
            
        </div>
      </div>
    </section>

@endsection