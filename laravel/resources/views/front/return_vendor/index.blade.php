@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
     <div class="row">
         
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Return vendors</h3>
              <a href="{{ url('return-vendor/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Return vendor</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vendors" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Vendor Return ID</th>
                  <th>Vendor Name</th>
                  <th>Tracking Info</th>
                  <th>Order Status</th>
                   <th>Created Date</th>
                   <th>Total PO Cost</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
               
                    @if(count($model) >0)
                    @foreach($model as $item)
                     <tr>
                <td><a href="{{ url('return-vendor/'.$item->id) }}">{{$item->id}}</a></td>
                   <td><a href="{{ url('vendor/edit/'.$item->vendor_u_id) }}">{{$item->vendor_name}}</a></td>
                  <td>{{$item->tracking_info}}</td>
                  @if($item->status == 'open')
                  <td><a class="btn btn-primary btn-xs"> Open</a></td>
                  @elseif($item->status == 'draft')
                   <td><a class="btn btn-success btn-xs"> Draft</a></td>
                    @else
                   <td><a class="btn btn-warning btn-xs"> Closed</a></td>
                   @endif
                  <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                     <td>{{ $item->total_cost }}</td>
                  <td>
                   <a href="{{ url('return-vendor/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                    @if($item->status == 'draft')
                  <a href="{{ url('return-vendor/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                   <a href="{{ url('return-vendor/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                  @endif
                 
                 
                  </td>
                </tr>
                    @endforeach
                  
                    @endif
               
                
                </tbody>
                <tfoot>
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>

<script>
  $(function () {
    $('#vendors').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "pageLength": {{Config::get('params.default_list_length')}},
      "autoWidth": false
    });
  });
</script>
@endsection



