@extends('home_master')

@section('content')
<div class="home-product-cata-nav">
        
                                <ul class="nav nav-tabs" role="tablist">
                                <?php if(count($tags) > 0){ ?>
                                    <?php foreach($tags as $row){ ?>
        
                                        <?php if (count($row->child) == 0) {?>
                                           <li> <a href="{{ url('home-search-new?tag='.$row->id) }}" class="active"><?php echo $row->title; ?></a> </li>
                                        <?php }else{ ?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $row->title; ?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                            @foreach($row->child as $chil)
                                                <li><a href="{{ url('home-search-new?subtag='.$chil->id) }}"><?php echo $chil->title; ?></a></li>
                                            @endforeach
                                            </ul>
        
                                        </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php } ?>
                                    
                                </ul>                            
                          </div>
@if(1==2)
<div class="row">

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Item Search Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'home-search', 'method' => 'get')) !!}
            <div class="box-body">
                <div class="row">

                    @if(1==2)
                    <div class="form-group col-sm-4">
                        {!!   Form::select('selling_price_template', $templates, $template_id, array('class' => 'form-control select2', 'id' => 'template_id' ))  !!}
                    </div>
                    @endif

                    <div class="form-group col-sm-4">
                        {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control select2','id' => 'category_id' ))  !!}
                    </div>

                    <div class="form-group col-sm-4">
                        {!!   Form::select('item_id', $items_list, $item_id, array('class' => 'form-control select2','id' => 'item_id' ))  !!}
                    </div>

                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('item-gallery') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
   
</div>
@endif

<!--<h2>Trending <b>Products</b></h2>-->
<section class="main-slider-area" id="swiperSlider"> 

    <div id="alert_message"  style="display: none" class="alert alert-success" role="alert">
    </div>
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <div class="box col-md-12">
        <div class="box-body col-sm-12" id="gird" data-view="girdLg">
            <div class="row">
                    <div class="search-area col-md-12">
                        
                        <div class="row">
<!--                          <div class="col-md-12">
                              <h3>Products</h3>
                          </div>
                            -->
                                  {!! Form::open(array( 'class' => '','url' => 'home-search-new', 'method' => 'get')) !!}
                        <div class="search-form  col-md-10">
                            <div class="form-group col-md-12 row">
                                <div class="input-group">
                                   
                                    {!! Form::text('item_name', Request::input('item_name') , array('placeholder'=>"Item Name ",'maxlength' => 200,'class' => 'form-control onLoadFocus', 'id' => 'item_name', 'autofocus') ) !!}
                                    <span class="input-group-addon">
                                        <button class="btn btn-primary" type="submit" class="btn btn-primary" name="submit" id="submit"> 
                                           <i class="fa fa-search"></i>
                                            <span>Search</span>
                                        </button>
                                        <!--<a href="{{ URL::to('/') }}" class="btn btn-danger">Clear</a>-->
                                    </span>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}

                                  <div class="box__action col-sm-2 text-right">
                                      <button id="girdLg" class="active">
                                          <i class="fa fa-th-large"></i>
                                      </button>
                                      <button id="girdMd">
                                          <i class="fa fa-delicious"></i>
                                      </button>
                                      <button id="girdSm">
                                          <i class="fa fa-th"></i>
                                      </button>
                                      <button id="girdList">
                                          <i class="fa fa-list-ul"></i>
                                      </button>
                                  </div>
                            </div>
                        

                    </div>

                   


            </div>


            <div class="sort-area ">
                <div class="row">
                   
                    @if(1==2)
                    <div class="sort col-sm-5 text-right ">
                        <div class="row">
                            <div class="col-md-5 ">
                                <div class="input-group">
                                    <div class="input-group-addon bdr0">
                                        Show:
                                    </div>
                                    <select class="form-control">
                                        <option value="">5</option>
                                        <option value="">10</option>

                                    </select>   
                                </div>
                            </div>
                            <div class="col-md-7 ">
                                <div class="input-group">
                                    <div class="input-group-addon bdr0">
                                        Sort by:
                                    </div>
                                    <select class="form-control">
                                        <option value="">Date</option>
                                        <option value="">Name</option>

                                    </select>   
                                </div>
                            </div>
                        </div>

                    </div>
                    @endif
                </div>
            </div>
            
            <div class="row">
                <div class="product-group">                           
                    @if(count($items) >0)
                    @foreach($items as $item)
                    <div class="item-gallery col-sm-12">
                        <div class="item-gallery__inr">
                            <div class="tag">
                                {{ $item->code }}
                            </div>
                            <div class="item-gallery__img col-sm-4">
                                <a href="#"   onclick="showItemGallery('{{$item->image}}','{{$item->id}}')" class="view">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if($item->image == '')
                                <a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a>
                                @else
                                <td><a href='#' onclick="showItemGallery('{{$item->image}}','{{$item->id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>
                                <!-- <a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a> -->
                                @endif
                            </div>


                            <div class="item-gallery__cont  col-sm-8">
                                <span class="name">{{$item->name}}</span>
                                @if(1==2)
                                <span class="cata">
                                    {{ $item->category_name }}
                                </span>
                                @endif
                                <span class="price">
                                    <i class="fa fa-money"></i>    
                                    {{$item->sale_price}}
                                </span>

                                <div class="actions">

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="float: right !important"> 
                <?php echo $items->appends(Input::query())->render(); ?>
            </div>
        </div>
</section>

<script>
    var autoSearch = false;
    window.onload = function() {
        $('.onloadFocus').trigger("focus");
    };
    var typingTimer;
    var doneTypingInterval = 1000;
    var input = $('#item_name');
    input.on('keyup', function () {
    clearTimeout(typingTimer);
        autoSearch = true;
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    input.on('keydown', function () {
        autoSearch = true;
        clearTimeout(typingTimer);
    });
    function doneTyping () {
        var len = $('#item_name').val().length;
        if(len > 2 && autoSearch == true){
            document.getElementById("submit").click();
        }
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".onLoadFocus").trigger("focus");
        
    document.getElementById("alert_message").innerHTML = '';
    document.getElementById("alert_message").style.display = "none";
    var category_select = document.getElementById('category_id');
    var template_id = '0';
    category_select.addEventListener('change', function () {

    }, false);
    $('#template_id').on('select2:select', function (e) {
    var data = e.params.data;
    template_id = data.id;
    updateFilters(data.id);
    });
    $('#category_id').on('select2:select', function (e) {
    var data = e.params.data;
    var template_id = $('#template_id').val();
    updateItemFilters(template_id, data.id);
    });
    });
    function updateItemList(vendor_id) {

    var url = "<?php echo url('get/open-po-by-vendor'); ?>" + '/' + vendor_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            $("select[name='po_id'").html('');
            $("select[name='po_id'").html(response);
            var $table = $('#item_body');
            var $table = $('#item_body');
            $table.find('tbody').empty();
            },
            error: function (xhr, status, response) {
            }
    });
    }



    function addToOrder(item_id, quantity, price) {

    var url = "<?php echo url('customer/add-to-order/'); ?>" + '/' + item_id + '/' + quantity + '/' + price;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }


    function addToInvoice(item_id, quantity, price) {

    var url = "<?php echo url('customer/add-to-invoice/'); ?>" + '/' + item_id + '/' + quantity + '/' + price;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }




    $("#girdLg").on("click", function(){
    $("#gird").attr("data-view", "girdLg");
    $(".box__action button").removeClass("active");
    $(this).addClass("active");
    });
    $("#girdMd").on("click", function(){
    $("#gird").attr("data-view", "girdMd");
    $(".box__action button").removeClass("active");
    $(this).addClass("active");
    });
    $("#girdSm").on("click", function(){
    $("#gird").attr("data-view", "girdSm");
    $(".box__action button").removeClass("active");
    $(this).addClass("active");
    });
    $("#girdList").on("click", function(){
    $("#gird").attr("data-view", "List");
    $(".box__action button").removeClass("active");
    $(this).addClass("active");
    });
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    });
    function updateFilters(template_id) {
    template_id = 1;
    var url = "<?php echo url('get/templates-categories'); ?>" + '/' + template_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {

            $("#category_id").select2("val", "0");
            $("select[name='category_id'").html('');
            $("select[name='category_id'").html(response);
            },
            error: function (xhr, status, response) {
            }
    });
    updateItemFilters(template_id, '0');
    }

    function updateItemFilters(template_id, category_id) {
    template_id = 1;
    var url = "<?php echo url('get/templates-items'); ?>" + '/' + template_id + '/' + category_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {

            $("#item_id").select2("val", "0");
            $("select[name='item_id'").html('');
            $("select[name='item_id'").html(response);
            },
            error: function (xhr, status, response) {
            }
    });
    }

    function showGalleryItemDetailModal($item_id){

    $('#galleryModal').modal('toggle');
        subItemsModal($item_id)
    }




</script>
@include('front/common/image_modal')
@include('front/common/gallery_purchase_detail')

@endsection



