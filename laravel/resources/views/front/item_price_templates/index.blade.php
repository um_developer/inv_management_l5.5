@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <div class="box">
            <div class="box-header">

                <h3 class="box-title">Item Price Templates</h3>
                <a href="{{ url('item-price-template/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add new Price Template</a>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">

                    <thead>

                        <tr>
                            <th>Template ID</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            <td>
                                <a href="{{ url('item-price-template/'.$item->id) }}" class="btn btn-success">Detail</i></a>
                                <a href="{{ url('item-price-template/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('item-price-template/copy/'.$item->id) }}" onclick="return confirm('Are you sure you want to copy this template.')" class="btn btn-primary"><i class="fa fa-copy"></i></a>
                                @if($item->is_default == '0')
                                <a href="{{ url('item-price-template/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
//       "dom": '<"toolbar">frtip'
        });
        $("div.toolbar")
                .html('<div class = "ml-5"><button type="button" class = "btn btn-primary pull-right important" id="any_button">Click Me!</button> </div>');
    });
</script>
@endsection



