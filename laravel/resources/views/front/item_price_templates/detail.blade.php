@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

@section('content')
<?php
$required = 'required';
?>
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Select Filter</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>

                    {!! Form::open(array( 'class' => '','url' => 'item-price-template/search', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-sm-3">
                                {!! Form::text('item_name', $item_name , array('placeholder'=>"Item Name",'maxlength' => 100,'class' => 'form-control') ) !!}

                            </div>

                            <div class="form-group col-sm-3">
                                {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control ','id' => 'category_id' ))  !!}
                                {!! Form::hidden('id', $template_id ) !!}
                            </div>

                            <div class="form-group col-sm-3">
                                {!!   Form::select('display_type', array('0' => 'Select Product Display','1' => 'No','2' => 'Yes'), $display_type, array('class' => 'form-control' ))  !!}
                            </div>

                            <div class="form-group col-sm-3">
                                {!!   Form::select('profit_type', array('0' => 'Select Profit Type','1' => 'Negative'), $profit_type, array('class' => 'form-control' ))  !!}
                            </div>

                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('item-price-template/'.$template_id) }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Template Name: {{ $template_name }}</h3>
                <button class="btn btn-primary pull-right" onclick="showBulkUpdate()">Bulk Update</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll">Check All</th>
                                    <th>IMG</th>
                                    <th>Id</th>
                                    <th>Item Name</th>
                                    <th>Category</th>
                                    <th>My Cost Price</th>
                                    <th>My selling Price</th>
                                    <th>Profit</th>
                                    <th>Display Product</th>
                                    <th style="display: none">Display Product</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($model) >0)
                                @foreach($model as $item)

                                <tr>
                                    
                                     <?php
                                    $selling_price = '';
                                    if (isset($item['selling_price']))
                                        $selling_price = $item['selling_price'];

                                    $item_template_price_id = '';
                                    if (isset($item['item_template_price_id']))
                                        $item_template_price_id = $item['item_template_price_id'];

                                    $profit = '00';
                                    if (isset($item['profit']))
                                        $profit = $item['profit'];

                                    $display = '0';
                                    if (isset($item['display']))
                                        $display = $item['display'];
                                    ?>
                                    
                                    <td><input class="chk" type="checkbox" id="{{'check_box_'.$item['id']}}" value="{{ $item['id'].'_'.$item['cost'].'_'.$selling_price}}" name="{{'check_box_'.$item['id']}}"></td>
                                    @if($item['image'] == '')
                                    <td><a href='#' onclick="showItemModal('{{$item['image']}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                                    @else
                                    <td><a href='#' onclick="showItemGallery('{{$item['image']}}','{{$item['id']}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item['image']}}" height="42" width="42"></a></td>
                                    <!-- <td><a href='#' onclick="showItemModal('{{$item['image']}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item['image']}}" height="42" width="42"></a></td> -->
                                    @endif
                                   
                                    <td>{{$item['id']}}</td>
                                    <td>{{ $item['name']}}</td>
                                    <td>{{$item['category_name']}}</td>
                                    <td><span id="text_c_{{ $item['id'] }}">{{$item['cost']}}</span></td>
                                    <!--{!! Form::open(array( 'class' => 'form-horizontal','url' => 'item-price-template/price-update','files' => true, 'method' => 'post')) !!}-->
                                    {!! Form::hidden('item_id', $item['id']) !!}
                                    {!! Form::hidden('cost', $item['cost'], array('id' => 'cost_'.$item['id'] )) !!}
                                    {!! Form::hidden('item_template_price_id', $item_template_price_id , array('id' => 'item_template_price_id_'.$item['id'] )) !!}
                                    {!! Form::hidden('price_template_id', $template_id , array('id' => 'price_template_id_'.$item['id'] )) !!}
                                    <td>
                                        <span id="{{ 'dis_sp_'.$item['id'] }}" style="display: none">{{ $selling_price }}</span>
                                        <input data-id="{{ $item['id'] }}" class="form-control selling-price-input" type="text" id="{{ 'selling_price_'.$item['id'] }}" placeholder="00" maxlength="10" value="{{ $selling_price }}" name="selling_price" required="required" >
                                    </td>
                                    @if(strpos($profit, '-') !== false)
                                    <td><span style="color: red "id="text_p_{{ $item['id'] }}">{{ $profit }}</span></td>
                                    @else
                                    <td><span id="text_p_{{ $item['id'] }}">{{ $profit }}</span></td>
                                    @endif
                            <span id="{{ 'dis_sp_'.$item['id'] }}" style="display: none">{{ $display }}</span>
                            <td>{!!   Form::select('display', array('0' => 'No','1' => 'Yes'), $display, array('class' => 'form-control item-display','data-id' => $item['id'],'id' => 'display_'.$item['id'] ))  !!}</td>
                            @if( $display == '0')
                            <td style="display: none"><span id="{{ 'dis_'.$item['id'] }}" >No</span></td>  
                            @else
                            <td style="display: none"><span id="{{ 'dis_'.$item['id'] }}" >Yes</span></td>  
                            @endif
                            </tr>
                            @endforeach

                            @endif


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>

                    </div></div>

            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

@include('front/common/dataTable_js')
<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "pageLength": 25,
            "autoWidth": false,
            "columnDefs": [
            { "targets": [1, 2, 4, 5, 6], "searchable": false }
            ],
            dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: '{{ $template_name }}',
                    exportOptions: {
                    columns: [2, 3, 4, 5, 6, 7, 9]
                    }
            },
            {
            extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: '{{ $template_name }}',
                    exportOptions: {
                    columns: [2, 3, 4, 5, 6, 7, 9]
                    }
            }
            ]
    });
    });
    $("#checkAll").click(function () {
         $('input:checkbox').not(this).prop('checked', this.checked);
     });
    function updateProfit(id){
        var profit = document.getElementById('selling_price_' + id).value - document.getElementById('text_c_' + id).innerHTML;
    profit = Math.round(profit * 100) / 100;
        document.getElementById('text_p_' + id).innerHTML = profit;
    var profit_span = document.getElementById('text_p_' + id);
    if (profit < 0){
    profit_span.style.color = 'red';
    } else{
    profit_span.style.color = 'black';
    }

        }

    function getValueUsingClass(){
    var chkArray = [];
    $(".chk:checked").each(function() {
    chkArray.push($(this).val());
    });
    var selected;
    selected = chkArray.join(',');
    if (selected.length > 0){
    $('#bulk_item_id').val(selected);
    } else{
//		alert("Please at least check one of the checkbox");	
    }
    }
</script>

<script>   

    $(".item-display").change(function() {
      var id = $(this).attr("data-id");
    var selling_price = document.getElementById('selling_price_' + id).value;
    var cost = document.getElementById('cost_' + id).value;
    var template_id = document.getElementById('price_template_id_' + id).value;
    var item_template_price_id = document.getElementById('item_template_price_id_' + id).value;
    var selling_price = document.getElementById('selling_price_' + id).value;
    var display = $('#display_' + id).val(); ;
    saveSellingPrice(id, selling_price, cost, display, item_template_price_id, template_id);
    if (display == '0'){
    document.getElementById('dis_' + id).textContent = 'No';
    } else{
    document.getElementById('dis_' + id).textContent = 'Yes';
    }

    });
        $(".selling-price-input").blur(function (event) {
            var id = $(this).attr("data-id");
    var selling_price = document.getElementById('selling_price_' + id).value;
    var cost = document.getElementById('cost_' + id).value;
    var template_id = document.getElementById('price_template_id_' + id).value;
    var item_template_price_id = document.getElementById('item_template_price_id_' + id).value;
    var selling_price = document.getElementById('selling_price_' + id).value;
    var display = $('#display_' + id).val(); ;
    saveSellingPrice(id, selling_price, cost, display, item_template_price_id, template_id);
        
        });
    $(".selling-price-input").change(function (event) {

    var id = $(this).attr("data-id");
    var selling_price = document.getElementById('selling_price_' + id).value;
    document.getElementById('dis_sp_' + id).textContent = selling_price;
    });
    function saveSellingPrice(item_id, selling_price, cost, display, item_template_price_id, template_id){

    updateProfit(item_id);
        $.ajax({
        url: "<?php echo url("/"); ?>/item-price-template/price-update",
                        type: 'post',
                        //dataType: 'json',
                        cache: false,
                        async: true,
                        data: {
                        ajax: 1,
                           item_id: item_id,
                    cost: cost,
                    selling_price: selling_price,
                    price_template_id: template_id,
                    item_template_price_id: item_template_price_id,
                    display: display,
                     _token: '<?php echo csrf_token() ?>',
                        },
                        success: function (response) {
//       alert(response);
            console.log(response);
//                
            },
            error: function (xhr, status, response) {
            alert(response);
            }
        });
        }
</script>

@include('front/common/image_modal')
@include('front/common/item_selling_price_bulk_update')
@endsection



