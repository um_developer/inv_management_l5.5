@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Tags.. </h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'sample/create', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Title</label>
                    {!! Form::text('title', Request::input('title') , array('placeholder'=>"Title *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                </div>
            </div>



            <div class="tags-area">
                <div class="row">

                <div class="col-md-12" id="wrapper">
                    
                    <div  class=" col-md-5" style="margin-left: 4%">
                        
                        <div class="col-md-7 row">
                            <a class="btn btn-primary " id="selectAllOrigin">Select All</a>
                        </div>
                        
                            <select class="btn  btn-default col-md-offset-1 col-md-4">
                                <option value="1">Category 1</option>
                                <option value="2">Category 2</option>
                                <option value="3">Category 3</option>
                                <option value="4">Category 4</option>
                            </select>
                        
                        <!--button class="btn btn-primary" id="moveAll">Move All ></button-->

                        
                        
                        <div id="origin" class="tagbox col-md-12">
                            <div class="form-group col-md-12">
                                <input class="form-control" type="search" id="searchTag1"  placeholder="Search..." />
                            </div>
                        
                            <select id="select1" name="select1[]" multiple>
                                <option value="v1">Item 1</option>
                                <option value="v2">Item 2</option>
                                <option value="v3">Item 3</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                                <option value="v4">Item 4</option>
                            </select>
                        </div>
                    </div>

                    <div class="tags__center col-md-1">
                        
                        <a class="btn btn-primary" id="add">&#8702;</a>
                        <br><br>
                        <a  class="btn btn-primary" id="remove">&#8701;</a>
        
                    </div>

                    <div class="col-md-5"> 

                        <div class="col-md-7 row">
                            <a class="btn btn-primary " id="selectAllDrop">Select All</a>
                        
                            <!-- <button class="btn btn-primary" id="restoreAll">< Restore All</button>
                            -->
                            <input class="btn btn-primary" type="button" value="Up">
                            <input  class="btn btn-primary" type="button" value="Down">
                        </div>

                        <select class="btn  btn-default col-md-offset-1 col-md-4">
                                <option value="1">Category 1</option>
                                <option value="2">Category 2</option>
                                <option value="3">Category 3</option>
                                <option value="4">Category 4</option>
                            </select>
                        <div id="drop"  class="tagbox col-md-12">
                        
                            <div class="form-group col-md-12">
                                <input class="form-control" type="search" id="searchTag2" placeholder="Search..." />
                            </div>
                            
                            <select id="select2" name="select2[]" multiple> 
                            </select>
                        
                        </div>
                    </div>


                </div>
                </div>
            
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" id="createTagBtn" class="btn btn-primary pull-right" value="Create">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>



</section>
 

@endsection