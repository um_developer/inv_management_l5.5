<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      {{ Config('params.site_name') }}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y');?> {{ Config('params.site_name') }}.</strong> All rights reserved. 
  </footer>


