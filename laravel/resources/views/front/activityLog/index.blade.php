@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
              <div class="row">
         
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'activityLog/searching', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-sm-6">
                    {!!   Form::select('customer_id', $customers, Request::input('customer_id'), array('class' => 'form-control' , 'id' => 'customer_id'))  !!}
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ url('activityLog') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Activity Logs</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vendors" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Log ID</th>
                    <th>Customer Count</th>
                    <th>Client Count</th>
                    <th>Invoices Count</th>
                    <th>Orders Count</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                    <th>Created By</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
               
                    @if(count($modal) >0)
                    @foreach($modal as $item)
                    <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->customer_count}}</td>
                    <td>{{$item->client_count}}</td>
                    <td>{{$item->invoices_count}}</td>
                    <td>{{$item->orders_count}}</td>
                    <td>{{$item->created_at}}</td>
                    <td>{{$item->updated_at}}</td>
                    <td>{{$item->type}}</td>
                    <td>{{$item->status}}</td>
                    <td>
                      <a href="{{ url('activityLogInvoices?type=invoices&id='.$item->id) }}" class="btn btn-primary"> Dummy Invoices</a>
                      <a href="{{ url('activityLogInvoices?type=orders&id='.$item->id) }}" class="btn btn-primary"> Dummy Orders</a>
                      <a href="{{ url('activityLogDelete?&id='.$item->id) }}" class="btn btn-danger"> Delete</a>
                    </td>
                </tr>
                    @endforeach
                  
                    @endif
               
                
                </tbody>
                <tfoot>
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>

<script>
  $(function () {
    $('#vendors').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "pageLength": {{Config::get('params.default_list_length')}},
      "autoWidth": false
    });
  });
</script>
@endsection



