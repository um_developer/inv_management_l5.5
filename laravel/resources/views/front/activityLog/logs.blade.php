@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
     <!-- <div class="row">
         
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'activityLog/search', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">
                <input type="hidden" name="type" value="<?php echo $type; ?>">
                <input type="hidden" name="log_id" value="<?php echo $log_id; ?>">
                    <div class="form-group col-sm-6">
                    <select class ='form-control' name="status" >
                    <option value="">Select Status</option>
                    <?php if($status == 'pending'){ ?> 
                      <option value="pending" selected='selected'>Pending</option>
                    <?php }else{ ?>
                      <option value="pending">Pending</option>
                    <?php } ?>
                    <?php if($status == 'completed'){ ?>
                    
                    <option value="completed" selected='selected'>Completed </option>
                    <?php }else{ ?>
                      <option value="completed">Completed </option>
                    <?php } ?>
                    </select>
                    </div>

                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ url('activityLog') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div> -->
             <div class="box">
            <div class="box-header">
            @if($type == 'invoices')
              <h3 class="box-title">Dummy Invoices</h3>
            @elseif($type == 'orders')
            <h3 class="box-title">Dummy Orders</h3>
            @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vendors" class="table table-bordered table-striped">
                <thead>
                <tr>
                   <th>Log ID</th>
                    @if($type == 'invoices')
                    <th>Customer Name</th>
                    @elseif($type == 'orders')
                    <th>Client Name</th>
                    @endif
                    @if($type == 'invoices')
                    <th>Invoice ID</th>
                    @elseif($type == 'orders')
                    <th>Orders ID</th>
                    @endif
                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
               
                    @if(count($modal) >0)
                    @foreach($modal as $item)
                    <tr>
                    <td>{{$log_id}}</td>
                    <td>{{$item->c_name}}</td>
                    @if($type == 'invoices')
                    <td>
                    <a href="{{ url('invoice/'.$item->id) }}">
                        Invoice # {{$item->id}} </a>
                    </td>
                    @elseif($type == 'orders')
                    <td>
                    <a href="{{ url('ref/order/detail/'.$item->id) }}">
                    Order # {{$item->id}}</a>
                    </td>
                    @endif
                    <td>{{$item->created_at}}</td>
                    <td><a href="{{ url('activityLogItemDelete/'.$item->id.'/'.$type) }}" onclick="return confirm('Are you sure you want to delete this')" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                </tr>
                    @endforeach
                  
                    @endif
               
                
                </tbody>
                <tfoot>
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>

<script>
  $(function () {
    $('#vendors').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "pageLength": {{Config::get('params.default_list_length')}},
      "autoWidth": false
    });
  });
</script>
@endsection



