@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
     <div class="row">
         
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Received Items Orders</h3>
               <a href="{{ url('receive-item/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Receive Items</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vendors" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>RO ID</th>
                   <th>PO ID</th>
                  <th>Vendor Name</th>
                  <th>Received Quantity</th>
                  <th>Total Price</th>
                   <th>Receive Date</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
               
                    @if(count($model) >0)
                    @foreach($model as $item)
                     <tr>
                <td><a href="{{ url('receive-item/'.$item->id) }}">{{$item->id}}</a></td>
                <td><a href="{{ url('purchase-order/'.$item->po_id) }}">{{$item->po_id}}</a></td>
                   <td><a href="{{ url('vendor/edit/'.$item->vendor_u_id) }}">{{$item->vendor_name}}</a></td>
                  <td>{{$item->received_quantity}}</td>
                    <td>{{$item->total_price}}</td>
                 
                  <td><?php echo date("d M Y", strtotime($item->receive_date)); ?></td>
                  <td>
                   <a href="{{ url('receive-item/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                  <a href="{{ url('receive-item/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                 
                  </td>
                </tr>
                    @endforeach
                  
                    @endif
               
                
                </tbody>
                <tfoot>
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>

<script>
  $(function () {
    $('#vendors').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "pageLength": {{Config::get('params.default_list_length')}},
      "autoWidth": false
    });
  });
</script>
@endsection



