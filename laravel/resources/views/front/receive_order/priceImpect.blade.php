@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
     <div class="row">
         
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'priceImpect/search', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">

                    
                    <div class="form-group col-sm-6">
                    <select class ='form-control' name="status" >
                    <option value="">Select Status</option>
                    <?php if($status == 'pending'){ ?> 
                      <option value="pending" selected='selected'>Pending</option>
                    <?php }else{ ?>
                      <option value="pending">Pending</option>
                    <?php } ?>
                    <?php if($status == 'completed'){ ?>
                    
                    <option value="completed" selected='selected'>Completed </option>
                    <?php }else{ ?>
                      <option value="completed">Completed </option>
                    <?php } ?>
                    </select>
                    </div>

                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('priceImpect') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div> 
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Update Price From Receive Order</h3>
               <a href="{{ url('priceImpectInvoiceAll2') }}" class="btn btn-primary pull-right" id="buttondisable"><i class="fa fa-plus"></i> Update Price All</a>
               <a href="{{ url('priceImpectadditem') }}" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i> Add Manual Record</a>
               <a href="{{ url('activityLog') }}" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i> Activity Logs</a>
               <a href="{{ url('addDummyInvoice') }}" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i> Add Dummy Invoice</a>
               <a href="{{ url('addDummyOrder') }}" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i> Add Dummy Order</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vendors" class="table table-bordered table-striped">
                <thead>
                <tr>                    
                    <th>Item Name</th>
                    <th>Old Price</th>
                    <th>Price Change</th>
                    <th>New Price</th>                    
                    <th>Impect</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th>Completed Date</th>
                    <th>Receive Order ID</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
               
                    @if(count($model) >0)
                    @foreach($model as $item)
                    <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->old_price}}</td>
                    <td>{{$item->diff_amount}}</td>
                    <td>{{$item->amount}}</td>
                    <td>{{$item->impact}}</td>
                    <td>{{$item->status}}</td>
                    <td>{{ \Carbon\Carbon::parse($item->created_at)->format('j F, Y ,g:i A') }}</td>
                    <?php if($item->status == 'completed'){ ?>
                    <td>{{ \Carbon\Carbon::parse($item->updated_at)->format('j F, Y ,g:i A') }}</td>
                    <?php }else{ ?>
                    <td>--</td>
                    <?php } ?>
                    <td><a href="{{ url('receive-item/'.$item->ro_id) }}"> {{$item->ro_id}}</a></td>
                    <?php if($item->status == 'pending'){ ?>
                      <td><a href="{{ url('priceImpectInvoice/'.$item->item_id.'/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Update Price</a>
                      <a href="{{ url('priceImpectDelete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this')" class="btn btn-danger"> Delete</a>
                      </td>
                    <?php }else{ ?>
                      <td><a href="#" class="btn btn-primary" disabled><i class="fa fa-external-link"></i> Update Price</a></td>
                    <?php } ?>
                </tr>
                    @endforeach
                  
                    @endif
               
                
                </tbody>
                <tfoot>
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>

<script>
  $(function () {
    $('#vendors').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "pageLength": {{Config::get('params.default_list_length')}},
      "autoWidth": false
    });
  });

  $(document).ready(function () {

$('#buttondisable').on('click', function (e) {
 $('#buttondisable').css({"opacity": ".4", "pointer-events": "none",  "cursor": "default !important"});
})
  })
</script>
@endsection



