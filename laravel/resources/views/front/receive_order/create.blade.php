@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Receive Items Order</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'receive-item/create', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Vendor</label>
                    {!!   Form::select('vendor_id', $vendors, Request::input('vendor_id'), array('class' => 'form-control','id' => 'vendor_id',$required ))  !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Purchase Order</label>
                    {!! Form::select('po_id',[''=>'--- Select Purchase Order ---'],Request::input('po_id'),['class'=>'form-control','id' => 'po_id',]) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Receive Date</label>
                    <div class='col-sm-4 input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="receive_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>
            </div>


            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Remaining QTY</th>
                            <th>Unit Cost</th>
                            <th>Receive Quantity</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>


            <script type="text/javascript">
                $(function () {
//            Bootstrap DateTimePicker v4
                    $('#datetimepicker4').datetimepicker({
                        format: 'DD-MM-YYYY'
                    });
                });
            </script>

            <script type="text/javascript">

                $(document).ready(function () {
                    $('#datetimepicker4').data("DateTimePicker").date(moment(new Date()).format('DD/MM/YYYY'));
                    var vendor_select = document.getElementById('vendor_id');
                    vendor_select.addEventListener('change', function () {
                        updatePO(vendor_select.value);
                    }, false);


                    var po_select = document.getElementById('po_id');
                    po_select.addEventListener('change', function () {
                        updateItemList(po_select.value);
                    }, false);
                    
                    $('.form-horizontal').submit(function () {
                $('#submit').prop('disabled', true);
            });
                    

                });

                function updatePO(vendor_id) {

                    var url = "<?php echo url('get/open-po-by-vendor'); ?>" + '/' + vendor_id;
                    $.ajax({
                        url: url,
                        type: 'get',
                        dataType: 'html',
                        success: function (response) {
                            $("select[name='po_id'").html('');
                            $("select[name='po_id'").html(response);
                            var $table = $('#item_body');
                            var $table = $('#item_body');
                            $table.find('tbody').empty();

                        },
                        error: function (xhr, status, response) {
                        }
                    });

                }

                function updateItemList(po_id) {

                    var url = "<?php echo url('get/po-detail'); ?>" + '/' + po_id;
                    var $table = $('#item_body');
                    $table.find('tbody').empty().append('Data Loading .....');
                    $.ajax({
                        url: url,
                        type: 'get',
                        dataType: 'html',
                        success: function (response) {
                            console.log(response);
                            var $table = $('#item_body');
                            $table.find('tbody').empty().append(response);
                            $table.trigger('reflow');
                        },
                        error: function (xhr, status, response) {
                        }
                    });

                }

//                $(function () {
//                    $('#item_body').DataTable({
//                        "paging": false,
//                        "lengthChange": false,
//                        "searching": false,
//                        "ordering": false,
//                        "info": false,
//                        "autoWidth": false
//                    });
//                });

            </script>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" id="submit" disabled="true" class="btn btn-primary pull-right cr_rc_btn" value="Create Receive Order">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

</section>

@endsection