@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<div class="table table-responsive">
<section id="form" class="mt30 mb30 col-sm-12 p0">
    
         <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Vendors Listing</h3>
                <a href="{{ url('vendor/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Vendor</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">

                    <thead>

                        <tr>
                            <th>Vendor ID</th>
                            <th>Vendor Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Balance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->u_id}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->phone}}</td>
                            <td>{{$item->balance}}</td>
                            <td>
                                <a href="{{ url('vendor/edit/'.$item->u_id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('vendor/detail/'.$item->u_id) }}" class="btn btn-primary">Details</a>
                                <a href="{{ url('vendor/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
     
</section>
</div>
<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
//       "dom": '<"toolbar">frtip'
        });
        $("div.toolbar")
                .html('<div class = "ml-5"><button type="button" class = "btn btn-primary pull-right important" id="any_button">Click Me!</button> </div>');
    });
</script>
@endsection



