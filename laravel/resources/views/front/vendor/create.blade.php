@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Create Vendor</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'vendor/create', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Vendor Name</label>
                     {!! Form::text('name', Request::input('name') , array('placeholder'=>"Vendor Name *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>

                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Phone</label>
                     {!! Form::text('phone', Request::input('phone') , array('placeholder'=>"Phone",'maxlength' => 20,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Phone 2</label>
                     {!! Form::text('phone_2', Request::input('phone_2') , array('placeholder'=>"Phone",'maxlength' => 20,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Email</label>
                     {!! Form::text('email', Request::input('email') , array('placeholder'=>"Email *",'maxlength' => 30,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Email 2</label>
                     {!! Form::text('email_2', Request::input('email_2') , array('placeholder'=>"Email *",'maxlength' => 30,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Address Line 1</label>
                     {!! Form::text('address1', Request::input('address1') , array('placeholder'=>"Address Line 1",'maxlength' => 400,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Address Line 2</label>
                     {!! Form::text('address2', Request::input('address2') , array('placeholder'=>"Address Line 2",'maxlength' => 400,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-6">
                       <label for="exampleInputEmail1">City</label>
                     {!! Form::text('city', Request::input('city') , array('placeholder'=>"City",'maxlength' => 30,'class' => 'form-control') ) !!}
                  </div>
                      
                       <div class="col-sm-6">
                       <label for="exampleInputEmail1">State</label>
                  
                     {!!   Form::select('state', $states, Request::input('state'), array('class' => 'form-control' ))  !!}
                  </div>
                </div>
               <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Zip Code</label>
                     {!! Form::text('zip_code', Request::input('zip_code') , array('placeholder'=>"Zip Code",'maxlength' => 200,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Note</label>
<!--                     {!! Form::text('note', Request::input('note') , array('placeholder'=>"Note ",'maxlength' => 20,'class' => 'form-control') ) !!}-->
                     {!! Form::textarea('note', Request::input('note') , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}
                     <!--{!! Form::textarea('placeOfDeath',null,['class'=>'form-control', 'rows' => 2, 'cols' => 10]) !!}-->
                  </div>
                </div>
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Create">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
        			
</section>

@endsection