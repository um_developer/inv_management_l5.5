@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Dummy Order</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'dummyOrderCreate', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!! Form::select('customer_id', $customers, Request::input('suctomer_id'), array('class' => 'form-control select2','id' => 'customer_id',$required )) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Client</label>
                    <select id='sel_emp' name='client_id' class="form-control select2">
                       <option value='0'>Select Client</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Items</label>
                    {!! Form::select('item_id', $items, Request::input('item_id'), array('class' => 'form-control select2','id' => 'item_id',$required )) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Difference</label>
                    {!! Form::text('diff', Request::input('diff') , array('placeholder'=>"Difference *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="radio" id="effect" value="1" name="effect" checked><b>Increase<b>
                    <input type="radio" id="effect" value="2" name="effect"><b>Decrease<b>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right cr_rc_btn" value="Save">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

</section>
<script type="text/javascript">
    $(document).ready(function () {
        var id = $('#customer_id').val();
     $.ajax({
        url: "{{ url('getClientData')}}",
        data: { "c_id": id },
        dataType:"html",
        type: "get",
        success: function(response){
             
            var array = JSON.parse(response);

            var clients = array.clients
            $("#sel_emp").empty();
            clients.forEach(function(object) {
                   var option = "<option value='"+object.id+"'>"+object.name+"</option>"; 
                 $("#sel_emp").append(option);
            });
        }
    });    
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    });

$('#customer_id').change(function () {
    var id = $(this).val();
     $.ajax({
        url: "{{ url('getClientData')}}",
        data: { "c_id": id },
        dataType:"html",
        type: "get",
        success: function(response){
             
            var array = JSON.parse(response);

            var clients = array.clients
            $("#sel_emp").empty();
            clients.forEach(function(object) {
                   var option = "<option value='"+object.id+"'>"+object.name+"</option>"; 
                 $("#sel_emp").append(option);
            });
        }
    });
});


 $('.form-horizontal').submit(function () {
$('#submit_btn').prop('disabled', true);
});



</script>
@endsection