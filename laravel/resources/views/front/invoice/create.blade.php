@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Invoice</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'invoice/create','id' => 'invoice_form', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">


            <div class="form-group">
                @if($customer_id == '0')
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $customers, Request::input('customer_id'), array('class' => 'form-control select2 select_customer','id' => 'customer_id','required' =>'required' ))  !!}
                </div>

                @else
                <input type="hidden"  name="customer_id" value="{{ $customer_id }}">
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $customers, $customer_id, array('class' => 'form-control select2 select_customer','id' => 'customer_id',$required ))  !!}
                </div>

                @endif
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Package #</label>
                    {!!   Form::select('package_id', $packages, Request::input('package_id'), array('class' => 'form-control select2 select_package','id' => 'package_id' ))  !!}

                </div>
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select2 select_bundle ', 'disabled' => true,'id' => 'bundle_id' ))  !!}

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', Request::input('memo') , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>
                
                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select List Title</label>
                    {!!   Form::select('list_id', $list_words, Request::input('list_id'), array('class' => 'form-control','id' => 'list_id' ))  !!}

                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Price Template Name</label>
                    <span class="form-control">{{ $template_name }}</span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Address</label>
                    <span class="form-control">{{ $address }}</span>
                </div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Phone #</label>
                    <span class="form-control">{{ $phone }}</span>
                </div>
            </div>

            <!--<button id="add_more"  class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</button><br><br>-->

            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a> <br><br>
            
             @include('front/common/invoice_js')
             
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody></tbody>
                    <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                            <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
            <span class="text-red" id="err_mesg"></span>
        </div>
        {!! Form::close() !!} 
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Create Invoice">
        </div>
        <!-- /.box-footer -->

    </div>

    @include('front/common/package_view_modal')
    @include('front/common/bundle_view_modal')
    @include('front/common/bundle_edit_modal')
    @include('front/common/image_modal')

    <script type="text/javascript">
        var j = 1;
//        var reloaded = 0;
        $(document).ready(function () {
            checkcustomer();
          function checkcustomer(){
          
            var customer_id = $("#customer_id option:selected").val();
          
if(customer_id==0){
//add disabled
$("#bundle_id").attr('disabled', 'disabled');
}else{
    $("#bundle_id").removeAttr("disabled");
}
          }

            $('.select_package').on('change', function () {
                var data = $(".select_package option:selected").text();
                loadPackageAjax(data, j);
            });
            $('.select_bundle').on('change', function () {
                var data = $(".select_bundle option:selected").val();
                var customer_id = $("#customer_id option:selected").val();
                var type="invoice";
                loadbundleAjax(data, j,customer_id,type);
            });

            var customer_select = document.getElementById('customer_id');
            $('.select_customer').on('change', function () {
                $('#submit_btn').prop('disabled', true);
                var url = "{{ url('/invoice/create/') }}"
                window.location = url + '/' + customer_select.value;
            });

        });

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });
            function editBundle(bundle_id ,qty){
                var j = 1;
                var data = $(".select_bundle option:selected").val();
                var customer_id = $("#customer_id option:selected").val();
                var type="invoice";
                loadbundleAjax2(bundle_id, j,customer_id,type,qty)
            }
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }
        function loadbundleAjax2(bundle_id, j,customer_id,type,qty) {
            var q =1;
            $.ajax({
                url: "<?php echo url('invoice/edit/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type+'/'+q,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle1');
                        modal.style.display = "block";
                        $(modal_bundle1).find('tbody').empty();
                        $(modal_bundle1).find('tbody').append(response);
                        document.getElementById('getQty1').value = qty;
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }

        function loadPackageAjax(package_id, j) {

            $.ajax({
                url: "<?php echo url('invoice/create/bulk/'); ?>" + '/' + package_id + '/' + j,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_package');
                        modal.style.display = "block";
                        $(modal_package).find('tbody').empty();
                        $(modal_package).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });

        }
        
//        $('.form-horizontal').submit(function(){
//    $(this).find(':input[type=submit]').prop('disabled', true);
//});

        $(document).ready(function () {
            $('#customer_id').select2({
              language: {
                noResults: function (params) {
                  alert("Customer not found. Please create this customer.");
                }
              }
            });
            $('#package_id').select2({
              language: {
                noResults: function (params) {
                  alert("This package is not existing please add this this package");
                }
              }
            });
            $('#bundle_id').select2({
              language: {
                noResults: function (params) {
                  alert("This Bundle is not existing please add this Bundle");
                }
              }
            });
        });

    </script>



</section>
@include('front/common/item_detail_modal')
@endsection