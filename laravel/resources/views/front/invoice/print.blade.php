@extends('customer_print_template')
@section('content')
<section class="invoice">


    <!-- info row -->
    <div class="row invoice-info">
        

        <div class="col-sm-9 invoice-col">
        
            <div class="upper_info">    
                <h4> Broadmax Distributor INC</h4>
                7 Odell Plaza , Ste 140<br>
                Yonkers, N.Y 10701<br>
                Phone: 877-333-9220 
            </div>

            <div class=" print-bill-to">
                <strong>Bill To</strong><br>
                <address>
                    <p>{{ $model->customer_name }}</p>
                    <p>
                    {{ $model->address1 }}
                    </p>
                    <p>{{ $model->address2 }}   
                    </p>
                    <p>{{ $model->phone }}</p>
                    <p>{{ $model->city }}, {{ $model->state }} {{ $model->zip_code }}</p>
                </address>
            </div>
        </div>
        <!-- /.col -->
        
        <div class="col-sm-1 invoice-col">   
        </div>
        <!-- /.col -->
        <div class="col-sm-3 invoice-col">


            <div class="print-logo">
                <img src="{{ asset('front/images/keap-logo.jpg')}}" />
            </div>
            <div class="barcode">            
                <?php
                echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($model->id, "C39") . '" alt="barcode" />';
                ?>
            </div>

            @if($model->list_title != '')
             <strong><span style="font-size: 30px" > {{ $model->list_title }}</span></strong><br>
             @endif
             @if($model->deleted == '2')
            <strong><span style="font-size: 20px" >Price Change Invoice #  {{ $model->id }}</span></strong><br>
            @else
            <strong><span style="font-size: 20px" >Invoice #  {{ $model->id }}</span></strong><br>
            @endif
            Customer Balance {{$model->balance}}<br>
            Statement Date: <?php echo date("d M Y", strtotime($model->statement_date)); ?>
            <br>
            <br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-print">
                <thead>
                    <tr>
                        @if($model->show_image == '1')
                        <th>Image</th>
                        @endif
                        <th>Name</th>
                        @if($model->show_serial == '1')
                        <th>Start Serial #</th>
                        <th>End Serial #</th>
                        @endif
                        <th>QTY</th>
                        @if($model->show_package_id == '1')
                        <th>Package ID</th>
                        @endif
                        @if($model->show_unit_price == '1')
                        <th>Unit Price</th>
                        @endif
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $addQuantity=0; $isBundle=0;?>
                    @if(count($po_items)>0)
                    @foreach($po_items as $items_1)
                    
                     <?php 
                     $bundle_detail = '';
                    if($items_1->bundle_name != ''){
                        if(isset($items_1->is_bundle)){
                         $bundle_detail = "";
                         $items_1->item_name = $items_1->item_name . $bundle_detail;
                         $bundleUnitPrice=$items_1->total_price/$items_1->bundle_quantity;
                        }else{
                         $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                         $items_1->item_name = $items_1->item_name . $bundle_detail;
                        }
                    }
                    if(isset($items_1->is_bundle) ){ $addQuantity=$addQuantity+$items_1->bundle_quantity; $isBundle=1; }else{  $addQuantity=$addQuantity+=$items_1->quantity; }
                 
                    ?>
                    
                    <tr>@if(isset($items_1->is_bundle))
                        <td></td>
                        @else
                        @if($model->show_image == '1')
                        @if($items_1->image == '')
                        <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                        @else
                        <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"</a></td>
                        @endif
                        @endif
                        @endif
                        <td>@if(isset($items_1->is_bundle)) <b>{{ $items_1->item_name }}</b>  @else{{ $items_1->item_name }} @endif</td>
                        {{-- <td>{{ $items_1->item_name }} <strong>{{ $bundle_detail }}</strong></td> --}}
                        @if($model->show_serial == '1')
                        <td>{{ $items_1->start_serial_number }}</td>
                        <td>{{ $items_1->end_serial_number }}</td>
                        @endif
                        <td>@if(isset($items_1->is_bundle)) {{ $items_1->bundle_quantity }} @else {{ $items_1->quantity }} @endif</td>
                        @if($model->show_package_id == '1')
                        <td>{{ $items_1->package_id }}</td>
                        @endif
                        @if($model->show_unit_price == '1')
                        <td>@if(isset($items_1->is_bundle)) {{ $bundleUnitPrice }} @else {{ $items_1->item_unit_price }} @endif</td>
                        @endif
                        <td>{{ $items_1->total_price }}</td>
                    </tr>

                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!--<p class="lead">Amount Due 2/22/2014</p>-->
            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                           <th style="width:12%"></th>
                            <th style="width:25%">Total Quantity:</th>
                            <td style="width:28%">@if(isset($isBundle) && $isBundle==1) {{ $addQuantity }} @else  {{ $model->total_quantity }} @endif</td>
                            <th style="width:25%">Total Price:</th>
                            <td style="width:15%;"><h4><b>${{ $model->total_price }}</b></h4></td>

                        </tr>


                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>

    <div class="row">
        <div class="col-xs-12">
            <b> {{ $setting->bottom_text_1 }}</b>
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                 {{ $setting->bottom_text_2 }}
            </p>
        </div>
    </div>
</section>
@endsection