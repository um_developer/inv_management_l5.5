@extends('customer_template')
<?php
$modal_title = 'Profit';
?>
@section('content')
<section class="invoice">
    <!-- title row -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif

    @if (session('bulk_error'))
    @foreach(session('bulk_error') as $item)

    @if($item['type'] == 'normal')
    <div class="alert alert-danger">
        {{ $item['message'] }}
    </div>
    @else
    <div class="alert alert-warning">
        {{ $item['message'] }}
        <a href="{{ url('package/'.$item['package_id']) }}" class="btn btn-primary btn-sm"> Unpack Package #{{ $item['package_id'] }}</a>
    </div>

    @endif

    @endforeach
    @endif

    @if (session('bulk_warnings'))
    @foreach(session('bulk_warnings') as $item)

    @if($item['type'] == 'normal')
    <div class="alert alert-warning">
        {{ $item['message'] }}
    </div>
    @else
    <div class="alert alert-warning">
        {{ $item['message'] }}
        <a href="{{ url('package/'.$item['package_id']) }}" class="btn btn-primary btn-sm"> Unpack Package #{{ $item['package_id'] }}</a>
    </div>

    @endif

    @endforeach
    @endif
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                @if($model->deleted == '2')
                <i class="fa fa-hdd-o"></i> Price Change Invoice # {{ $model->id }}
                @else
                <i class="fa fa-hdd-o"></i> Invoice # {{ $model->id }}
                @endif
                <small class="pull-right">Statement Date: <?php echo date("d M Y", strtotime($model->statement_date)); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <b>Invoice # {{ $model->id }}</b><br>
            @if($model->order_id != '0')
             <b>Sync Order # <a href="{{ url('ref/order/detail/'.$model->order_id) }}">{{$model->order_id}}</a></b>
             @endif
             <br>
            <b>Customer Name:</b> {{ $model->customer_name }}<br>
            <!--          <b>Account:</b> 968-34567-->
            <br>
            <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <!--          To
                      <address>
                        <strong>John Doe</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone: (555) 539-1037<br>
                        Email: john.doe@example.com
                      </address>-->
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

            <br>
            @if($model->status == 'rejected')
            @if($model->order_refrence > 0)
            <i class="fa fa-refresh" aria-hidden="true">
            @endif
            Invoice Status: <b class = "label label-danger btn-xs">{{ ucwords($model->status) }}</b><br>
            @elseif($model->status == 'approved')
            @if($model->order_refrence > 0)
            <i class="fa fa-refresh" aria-hidden="true">
            @endif
            Invoice Status: <b class = "label label-success btn-xs">{{ ucwords($model->status) }}</b><br>
            @elseif($model->status == 'delivered')
            @if($model->order_refrence > 0)
            <i class="fa fa-refresh" aria-hidden="true">
            @endif
            Invoice Status: <b class = "label label-primary btn-xs">{{ ucwords($model->status) }}</b><br>
            @elseif($model->status == 'processing')
            @if($model->order_refrence > 0)
            <i class="fa fa-refresh" aria-hidden="true">
            @endif
            Invoice Status: <b class = "label label-primary btn-xs">{{ ucwords($model->status) }}</b><br>
            @else
            @if($model->order_refrence > 0)
            <i class="fa fa-refresh" aria-hidden="true">
            @endif
            Invoice Status: <b class = "label label-warning btn-xs"> {{ ucwords($model->status) }}</b><br>
            @endif
            @if($model->status == 'pending')
            <br>
            <b> <a href="{{ url('invoice/edit/'.$model->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit ml-20"> Edit</i></a>
                @if(Auth::user()->role_id == 2 && $model->customer_id != '')
                <a href="{{ url('invoice/status/'.$model->id.'/approved') }}" onclick="return confirm('Are you sure you want to approve this invoice?')" class="btn btn-success btn-sm">Mark as Approve</i></a></b>
            @endif
            @endif
            
            Print Bundle in Group
            <input type="checkbox" name="print_type" id="print_type" @if($model->print_type=="bundle_item") checked @endif>
            <br>
            <a href="{{url('invoice-print/'.$model->id)}}" class="btn btn-success btn_print btn-sm"><i class="fa fa-print"></i> Print</a>
            @if($show_profit == 0)
            <a href="{{ url('invoice/'.$model->id.'/1') }}" class="btn btn-primary btn-sm">Show Details</i></a>
            @else
            <a href="{{ url('invoice/'.$model->id) }}" class="btn btn-danger btn-sm">Hide Details</i></a>
            @endif

            <script src="{{ asset('front/js/jquery.printPage.js')}}"></script>
            <script>
$(document).ready(function () {
$(".btn_print").printPage();
});
            </script>
            <br>
            <br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Start Serial #</th>
                        <th>End Serial #</th>
                        <th>QTY</th>
                        <th>Package ID</th>
                        <th>Bundle ID</th>
                        <th>Unit Price</th>
                        <th>Total Price</th>
                        @if($show_profit == 1)
                        <th>Total Cost</th>
                        <th>Profit</th>
                        <th>PO id</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @if(count($po_items)>0)
                    @foreach($po_items as $items_1)
                    
                    <?php 
                    if($items_1->bundle_name != ''){
                         $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                         $items_1->item_name = $items_1->item_name . $bundle_detail;
                    }
                    ?>
                    <tr>
                        @if($items_1->image == '')
                        <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                        @else
                        <td><a href='#' onclick="showItemGallery('{{$items_1->image}}','{{$items_1->item_id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"></a></td>
                        @endif
                        <td>{{ $items_1->item_name }}</td>
                        <td>{{ $items_1->start_serial_number }}</td>
                        <td>{{ $items_1->end_serial_number }}</td>
                        <td>{{ $items_1->quantity }}</td>
                        <td>{{ $items_1->package_id }}</td>
                        <td>{{ $items_1->bundle_id }}</td>
                        <td>{{ $items_1->item_unit_price }}</td>
                        <td>{{ $items_1->total_price }}</td>
                        @if($show_profit == 1)
                        <td>{{ $items_1->total_cost }}</td>
                        <td>{{ $items_1->profit }}</td>
                        @if($items_1->purchase_order_id != '0')
                        <td><a target="_blank" href="{{ url('purchase-order/'.$items_1->purchase_order_id) }}" >{{ $items_1->purchase_order_id }}</a></td>
                         @else
                         <td>Item Cost</td>
                         @endif
                        @endif
                    </tr>

                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
<!--          <p class="lead">Payment Methods:</p>
        

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>-->
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!--<p class="lead">Amount Due 2/22/2014</p>-->

            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                            <th style="width:50%">Total Quantity:</th>
                            <td>{{ $model->total_quantity }}</td>
                        </tr>
                        <tr>
                            <th>Total Price</th>
                            <td>{{ $model->total_price }}</td>
                        </tr>

                        @if($show_profit == 1)
                        <tr>
                            <th>Total Cost</th>
                            <td>{{ $model->total_cost }}</td>
                        </tr>

                        <tr>
                            <th>Profit</th>
                            <td>{{ $model->profit }}</td>
                        </tr>
                        @endif
                       
                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            @if($model->status == 'draft')
            <a href="{{ url('purchase-order/edit/'.$model->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> Edit Purchase Order</a>
            @endif
  <!--          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
            </button>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              <i class="fa fa-download"></i> Generate PDF
            </button>-->
        </div>
    </div>

</section>
<!--<section class="invoice">
<div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="fa fa-hdd-o"></i> Invoice # {{ $model->id }}
        <small class="pull-right">Created Date: <?php echo date("d M Y", strtotime($model->created_at)); ?></small>
      </h2>
    </div>
     /.col 
  </div>
    </section>-->
<script>
 $("#print_type").change(function() {
    if(this.checked) {
     var status="bundle_item "
    }else{
        var status='Normal'
    }
    var customer_id={{ $model->customer_id }};
    $.ajax({
                url: "<?php echo url('invoice/printType/'); ?>" + '/' + status + '/' + customer_id,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    // if (response != 0) {
                    //     var modal = document.getElementById('modal_bundle');
                    //     modal.style.display = "block";
                    //     $(modal_bundle).find('tbody').empty();
                    //     $(modal_bundle).find('tbody').append(response);
                    // }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
});
   
</script>
@endsection

<script>
    function showProfitModal(){

    var modal = document.getElementById('modal_profit_new');
    modal.style.display = "block";
    }

</script>
@include('front/common/image_modal')