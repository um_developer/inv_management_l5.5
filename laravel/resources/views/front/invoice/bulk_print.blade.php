@extends('customer_print_template')
@section('content')
<section class="invoice">

    <div class="row">
 <div class="box-body">

            <div class="table-responsive">
                <div class="col-md-12">

                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Inv #</th>
                            <th>Customer Name</th>
                            <th>Amount</th>
                            <th>Statement Date</th>
                            <th>Delivery Memo</th>
                            <th>Balance</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) > 0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item['id']}}</td>
                            <td>{{$item['customer_name']}}</td>
                            <td>{{$item['total_price']}}</td>
                            <td>{{$item['statement_date']}}</td>
                            <td>{{$item['delivered_comments']}}</td>
                            <td>{{$item['balance']}}</td>
                            <td></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

                </div></div>
            </div>
    </div>

   
</section>
@endsection