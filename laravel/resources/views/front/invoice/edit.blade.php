@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">

            @if($model->status != 'pending')
            <h3 class="box-title">Modify Invoice # {{ $model->id }}</h3>
            @else
            <h3 class="box-title">Edit Invoice # {{ $model->id }}</h3>
            @endif
            @if($model->order_id != '0')
            &nbsp; &nbsp;(Sync Order # <a href="{{ url('ref/order/detail/'.$model->order_id) }}">{{$model->order_id}}</a>)
            @endif
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if($is_sync == 1)
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'invoice/modified','id' => 'invoice_form', 'method' => 'post')) !!}
        @endif

        @if($model->status != 'pending')
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'invoice/modified','id' => 'invoice_form', 'method' => 'post')) !!}
        @else
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'invoice/update','id' => 'invoice_form', 'method' => 'post')) !!}
        @endif
        <input type="hidden"  name="id" value="{{ $model->id }}">
        <div class="box-body">


            <div class="form-group">
                @if($model->customer_id == '')
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $vendors, $model->customer_id, array('class' => 'form-control select2 select_customer ',$required, 'id' => 'customer_id','required' =>'required' ))  !!}
                </div>
                @else
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $vendors, $model->customer_id, array('class' => 'form-control ','id' => 'customer',$required, 'disabled' => true ))  !!}
                </div>
                @endif


                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Package #</label>

                    @if($model->status == 'delivered')
                    {!!   Form::select('package_id', $packages, Request::input('package_id'), array('class' => 'form-control select2','id' => 'package_id','disabled' => true ))  !!}
                    @else
                    {!!   Form::select('package_id', $packages, Request::input('package_id'), array('class' => 'form-control select2','id' => 'package_id' ))  !!}
                    @endif

                </div>
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    @if($model->status == 'delivered')
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select22 select_bundle','id' => 'bundle_id' ,'disabled' => true))  !!}
                    @else
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select22 select_bundle','id' => 'bundle_id'))  !!}
                    @endif
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-9">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', $model->memo , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>

                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select List Title</label>
                    {!!   Form::select('list_id', $list_words, $model->list_id, array('class' => 'form-control','id' => 'list_id' ))  !!}

                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" value="" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Price Template Name</label>
                    <span class="form-control">{{ $template_name }}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Address</label>
                    <span class="form-control">{{ $address }}</span>
                </div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Phone #</label>
                    <span class="form-control">{{ $phone }}</span>
                </div>
            </div>



            @if($model->status == 'delivered')
            <a href="#" id="add_more" class="btn btn-primary pull-right disabled"><i class="fa fa-plus"></i> Insert Item</a><br><br>
            @else
            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>
            @endif

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        ?>
                        @if(count($po_items)>0 && isset($po_items[0]))
                        @foreach($po_items as $items_1)
                        <?php
                        $delivered_items = 0; //$items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
                        ?>
                        
                         <?php 
                    if($items_1->bundle_name != ''){
                         $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                         $items_1->item_name = $items_1->item_name . $bundle_detail;
                    }
                    ?>
                        
                        @if(1==1)
                        <tr id="item_{{ $i }}">
                            <td><input class="form-control" type="text"  title= "{{$items_1->item_name}}" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
                                <input type="hidden" readonly="readonly" name="po_item_id_{{ $i }}" id="po_item_id_{{ $i }}"  value="{{$items_1->id}}">
                                <input type="hidden" name="num[]" id="num[]"></td>
                    <input type="hidden" name="package_id" id="package_id" value="{{ $items_1->package_id }}">
                    <input type="hidden" name="bundle_id" id="bundle_id" value="{{ $items_1->bundle_id }}">
                    <input type="hidden" id="item_package_id_{{ $i }}" name="item_package_id_{{ $i }}" value="{{ $items_1->package_id }}">
                    <input type="hidden" id="item_bundle_id_{{ $i }}" name="item_bundle_id_{{ $i }}" value="{{ $items_1->bundle_id }}">
                    <input type="hidden" name="item_image_{{ $i }}" id="item_image_{{ $i }}" value="{{ $items_1->image}}"></td>

                    @if($items_1->start_serial_number != '0')
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" onkeyup="updateQuantityStatus('{{ $i }}')" onchange="updateQuantityStatus('{{ $i }}')" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"readonly="readonly"  onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @else
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                    @if($items_1->package_id == 0)
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" @if($items_1->bundle_id!="") readonly="readonly" @endif onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @else
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"  readonly="readonly" onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @endif
                    @endif
                    @if(Auth::user()->role->role == 'client' && $model->status != 'delivered')
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="updatePriceCheck('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                    @else
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="updatePriceCheck('{{ $i }}')" type="number"  readonly="readonly" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                    @endif

                    <td><input class="form-control" min ="0" step="any" type="number" style="width: 150px;" onkeyup="hideSuggestionPopup()" name="total_{{ $i }}" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>
                    @if($items_1->package_id == 0 && $items_1->bundle_id=="")
                    <td>
                        <button onclick="showItemImage(this)" id="img_button_{{ $i }}" class="btn btn-success btn-sm" type="button"><i class="fa fa-image"></i></button>
                        <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete" onclick="deleteRow(this,{{ $i }})"></td>
                    @elseif($items_1->bundle_id!="")
                    <td>
                        <button onclick="showItemImage(this)" id="img_button_{{ $i }}" class="btn btn-success btn-sm" type="button"><i class="fa fa-image"></i></button>
                        @if($items_1->package_id!=0)
                        <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Bundle# {{ $items_1->bundle_id }}" onclick="deletebundleRow({{ $items_1->bundle_id }})">
                    
                        @endif
                    @if($items_1->package_id == 0 && $items_1->bundle_id=="")
                  
                     <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete" onclick="deleteRow(this,{{ $i }})">
                  
                    @elseif($items_1->bundle_id!="")
                    <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Bundle# {{ $items_1->bundle_id }}" onclick="deletebundleRow({{ $items_1->bundle_id }})">
                    <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete item" onclick="deleteRow(this)">
                    <input id="edit_btn_{{ $i }}" name="edit_btn_{{ $i }}" class="btn btn-primary btn-sm" type="button" value="Edit Bundle# {{ $items_1->bundle_id }}" onclick="editBundle(<?php echo $items_1->bundle_id; echo ',';echo $items_1->bundle_quantity; ?>)"> 
                    @elseif($items_1->package_id != 0)
                    <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Package# {{ $items_1->package_id }}" onclick="deletePackageRowEdit({{ $items_1->package_id }})">
                  
                    @endif
                        <!--<td><input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" disabled="true" type="button" value="Delete" onclick="deleteRow(this,{{ $i }})"></td>-->
                    @endif
                    @if(1 == 2)
                     <input id="edit_btn_{{ $i }}" name="edit_btn_{{ $i }}" class="btn btn-primary btn-sm" type="button" value="Edit Bundle# {{ $items_1->bundle_id }}" onclick="editBundle(<?php echo $items_1->bundle_id; echo ',';echo $items_1->bundle_quantity; ?>)"> 
                     @endif
                    </tr>

                    <?php $i++; ?>
                    @endif
                    @endforeach

                    @endif

                    </tbody>
                    <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                            <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
            <span class="text-red" id="err_mesg"></span>
            @include('front/common/package_view_modal')
            @include('front/common/bundle_view_modal')
            @include('front/common/bundle_edit_modal')
            @include('front/common/invoice_edit_js')
            @include('front/common/image_modal')
        </div>
        {!! Form::close() !!} 
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Update Invoice">
            @if(Auth::user()->role_id == 2 && $model->status == 'pending')
            <a href="{{ url('invoice/'.$model->id) }}" class="btn btn-success btn-sm">Mark as Approve</i></a></b>
            @endif

        </div>
    </div>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });

        $(document).ready(function () {
            $('#package_id').select2({
                language: {
                    noResults: function (params) {
                        alert("This package is not existing please add this this package");
                    }
                }
            });
            $('#bundle_id').select2({
                language: {
                    noResults: function (params) {
                        alert("This bundle is not existing please add this this package");
                    }
                }
            });
        });
        $('.select_bundle').on('change', function () {
        var data = $(".select_bundle option:selected").val();
                var customer_id = "{{ $model->customer_id }}";
                if (customer_id != '' && customer_id != '0'){
                    var type="invoice"; 
        loadbundleAjax(data, j, customer_id,type);
        }
        });
        function editBundle(bundle_id,qty){
            // alert(qty)
                var j = 1;
                var data = $(".select_bundle option:selected").val();
                var customer_id = $("#customer_id option:selected").val();
                var type="invoice";
                loadbundleAjax2(bundle_id, j, customer_id, type, qty)
            }
                function loadbundleAjax(bundle_id, j, customer_id,type) {
                $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j + '/' + customer_id+ '/' +type,
                        type: 'get',
                        dataType: 'html',
                        success: function (response) {
                        if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                                modal.style.display = "block";
                                $(modal_bundle).find('tbody').empty();
                                $(modal_bundle).find('tbody').append(response);
                        }
                        },
                        error: function (xhr, status, response) {
                        alert(response);
                        }
                });
                }
                function loadbundleAjax2(bundle_id, j,customer_id,type, qty) {
            $.ajax({
                url: "<?php echo url('invoice/edit/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type+'/'+qty,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle1');
                        modal.style.display = "block";
                        $(modal_bundle1).find('tbody').empty();
                        $(modal_bundle1).find('tbody').append(response);
                        document.getElementById('getQty1').value = qty;
 
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }
        $(document).ready(function () {

        var customer_select = document.getElementById('customer_id');
                var invoice_id = "{{ $model->id }}";
                $('.select_customer').on('change', function () {
        if (customer_select.value != 0) {
        var url = "{{ url('/invoice/edit/') }}" + "/" + invoice_id + "/" + customer_select.value;
                window.location = url;
        } else {
        alert('Please Select any customer');
        }
        });
        });

    </script>


</section>
@include('front/common/item_detail_modal')
@endsection