@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Package Invoice</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'invoice/create','id' => 'invoice_form', 'method' => 'post')) !!}
        <input type="hidden"  name="package_id" value="{{ $package_id }}">
        <div class="box-body">

            @if(1==1)
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $vendors, $model->customer_id, array('class' => 'form-control',$required ))  !!}
                </div>
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Package #</label>
                    {!!   Form::select('package_id', $packages, $package_id, array('class' => 'form-control select2','id' => 'package_id' ))  !!}

                </div>
            </div>
            @endif
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', $model->memo , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>
            </div>

            <!--<a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>-->

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        <tr>
                            <th>Item Name</th>
<!--                            <th>Start Serial #</th>
                            <th>End Serial #</th>-->
                            <th>QTY</th>
                            <th style="width: 150px;">
                                
                            
                            Unit Price
                            </th>
                            <th style="width: 150px;">Total</th>
                            <!--<th>Action</th>-->

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        ?>
                        @if(count($po_items)>0 && isset($po_items[0]))
                        @foreach($po_items as $items_1)
                        <?php
//                        print_r($items_1); die;
                        $delivered_items = $items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
//                        $validation_num = '';
                        ?>
                        @if(1==1)
                        <tr id="item_{{ $i }}">
                            <td><input class="form-control" type="text" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
                                <input type="hidden" readonly="readonly" name="item_type_{{ $i }}" id="item_type_{{ $i }}"  value="">
                                <input type="hidden" name="num[]" id="num[]"></td>
                            
                            <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"readonly="readonly"  onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>

<!--                            @if($items_1->start_serial_number != '0')
                            <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                            <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" onkeyup="updateQuantityStatus('{{ $i }}')" onchange="updateQuantityStatus('{{ $i }}')" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                            
                            @else
                            <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                            <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                            <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                            @endif-->
                            @if(Auth::user()->role->role == 'client')
                            <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="calculatePrice('{{ $i }}')" style="width: 150px;" onchange="updatePriceCheck('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                            @else
                            <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="calculatePrice('{{ $i }}')" style="width: 150px;" onchange="updatePriceCheck('{{ $i }}')" type="number"  readonly="readonly" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                            @endif

                            <td><input class="form-control" min ="0" step="any" type="number" style="width: 150px;" name="total_{{ $i }}" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>
                            <!--<td><input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete" onclick="deleteRow(this,{{ $i }})"></td>-->
                        </tr>

                        <?php $i++; ?>
                        @endif
                        @endforeach

                        @endif

                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
        {!! Form::close() !!} 
        <div class="box-footer">
            <input type="button" name="submit" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Create Invoice">
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
                var j = 1;
                $(document).ready(function () {

                $('.select2').on('change', function () {
                var data = $(".select2 option:selected").text();
                var table = $('#item_body').DataTable();
                table.clear().draw();
                loadPackage(data);
                });
                });
                $(function () {
                //Initialize Select2 Elements
                $('.select2').select2()
                });
                function loadPackage(package_id) {
                if (package_id == 'Select Package'){
                window.location.href = "{{ url('invoice/create') }}";
                }
                else{
                window.location.href = "{{ url('invoice/create/bulk') }}" + '/' + package_id; }
                }

                function validateSerialItem(type = 'new') {
                $("#invoice_form").submit();
                }

$(function () {
    $('#item_body').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "language": {
            "emptyTable": "Table is reloaded. Please add items again"
        },
        "columns": [
            {"width": "10%"},
            {"width": "10%"},
            {"width": "10%"},
            {"width": "10%"},
//            {"width": "10%"},
//            {"width": "10%"},
        ]

    });
});
    </script>
</section>
@endsection