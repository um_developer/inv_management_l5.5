@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filter by Item</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>

                    {!! Form::open(array( 'class' => '','url' => 'package/search', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-sm-4">

                                {!!  Form::select('item_id', $items, Request::input('item_id'), array('class' => 'form-control','id' => 'item_id' ))  !!}
                            </div>
                            @if(1==2)
                            <div class="form-group col-sm-4">
                                {!!  Form::select('warehouse_id', $warehouses, Request::input('warehouse_id'), array('class' => 'form-control','id' => 'warehouse_id' ))  !!}
                            </div>
                            @endif
                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <!--                        <div class="form-group col-sm-6">
                                                        <button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-search"></i> Search</button>
                                                    </div>-->
                            <div class=" form-group col-sm-2">
                                <a href="{{ URL::to('packages') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-2">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Packages</h3>
                <a href="{{ url('package/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Package</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Package Code</th>
                            <th>Package Type</th>
                            <th>Vendor ID</th>
                            <th>Location</th>
                            <th>Item Quantity</th>
                            <th>Status</th>
                            <th>Created Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->package_type}}</td>
                            <td>{{$item->vendor_id}}</td>
                            @if($item->location == '')
                            <td>Admin Inventory</td>
                            @else
                            <td>{{$item->location}}</td>
                            @endif
                            <td>{{$item->quantity}}</td>
                            @if($item->status == 'assigned')
                            <td><a class="btn btn-primary btn-xs"> Assigned</a></td>
                            @elseif($item->status == 'unassigned')
                            <td><a class="btn btn-success btn-xs"> Unassigned</a></td>
                            @else
                            <td><a class="btn btn-success btn-xs"> invoice # {{ $item->invoice_id }}</a></td>

                            @endif
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            <td>
                                <a href="{{ url('package/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                <a href="{{ url('package/generate/pdf/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
                                @if($item->status == 'unassigned')
                                <a href="{{ url('package/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                                @endif


                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
        });
    });
</script>
@endsection



