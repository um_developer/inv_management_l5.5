@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Transfer Package</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'package/transfer/all', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">

            <div class="form-group">
                 
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Source</label>
                    {!!   Form::select('warehouse_id', $from_warehouses, Request::input('warehouse_id'), array('class' => 'form-control','id' => 'warehouse_id',$required ))  !!}
                </div>
                
                 <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select Package</label>
                     {!!   Form::select('package_id', $all_packages, Request::input('package_id'), array('class' => 'form-control select2','id' => 'package_id',$required ))  !!}
                </div>
                
                 <div class="col-sm-2">
                    <label for="exampleInputEmail1">Total Packages</label>
                    {!!  Form::input('number', 'quantity', 1, $options = array('min' => '1', 'class' => 'form-control','id' => 'quantity',$required)) !!}
                </div>
                
                 <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select Warehouse</label>
                    {!!   Form::select('warehouse_t_id', $all_warehouses, Request::input('warehouse_t_id'), array('class' => 'form-control','id' => 'warehouse_t_id',$required ))  !!}
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Transfer">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

   
</section>
 <script type="text/javascript">

                $(document).ready(function () {

                    var warehouse_select = document.getElementById('warehouse_id');
                    warehouse_select.addEventListener('change', function () {
                        updatePackages(warehouse_select.value);
                    }, false);
                     });
                    
                      function updatePackages(warehouse_id) {
                   
                    var url = "<?php echo url('get/package-by-warehouse'); ?>" + '/' + warehouse_id;
                    $.ajax({
                        url: url,
                        type: 'get',
                        dataType: 'html',
                        success: function (response) {
                          
                            $("select[name='package_id'").html('');
                            $("select[name='package_id'").html(response);
                        },
                        error: function (xhr, status, response) {
                        }
                    });

                }
                
                 $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
	});

                     </script>
@endsection