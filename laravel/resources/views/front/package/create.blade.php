@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Package</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'package/create', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">

           <div class="form-group">
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Package Type</label>
                    {!!   Form::select('package_type', array('box' => 'Box','pallet' => 'Pallet'), Request::input('package_type'), array('class' => 'form-control',$required ))  !!}

                </div>
              <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Vendor</label>
                    {!!   Form::select('vendor_id', $vendors, Request::input('vendor_id'), array('class' => 'form-control',$required ))  !!}
                </div>

               @if(1==2)
                <div id="initial_code" class="col-sm-4">
                    <label for="exampleInputEmail1">Package Initial Code (4-Digits)</label>
                    {!!  Form::input('text', 'pkg_initials', Request::input('pkg_initials'), $options = array( 'class' => 'form-control','maxlength'=>'4','minlength'=>'4')) !!}
                </div>
               @endif
            </div>

            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        <tr>
                            <th>Item Name</th>
                            <th>SKU/Item Code</th>
                            <th>UPC/Barcode</th>
                            <th>Remaining Qty</th>
                            <th>Total Qty</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>
            <div class="row" hidden="true">

                <div class="col-xs-8"></div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Total Amoount for Purchase Order</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Total:</th>
                                <td id="po_total">$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

            <script type="text/javascript">
var j = 1;
$(document).ready(function () {

    InsertNewRow();
});
function vendorStatus(item){
    
    if(item.value == '0'){
    $('#initial_code').show();
    } else{
       $('#initial_code').hide(); 
    }

}

function InsertNewRow() {
    var i = window.j;
    var scntDiv = '<tr><td><input type="text" name="item_name_' + i + '" class="form-control" id="item_name_' + i + '"  autocomplete="on" placeholder="Enter item name"><input type="hidden" readonly="readonly" name="item_id_' + i + '" id="item_id_' + i + '"><input type="hidden" name="num[]" id="num[]"></td><td><input type="text" readonly="readonly" name="sku_' + i + '" class="form-control" id="sku_' + i + '"></td><td><input type="text" name="upc_barcode_' + i + '" class="form-control" readonly="readonly" id="upc_barcode_' + i + '"></td><td><input type="number" readonly="readonly"  name="r_quantity_' + i + '" class="form-control" id="r_quantity_' + i + '"></td><td><input type="number" name="quantity_' + i + '" class="form-control"  min ="1" max = "2" id="quantity_' + i + '"></td></tr>';
    var item_array = <?php echo json_encode($items, JSON_PRETTY_PRINT) ?>;

    $("#item_body").append(scntDiv);
    $(function ($) {
        $('#item_name_' + i).autocomplete({
            source: item_array,
            select: function (event, ui) {

                $("#item_id_" + i).val(ui.item.id);
                $("#sku_" + i).val(ui.item.sku);
                $("#upc_barcode_" + i).val(ui.item.upc_barcode);
                $("#r_quantity_" + i).val(ui.item.inventory_quantity);
                $("#num[0]").val(ui.item.id);
                $("#quantity_" + i).val('1');
                $("#quantity_" + i).attr('max', ui.item.inventory_quantity);


            }
        });


    });
    window.j++;
    return false;
}

$('#add_more').on('click', function () {
    InsertNewRow();
    // getDatableData();
});

$(function () {
    $('#item_body').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false
    });
});

            </script>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Create Package">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>


    <div class="modal" id="myModal1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Default Modal</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </div>

    </div>

</section>

@endsection