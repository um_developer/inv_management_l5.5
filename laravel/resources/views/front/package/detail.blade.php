@extends('customer_template')

@section('content')
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-hdd-o"></i> Package # {{ $model->id }}
            <small class="pull-right">Created Date: <?php echo date("d M Y", strtotime($model->created_at)); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <!--<b>Order # {{ $model->id }}</b><br>-->
          <br>
          <b>Vendor Name:</b> {{ $model->vendor_name }}<br>
          <b>Order Modified Date:</b> <?php echo date("d M Y", strtotime($model->updated_at)); ?><br>
<!--          <b>Account:</b> 968-34567-->
 <br>
  <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
<!--          To
          <address>
            <strong>John Doe</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (555) 539-1037<br>
            Email: john.doe@example.com
          </address>-->
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          
          <br>
         @if($model->status == 'assigned')
          <b class = "btn btn-primary btn-xs">{{ $model->status }}</b><br>
        @else
        <b class = "btn btn-success btn-xs">{{ $model->status }}</b>
          <a href="{{ url('package/delete/'.$model->id) }}" class="btn btn-primary btn-xs"> Unpack Package</a><br>
         @endif
          <b>Package Type:</b> {{ $model->package_type }}<br>
          <!--<b>Order Modified Date:</b> <?php echo date("d M Y", strtotime($model->updated_at)); ?><br>-->
<!--          <b>Account:</b> 968-34567-->
 <br>
  <br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
        <tr>
              <th>Name</th>
              <th>SKU</th>
              <th>Item code</th>
              <th>QTY</th>
            
            </tr>
            </thead>
            <tbody>
                 @if(count($po_items)>0)
                @foreach($po_items as $items_1)
            <tr>
              <td>{{ $items_1->item_name }}</td>
              <td>{{ $items_1->item_sku }}</td>
              <td>{{ $items_1->item_code }}</td>
              <td>{{ $items_1->quantity }}</td>
            </tr>
             
                @endforeach
                @endif
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
<!--          <p class="lead">Payment Methods:</p>
        

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>-->
        </div>
        <!-- /.col -->
<!--        <div class="col-xs-6">
          <p class="lead">Amount Due 2/22/2014</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Total Quantity:</th>
                <td>{{ $model->total_quantity }}</td>
              </tr>
              <tr>
                <th>Total Cost</th>
                 <td>{{ $model->total_cost }}</td>
              </tr>
             
            </tbody></table>
          </div>
        </div>-->
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
            @if($model->status == 'draft')
          <a href="{{ url('purchase-order/edit/'.$model->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> Edit Purchase Order</a>
          @endif
<!--          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>-->
        </div>
      </div>
    </section>

@endsection