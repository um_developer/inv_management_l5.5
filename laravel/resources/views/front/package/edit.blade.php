@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Edit Purchase Order</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'purchase-order/update', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <input type="hidden"  name="id" value="{{ $model->id }}">
        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Vendor</label>
                    {!!   Form::select('vendor_id', $vendors, $model->vendor_id, array('class' => 'form-control',$required ))  !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Tracking Info</label>
                    {!! Form::text('tracking_info', $model->tracking_info , array('placeholder'=>"Tracking Info",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>
            </div>

            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        <tr>

                            <th>Name</th>
                            <th>Start Serial #</th>
                            <th>QTY</th>
                            <th>Unit Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @if(count($po_items)>0)
                        @foreach($po_items as $items_1)
                        <?php
                        $delivered_items = $items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
                        ?>
                        @if(1==1)
                        <tr>
                            <td><input class="form-control" type="text" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
                                <input type="hidden" readonly="readonly" name="po_item_id_{{ $i }}" id="po_item_id_{{ $i }}"  value="{{$items_1->id}}">
                                <input type="hidden" name="num[]" id="num[]"></td>

                            @if($items_1->start_serial_number != '0')
                            <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                            @else
                            <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                            @endif

                            <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                            <td><input class="form-control" min ="0" step="any" onkeyup="calculatePrice('{{ $i }}')" onchange="calculatePrice('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                            <td><input class="form-control" min ="0" step="any" type="number" name="total_{{ $i }}" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>
                        </tr>


                        <?php $i++; ?>
                        @endif
                        @endforeach

                        @endif

                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div>
            <div class="row" hidden="true">

                <div class="col-xs-8"></div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Total Amount for Purchase Order</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Total:</th>
                                <td id="po_total">$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

            <script type="text/javascript">

                                var j = {{ count($po_items) + 1 }}
                                $(document).ready(function(){
                                InsertNewRow();
                                });
                                function calculatePrice(i){

                                var price = ($("#price_" + i).val());
                                var quantity = parseInt($("#quantity_" + i).val());
                                // alert(price);
                                var total = price * quantity;
                                // alert(total);
                                $("#total_" + i).val(total);
                                }

                                function getDatableData(){
                                var aTable = $('#item_body').DataTable();
                                var w = aTable.cell(0, 2).nodes().to$().find('input').val()
                                        alert(w);
                                var x = document.getElementById("item_body").rows[1].cells[4];
                                console.log(x);
                                alert(x);
                                alert(document.getElementById("item_body").rows[0].cells.innerHTML);
                                }

                                function InsertNewRow () {
                                var i = window.j;
                                var validation_num = "{{ $validation_num }}"

                                        var scntDiv = '<tr><td><input type="text" name="item_name_' + i + '" class="form-control" id="item_name_' + i + '"  \n\
                autocomplete="on" placeholder="Enter item name"><input type="hidden" readonly="readonly" name="item_id_' + i + '" id="item_id_' + i + '">\n\
            <input type="hidden" name="num[]" id="num[]"></td><td><input type="number" min ="0" name="start_serial_number_' + i + '" class="form-control" readonly="readonly" id="start_serial_number_' + i + '"></td><td><input type="number" min ="0" name="quantity_' + i + '" class="form-control" id="quantity_' + i + '"></td><td><input ' + validation_num + ' name="price_' + i + '" class="form-control"  step=any id="price_' + i + '"></td><td><input type="number" name="total_' + i + '" class="form-control" readonly="readonly" id="total_' + i + '"></td></tr>';
                                var item_array = <?php echo json_encode($items, JSON_PRETTY_PRINT) ?>;
                                $("#item_body").append(scntDiv);
                                $(function ($) {
                                $('#item_name_' + i).autocomplete({
                                source: item_array,
                                        select: function (event, ui) {

                                        $("#item_id_" + i).val(ui.item.id);
                                        $("#price_" + i).val(ui.item.cost);
                                        $("#quantity_" + i).val('1');
                                        if (ui.item.serial == 'yes'){
                                        $("#start_serial_number_" + i).val('1'); // save selected id to hidden input    
                                        $("#start_serial_number_" + i).prop("readonly", false);
                                        }

                                        calculatePrice(i);
                                        }
                                });
                                $("#price_" + i).on('keyup change', function (){
                                calculatePrice(i);
                                });
                                $("#item_name_" + i).on('blur', function () {
                                if ($("#item_id_" + i).val() == '') {
                                var modal = document.getElementById('myModal');
                                modal.style.display = "block";
                                }

                                });
                                $("#quantity_" + i).on('keyup change', function (){
                                calculatePrice(i);
                                });
                                });
                                window.j++;
                                return false;
                                }

                                $('#add_more').on('click', function () {
                                InsertNewRow();
                                // getDatableData();
                                });
                                $(function () {
                                $('#item_body').DataTable({
                                "paging": false,
                                        "lengthChange": false,
                                        "searching": false,
                                        "ordering": false,
                                        "info": false,
                                        "autoWidth": false
                                });
                                });

            </script>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update Purchase Order">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

</section>

@endsection