@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Update Customer</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer-care-manager/update', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
            <div class="form-group">
                 {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
               
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">First Name</label>
                    {!! Form::text('firstName', $model->firstName , array('placeholder'=>"First Name",'maxlength' => 100,'class' => 'form-control') ) !!}
                </div>
            </div>

            
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Last Name</label>
                    {!! Form::text('lastName', $model->lastName , array('placeholder'=>"Last Name",'maxlength' => 100,'class' => 'form-control') ) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Enter New Password</label>
                    <!--{!! Form::text('password', $model->password , array('placeholder'=>"Password",'maxlength' => 100,'class' => 'form-control') ) !!}-->
                    {!! Form::input('password1', 'password1', '',array('placeholder'=>"Enter New Password",'maxlength' => 100,'class' => 'form-control')) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Email</label>
                    <span class="form-control">{{ $model->email }}</span>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>


</section>

@endsection