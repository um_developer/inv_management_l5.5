@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<div class = "table table-responsive" >
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        
         @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Customers Listing</h3>
                <a href="{{ url('customer/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create New Customer</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                           
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <!--<th>Status</th>-->
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->firstName}}</td>
                            <td>{{$item->lastName}}</td>
                            <td>{{$item->email}}</td>
                            @if(1==2)
                            <td>
                              @if($item->status == 1)
                                <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}')" checked data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @else
                               <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}')" data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @endif
                                </td>
                                @endif
                            <td>
                               
                                <a href="{{ url('customer-care-manager/edit/'.$item->id) }}" class="btn btn-primary">Change Password</a>
                                <!--<a href="{{ url('customer/delete/'.$item->u_id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>-->
                                

                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>
@endsection