@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
     <div class="row">
         
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Categories Listing</h3>
              <a href="{{ url('category/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create New Category</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categories" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Category ID</th>
                  <th>Category Name</th>
                 
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
               
                    @if(count($categories) >0)
                    @foreach($categories as $category)
                     <tr>
                  <td>{{$category->id}}</td>
                  <td>{{$category->name}}</td>
                  
                  <td>
                  <a href="{{ url('category/edit/'.$category->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                  <a href="{{ url('category/delete/'.$category->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                    @endforeach
                    @else
                    No Category Found
                    @endif
               
                
                </tbody>
                <tfoot>
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>

<script>
  $(function () {
//    $("#example1").DataTable();
    $('#categories').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "pageLength": {{Config::get('params.default_list_length')}},
    });
  });
</script>
@endsection



