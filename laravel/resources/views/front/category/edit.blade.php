@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Update Category</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->vendor_create) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->vendor_create->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'category/update', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                <div class="form-group">
                  <div class="col-sm-12">
                      {!! Form::hidden('id', $category->id , array('maxlength' => 20,$required) ) !!}
                       <label for="exampleInputEmail1">Category Name</label>
                     {!! Form::text('name', $category->name , array('placeholder'=>"Category Name *",'maxlength' => 40,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
        			
</section>

@endsection