@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
if (!isset($user_id))
    $user_id = '';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Add Payment</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'client/payment/create', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">

            {!! Form::hidden('user_type', $user_type ,Request::input('user_type') , array('maxlength' => 20,$required) ) !!}
            <div class="form-group">
                <div class="col-sm-6">
                    @if($user_type == 'customer')
                    <label for="exampleInputEmail1">Select Customer</label>
                    @else
                    <label for="exampleInputEmail1">Select Vendor</label>
                    @endif
                    
                    {!!   Form::select('user_id', $users, Request::input('user_id'), array('class' => 'form-control select2 us_id' ))  !!}
                    
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Clients</label>
                    <select id='sel_emp' name='client_id' class="form-control select2">
                       <option value='0'>Select Client</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Amount</label>
                    {!! Form::text('amount', Request::input('amount') , array('placeholder'=>"Amount ",'maxlength' => 10,'class' => 'form-control',$required) ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Payment Type</label>
                    <select id='payment_source_id' name='payment_source_id' class="form-control select2">
                       <option value='0'>Select Payment Type</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Account Type</label>
                    <select id='account_type_id' name='account_type_id' class="form-control select2">
                       <option value='0'>Select Account Type</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>


            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Payment Message</label>
                    {!! Form::textarea('note', Request::input('note') , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" id="submit_btn" name="submit" class="btn btn-primary pull-right" value="Create">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });

        $(function () {
//            Bootstrap DateTimePicker v4
            $('#datetimepicker4').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });

        $(document).ready(function () {

            $('.us_id').change(function () {
                var id = $(this).val();
                 $.ajax({
                    url: "{{ url('getClientData')}}",
                    data: { "c_id": id },
                    dataType:"html",
                    type: "get",
                    success: function(response){
                         
                        var array = JSON.parse(response);

                        var clients = array.clients
                        clients.forEach(function(object) {
                               var option = "<option value='"+object.id+"'>"+object.name+"</option>"; 
                             $("#sel_emp").append(option); 
                        });

                        var payment_sources = array.payment_sources
                        payment_sources.forEach(function(object) {
                               var option = "<option value='"+object.id+"'>"+object.title+"</option>"; 
                             $("#payment_source_id").append(option); 
                        });

                        var account_sources = array.account_sources
                        account_sources.forEach(function(object) {
                               var option = "<option value='"+object.id+"'>"+object.title+"</option>"; 
                             $("#account_type_id").append(option); 
                        });
                    }
                });
            });

            $('#datetimepicker4').data("DateTimePicker").date(moment(new Date()).format('MM/DD/YYYY'));
            
             $('.form-horizontal').submit(function () {
            $('#submit_btn').prop('disabled', true);
        });


        });
    </script>


</section>

@endsection