@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">

    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    <!-- {!! Form::open(array( 'class' => '','url' => 'customer/orders/search', 'method' => 'get')) !!} -->
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <!-- <div class="form-group col-sm-3"> -->
                                <!-- <select name="id_type" id="id_type" class="form-control select2">
                                    <option value="1">Invoice</option>
                                    <option value="2">Order</option>
                                    
                                </select> -->
                                <!-- </div> -->
                                <div class="form-group col-sm-3">
                                    <input id="number" type="text" name="number" value="" placeholder="Number" min="6" class="form-control onloadFocus" autofocus />
                                    <span id="errorMsg" style="display:none;color: red;">Please enter minimum 6 digits</span>
                                </div>

                                <div class="form-group col-sm-3">

                                </div>
                                <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                                <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                                <script>
  $(function () {
    $('.onloadFocus').trigger("focus");
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                                </script>


                                <div class="clearfix"></div>
                                <input type="hidden" class="form-control" name="page" id="page" value="1">
                                <div class="clearfix"></div>
                                <div class=" form-group col-sm-3">
                                    <a href="{{ URL::to('add-new-payment/') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                                </div>
                                <div class=" form-group col-sm-3">
                                    <button id="submit" class="btn btn-primary btn-block btn-flat" name="submit">Search</button>
                                </div>
                            </div>
                        </div>
                        <!-- {!! Form::close() !!}  -->
                    </div>
                </div>
            </div>

            <!-- <div class="box"> -->
            <div class="box-body" id="body" style="display: none">


                <!-- <div class="form-group">
                    <div class="col-sm-6">
                    <label for="exampleInputEmail1">Customer Details</label>
                    <div id="invoice"></div>     
                    </div>
                    <div class="col-sm-6">
                    <label for="exampleInputEmail1">Client Details</label>
                    <div id="order"></div>     
                    </div>
                </div> -->

                {!! Form::open(array( 'class' => 'form-horizontal','url' => 'syncPayment', 'method' => 'post')) !!}
                <input type="hidden"  name="_token" value="{{ csrf_token() }}">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Customer Payment</label>
                                <input type="checkbox" id="customer_payment" name="customer_payment" value="1" checked>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Customer Name</label>
                                <input id="customer_name" type="text" name="cus_name" value="" placeholder="Customer Name" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Amount</label>
                                <input id="amount" type="text" name="amount" value="" placeholder="Amount" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Payment Type</label>
                                {!!   Form::select('payment_source_id', $payment_sources, Request::input('payment_sources_id'), array('class' => 'form-control' ))  !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Account Type</label>
                                {!!   Form::select('account_type_id', $account_sources, Request::input('account_type_id'), array('class' => 'form-control' ))  !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Select Statement Date</label>
                                <div class='input-group date' id='datetimepicker3'>
                                    <input type='text' id="receive_date" name="statement_date" class="form-control" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Payment Message</label>
                                {!! Form::textarea('note', Request::input('note') , ['class'=>'form-control','id'=>'note', 'rows' => 5, 'cols' => 10] ) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="client_section">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Client Payment</label>
                                <input type="checkbox" id="client_payment" name="client_payment" value="1" checked>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Client Name</label>
                                <input id="client_name" type="text" name="cli_name" value="" placeholder="Client Name" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Amount</label>
                                <input id="cli_amount" type="text" name="cli_amount" value="" placeholder="Amount" class="form-control">

                            </div>
                        </div>
                        <input type="hidden" name="customer_id" id="customer_id">
                        <input type="hidden" name="invoice_id" id="invoice_id">
                        <input type="hidden" name="client_id" id="client_id">
                        <input type="hidden" name="order_id" id="order_id">
                        <div class="form-group">
                            <div class="col-sm-12 ">
                                <label for="exampleInputEmail1">Payment Type</label>
                                <select id='cli_payment_source_id' name='cli_payment_source_id' class="form-control"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Account Type</label>
                                <select id='cli_account_type_id' name='cli_account_type_id' class="form-control"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Select Statement Date</label>
                                <div class='input-group date' id='datetimepicker4'>
                                    <input type='text' id="receive_date" name="cli_statement_date" class="form-control" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="exampleInputEmail1">Payment Message</label>
                                {!! Form::textarea('cli_note', Request::input('cli_note') , ['class'=>'form-control','id'=>'cli_note', 'rows' => 5, 'cols' => 10] ) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <input type="submit" id="submit_btn" name="submit" class="btn btn-primary pull-right" value="Add">
                </div>
                {!! Form::close() !!} 
            </div>


        </div>
        <!-- </div> -->

    </div>

</section>

<script>
    $(document).ready(function () {

        $("#number").keyup(function () {
            var a = $('#number').val()
            if (a.length < 6) {
                document.getElementById("errorMsg").style.display = "block";
            } else {
                document.getElementById("errorMsg").style.display = "none";
            }
        });

        $("#submit").click(function () {

            var val = $('#number').val();
            var type = $('#id_type').val();
            var url = "<?php echo url('getDetails'); ?>"
            //  + '/' + item_id + '/' + status;
            if (val.length < 6) {
                alert('Please enter minimum 6 digits');
                return false;
            }

            var check = (val).toString().substr(0, 2);
            console.log(check);
            if (check == '11' || check == '22') {
                $.ajax({
                    url: url,
                    type: 'get',
                    data: {val: val, type: type},
                    success: function (response) {
                        if (response != 0) {
                            if (response.single != 1) {
                                var invoice = response.invoice
                                var order = response.order
                                $("#customer_name").val(invoice.customer_name + ' ('+invoice.customer_balance+')');
                                $("#client_name").val(order.client_name+ ' ('+order.client_balance+')');
                                $('#customer_id').val(invoice.customer_id)
                                $('#client_id').val(order.client_id)
                                $('#invoice_id').val(invoice.id)
                                $('#order_id').val(order.id)
                                var payment_sources = response.cli_payment_sources
                                payment_sources.forEach(function (object) {
                                    var option = "<option value='" + object.id + "'>" + object.title + "</option>";
                                    $("#cli_payment_source_id").append(option);
                                });
                                var account_sources = response.cli_account_sources
                                account_sources.forEach(function (object) {
                                    var option = "<option value='" + object.id + "'>" + object.title + "</option>";
                                    $("#cli_account_type_id").append(option);
                                });
                                document.getElementById("client_payment").checked = true;
                                document.getElementById("customer_payment").checked = true;

                                $('#body').css('display', 'block')
                                $('#client_section').css('display', 'block')
                            } else {
                                var invoice = response.invoice
                                $("#customer_name").val(invoice.customer_name + ' ('+invoice.customer_balance+')');
                                $('#customer_id').val(invoice.customer_id)
                                $("#client_name").val('');
                                $('#client_id').val('')
                                $('#invoice_id').val(invoice.id)
                                $('#order_id').val('')
                                $('#body').css('display', 'block')
                                $('#client_section').css('display', 'none')
                                document.getElementById("client_payment").checked = false;
                            }

                        } else {
                            $('#body').css('display', 'none')
                            $('#client_section').css('display', 'none')
                            alert('No record found');
                        }
                    },
                    error: function (xhr, status, response) {
                    }
                });

            } else {
                $('#body').css('display', 'none')
                $('#client_section').css('display', 'none')
                alert('First 2 digits should be started with 11 or 22');
                return false;
            }



        });

        $("#amount").on("change paste keyup", function () {
            $('#cli_amount').val($(this).val());
        });

        $("#note").on("change paste keyup", function () {
            $('#cli_note').val($(this).val());
        });

    });

    function isNumber(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>

<script>
    $(function () {
        $('.select2').select2();
    });

 $(function () {
        $('#datetimepicker3').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });

    $(function () {
        $('#datetimepicker4').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });

    $(document).ready(function () {

        $('#datetimepicker4').data("DateTimePicker").date(moment(new Date()).format('MM/DD/YYYY'));
         $('#datetimepicker3').data("DateTimePicker").date(moment(new Date()).format('MM/DD/YYYY'));

        $('.form-horizontal').submit(function () {
            $('#submit_btn').prop('disabled', true);
        });


    });


   

</script>
@endsection



