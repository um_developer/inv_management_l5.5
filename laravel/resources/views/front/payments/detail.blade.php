@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <b>  {{ $model->name }} </b>
                <small class="pull-right">Created Date: <?php echo date("d M Y", strtotime($model->created_at)); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->

    <div class="box-body form-horizontal">

        <div class="form-group">
            <div class="col-sm-6">
                <label for="exampleInputEmail1">Phone</label>
                {!! Form::text('phone', $model->phone , array('class' => 'form-control','readonly') ) !!}
            </div>
            <div class="col-sm-6">
                <label for="exampleInputEmail1">Email</label>
                {!! Form::text('email', $model->email , array('class' => 'form-control','readonly') ) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-6">
                <label for="exampleInputEmail1">Address Line 1</label>
                {!! Form::text('address1', $model->address1 , array('class' => 'form-control' ,'readonly') ) !!}
            </div>

            <div class="col-sm-6">
                <label for="exampleInputEmail1">Address Line 2</label>
                {!! Form::text('address2', $model->address2 , array('class' => 'form-control','readonly') ) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <label for="exampleInputEmail1">City</label>
                {!! Form::text('city', $model->city , array('class' => 'form-control','readonly') ) !!}
            </div>

            <div class="col-sm-4">
                <label for="exampleInputEmail1">State</label>


                {!! Form::text('state', $model->state , array('class' => 'form-control','readonly') ) !!}
            </div>

            <div class="col-sm-4">
                <label for="exampleInputEmail1">Zip Code</label>
                {!! Form::text('zip_code', $model->zip_code , array('class' => 'form-control','readonly') ) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <label for="exampleInputEmail1">Note</label>
                {!! Form::textarea('note', $model->note , ['class'=>'form-control','readonly', 'rows' => 5, 'cols' => 10] ) !!}

            </div>
        </div>

        <div class="row no-print">
            <div class="col-xs-12">
                <a href="{{ url('vendor/edit/'.$model->u_id) }}" class="btn btn-default"><i class="fa fa-edit"></i> Edit Vendor</a>

            </div>
        </div>

    </div>

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <b> Purchase Orders</b>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>PO ID</th>
                        <th>Total Cost</th>
                        <th>Total Quantity</th>
                        <th>Delivered Quantity</th>
                        <th>Created Date</th>
                         <th>Order Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @if(count($purchase_orders) >0)
                    @foreach($purchase_orders as $item)
                    <tr>
                        <td><a href="{{ url('purchase-order/'.$item->id) }}">{{$item->id}}</a></td>

                        <td>{{ $item->total_cost }}</td>
                        <td>{{ $item->total_quantity }}</td>
                        <td>{{ $item->delivered_quantity }}</td>

                        <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                         @if($item->status == 'open')
                        <td><a class="btn btn-primary btn-xs"> Open</a></td>
                        @elseif($item->status == 'draft')
                        <td><a class="btn btn-success btn-xs"> Draft</a></td>
                        @else
                        <td><a class="btn btn-warning btn-xs"> Closed</a></td>
                        @endif
                        <td>
                            <a href="{{ url('purchase-order/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                            @if($item->status == 'draft')
                            <a href="{{ url('purchase-order/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <a href="{{ url('purchase-order/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            @endif


                        </td>
                    </tr>
                    @endforeach

                    @endif


                </tbody>
            </table>
        </div>
        
         <h2 class="page-header">
               
                <small class="pull-left">Total Amount:  {{ $ro_payment }}</small>
            </h2>
        <!-- /.col -->
    </div>

    
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <b> Balance </b>
                <small class="pull-right">Created Date: <?php echo date("d M Y", strtotime($model->created_at)); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
</section>

@endsection