@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

@section('content')
<?php
$required = 'required';
if (!isset($address))
    $address = '';
$base_url = url('credit-memo');
if (Auth::user()->role_id == 4)
    $base_url = url('customer/credit-memo');
?>
<section id="form">
    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Credit Memo</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(Auth::user()->role_id == 4)
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/credit-memo/create','id' => 'invoice_form', 'method' => 'post')) !!}
        @else
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'credit-memo/create','id' => 'invoice_form', 'method' => 'post')) !!}
        @endif
        <div class="box-body">

            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
            <div class="form-group">
                @if($customer_id == '0')
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $customers, Request::input('customer_id'), array('class' => 'form-control select2 select_customer','id' => 'customer_id',$required ))  !!}
                </div>
                @else
                <input type="hidden"  name="customer_id" value="{{ $customer_id }}">
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $customers, $customer_id, array('class' => 'form-control  select2 select_customer','id' => 'customer_id',$required ))  !!}
                </div>
                @endif


                @include('front/common/package_credit_memo')    
                @if(isset($show_bundle) && $show_bundle==1)
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select2 select_bundle ', 'disabled' => true,'id' => 'bundle_id' ))  !!}

                </div>
                @endif
            </div>

            @else
            
           
            <input type="hidden" id="customer_id" name="customer_id" value="00">
            <input type="hidden" id="customer_id_2"  name="customer_id_2" value="{{ $customer_id }}">
            @endif

           
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', Request::input('memo') , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>
            
            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
            @else 
            @if(isset($show_bundle) && $show_bundle==1)
            <div class="col-sm-6">
                <label for="exampleInputEmail1">Select Bundle #</label>
                {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select2 select_bundle ', 'id' => 'bundle_id' ))  !!}

            </div> 
            @endif
            @endif
        </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>
                  
                @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Price Template Name</label>
                    <span class="form-control">{{ $template_name }}</span>
                </div>
                @endif
            </div>
            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Address</label>
                    <span class="form-control">{{ $address }}</span>
                </div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Phone #</label>
                    <span class="form-control">{{ $phone }}</span>
                </div>
            </div>
            @endif
            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>

            @include('front/common/credit_memo_js')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody></tbody>
                    <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                            <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
        </div>
        {!! Form::close() !!} 
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Create Memo">
        </div>
    </div>
    
    @include('front/common/package_view_modal')
    @include('front/common/bundle_view_modal')
@if(isset($customer_id) && !empty($customer_id))
<script>
       checkcustomer();
                    function checkcustomer(){
                    
                        var customer_id = $("#customer_id option:selected").val();
                    
            if(customer_id==0){
            //add disabled
            $("#bundle_id").attr('disabled', 'disabled');
            }else{
            $("#bundle_id").removeAttr("disabled");
            }
                    }
    $('.select_bundle').on('change', function () {
                        var data = $(".select_bundle option:selected").val();
                        var customer_id = {{$customer_id}}
                        var type="invoice";
                      loadbundleAjax(data, j,customer_id,type);
                    });
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }
    </script>
@else  
    <script>

       $(document).ready(function () {
//            $('#datetimepicker4').data("DateTimePicker").date(moment(new Date()).format('MM/DD/YYYY'));
            checkcustomer();
                    function checkcustomer(){
                    
                        var customer_id = $("#customer_id option:selected").val();
                    
            if(customer_id==0){
            //add disabled
            $("#bundle_id").attr('disabled', 'disabled');
            }else{
            $("#bundle_id").removeAttr("disabled");
            }
                    }
            $('.select2').on('change', function () {
                var data = $(".select2 option:selected").text();
                loadPackageAjax(data, j);
            })
            var customer_select = document.getElementById('customer_id');

            $('.select_customer').on('change', function () {
                $('#submit_btn').prop('disabled', true);
                var url = "{{ url('/credit-memo/create/') }}"
                window.location = url + '/' + customer_select.value;
            });

        });
 
        $('.select_bundle').on('change', function () {
                        var data = $(".select_bundle option:selected").val();
                        var customer_id = $("#customer_id option:selected").val();
                        var type="invoice";
                       loadbundleAjax(data, j,customer_id,type);
                    });
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }
    </script>
 @endif   
 <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
        $(document).ready(function () {
//            $('#customer_id').select2({
//              language: {
//                noResults: function (params) {
//                  alert("Customer not found. Please create this customer.");
//                }
//              }
//            });
//            $('#package_id').select2({
//              language: {
//                noResults: function (params) {
//                  alert("This package is not existing please add this this package");
//                }
//              }
//            });
        });
    
    </script>
</section>
@include('front/common/item_detail_modal')
@endsection