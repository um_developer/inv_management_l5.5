@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

@section('content')
<?php
$required = 'required';
if (!isset($address))
    $address = '';
?>
<section id="form">

    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
             @if($model->status == 'approved')
         <h3 class="box-title">Modify Credit Memo # {{ $model->id }}</h3>
        @else
          <h3 class="box-title">Edit Credit Memo # {{ $model->id }}</h3>
        @endif
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(Auth::user()->role_id == 4)
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/credit-memo/update','id' => 'invoice_form', 'method' => 'post')) !!}
        @else
         @if($model->status == 'approved')
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'credit-memo/modified','id' => 'invoice_form', 'method' => 'post')) !!}
        @else
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'credit-memo/update','id' => 'invoice_form', 'method' => 'post')) !!}
        @endif
        @endif
        <input type="hidden"  name="id" value="{{ $model->id }}">
        <div class="box-body">

            @if(Auth::user()->role_id == 2  || Auth::user()->role_id == 5)
            <div class="form-group">
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $customers, $model->customer_id, array('class' => 'form-control','id' => 'customer_id',$required, 'disabled' => true ))  !!}
                </div>

                @include('front/common/package_credit_memo')
                @if(isset($show_bundle) && $show_bundle==1)
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select2 select_bundle ','id' => 'bundle_id' ))  !!}

                </div>
               @endif 
            </div>
            @endif
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', $model->memo , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>
                @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
                @else 
                @if(isset($show_bundle) && $show_bundle==1)
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select2 select_bundle ', 'id' => 'bundle_id' ))  !!}
    
                </div> 
                @endif
                @endif
            </div>


            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>
                  
                @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Price Template Name</label>
                    <span class="form-control">{{ $template_name }}</span>
                </div>
                @endif
            </div>

            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Address</label>
                    <span class="form-control">{{ $address }}</span>
                </div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Phone #</label>
                    <span class="form-control">{{ $phone }}</span>
                </div>
            </div>
            @else
            <input type="hidden" id="customer_id_2"  name="customer_id_2" value="{{ $customer_id }}">
            @endif

            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>
            
            @include('front/common/credit_memo_edit_js')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        ?>
                        @if(count($credit_memo_items)>0 && isset($credit_memo_items[0]))
                        @foreach($credit_memo_items as $items_1)
                        <?php
//                        print_r($items_1); die;
                        $delivered_items = 0;//$items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
//                        $validation_num = '';
                        ?>
                          <?php 
                          if($items_1->bundle_name != ''){
                               $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                              $items_1->item_name = $items_1->item_name . $bundle_detail;
                          }
                          ?>
                        @if(1==1)
                        <tr id="item_{{ $i }}">
                            <td><input class="form-control" title= "{{$items_1->item_name}}" type="text" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
                                <input type="hidden" readonly="readonly" name="po_item_id_{{ $i }}" id="po_item_id_{{ $i }}"  value="{{$items_1->id}}">
                                <input type="hidden" name="num[]" id="num[]"></td>
                    <input type="hidden" name="package_id" id="package_id" value="{{ $items_1->package_id }}">
                    <input type="hidden" name="bundle_id" id="bundle_id" value="{{ $items_1->bundle_id }}">
                    <input type="hidden" id="item_package_id_{{ $i }}" name="item_package_id_{{ $i }}" value="{{ $items_1->package_id }}"></td>

                    @if($items_1->start_serial_number != '0')
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" onkeyup="updateQuantityStatus('{{ $i }}')" onchange="updateQuantityStatus('{{ $i }}')" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"readonly="readonly"  onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @else
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                    @if($items_1->package_id == 0)
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}" ></td>
                    @else
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" readonly="readonly" onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"  readonly="readonly"></td>
                    @endif
                    @endif

                    @if(Auth::user()->role->role == 'client')
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="calculatePrice('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}"   ></td>
                    @else
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="calculatePrice('{{ $i }}')" type="number"  readonly="readonly" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}"  readonly="readonly"></td>
                    @endif
                    <td><input class="form-control" min ="0" step="any" type="number" style="width: 150px;"  onkeyup="hideSuggestionPopup()" name="total_{{ $i }}" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>

                    @if($items_1->package_id == 0 && $items_1->bundle_id=="")
                  
                  <td><input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete" onclick="deleteRow(this,{{ $i }})"></td>
                  
                    @elseif($items_1->bundle_id!="")
                    <td><input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Bundle# {{ $items_1->bundle_id }}" onclick="deletebundleRow({{ $items_1->bundle_id }})"></td> 
                    @elseif($items_1->package_id != 0)
                    <td><input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Package# {{ $items_1->package_id }}" onclick="deletePackageRowEdit({{ $items_1->package_id }})"></td>
                  
                    @endif
                    </tr>


                    <?php $i++; ?>
                    @endif
                    @endforeach

                    @endif

                    </tbody>
                    <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                            <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
            <div class="row" hidden="true">

                <div class="col-xs-8"></div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Total Amount for Purchase Order</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Total:</th>
                                <td id="po_total">$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            
            @include('front/common/package_view_modal')
            @include('front/common/bundle_view_modal')
        </div>
        {!! Form::close() !!} 
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Update Memo">
            @if(Auth::user()->role_id == 2)
            <a href="{{ url('credit-memo/'.$model->id) }}" class="btn btn-success btn-sm">Mark as Approve</i></a></b>
            @endif
        </div>
        <!-- /.box-footer -->

    </div>
    <script>

$('.select_bundle').on('change', function () {
                        var data = $(".select_bundle option:selected").val();
                        var customer_id = "{{ $model->customer_id }}";
                            if (customer_id != '' && customer_id != '0'){
                                var type="invoice"; 
                    loadbundleAjax(data, j, customer_id,type);
                    }
                    });
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }

        function deletebundleRow(r) {

$("#item_body").find('input[name="bundle_id"]').each(function () {
    if ($(this).val() == r) {
        var id = $(this).closest("tr")[0].id;
        var row_id = id.substr(id.length - 1); // => "1"
        $("#quantity_" + row_id).val(0);
        $(this).closest("tr")[0].style.display = 'none';
    }
});
        }
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
        $(document).ready(function () {
//            $('#package_id').select2({
//              language: {
//                noResults: function (params) {
//                  alert("This package is not existing please add this this package");
//                }
//              }
//            });
        });
    </script>
</section>
@include('front/common/item_detail_modal')
@endsection