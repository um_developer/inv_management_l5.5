@extends('customer_print_template')
@section('content')
<section class="invoice">

    <div class="row">
        <div class="col-xs-12">
            <h4> Broadmax Distributor INC</h4>
            7 Odell Plaza , Ste 140<br>
            Yonkers, N.Y 10701<br>
            Phone: 877-333-9220 

        </div>
        <!-- /.col -->
    </div>

    <!-- info row -->
    <div class="row invoice-info">
        
        <div class="col-sm-6 invoice-col">
            <br>
            <strong>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Bill To</strong><br>
            <address>
                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp {{ $model->customer_name }}<br>
                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp {{ $model->address1 }} {{ $model->address2 }}<br>
                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp {{ $model->phone }}<br>
                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp {{ $model->city }}, {{ $model->state }} {{ $model->zip_code }}<br>
            </address>
        </div>
        <!-- /.col -->
        
        <div class="col-sm-2 invoice-col">   
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <br><br><br>
            <?php
            echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($model->id, "C39") . '" alt="barcode" />';
            ?><br>
            <strong><span style="font-size: 20px" >Credit Memo #  {{ $model->id }}</span></strong><br>
            Statement Date: <?php echo date("d M Y", strtotime($model->statement_date)); ?>
            <br>
            <br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        @if($model->show_image == '1')
                        <th>Image</th>
                        @endif
                        <th>Name</th>
                        @if($model->show_serial == '1')
                        <th>Start Serial #</th>
                        <th>End Serial #</th>
                        @endif
                        <th>QTY</th>
                        @if($model->show_package_id == '1')
                        <th>Package ID</th>
                        @endif
                        @if($model->show_unit_price == '1')
                        <th>Unit Price</th>
                        @endif
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($po_items)>0)
                    @foreach($po_items as $items_1)
                    <?php 
                          if($items_1->bundle_name != ''){
                               $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                              $items_1->item_name = $items_1->item_name . $bundle_detail;
                          }
                          ?>
                    <tr>
                        @if($model->show_image == '1')
                        @if($items_1->image == '')
                        <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                        @else
                        <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"</a></td>
                        @endif
                        @endif
                        <td>{{ $items_1->item_name }}</td>
                        @if($model->show_serial == '1')
                        <td>{{ $items_1->start_serial_number }}</td>
                        <td>{{ $items_1->end_serial_number }}</td>
                        @endif
                        <td>{{ $items_1->quantity }}</td>
                        @if($model->show_package_id == '1')
                        <td>{{ $items_1->package_id }}</td>
                        @endif
                        @if($model->show_unit_price == '1')
                        <td>{{ $items_1->item_unit_price }}</td>
                        @endif
                        <td>{{ $items_1->total_price }}</td>
                    </tr>

                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!--<p class="lead">Amount Due 2/22/2014</p>-->

            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                            <th style="width:12%"></th>
                            <th style="width:25%">Total Quantity:</th>
                            <td style="width:28%">{{ $model->total_quantity }}</td>
                            <th style="width:25%">Total Price:</th>
                            <td style="width:15%;"><h4><b>${{ $model->total_price }}</b></h4></td>

                        </tr>


                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>

    <div class="row">
        <div class="col-xs-12">
            <b> {{ $setting->bottom_text_1 }}</b>
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                 {{ $setting->bottom_text_2 }}
            </p>
        </div>
    </div>
</section>
@endsection