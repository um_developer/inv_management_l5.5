@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')
@section('content')

<?php
$base_url = url('credit-memo');
if (Auth::user()->role_id == 4)
    $base_url = url('customer/credit-memo');
?>
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>

                    @if(Auth::user()->role_id != 4)
                    {!! Form::open(array( 'class' => '','url' => 'credit-memo/search', 'method' => 'get')) !!}
                    @else
                    {!! Form::open(array( 'class' => '','url' => 'customer/credit-memo/search', 'method' => 'get')) !!}
                    @endif
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-3">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div></div>
                            <?php if (Auth::user()->role_id != 4) { ?>
                            <div class="form-group col-sm-4">

                                {!!  Form::select('customer_id', $customers, Request::input('customer_id'), array('class' => 'form-control','id' => 'customer_id' ))  !!}
                            </div>
                            <?php } ?>                                    <div class="form-group col-sm-4" id="type">

                            <select id="type" name="type" class="form-control">
                                <option value="" <?php echo ($selected_report_type == '') ? 'selected' : ''; ?>>Select Status</option>        
                                <option value="pending" <?php echo ($selected_report_type == 'pending') ? 'selected' : ''; ?>>Pending</option>
                                <option value="approved" <?php echo ($selected_report_type == 'approved') ? 'selected' : ''; ?>>Approved</option>
                            </select>
                            </div>
                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>


                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <?php if (Auth::user()->role_id == 4) { ?>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('customer/credit-memo') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                        <?php }else{ ?>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('credit-memo') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                        <?php } ?>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Credit Memo</h3>
                <a href="{{ $base_url . '/create' }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Credit Memo</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Credit Memo ID</th>
                                    @if(Auth::user()->role_id == 2)
                                    <th>Customer Name</th>
                                    @endif
                                    <th>Memo</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <!--<th>Created Date</th>-->
                                    <th>Statement Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($model) >0)
                                @foreach($model as $item)
                                <tr>
                                    <td><a href="{{ $base_url .'/'.$item->id }}">{{$item->id}}</a></td>
                                    @if(Auth::user()->role_id == 2)
                                    <td><a href="{{ url('customer/edit/'.$item->customer_u_id) }}">{{$item->customer_name}}</a></td>
                                    @endif
                                    <td>{{$item->memo}}</td>
                                    <td>{{$item->total_price}}</td>
                                    @if($item->status == 'pending')
                                    <td><a class="label label-warning btn-xs"> Pending</a></td>
                                    @elseif($item->status == 'approved')
                                    <td><a class="label label-success btn-xs"> Approved</a></td>
                                    @else
                                    <td><a class="label label-danger btn-xs"> Rejected</a></td>
                                    @endif
                                    
                                    <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                                    <td>
                                        <a href="{{ $base_url .'/'.$item->id }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                         @if(Auth::user()->role_id == 2 || Auth::user()->role_id != 2 && $item->status == 'pending')
                                        <a href="{{ $base_url .'/edit/'.$item->id }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                         @endif
                                        @if($item->status == 'pending')
                                        <a href="{{ $base_url .'/delete/'.$item->id }}" onclick="return confirm('Are you sure you want to delete this Credit Memo?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                        <!--<a href="{{ $base_url .'/delete/'.$item->id }}"  onclick="return confirm('Are you sure you want to delete this Credit Memo.')" class="btn btn-danger"><i class="fa fa-trash"></i></a>-->
                                        @endif


                                    </td>
                                </tr>
                                @endforeach

                                @endif


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>

                    </div></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>

    function change_status(item_id, status) {

//        alert(item_id);
//        alert(status);


    var url = "<?php echo url('customer/invoice/update-status'); ?>" + '/' + item_id + '/' + status;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            $("select[name='po_id'").html('');
            $("select[name='po_id'").html(response);
            var $table = $('#item_body');
            var $table = $('#item_body');
            $table.find('tbody').empty();
            },
            error: function (xhr, status, response) {
            }
    });
    }

    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
    });
    });

        $(document).ready(function () {
            $('#customer_id').select2({
              language: {
                noResults: function (params) {
                  alert("Customer not found. Please create this customer");
                }
              }
            });
        });
</script>
@endsection



