@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')

<?php
$approve_status = '';
if (session('skip'))
    $approve_status = 'skip';

$modal_title = 'Loss';
?>
@section('content')
<section class="invoice">
    <!-- /.box-header -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    @if (session('bulk_error'))
    @foreach(session('bulk_error') as $item)
    <div class="alert alert-danger">
        {{ $item }}
    </div>
    @endforeach
    @endif
    @if (count($errors->form) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->form->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @include('front/common/approve_credit_memo_modal')
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-hdd-o"></i> Credit Memo # {{ $model->id }}
                <small class="pull-right">Statement Date: <?php echo date("d M Y", strtotime($model->statement_date)); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <b>Memo # {{ $model->id }}</b><br>
            <br>
            <b>Customer Name:</b> {{ $model->customer_name }}<br>
            <!--          <b>Account:</b> 968-34567-->
            <br>
            <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <!--          To
                      <address>
                        <strong>John Doe</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone: (555) 539-1037<br>
                        Email: john.doe@example.com
                      </address>-->
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

            <br>
            @if($model->status == 'rejected')
            Memo Status: <b class = "label label-danger btn-xs">{{ ucwords($model->status) }}</b><br>
            @elseif($model->status == 'approved')
            Memo Status: <b class = "label label-success btn-xs">{{ ucwords($model->status) }}</b><br>
            @else
            Memo Status: <b class = "label label-warning btn-xs"> {{ ucwords($model->status) }}</b><br>
            @endif
            @if($model->status == 'pending')
            <br>
            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
            <b> <a href="{{ url('credit-memo/edit/'.$model->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit ml-20"> Edit</i></a>
                @if(Auth::user()->role_id != 5)
                <a href="{{ url('credit-memo/status/'.$model->id.'/approved/complete') }}" onclick="return confirm('Are you sure you want to approve this Credit Memo.')" class="btn btn-success btn-sm">Mark as Approve</i></a>
                @endif
                @endif
                @endif

                @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 5)
                <a href="{{url('credit-memo-print/'.$model->id)}}" class="btn btn-success btn_print btn-sm"><i class="fa fa-print"></i> Print</a>

                @if($show_profit == 0)
                <a href="{{ url('credit-memo/'.$model->id.'/1') }}" class="btn btn-primary btn-sm">Show Details</i></a>
                @else
                <a href="{{ url('credit-memo/'.$model->id) }}" class="btn btn-danger btn-sm">Hide Details</i></a>
                @endif
                @else
                <a href="{{url('customer/credit-memo-print/'.$model->id)}}" class="btn btn-success btn_print btn-sm"><i class="fa fa-print"></i> Print</a>
                @endif

                <script src="{{ asset('front/js/jquery.printPage.js')}}"></script>
                <script>
                    $(document).ready(function () {
                        $(".btn_print").printPage();
                    });
                </script>

                <br>
                <br>
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Start Serial #</th>
                                    <th>End Serial #</th>
                                    <th>QTY</th>
                                    <th>Package ID</th>
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                    @if($show_profit == 1)
                                    <th>Total Cost</th>
                                    <th>Loss</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($po_items)>0)
                                @foreach($po_items as $items_1)
                                <?php 
                                if($items_1->bundle_name != ''){
                                     $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                                     $items_1->item_name = $items_1->item_name . $bundle_detail;
                                }
                                ?>
                                <tr>
                                    <td>{{ $items_1->item_name }}</td>
                                    <td>{{ $items_1->start_serial_number }}</td>
                                    <td>{{ $items_1->end_serial_number }}</td>
                                    <td>{{ $items_1->quantity }}</td>
                                    <td>{{ $items_1->package_id }}</td>
                                    <td>{{ $items_1->item_unit_price }}</td>
                                    <td>{{ $items_1->total_price }}</td>
                                    @if($show_profit == 1)
                                    <td>{{ $items_1->total_cost }}</td>
                                    <td>{{ $items_1->loss }}</td>
                                    @endif
                                </tr>

                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-6">
            <!--          <p class="lead">Payment Methods:</p>
                    
            
                      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                      </p>-->
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-6">
                      <!--<p class="lead">Amount Due 2/22/2014</p>-->

                        <div class="table-responsive">
                            <table class="table">
                                <tbody><tr>
                                        <th style="width:50%">Total Quantity:</th>
                                        <td>{{ $model->total_quantity }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Price</th>
                                        <td>{{ $model->total_price }}</td>
                                    </tr>

                                    @if($show_profit == 1)
                                    <tr>
                                        <th>Total Cost</th>
                                        <td>{{ $model->total_cost }}</td>
                                    </tr>

                                    <tr>
                                        <th>Loss</th>
                                        <td>{{ $model->loss }}</td>
                                    </tr>

                                    @endif
                                </tbody></table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                    <div class="col-xs-12">
                        @if($model->status == 'draft')
                        <a href="{{ url('purchase-order/edit/'.$model->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> Edit Purchase Order</a>
                        @endif
                    </div>
                </div>
                </section>

                <script>

                    $(document).ready(function () {
                        var approve_status = "{{ $approve_status }}";
                        if (approve_status == 'skip') {
                            var modal = document.getElementById('myModal');
                            modal.style.display = "block";
                        }

                    });

                    function approve() {

                        var item_id = "{{ $model->id }}";
                        alert(item_id);
                        $.ajax({
                            url: "<?php echo url('credit-memo/status/'); ?>" + '/' + item_id + '/approved/complete',
                            type: 'get',
                            dataType: 'html',
                            success: function (response) {
                                console.log(response);
                                if (response == 1) {
                                    var modal = document.getElementById('myModal');
                                    modal.style.display = "block";
                                    //                    alert(response);

                                } else if (response == 0) {

                                }
                            },
                            error: function (xhr, status, response) {
                                alert(response);
                            }
                        });



                    }



                    function showProfitModal() {

                        var modal = document.getElementById('modal_profit_new');
                        modal.style.display = "block";
                    }

                </script>

                @endsection