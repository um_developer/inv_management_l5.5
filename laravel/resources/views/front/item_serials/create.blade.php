@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Generate Item Serial</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'item-serial/create', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-6">

                    <label for="exampleInputEmail1">Select Item</label>
                    {!!   Form::select('item_id', $items, Request::input('item_id'), array('class' => 'form-control' ))  !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Start Serial #</label>
                    {!!  Form::input('number', 'start_serial', 1, $options = array('min' => '1', 'class' => 'form-control','id' => 'start_serial',$required)) !!}

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">End Serial #</label>
                    {!!  Form::input('number', 'end_serial', 1, $options = array('min' => '1', 'class' => 'form-control','id' => 'end_serial',$required)) !!}

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Quantity</label>
                    {!!  Form::input('number', 'quantity', 1, $options = array('min' => '1', 'class' => 'form-control','id' => 'quantity',$required)) !!}

                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Generate">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>


</section>
<script>
    function calculateQuantity() {

        var start_serial = ($("#start_serial").val());
        var end_serial = ($("#end_serial").val());
        var result = parseInt(end_serial) - parseInt(start_serial);
        $("#quantity").val(result + 1);
    }

    function calculateEndSerial() {

        var start_serial = ($("#start_serial").val());
        var quantity = ($("#quantity").val());
        var result = parseInt(start_serial) + parseInt(quantity);
        $("#end_serial").val(result - 1);
    }

    $("#end_serial").on('keyup change', function (e) {
        calculateQuantity();
    });

    $("#quantity").on('keyup change', function (e) {
        calculateEndSerial();
    });

</script>
@endsection