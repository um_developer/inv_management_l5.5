@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<?php
if (!isset($search_text))
    $search_text = '';

$required = 'required';
?>
<section id="form" class="mt30 mb30 col-sm-12 p0">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Filters</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                    </div>
                </div>

                {!! Form::open(array( 'class' => '','url' => 'item-serial/search', 'method' => 'post')) !!}
                <div class="box-body">
                    <div class="row">

                        <div class="form-group col-sm-2">
                            <label>Start Serail #</label>
                            {!! Form::text('start_serial', Request::input('start_serial') , array('placeholder'=>"Start Serial Number",'maxlength' => 20,'class' => 'form-control') ) !!}
                        </div>
                         <div class="form-group col-sm-2">
                            <label>End Serail #</label>
                            {!! Form::text('end_serial', Request::input('end_serial') , array('placeholder'=>"End Serial Number",'maxlength' => 20,'class' => 'form-control') ) !!}
                        </div>
                         <div class="form-group col-sm-2">
                              <label>Select Item</label>
                         {!!  Form::select('item_id', $items, Request::input('item_id'), array('class' => 'form-control','id' => 'item_id' ))  !!}
                         </div>
                        
                        <div class="form-group col-sm-2">
                             <label>Select Status</label>
                         {!!   Form::select('status', array('0' => 'Select Status','active' => 'Active','inactive' => 'Inactive'), Request::input('status'), array('class' => 'form-control',$required ))  !!}
                         </div>
                         
                         <div class="form-group col-sm-2">
                              <label></label>
                           <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                        </div>
                         <div class="form-group col-sm-2">
                            <label></label>
                             <a href="{{ URL::to('item-serial') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                        </div>


                        <input type="hidden" class="form-control" name="page" id="page" value="1">
                        <div class="clearfix"></div>
                       
                    </div>
                </div>
                {!! Form::close() !!} 
            </div>
        </div>
    </div>

    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">

            <div class="box-header">

                <h3 class="box-title">Item Serials</h3>
                <a href="{{ url('item-serial/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Generate Serial</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">

                    <thead>

                        <tr>
                            <th>Item</th>
                            <th>Serial#</th>
                            <th>Date</th>
                            <th>Status</th>
                             <th>Location</th>
<!--                            <th>Actions</th>-->

                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->item_name}}</td>
                            <td>{{$item->serial}}</td>
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            @if($item->status == 'inactive')
                            <td><a class="btn btn-warning btn-xs"> Inactive</a></td>
                            @elseif($item->status == 'active')
                            <td><a class="btn btn-success btn-xs"> Active</a></td>
                            @endif
                            @if($item->location == 'invoice')
                             <td><a href="{{ url('invoice/'. $item->location_id) }}">Invoice #{{ $item->location_id }}</i></a></td>
                            @elseif($item->location == 'credit_memo')
                            <td><a href="{{ url('credit-memo/'. $item->location_id) }}">Credit Memo #{{ $item->location_id }}</i></a></td>
                            @else
                             <td></td>
                            @endif
                            
                            @if(1==2)
                            <td>
                                @if($item->status == 'inactive')
                                <a href="{{ url('item-serial/active/'.$item->id) }}" class="btn btn-success">Active</i></a>
                                <a href="{{ url('item-serial/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                @endif


                            </td>
                            @endif
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
                <div style="float: right !important">

<?php echo $model->appends(Input::query())->render(); ?>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
//       "dom": '<"toolbar">frtip'
        });
        $("div.toolbar")
                .html('<div class = "ml-5"><button type="button" class = "btn btn-primary pull-right important" id="any_button">Click Me!</button> </div>');
    });



    $(document).ready(function () {
//    console.log( "ready!" );
//        $('.pagination li [rel=prev]').html('Prev');
//        $('.pagination li [rel=next]').html('Next');
    });



</script>


@endsection



