@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">

            <div class="box-header">

                <h3 class="box-title">Item Serials Queue</h3>
                <a href="{{ url('item-serial/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Generate Serial</a>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">

                    <thead>

                        <tr>
                            <th>Item</th>
                            <th>Start Serial#</th>
                            <th>End Serial#</th>
                            <th>Quantity</th>
                            <th>Date</th>
                            <th>Status</th>
                             <th>Message</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->item_name}}</td>
                            <td>{{$item->start_serial_number}}</td>
                            <td>{{$item->end_serial_number}}</td>
                            <td>{{$item->quantity}}</td>
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            @if($item->status == 'pending')
                            <td><a class="btn btn-warning btn-xs"> Pending</a></td>
                            @elseif($item->status == 'completed')
                            <td><a class="btn btn-success btn-xs"> Completed</a></td>
                            @endif
                            <td>{{$item->message}}</td>
                            <td>
                                <a href="{{ url('item-serial-queue/detail/'.$item->id) }}" class="btn btn-primary">Details</i></a>
                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
//       "dom": '<"toolbar">frtip'
        });
        $("div.toolbar")
                .html('<div class = "ml-5"><button type="button" class = "btn btn-primary pull-right important" id="any_button">Click Me!</button> </div>');
    });
</script>
@endsection



