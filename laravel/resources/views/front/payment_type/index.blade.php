@extends('customer_template')

@section('content')

<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Payment Type Listing</h3>
                <a href="{{ url('payment-type/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Payment Type</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Title</th>
                            <th>For Admin</th>
                            <th>For Customer</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($payment_types) >0)
                        <?php $i = 1; ?>
                        @foreach($payment_types as $item)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$item->title}}</td>
                            @if($item->for_admin == '1')
                            <td><b class = "label label-success btn-xs">ACTIVE</b></td>
                            @else
                             <td></td>
                            @endif
                            @if($item->for_customer == '1')
                            <td><b class = "label label-success btn-xs">ACTIVE</b></td>
                             @else
                             <td></td>
                            @endif
                            <td>
                                <a href="{{ url('payment-type/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('payment-type/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
    });
    });
</script>
@endsection



