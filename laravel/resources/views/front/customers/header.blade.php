<!-- Main Header -->
<header class="main-header">
<?php
       $customer = App\Customers::where('user_id',Auth::user()->id)->first();
       
      ?>
    <!-- Logo -->
    <a href="{{ url('/customer/dashboard') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">{{Config::get('params.site_name')}}</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Customer Panel</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        
		
        <!-- Navbar Right Menu -->
        @if(1==1)
        <div class="navbar-custom-menu">
              
        <ul class="nav navbar-nav">
	            <li><a href="{{ url('/customer/payments') }}"> Current Balance: <?php echo $customer->balance; ?></a></li>
        
                <!-- User Account Menu -->
                
                <!-- Control Sidebar Toggle Button -->

        </ul>

            <ul class="nav navbar-nav">
	<li><a href="{{ url('/') }}"><i class="ion-person"></i> {{ $customer->name }}</a></li>
        
         
  
                <!-- User Account Menu -->
                
                <!-- Control Sidebar Toggle Button -->

            </ul>
        </div>
        @endif
    </nav>
</header>
