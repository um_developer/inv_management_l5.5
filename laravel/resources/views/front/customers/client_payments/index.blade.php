@extends('customer_c_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        <div class="row">
            <div class="col-xs-12">
                       @include('front/common/client_common_tabs')
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    {!! Form::open(array( 'class' => '','url' => 'customer/client/payments/search', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-3">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div></div>

                            <div class="form-group col-sm-3">

                                {!!  Form::select('user_id', $clients, Request::input('user_id'), array('class' => 'form-control select2','id' => 'user_id' ))  !!}
                            </div>

                            <div class="form-group col-sm-2">

                                {!!  Form::select('payment_status_id', $payment_status, Request::input('payment_status_id'), array('class' => 'form-control','id' => 'payment_status_id' ))  !!}
                            </div>

                            <div class="form-group col-sm-2">

                                {!!  Form::select('payment_source_id', $payment_sources, Request::input('payment_source_id'), array('class' => 'form-control','id' => 'payment_source_id' ))  !!}
                            </div>

                            <div class="form-group col-sm-2">

                                {!!  Form::select('account_id', $accounts, Request::input('account_id'), array('class' => 'form-control','id' => 'account_id' ))  !!}
                            </div>

                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>


                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('customer/client/payments') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Client Payments Listing</h3>
                <a href="{{ url('customer/client/payment/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add new Payment</a>


            </div>


            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="vendors" class="table table-bordered table-striped">

                            <thead>

                                <tr>
                                    <th>Payment ID</th>
                                    <th>Client Name</th>
                                    <th>Payment Type</th>
                                    <th>Account</th>
                                    <th>Payment Message</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Statement_date</th>
                                    <th>Payment Reference</th>
                                    <th>Status</th>
                                    <th>Actions</th>

                                </tr>
                            </thead>
                            <tbody>

                                @if(count($model) >0)
                                <?php
                                $i = 1;
                                $total_amount = 0;
                                ?>
                                @foreach($model as $item)
                                <tr>
                                     <td>{{$item->initials . $item->id}}</td>
                                    <td>{{$item->user_name}}</td>
                                    <td>{{$item->payment_type_title}}</td>
                                    <td>{{$item->account_type_title}}</td>
                                    <td>{{$item->message}}</td>
                                    <td>{{$item->total_modified_amount}}</td>
                                    <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                                    <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                                     <td>{{$item->payment_reference}}</td>
                                    @if($item->status == 'pending')
                                    <td><a class="btn btn-primary btn-xs"> Pending</a></td>
                                    @elseif($item->status == 'approved')
                                    <td><a class="btn btn-success btn-xs"> Approved</a></td>
                                    @else
                                    <td><a class="btn btn-warning btn-xs"> Rejected</a></td>
                                    @endif
                                    <td>
                                        @if($item->modification_count == 0)
                                        <a href="{{ url('customer/client/payment/edit/'.$item->id) }}"  class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        @if($item->status == 'pending')
                                        <a href="{{ url('payment/status/'.$item->id.'/approved') }}" onclick="return confirm('Are you sure you want to approve this Payment?')" class="btn btn-success">Approve</i></a>
                                        @endif
                                        <a href="{{ url('customer/client/payment/delete/'.$item->id) }}"  onclick="return confirm('Are you sure you want to delete this Payment?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                        @else
                                PMT{{ $item->payment_type_id }}
                                @endif
                                    </td>
                                </tr>
                                <?php
                                $total_amount = $total_amount + $item->amount;
                                $i++;
                                ?>
                                @endforeach
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Total Amount </th>
                                    <th> ${{ $total_amount }}</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>

                                </tr>
                                @endif


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    @include('front/common/dataTable_js')
</section>

<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.reports_list_length')}},
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    text: 'Export To Excel',
                    title: 'Client Payments Report',
                    exportOptions: {
                    columns: [1, 2, 3, 4, 5,7,8]
                    }
            },
            {
            extend: 'pdfHtml5',
                    text: 'Export To PDF',
                    title: 'Client Payments Report',
                    exportOptions: {
                    columns: [1, 2, 3, 4, 5,7,8]
                    }
            }
            ]
//       "dom": '<"toolbar">frtip'
    });
    $("div.toolbar")
            .html('<div class = "ml-5"><button type="button" class = "btn btn-primary pull-right important" id="any_button">Click Me!</button> </div>');
    });
    
     $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });
</script>
@endsection



