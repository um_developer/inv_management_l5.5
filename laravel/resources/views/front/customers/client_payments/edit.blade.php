@extends('customer_c_template_2')

@section('content')
<?php
$required = 'required';
if (!isset($user_id))
    $user_id = '';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Edit Payment # {{ $model->id }}</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/client/payment/update', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <input type="hidden"  name="id" value="{{ $model->id }}">
        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Client</label>

                    @if($user_id != '')
                    {!!   Form::select('user_id', $clients, $user_id, array('class' => 'form-control','disabled' => true  ))  !!}
                    @else
                    {!!   Form::select('user_id', $clients, Request::input('user_id'), array('class' => 'form-control', 'disabled' => true  ))  !!}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Amount</label>
                    {!! Form::text('amount', $model->total_modified_amount , array('placeholder'=>"Amount ",'maxlength' => 10,'class' => 'form-control',$required) ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Payment Type</label>
                    {!!   Form::select('payment_source_id', $payment_sources, Request::input('payment_sources_id'), array('class' => 'form-control',$required ))  !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Accounts</label>
                    {!!   Form::select('account_type_id', $accounts, $model->account_type_id, array('class' => 'form-control',$required, 'disabled' => true ))  !!}
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>

            </div>



            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Payment Message</label>
                    {!! Form::textarea('note', $model->message , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" id="submit_btn" name="submit" class="btn btn-primary pull-right" value="Update">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>


</section>
<script>

    $(function () {
//            Bootstrap DateTimePicker v4
        $('#datetimepicker4').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });


    $(document).ready(function () {

        var my_date = "{{ $model->statement_date }}";
        $('#datetimepicker4').data("DateTimePicker").date(moment(my_date).format('MM/DD/YYYY'));

        $('.form-horizontal').submit(function () {
            $('#submit_btn').prop('disabled', true);
        });


    });
</script>
@endsection