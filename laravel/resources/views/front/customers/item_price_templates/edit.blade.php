@extends('customer_c_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Update Price Template</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/item-price-template/update', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
               
                  {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Title</label>
                    {!! Form::text('title', $model->title , array('placeholder'=>"Title ",'maxlength' => 200,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Enter X times Cost</label>
                    {!! Form::text('price_percentage', $model->price_percentage , array('placeholder'=>"Percentage ",'maxlength' => 200,'class' => 'form-control') ) !!}
                  </div>
                </div>

                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Description</label>
                     {!! Form::textarea('description', $model->description , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}
                  </div>
                </div>
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
		
</section>
@endsection