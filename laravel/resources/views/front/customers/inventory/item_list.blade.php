@extends('customer_c_template')

@section('content')
@include('front/common/image_modal')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">
<div id="alert_message" style="display: none" class="alert alert-success">
        </div>
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'customer/item-list-search', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">

                    <div class="form-group col-sm-3">
                        {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control ','id' => 'category_id' ))  !!}
                    </div>
                    <div class="form-group col-sm-3">
                        <select class ='form-control' name="pricestatus" >
                            <option value="" @if(isset($pricestatus) && empty($pricestatus)) selected  @endif>Select price status</option> 
                            <option value="increase" <?php if(isset($pricestatus) && $pricestatus=="increase"){ ?> selected  <?php }?>>Increase</option>
                            <option value="decrease"<?php if(isset($pricestatus) && $pricestatus=="decrease"){ ?> selected  <?php }?>>Decrease </option>
                            </select>
                    </div>   

                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('customer/items_list') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Items List</h3>
<!--                <a href="{{ URL::to('customer/inventory/generate/report') }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>
                <a href="{{ url('item/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Item</a>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="home-product-cata-nav">

                <ul class="nav nav-tabs" role="tablist">
                <?php 
                    if( isset($tags) && count($tags) > 0){ ?>
                    <?php foreach($tags as $row){ ?>

                        <?php if (count($row->child) == 0) {?>
                        <li> <a href="{{ url('customer/items-list-tag-new?tag='.$row->id) }}" class="active"><?php echo $row->title; ?></a> </li>
                        <?php }else{ ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $row->title; ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                            @foreach($row->child as $chil)
                                <li><a href="{{ url('customer/items-list-tag-new?subtag='.$chil->id) }}"><?php echo $chil->title; ?></a></li>
                            @endforeach
                            </ul>

                        </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                </ul>                            
            </div> 
                <div class="table-responsive">
                    <div class="col-md-12">

                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Item Image</th>
                            <th>Item ID</th>
                            <th>SKU/Item Code</th>
                            <th>Item Name</th>
                            <th>UPC/Barcode</th>
                            <th>Category Name</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                        @if(count($model) >0 && !empty($model))
                        @foreach($model as $item)
                       
                        <tr>
                            @if($item->image == '')
                            <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                            @else
                            <td><a href='#' onclick="showItemGallery('{{$item->image}}','{{$item->id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>
                            @endif
                            <td>{{$item->id}}</td>
                            <td>{{$item->code}}</td>
                        <td id="gettitle_{{$item->id}}">{{$item->name}}</td>
                            <td>{{$item->upc_barcode}}</td>
                            <td>{{$item->category_name}}</td>
                            <td>{{$item->cost}}</td>


                            <td> 
                                <a onclick="addToInvoice('{{ $item['id'] ."','".$item->cost }}')"  class="btn btn-primary"><i class="fa fa-plus"> Add to invoice</i></a>
                                {{-- |<a href="#"  data-toggle="modal" data-target="#viewstatus" onclick="loadstatusAjax({{ $item['id']}})">Item Config</i></a> --}}
                                <a href="#" data-toggle="modal" data-target="#viewstatus" onclick="loadstatusAjax({{ $item['id']}})" class="btn btn-primary"><i class="fa fa-cog"> Item Config</i></a>
                           
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

                </div></div>

            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>
 <!-- Modal -->
 <div class="table-responsive" style=" height: 400px;
    display: inline-block;
    overflow: auto; ">
 <div class="modal fade" id="viewstatus" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title sattitle"></h4>
        </div>
        <div class="modal-body" id="impactstatus">
            <table class="table table-bordered table-striped" id="item_body">
                <thead>
                    <tr>
        <!--              <th>S#.</th>-->
                        <th>Increase Impact</th>
                        <th>Decrease Impact</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot></tfoot>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@include('front/common/dataTable_js')
<script>
    function loadstatusAjax(itemid) {
        var getTitle=$('#gettitle_'+itemid).html()
         $('.sattitle').html(getTitle);
            $.ajax({
                url: "<?php echo url('customer/getItemStatus/'); ?>" + '/' + itemid,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('viewstatus');
                        modal.style.display = "block";
                        $('#item_body').find('tbody').empty();
                        $('#item_body').find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }
      $(function() {
    $('.toggle-two').bootstrapToggle({
      on: 'Active',
      off: 'Disabled'
    });
  })
    $(document).ready(function () {
    document.getElementById("alert_message").innerHTML = '';
    document.getElementById("alert_message").style.display = "none";
    });
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": false,
             dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export To Excel',
                title: 'my_inventory',
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Export To PDF',
                title: 'my_inventory',
               exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
            }
        ]
    });
    });
    function addToInvoice(item_id,price) {

    var url = "<?php echo url('customer/add-to-invoice/'); ?>" + '/' + item_id + '/1/' + price;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
           document.getElementById("alert_message").innerHTML = response;
           
                setTimeout(function(){
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
           
            },
            error: function (xhr, status, response) {
            }
    });
    }
    $('.payMethodjj').on( 'change', function() {
       var  item_id=  $(this).attr("#data-item");
      var  status=$(this).attr("#data-type");
      var  type= $(this).attr("#data-val");
      alert(item_id)
      alert(status)
      alert(type)
        });
      
    function changeItemStatus(elem) {
      
      
        var  item_id= $(elem).data("item");
       // var  status=  $(elem).data("val");
        var  type=  $(elem).data("type");
        if($(elem).prop("checked")==true)
        {
            status=1;
        }
        if($(elem).prop("checked")==false)
        {
            status=0;
        }
   $.ajax({
    url: "<?php echo url('customer/itemstatus/'); ?>" +"/"+status+"/"+item_id+"/"+type,
    type: 'get',
    dataType: 'html',
    success: function (response) {
        console.log(response);
        if (response == 1) {
//                    var modal = document.getElementById('myModal');
//                    modal.style.display = "block";
//                    alert(response);

        } else if (response == 0) {

        }
    },
    error: function (xhr, status, response) {
        alert(response);
    }
});
    }





</script>

@endsection



