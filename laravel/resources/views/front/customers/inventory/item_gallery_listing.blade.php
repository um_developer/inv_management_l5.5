@extends('customer_c_template')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="home-product-cata-nav">
            <ul class="nav nav-tabs" role="tablist">
                <?php if(count($tags) > 0){ ?>
                    <?php foreach($tags as $row){ ?>
                        <?php if (count($row->child) == 0) {?>
                            <li>
                                <a href="{{ url('customer/item-gallery-search-new?tag='.$row->id) }}" class="active myclass0"><?php echo $row->title; ?></a>
                            </li>
                        <?php }else{ ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $row->title; ?> <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                @foreach($row->child as $chil)
                                    <li>
                                        <a href="{{ url('customer/item-gallery-search-new?subtag='.$chil->id) }}" class="myclass0"><?php echo $chil->title; ?></a>
                                    </li>
                                @endforeach
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>                            
        </div> 
    </div> 

    <div class="col-xs-12 hidden">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Item</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'customer/item-gallery-search', 'method' => 'get')) !!}
            <div class="box-body">
                <div class="row">


                    <div class="form-group col-sm-4">
                        {!!   Form::select('selling_price_template', $templates, $template_id, array('class' => 'form-control t_id', 'id' => 'template_id' ))  !!}
                    </div>

                    <div class="form-group col-sm-4 hidden">
                        {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control select2','id' => 'category_id' ))  !!}
                    </div>

                    <div class="form-group col-sm-4 hidden">
                        {!!   Form::select('item_id', $items_list, $item_id, array('class' => 'form-control select2','id' => 'item_id' ))  !!}
                    </div>

                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('customer/item-gallery') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>


<!--<h2>Trending <b>Products</b></h2>-->
<section class="main-slider-area" id="swiperSlider">

      

    <div id="alert_message"  style="display: none" class="alert alert-success" role="alert">
    </div>
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <div class="box col-md-12">
        <div class="box-body col-sm-12" id="gird" data-view="girdLg">
            <div class="sort-area ">

           

                <div class="row">
                      <div class="search-area col-md-12">
                         
                        <div class="row">
                             {!! Form::open(array( 'class' => '','url' => 'customer/item-gallery-search-new', 'method' => 'get')) !!}
                            
                             <div class="search-form  col-md-6">
                                    <div class="form-group col-md-12 row">
                                        <div class="input-group">
                                           
                                            {!! Form::text('item_name', Request::input('item_name') , array('placeholder'=>"Item Name ",'maxlength' => 200,'class' => 'form-control onloadFocus', 'id' => 'item_name') ) !!}
                                            <span class="input-group-addon">
                                                <button class="btn btn-primary" type="submit" class="btn btn-primary" name="submit" id="submit"> 
                                                   <i class="fa fa-search"></i>
                                                    <span>Search</span>
                                                </button>
                                               
                                                
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="search-form col-md-2">                                
                                {!!   Form::select('selling_price_template', $templates, $template_id, array('class' => 'form-control tmpl', 'id' => 'template_id' ))  !!}
                            </div>
                            <div class="search-form col-md-2">
                                {!!   Form::select('client_id', $clients, Request::input('client_id'), array('class' => 'form-control' , 'id' => 'client_id'))  !!}
                            </div>
                        
                        {!! Form::close() !!}
                            
                    <!--<div class="box__action col-sm-7 text-left">-->
                         <div class="box__action col-sm-2 text-right">
                        <button id="girdLg" class="active">
                            <i class="fa fa-th-large"></i>
                        </button>
                        <button id="girdMd">
                            <i class="fa fa-delicious"></i>
                        </button>
                        <button id="girdSm">
                            <i class="fa fa-th"></i>
                        </button>
                        <button id="girdList">
                            <i class="fa fa-list-ul"></i>
                        </button>
                    </div>
                </div>
                          </div>
                </div>
            </div>
            <div class="row">
                <div class="product-group">                           
                    @if(count($items) >0)
                    @foreach($items as $item)
                    <div class="item-gallery col-sm-12">
                        <div class="item-gallery__inr">
                            <div class="tag">
                                {{ $item->code }}
                            </div>
                            <div class="item-gallery__img col-sm-4">
                                <!-- <a href="#"  onclick="showItemModal('{{$item->image}}')" class="view">
                                    <i class="fa fa-eye"></i>
                                </a> -->
                                @if($item->image == '')
                                <a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a>
                                @else
                                <td><a href='#' onclick="showItemGallery('{{$item->image}}','{{$item->id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>
                                <!-- <a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a> -->
                                @endif
                            </div>


                            <div class="item-gallery__cont  col-sm-8">
                                <span class="name">{{$item->name}} ({{ $item->upc_barcode }})</span>
                                @if(1==2)
                                <span class="cata">
                                    {{ $item->category_name }}
                                </span>
                                @endif
                                <span class="price">
                                    <i class="fa fa-money"></i>    
                                    {{$item->sale_price}} ({{ $item->tmpltName }})
                                </span>

                                <div class="actions">
                                    @if($invoice_btn == 1)
                                    <a onclick="showGalleryItemDetailModal({{ $item->id }})" data-id="{{ $item->id }}" data-type="invoice" data-price="{{ $item->sale_price }}" href="#galleryModal" class="open-AddBookDialog btn btn-primary"><i class="fa fa-plus"></i>
                                        <span>Add to invoice</span>
                                    </a>
                                    @endif
                                    @if($order_btn == 1)
                                    <!--<a data-toggle="modal" data-id="{{ $item->id }}" data-type="order" data-price="{{ $item->sale_price }}" href="#galleryModal" class="open-AddBookDialog btn btn-primary"><i class="fa fa-plus"> </i>-->
                                    <a onclick="showGalleryItemDetailModal({{ $item->id }})" data-id="{{ $item->id }}" data-type="order" data-price="{{ $item->sale_price }}" href="#galleryModal" class="open-AddBookDialog btn btn-primary"><i class="fa fa-plus"> </i>
                                        <span>Add to Order</span>
                                    </a>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="float: right !important"> 
                <?php echo $items->appends(Input::query())->render(); ?>
            </div>
        </div>
</section>
<script>
$(".myclass0").on("click", function (e) {
    e.preventDefault();
    var query = $('#item_name').val();
    var query_templt = $('#template_id').val();
    var query_client = $('#client_id').val();
    
    window.location = this.href + '&item_name='+$.trim(query) + '&template='+ $.trim(query_templt) + '&client_id=' + $.trim(query_client);
});
var cT_array = <?php echo json_encode($clientTemplate); ?>;
$(document).ready(function () {
    $(".tmpl").change(function() {
        $('#client_id').val('0');
    });
    $("#client_id").change(function() {
        var a = $(this).val();
        $(".tmpl").val(cT_array[a]);
        // $(".tmpl").val("0");
    });
}) 
</script>
<script>
    var autoSearch = false;
    window.onload = function() {
        $('.onloadFocus').trigger("focus");
    };
    var typingTimer;
    var doneTypingInterval = 1000;
    var input = $('#item_name');
    input.on('keyup', function () {
    clearTimeout(typingTimer);
        autoSearch = true;
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    input.on('keydown', function () {
        autoSearch = true;
        clearTimeout(typingTimer);
    });
    function doneTyping () {
        var len = $('#item_name').val().length;
        if(len > 2 && autoSearch == true){
            document.getElementById("submit").click();
        }
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.onloadFocus').trigger("focus");

        var cname = 'gview'
       var getcook = getCookie(cname);
       if (getcook == 'girdLg') {
            $("#gird").attr("data-view", "girdLg");
       }else if(getcook == 'girdMd'){
            $("#gird").attr("data-view", "girdMd");
       }else if(getcook == 'girdSm'){
            $("#gird").attr("data-view", "girdSm");
       }else if(getcook == 'girdList'){
            $("#gird").attr("data-view", "List");
       }
    document.getElementById("alert_message").innerHTML = '';
    document.getElementById("alert_message").style.display = "none";
    var category_select = document.getElementById('category_id');
    var template_id = '0';
    category_select.addEventListener('change', function () {

    }, false);
    $('#template_id').on('select2:select', function (e) {
    var data = e.params.data;
    template_id = data.id;
    updateFilters(data.id);
    });
    $('#category_id').on('select2:select', function (e) {
    var data = e.params.data;
    var template_id = $('#template_id').val();
    updateItemFilters(template_id, data.id);
    });
    });

    function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

    function updateItemList(vendor_id) {

    var url = "<?php echo url('get/open-po-by-vendor'); ?>" + '/' + vendor_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            $("select[name='po_id'").html('');
            $("select[name='po_id'").html(response);
            var $table = $('#item_body');
            var $table = $('#item_body');
            $table.find('tbody').empty();
            },
            error: function (xhr, status, response) {
            }
    });
    }
    


    function addToOrder(item_id,quantity,price) {

    var url = "<?php echo url('customer/add-to-order/'); ?>" + '/' + item_id + '/' + quantity + '/'+ price;

    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
//            document.getElementById("alert_message").style.display = "none";
//            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }


    function addToInvoice(item_id,quantity,price) {

    var url = "<?php echo url('customer/add-to-invoice/'); ?>" + '/' + item_id + '/' + quantity + '/'+ price;

    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
//            document.getElementById("alert_message").style.display = "none";
//            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }



    $("#girdLg").on("click", function(){
        $("#gird").attr("data-view", "girdLg");
        $(".box__action button").removeClass("active");
        $(this).addClass("active");
        document.cookie = "gview=girdLg"; 
        localStorage.setItem("gview", "girdLg");
    });
    $("#girdMd").on("click", function(){
        $("#gird").attr("data-view", "girdMd");
        $(".box__action button").removeClass("active");
        $(this).addClass("active");
        document.cookie = "gview=girdMd";
        localStorage.setItem("gview", "girdMd");
    });
    $("#girdSm").on("click", function(){
        $("#gird").attr("data-view", "girdSm");
        $(".box__action button").removeClass("active");
        $(this).addClass("active");
        document.cookie = "gview=girdSm"; 
        // startTime = 'sohaib';
        // localStorage.setItem("startTime", startTime);
        localStorage.setItem("gview", "girdSm");
    });
    $("#girdList").on("click", function(){
        $("#gird").attr("data-view", "List");
        $(".box__action button").removeClass("active");
        $(this).addClass("active");
        document.cookie = "gview=girdList"; 
        localStorage.setItem("gview", "girdList");
    });

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    });
    function updateFilters(template_id) {

    var url = "<?php echo url('customer/get/templates-categories'); ?>" + '/' + template_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {

            $("#category_id").select2("val", "0");
            $("select[name='category_id'").html('');
            $("select[name='category_id'").html(response);
            },
            error: function (xhr, status, response) {
            }
    });
    updateItemFilters(template_id, '0');
    }

    function updateItemFilters(template_id, category_id) {

    var url = "<?php echo url('customer/get/templates-items'); ?>" + '/' + template_id + '/' + category_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {

            $("#item_id").select2("val", "0");
            $("select[name='item_id'").html('');
            $("select[name='item_id'").html(response);
            },
            error: function (xhr, status, response) {
            }
    });
    }
    
    function showGalleryItemDetailModal($item_id){
        
       $('#galleryModal').modal('toggle');
       subItemsModal($item_id)
    }


    function xyz() {

        let d = localStorage.getItem("gview")
        console.log(d);
        $(".box__action button").removeClass("active");

        document.getElementById(d).classList.add("active")
    }

    xyz();
name="item_name"

  

</script>
@include('front/common/image_modal')
@include('front/common/gallery_purchase_detail')

@endsection



