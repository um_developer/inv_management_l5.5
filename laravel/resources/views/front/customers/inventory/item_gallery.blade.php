@extends('customer_c_template')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Item</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'customer/item-gallery-search', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">

                    <div class="form-group col-sm-4">

                        <!--<label for="exampleInputEmail1">Select Item</label>-->
                        {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control ','id' => 'category_id' ))  !!}
                    </div>

                    <div class="form-group col-sm-4">

                        <!--<label for="exampleInputEmail1">Select Item</label>-->
                        {!!   Form::select('item_id', $items_list, $item_id, array('class' => 'form-control select2','id' => 'item_id' ))  !!}
                    </div>
                    
                    <div class="form-group col-sm-4">

                         {!!   Form::select('selling_price_template', $templates, $template_id, array('class' => 'form-control' ))  !!}
                    </div>


                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('customer/item-gallery') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>
<style type="text/css">
    body .main-slider-area .dontfly0.swiper-container .swiper-slide {
        border-radius: 15px;
    }
    body .main-slider-area .dontfly.swiper-container .swiper-slide img {
        max-width: 100%;
        max-height: auto;
        height: auto;
    }
    .swiper-button-custom {
        position: absolute;
        font-size: 40px;
        line-height: 0;
        width: 40px;
        height: 40px;
        background: transparent;
        color: #605ca8;
        border-radius: 50%;
        text-align: center;
        border: 3px solid #605ca8;
        top: 50%;
        cursor: pointer;
    }
    .swiper-button-custom.swiper-button-next1>.fa {
        position: relative;
        top: -5px;
        left: 2px;
    }
    .swiper-button-custom.swiper-button-prev1>.fa {
        position: relative;
        top: -5px;
        left: -1px;
    }
    .swiper-button-custom.swiper-button-next1 {
        right: 128px;
    }
    .swiper-button-custom.swiper-button-prev1 {
        left: 115px;
    }
    .carousel .item .btn {
        color: #333;
        border-radius: 0;
        font-size: 11px;
        text-transform: uppercase;
        font-weight: bold;
        background: none;
        border: 1px solid #ccc;
        padding: 5px 10px;
        margin-top: 5px;
        line-height: 16px;
    }
    .carousel .item .btn:hover, .carousel .item .btn:focus {
        color: #fff;
        background: #000;
        border-color: #000;
        box-shadow: none;
    }
    .carousel .item .btn i {
        font-size: 14px;
        font-weight: bold;
        margin-left: 5px;
    }
    .carousel .thumb-wrapper {
        text-align: center;
    }
    .carousel .thumb-content {
        padding: 15px;
    }
    .carousel .carousel-control {
        height: 100px;
        width: 40px;
        background: none;
        margin: auto 0;
        background: rgba(0, 0, 0, 0.2);
    }
    .carousel .carousel-control i {
        font-size: 30px;
        position: absolute;
        top: 50%;
        display: inline-block;
        margin: -16px 0 0 0;
        z-index: 5;
        left: 0;
        right: 0;
        color: rgba(0, 0, 0, 0.8);
        text-shadow: none;
        font-weight: bold;
    }
</style>
<style type="text/css">
    body {
        font-family: "Open Sans", sans-serif;
    }
    h2 {
        color: #000;
        font-size: 26px;
        font-weight: 300;
        text-align: center;
        text-transform: uppercase;
        position: relative;
        margin: 14px 0 41px;
    }
    h2 b {
        color: #605ca8;
    }
    h2::after {
        content: "";
        width: 100px;
        position: absolute;
        margin: 0 auto;
        height: 4px;
        background: rgba(0, 0, 0, 0.2);
        left: 0;
        right: 0;
        bottom: -20px;
    }
    .carousel {
        margin: 50px auto;
        padding: 0 70px;
    }
    .item {
        min-height: 330px;
        text-align: center;
        overflow: hidden;
    }
    .img-box {
        height: 400px;
        width: 400px;
        position: relative;
        margin-left:225px;
        /*         text-align: center;*/
    }
    .img {	
        max-width: 100%;
        max-height: 100%;
        height: 400px;
        width: 400px;
        display: inline-block;
        position: absolute;
        bottom: 0;
        margin: 0 auto;
        left: 0;
        right: 0;
    }
    .item h4 {
        font-size: 18px;
        margin: 10px 0;
    }
    .item .btn {
        color: #333;
        border-radius: 0;
        font-size: 11px;
        text-transform: uppercase;
        font-weight: bold;
        background: none;
        border: 1px solid #ccc;
        padding: 5px 10px;
        margin-top: 5px;
        line-height: 16px;
    }
    .item .btn:hover, .carousel .item .btn:focus {
        color: #fff;
        background: #000;
        border-color: #000;
        box-shadow: none;
    }
    .item .btn i {
        font-size: 14px;
        font-weight: bold;
        margin-left: 5px;
    }
    .thumb-wrapper {
        text-align: center;
    }
    .thumb-content {
        padding: 15px;
    }
    .carousel-control {
        height: 100px;
        width: 40px;
        background: none;
        margin: auto 0;
        background: rgba(0, 0, 0, 0.2);
    }
    .carousel-control i {
        font-size: 30px;
        position: absolute;
        top: 50%;
        display: inline-block;
        margin: -16px 0 0 0;
        z-index: 5;
        left: 0;
        right: 0;
        color: rgba(0, 0, 0, 0.8);
        text-shadow: none;
        font-weight: bold;
    }
    .item-price {
        font-size: 13px;
        padding: 2px 0;
    }
    .main-slider-area {
        background-color: #ffffff;
        padding: 10px 0;
        min-height:200px;
    }

    section[class*="-area"] {position: relative; clear:both; }

</style>
<!--<h2>Trending <b>Products</b></h2>-->
<section class="main-slider-area" id="swiperSlider">
        <div class="container">
        <div id="alert_message" style="display: none" class="alert alert-success" role="alert">
        </div>
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if(count($items) > 0)
         <div class="swiper-container dontfly0 w90 s3" style="width:80%"><div class="swiper-wrapper">
                @foreach($items as $item)

                <div class="col-sm-4 swiper-slide">
                    <div class="thumb-wrapper">
                        <div class="img-box">
                            @if($item['image'] == '')
                            <a href='#' onclick="showItemModal('{{$item['image']}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" class="img-responsive img img-fluid"></a>
                            @else
                            <a href='#' onclick="showItemModal('{{$item['image']}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item['image']}}" class="img-responsive img img-fluid"></a>
                            @endif
                        </div>
                        <div class="thumb-content">
                            <h4>{{ ucfirst($item['name']) }}</h4>
                            <p class="item-price"><span>${{ $item['sale_price'] }}</span></p>
                            @if($invoice_btn == 1)
                            <a onclick="addToInvoice('{{ $item['id'] }}')"  class="btn btn-primary"><i class="fa fa-plus"> Add to invoice</i></a>
                            @endif
                            @if($order_btn == 1)
                            <a onclick="addToOrder('{{ $item['id'] }}')" class="btn btn-primary"><i class="fa fa-plus"> Add to Order</i></a>
                            @endif
                        </div>						
                    </div>
                </div>
                 @endforeach
            </div></div>
                <div class="swiper-button-custom swiper-button-next1"><i class="fa fa-angle-right"></i></div>
                <div class="swiper-button-custom swiper-button-prev1"><i class="fa fa-angle-left"></i></div>
        @else
        <div class="col-sm-4">

        </div>
        <div class="col-sm-4">
            No Items Found
        </div>
        <div class="col-sm-4">

        </div>
        @endif

            </div>
</section>


<link href="{{ asset('front/css/swiper.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ asset('front/js/swiper.jquery.min.js')}}"></script>

<script>

                                var swiper1 = new Swiper('.s3', {
                                    pagination: '.swiper-pagination1',
                                            slidesPerView: 1,
                                           	centeredSlides: false,
                                        nextButton: '.swiper-button-next1',
                                        prevButton: '.swiper-button-prev1',
                                        autoplay: false,
                                        autoplayDisableOnInteraction: false,
                                            paginationClickable: true,
                                            spaceBetween: 0,
                                            breakpoints: {
                                                1024: {slidesPerView: 1},
                                                        860: {slidesPerView: 1},
                                                        568: {slidesPerView: 1},
                                                        320: {slidesPerView: 1}
                                            }
                                });
                                $(function () {
                                $('.select2').select2()
                                });</script>

<script type="text/javascript">
    $(document).ready(function () {
    document.getElementById("alert_message").innerHTML = '';
    document.getElementById("alert_message").style.display = "none";
    var category_select = document.getElementById('category_id');
    category_select.addEventListener('change', function () {
//    alert('a');
    // updateItemList(category_select.value);
    }, false);
    });
    function updateItemList(vendor_id) {

    var url = "<?php echo url('get/open-po-by-vendor'); ?>" + '/' + vendor_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            $("select[name='po_id'").html('');
            $("select[name='po_id'").html(response);
            var $table = $('#item_body');
            var $table = $('#item_body');
            $table.find('tbody').empty();
            },
            error: function (xhr, status, response) {
            }
    });
    }

    function addToOrder(item_id) {

    var url = "<?php echo url('customer/add-to-order/'); ?>" + '/' + item_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
                        setTimeout(function(){
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }


    function addToInvoice(item_id) {

    var url = "<?php echo url('customer/add-to-invoice/'); ?>" + '/' + item_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            
            setTimeout(function(){
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }

//    $("document").ready(function(){
//    setTimeout(function(){
//       $("div.alert").remove();
//    }, 2000 ); // 5 secs
//
//});
</script>
@include('front/common/image_modal')
@endsection



