@extends('customer_c_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Inventory</h3>
<!--                <a href="{{ URL::to('customer/inventory/generate/report') }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>
                <a href="{{ url('item/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Item</a>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Item Image</th>
                            <th>Item ID</th>
                            <th>SKU/Item Code</th>
                            <th>Item Name</th>
                            <th>UPC/Barcode</th>
                            <th>Category Name</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            @if($item->image == '')
                            <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                            @else
                            <td><a href='#' onclick="showItemGallery('{{$item->image}}','{{$item->item_id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>
                            <!-- <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td> -->
                            @endif
                            <td>{{$item->item_id}}</td>
                            <td>{{$item->item_sku}}</td>
                            <td>{{$item->item_name}}</td>
                            <td>{{$item->item_upc_barcode}}</td>
                            <td>{{$item->category_name}}</td>
                            <td>{{$item->quantity}}</td>
                            <td> <a href="{{ url('customer/add-to-invoice/'.$item->item_id) }}" class="btn btn-primary"><i class="fa fa-plus"> Add to invoice</i></a></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>
@include('front/common/image_modal')
@include('front/common/dataTable_js')
<script>
    $(function () {
    $('#vendors').DataTable({
    "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
             dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export To Excel',
                title: 'my_inventory',
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Export To PDF',
                title: 'my_inventory',
               exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
            }
        ]
    });
    });
</script>

@endsection



