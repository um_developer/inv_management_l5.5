@extends('customer_c_template')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Item</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'customer/item-gallery-search', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">

                    <div class="form-group col-sm-4">

                        <!--<label for="exampleInputEmail1">Select Item</label>-->
                        {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control ','id' => 'category_id' ))  !!}
                    </div>

                    <div class="form-group col-sm-4">

                        <!--<label for="exampleInputEmail1">Select Item</label>-->
                        {!!   Form::select('item_id', $items_list, $item_id, array('class' => 'form-control select2','id' => 'item_id' ))  !!}
                    </div>

                    <div class="form-group col-sm-4">

                        {!!   Form::select('selling_price_template', $templates, $template_id, array('class' => 'form-control' ))  !!}
                    </div>


                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('customer/item-gallery') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>

<style>
    .col-item
    {
        border: 1px solid #E1E1E1;
        border-radius: 5px;
        background: #FFF;
    }
    .col-item .photo img
    {
        margin: 0 auto;
        width: 100%;
    }

    .col-item .info
    {
        padding: 10px;
        border-radius: 0 0 5px 5px;
        margin-top: 1px;
    }

    .col-item:hover .info {
        background-color: #F5F5DC;
    }
    .col-item .price
    {
        /*width: 50%;*/
        float: left;
        margin-top: 5px;
    }

    .col-item .price h5
    {
        line-height: 20px;
        margin: 0;
    }

    .price-text-color
    {
        color: #219FD1;
    }

    .col-item .info .rating
    {
        color: #777;
    }

    .col-item .rating
    {
        /*width: 50%;*/
        float: left;
        font-size: 17px;
        text-align: right;
        line-height: 52px;
        margin-bottom: 10px;
        height: 52px;
    }

    .col-item .separator
    {
        border-top: 1px solid #E1E1E1;
    }

    .clear-left
    {
        clear: left;
    }

    .col-item .separator p
    {
        line-height: 20px;
        margin-bottom: 0;
        margin-top: 10px;
        text-align: center;
    }

    .col-item .separator p i
    {
        margin-right: 5px;
    }
    .col-item .btn-add
    {
        width: 50%;
        float: left;
    }

    .col-item .btn-add
    {
        border-right: 1px solid #E1E1E1;
    }

    .col-item .btn-details
    {
        width: 50%;
        float: left;
        padding-left: 10px;
    }
    .controls
    {
        margin-top: 20px;
    }
    [data-slide="prev"]
    {
        margin-right: 10px;
    }

</style>

<!--<h2>Trending <b>Products</b></h2>-->
<section class="main-slider-area" id="swiperSlider">
        <div class="container">
        <div id="alert_message"  style="display: none" class="alert alert-success" role="alert">
        </div>
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif


        <div id="body">

            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>
                            </h3>
                        </div>
                        <div class="col-md-3">
                            <!-- Controls -->
                            <div class="controls pull-right hidden-xs">
                                <a class="left fa fa-chevron-left btn btn-primary" href="#carousel-example"
                                   data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-primary" href="#carousel-example"
                                   data-slide="next"></a>
                            </div>
                        </div>
                    </div>
                    <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <?php
                        $products = $items;
                        $is_active = true;
                        $i = 0;
                        ?>
                            <div class="carousel-inner">
                                    @foreach($products as $item)
                                    @if($i % 4 == 0)
                                    <div class="item<?php if ($is_active) echo ' active' ?>">
                                            @endif
                                            <div class="row">
                                                    <div class="col-sm-3">
                                        <div class="col-item">
                                            <div class="photo">
                                                @if($item['image'] == '')
                                                <a href='#' onclick="showItemModal('{{$item['image']}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" class="img-responsive"></a>
                                                @else
                                                <a href='#' onclick="showItemModal('{{$item['image']}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item['image']}}" class="img-responsive"></a>
                                                @endif

                                            </div>
                                            <div class="info">
                                                <div class="row">
                                                    <div class="price col-md-6">
                                                        <h5>
                                                            {{ ucfirst($item['name']) }}</h5>
                                                        <h5 class="price-text-color">
                                                            ${{ $item['sale_price'] }}</h5>
                                                    </div>
                                                    <div class="rating hidden-sm col-md-6">
                                                    </div>
                                                </div>


                                                <div class="separator clear-left">
                                                    @if($invoice_btn == 1)
                                                    <p class="btn-add">
                                                        <a onclick="addToInvoice('{{ $item['id'] }}')"  class="btn btn-primary"><i class="fa fa-plus"> Add to invoice</i></a>
                                                    </p>
                                                    @endif
                                                    @if($order_btn == 1)
                                                    <p class="btn-details">
                                                        <a onclick="addToOrder('{{ $item['id'] }}')" class="btn btn-primary"><i class="fa fa-plus"> Add to Order</i></a>
                                                    </p>

                                                    @endif
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                </div>
                                            @if (($i+1) % 4 == 0 || $i == count($products)-1)
                                        </div>
                                    @endif
                                    <?php
                            $i++;
                            if ($is_active) {
                                $is_active = false;
                            }
                            ?>
                                    @endforeach
                                </div>
                    </div>
                </div>

            </div>

        </div>



            </div>
</section>


<script type="text/javascript">
    $(document).ready(function () {
    document.getElementById("alert_message").innerHTML = '';
    document.getElementById("alert_message").style.display = "none";
    var category_select = document.getElementById('category_id');
    category_select.addEventListener('change', function () {
//    alert('a');
    // updateItemList(category_select.value);
    }, false);
    });
    function updateItemList(vendor_id) {

    var url = "<?php echo url('get/open-po-by-vendor'); ?>" + '/' + vendor_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            $("select[name='po_id'").html('');
            $("select[name='po_id'").html(response);
            var $table = $('#item_body');
            var $table = $('#item_body');
            $table.find('tbody').empty();
            },
            error: function (xhr, status, response) {
            }
    });
    }

    function addToOrder(item_id) {

    var url = "<?php echo url('customer/add-to-order/'); ?>" + '/' + item_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }


    function addToInvoice(item_id) {

    var url = "<?php echo url('customer/add-to-invoice/'); ?>" + '/' + item_id;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            document.getElementById("alert_message").style.display = "block";
            document.getElementById("alert_message").innerHTML = response;
            setTimeout(function () {
            document.getElementById("alert_message").style.display = "none";
            document.getElementById("alert_message").innerHTML = '';
            }, 2000);
            },
            error: function (xhr, status, response) {
            }
    });
    }

//    $("document").ready(function(){
//    setTimeout(function(){
//       $("div.alert").remove();
//    }, 2000 ); // 5 secs
//
//});
</script>
@include('front/common/image_modal')
@endsection



