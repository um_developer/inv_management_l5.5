@extends('customer_c_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Add Payment</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/payment/create', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Amount</label>
                    {!! Form::text('amount', Request::input('amount') , array('placeholder'=>"Amount ",'maxlength' => 10,'class' => 'form-control',$required) ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Payment Type</label>
                    {!!   Form::select('payment_source_id', $payment_sources, Request::input('payment_sources_id'), array('class' => 'form-control',$required ))  !!}
                </div>
            </div>
            
             <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Account Type</label>
                    {!!   Form::select('account_id', $accounts, Request::input('account_id'), array('class' => 'form-control',$required ))  !!}
                </div>
            </div>
            
             <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>
               
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Payment Message</label>
                    {!! Form::textarea('note', Request::input('note') , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}
                </div>
            </div>



        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" id="submit_btn" name="submit" class="btn btn-primary pull-right" value="Create">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

<script>
    
    $(function () {
//            Bootstrap DateTimePicker v4
    $('#datetimepicker4').datetimepicker({
        format: 'MM/DD/YYYY'
    });
});

    $(document).ready(function () {

 $('#datetimepicker4').data("DateTimePicker").date(moment(new Date()).format('MM/DD/YYYY'));
 
 $('.form-horizontal').submit(function () {
            $('#submit_btn').prop('disabled', true);
        });

       
    });    
    </script>
</section>

@endsection