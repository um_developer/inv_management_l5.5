@extends('register_template')
@section('content')

<?php
$passwordPattern = Config::get('params.password_pattern');
$required = 'required';
?>
  <div class="register-logo">
   <b>Owner</b> Registration Area
  </div>
 <div class="register-box-body">
    <p class="login-box-msg">Fill this form to register yourself.</p>
 @if (count($errors->register) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->register->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success">
            <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
        </div>
        @endif
    {!! Form::open(array( 'class' => 'form','url' => 'signUpPost', 'name' => 'register','id' => 'register_formid')) !!}

        <div class="row">
            <div class="form-group col-sm-6">  
                {!! Form::text('firstName', Request::input('firstName') , array('placeholder'=>"First Name *",'maxlength' => 40,'class' => 'form-control',$required) ) !!}
            </div>
            <div class="form-group col-sm-6">  
                {!! Form::text('lastName', null , array('placeholder'=>"Last Name *",'class' => 'form-control','maxlength' => 30,$required) ) !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-12">
                {!! Form::text('email', null , array('placeholder'=>"Email *",'class' => 'form-control','maxlength' => 40,$required) ) !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                {!! Form::password('password', array('placeholder'=>"Password *",'class' => 'form-control',$required) ) !!}

            </div>

            <div class="form-group col-sm-6">
                {!! Form::password('password_confirmation', array(

                'data-match-error'=>"Whoops, these don't match",
                'placeholder'=>"Confirm Password *",
                "data-match"=>"#password",
                'class' => 'form-control',$required))
                !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-12">
                <div class="alert alert-info">
                    Your password must contain minimum 6 characters.
                </div> 
            </div>
        </div>
        <div class="row">                    
        </div>
        <div class="row">

            <div class="form-group col-sm-12">
          @include('front/common/country')
                
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-6">
                {!! Form::text('state', null , array('placeholder'=>"state ",'maxlength' => 20,'class' => 'form-control') ) !!}
            </div>

            <div class="form-group col-sm-6">
                {!! Form::text('city', null , array('placeholder'=>"City *",'maxlength' => 20,'class' => 'form-control',$required) ) !!}
            </div>
        </div>
        <div class="row">

        </div>

        <div class="row">

            <div class="form-group col-sm-6">
                {!! Form::text('phone', null , array('placeholder'=>"Phone *",'maxlength' => 20,'class' => 'form-control',$required) ) !!}
            </div>
            <div class="form-group col-sm-6">
                {!! Form::text('company', null , array('placeholder'=>"Company name *",'maxlength' => 20,'class' => 'form-control',$required) ) !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-6">  
                <?php
                //echo Recaptcha::render(['lang' => 'en']);
                ?>
            </div>
            <div class="form-group col-sm-6 mb40 text-right">
                <button type="submit" onclick="$('#register_formid').submit();" class="btn btn-flat btn-primary " >SIGN UP</button>
            </div>
        </div>

        {!! Form::hidden('role_id',2) !!}
        </div>
        <div class="clearfix"></div>
    </div>



{!! Form::close() !!}

@endsection
