@extends('customer_c_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
       @include('front/common/client_common_tabs')
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    {!! Form::open(array( 'class' => '','url' => 'customer/orders/search', 'method' => 'get')) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-3">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div></div>
                            <div class="form-group col-sm-3">

                                {!!  Form::select('client_id', $clients, Request::input('client_id'), array('class' => 'form-control select2','id' => 'client_id' ))  !!}
                            </div>
                            <div class="form-group col-sm-3" id="type">

                                <select id="type" name="type" class="form-control">
                                    <option value="" <?php echo ($selected_report_type == '') ? 'selected' : ''; ?>>Select Status</option>        
                                    <option value="pending" <?php echo ($selected_report_type == 'pending') ? 'selected' : ''; ?>>Draft</option>
                                    <option value="approved" <?php echo ($selected_report_type == 'approved') ? 'selected' : ''; ?>>Inv Created (Admin)/Client Charged</option>
                                    <option value="approved_charged" <?php echo ($selected_report_type == 'approved_charged') ? 'selected' : ''; ?>>Client Charged</option>
                                    <option value="delivered" <?php echo ($selected_report_type == 'delivered') ? 'selected' : ''; ?>>Delivered</option>
                                    <option value="processing" <?php echo ($selected_report_type == 'processing') ? 'selected' : ''; ?>>Processing</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <input id="order_number" onkeypress="return isNumber(event)" type="text" name="order_number" value="{{$selected_order_number}}" placeholder="Order Number" class="form-control">
                            </div>
                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>


                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                                <a href="{{ URL::to('customer/orders') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>

    @include('front/common/common_orders')
    </div>

</section>

<script>

    function change_status(item_id, status) {

    var url = "<?php echo url('customer/invoice/update-status'); ?>" + '/' + item_id + '/' + status;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            $("select[name='po_id'").html('');
            $("select[name='po_id'").html(response);
            var $table = $('#item_body');
            var $table = $('#item_body');
            $table.find('tbody').empty();
            },
            error: function (xhr, status, response) {
            }
    });
    }
    function isNumber(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    
    $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });
        
</script>
@endsection



