@extends('customer_c_template') 

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Price Change</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/priceChanged', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
        <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Client</label>
                    {!! Form::select('client_id', $clients, Request::input('client_id'), array('class' => 'form-control select2','id' => 'client_id1',$required )) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Items</label>
                    {!! Form::select('item_id', $items, Request::input('item_id'), array('class' => 'form-control select2','id' => 'item_id1',$required )) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Difference</label>
                    {!! Form::text('diff', Request::input('diff') , array('placeholder'=>"Difference *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="form-group" id="priviousPrice0" style="display:none;">
                <div class="col-sm-12">
                <label for="exampleInputEmail1">Current Price</label>
                    {!! Form::text('priviousPrice',Request::input('priviousPrice') , array('placeholder'=>"Privious Price",'maxlength' => 100,'class' => 'form-control', 'readonly' => 'readonly','id' => 'priviousPrice') ) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="radio" id="effect" value="1" name="effect" checked><b>Increase<b>
                    <input type="radio" id="effect" value="2" name="effect"><b>Decrease<b>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right cr_rc_btn" value="Save">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>

</section>
<script type="text/javascript">
    $(document).ready(function () {
        let client_id = $('#client_id1').val()
        let item_id = $('#item_id1').val()
        getItemPriceHTML(client_id, item_id)
        $('#item_id1').on('change', function() {
        let item_id = this.value;
        let client_id = $('#client_id1').val()
        getItemPriceHTML(client_id, item_id)
    });
    $('#client_id1').on('change', function() {
        let client_id = this.value;
        let item_id = $('#item_id1').val()
        getItemPriceHTML(client_id, item_id)
    });

    $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    }) 

    function getItemPriceHTML(client_id, item_id) {
        type = 'order';
        my_url = "<?php echo url('customer/get-item-price/'); ?>" + '/' + item_id + '/' + client_id;

        $.ajax({
            url: my_url,
            type: 'get',
            success: function (response) {

                if (response != '0') {
                    $('#priviousPrice').val(response)
                    document.getElementById("priviousPrice0").style.display = "block";
                }else{
                    $('#priviousPrice').val('0')
                    document.getElementById("priviousPrice0").style.display = "none";
                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });
    }
</script>
@endsection