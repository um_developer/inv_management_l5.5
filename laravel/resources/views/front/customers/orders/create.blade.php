@extends('customer_c_template')

@section('content')
<?php
$required = 'required';
if (!isset($address))
    $address = '';
?>
<section id="form">
    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Client Order</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/order/create','id' => 'invoice_form', 'method' => 'post')) !!}
        <div class="box-body">



            @if($client_id == '0')
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Client</label>
                    {!!   Form::select('client_id', $clients, Request::input('client_id'), array('class' => 'form-control select_client','id' => 'client_id',$required ))  !!}
                </div>
                @if($show_bundle==1)
                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control  select_bundle ', 'disabled' => true,'id' => 'bundle_id' ))  !!}
            
               </div>
               @endif
             
                 <div class="col-sm-3">
                <label for="exampleInputEmail1">Select List Title</label>
                {!!   Form::select('list_id', $list_words, Request::input('list_id'), array('class' => 'form-control','id' => 'list_id' ))  !!}

            </div>

            </div>
            @else
            <input type="hidden"  name="client_id" value="{{ $client_id }}">
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Client</label>
                    {!!   Form::select('client_id', $clients, $client_id, array('class' => 'form-control select_client','id' => 'client_id',$required ))  !!}
                </div>
                
                @if($show_bundle==1)
                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control  select_bundle ', 'disabled' => true,'id' => 'bundle_id' ))  !!}
            
               </div>
               @endif
                 <div class="col-sm-3">
                <label for="exampleInputEmail1">Select List Title</label>
                {!!   Form::select('list_id', $list_words, Request::input('list_id'), array('class' => 'form-control','id' => 'list_id' ))  !!}

            </div>

            </div>
            @if(1==2)
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Client Name</label>
                    {!! Form::text('name', $client_name , array('placeholder'=>"Customer Name *",'maxlength' => 100,'class' => 'form-control',$required,"disabled"=>true) ) !!}
                </div>
            </div>
            @endif
            @endif
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', Request::input('memo') , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                    <input type="hidden"  name="package_id" value="0">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>
                
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Price Template Name</label>
                    <span class="form-control">{{ $template_name }}</span>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Address</label>
                    <span class="form-control">{{ $address }}</span>
                </div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Phone #</label>
                    <span class="form-control">{{ $phone }}</span>
                </div>
            </div>

            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>
            
           @include('front/common/invoice_js')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody></tbody>
                    <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                            <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
            <span class="text-red" id="err_mesg"></span>
        </div>
        {!! Form::close() !!} 
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Create Order">
        </div>
    </div>
    
    @include('front/common/image_modal')
    @include('front/common/item_detail_modal')
    @include('front/common/bundle_view_modal')	
</section>

<script>

    $(document).ready(function () {
        var client_select = document.getElementById('client_id');
         client_select.addEventListener('change', function () {
            $('#submit_btn').prop('disabled', true);
            var url = "{{ url('customer/order/create/') }}"
            window.location = url + '/' + client_select.value;
        }, false);
    });
    checkcustomer();
                    function checkcustomer(){
                    
                        var client_id = $("#client_id option:selected").val();
                    
            if(client_id==0){
            //add disabled
            $("#bundle_id").attr('disabled', 'disabled');
            }else{
              $("#bundle_id").removeAttr("disabled");
            }
                    }  

 $('.select_bundle').on('change', function () {
                        var data = $(".select_bundle option:selected").val();
                        var customer_id = $("#client_id").val();
                        var type="order";
                       loadbundleAjax(data, j,customer_id,type);
                    });
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }


 $(function () {
            //Initialize Select2 Elements
          //  $('.select2').select2();
        });
</script>
@endsection