@extends('customer_c_template')

@section('content')
<?php
$required = 'required';
$is_order = strpos(Request::url(), 'order');
if (!isset($address))
    $address = '';
?>
<section id="form">

    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            @if($model->status != 'pending')
            <h3 class="box-title">Modify Client Order # {{ $model->id }}</h3>
            @else
            <h3 class="box-title">Edit Client Order # {{ $model->id }}</h3>
            @endif

            @if($model->invoice_refrence_id != '0')
            &nbsp; &nbsp;(Sync Invoice # <a href="{{ url('customer/invoice/'.$model->invoice_refrence_id) }}">{{$model->invoice_refrence_id}}</a>)
            @endif
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if($model->invoice_refrence_id > 0)
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/order/modified','id' => 'invoice_form', 'method' => 'post')) !!}
        @endif

        @if($model->status != 'pending')
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/order/modified','id' => 'invoice_form', 'method' => 'post')) !!}
        @else
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/order/update','id' => 'invoice_form', 'method' => 'post')) !!}
        @endif
        <input type="hidden"  name="id" value="{{ $model->id }}">
        <input type="hidden"  name="package_id" value="0">
        <div class="box-body">


            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Client</label>
                    @if($model->invoice_refrence_id > 0)
                    {!!   Form::select('client_id', $clients, $client_id, array('class' => 'form-control','id' => 'client_id',$required,"disabled"=>true ))  !!}
                    @else
                    {!!   Form::select('client_id', $clients, $client_id, array('class' => 'form-control','id' => 'client_id',$required ))  !!}

                    @endif
                </div>
                
                 <div class="col-sm-3">
                <label for="exampleInputEmail1">Select List Title</label>
                {!!   Form::select('list_id', $list_words, $model->list_id, array('class' => 'form-control','id' => 'list_id' ))  !!}

            </div>
            @if($show_bundle==1)
    <div class="col-sm-3">
        <label for="exampleInputEmail1">Select Bundle #</label>
        {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control  select_bundle ', 'disabled' => true,'id' => 'bundle_id' ))  !!}

   </div>
   @endif


            </div>

            <script>

                $(document).ready(function () {

                    var client_select = document.getElementById('client_id');
                    var order_id = "{{ $model->id }}";
                    client_select.addEventListener('change', function () {

                        if (client_select.value != 0) {
                            var url = "{{ url('customer/order/edit/') }}" + "/" + order_id + "/" + client_select.value;
                            window.location = url;
                        } else {
                            alert('Please Select any client');

                        }

                    }, false);
                });
            </script>
            @if(1==2)
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Client Name</label>

                    {!! Form::text('name', $client_name , array('placeholder'=>"Customer Name *",'maxlength' => 100,'class' => 'form-control',$required,"disabled"=>true) ) !!}
                </div>
            </div>
            @endif

                
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', $model->memo , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Price Template Name</label>
                    <span class="form-control">{{ $template_name }}</span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Address</label>
                    <span class="form-control">{{ $address }}</span>
                </div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Phone #</label>
                    <span class="form-control">{{ $phone }}</span>
                </div>
            </div>

            @if($model->status == 'processing' || $model->status == 'completed' || $model->status == 'delivered')
            <a href="#" id="add_more" class="btn btn-primary pull-right disabled"><i class="fa fa-plus"></i> Insert Item</a><br><br>
            @else
            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>
            @endif

            @include('front/common/invoice_edit_js')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped item_body" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        ?>
                        @if(count($po_items)>0 && isset($po_items[0]))
                        @foreach($po_items as $items_1)
                        <?php 
                        if($items_1->bundle_name != ''){
                               $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                              $items_1->item_name = $items_1->item_name . $bundle_detail;
                          }
                        ?>
                        <?php
//                        print_r($items_1); die;
                        $delivered_items = $items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
//                        $validation_num = '';
                        ?>
                        @if(1==1)
                        <tr id="item_{{ $i }}">
                            <td><input class="form-control" type="text" title= "{{$items_1->item_name}}" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
                                <input type="hidden" readonly="readonly" name="po_item_id_{{ $i }}" id="po_item_id_{{ $i }}"  value="{{$items_1->id}}">
                                <input type="hidden" name="num[]" id="num[]">
                                <input type="hidden" name="bundle_id" id="bundle_id" value="{{$items_1->bundle_id}}">
                            </td>
                    <input type="hidden" name="item_image_{{ $i }}" id="item_image_{{ $i }}" value="{{ $items_1->image}}"></td>

                    @if($items_1->start_serial_number != '0')
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" onkeyup="updateQuantityStatus('{{ $i }}')" onchange="updateQuantityStatus('{{ $i }}')" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"readonly="readonly"  onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>


                    @else
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>

                    @if($model->status == 'processing' || $model->status == 'delivered' || $model->status == 'completed')
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')" readonly="readonly" onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @else
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"   @if($items_1->bundle_id!="") readonly="readonly" @endif onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @endif

                    @endif

                    @if(Auth::user()->role->role == 'client' || $is_order !== false)
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="calculatePrice('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                    @else
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="calculatePrice('{{ $i }}')" type="number"  readonly="readonly" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                    @endif
                    <td><input class="form-control" min ="0" step="any" type="number" style="width: 150px;" onkeyup="hideSuggestionPopup()" name="total_{{ $i }}" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>
                    <td>
                        <button onclick="showItemImage(this)" id="img_button_{{ $i }}" class="btn btn-success btn-sm" type="button"><i class="fa fa-image"></i></button>
                        @if($model->status != 'processing' && $model->status != 'delivered' && $model->status != 'completed')
                        @if($items_1->bundle_id=="")
                  
                        <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete" onclick="deleteRow(this,{{ $i }})">
                        
                          @elseif($items_1->bundle_id!="")
                          <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Bundle# {{ $items_1->bundle_id }}" onclick="deletebundleRow({{ $items_1->bundle_id }})">
                         @endif
                        @endif
                    </tr>


                    <?php $i++; ?>
                    @endif
                    @endforeach

                    @endif

                    </tbody>
                    <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                            <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
            <span class="text-red" id="err_mesg"></span>
            <div class="row" hidden="true">

                <div class="col-xs-8"></div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Total Amount for Purchase Order</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Total:</th>
                                <td id="po_total">$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            @include('front/common/image_modal')
            @include('front/common/bundle_view_modal')	
        </div>
        {!! Form::close() !!} 
        <!-- /.box-body -->
        @if($model->invoice_refrence_id > 0)
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Update / Sync Order">
        </div>
        @else
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Update Order">
        </div>
        @endif
        <!-- /.box-footer -->

    </div>

</section>
<script>
 checkcustomer();
                    function checkcustomer(){
                    
                        var client_id = $("#client_id option:selected").val();
                    
            if(client_id==0){
            //add disabled
            $("#bundle_id").attr('disabled', 'disabled');
            }else{
              $("#bundle_id").removeAttr("disabled");
            }
                    }  

 $('.select_bundle').on('change', function () {
                        var data = $(".select_bundle option:selected").val();
                        var customer_id = $("#client_id").val();
                        var type="order";
                       loadbundleAjax(data, j,customer_id,type);
                    });
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }

		function deletebundleRow(r) {

$("#item_body").find('input[name="bundle_id"]').each(function () {
    if ($(this).val() == r) {
        var id = $(this).closest("tr")[0].id;
        var row_id = id.substr(id.length - 1); // => "1"
        $("#quantity_" + row_id).val(0);
        $(this).closest("tr")[0].style.display = 'none';
    }
});
		}
</script>
@include('front/common/item_detail_modal')
@endsection