@extends('customer_c_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
       {{-- @include('front/common/client_common_tabs') --}}
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif


        <div class="box">
            <div class="box-header">
                {{-- <h3 class="box-title">Client Orders</h3> --}}
                {{-- @if($sync->is_order_sync == 0)
                <a href="{{ url('customer/order/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Order</a>
                @else
                <a href="{{ url('customer/sync/order/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Order</a>
                @endif
                <button class="btn btn-primary pull-right" onclick="showBulkUpdate(2)" style="margin-right: 5px;">Bulk Delivery Update</button>
                <button class="btn btn-primary pull-right" onclick="showBulkUpdate('orderPrint')" style="margin-right: 5px;">Bulk Print</button>
                <a href="" id="phiddenbtn" class="btn_print"></a> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                        <table id="orders01" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    {{-- <th><input type="checkbox" id="checkAll">Bulk Operations</th> --}}
                                    <th>Order ID</th>
                                    <th>Client Name</th>
                                    <th>Memo</th>
                                    <th>Amount</th>
                                    <th>Invoice Ref</th>
                                    <th>Profit</th>
                                    <th>Status</th>
                                    <!--<th>Created Date</th>-->
                                    <th>Statement Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($orders) >0)
                                @foreach($orders as $item)
                                <tr>

                                    {{-- @if($item->status == 'approved' || $item->status == 'delivered' || $item->status == 'approved_charged' || $item->status == 'processing')
                                    <td><input class="chk" type="checkbox" id="{{'check_box_'.$item['id']}}" value="{{ $item['id']}}" name="checks" onclick="SelectallidsForprint({{ $item['id']}});"></td>
                                    @else
                                    <td></td>
                                    @endif --}}
                                    @if($item->invoice_status != 'delivered' && $item->status != 'delivered')
                                    <td><a href="{{ url('customer/order/edit/'.$item->client_id) }}">{{$item->id}}</a></td>
                                    <td><a href="{{ url('customer/client/edit/'.$item->client_id) }}">{{$item->client_name}}</a></td>
                                    @else
                                    <td>{{$item->id}}</a></td>
                                    <td>{{$item->client_name}}</a></td>
                                    @endif
                                    <td>{{$item->memo}}</td>
                                    <td>{{$item->total_price}}</td>
                                    <td>
                                        @if($item->invoice_refrence > 0)<a href="{{ url('customer/invoice/'.$item->invoice_refrence) }}">
                                            {{$item->customer_name}}
                                            ( Invoice # {{$item->invoice_refrence}} )</a>
                                        @endif
                                    </td>
                                    <td>{{$item->profit}}</td>
                                    @if($item->status == 'pending')
                                    <td><a class="btn btn-warning btn-xs">
                                            @if($item->invoice_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                                                @endif
                                                Draft</a></td>
                                    @elseif($item->status == 'approved_charged')
                                    <td><a class="btn btn-success btn-xs">
                                            @if($item->invoice_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                                                @endif
                                                Client Charged</a></td>
                                    @elseif($item->status == 'approved')
                                    <td><a class="btn btn-success btn-xs">
                                            @if($item->invoice_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                                                @endif
                                                Inv Created (Admin)/Client Charged</a></td>
                                    @elseif($item->status == 'delivered')
                                    <td><a href="#my_modal" class="btn btn-primary btn-xs" data-toggle="modal" data-book-id="{{$item->id}}">
                                            @if($item->invoice_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                                                @endif
                                            </i>Delivered</a></td>
                                            @elseif($item->status == 'completed')
                                    <td><a class="label label-warning btn-xs">
                                            @if($item->invoice_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                                                @endif
                                            </i>Completed</a></td>
                                    @elseif($item->status == 'processing')
                                    <td><a class="label label-warning btn-xs">
                                            @if($item->invoice_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                                                @endif
                                                Processing</a></td>     
                                    @endif


                                    <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                                    <td>
                                        <a href="{{ url('customer/order/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                        @if( $item->status != 'delivered' )
                                        @if($item->status != 'completed')
                                        <a href="{{ url('customer/order/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        @endif
                                        @if($item->status == 'pending')
                                        <a href="{{ url('customer/order/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this Order?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                        @endif   
                                        @endif
                                        @if($item->status == 'approved' && $item->invoice_refrence_id > 0)
                                        <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-primary deliver">Deliver</a>
                                        @elseif($item->status == 'processing' || $item->status == 'approved' || $item->status == 'approved_charged')
                                        <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-primary deliver">Deliver</a>
                                        @elseif($item->status == 'delivered')
                                        <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-danger deliver"><i class="fa fa-undo" aria-hidden="true"></i>
                                        </a>
                                        @endif
                                        <a href="{{ url('customer/orderSkip/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i> Skip</a>
                                    </td>
                                </tr>

                            <script>

                                var item_id = '{{ $item->id }}';
                            </script>
                            @endforeach
                            @endif

                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <input type="hidden" id="selectedids" name="artistslist" value="" />
            @include('front/common/bulk_status_update_invoices_orders')


    </div>

</section>

<script>

    function change_status(item_id, status) {

    var url = "<?php echo url('customer/invoice/update-status'); ?>" + '/' + item_id + '/' + status;
    $.ajax({
    url: url,
            type: 'get',
            dataType: 'html',
            success: function (response) {
            $("select[name='po_id'").html('');
            $("select[name='po_id'").html(response);
            var $table = $('#item_body');
            var $table = $('#item_body');
            $table.find('tbody').empty();
            },
            error: function (xhr, status, response) {
            }
    });
    }
    function isNumber(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>

<script>
    var allids = [];
     
     function checkValue(value,arr){
       var status = 'Not exist';
      
       for(var i=0; i<arr.length; i++){
         var name = arr[i];
         if(name == value){
           status = 'Exist';
           break;
         }
       }
     
       return status;
     }
     
         function SelectallidsForprint(val) {
             
         if(allids != ''){
             var values = $("#selectedids").val().split(",");
     
             console.log('status : ' + checkValue(val, values) );
     
         var newValue = [];
     
     var ck = checkValue(val, values)
     if(ck == 'Exist'){
         for ( var i = 0 ; i < values.length ; i++ )
         {
             if ( val != values[i] )
             {
                 console.log(values[i])
                 newValue = newValue + values[i] + ",";
             }
         }
     
             $("#selectedids").val( newValue );
             }else{
             allids.push(val);
                 $("input[name=artistslist]").val(allids.join(','));
             }
     
         }else{
             allids.push(val);
             $("input[name=artistslist]").val(allids.join(','));
         }
     
         }
        $('#my_modal').on('show.bs.modal', function(e) {
    
        var bookId = $(e.relatedTarget).data('book-id');
        var status = 'orders';
        var url = "<?php echo url('invoice/getComments'); ?>" + '/' + bookId + '/' + status;
        $("#bookId").empty();
        $.ajax({
        url: url,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                var array = JSON.parse(response);
                var clients = array
    
                        clients.forEach(function(object) {
                        var option = '<div class="row"><div class="col-md-6" style="text-transform: capitalize;">By[ ' + object.name + ' ] On ' + object.created_at + '</br>' + object.status_type + ': ' + object.delivered_comments + '</div></div><hr>';
                        $("#bookId").append(option);
                        });
                },
                error: function (xhr, status, response) {
                }
        });
        });
        $('.deliver').click(function(){
        var id = $(this).data('id');
        var currentLocation = window.location;
        document.getElementById("updateStatusDeliver").value = id;
        document.getElementById("location").value = currentLocation.href;
        document.getElementById("table1").value = 'orders';
        })
    
                function gameCheck() {
                checked = $("input[class=chk]:checked").length;
                if (!checked) {
                alert('Please select atleast 1 item.');
                return false;
                }
                var currentLocation = window.location;
                document.getElementById("location1").value = currentLocation.href;
                document.getElementById("table").value = 'orders';
                return true;
                }
    //                $(function () {
    //    $('#orders').DataTable({
    //    "paging": true,
    //            "lengthChange": false,
    //            "searching": true,
    //            "ordering": false,
    //            "pageLength": {{Config::get('params.default_list_length')}},
    //            "info": false,
    //            "autoWidth": false
    //    });
    //    });
    
    $(function () {
                                            $('#orders01').DataTable({
                                            "paging": true,
                                                    "lengthChange": false,
                                                    "searching": false,
                                                    "ordering": false,
                                                    "info": false,
                                                    "pageLength": {{Config::get('params.default_list_length')}},
                                                    "autoWidth": false,
                                                    dom: 'Bfrtip',
                                                    buttons: [
                                                    {
                                                    extend: 'excelHtml5',
                                                            text: 'Export To Excel',
                                                            title: 'Report',
                                                    },
                                                    {
                                                    extend: 'pdfHtml5',
                                                            text: 'Export To PDF',
                                                            title: 'Report',
                                                    }
                                                    ]
                                            });
                                            });
    </script>
@endsection



