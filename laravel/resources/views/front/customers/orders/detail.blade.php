@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')
<?php
$modal_title = 'Profit';
?>

@section('content')
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                    @if($model->deleted == '2')
                    <i class="fa fa-hdd-o"></i> Price Change Order # {{ $model->id }}
                    @else
                    <i class="fa fa-hdd-o"></i> Order # {{ $model->id }}
                    @endif
                <small class="pull-right">Statement Date: <?php echo date("d M Y", strtotime($model->statement_date)); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <b>Client Name: {{ $model->client_name }}</b><br>
            @if($model->invoice_refrence_id != '0')
             @if( Auth::user()->role_id == '4')
            <b>Sync Invoice # <a href="{{ url('customer/invoice/'.$model->invoice_refrence_id) }}">{{$model->invoice_refrence_id}}</a></b><br>
            @else
            <b>Sync Invoice # <a href="{{ url('invoice/'.$model->invoice_refrence_id) }}">{{$model->invoice_refrence_id}}</a></b><br>
             @endif
            @endif
            <b>Bill To:</b> {{ $model->client_address }}<br>
            <b>Phone #:</b>{{ $model->client_phone }}<br>
            <!--          <b>Account:</b> 968-34567-->
            <br>
            <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <!--          To
                      <address>
                        <strong>John Doe</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone: (555) 539-1037<br>
                        Email: john.doe@example.com
                      </address>-->
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

            @if($model->status == 'pending')
            @if($model->invoice_refrence > 0)
            <i class="fa fa-refresh" aria-hidden="true">
                @endif
                Order Status: <b class = "label label-warning btn-xs">Draft</b><br>
                @elseif($model->status == 'approved_charged')
                @if($model->invoice_refrence > 0)
                <i class="fa fa-refresh" aria-hidden="true">
                    @endif
                    Order Status: <b class = "label label-success btn-xs">Client Charged</b><br>
                    @elseif($model->status == 'approved')
                    @if($model->invoice_refrence > 0)
                    <i class="fa fa-refresh" aria-hidden="true">
                        @endif
                        Order Status: <b class = "label label-success btn-xs"> Inv Created (Admin) / Client Charged</b><br>
                        @elseif($model->status == 'delivered')
                        @if($model->invoice_refrence > 0)
                        <i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Order Status: <b class = "label label-primary btn-xs"> Delivered</b><br>

                            @elseif($model->status == 'processing')
                            @if($model->invoice_refrence > 0)
                            <i class="fa fa-refresh" aria-hidden="true">
                                @endif
                                Order Status: <b class = "label label-primary btn-xs"> Delivered</b><br>
                                @endif

                                   @if( $model->status != 'delivered' && Auth::user()->role_id == '4' )
                                        <a href="{{ url('customer/order/edit/'.$model->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                        @endif
                                    @if($model->client_id != 0 && $model->status == 'pending')
                                    <button type="button" class="btn btn-success btn-sm" onclick="showModal()"><i class="fa fa-check-square"></i> Approve</button>
                                    @endif
                                  
                                    Print Bundle in Group
                                    <input type="checkbox" name="print_type" id="print_type" @if($model->print_type=="bundle_item") checked @endif>
                                    <br>
                                   @if( Auth::user()->role_id == '4')
                                    <a href="{{url('customer/order-print/'.$model->id)}}" class="btn btn-success btn_print btn-sm"><i class="fa fa-print"></i> Print</a>
                                   @else
                                   <a href="{{url('ref/order/print/'.$model->id)}}" class="btn btn-success btn_print btn-sm"><i class="fa fa-print"></i> Print</a>
                                   @endif
                                   
                                    <script src="{{ asset('front/js/jquery.printPage.js')}}"></script>
                                    <script>
                                        $(document).ready(function () {
                                        $(".btn_print").printPage();
                                        });
                                    </script>
                                    @if(Auth::user()->role_id == 4)
                                    @if($show_profit == 0)
                                    <a href="{{ url('customer/order/'.$model->id.'/1') }}" class="btn btn-primary btn-sm">Show Details</i></a>
                                    @else
                                    <a href="{{ url('customer/order/'.$model->id) }}" class="btn btn-danger btn-sm">Hide Details</i></a>
                                    @endif
                                    @else
                                    @if($show_profit == 0)
                                    <a href="{{ url('ref/order/detail/'.$model->id.'/1') }}" class="btn btn-primary btn-sm">Show Details</i></a>
                                    @else
                                    <a href="{{ url('ref/order/detail/'.$model->id) }}" class="btn btn-danger btn-sm">Hide Details</i></a>
                                    @endif       
                                    @endif
                                    <br>
                                    <br>
                                    </div>
                                    <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <!-- Table row -->
                                    <div class="row">
                                        <div class="col-xs-12 table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>Start Serial #</th>
                                                        <th>End Serial #</th>
                                                        <th>QTY</th>
                                                        <!--<th>Delivered QTY</th>-->
                                                        <th>Unit Price</th>
                                                        <th>Total Price</th>
                                                        @if($show_profit == 1)
                                                        <th>Total Cost</th>
                                                        <th>Profit</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($po_items)>0)
                                                    @foreach($po_items as $items_1)
                                                    <?php 
                                                    
                                                    if($items_1->bundle_name != ''){
                               $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                              $items_1->item_name = $items_1->item_name . $bundle_detail;
                          }
                                                    ?>
                                                    <tr>
                                                        @if($items_1->image == '')
                                                        <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                                                        @else
                                                        <td><a href='#' onclick="showItemGallery('{{$items_1->image}}','{{$items_1->item_id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"></a></td>
                                                        <!-- <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"></a></td> -->
                                                        @endif
                                                        <td>{{ $items_1->item_name }}</td>
                                                        <td>{{ $items_1->start_serial_number }}</td>
                                                        <td>{{ $items_1->end_serial_number }}</td>
                                                        <td>{{ $items_1->quantity }}</td>
                                                        <!--<td>{{ $items_1->delivered_quantity }}</td>-->
                                                        <td>{{ $items_1->item_unit_price }}</td>
                                                        <td>{{ $items_1->total_price }}</td>
                                                        @if($show_profit == 1)
                                                        <td>{{ $items_1->total_cost }}</td>
                                                        <td>{{ $items_1->profit }}</td>
                                                        @endif
                                                    </tr>

                                                    @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <div class="row">
                                        <!-- accepted payments column -->
                                        <div class="col-xs-6">
                                <!--          <p class="lead">Payment Methods:</p>
                                        
                                
                                          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                                            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                                          </p>-->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-xs-6">
                                          <!--<p class="lead">Amount Due 2/22/2014</p>-->

                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody><tr>
                                                            <th style="width:50%">Total Quantity:</th>
                                                            <td>{{ $model->total_quantity }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Total Price</th>
                                                            <td>{{ $model->total_price }}</td>
                                                        </tr>
                                                        @if($show_profit == 1)
                                                        <tr>
                                                            <th>Total Cost</th>
                                                            <td>{{ $model->total_cost }}</td>
                                                        </tr>

                                                        <tr>
                                                            <th>Profit</th>
                                                            <td>{{ $model->profit }}</td>
                                                        </tr>
                                                        @endif
                                                    </tbody></table>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <!-- this row will not appear when printing -->
                                    <div class="row no-print">
                                        <div class="col-xs-12">
                                            @if($model->status == 'pending')
                                            <a href="{{ url('customer/order/edit/'.$model->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> Edit Client Order</a>
                                            @endif
                                  <!--          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                                            </button>
                                            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                              <i class="fa fa-download"></i> Generate PDF
                                            </button>-->
                                        </div>
                                    </div>
                                    </section>
                                    @include('front/common/image_modal')
                                    <div class="modal" id="myModal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button"  id="cross"  class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title">Update Order Status</h4>
                                                </div>
                                                @if(1==1)
                                                <div class="modal-body">
                                                    <p>Update the order status by choose any 1 option from below.</p>
                                                </div>
                                                @endif
                                                <div class="modal-footer">
                                                    <a id="closemodal" class="btn btn-danger pull-left" href="{{ url('customer/order-send/invoice/'.$model->id) }}">Charged Client and Create Invoice</a>
                                                    <a id="closemodal" class="btn btn-success" href="{{ url('customer/order-send/0/'.$model->id) }}">Charge Client</a>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <script type="text/javascript">

                                        function showModal(){
                                        var modal = document.getElementById('myModal');
                                        modal.style.display = "block";
                                        }

                                        var cross = document.getElementById("cross");
                                        cross.onclick = function () {
                                        var modal = document.getElementById('myModal');
                                        modal.style.display = "none";
                                        }


                                        function showProfitModal(){

                                        var modal = document.getElementById('modal_profit_new');
                                        modal.style.display = "block";
                                        }

                                        $("#print_type").change(function() {
    if(this.checked) {
     var status="bundle_item "
    }else{
        var status='Normal'
    }
    var client_id={{ $model->client_id }};
    $.ajax({
                url: "<?php echo url('customer/order/printType'); ?>" + '/' + status + '/' + client_id,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    // if (response != 0) {
                    //     var modal = document.getElementById('modal_bundle');
                    //     modal.style.display = "block";
                    //     $(modal_bundle).find('tbody').empty();
                    //     $(modal_bundle).find('tbody').append(response);
                    // }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
});
                                    </script>
                                    
                                    <!-- @include('front/common/image_modal') -->
                                    @endsection