@extends('customer_c_template') 

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Activity Logs</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vendors" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Log ID</th>
                    <th>Client Name</th>
                    <th>Item Name</th>
                    <th>Increase/Decrease</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                
                    @if(count($modal) >0)
                    @foreach($modal as $item)
                    <tr>
                    <td>{{$item->id}}</td>
                    @if(isset($item->name))
                    <td>{{$item->name}}</td>
                    @else
                    <td>N/A</td>
                    @endif
                    <td>{{$item->item_name}}</td>
                    <td>{{$item->item_price}}</td>
                    <td>{{$item->created_at}}</td>
                    <td>{{$item->updated_at}}</td>
                    <td>{{$item->status}}</td>
                </tr>
                    @endforeach                  
                    @endif
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>

<script>
  $(function () {
    $('#vendors').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "pageLength": {{Config::get('params.default_list_length')}},
      "autoWidth": false
    });
  });
</script>
@endsection



