@extends('login_template')
@section('content')

		  
		  <div class="login-box">
  <div class="login-logo">
    <a href="#"><b> Inventory Management System</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
    <form id="loginform" class="login__form"  method="POST" action="{{ url('postLogin') }}" >
         <input type="hidden"  name="_token" value="{{ csrf_token() }}">
      <div class="form-group has-feedback">
          <input type="text"  class="form-control" name="email" placeholder="Username" required="">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
 <div class="form-group / in-view">
                            </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" onclick="document.getElementById('loginform').submit();" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <br>
       
          
        <!-- /.col -->
      </div>
         
    </form>


  </div>
  <!-- /.login-box-body -->
</div>
		   
	
@endsection

