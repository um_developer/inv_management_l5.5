@extends('customer_template_2')

@section('content')

<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Create Vendor</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                    
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'postchangepassword', 'method' => 'post')) !!}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Current Password</label>

                  <div class="col-sm-10">
                   {!! Form::password('old_password', ['class'=>'form-control','placeholder'=>'Current Password']) !!}
                  </div>
                </div>

                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Update Password</label>

                  <div class="col-sm-10">
                    {!! Form::password('password', ['class'=>'form-control','placeholder'=>'New Password']) !!}
                  </div>
                </div>
                  
               
                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Confirm Password</label>

                  <div class="col-sm-10">
                   {!! Form::password('password_confirmation', ['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
                  </div>
                </div>
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
        			
</section>

@endsection