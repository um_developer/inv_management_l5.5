@extends('customer_c_template')

@section('content')


<section class="prod-area bg-cvr" >

    <div class="row">
        <section class="content-header">
            <h1>
                Dashboard
                <!--<small>Version 2.0</small>-->
            </h1><br>
            <ol class="breadcrumb">
                <li><a href="{{ url('/customer/report/client')}}"><i class="fa fa-dashboard"></i></a>Home</li>
                <li class="active">Dashboard</li>
            </ol>

        </section>
    </div>

    <div class="row">

        <!-- ./col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-stats-bars"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">DAILY SALES</span>
                    <span class="info-box-number">{{ $daily_sales }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>      

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-pie-graph"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">DAILY PROFIT</span>
                    <span class="info-box-number">{{ $daily_profit }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>     

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-stats-bars"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">MONTHLY SALES</span>
                    <span class="info-box-number">{{ $month_sales }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>      

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-pie-graph"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">MONTHLY PROFIT</span>
                    <span class="info-box-number">{{ $month_profit }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>     

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="ion ion-pie-graph"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">CLIENT BALANCE</span>
                    <span class="info-box-number">{{ $client_balance }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>  
        <div class="col-md-3 col-sm-6 col-xs-12">

            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="ion ion-cash"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">ORDER 0/- PROFIT </span>
                    <a href="{{ url('/customer/zeroprofit')}}">
                        <span class="info-box-number">{{ count($zero_profit_order_count) }}</span>
                    </a>
                </div>

                <!-- /.info-box-content -->
            </div>

            <!-- /.info-box -->
        </div>   


    </div>

    <div class="row">
        @if(1==2)
        <section class="content-header">
            <h1>
                Dashboard
        <!--        <small>Version 2.0</small>-->
            </h1><br>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        @endif

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$balance[0]->balance}}</h3>

                    <p>Current Balance</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/customer/payments')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{$invoice_count}}</h3>

                    <p>Invoices</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/customer/invoices')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$credit_memo_count}}</h3>

                    <p>Credit Memos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/customer/credit-memo')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$item_count}}</h3>

                    <p>Items</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/customer/items_list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$order_count}}</h3>

                    <p>Orders</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/customer/orders')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$return_order_count}}</h3>

                    <p>Return Orders</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/customer/order-returns')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        {{-- <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>{{$total_sales}}</h3>

        <p>Total Sales</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person"></i>
                            </div>
                            <a href="{{ url('/customer/report/client/sale-profit')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div> --}}

                    {{-- <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>{{$total_profit}}</h3>

                <p>Total Profit</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-person"></i>
                                        </div>
                                        <a href="{{ url('/customer/report/client/sale-profit')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div> --}}

                            </div>

                            <br><br>
                            @if(1==1)
                            <div class="row">

                                <div class="col-xs-12 col-md-6">
                                    <div class="box box-success">
                                        <div class="box-header">
                                            <h3 class="box-title">Orders not Delivered</h3>

                                        </div><!-- /.box-header -->
                                        <div class="box-body table-responsive no-padding">

                                            <table class="table table-hover">
                                                <tbody><tr>

                                                        <th>Client</th>
                                                        <th>Amount</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    @if(count($approved_orders) > 0)
                                                    @foreach($approved_orders as $item)
                                                    <tr>
                                                        <td>{{ $item->client_name }}</td>
                                                        <td>{{ $item->total_price }}</td>
                                                        <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>

                                                        <td>
                                                            <a href="{{ url('customer/order/'.$item->id) }}" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-external-link"></i> Detail</a>
                                                            <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-primary deliver btn-xs">Deliver</a> 

                                                        </td>

                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody></table>
                                        </div><!--/.box-body -->

                                    </div><!--/.box -->
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <div class="box box-success">
                                        <div class="box-header">
                                            <h3 class="box-title">Orders not send to Admin</h3>

                                        </div><!-- /.box-header -->
                                        <div class="box-body table-responsive no-padding">

                                            <table class="table table-hover">
                                                <tbody><tr>

                                                        <th>Client</th>
                                                        <th>Amount</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    @if(count($pending_orders) > 0)
                                                    @foreach($pending_orders as $item)
                                                    <tr>
                                                        <td>{{ $item->client_name }}</td>
                                                        <td>{{ $item->total_price }}</td>
                                                        <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                                                        <td><a href="{{ url('customer/order/'.$item->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-external-link"></i> Detail</a></td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody></table>
                                        </div><!--/.box-body -->

                                    </div><!--/.box -->
                                </div>
                                <!-- cfscfd -->

                                <div class="col-xs-12 col-md-6">
                                    <div class="box box-success">
                                        <div class="box-header">
                                            <h3 class="box-title">Return Orders not send to Admin</h3>

                                        </div><!-- /.box-header -->
                                        <div class="box-body table-responsive no-padding">

                                            <table class="table table-hover">
                                                <tbody><tr>

                                                        <th>Client</th>
                                                        <th>Amount</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    @if(count($pending_return_orders) > 0)
                                                    @foreach($pending_return_orders as $item)
                                                    <tr>
                                                        <td>{{ $item->client_name }}</td>
                                                        <td>{{ $item->total_price }}</td>
                                                        <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                                                        <td><a href="{{ url('customer/order-return/'.$item->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-external-link"></i> Detail</a></td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody></table>
                                        </div><!--/.box-body -->

                                    </div><!--/.box -->
                                </div>


                                <!-- haere -->

                            </div>
                            @endif  
                            @include('front/common/bulk_status_update_invoices_orders')
                            </section>

                            <script>
                                $('.deliver').click(function () {
                                    var id = $(this).data('id');
                                    var currentLocation = window.location;
                                    document.getElementById("updateStatusDeliver").value = id;
                                    document.getElementById("location").value = currentLocation.href;
                                    document.getElementById("table1").value = 'orders';
                                })
                            </script>


                            @endsection
