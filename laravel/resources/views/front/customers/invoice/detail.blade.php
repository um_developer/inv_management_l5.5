@extends('customer_c_template')

@section('content')
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-hdd-o"></i> Invoice # {{ $model->id }}
                <small class="pull-right">Statement Date: <?php echo date("d M Y", strtotime($model->statement_date)); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <b>Invoice # {{ $model->id }}</b><br>
            <br>
            <b>Customer Name:</b> {{ $model->customer_name }}<br>
          
            <!--          <b>Account:</b> 968-34567-->
            <br>
            <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <!--          To
                      <address>
                        <strong>John Doe</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone: (555) 539-1037<br>
                        Email: john.doe@example.com
                      </address>-->
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

            @if($model->status == 'rejected')
            Invoice Status: <b class = "label label-danger btn-xs">{{ ucwords($model->status) }}</b><br>
            @elseif($model->status == 'approved')
            Invoice Status: <b class = "label label-success btn-xs">{{ ucwords($model->status) }}</b><br>
            @elseif($model->status == 'delivered')
            Invoice Status: <b class = "label label-primary btn-xs">{{ ucwords($model->status) }}</b><br>
            @else
            Invoice Status: <b class = "label label-warning btn-xs"> {{ ucwords($model->status) }}</b><br>
            @endif

            @if(1==1)
            <a href="{{url('customer/invoice-print/'.$model->id)}}" class="btn btn-success btn_print btn-sm"><i class="fa fa-print"></i> Print</a>
            <script src="{{ asset('front/js/jquery.printPage.js')}}"></script>
            <script>
$(document).ready(function () {
$(".btn_print").printPage();
});
            </script>
            @endif
            <br>
            <br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Start Serial #</th>
                        <th>End Serial #</th>
                        <th>QTY</th>
                        <!--<th>Delivered QTY</th>-->
                        <th>Unit Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($po_items)>0)
                    @foreach($po_items as $items_1)
                    <?php 
                    if($items_1->bundle_name != ''){
                         $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                         $items_1->item_name = $items_1->item_name . $bundle_detail;
                    }
                    ?>
                    <tr>
                        @if($items_1->image == '')
                        <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                        @else
                        <td><a href='#' onclick="showItemGallery('{{$items_1->image}}','{{$items_1->item_id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"></a></td>
                        <!-- <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"></a></td> -->
                        @endif
                        <td>{{ $items_1->item_name }}</td>
                        <td>{{ $items_1->start_serial_number }}</td>
                        <td>{{ $items_1->end_serial_number }}</td>
                        <td>{{ $items_1->quantity }}</td>
                        <!--<td>{{ $items_1->delivered_quantity }}</td>-->
                        <td>{{ $items_1->item_unit_price }}</td>
                        <td>{{ $items_1->total_price }}</td>
                    </tr>

                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
<!--          <p class="lead">Payment Methods:</p>
        

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>-->
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!--<p class="lead">Amount Due 2/22/2014</p>-->

            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                            <th style="width:50%">Total Quantity:</th>
                            <td>{{ $model->total_quantity }}</td>
                        </tr>
                        <tr>
                            <th>Total Price</th>
                            <td>{{ $model->total_price }}</td>
                        </tr>

                    </tbody></table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            @if($model->status == 'draft')
            <a href="{{ url('purchase-order/edit/'.$model->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> Edit Purchase Order</a>
            @endif
  <!--          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
            </button>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              <i class="fa fa-download"></i> Generate PDF
            </button>-->
        </div>
    </div>
</section>
@include('front/common/image_modal')
@endsection