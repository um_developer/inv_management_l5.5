@extends('customer_c_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Invoice</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/invoice/create','id' => 'invoice_form', 'method' => 'post')) !!}
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', Request::input('memo') , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                    <input type="hidden"  name="package_id" value="0">
                    <input type="hidden" id="customer_id_2"  name="customer_id" value="{{ $customer_id }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>
                
                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select List Title</label>
                    {!!   Form::select('list_id', $list_words, Request::input('list_id'), array('class' => 'form-control','id' => 'list_id' ))  !!}

                </div>
               
                @if($show_bundle==1)
                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select2 select_bundle','id' => 'bundle_id' ))  !!}

               </div>
               @endif
                
            </div>
            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>
            
             @include('front/common/invoice_js')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody></tbody>
                   <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                        <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
            <span class="text-red" id="err_mesg"></span>
        </div>
        {!! Form::close() !!} 
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Create Invoice">
        </div>
    </div>
       
        @include('front/common/image_modal')
        @include('front/common/bundle_view_modal')	
</section>
    <script>

        $(function () {
//            Bootstrap DateTimePicker v4
            $('#datetimepicker4').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });

        $(document).ready(function () {

            $('#datetimepicker4').data("DateTimePicker").date(moment(new Date()).format('MM/DD/YYYY'));


        });
        
			  
            $('.select_bundle').on('change', function () {
                        var data = $(".select_bundle option:selected").val();
                        var customer_id = $("#customer_id_2").val();
                        var type="invoice";
                       loadbundleAjax(data, j,customer_id,type);
                    });
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }
    </script>
@include('front/common/item_detail_modal')
@endsection