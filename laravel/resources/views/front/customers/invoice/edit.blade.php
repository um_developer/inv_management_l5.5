@extends('customer_c_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    @include('front/common/create_item_modal')
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Edit Invoice # {{ $model->id }}</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/invoice/update','id' => 'invoice_form', 'method' => 'post')) !!}

        <input type="hidden"  name="id" value="{{ $model->id }}">
        <input type="hidden"  name="package_id" value="0">
        <input type="hidden" id="customer_id_2"  name="customer_id" value="{{ $customer_id }}">
        <div class="box-body">

            @if(1==2)
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Select Customer</label>
                    {!!   Form::select('customer_id', $vendors, $model->customer_id, array('class' => 'form-control',$required, 'disabled' => true ))  !!}
                </div>
            </div>
            @endif
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Memo</label>
                    {!! Form::text('memo', $model->memo , array('placeholder'=>"Memo",'maxlength' => 300,'class' => 'form-control') ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Select Statement Date</label>
                    <div class='input-group date' id='datetimepicker4'>
                        <input type='text' id="receive_date" name="statement_date" class="form-control" value="" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div></div>

                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select List Title</label>
                    {!!   Form::select('list_id', $list_words, $model->list_id, array('class' => 'form-control','id' => 'list_id' ))  !!}

                </div>
                @if($show_bundle==1)
                <div class="col-sm-3">
                    <label for="exampleInputEmail1">Select Bundle #</label>
                    {!!   Form::select('bundle_id', $bundles, Request::input('bundle_id'), array('class' => 'form-control select2 select_bundle','id' => 'bundle_id' ))  !!}

               </div>
               @endif

            </div>

            <a href="#" id="add_more" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Insert Item</a><br><br>

            @include('front/common/invoice_edit_js')

            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered table-striped" id="item_body">
                    <thead>
                        @include('front/common/item_table_heading')
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        ?>
                        @if(count($po_items)>0 && isset($po_items[0]))
                        @foreach($po_items as $items_1)
                        
                        <?php
                        if($items_1->bundle_name != ''){
                               $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                              $items_1->item_name = $items_1->item_name . $bundle_detail;
                          }
                        $delivered_items = $items_1->delivered_quantity;
                        $validation_num = 'class=form-control type=number min =' . $delivered_items . ' step=any';
//                        $validation_num = '';
                        ?>
                        @if(1==1)
                        <tr id="item_{{ $i }}">
                            <td><input class="form-control" type="text" title= "{{$items_1->item_name}}" name="item_name_{{ $i }}" id="item_name_{{ $i }}" readonly="readonly"  autocomplete="on" placeholder="Enter item name" value="{{$items_1->item_name}}">
                                <input type="hidden" readonly="readonly" name="item_id_{{ $i }}" id="item_id_{{ $i }}"  value="{{$items_1->item_id}}">
                                <input type="hidden" readonly="readonly" name="po_item_id_{{ $i }}" id="po_item_id_{{ $i }}"  value="{{$items_1->id}}">
                                <input type="hidden" name="num[]" id="num[]">
                                <input type="hidden"  name="bundle_id" value="{{ $items_1->bundle_id}}">
                            </td>
                    <input type="hidden" name="item_image_{{ $i }}" id="item_image_{{ $i }}" value="{{ $items_1->image}}"></td>

                    @if($items_1->start_serial_number != '0')
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" onkeyup="updateQuantityStatus('{{ $i }}')" onchange="updateQuantityStatus('{{ $i }}')" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"readonly="readonly"  onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @else
                    <td><input class="form-control" min ="0" type="number" name="start_serial_number_{{ $i }}" readonly="readonly" id="start_serial_number_{{ $i }}" value="{{$items_1->start_serial_number}}"></td>
                    <td><input class="form-control" min ="0" type="number" name="end_serial_number_{{ $i }}" readonly="readonly" id="end_serial_number_{{ $i }}" value="{{$items_1->end_serial_number}}"></td>
                    <td><input {{ $validation_num }} onkeypress="calculatePrice('{{ $i }}')"  @if($items_1->bundle_id!="") readonly="readonly" @endif onchange="calculatePrice('{{ $i }}')" name="quantity_{{ $i }}" id="quantity_{{ $i }}" value="{{$items_1->quantity}}"></td>
                    @endif

                    @if(Auth::user()->role->role == 'client')
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="calculatePrice('{{ $i }}')" type="number" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                    @else
                    <td><input class="form-control" min ="{{$items_1->cost}}" step="any" onkeyup="showSuggestionPopup('{{ $i }}')" style="width: 150px;" onchange="calculatePrice('{{ $i }}')" type="number"  readonly="readonly" name="price_{{ $i }}" id="price_{{ $i }}" value="{{$items_1->item_unit_price}}" ></td>
                    @endif
                    <td><input class="form-control" min ="0" step="any" type="number" style="width: 150px;" onkeyup="hideSuggestionPopup()" name="total_{{ $i }}" readonly="readonly" id="total_{{ $i }}"value="{{$items_1->total_price}}"></td>
                    <td>
                        <button onclick="showItemImage(this)" id="img_button_{{ $i }}" class="btn btn-success btn-sm" type="button"><i class="fa fa-image"></i></button>
                      
                        @if($items_1->bundle_id=="")
                  
                  <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete" onclick="deleteRow(this,{{ $i }})">
                  
                    @elseif($items_1->bundle_id!="")
                    <input id="delete_btn_{{ $i }}" name="delete_btn_{{ $i }}" class="btn btn-danger btn-sm" type="button" value="Delete Bundle# {{ $items_1->bundle_id }}" onclick="deletebundleRow({{ $items_1->bundle_id }})">
                   @endif
                </td> 
                    </tr>


                    <?php $i++; ?>
                    @endif
                    @endforeach

                    @endif

                    </tbody>
                    <tfoot><tr><td></td><td></td><td></td><td></td><th>TOTAL</th>
                            <th id="footer_total"></th><td></td></tr></tfoot>
                </table>
            </div>
            <span class="text-red" id="err_mesg"></span>
            <div class="row" hidden="true">

                <div class="col-xs-8"></div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <p class="lead">Total Amount for Purchase Order</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Total:</th>
                                <td id="po_total">$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            @include('front/common/image_modal')
            @include('front/common/bundle_view_modal')	
        </div>
        {!! Form::close() !!} 
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="button" name="submit" id="submit_btn" class="btn btn-primary pull-right" onclick="validateSerialItem('submit');" value="Update Invoice">
        </div>
        <!-- /.box-footer -->

    </div>

</section>
<script>
 $('.select_bundle').on('change', function () {
                        var data = $(".select_bundle option:selected").val();
                        var customer_id = $("#customer_id_2").val();
                        var type="invoice";
                       loadbundleAjax(data, j,customer_id,type);
                    });
        function loadbundleAjax(bundle_id, j,customer_id,type) {
            $.ajax({
                url: "<?php echo url('invoice/create/bulkbundle/'); ?>" + '/' + bundle_id + '/' + j+'/'+customer_id+'/'+type,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 0) {
                        var modal = document.getElementById('modal_bundle');
                        modal.style.display = "block";
                        $(modal_bundle).find('tbody').empty();
                        $(modal_bundle).find('tbody').append(response);
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }

</script>
@include('front/common/item_detail_modal')
@endsection