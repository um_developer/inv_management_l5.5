@extends('customer_c_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        
         @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                    
                   
                    {!! Form::open(array( 'class' => '','url' => 'customer/invoice/search', 'method' => 'get')) !!}
                   
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-3">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div></div>
                        <div class="form-group col-sm-4" id="type">

                            <select id="type" name="type" class="form-control">
                                <option value="" <?php echo ($selected_report_type == '') ? 'selected' : ''; ?>>Select Status</option>        
                                <option value="pending" <?php echo ($selected_report_type == 'pending') ? 'selected' : ''; ?>>Pending</option>
                                <option value="approved" <?php echo ($selected_report_type == 'approved') ? 'selected' : ''; ?>>Approved</option>
                                 <option value="processing" <?php echo ($selected_report_type == 'processing') ? 'selected' : ''; ?>>Processing</option>
                                 <option value="delivered" <?php echo ($selected_report_type == 'delivered') ? 'selected' : ''; ?>>Delivered</option>
                            </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <input id="invoice_number" onkeypress="return isNumber(event)" type="text" name="invoice_number" value="{{$selected_invoice_number}}" placeholder="Invoice Number" class="form-control">
                            </div>   
                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
        $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>


                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                               <a href="{{ URL::to('credit-memo') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                   
                </div>
            </div>
        </div>
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Invoices</h3>
                <a href="{{ url('customer/invoice/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Invoice</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                <div class="col-md-12">

                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Invoice ID</th>
                            <!--<th>Customer Name</th>-->
                            <th>Memo</th>
                            <th>Amount</th>
                            <th>Order Refrence</th>
                            <th>Status</th>
                            <!--<th>Created Date</th>-->
                            <th>Statement Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td><a href="{{ url('customer/invoice/'.$item->id) }}">{{$item->id}}</a></td>
                            <!--<td><a href="{{ url('customer/edit/'.$item->customer_u_id) }}">{{$item->customer_name}}</a></td>-->
                            <td>{{$item->memo}}</td>
                            <td>{{$item->total_price}}</td>
                            <td>
                            @if($item->order_refrence > 0)
                            <a href="{{ url('customer/order/'.$item->order_refrence) }}">
                            {{$item->client_name}}
                                ( Order # {{$item->order_refrence}} ) </a>
                            @endif
                            </td>
                            @if($item->status == 'pending')
                            <td><a class="btn btn-warning btn-xs"> 
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Pending</a></td>
                            @elseif($item->status == 'approved')
                            <td><a class="btn btn-success btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Approved</a></td>
                            @elseif($item->status == 'delivered')
                            <td><a class="btn btn-primary btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Delivered</a></td>
                            @elseif($item->status == 'processing')
                            <td><a class="label label-warning btn-xs">
                            @if($item->order_refrence > 0)<i class="fa fa-refresh" aria-hidden="true">
                            @endif
                            Process</a></td>
                            @else
                            <td><a class="btn btn-danger btn-xs"> Rejected</a></td>
                            @endif
                            <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                            <td>
                                <a href="{{ url('customer/invoice/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                @if($item->status == 'pending')
                                <a href="{{ url('customer/invoice/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('customer/invoice/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this Invoice?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                
                              
                                @endif
                                
 
<!--                                <select id="invoice_status_{{ $item->id }}" name="{{ $item->id }}" class="form-control">
                                    <option id="{{ $item->id }}">Approve</option>
                                    <option>Reject</option>

                                </select>-->
                            </td>
                        </tr>

                    <script>

                        var item_id = '{{ $item->id }}';
//                        var invoice_select = document.getElementById('invoice_status_{{ $item->id }}');
//                        invoice_select.addEventListener('change', function () {
//                            change_status(this.name, this.value);
//                        }, false);
                    </script>
                    @endforeach

                    @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

                </div></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>

    function change_status(item_id, status) {

        alert(item_id);
        alert(status);
        
        
         var url = "<?php echo url('customer/invoice/update-status'); ?>" + '/' + item_id + '/' + status;
                    $.ajax({
                        url: url,
                        type: 'get',
                        dataType: 'html',
                        success: function (response) {
                            $("select[name='po_id'").html('');
                            $("select[name='po_id'").html(response);
                            var $table = $('#item_body');
                            var $table = $('#item_body');
                            $table.find('tbody').empty();

                        },
                        error: function (xhr, status, response) {
                        }
                    });


    }

    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
        });
    });

    function isNumber(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
@endsection



