@extends('customer_template')

@section('content')


<section class="prod-area bg-cvr" >
    
    
     <div class="row">
        <section class="content-header">
            <h1>
                Dashboard
                <!--<small>Version 2.0</small>-->
            </h1><br>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>

        </section>
    </div>
    
     <div class="row">
       
        <!-- ./col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-stats-bars"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">DAILY SALES</span>
                    <span class="info-box-number">{{ $daily_sales }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>      
        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-pie-graph"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">DAILY PROFIT</span>
                   <span class="info-box-number">{{ $daily_profit }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>     
        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-stats-bars"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">MONTHLY SALES</span>
                    <span class="info-box-number">{{ $month_sales }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>      
        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-pie-graph"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">MONTHLY PROFIT</span>
                    <span class="info-box-number">{{ $month_profit }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>  
        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="ion ion-cash"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">CUSTOMER BALANCE</span>
                   <span class="info-box-number">{{ $customer_balance }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>     
        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="ion ion-cash"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">VENDOR BALANCE</span>
                   <span class="info-box-number">{{ $vendor_balance }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
       
        <div class="col-md-3 col-sm-6 col-xs-12 hidden">
            <a href="{{ url('/zeropurchaseorder')}}">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="ion ion-cash"></i></span>
                
                    <div class="info-box-content">
                        <span class="info-box-text">Purchase Orders</span>
                    <span class="info-box-number">{{ count($from_perchase_zero_profit_invoice_count) }}</span>
                    </div>
                
                <!-- /.info-box-content -->
            </div>
        </a>
            <!-- /.info-box -->
        </div>
        
    </div>
    
    <div class="row">
       
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$vendor}}</h3>

                    <p>Vendors</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/vendors')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$customer}}</h3>

                    <p>Customers</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/customers')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$po}}</h3>

                    <p>Open Purchase Orders</p>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="{{ url('/purchase-orders')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{$ro}}</h3>

                    <p>Receive Orders</p>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="{{ url('/receive-items')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$items}}</h3>

                    <p>Items</p>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="{{ url('/items')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{$invoice}}</h3>

                    <p>Invoices</p>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="{{ url('/invoices')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$credit_memo}}</h3>

                    <p>Credit Memos</p>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="{{ url('/credit-memo')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        
         <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{count($zero_profit_invoice_count)}}</h3>

                    <p>Invoice with 0 Profit Items</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/zeroprofit')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$no_po_items}}</h3>

                    <p>Items without PO</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/items-with-no-po')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{count($itemcount)}}</h3>

                    <p>Non Tag items</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/itemTags')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{$priceImpect}}</h3>

                    <p>Price impect items</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{ url('/priceImpect')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        
    </div>
    
    <br><br>
     <style type="text/css">
      .scrollable {
        height: 400px;
        overflow-y: scroll;
      width: 33%; /* as @passatgt mentioned in the comment*/
  width: calc(100% / 2  );
  display: inline-block;
      }
    </style>
    <div class="row">

@if(1==1)
        @if(count($delivered_orders) > 10)
            <div class="col-xs-4 " style="width: 100% !important;">
        @else
            <div class="col-xs-4 " style="width: 100% !important;" >
        @endif
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Pending Delivered Invoices</h3>

                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <div class="box-body">
                        <table id="devinvoice" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                
                                <th>Customer</th>
                                <th>city</th>
                                <th>state</th>
                                <th>Amount</th>
                                <th>Profit</th>
                                <th>Date</th>
                                <th>Days</th>
                                <th>Order Ref</th>
                                <th>client Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>    
                            <tbody>
                            
                            @if(count($delivered_orders) > 0)
                            @foreach($delivered_orders as $item)
                            <?php 
                            $createdDate=date("d M Y", strtotime($item->created_at));
                            $currentDate=date("d M Y");
                            $date1 = new DateTime($createdDate);
                            $date2 = new DateTime($currentDate);
                            $interval = $date1->diff($date2);
                            
                            ?>
                            <tr>
                                <td>{{ $item->customer_name }}</td>
                                <td> @if($item->order_id > 0)  {{ $item->cl_city }} @else  {{ $item->city }} @endif</td>
                                <td> @if($item->order_id > 0)  {{ $item->cl_state}} @else {{ $item->state }} @endif</td>
                                <td>{{ $item->total_price }}</td>
                                <td>{{ $item->profit }}</td>
                                <td @if( $interval->days >1) style="background-color: red;"@endif><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            <td>{{  $interval->days }}</td>
                                 <td>
                            @if($item->order_id > 0)
                            <a href="{{ url('ref/order/detail/'.$item->order_id) }}" target="_blank">
                               Order # {{$item->order_id}}</a>
                            @else
                            --
                            @endif
                            </td>
                            <td>{{$item->client_name}}</td>
                                 <td>
                                     <a href="{{ url('invoice/'.$item->id) }}" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-external-link"></i> Detail</a>
                                     <a href="#" data-id="{{$item->id}}" data-toggle="modal" data-target="#view" class="btn btn-primary deliver  btn-xs">Deliver</a></td> 
                            </tr>
                            @endforeach
                            @else
                            <td>No record(s) found</td>
                            @endif
                        </tbody></table>
                </div><!--/.box-body -->

            </div><!--/.box -->
        </div>
        @endif


 
    
    </div>
    <br><br>
    <div class="row">
        @if(count($pending_payments) > 10)
        <div class="col-xs-4 scrollable">
    @else
        <div class="col-xs-4 scrollable">
    @endif
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Payments For Approval</h3>

            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">

                <table class="table table-hover">
                    <tbody><tr>
                            <th>Customer</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                        @if(count($pending_payments) > 0)
                        @foreach($pending_payments as $item)
                        <tr>
                            <td>{{$item->user_name}}</td>
                            <td>{{$item->amount}}</td>
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            <td>
                                @if($item->status == 'pending')
                                <a href="{{ url('payment/status/'.$item->id.'/approved') }}" class="btn btn-success btn-xs">Approve</i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <td>No record(s) found</td>
                        @endif
                    </tbody></table>
            </div><!--/.box-body -->

        </div><!--/.box -->
    </div>
    </div>    
    <br><br>
    <div class="row">

        @if(count($pending_credit_memo) > 10)
            <div class="col-xs-4 scrollable">
        @else
            <div class="col-xs-4 scrollable">
        @endif  
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Credit Memo For Approval</h3>

                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <table class="table table-hover">
                        <tbody><tr>
                              
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Date</th>
                                  <th>Action</th>
                            </tr>
                            @if(count($pending_credit_memo) > 0)
                            @foreach($pending_credit_memo as $item)
                            <tr>
                              <td>{{ $item->customer_name }}</td>
                                <td>{{ $item->total_price }}</td>
                                <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                                <td><a href="{{ url('credit-memo/'.$item->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-external-link"></i> Detail</a></td>
                            </tr>
                            @endforeach
                            @else
                            <td>No record(s) found</td>
                            @endif
                        </tbody></table>
                </div><!--/.box-body -->

            </div><!--/.box -->
        </div>
<!-- ddd -->
       <div class="col-xs-4 scrollable">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Invoices For Approval</h3>

                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <table class="table table-hover">
                        <tbody><tr>
                                
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            @if(count($pending_invoice) > 0)
                            @foreach($pending_invoice as $item)
                            <tr>
                               
                                <td>{{ $item->customer_name }}</td>
                                <td>{{ $item->total_price }}</td>
                                <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                                 <td><a href="{{ url('invoice/'.$item->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-external-link"></i> Detail</a></td>
                            </tr>
                            @endforeach
                            @else
                            <td>No record(s) found</td>
                            @endif
                        </tbody></table>
                </div><!--/.box-body -->

            </div><!--/.box -->
        </div>
    </div>
        @include('front/common/bulk_status_update_invoices_orders')
</section>
@include('front/common/dataTable_js')
<script>
   $(function () {
        $('#devinvoice').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export To Excel',
                title: 'Customers List',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6]
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Export To PDF',
                title: 'Customers List',
               exportOptions: {
                    columns: [0,1,2,3,4,5,6]
                }
            }
        ]
        });
    });
</script>
<script>
        $('.deliver').click(function(){
        var id=$(this).data('id');
        var currentLocation = window.location;
        document.getElementById("updateStatusDeliver").value = id;
        document.getElementById("location").value = currentLocation.href;
        document.getElementById("table1").value = 'invoices';
    })
</script>

@endsection
