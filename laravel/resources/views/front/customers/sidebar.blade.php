<!-- Left side column. contains the logo and sidebar -->

<?php

use App\Categories;
use App\Functions\Functions;
use App\Cart;
?>
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">

<!--            <style>
                li.multi-links>a {
                    display: inline-block;
                }

                li.multi-links>a:nth-child(2) {
                    float: right;
                    min-height:40px;    
                }
            </style>-->


            <style>
                /*li.treeview.multi-links a {
                    padding: 12px 30px 12px 15px;
                }*/

                .sidebar-menu>li>a.agnle_anchor {
                    position: absolute;
                    right: 0px;
                    left: auto;
                    top: 0;
                    border: 0;
                    padding: 0;
                    z-index: 99;
                    width: 30px;
                }
                li.treeview.multi-links {
                    position: relative;
                }
                li.multi-links>a:nth-child(2) {
                    min-height:40px;    
                }
            </style>
            
            <?php
                   $client = \App\Customers::where('user_id',Auth::user()->id)->select('client_menu')->first();
                  ?>

            @if(Auth::user()->role->role == 'customer')
            <li><a href="{{ url('/customer/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="{{ url('/customer/item-gallery') }}"><i class="fa fa-dashboard"></i> <span>Item Gallery</span></a></li>
             <li><a href="{{ url('customer/statement-report') }}"><i class="fa fa-table"></i>My Statement Report</a></li>
            <li><a href="{{ url('/customer/profile') }}"><i class="fa fa-dashboard"></i> <span>Profile</span></a></li>
           
             <li><a href="{{ url('/customer/invoices') }}"><i class="fa fa-dashboard"></i> <span>Invoice</span></a></li>
             <li><a href="{{ url('/customer/credit-memo') }}"><i class="fa fa-dashboard"></i> <span>Credit Memo</span></a></li>
               
                  @if($client->client_menu == 1)
                  
                   <li><a href="{{ url('/customer/clients') }}"><i class="fa fa-user"></i> <span>My Clients</span></a></li>
                   
<!--              <li class="treeview multi-links active">
                <a href="{{ url('/customer/clients') }}">
                    <i class="fa fa-user"></i> <span>My Clients</span>
                </a>
                <a class="agnle_anchor" href="{{ url('/customer/clients') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                
                <ul class="treeview-menu menu-open" style="">
                     <li hidden=""><a href="{{ url('/customer/clients') }}"><i class="fa fa-table"></i>Clients</a></li>
                    <li><a href="{{ url('/customer/orders') }}"><i class="fa fa-table"></i>Clients Orders</a></li>
                    <li><a href="{{ url('/customer/order-returns') }}"><i class="fa fa-table"></i>Clients Orders Returns</a></li>
                    <li><a href="{{ url('/customer/client/payments') }}"><i class="fa fa-table"></i>Client Payments</a></li>
                     <li><a href="{{ url('customer/report/client-statement') }}"><i class="fa fa-table"></i>Client Statement Report</a></li>
                     <li><a href="{{ url('customer/print_page_setting') }}"><i class="fa fa-table"></i>Settings</a></li>
                </ul>
                  
            </li>-->
            
<!--            <li class="treeview multi-links active">
                <a href="{{ url('/customer/report/client') }}">
                    <i class="fa fa-user"></i> <span>Client Reports</span>
                </a>
                <a class="agnle_anchor" href="{{ url('/customer/report/client') }}">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                
                <ul class="treeview-menu menu-open" style="">
                    @if(1==2)
                     <li><a href="{{ url('customer/report/client') }}"><i class="fa fa-table"></i>Client Transaction Report</a></li>
                     @endif
                     <li><a href="{{ url('customer/report/client/sale-profit') }}"><i class="fa fa-table"></i>Client Sale /Profit Report</a></li>
                    <li><a href="{{ url('customer/report/item/sale-profit') }}"><i class="fa fa-table"></i>Item Sale /Profit Client Report</a></li>
                     <li><a href="{{ url('customer/report/client/balance') }}"><i class="fa fa-table"></i>Client Balance Report</a></li>
                </ul>
                  
            </li>-->
            @endif
            
             
            <li><a href="{{ url('/customer/items_list') }}"><i class="fa fa-dashboard"></i> <span>Item List</span></a></li>
            <li><a href="{{ url('customer/item-price-templates') }}"><i class="fa fa-table"></i>Price Templates</a></li>
            <!--<li><a href="{{ url('/customer/order-returns') }}"><i class="fa fa-dashboard"></i> <span>Order Returns</span></a></li>-->
<!--            <li><a href="{{ url('customer/payment-types') }}"><i class="fa fa-table"></i>Client Payment Type</a></li>
            <li><a href="{{ url('customer/account-types') }}"><i class="fa fa-table"></i>Client Accounts</a></li>-->
            <li><a href="{{ url('/customer/payments') }}"><i class="fa fa-dashboard"></i> <span>Payment</span></a></li>
            @if(1==2)
             <li><a href="{{ url('customer/reports') }}"><i class="fa fa-table"></i>My Reports</a></li>
             @endif

            @endif

            <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-dashboard"></i> <span>Logout</span></a></li>
        </ul>

        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>