@extends('customer_c_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Transfer Amount</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/account-type/post/transfer', 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body">

            <div class="form-group">

                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Transfer From</label>
                    {!!   Form::select('source_id', $account_types, Request::input('source_id'), array('class' => 'form-control select2','id' => 'source_id',$required ))  !!}
                </div>

                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Select Package</label>
                    {!! Form::text('amount', Request::input('amount') , array('placeholder'=>"Amount",'maxlength' => 190,'minlength' => 1,'class' => 'form-control',$required) ) !!}
                </div>


                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Transfer To</label>
                    {!!   Form::select('destination_id', $account_types, Request::input('destination_id'), array('class' => 'form-control select2','id' => 'destination_id',$required ))  !!}
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Transfer">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>


</section>
<script type="text/javascript">

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
    });

</script>
@endsection