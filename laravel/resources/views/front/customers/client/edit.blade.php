@extends('customer_c_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Update Client</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/client/update', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
               <div class="form-group">
                  <div class="col-sm-12">
                      {!! Form::hidden('u_id', $model->u_id , array('maxlength' => 20,$required) ) !!}
                       {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
                       <label for="exampleInputEmail1">Client Name</label>
                     {!! Form::text('name', $model->name , array('placeholder'=>"Customer Name *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                  </div>
                   </div>
              
                   @if(1==2)
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Sale Price by %</label>
                     {!! Form::text('sale_price_template', $model->sale_price_template , array('placeholder'=>"Sale Price in %",'maxlength' => 4,'max' => 1000,'class' => 'form-control') ) !!}
                  </div>
                </div>
                   @endif
                     <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Phone</label>
                     {!! Form::text('phone', $model->phone , array('placeholder'=>"Phone",'maxlength' => 20,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Email</label>
                     {!! Form::text('email', $model->email , array('placeholder'=>"Email *",'maxlength' => 80,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>
                <?php if(false)
                 {
                ?>
                <div class="form-group">
                  <div class="col-sm-12">
                         <label for="exampleInputEmail1">Password</label>
                       <!--{!! Form::text('password', $model->password , array('placeholder'=>"Password",'maxlength' => 100,'class' => 'form-control') ) !!}-->
                        {!! Form::input('password', 'password', $model->password,array('placeholder'=>"Password",'maxlength' => 100,'class' => 'form-control',$required)) !!}
                    </div>
                  </div>
                <?php }?> 
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Address Line 1</label>
                     {!! Form::text('address1', $model->address1 , array('placeholder'=>"Address Line 1",'maxlength' => 400,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Address Line 2</label>
                     {!! Form::text('address2', $model->address2 , array('placeholder'=>"Address Line 2",'maxlength' => 400,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-6">
                       <label for="exampleInputEmail1">City</label>
                     {!! Form::text('city', $model->city , array('placeholder'=>"City",'maxlength' => 30,'class' => 'form-control') ) !!}
                  </div>
                      
                       <div class="col-sm-6">
                       <label for="exampleInputEmail1">State</label>
                  
                     {!!   Form::select('state', $states, $model->state, array('class' => 'form-control' ))  !!}
                  </div>
                </div>
               <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Zip Code</label>
                     {!! Form::text('zip_code', $model->zip_code , array('placeholder'=>"Zip Code",'maxlength' => 20,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="col-sm-12">
                  <div class="form-group">
                       <label for="exampleInputEmail1">Select New Sale Price Template</label>
                  
                     {!!   Form::select('selling_price_template', $templates, $model->selling_price_template, array('class' => 'form-control' ))  !!}
                  </div>
                      </div>
                  
                  <div class="form-group">
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Show Unit Price</label>
                    {!! Form::checkbox('show_unit_price',1,$model->show_unit_price, array('id'=>'show_unit_price','class' => 'flat-red')) !!}
                </div> 
                
                 <div class="col-sm-4">
                    <label for="exampleInputEmail1">Show Image</label>
                    {!! Form::checkbox('show_image',1,$model->show_image, array('id'=>'show_image','class' => 'flat-red')) !!}
                </div> 
                
                 <div class="col-sm-4">
                    <label for="exampleInputEmail1">Show Serial</label>
                    {!! Form::checkbox('show_serial',1,$model->show_serial, array('id'=>'show_serial','class' => 'flat-red')) !!}
                </div> 
                
             </div> 
            @if(1==2)
            <div class="form-group">
                <div class="col-sm-4">
                    <label for="exampleInputEmail1">Show Package</label>
                    {!! Form::checkbox('show_package_id',1,$model->show_package_id, array('id'=>'show_package_id','class' => 'flat-red')) !!}
                </div> 
                
            </div>
                @endif  
                <div class="col-sm-4" style="display: block;">
                    <label for="exampleInputEmail1">Increase Price</label>
                    {!! Form::checkbox('increase_effect',1,$model->increase_effect, array('id'=>'increase_effect','class' => 'flat-red')) !!}
                </div>
                <div class="col-sm-4" style="display: block;">
                    <label for="exampleInputEmail1">Decrease Price</label>
                    {!! Form::checkbox('decrease_effect',1,$model->decrease_effect, array('id'=>'decrease_effect','class' => 'flat-red')) !!}
                </div> 
                <div class="form-group">
                <div class="col-sm-12" style="display: block;">
                  <label for="exampleInputEmail1">status</label>
                  <br>
                  <input type="radio" name="status" class="btn btn-primary" value="1" @if($model->status==1) checked @endif>
                  <b>Active</b>
                  <input type="radio" name="status" class="btn btn-primary" value="0" @if($model->status==0) checked @endif>
                  <b>Inactive </b>
               </div> 
                </div>
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Note</label>
                     {!! Form::textarea('note', $model->note , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}
                    
                  </div>
                </div>
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
         <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js')}}"></script>	
    <script>

//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-purple',
    radioClass: 'iradio_flat-purple'
});
    </script>   			
</section>

@endsection