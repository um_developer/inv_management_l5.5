@extends('customer_c_template_2')

@section('content')
<?php
$required = 'required';
?>
 @include('front/common/client_common_tabs')
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Update Customer</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/print_page_setting/update', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
               <div class="form-group">
                  <div class="col-sm-12">
                      
                       {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
                       <label for="exampleInputEmail1">Bottom Line 1</label>
                     {!! Form::text('bottom_text_1', $model->bottom_text_1 , array('placeholder'=>"Bottom Line 2",'maxlength' => 500,'class' => 'form-control') ) !!}
                  </div>
                   </div>
                     <div class="form-group">
                <div class="col-sm-12">
                       <label for="exampleInputEmail1">Bottom Line 2</label>
                     {!! Form::text('bottom_text_2', $model->bottom_text_2 , array('placeholder'=>"Bottom Line 2",'maxlength' => 500,'class' => 'form-control') ) !!}
                  </div>
                </div>
              
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
</section>

@endsection