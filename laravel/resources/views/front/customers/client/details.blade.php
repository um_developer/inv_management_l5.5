@extends('customer_c_template')


@section('content')
<?php
$required = 'required';
?>
<section id="form">
    <?php //print_r($date_range); die(); ?>
    <div class="box-header with-border text-center">
        <h3 class="box-title">{{ucwords($basic_info[0]->name)  .' ( $'.$basic_info[0]->balance .' )'}}</h3>
        <a href="{{ url('/customer/clients') }}" style="float: right;width: 15%;" class="btn btn-primary btn-block btn-flat">Client Listing</a>
    </div>
    @include('front/customers/client/filter')    
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" id="tabs">
                <li <?php if (isset($activeTab) && $activeTab == 'info') { ?> class="active" <?php } else if (!isset($activeTab) || empty($activeTab)) { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#info" data-toggle="tab" aria-expanded="false">Basic Info</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'payments') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#payments" data-toggle="tab" aria-expanded="false">Payments</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'transactions') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#transactions" data-toggle="tab" aria-expanded="false">Statement Report</a></li>

                <li <?php if (isset($activeTab) && $activeTab == 'orders') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#orders" data-toggle="tab" aria-expanded="true">Orders</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'return_orders') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#return_orders" data-toggle="tab" aria-expanded="true">Order Returns</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'settings') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#settings" data-toggle="tab" aria-expanded="true">Sale / Profit</a></li>
            </ul>
            <div class="tab-content">

                <!-- /.tab-pane -->




                <?php if (isset($activeTab) && $activeTab == 'payments') { ?>
                    <div class="tab-pane active" id="payments">
                    <?php } else { ?>
                        <div class="tab-pane" id="payments">
                        <?php } ?>

                        <!-- The timeline -->
                        <div class="box">
                            <!-- /.box-header -->
                            <a href="{{ url('customer/client/payment/create/'.$basic_info[0]->id) }}" class="btn btn-primary pull-right">Create Payment</i></a>
                            <div class="box-body">

                                <div class="table-responsive">
                                    <div class="col-md-12">

                                      <table id="vendors" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                              <th>Payment ID</th>
                                              <th>Client Name</th>
                                              <th>Payment Type</th>
                                              <th>Account</th>
                                              <th>Payment Message</th>
                                              <th>Amount</th>
                                              <th>Date</th>
                                              <th>Statement_date</th>
                                              <th>Status</th>
                                              <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                    
                                            @if(count($payments0) >0)
                                            <?php
                                            $i = 1;
                                            $total_amount = 0;
                                            ?>
                                            @foreach($payments0 as $item)
                                            <tr>
                                              <td>{{$item->initials . $item->id}}</td>
                                              <td>{{$item->user_name}}</td>
                                              <td>{{$item->payment_type_title}}</td>
                                              <td>{{$item->account_type_title}}</td>
                                              <td>{{$item->message}}</td>
                                              <td>{{$item->total_modified_amount}}</td>
                                              <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                                              <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                                              @if($item->status == 'pending')
                                              <td><a class="btn btn-primary btn-xs"> Pending</a></td>
                                              @elseif($item->status == 'approved')
                                              <td><a class="btn btn-success btn-xs"> Approved</a></td>
                                              @else
                                              <td><a class="btn btn-warning btn-xs"> Rejected</a></td>
                                              @endif
                                              <td>
                                                  @if($item->modification_count == 0)
                                                  <a href="{{ url('customer/client/payment/edit/'.$item->id) }}"  class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                  @if($item->status == 'pending')
                                                  <a href="{{ url('payment/status/'.$item->id.'/approved') }}" onclick="return confirm('Are you sure you want to approve this Payment?')" class="btn btn-success">Approve</i></a>
                                                  @endif
                                                  <a href="{{ url('customer/client/payment/delete/'.$item->id) }}"  onclick="return confirm('Are you sure you want to delete this Payment?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                  @else
                                          PMT{{ $item->payment_type_id }}
                                          @endif
                                              </td>
                    
                                            </tr>
                                            <?php
                                            $total_amount = $total_amount + $item->amount;
                                            $i++;
                                            ?>
                                            @endforeach
                    
                                            @endif
                    
                    
                                        </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>

                                    </div></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.tab-pane -->








                    <?php if (isset($activeTab) && $activeTab == 'orders') { ?>
                        <div class="tab-pane active" id="orders">
                        <?php } else { ?>
                            <div class="tab-pane" id="orders">
                            <?php } ?>
                            @include('front/common/common_orders')

                        </div>



                        <?php if (isset($activeTab) && $activeTab == 'return_orders') { ?>
                            <div class="tab-pane active" id="return_orders">
                            <?php } else { ?>
                                <div class="tab-pane" id="return_orders">
                                <?php } ?>

                                <div class="box-body">
                                    <a href="{{ url('customer/order-return/create/'.$basic_info[0]->id) }}" class="btn btn-primary pull-right">Create Return </i></a>
                                    <div class="table-responsive">
                                        <div class="col-md-12">

                                            <table id="vendors" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Order Return ID</th>
                                                        <th>Client Name</th>
                                                        <th>Memo</th>
                                                        <th>Amount</th>
                                                        <th>Status</th>
                                                        <!--<th>Created Date</th>-->
                                                        <th>Statement Date</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if(count($order_returns) >0)
                                                    @foreach($order_returns as $item)
                                                    <tr>
                                                        <td><a href="{{ url('customer/order-return/'.$item->id) }}">{{$item->id}}</a></td>
                                                        <td>{{$item->client_name}}</td>
                                                        <td>{{$item->memo}}</td>
                                                        <td>{{$item->total_price}}</td>
                                                        @if($item->status == 'pending')
                                                        <td><a class="btn btn-warning btn-xs"> Draft</a></td>
                                                        @elseif($item->status == 'approved_charged')
                                                        <td><a class="btn btn-success btn-xs"> Client Credited</a></td>
                                                        @elseif($item->status == 'approved')
                                                        <td><a class="btn btn-success btn-xs"> Credit Memo Created (Admin) / Client Credited</a></td>
                                                        @endif

                                                        <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                                                        <td>
                                                            <a href="{{ url('customer/order-return/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                                            <a href="{{ url('customer/order-return/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                            @if($item->status == 'pending')
                                                            <a href="{{ url('customer/order-return/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this Order Return?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                            @endif


                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                    @endif


                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>

                                            <div></div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>  
                            </div>

                            <?php if (isset($activeTab) && $activeTab == 'transactions') { ?>
                                <div class="tab-pane active" id="transactions">
                                <?php } else { ?>
                                    <div class="tab-pane" id="transactions">
                                    <?php } ?>

                                    <div class="box-body">

                                        <div class="table-responsive">
                                            <div class="col-md-12">

                                                <table id="vendors4" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>S.#</th>
                                                            <th>Transaction ID</th>
                                                            <th>Customer Name</th>
                                                            <th>Start Bal</th>
                                                            <th>Amount</th>
                                                            <th>End Bal</th>
                                                            <th>Transaction Type</th>
                                                            <th>Transaction Details </th>
                                                            <!--<th>Transaction Date</th>-->
                                                            <th>Statement Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @if(count($transactions) > 0)
                                                        <?php $i = 1; ?>
                                                        @foreach($transactions as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->id }}</td>
                                                            <td>{{ $item->client_name }}</td>
                                                            <td>{{ $item->new_previous_balance }}</td>
                                                            <td>{{ $item->new_total_modified_amount }}</td>
                                                            <td>{{ $item->new_updated_balance }}</td>

                                                            @if($item->payment_type == 'direct')
                                                            <td>Payment</td>
                                                            @else
                                                            <td>{{ ucfirst(str_replace("_"," ",$item->payment_type)) }}</td>
                                                            @endif

                                                            @if($item->payment_type == 'direct')
                                                            @if($item->modification_count > 0)
                                                            <td>PMT{{ $item->payment_type_id }}</td>
                                                            @else
                                                            <td> {{ $item->initials  . $item->id }}</td>
                                                            @endif
                                                            @elseif($item->payment_type == 'order')
                                                            @if($item->modification_count > 0)
                                                            <td> <a href="{{ url('customer/order/'.$item->payment_type_id) }}" target="_blank">Order# {{ $item->payment_type_id }}</a></td>
                                                            @else
                                                            <td> <a href="{{ url('customer/order/'.$item->payment_type_id) }}" target="_blank">Order# {{ $item->payment_type_id }}</a></td>
                                                            @endif
                                                            @else
                                                            @if($item->modification_count > 0)
                                                            <td> <a href="{{ url('customer/order-return/'.$item->payment_type_id) }}" target="_blank">Return Order# {{ $item->payment_type_id }}</a></td>
                                                            @else 
                                                            <td> <a href="{{ url('customer/order-return/'.$item->payment_type_id) }}" target="_blank">Return Order# {{ $item->payment_type_id }}</a></td>
                                                            @endif
                                                            @endif

                                                            <td><?php echo date("d M Y", strtotime($item['statement_date'])); ?></td>

                                                        </tr>
                                                        <?php $i++; ?>
                                                        @endforeach

                                                        @endif


                                                    </tbody>
                                                    <tfoot>

                                                    </tfoot>
                                                </table>

                                                <div></div>

                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </div>  
                                </div>



                                <?php if (isset($activeTab) && $activeTab == 'settings') { ?>
                                    <div class="tab-pane active" id="settings">
                                    <?php } else { ?>
                                        <div class="tab-pane" id="settings">
                                        <?php } ?>

                                        <!-- <div class="tab-pane" id="settings"> -->
                                        <div class="box-body">

                                            <div class="table-responsive">
                                                <div class="col-md-12">

                                                    <table id="settings" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Customer Name</th>
                                                                <th>Sale Amount</th>
                                                                <th>Profit Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php
                                                            $total_sale_amount = 0;
                                                            $total_profit = 0;
                                                            ?>

                                                            @if(count($model) >0)
                                                            <?php $i = 1; ?>
                                                            @foreach($model as $item)
                                                            <?php
                                                            $f_sale = $item->sale_amount - $item->credit_memo_sale;
                                                            $f_profit = $item->profit_amount - $item->credit_memo_loss;
                                                            ?>
                                                            <tr>
                                                                <td>{{ $item->customer_name }}</td>
                                                                <td>{{ $f_sale }}</td>
                                                                <td>{{ $f_profit }}</td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                            $total_sale_amount = $total_sale_amount + $f_sale;
                                                            $total_profit = $total_profit + $f_profit;
                                                            ?>
                                                            @endforeach
                                                            <tr>
                                                                <th></th>
                                                                <th>Total Sale ${{ $total_sale_amount }}</th>
                                                                <th>Total Profit ${{ $total_profit }}</th>
                                                            </tr>
                                                            @endif


                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>

                                                </div></div>
                                        </div>
                                    </div>
                                    <style>
                                        .spacer {
                                            margin-top: 10px; /* define margin as you see fit */
                                        }
                                    </style>
                                    <?php if (isset($activeTab) && $activeTab == 'info') { ?>
                                        <div class="tab-pane active" id="info">
                                        <?php } else if (!isset($activeTab) || empty($activeTab)) { ?>
                                            <div class="tab-pane active" id="info">
                                            <?php } else { ?>
                                                <div class="tab-pane" id="info">
                                                <?php } ?>

                                                <div class="box">
                                                    <div class="box-body">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-2"><label>Customer Name: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->name}}</div>
                                                                <div class="col-md-2"><label>Phone: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->phone}}</div>
                                                                <div class="col-md-2"><label>Email: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->email}}</div>
                                                            </div>
                                                        </div>
                                                        <div class="spacer"></div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-2"><label>Phone: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->phone}}</div>
                                                                <div class="col-md-2"><label>Address 1: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->address1}}</div>
                                                                <div class="col-md-2"><label>Address 2: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->address2}}</div>
                                                            </div>
                                                        </div>
                                                        <div class="spacer"></div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-2"><label>City: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->city}}</div>
                                                                <div class="col-md-2"><label>State: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->state}}</div>
                                                                <div class="col-md-2"><label>Zip Code: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->zip_code}}</div>
                                                            </div>
                                                        </div>
                                                        <div class="spacer"></div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-2"><label>balance: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->balance}}</div>
                                                                <div class="col-md-2"><label>Created Date: </label></div>
                                                                <div class="col-md-2">{{$basic_info[0]->created_at}}</div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <div class="col-sm-4">
                                                                    <label for="exampleInputEmail1">Show Unit Price</label>
                                                                    {!! Form::checkbox('show_unit_price',1,$basic_info[0]->show_unit_price, array('id'=>'show_unit_price','class' => 'flat-red')) !!}
                                                                </div> 

                                                                <div class="col-sm-4">
                                                                    <label for="exampleInputEmail1">Show Image</label>
                                                                    {!! Form::checkbox('show_image',1,$basic_info[0]->show_image, array('id'=>'show_image','class' => 'flat-red')) !!}
                                                                </div> 

                                                                <div class="col-sm-4">
                                                                    <label for="exampleInputEmail1">Show Serial</label>
                                                                    {!! Form::checkbox('show_serial',1,$basic_info[0]->show_serial, array('id'=>'show_serial','class' => 'flat-red')) !!}
                                                                </div> 
                                                                <div class="col-sm-4" style="display: block;">
                                                                    <label for="exampleInputEmail1">Increase Price</label>
                                                                    <input type="checkbox" class="" data-id="{{ $basic_info[0]->id }}"  data-type="increase"  @if($basic_info[0]->increase_effect==1) checked @endif id="increase_effect"  name="increase_effect" onchange="changeclientStatus(this)">
                                                                </div>
                                                                <div class="col-sm-4" style="display: block;">
                                                                    <label for="exampleInputEmail1">Decrease Price</label>
                                                                    <input type="checkbox" class=""  data-id="{{$basic_info[0]->id}}" data-type="decrease"   @if($basic_info[0]->decrease_effect==1) checked @endif id="decrease_effect" name="decrease_effect" onchange="changeclientStatus(this)">
                                                                </div> 

                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <a href="{{ url('customer/client/payment/create/'.$basic_info[0]->id) }}" class="btn btn-primary">$$</i></a>
                                                            <a href="{{ url('customer/client/edit/'.$basic_info[0]->u_id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                           
                                                            @if($sync->is_order_sync == 0)
                                                            <a href="{{ url('customer/order/create/'.$basic_info[0]->id) }}" data-toggle="tooltip" title="Create Order" class="btn btn-success"><i class="fa fa-list-alt"></i></a>
                                                            @else
                                                            <a href="{{ url('customer/sync/order/create/'.$basic_info[0]->id) }}" data-toggle="tooltip" title="Create Order" class="btn btn-success"><i class="fa fa-list-alt"></i></a>
                                                            @endif
                                                            <a href="{{ url('customer/order-return/create/'.$basic_info[0]->id) }}"  data-toggle="tooltip" title="Create Order Return"  class="btn btn-warning"><i class="fa fa-share-square-o"></i></a>
                                                            <a href="{{ url('customer/client/delete/'.$basic_info[0]->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- /.nav-tabs-custom -->
                        </div>

                        </section>
                        @include('front/common/dataTable_js')

                        <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
                        <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js')}}"></script>	
                        <script>

                                                                //Flat red color scheme for iCheck
                                                                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                                                                    checkboxClass: 'icheckbox_flat-purple',
                                                                    radioClass: 'iradio_flat-purple'
                                                                });
                        </script>
                        <script>
                           
                           function changeclientStatus(elem) {
      
      
      var  id= $(elem).data("id");
     // var  status=  $(elem).data("val");
      var  type=  $(elem).data("type");
      if($(elem).prop("checked")==true)
      {
          status=1;
      }
      if($(elem).prop("checked")==false)
      {
          status=0;
      }
 $.ajax({
  url: "<?php echo url('customer/client/clientimpact/'); ?>" +"/"+status+"/"+id+"/"+type,
  type: 'get',
  dataType: 'html',
  success: function (response) {
      console.log(response);
      if (response == 1) {
//                    var modal = document.getElementById('myModal');
//                    modal.style.display = "block";
//                    alert(response);

      } else if (response == 0) {

      }
  },
  error: function (xhr, status, response) {
      alert(response);
  }
});
  }  
                
              

                            $(document).ready(function () {

                                var valnew = '{{ $activeTab }}';

                                if (!valnew) {
                                    valnew = 'info';
                                }
                                var res = valnew.toLowerCase();
                                $("#tab0").val(res);

                                document.getElementById("show_unit_price").disabled = true;
                                document.getElementById("show_image").disabled = true;
                                document.getElementById("show_serial").disabled = true;
                                //var valnew = $(".nav-tabs").find(".active a").html();


                                var selected = '<?php echo $report_status; ?>'

                                if (selected == 'delivered' && res != 'orders') {
                                    document.getElementById('type011').selectedIndex = -1;
                                    $("#type011").val("");
                                    document.getElementById('type012').selectedIndex = -1;
                                    $("#type012").val("");
                                }

                                if (valnew == 'transactions') {
                                    $("#filter_row").css("display", "block");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#customer_dropdown").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    $("#type01").css("display", "none");
                                    $("#type02").css("display", "none");
                                    $("#deliver_option").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                    document.getElementById("mybutton2").disabled = false;
                                    document.getElementById("mybutton").disabled = false;
                                } else if (valnew == 'orders') {
                                    $("#filter_row").css("display", "block");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type01").css("display", "block");
                                    $("#deliver_option").css("display", "block");
                                    $("#deliver_option1").css("display", "block");
                                    $("#completed_option1").css("display", "block");
                                    $("#type01").css("display", "none");
                                    $("#type02").css("display", "block");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById("mybutton2").disabled = false;
                                    document.getElementById("mybutton").disabled = false;
                                }else if (valnew == 'payments') {
                                    $("#filter_row").css("display", "block");
                                    $("#payment_source_type").css("display", "block");
                                    $("#account_type").css("display", "block");
                                    $("#type01").css("display", "block");
                                    $("#deliver_option").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                    $("#type01").css("display", "block");
                                    $("#type02").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById("mybutton2").disabled = false;
                                    document.getElementById("mybutton").disabled = false;
                                } else if (valnew == 'return_orders') {
                                    $("#filter_row").css("display", "block");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type01").css("display", "block");
                                    $("#deliver_option").css("display", "none");
                                    $("#deliver_option1").css("display", "none");
                                    $("#processing_option1").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                    $("#type01").css("display", "none");
                                    $("#type02").css("display", "block");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById("mybutton2").disabled = false;
                                    document.getElementById("mybutton").disabled = false;
                                } else if (valnew == 'info') {
                                    document.getElementById("mybutton2").disabled = true;
                                    document.getElementById("mybutton").disabled = true;
                                    $("#transection_type").css("display", "none");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type02").css("display", "none");
                                    // $("#filter_row").css("display", "hide");
                                } else if (valnew == 'settings') {
                                    document.getElementById("mybutton2").disabled = false;
                                    document.getElementById("mybutton").disabled = false;
                                    $("#transection_type").css("display", "none");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type02").css("display", "none");
                                    // $("#filter_row").css("display", "hide");
                                } else {
                                    document.getElementById("mybutton2").disabled = false;
                                    document.getElementById("mybutton").disabled = false;
                                    $("#filter_row").css("display", "block");
                                    $("#customer_dropdown").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    $("#type01").css("display", "block");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type02").css("display", "none");
                                    $("#deliver_option").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                }
                                // if(valnew == 'Transactions') {
                                //     $("#customer_dropdown").css("display", "block");
                                //     $("#transection_type").css("display", "block");
                                //   }else if(valnew == 'orders'){
                                //     $("#filter_row").css("display", "block");
                                //     $("#type01").css("display", "block");
                                //     $("#deliver_option").css("display", "block");
                                //     $("#transection_type").css("display", "none");
                                //   }else if(valnew == 'Basic Info'){
                                //     $("#transection_type").css("display", "none");
                                //     $("#filter_row").css("display", "hide");
                                //   }else{
                                //     $("#customer_dropdown").css("display", "none");
                                //     $("#transection_type").css("display", "none");
                                // }

                            });

                            $("ul.nav-tabs > li > a").click(function () {
                                $("#tab0").val($(this).attr("href").replace("#", ""));
                                if ($(this).attr("href").replace("#", "") == 'transactions') {
                                    $("#filter_row").css("display", "block");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#customer_dropdown").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    $("#deliver_option").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                    $("#type01").css("display", "none");
                                    $("#type02").css("display", "none");
                                    document.getElementById("mybutton").disabled = false;
                                    document.getElementById("mybutton2").disabled = false;
                                } else if ($(this).attr("href").replace("#", "") == 'orders') {
                                    $("#filter_row").css("display", "block");
                                    $("#deliver_option").css("display", "block");
                                    $("#type01").css("display", "none");
                                    $("#deliver_option1").css("display", "block");
                                    $("#completed_option1").css("display", "block");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type02").css("display", "block");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById("mybutton").disabled = false;
                                    document.getElementById("mybutton2").disabled = false;
                                } else if ($(this).attr("href").replace("#", "") == 'return_orders') {
                                    $("#filter_row").css("display", "block");
                                    $("#deliver_option").css("display", "block");
                                    $("#type01").css("display", "none");
                                    $("#deliver_option1").css("display", "none");
                                    $("#processing_option1").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type02").css("display", "block");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById('type012').selectedIndex = -1;
                                    $("#type012").val("");
                                    document.getElementById("mybutton").disabled = false;
                                    document.getElementById("mybutton2").disabled = false;
                                } else if ($(this).attr("href").replace("#", "") == 'payments') {
                                    $("#filter_row").css("display", "block");
                                    $("#deliver_option").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                    $("#type01").css("display", "block");
                                    $("#payment_source_type").css("display", "block");
                                    $("#account_type").css("display", "block");
                                    $("#type02").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById("mybutton").disabled = false;
                                    document.getElementById("mybutton2").disabled = false;
                                } else if ($(this).attr("href").replace("#", "") == 'statement') {
                                    $("#filter_row").css("display", "block");
                                    $("#type01").css("display", "none");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type02").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById("mybutton").disabled = false;
                                    document.getElementById("mybutton2").disabled = false;
                                } else if ($(this).attr("href").replace("#", "") == 'settings') {
                                    $("#filter_row").css("display", "block");
                                    $("#type01").css("display", "none");
                                    $("#type02").css("display", "none");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    document.getElementById("mybutton").disabled = false;
                                    document.getElementById("mybutton2").disabled = false;
                                } else if ($(this).attr("href").replace("#", "") == 'info') {
                                    $("#type01").css("display", "none");
                                    $("#type02").css("display", "none");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    document.getElementById("mybutton2").disabled = true;
                                    document.getElementById("mybutton").disabled = true;
                                    // $("#filter_row").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                } else {
                                    $("#deliver_option").css("display", "none");
                                    $("#completed_option1").css("display", "none");
                                    $("#customer_dropdown").css("display", "none");
                                    $("#transection_type").css("display", "none");
                                    $("#type01").css("display", "block");
                                    $("#payment_source_type").css("display", "none");
                                    $("#account_type").css("display", "none");
                                    $("#type02").css("display", "none");
                                    $("#filter_row").css("display", "block");
                                    document.getElementById("mybutton2").disabled = false;
                                    document.getElementById("mybutton").disabled = false;
                                }
                                //   if($(this).attr("href").replace("#", "") == 'transactions'){
                                //     $("#customer_dropdown").css("display", "block");
                                //     $("#transection_type").css("display", "block");
                                //   }else{
                                //     $("#customer_dropdown").css("display", "none");
                                //     $("#transection_type").css("display", "none");
                                //   }
                                console.log(
                                        $(this).attr("href").replace("#", "")
                                        )
                            });

 $(function () {
                                        $('#vendors').DataTable({
                                        "paging": true,
                                                "lengthChange": false,
                                                "searching": false,
                                                "ordering": false,
                                                "info": false,
                                                "pageLength": {{Config::get('params.default_list_length')}},
                                                "autoWidth": false,
                                                dom: 'Bfrtip',
                                                buttons: [
                                                {
                                                extend: 'excelHtml5',
                                                        text: 'Export To Excel',
                                                        title: 'Report',
                                                },
                                                {
                                                extend: 'pdfHtml5',
                                                        text: 'Export To PDF',
                                                        title: 'Report',
                                                }
                                                ]
                                        });
                                        });

                                        $(function () {
                                        $('#vendors4').DataTable({
                                        "paging": true,
                                                "lengthChange": false,
                                                "searching": false,
                                                "ordering": false,
                                                "info": false,
                                                "pageLength": {{Config::get('params.default_list_length')}},
                                                "autoWidth": false,
                                                dom: 'Bfrtip',
                                                buttons: [
                                                {
                                                extend: 'excelHtml5',
                                                        text: 'Export To Excel',
                                                        title: 'Statement Report',
                                                },
                                                {
                                                extend: 'pdfHtml5',
                                                        text: 'Export To PDF',
                                                        title: 'Statement Report',
                                                }
                                                ]
                                        });
                                        });

                        </script>

                        @endsection
                  
