@extends('customer_c_template')
<?php
$modal_title = 'Loss';
?>
@section('content')
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-hdd-o"></i> Client Return Order # {{ $model->id }}
                <small class="pull-right">Statement Date: <?php echo date("d M Y", strtotime($model->statement_date)); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <b>Client Name: {{ $model->client_name }}</b><br>
            <br>
            <b>Bill To:</b> {{ $model->client_address }}<br>
            <b>Phone #:</b>{{ $model->client_phone }}<br>
            <!--          <b>Account:</b> 968-34567-->
            <br>
            <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <!--          To
                      <address>
                        <strong>John Doe</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone: (555) 539-1037<br>
                        Email: john.doe@example.com
                      </address>-->
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

            @if($model->status == 'pending')
            Order Return Status: <b class = "label label-warning btn-xs">Draft</b><br>
            @elseif($model->status == 'approved_charged')
            Order Return Status: <b class = "label label-success btn-xs">Client Credited</b><br>
            @elseif($model->status == 'approved')
            Order Return Status: <b class = "label label-success btn-xs"> Credit Memo Created (Admin) / Client Credited</b><br>
            @endif

            @if($model->status == 'pending')
            <br>
            <b> 
                <a href="{{ url('customer/order-return/edit/'.$model->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                <button type="button" class="btn btn-success btn-sm" onclick="showModal()"><i class="fa fa-check-square"></i> Approve</button>                
                @endif

                @if(1==1)
                <a href="{{url('customer/order-return-print/'.$model->id)}}" class="btn btn-success btn_print btn-sm"><i class="fa fa-print"></i> Print</a>
                 <script src="{{ asset('front/js/jquery.printPage.js')}}"></script>
                <script>
                        $(document).ready(function () {
                        $(".btn_print").printPage();
                        });
                </script>
                 @if($show_profit == 0)
                <a href="{{ url('customer/order-return/'.$model->id.'/1') }}" class="btn btn-primary btn-sm">Show Details</i></a>
                @else
                <a href="{{ url('customer/order-return/'.$model->id) }}" class="btn btn-danger btn-sm">Hide Details</i></a>
                @endif
               
                @endif
                <br>
                <br>
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Start Serial #</th>
                                    <th>End Serial #</th>
                                    <th>QTY</th>
                                    <!--<th>Delivered QTY</th>-->
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                     @if($show_profit == 1)
                                    <th>Total Cost</th>
                                    <th>Loss</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($po_items)>0)
                                @foreach($po_items as $items_1)
                                <?php 
                                if($items_1->bundle_name != ''){
                                     $bundle_detail = ' ('.$items_1->bundle_name.' QTY: '. $items_1->bundle_quantity . ')'; 
                                    $items_1->item_name = $items_1->item_name . $bundle_detail;
                                }
                                ?>
                                <tr>
                                    @if($items_1->image == '')
                                    <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                                    @else
                                    <td><a href='#' onclick="showItemGallery('{{$items_1->image}}','{{$items_1->item_id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"></a></td>
                                    <!-- <td><a href='#' onclick="showItemModal('{{$items_1->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$items_1->image}}" height="42" width="42"></a></td> -->
                                    @endif
                                    <td>{{ $items_1->item_name }}</td>
                                    <td>{{ $items_1->start_serial_number }}</td>
                                    <td>{{ $items_1->end_serial_number }}</td>
                                    <td>{{ $items_1->quantity }}</td>
                                    <!--<td>{{ $items_1->delivered_quantity }}</td>-->
                                    <td>{{ $items_1->item_unit_price }}</td>
                                    <td>{{ $items_1->total_price }}</td>
                                     @if($show_profit == 1)
                                    <td>{{ $items_1->total_cost }}</td>
                                    <td>{{ $items_1->loss }}</td>
                                    @endif
                                </tr>

                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-6">
            <!--          <p class="lead">Payment Methods:</p>
                    
            
                      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                      </p>-->
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-6">
                      <!--<p class="lead">Amount Due 2/22/2014</p>-->

                        <div class="table-responsive">
                            <table class="table">
                               <tbody><tr>
                            <th style="width:50%">Total Quantity:</th>
                            <td>{{ $model->total_quantity }}</td>
                        </tr>
                        <tr>
                            <th>Total Price</th>
                            <td>{{ $model->total_price }}</td>
                        </tr>
 @if($show_profit == 1)
                        <tr>
                            <th>Total Cost</th>
                            <td>{{ $model->total_cost }}</td>
                        </tr>

                        <tr>
                            <th>Loss</th>
                            <td>{{ $model->loss }}</td>
                        </tr>
                        @endif

                    </tbody></table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                    <div class="col-xs-12">
                        @if($model->status == 'pending')
                        <a href="{{ url('customer/order-return/edit/'.$model->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> Edit Return Order</a>
                        @endif
              <!--          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                        </button>
                        <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                          <i class="fa fa-download"></i> Generate PDF
                        </button>-->
                    </div>
                </div>
                </section>
                @include('front/common/image_modal')
                @include('front/common/show_profits_modal')
                <div class="modal" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button"  id="cross"  class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Update Return Order Status</h4>
                            </div>
                            @if(1==1)
                            <div class="modal-body">
                                <p>Update the Return order status by choose any 1 option from below.</p>
                            </div>
                            @endif
                            <div class="modal-footer">
                                <a id="closemodal" class="btn btn-danger" href="{{ url('customer/order-return-send/credit-memo/'.$model->id) }}">CREDIT CLIENT AND MAKE CREDIT MEMO</a>
                                <a id="closemodal" class="btn btn-success" href="{{ url('customer/order-return-send/0/'.$model->id) }}">CREDIT CLIENT</a>
                            </div>
                        </div>

                    </div>

                </div>

                <script type="text/javascript">

                    function showModal(){
                    var modal = document.getElementById('myModal');
                    modal.style.display = "block";
                    }

                    var cross = document.getElementById("cross");
                    cross.onclick = function () {
                    var modal = document.getElementById('myModal');
                    modal.style.display = "none";
                    }
                    
                    
   function showProfitModal(){

    var modal = document.getElementById('modal_profit_new');
    modal.style.display = "block";
    }

                </script>
                                                    @include('front/common/image_modal')
                @endsection