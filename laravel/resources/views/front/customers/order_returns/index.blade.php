@extends( (Auth::user()->role_id == 4) ? 'customer_c_template' : 'customer_template')
@section('content')

<?php
$base_url = url('credit-memo');
if(Auth::user()->role_id == 4)
    $base_url = url('customer/order-return');
?>
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        
         @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        
        <div class="row">
            <div class="col-xs-12">
                       @include('front/common/client_common_tabs')
                <div class="box">
                    <div class="box-header with-border">

                        <h3 class="box-title">Search Filter</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>
                   {!! Form::open(array( 'class' => '','url' => 'customer/order-returns/search', 'method' => 'get')) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-3">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div><input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation"></div></div>
                            <div class="form-group col-sm-4">

                                {!!  Form::select('client_id', $clients, Request::input('client_id'), array('class' => 'form-control select2','id' => 'client_id' ))  !!}
                            </div>                                    <div class="form-group col-sm-4" id="type">

                            <select id="type" name="type" class="form-control">
                                <option value="" <?php echo ($selected_report_type == '') ? 'selected' : ''; ?>>Select Status</option>        
                                <option value="pending" <?php echo ($selected_report_type == 'pending') ? 'selected' : ''; ?>>Draft</option>
                                <option value="approved" <?php echo ($selected_report_type == 'approved') ? 'selected' : ''; ?>>Inv Created (Admin)/Client Charged</option>
                                <option value="approved_charged" <?php echo ($selected_report_type == 'approved_charged') ? 'selected' : ''; ?>>Client Charged</option>
                            </select>
                            </div>

                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
        $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>


                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="page" id="page" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">
                               <a href="{{ URL::to('customer/order-returns') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Order Returns</h3>
                <a href="{{ $base_url . '/create' }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Order Return</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Order Return ID</th>
                            <th>Client Name</th>
                            <th>Memo</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <!--<th>Created Date</th>-->
                            <th>Statement Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td><a href="{{ $base_url .'/'.$item->id }}">{{$item->id}}</a></td>
                            <td>{{$item->client_name}}</td>
                            <td>{{$item->memo}}</td>
                            <td>{{$item->total_price}}</td>
                            @if($item->status == 'pending')
                            <td><a class="btn btn-warning btn-xs"> Draft</a></td>
                            @elseif($item->status == 'approved_charged')
                            <td><a class="btn btn-success btn-xs"> Client Credited</a></td>
                             @elseif($item->status == 'approved')
                            <td><a class="btn btn-success btn-xs"> Credit Memo Created (Admin) / Client Credited</a></td>
                            @endif
                            
                            <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                            <td>
                                <a href="{{ $base_url .'/'.$item->id }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>
                                <a href="{{ $base_url .'/edit/'.$item->id }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                 @if($item->status == 'pending')
                                <a href="{{ $base_url .'/delete/'.$item->id }}" onclick="return confirm('Are you sure you want to delete this Order Return?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                @endif
                                

                            </td>
                        </tr>
                    @endforeach

                    @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

                <div></div>

            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false
        });
    });
    
    $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });
    
</script>
@endsection



