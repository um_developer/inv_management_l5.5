<div class="modal" id="modal_log" style="height: 150% !important;">
    <div class="modal-dialog" style="margin: 69px;margin-left: 40px;height: 67%;">
        <div class="modal-content" style="width: 240%;">
            <div class="modal-header">
                <button type="button"  id="cross1bundle"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Log Detail</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style=" height: 400px;
    display: inline-block;
    overflow: auto; ">
                    <table class="table table-bordered table-striped" id="item_body">
                        <!-- <thead>
                            <tr>
                            </tr>
                        </thead> -->
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>
<script type="text/javascript">

    var cross = document.getElementById("cross1bundle");
    var no = document.getElementById("no1bundle");
    cross.onclick = function () {
        var modal = document.getElementById('modal_log');
        modal.style.display = "none";
    }
    no.onclick = function () {
        var modal = document.getElementById('modal_log');
        modal.style.display = "none";
    }
</script>