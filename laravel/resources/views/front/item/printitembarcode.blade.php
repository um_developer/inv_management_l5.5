<strong><span style="font-size: 25px" > {{ $model->name }}</span></strong><br>         
<div class="barcode">
    <?php
   $cleanStr = preg_replace('/[^A-Za-z0-9]/', ' ',$model->upc_barcode);
    
   // echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($cleanStr, "Upce") . '" alt="barcode" />';
   use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;

$barcode = new BarcodeGenerator();
$barcode->setText( $cleanStr);
$barcode->setType(BarcodeGenerator::Upca);
$barcode->setScale(2);
$barcode->setThickness(25);
$barcode->setFontSize(14);
$code = $barcode->generate();

echo '<img src="data:image/png;base64,'.$code.'" />';
    
    ?>
</div>

<strong><span style="font-size: 25px" > {{ $model->upc_barcode }}</span></strong><br>
