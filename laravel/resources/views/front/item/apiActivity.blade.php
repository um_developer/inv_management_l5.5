<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  /* border-collapse: collapse; */
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
.table0 tr td {
  vertical-align:top;
  max-width:700px;
  background-color: #dedede;
  color: black;
}
.talign{
display:inline-block;
}
.dalign{
display:inline;
vertical-align:top;
}
</style>
</head>
<body>

<b>Item Name :</b> <?php echo $records['item_name']; ?> </br>
<b>Action : </b><?php echo $records['action']; ?> </br>
<b>Source :</b> <?php echo $records['source']; ?></br>
<b>Update Date :</b> <?php echo $records['updated_at']; ?> </br> 
<table class='dalign table0'>
  <tr>
    <th>Send</th>
    <th>Receive</th>
  </tr>
  <tr>
    <td><?php 
    header('Content-Type: application/json');
    $json_pretty = json_encode(json_decode($records['send']), JSON_PRETTY_PRINT);
    //echo '<pre>'; 
    print_r($json_pretty) ;// echo '</pre>';
    // echo json_decode($records['send'], JSON_PRETTY_PRINT);
    // dd(json_decode($records['send'])); 
    //echo '</pre>'; ?></td>
    <td><?php echo json_decode($records['receive']); ?></td>
  </tr>
</table>
</body>
</html>
