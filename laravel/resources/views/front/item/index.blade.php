@extends('customer_template')

@section('content')
<style>
  .nbtnprint {
       pointer-events: none !important;
       cursor: default;
      
}  
</style>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>

            {!! Form::open(array( 'class' => '','url' => 'item/search', 'method' => 'post')) !!}
            <div class="box-body">
                <div class="row">

                    <div class="form-group col-sm-3">
                        {!!   Form::select('category_id', $categories, $category_id, array('class' => 'form-control ','id' => 'category_id' ))  !!}
                    </div>
                    <div class="form-group col-sm-3">
                    <select class ='form-control' name="status" >
                    <option value="" @if(isset($status) && empty($status)) selected  @endif>Select Status</option> 
                    <option value="1" <?php if(isset($status) && $status==1){ ?> selected  <?php }?>>Active</option>
                    <option value="0"<?php if(isset($status) && $status==0){ ?> selected  <?php }?>>inactive </option>
                    </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <select class ='form-control' name="pricestatus" >
                            <option value="" @if(isset($pricestatus) && empty($pricestatus)) selected  @endif>Select price status</option> 
                            <option value="increase" <?php if(isset($pricestatus) && $pricestatus=="increase"){ ?> selected  <?php }?>>Increase</option>
                            <option value="decrease"<?php if(isset($pricestatus) && $pricestatus=="decrease"){ ?> selected  <?php }?>>Decrease </option>
                            </select>
                    </div>   
                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                        <a href="{{ URL::to('items') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>
<style>
    .modal-lg {
  max-width: 900px;
}
</style>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Items Listing</h3>
                <a href="{{ url('item/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Item</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
               
                    <button  data-toggle="modal" data-target="#autogeneratebarcode" onclick="autogeneratebarcode()" class="btn btn-primary pull-right" style="
                    margin-right: 23px;
                ">Generate Barcode</button>
                
                  </div>
          
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>IMG</th>
                            <th>ID</th>
                            <th>SKU/Item Code</th>
                            <th>Item Name</th>
                            <th>Category</th>
                            <th>Serial</th>
                            <!--<th>API Name</th>-->
                            <th>UPC/Barcode</th>
                            <th>Cost</th>
                            <!--<th>S.P</th>-->
                            <th>QTY</th>
                            <th>State</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                   
                        <tr>
                            <?php
                            $quantity = 0;
                           // if ($item->warehouse_quantity != '') {
                                $quantity = $item->warehouse_quantity + $item->inventory_quantity + $item->package_quantity + $item->sub_item_quantity;
                            //}
                            ?>


                            @if($item->image == '')
                            <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                            @else
                            <td><a href='#' onclick="showItemGallery('{{$item->image}}','{{$item->id}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>
                             <!--<td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>-->
                            @endif
                            <td>{{$item->id}}</td>
                            <td>{{$item->code}}</td>
                           <td id="gettitle_{{ $item->id }}">{{$item->name}}</td>
                            <td>{{$item->category_name}}</td>
                            <td>{{$item->serial}}</td>
<!--                            @if($item->api == 0)
                            <td>No</td>
                            @else
                            <td>{{$item->api_title}}</td>
                            @endif-->
                            <td>{{$item->upc_barcode}}
                            @if($item->has_sub_item==1)<button  data-toggle="modal" data-target="#viewsubitems" onclick="loadsubitems({{  $item->id}})" class="btn btn-primary">Sub item list</button> @endif
                            </td>
                            <td>{{$item->cost}}</td>
                            <!--<td>{{$item->sale_price}}</td>-->
                            <td><a href="{{ url('inventory/item/'.$item->id) }}">{{$quantity}}</a></td>
                             <td>
                              @if($item->status == 1)
                                <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}')" checked data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @else
                               <input type="checkbox" onchange="changeItemStatus('{{ $item->id }}')" data-toggle="toggle" data-on="Active" data-off="Disable" data-onstyle="success" data-offstyle="danger">
                                @endif
                                </td>
                            <td>
                                <div style="width: 130px">
                                <a href="{{ url('item/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('item/copy/'.$item->id) }}" onclick="return confirm('Are you sure you want to copy item # {{ $item->id }}.')" class="btn btn-primary"><i class="fa fa-copy"></i></a>
                                <a href="{{ url('item/price/recalculate/'.$item->id) }}" onclick="return confirm('Are you sure you want to recalculate price of {{ $item->name }}.')" class="btn btn-primary"><i class="fa fa-calculator "></i></a>
                                {{-- <a href="#" data-toggle="modal" data-target="#viewprice" onclick="loadpriceAjax({{  $item->id}})" class="btn btn-primary"><i class="fa fa-eye"></i></a> --}}
                                <a href="{{ url('item/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete item # {{ $item->id }}.')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <button  data-toggle="modal" data-target="#viewbarcode" onclick="generatebarcode({{  $item->id}})" class="btn btn-primary">Generate Barcode</button>
                                <button  data-toggle="modal" data-target="#viewbarLog" onclick="loadLogAjax({{  $item->id}})" class="btn btn-primary">View Log</button>
                                </div>
                               
                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

                </div></div>

            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>
<!-- Modal -->
@include('front/item/log_view_modal')
<div class="table-responsive" style=" height: 400px;
display: inline-block;
overflow: auto; ">
<div class="modal fade" id="viewprice" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title sattitle" ></h4>
    </div>
    <div class="modal-body" id="itempricestatus">
       
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  
</div>
</div>

<div class="modal fade" id="viewsubitems" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title sattitle" ></h4>
        </div>
        <label>Enter Barcode To update</label>
        <div class="modal-body" id="itemslist">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
    </div>
    <div class="modal fade" id="viewbarcode" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title sattitle" ></h4>
            </div>
            
            <div class="modal-body" id="itemslistbarcode">
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <a href="" class="btn btn-success btn_print btn-sm nbtnprint" id="newbtnhref"><i class="fa fa-print"></i> Print</a>
            </div>
          </div>
          
        </div>
        </div>

        <div class="modal fade" id="autogeneratebarcode" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title sattitle" ></h4>
                </div>
                
                <div class="modal-body" id="">
                  
                    <div class="row">

                        <div class="form-group col-sm-4">
                            <input type="text" class="form-control" name="" id="newbarcode" value="" readonly>
                        </div>
                    </div>          
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-success btn_print btn-sm" onclick="copyToClipboard('#newbarcode')"> Copy barcode</button>
                </div>
              </div>
              
            </div>
            </div>  
</div>


<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@include('front/common/dataTable_js')
<script>
    function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).val()).select();
  document.execCommand("copy");
  $temp.remove();
}
function loadLogAjax(item_id) {
            $.ajax({
                url: "<?php echo url('checkApiItemsLogs/'); ?>" + '/' + item_id,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    if (response != 404) {
                        var modal = document.getElementById('modal_log');
                        modal.style.display = "block";
                        $(modal_log).find('tbody').empty();
                        $(modal_log).find('tbody').append(response);
                    }else{
                        alert('Log Not Found')
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
            });
        }
     function loadpriceAjax(itemid) {
         var getTitle=$('#gettitle_'+itemid).html()
         $('.sattitle').html(getTitle);
       $.ajax({
           url: "<?php echo url('itemts/getprice/'); ?>" + '/' + itemid,
           type: 'get',
           dataType: 'html',
           success: function (response) {
               if (response != 0) {
                   $('#itempricestatus').html(response);
                //    var modal = document.getElementById('viewstatus');
                //    modal.style.display = "block";
                //    $('#item_body').find('tbody').empty();
                //    $('#item_body').find('tbody').append(response);
               }
           },
           error: function (xhr, status, response) {
               alert(response);
           }
       });
       
   }
   function generatebarcode(itemid) {
         var getTitle=$('#gettitle_'+itemid).html()
         $('.sattitle').html(getTitle);
         $('#newbtnhref').addClass('nbtnprint')
       $.ajax({
           url: "<?php echo url('itemts/generatebarcode/'); ?>" + '/' + itemid,
           type: 'get',
           dataType: 'html',
           success: function (response) {
               if (response != 0) {
                   $('#itemslistbarcode').html(response);
                //    var modal = document.getElementById('viewstatus');
                //    modal.style.display = "block";
                //    $('#item_body').find('tbody').empty();
                //    $('#item_body').find('tbody').append(response);
               }
           },
           error: function (xhr, status, response) {
               alert(response);
           }
       });
   }
   function autogeneratebarcode() {
    $('#newbarcode').val("");
    $.ajax({
           url: "<?php echo url('itemts/autogeneratecode/'); ?>",
           type: 'get',
           dataType: 'html',
           success: function (response) {
               if (response != 0) {
                   $('#newbarcode').val(response);
                //    var modal = document.getElementById('viewstatus');
                //    modal.style.display = "block";
                //    $('#item_body').find('tbody').empty();
                //    $('#item_body').find('tbody').append(response);
               }
           },
           error: function (xhr, status, response) {
               alert(response);
           }
       });

   }
   function loadsubitems(itemid) {
         var getTitle=$('#gettitle_'+itemid).html()
         $('.sattitle').html(getTitle);
       $.ajax({
           url: "<?php echo url('item/getsubitems/'); ?>" + '/' + itemid,
           type: 'get',
           dataType: 'html',
           success: function (response) {
               if (response != 0) {
                   $('#itemslist').html(response);
                //    var modal = document.getElementById('viewstatus');
                //    modal.style.display = "block";
                //    $('#item_body').find('tbody').empty();
                //    $('#item_body').find('tbody').append(response);
               }
           },
           error: function (xhr, status, response) {
               alert(response);
           }
       });
   }
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export To Excel',
                title: 'item_list',
                exportOptions: {
                    columns: [1,2,3,4,5,6,7,8,9,10]
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Export To PDF',
                title: 'item_list',
               exportOptions: {
                    columns: [1,2,3,4,5,6,7,8,9]
                }
            }
        ]
        });
    });
    
    function changeItemStatus(item_id) {

        $.ajax({
            url: "<?php echo url('item/status/'); ?>" + '/0/' + item_id,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                console.log(response);
                if (response == 1) {
//                    var modal = document.getElementById('myModal');
//                    modal.style.display = "block";
//                    alert(response);

                } else if (response == 0) {

                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });



    }
</script>
<script>
  $(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'Active',
      off: 'Disabled'
    });
  })

  $(document).ready(function () {
            $('#category_id').on('select2:select', function (e) {});
            $('#category_id').select2({
              language: {
                noResults: function (params) {
                  alert("This category is not existing");
                }
              }
            });
        });
</script>

@include('front/common/image_modal')


@endsection