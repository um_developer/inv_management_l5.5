@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary col-md-12 ">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Create Item</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal-0','url' => 'item/create','files' => true, 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        <div class="box-body row">
            <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Item Name*</label>
                {!! Form::text('name', Request::input('name') , array('placeholder'=>"Item Name *",'maxlength' => 200,'class' => 'form-control',$required) ) !!}
            </div>

            <div class="form-group col-md-12">
                    <label for="exampleInputEmail1">Item Category*</label>
                    {!!   Form::select('category_id', $categories, Request::input('category_id'), array('class' => 'form-control',$required ))  !!}
            </div>

            @if(count($colors) > 0)
             <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Add Multiple Item Colors</label>
                    {!! Form::select('item_colors[]', $colors,$colors,['class' => 'form-control select2','multiple' => 'multiple']) !!}
            </div>
            @endif
            
             @if(count($sizes) > 0)
             <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Add Multiple Item Sizes</label>
                    {!! Form::select('item_sizes[]', $sizes,$sizes,['class' => 'form-control select2','multiple' => 'multiple']) !!}
                </div>
            </div>
            @endif
            
             @if(count($types) > 0)
             <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Add Multiple Item Types</label>
                    {!! Form::select('item_types[]', $types,$types,['class' => 'form-control select2','multiple' => 'multiple']) !!}
            </div>
            @endif
@if(1 == 2)
            @if(count($tags) > 0)
             <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Add Multiple Item Tags</label>
                    {!! Form::select('tags[]', $tags,$tags,['class' => 'form-control select2','multiple' => 'multiple']) !!}
            </div>
            @endif

            @endif
            

            <div class="form-group has-error col-sm-6">
                    <label for="exampleInputEmail1">Cost*</label>
                    {!! Form::text('cost', Request::input('cost') , array('placeholder'=>"Cost *",'id' => "cost",'maxlength' => 8,'class' => 'form-control',$required) ) !!}
            </div>

            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Item Code/SKU*</label>
                    {!! Form::text('code', Request::input('code') , array('placeholder'=>"Auto generated",'maxlength' => 190,'minlength' => 2,'class' => 'form-control','disabled') ) !!}
            </div>

            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">UPC/Barcode</label>
                    {!! Form::text('upc_barcode', Request::input('upc_barcode') , array('placeholder'=>"UPC/Barcode",'maxlength' => 25,'minlength' => 2,'class' => 'form-control',$required) ) !!}
            </div>
            
            <div class="form-group col-sm-3">
                    <label for="exampleInputEmail1">Serial #</label>
                    {!!   Form::select('serial', array('no' => 'No','yes' => 'Yes'), Request::input('serial'), array('class' => 'form-control',$required ))  !!}
                </div>

            <div class="form-group col-sm-3">
                    <label for="exampleInputEmail1">Item API</label>
                    {!!   Form::select('api', $api_servers, Request::input('api'), array('class' => 'form-control',$required ))  !!}
            </div>

            <div class="form-group col-sm-6">
                <label for="exampleInputEmail1">Increase Price</label>
                {!! Form::checkbox('increase_effect',1,Request::input('increase_effect'), array('id'=>'increase_effect','class' => 'flat-red', 'checked' => 'checked')) !!}
            </div>  
            <div class="form-group col-sm-6">
                <label for="exampleInputEmail1">Decrease Price</label>
                {!! Form::checkbox('decrease_effect',1,Request::input('decrease_effect'), array('id'=>'decrease_effect','class' => 'flat-red')) !!}
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Description</label>
                    {!! Form::textarea('description', Request::input('description') , ['class'=>'form-control', 'rows' => 12, 'cols' => 10] ) !!}
            </div>

            <div class="form-group col-sm-6">

                    <label for="exampleInputEmail1">Item Image</label>
                    <input class="form-control" type='file' id="imgInp" name="image" accept=".png, .jpg, .jpeg" />
                    
                    <div class="avatar-upload">
                        <div id="imagePreview__parent-div" class="avatar-preview__parent">
                            <div id="imagePreview__parent" style="background-image: url('{{ asset('adminlte/dist/img/no-preview.jpg')}}');"></div>
                            <div class="delete-area hidden ">
                            <a href="#" class="btn btn-xs btn-danger col-md-12" class="gimgdelete">
                                <i class="fa fa-trash"></i>
                                <span>Delete</span>
                            </a>
                        </div>
                        </div>
                    </div>    
            </div>

             <div class="form-group col-sm-6">

                    <label for="exampleInputEmail1">Item Gallery Image</label>
                    <input class="form-control mb10" type='file' id="gallery_img" name="gallery_image[]" multiple accept=".png, .jpg, .jpeg" />                            
                    <div class="avatar-upload">
                        <div id="preview"></div>
                        
                    </div>
            </div>
            <div class="tags-control col-md-12">
            <div class="form-group input-group0">
                <label>Search Tags</label>    
                <input id="tagControlSearhInput" type="text" placeholder="Search Tag..." class="form-control" />
                <!-- <span class="input-group-btn">
                    <div class="btn btn-danger" id="tagControl__removeTagBtn" >Add Selected Tag</div>
                </span> -->
            </div>
            <div class="form-group">
                <label>Select Tags</label>
                <select id="tagControl__tagList" name="tags[]" multiple class="form-control multiselect" style="height: 200px">
                    <?php foreach($tags as $tag){ ?>
                        <?php if(count($tag->child) > 0){ ?>
                            <option class="not-clickable"><?php echo $tag->title; ?></option>
                                <?php foreach($tag->child as $row){ ?>
                                    <option class="sub-tag" value="<?php echo $row->id; ?>"><?php echo $row->title; ?></option>
                                <?php } ?>
                        <?php }else{ ?>
                            <option value="<?php echo $tag->id; ?>"><?php echo $tag->title; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Create">
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>
</section>
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js')}}"></script>	
  
<script type="text/javascript">
    $(document).ready(function () {
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-purple',
            radioClass: 'iradio_flat-purple'
        });
        $("#cost").on('keyup change', function () {
            calculateSalePrice();
        });
    });
    function calculateSalePrice() {

        var cost = parseFloat($("#cost").val());
        if (cost > 0) {
           $("#sale_price").val((cost + (cost * 1)).toFixed(2));
            $("#sale_price_2").val((cost + (cost * 0.50)).toFixed(2));
            $("#sale_price_3").val((cost + (cost * 0.25)).toFixed(2));
            $("#sale_price_4").val((cost + (cost * 0.10)).toFixed(2));
            $("#sale_price_5").val((cost + (cost * 0.05)).toFixed(2));
        } else {
            $("#sale_price").val(0);
            $("#sale_price_2").val(0);
            $("#sale_price_3").val(0);
            $("#sale_price_4").val(0);
            $("#sale_price_5").val(0);

        }
    }
</script>

<script>
    $(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview__parent').css('background-image', 'url(' + e.target.result + ')');
//                    $('#imagePreview__child').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview__parent').hide();
//                    $('#imagePreview__child').hide();
                    $('#imagePreview__parent').fadeIn(650);
//                    $('#imagePreview__child').fadeIn(650);
                    console.log(e.target.result);
                }


                $(".avatar-preview__parent").find(".delete-area").removeClass("hidden");
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp").change(function () {
            readURL(this);
        });
        $("#gimgInp").change(function () {
            readURL(this);
        });
    });


    function previewImages() {

        var preview = document.querySelector('#preview');

        if (this.files) {
        [].forEach.call(this.files, readAndPreview);
        }

        function readAndPreview(file) {

        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
            return alert(file.name + " is not an image");
        } // else...
        
            var reader = new FileReader();
            
            reader.addEventListener("load", function() {
                var image = new Image();
                image.height = 100;
                image.title  = file.name;
                image.src    = this.result;
                holder = preview.appendChild(image);

                $(holder).wrap("<div class='thumbnail'></div>");
    
                $("#preview .thumbnail:last-child").append(`
                        <div class="delete-area ">
                            <a href="#" class="btn btn-xs btn-danger col-md-12" class="gimgdelete">
                                <i class="fa fa-trash"></i>
                                <span>Delete</span>
                            </a>
                        </div>
                `);
            });
            
            reader.readAsDataURL(file);
        
        }

}

document.querySelector('#gallery_img').addEventListener("change", previewImages);

 $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        });

</script>
@endsection