@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Update Item</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal','url' => 'item/update','files' => true, 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        {!! Form::hidden('id', $model->id , array('placeholder'=>"SKU",'maxlength' => 8,'class' => 'form-control') ) !!}
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Item Name*</label>
                    {!! Form::text('name', $model->name , array('placeholder'=>"Item Name *",'maxlength' => 200,'class' => 'form-control',$required) ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Item Category*</label>
                    {!!   Form::select('category_id', $categories, $model->category_id, array('class' => 'form-control',$required ))  !!}
                </div>
            </div>

            @if($model->parent_item_id == '0')
            @if(count($colors) > 0)
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Add Multiple Item Colors</label>
                    {!! Form::select('item_colors[]', $colors,$item_colors,['class' => 'form-control select2','multiple' => 'multiple']) !!}
                </div>
            </div>
            @endif

            @if(count($sizes) > 0)
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Add Multiple Item Sizes</label>
                    {!! Form::select('item_sizes[]', $sizes,$item_sizes,['class' => 'form-control select2','multiple' => 'multiple']) !!}
                </div>
            </div>
            @endif

            @if(count($types) > 0)
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Add Multiple Item Types</label>
                    {!! Form::select('item_types[]', $types,$item_types,['class' => 'form-control select2','multiple' => 'multiple']) !!}
                </div>
            </div>
            @endif
            @endif
            <div class="form-group has-error">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Cost*</label>
                    {!! Form::text('cost', $model->cost , array('placeholder'=>"Cost *",'maxlength' => 8,'id' => "cost",'class' => 'form-control',$required) ) !!}
                    <a href="{{ url('item/price/recalculate/'.$model->id) }}" onclick="return confirm('Are you sure you want to recalculate price of {{ $model->name }}.')" class="btn btn-primary"><i class="fa fa-calculator "></i> Recalculate Template Price</a>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Item Code/SKU*</label>
                    {!! Form::text('code', $model->code , array('placeholder'=>"Item Code",'maxlength' => 190,'minlength' => 2,'class' => 'form-control',$required) ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">UPC/Barcode</label>
                    {!! Form::text('upc_barcode', $model->upc_barcode , array('placeholder'=>"UPC/Barcode", 'maxlength'=>'25' , 'minlength'=>2,'class' => 'form-control') ) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Serial #</label>
                    {!!   Form::select('serial', array('no' => 'No','yes' => 'Yes'), $model->serial, array('class' => 'form-control',$required ))  !!}
                </div>

                <div class="col-sm-6">
                    <label for="exampleInputEmail1">Item API</label>
                    {!!   Form::select('api', $api_servers, $model->api, array('class' => 'form-control',$required ))  !!}

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Description</label>
                    {!! Form::textarea('description', $model->description , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Item Image</label>
                    <input class="form-control" type='file' id="imgInp" name="image" accept=".png, .jpg, .jpeg" />
                    <div class="avatar-upload">
                        <div id="imagePreview__parent-div" class="avatar-preview__parent">
                            @if($model->image == '')
                            <div id="imagePreview__parent" style="background-image: url('');"></div>
                            @else
                            <div id="imagePreview__parent" style="background-image: url('{{ asset('laravel/public/uploads/items').'/'.$model->image}}');"></div>
                            @endif
                        </div>
                    </div>    
                </div>
            </div>
           <style>
/* .button {
    display: none;
}

img:hover + .button, .button:hover {
    display: inline-block;
}
} */
.hovereffect {
  width: 50%;
  /* height: 100%; */
  float: left;
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: default;
}

.hovereffect .overlay {
  position: absolute;
  overflow: hidden;
  width: 50%;
  height: 50%;
  /* left: 10%; */
  top: 10%;
  border-bottom: 1px solid #FFF;
  border-top: 1px solid #FFF;
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: scale(0,1);
  -ms-transform: scale(0,1);
  transform: scale(0,1);
}

.hovereffect:hover .overlay {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
}

.hovereffect img {
  display: block;
  position: relative;
  -webkit-transition: all 0.35s;
  transition: all 0.35s;
}

.hovereffect:hover img {
  filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feComponentTransfer color-interpolation-filters="sRGB"><feFuncR type="linear" slope="0.6" /><feFuncG type="linear" slope="0.6" /><feFuncB type="linear" slope="0.6" /></feComponentTransfer></filter></svg>#filter');
  filter: brightness(0.6);
  -webkit-filter: brightness(0.6);
}

.hovereffect h2 {
  text-transform: uppercase;
  text-align: center;
  position: relative;
  font-size: 17px;
  background-color: transparent;
  color: #FFF;
  padding: 1em 0;
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(0,-100%,0);
  transform: translate3d(0,-100%,0);
}

.hovereffect a, .hovereffect p {
  color: black;
  padding: 1em 0;
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(0,100%,0);
  transform: translate3d(0,100%,0);
}

.hovereffect:hover a, .hovereffect:hover p, .hovereffect:hover h2 {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}
</style>


@if(1==2)            
<div class="form-group">
                <div class="col-sm-12">
                    <label for="exampleInputEmail1">Item Gallery Image</label>
                    <input class="form-control" type='file' id="gallery_img" name="gallery_image[]" multiple accept=".png, .jpg, .jpeg" />
                    @if(count($galleryimages) > 0)
    
                    @foreach ($galleryimages as $item)
                    <div class="hovereffect" id="gallery_item_{{$item->id}}" style="width: 50%;">
                        <img src="{{ asset('laravel/public/uploads/items/gallery').'/'.$item->image}}" style="width: 50%;" data-id="{{$item->id}}" class="gimgdelete img-responsive">
                            <div class="overlay">
                                <p>
                                    <a href="#" data-id="{{$item->id}}" class="gimgdelete">Delete</a>
                                </p>
                            </div>
                    </div> --}}
                    {{-- <span class="container" id="gallery_item_{{$item->id}}">
                    <img src="{{ asset('laravel/public/uploads/items/gallery').'/'.$item->image}}" style="width: 20%;" data-id="{{$item->id}}" class="gimgdelete">
                    <button class="btn">Button</button> --}}
                    {{-- <a class="button" href="#" onClick="getMessage($item->id)" id="delete"></a> --}}
                    {{-- onclick="deleteImage('{{$item->id}}')" --}}
                    {{-- </span>
                    @endforeach
                    @endif
                    <div id="preview"></div>
                </div>
            </div> @endif

        </div>
        <!-- /.box-body -->
        @if($model->parent_item_id == '0')
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
        </div>
        @endif

        <div class="box-footer">
            @if(count($subitems) > 0)
            SUB ITEMS LIST
            @foreach($subitems as $sub_item)
            <a href="{{ url('item/edit/'.$sub_item->id) }}