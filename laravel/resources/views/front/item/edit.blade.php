@extends('customer_template_2')

@section('content')


<?php
$required = 'required';
?>
<section id="form">

    <div class="box box-primary col-md-12">
        <div class="box-header with-border text-center">
            <h3 class="box-title">Update Item</h3>
        </div>
        <!-- /.box-header -->
        @if (session('success'))
        <div class="alert alert-success" id="customMsg">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(array( 'class' => 'form-horizontal-0','url' => 'item/update','files' => true, 'method' => 'post')) !!}
        <input type="hidden"  name="_token" value="{{ csrf_token() }}">
        {!! Form::hidden('id', $model->id , array('placeholder'=>"SKU",'maxlength' => 8,'class' => 'form-control') ) !!}
        <div class="box-body row">
            <div class="form-group col-md-12">
                    <label for="exampleInputEmail1">Item Name*</label>
                    {!! Form::text('name', $model->name , array('placeholder'=>"Item Name *",'maxlength' => 200,'class' => 'form-control',$required) ) !!}
            </div>

            <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">Item Category*</label>
                    {!!   Form::select('category_id', $categories, $model->category_id, array('class' => 'form-control',$required ))  !!}
            </div>

            @if($model->parent_item_id == '0')
            @if(count($colors) > 0)
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Add Multiple Item Colors</label>
                    {!! Form::select('item_colors[]', $colors,$item_colors,['class' => 'form-control select2','multiple' => 'multiple']) !!}
            </div>
            @endif

            @if(count($sizes) > 0)
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Add Multiple Item Sizes</label>
                    {!! Form::select('item_sizes[]', $sizes,$item_sizes,['class' => 'form-control select2','multiple' => 'multiple']) !!}
            </div>
            @endif

            @if(count($types) > 0)
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Add Multiple Item Types</label>
                    {!! Form::select('item_types[]', $types,$item_types,['class' => 'form-control select2','multiple' => 'multiple']) !!}
            </div>
            @endif

            @endif
@if(1 == 2)
            @if(count($tags) > 0)
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Add Multiple Item Tags</label>
                    {!! Form::select('tags[]', $tags,$item_tags,['class' => 'form-control select2','multiple' => 'multiple']) !!}
            </div>
            @endif
@endif

            <div class="form-group has-error col-sm-6">
                    <label for="exampleInputEmail1">Cost*</label>
                    {!! Form::text('cost', $model->cost , array('placeholder'=>"Cost *",'maxlength' => 8,'id' => "cost",'class' => 'form-control',$required) ) !!}
                    <br>
                    <a href="{{ url('item/price/recalculate/'.$model->id) }}" onclick="return confirm('Are you sure you want to recalculate price of {{ $model->name }}.')" class="btn btn-primary"><i class="fa fa-calculator "></i> Recalculate Template Price</a>
            </div>


            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Item Code/SKU*</label>
                    {!! Form::text('code', $model->code , array('placeholder'=>"Item Code",'maxlength' => 190,'minlength' => 2,'class' => 'form-control','disabled') ) !!}
            </div>
            @if($model->parent_item_id == '0')
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">UPC/Barcode</label>
                    {!! Form::text('upc_barcode', $model->upc_barcode , array('placeholder'=>"UPC/Barcode", 'maxlength'=>'25' , 'minlength'=>2,'class' => 'form-control') ) !!}
            </div>
@else
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">UPC/Barcode</label>
                    {!! Form::text('upc_barcode', $model->upc_barcode , array('placeholder'=>"UPC/Barcode", 'maxlength'=>'25' , 'minlength'=>2,'class' => 'form-control', 'id' => 'upc_barcode') ) !!}
            </div>
@endif
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Serial #</label>
                    {!!   Form::select('serial', array('no' => 'No','yes' => 'Yes'), $model->serial, array('class' => 'form-control',$required ))  !!}
            </div>

            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Item API</label>
                    {!!   Form::select('api', $api_servers, $model->api, array('class' => 'form-control',$required ))  !!}

            </div>
            
            <div class="form-group col-sm-6">
                <label for="exampleInputEmail1">Increase Price</label>
                {!! Form::checkbox('increase_effect',1,$model->increase_effect,Request::input('increase_effect'), array('id'=>'increase_effect','class' => 'flat-red')) !!}
            </div>  
            <div class="form-group col-sm-6">
                <label for="exampleInputEmail1">Decrease Price</label>
                {!! Form::checkbox('decrease_effect',1,$model->decrease_effect,Request::input('decrease_effect'), array('id'=>'decrease_effect','class' => 'flat-red')) !!}
            </div>            
            
            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Description</label>
                    {!! Form::textarea('description', $model->description , ['class'=>'form-control', 'rows' => 12, 'cols' => 10] ) !!}

            </div>

            <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Item Image</label>
                    <input class="form-control" type='file' id="imgInp" name="image" accept=".png, .jpg, .jpeg" />
                    <div class="avatar-upload">
                        <div id="imagePreview__parent-div" class="avatar-preview__parent">
                            @if($model->image == '')
                            <div id="imagePreview__parent" style="background-image: url('');"></div>
                            @else
                            <div id="imagePreview__parent" style="background-image: url('{{ asset('laravel/public/uploads/items').'/'.$model->image}}');"></div>
                            <div class="delete-area">
                                <a href="javascript:;" class="btn btn-xs btn-danger col-md-12 gimgdelete">
                                    <i class="fa fa-trash"></i>
                                    <span>Delete</span>
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
            </div>
            



         <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1">Item Gallery Image</label>
                    <input class="form-control mb10" type='file' id="gallery_img" name="gallery_image[]" multiple accept=".png, .jpg, .jpeg" />
                    @if(count($galleryimages) > 0)
    
                    @foreach ($galleryimages as $item)
                    <div class="col-md-12 thumbnail" id="gallery_item_{{$item->id}}" >
                        <img src="{{ asset('laravel/public/uploads/items/gallery').'/'.$item->image}}" data-id="{{$item->id}}" class="gimgdelete img-responsive">
                            <div class="delete-area">
                                <a href="javascript:;" class="btn btn-xs btn-danger col-md-12 gimgdelete" data-id="{{$item->id}}" >
                                    <i class="fa fa-trash"></i>
                                    <span>Delete</span>
                                </a>
                            </div>
                    </div> 
                    <!-- <span class="container" id="gallery_item_{{$item->id}}">
                    <img src="{{ asset('laravel/public/uploads/items/gallery').'/'.$item->image}}" style="width: 20%;" data-id="{{$item->id}}" class="gimgdelete">
                    <button class="btn"></button>
                    <a class="button" href="#" onClick="getMessage($item->id)" id="delete"></a>
                    onclick="deleteImage('{{$item->id}}')"
                     </span> -->
                    @endforeach
                    @endif
                    <div id="preview"></div>
                </div>
            </div> 
        
        
        
        
        <div class="tags-control col-md-12">
            <div class="form-group input-group0">
                <label>Search Tags</label>
                <input id="tagControlSearhInput" type="text" placeholder="Search Tag..." class="form-control" />
                <!-- <span class="input-group-btn">
                    <div class="btn btn-danger" id="tagControl__removeTagBtn" >Add Selected Tag</div>
                </span> -->
            </div>
            
            <div class="form-group">
            
                <label>Selected Tags</label>
                <div class="clearfix"></div>

                <select id="tagControl__tagList" name="tags[]" multiple class="form-control multiselect" style="height: 200px">
                    <?php foreach($tags as $tag){ ?>
                    <?php if(count($tag->child) > 0){ ?>
                    <option class="not-clickable"><?php echo $tag->title; ?></option>
                    <?php foreach($tag->child as $row){ ?>
                        <option class="sub-tag" value="<?php echo $row->id; ?>"><?php echo $row->title; ?></option>
                        <!-- <option class="sub-tag">Sub Tag 2</option>
                        <option class="sub-tag">Sub Tag 3</option>
                        <option class="sub-tag">Sub Tag 4</option> -->
                    <?php } ?>
                    <?php }else{ ?>
                    <option value="<?php echo $tag->id; ?>"><?php echo $tag->title; ?></option>
                    <?php } ?>
                    <?php } ?>
                    <!-- <option>Main Tag 2 (Main  Delete)</option> -->
                    
                    <!-- <option class="not-clickable">Science ( has sub tags )</option>
                        <option class="sub-tag">Biology</option>
                        <option class="sub-tag">Physics</option>
                        <option class="sub-tag">Maths</option>
                        
                        <option class="not-clickable">Main Tag 3 ( has sub tags )</option>
                        <option class="sub-tag">Sub Tag 1</option>
                        <option class="sub-tag">Sub Tag 2</option>
                        
                         <option>Main Tag 12 (Main Tag)</option>
                    <option>Main Tag 44 (Main Tag)</option> -->
                    
                </select>
            </div>
        </div> 


        <div class="tags-control col-md-12">
            <input type="hidden" name="deletedTag[]" id="selectedTag">
            <div class="form-group legends">
             
                <label>Selected Tags</label>

                <div class="clearfix"></div>

                <?php foreach($item_tags as $row){ ?>
                    <div class="button0 btn-deleteTag"><?php echo $row->title; ?><span><i data-id="<?php echo $row->id; ?>" class="fa fa-close deleteTag js-deleteTag"></i></span></div> 
                <?php } ?>
            
            </div>
        </div>


 
@if(1==2)
        <div class="tags-sortable-area col-md-12">
            <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
            <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
            <h4>Sortable Area Move Thise section to New Page</h4>
        <div class='sortable'>
            <div class='item1'><span class="handle">Main Tag 1</span> </div>
            <div class='item1 has-child'>
                <span class="handle">Main Tag 2</span>
                <div class='sub-tag item2'>Sub Tag 1</div>
                <div class='sub-tag item2'>Sub Tag 2</div>
                <div class='sub-tag item2'>Sub Tag 3</div>
            </div>
            <div class='item1'> <span class="handle">Main Tag 3</span> </div>
        </div>
            <script>
                $(document).ready(function() {

    //                    $("#sortable2").sortable()

                        $(".has-child").sortable({
                            items: ".item2"
                        });
                        $(".sortable").sortable({
                            handle: ".handle"
                        });
                });
            </script>
        </div>
@endif
 

        
        <!-- /.box-body -->
        @if($model->parent_item_id == '0')
        <div class="box-footer">
            <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
        </div>
        @endif

        <div class="box-footer">
            @if(count($subitems) > 0)
            SUB ITEMS LIST
            @foreach($subitems as $sub_item)
            <a href="{{ url('item/edit/'.$sub_item->id) }}"><h5>{{ $sub_item->name }}</h5></a>
            @endforeach
            @endif
        </div>
        <!-- /.box-footer -->
        {!! Form::close() !!} 
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</section>












<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js')}}"></script>	
<script>
   
    $(document).ready(function () {


        $('#upc_barcode').on('focusout', function() {
    var x = $('#upc_barcode').val();
    // alert(x)
    var id = '<?php echo request()->route('id') ?>'
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    $.ajax({
        url: "<?php echo url('item/upcUpdate/'); ?>",
        type: 'post',
        data: {id: id, code:x},
        success: function (response) {
            if (response == '1') {
                alert('updated')
            }else if(response == '2'){
                alert('Try again')
            }else if(response == '3'){
                alert('please enter valid code')
            }else{
                alert('updated.'+response) 
            }

        },
        error: function (xhr, status, response) {
            console.log(response);
        }
    });
});




        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-purple',
            radioClass: 'iradio_flat-purple'
        });
    });
    

    $(".gimgdelete").click(function(){
    var el = $(this), image =  $(this).data('id');

        $.ajax({
            url: "<?php echo url('item/image/delete/'); ?>" + '/' + image,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                    if (response != 0) {
                        $("#gallery_item" + '_' + response).remove();
                    }
                },
                error: function (xhr, status, response) {
                    alert(response);
                }
        });

    })
    var idsd = [];
    $(".deleteTag").click(function(){
    var tag_id =  $(this).data('id');
    // alert(tag_id)

    idsd.push($(this).data('id'))
    $("#selectedTag").val(idsd)
    
    })
 </script>
<script>
    $(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview__parent').css('background-image', 'url(' + e.target.result + ')');
//                    $('#imagePreview__child').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview__parent').hide();
//                    $('#imagePreview__child').hide();
                    $('#imagePreview__parent').fadeIn(650);
//                    $('#imagePreview__child').fadeIn(650);
                    console.log(e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp").change(function () {
            readURL(this);
        });
    });

    function previewImages() {

var preview = document.querySelector('#preview');

if (this.files) {
  [].forEach.call(this.files, readAndPreview);
}


function wrap(el, wrapper) {
    el.parentNode.insertBefore(wrapper, el);
    wrapper.appendChild(el);
}


function readAndPreview(file) {

        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
            return alert(file.name + " is not an image");
        } // else...
        
        var reader = new FileReader();
        
        reader.addEventListener("load", function() {
            var image = new Image();
            
            image.height = 100;
            image.title  = file.name;
            image.src    = this.result;
            
            holder = preview.appendChild(image);

                $(holder).wrap("<div class='thumbnail'></div>");
    
                $("#preview .thumbnail:last-child").append(`
                        <div class="delete-area ">
                            <a href="javascript:;" class="btn btn-xs btn-danger col-md-12" class="gimgdelete">
                                <i class="fa fa-trash"></i>
                                <span>Delete</span>
                            </a>
                        </div>
                `);
        });
        
        reader.readAsDataURL(file);
    
    }

}

document.querySelector('#gallery_img').addEventListener("change", previewImages);

$('#cost').on('focusout', function() {
    var cost = $('#cost').val();
    var id = '<?php echo request()->route('id') ?>'
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    $.ajax({
        url: "<?php echo url('item/priceUpdate/'); ?>",
        type: 'post',
        data: {id: id, cost:cost},
        success: function (response) {
            alert(response)
        },
        error: function (xhr, status, response) {
            console.log(response);
        }
    });
});
$('#cost').keydown(function(e) {
    var key = e.which;
    if (key == 9) {
        var cost = $('#cost').val();
        var id = '<?php echo request()->route('id') ?>'
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            url: "<?php echo url('item/priceUpdate/'); ?>",
            type: 'post',
            data: {id: id, cost:cost},
            success: function (response) {
                alert(response)
            },
            error: function (xhr, status, response) {
                console.log(response);
            }
        });
    }
});
 

</script>
@endsection