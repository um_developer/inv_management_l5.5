@extends('customer_template')

@section('content')
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
       
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Items Without Purchase Orders</h3>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <div class="col-md-12">

                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>IMG</th>
                            <th>ID</th>
                            <th>SKU/Item Code</th>
                            <th>Item Name</th>
                            <th>Category</th>
                            <!--<th>Serial</th>-->
                            <!--<th>API Name</th>-->
                            <th>UPC/Barcode</th>
                            <th>Cost</th>
                            <!--<th>S.P</th>-->
                            <!--<th>QTY</th>-->
                            <!--<th>State</th>-->
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)

                        <tr>
                            <?php
                            $quantity = 0;
                           // if ($item->warehouse_quantity != '') {
                                $quantity = $item->warehouse_quantity + $item->inventory_quantity + $item->package_quantity;
                            //}
                            ?>


                            @if($item->image == '')
                            <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items/no-image.png')}}" height="42" width="42"></a></td>
                            @else
                            <td><a href='#' onclick="showItemModal('{{$item->image}}')"><img src="{{ asset('laravel/public/uploads/items').'/'.$item->image}}" height="42" width="42"></a></td>
                            @endif
                            <td>{{$item->id}}</td>
                            <td>{{$item->code}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->category_name}}</td>
                            <!--<td>{{$item->serial}}</td>-->
<!--                            @if($item->api == 0)
                            <td>No</td>
                            @else
                            <td>{{$item->api_title}}</td>
                            @endif-->
                            <td>{{$item->upc_barcode}}</td>
                            <td>{{$item->cost}}</td>
                            <!--<td>{{$item->sale_price}}</td>-->
                            <!--<td><a href="{{ url('inventory/item/'.$item->id) }}">{{$quantity}}</a></td>-->
                            
                            <td>
                                <div style="width: 130px">
                                <a href="{{ url('item/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                               </div>
                               
                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

                </div></div>

            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>



<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@include('front/common/dataTable_js')
<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
        
        });
    });
    
    function changeItemStatus(item_id) {

        $.ajax({
            url: "<?php echo url('item/status/'); ?>" + '/0/' + item_id,
            type: 'get',
            dataType: 'html',
            success: function (response) {
                console.log(response);
                if (response == 1) {
//                    var modal = document.getElementById('myModal');
//                    modal.style.display = "block";
//                    alert(response);

                } else if (response == 0) {

                }
            },
            error: function (xhr, status, response) {
                alert(response);
            }
        });



    }
</script>
<script>
  $(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'Active',
      off: 'Disabled'
    });
  })

  $(document).ready(function () {
            $('#category_id').on('select2:select', function (e) {});
            $('#category_id').select2({
              language: {
                noResults: function (params) {
                  alert("This category is not existing");
                }
              }
            });
        });
</script>

@include('front/common/image_modal')


@endsection



