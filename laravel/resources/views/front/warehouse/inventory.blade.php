@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        <br>
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="box">

            <div class="box-header">
                <h3 class="box-title">{{ $warehouse[0]->name }} Inventory</h3>
                <a href="{{ URL::to('warehouse/generate/report/'.$warehouse[0]->id) }}"><button class="btn btn-success   pull-right col-sm-offset-1"><i class="fa fa-download"></i> Download Report</button></a>

         <!--<a href="{{ url('item/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Item</a>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Item ID</th>
                            <!--<th>Item Code</th>-->
                            <th>Item Name</th>
                            <th>Category Name</th>
                             <!--<th>Serial #</th>-->
                           <!--<th>API Name</th>-->
                            <th>SKU/Item Code</th>
                            <th>Quantity</th>
                            <th>Updated Date</th>
                            <!--<th>Sale Price</th>-->

 <!--<th>Actions</th>-->
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->item_id}}</td>
                            <td>{{$item->item_name}}</td>
                           <!--<td>{{$item->name}}</td>-->
                            <td>{{$item->category_name}}</td>
                             <!--<td>{{$item->serial}}</td>-->
                              <!--<td>{{$item->api}}</td>-->
                            <td>{{$item->item_sku}}</td>
                            <td>{{$item->quantity}}</td>
                            <td><?php echo date("d M Y", strtotime($item->updated_at)); ?></td>
                          <!--<td>{{$item->cost}}</td>-->
                          <!--<td>{{$item->sale_price}}</td>-->
        <!--                  <td>
                          <a href="{{ url('item/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                          <a href="{{ url('item/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                          </td>-->
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>

<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        });
    });
</script>
@endsection



