@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
@endif
<section id="form" class="mt30 mb30 col-sm-12 p0">
     <div class="row">
         
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Warehouse Listing</h3>
              <a href="{{ url('warehouse/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Warehouse</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vendors" class="table table-bordered table-striped">
                  
                <thead>
                   
                <tr>
                  <th>Warehouse ID</th>
                  <th>Warehouse Name</th>
                  <th>Address</th>
                  <th>Phone</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
               
                    @if(count($model) >0)
                    @foreach($model as $item)
                     <tr>
                  <td>{{$item->id}}</td>
                  <td>{{$item->name}}</td>
                  <td>{{$item->address}}</td>
                  <td>{{$item->phone}}</td>
                  <td>
                  <a href="{{ url('warehouse/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                  <a href="{{ url('warehouse/delete/'.$item->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                  <a href="{{ url('warehouse/inventory/'.$item->id) }}" class="btn btn-primary">View Inventory</a>
                  </td>
                </tr>
                    @endforeach
                    @endif
               
                
                </tbody>
                <tfoot>
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
     </div>			
</section>
<script>
    $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
//       "dom": '<"toolbar">frtip'
        });
</script>
@endsection



