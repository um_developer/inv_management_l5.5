@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Update Warehouse</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'warehouse/update', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                <div class="form-group">
                  <div class="col-sm-12">
                      {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
                       <label for="exampleInputEmail1">Warehouse Name</label>
                     {!! Form::text('name', $model->name , array('placeholder'=>"Warehouse Name *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>

                    <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Address</label>
                     {!! Form::text('address', $model->address , array('placeholder'=>"Address",'maxlength' => 500,'class' => 'form-control') ) !!}
                  </div>
                </div>
                     <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Phone</label>
                     {!! Form::text('phone', $model->phone , array('placeholder'=>"Phone",'maxlength' => 20,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                 
                  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
        			
</section>

@endsection