@extends('customer_template')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    <?php //print_r($date_range); die(); ?>

    <div class="box-header with-border text-center">
        <h3 class="box-title">{{ucwords($customer->name)  .' ( $'.$customer->balance .' )'}}</h3>
        <a href="{{ url('/customers') }}" style="float: right;width: 15%;" class="btn btn-primary btn-block btn-flat">Customer Listing</a>
    </div> 
    @include('front/customer/filter')



    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" id="tabs">
                <li <?php if (isset($activeTab) && $activeTab == 'info') { ?> class="active" <?php } else if (!isset($activeTab) || empty($activeTab)) { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#info" data-toggle="tab" aria-expanded="false">Basic Info</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'payments') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#payments" data-toggle="tab" aria-expanded="false">Payments</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'transactions') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#transactions" data-toggle="tab" aria-expanded="false">Transactions</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'invoices') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#invoices" data-toggle="tab" aria-expanded="true">Invoices</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'credit_memo') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#credit_memo" data-toggle="tab" aria-expanded="true">Credit Memo</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'settings') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#settings" data-toggle="tab" aria-expanded="true">Sale / Profit</a></li>
                <li <?php if (isset($activeTab) && $activeTab == 'statement') { ?> class="active" <?php } else { ?> class="" <?php } ?>><a href="#statement" data-toggle="tab" aria-expanded="true">Statement Report</a></li>
            </ul>
            <div class="tab-content">



                <!-- hdjhs -->

                <?php if (isset($activeTab) && $activeTab == 'statement') {
                    ?>
                    <div class="tab-pane active" id="statement">
                    <?php } else { ?>
                        <div class="tab-pane" id="statement">
                        <?php } ?>
                        <!-- <div class="tab-pane" id="settings"> -->
                        <div class="box-body">

                            <div class="table-responsive">
                                <div class="col-md-12">

                                    <table id="statement1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>S.#</th>
                                                <th>Transaction ID</th>
                                                <th>Customer Name</th>
                                                <th>Start Bal</th>
                                                <th>Amount</th>
                                                <th>End Bal</th>
                                                <th>Transaction Type</th>
                                                <th>Transaction Details </th>
                                                <!--<th>Transaction Date</th>-->
                                                <th>Statement Date</th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if(count($statement) >0)
                                            <?php $i = 1; ?>
                                            @foreach($statement as $item)
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $item->id }}</td>
                                                <td>{{ $item->customer_name }}</td> 
                                                <td>{{number_format((float)$item->new_previous_balance, 2, '.', '')  }}
                                                </td>
                                                <td>{{ $item->new_total_modified_amount }}</td>
                                                <td>
                                                    {{number_format((float)$item->new_updated_balance, 2, '.', '')  }}

                                                </td>
                                                @if($item->payment_type == 'direct')
                                                <td>Payment</td>
                                                @else
                                                <td>{{ ucfirst(str_replace("_"," ",$item->payment_type)) }}</td>
                                                @endif

                                                @if($item->payment_type == 'direct')
                                                @if($item->modification_count > 0)
                                                <td>PMT{{ $item->payment_type_id }}</td>
                                                @else
                                                <td>{{$item->initials . $item->id}}</td>
                                                @endif
                                                @elseif($item->payment_type == 'invoice')
                                                @if($item->modification_count > 0)
                                                
                                                <td> <a href="{{ url('invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }}</a></td>
                                                @else
                                                <td> <a href="{{ url('invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }}</a></td>
                                                @endif
                                                @else

                                                @if($item->modification_count > 0)
                                                <td> <a href="{{ url('credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }}</a></td>
                                                @else
                                                <td> <a href="{{ url('credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }}</a></td>
                                                @endif

                                                @endif

                                                <td><?php echo date("d M Y", strtotime($item['statement_date'])); ?></td>

                                            </tr>

                                            <?php $i++; ?>

                                            @endforeach

                                            @endif


                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>

                                </div></div>
                        </div>
                    </div>
                    <!-- djskh -->



                    <!-- /.tab-pane -->
                    <?php if (isset($activeTab) && $activeTab == 'payments') { ?>
                        <div class="tab-pane active" id="payments">
                        <?php } else { ?>
                            <div class="tab-pane" id="payments">
                            <?php } ?>
                            <!-- The timeline -->

                            <div class="box">
                                <a href="{{ url('payment/customer/create/'.$customer->id) }}" class="btn btn-primary pull-right">Create Payment</i></a>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <div class="table-responsive">
                                        <div class="col-md-12">

                                            <table id="vendors2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Payment ID</th>
                                                        <th>Customer Name</th>
                                                        <th>Payment Type</th>
                                                         <th>Account Type</th>
                                                        <th>Payment Message</th>
                                                        <th>Amount</th>
                                                        <th>Date</th>
                                                         <th>Statement Date</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                             
                                                    </tr>
                                                </thead>
                                                <tbody>
                            
                                                    @if(count($payments0) >0)
                                                    <?php $i = 1;
                                                    $total_amount = 0;
                                                    ?>
                                                    @foreach($payments0 as $item)
                                                    <tr>
                                                        <td>{{$item->initials . $item->id}}</td>
                                                        <td>{{$item->user_name}}</td>
                                                         <td>{{$item->payment_type_title}}</td>
                                                         <td>{{$item->account_type_title}}</td>
                                                        <td>{{$item->message}}</td>
                                                        <td>{{$item->total_modified_amount}}</td>
                                                        <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                                                        <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                                                        @if($item->status == 'pending')
                                                        <td><a class="btn btn-primary btn-xs"> Pending</a></td>
                                                        @elseif($item->status == 'approved')
                                                        <td><a class="btn btn-success btn-xs"> Approved</a></td>
                                                        @else
                                                        <td><a class="btn btn-warning btn-xs"> Rejected</a></td>
                                                        @endif
                                                        <td>
                                                            @if($item->status == 'pending'  && Auth::user()->role_id == 2)
                                                            <a href="{{ url('payment/status/'.$item->id.'/approved') }}" onclick="return confirm('Are you sure you want to approve this Payment?')" class="btn btn-success">Approve</i></a>
                                                            @endif
                                                            @if($item->modification_count == 0)
                                                             <a href="{{ url('payment/edit/'.$item->id) }}"  class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                             
                                                            <a href="{{ url('payment/delete/customer/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this Payment?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                           
                                                            @else
                                                            PMT{{ $item->payment_type_id }}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <?php 
                                                    $total_amount = $total_amount + $item->total_modified_amount;
                                                    $i++; ?>
                                                    @endforeach
                            
                                                    @endif
                            
                            
                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>

                                        </div></div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <?php if (isset($activeTab) && $activeTab == 'transactions') { ?>
                            <div class="tab-pane active" id="transactions">
                            <?php } else { ?>
                                <div class="tab-pane" id="transactions">
                                <?php } ?>

                                <div class="box-body">

                                    <div class="table-responsive">
                                        <div class="col-md-12">

                                            <table id="vendors1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Transaction ID</th>
                                                        <th>Customer Name</th>
                                                        <th>Start Bal</th>
                                                        <th>Amount</th>
                                                        <th>End Bal</th>
                                                        <th>Transaction Type</th>
                                                        <th>Transaction Details </th>
                                                        <!--<th>Transaction Date</th>-->
                                                        <th>Statement Date</th>

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if(count($transactions) >0)
                                                    <?php $i = 1; ?>
                                                    @foreach($transactions as $item)
                                                    <tr>

                                                        <td>{{ $item->id }}</td>

                                                        <td>{{ $item->customer_name }}</td>

                                                        <td>{{ $item->previous_balance }}</td>

                                                        @if (strpos($item->amount, '-') !== false || $item->payment_type == 'invoice')
                                                        <td>{{ str_replace("-", "", $item->amount) }}</td>
                                                        @else
                                                        <td>{{ '-'.$item->amount }}</td>
                                                        @endif



                                                        <td>{{ $item->updated_balance }}</td>

                                                        @if($item->payment_type == 'direct')
                                                        <td>Payment</td>
                                                        @else
                                                        <td>{{ ucfirst(str_replace("_"," ",$item->payment_type)) }}</td>
                                                        @endif

                                                        @if($item->payment_type == 'direct')
                                                        @if($item->modification_count > 0)
                                                        <td>PMT{{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</td>
                                                        @else
                                                        <td>{{$item->initials . $item->id}}</td>
                                                        @endif
                                                        @elseif($item->payment_type == 'invoice')
                                                        @if($item->modification_count > 0)
                                                        <td> <a href="{{ url('invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</a></td>
                                                        @else
                                                        <td> <a href="{{ url('invoice/'.$item->payment_type_id) }}" target="_blank">Invoice# {{ $item->payment_type_id }}</a></td>
                                                        @endif
                                                        @else

                                                        @if($item->modification_count > 0)
                                                        <td> <a href="{{ url('credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }} (Modified {{ $item->modification_count }})</a></td>
                                                        @else
                                                        <td> <a href="{{ url('credit-memo/'.$item->payment_type_id) }}" target="_blank">Credit Memo# {{ $item->payment_type_id }}</a></td>
                                                        @endif
                                                        @endif

                                                        <td><?php echo date("d M Y", strtotime($item['statement_date'])); ?></td>

                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach

                                                    @endif


                                                </tbody>
                                                <tfoot>

                                                </tfoot>
                                            </table>

                                        </div></div>
                                </div>
                            </div>
                            <?php if (isset($activeTab) && $activeTab == 'credit_memo') { ?> 
                                <div class="tab-pane active" id="credit_memo">
                                <?php } else { ?>
                                    <div class="tab-pane" id="credit_memo">
                                    <?php } ?>
                                    <div class="box">
                                        <a href="{{ url('credit-memo/create') }}" class="btn btn-primary pull-right">Create Credit Memo</i></a>
                                        <div class="box-header">
                                            <h3 class="box-title">Credit Memo</h3>
                                        </div>
                                        <!--/.box-header--> 
                                        <div class="box-body">

                                            <div class="table-responsive">
                                                <div class="col-md-12">

                                                    <table id="vendors" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Invoice ID</th>
                                                                <th>Customer Name</th>
                                                                <th>Memo</th>
                                                                <th>Amount</th>
                                                                <th>Status</th>
                                                                <!--<th>Created Date</th>-->
                                                                <th>Statement Date</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @if(count($credit_memo0) >0)
                                                            @foreach($credit_memo0 as $item)
                                                            <tr>
                                                                <td><a href="{{ url('invoice/'.$item->id) }}">{{$item->id}}</a></td>
                                                                <td><a href="{{ url('customer/edit/'.$item->customer_u_id) }}">{{$item->customer_name}}</a></td>
                                                                <td>{{$item->memo}}</td>
                                                                <td>{{$item->total_price}}</td>
                                                                @if($item->status == 'pending')
                                                                <td><a class="label label-warning btn-xs"> Pending</a></td>
                                                                @elseif($item->status == 'approved')
                                                                <td><a class="label label-success btn-xs"> Approved</a></td>
                                                                @else
                                                                <td><a class="label label-danger btn-xs"> Rejected</a></td>
                                                                @endif
                                                                <td><?php echo date("d M Y", strtotime($item->statement_date)); ?></td>
                                                                <td>
                                                                    <a href="{{ url('invoice/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-external-link"></i> Detail</a>

                                                                    <a href="{{ url('invoice/edit/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                                    @if($item->status == 'pending')
                                                                    <a href="{{ url('invoice/delete/'.$item->id) }}"  onclick="return confirm('Are you sure you want to delete this Invoice?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>



                                                                    @endif

                                                                </td>
                                                            </tr>

                                                            @endforeach

                                                            @endif


                                                        </tbody>
                                                        <tfoot>

                                                        </tfoot>
                                                    </table>

                                                </div></div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                <?php if (isset($activeTab) && $activeTab == 'settings') {
                                    ?>
                                    <div class="tab-pane active" id="settings">
                                    <?php } else { ?>
                                        <div class="tab-pane" id="settings">
                                        <?php } ?>
                                        <!-- <div class="tab-pane" id="settings"> -->
                                        <div class="box-body">

                                            <div class="table-responsive">
                                                <div class="col-md-12">

                                                    <table id="settings" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Customer Name</th>
                                                                <th>Sale Amount</th>
                                                                <th>Profit Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php
                                                            $total_sale_amount = 0;
                                                            $total_profit = 0;
                                                            ?>

                                                            @if(count($model) >0)
                                                            <?php $i = 1; ?>
                                                            @foreach($model as $item)
                                                            <?php
                                                            $f_sale = $item->sale_amount - $item->credit_memo_sale;
                                                            $f_profit = $item->profit_amount - $item->credit_memo_loss;
                                                            ?>
                                                            <tr>
                                                                <td>{{ $item->customer_name }}</td>
                                                                <td>{{ $f_sale }}</td>
                                                                <td>{{ $f_profit }}</td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                            $total_sale_amount = $total_sale_amount + $f_sale;
                                                            $total_profit = $total_profit + $f_profit;
                                                            ?>
                                                            @endforeach
                                                            <tr>
                                                                <th></th>
                                                                <th>Total Sale ${{ $total_sale_amount }}</th>
                                                                <th>Total Profit ${{ $total_profit }}</th>
                                                            </tr>
                                                            @endif


                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>

                                                </div></div>
                                        </div>

                                    </div>

                                    <!-- statement -->

                                    <style>
                                        .spacer {
                                            margin-top: 10px; /* define margin as you see fit */
                                        }
                                    </style>
                                    <!-- end -->
<?php if (isset($activeTab) && $activeTab == 'invoices') { ?>
                                        <div class="tab-pane active" id="invoices">

                                            <?php } else { ?>
                                            <div class="tab-pane" id="invoices">
<?php } ?>
                                            @include('front/common/common_invoice')
                                        </div>

                                            <?php if (isset($activeTab) && $activeTab == 'info') { ?>
                                            <div class="tab-pane active" id="info">
                                                <?php } else if (!isset($activeTab) || empty($activeTab)) { ?>
                                                <div class="tab-pane active" id="info">
                                                    <?php } else { ?>
                                                    <div class="tab-pane" id="info">
<?php } ?>
                                                    <div class="box">

                                                        <!-- /.box-header -->
                                                        <div class="box-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-2"><label>Customer Name: </label></div>
                                                                    <div class="col-md-2">{{$customer->name}}</div>
                                                                    <div class="col-md-2"><label>Username: </label></div>
                                                                    <div class="col-md-2">{{$customer->username}}</div>
                                                                    <div class="col-md-2"><label>Price Template: </label></div>
                                                                    <div class="col-md-2">{{$customer->name}}</div>
                                                                </div>
                                                            </div>
                                                            <div class="spacer"></div>     
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-2"><label>Phone: </label></div>
                                                                    <div class="col-md-2">{{$customer->phone}}</div>
                                                                    <div class="col-md-2"><label>Phone 2: </label></div>
                                                                    <div class="col-md-2">{{$customer->phone_2}}</div>
                                                                    <div class="col-md-2"><label>Email: </label></div>
                                                                    <div class="col-md-2">{{$customer->email}}</div>
                                                                </div>
                                                            </div>
                                                            <div class="spacer"></div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-2"><label>Email 2: </label></div>
                                                                    <div class="col-md-2">{{$customer->email_2}}</div>
                                                                    <div class="col-md-2"><label>Address 1: </label></div>
                                                                    <div class="col-md-2">{{$customer->address1}}</div>
                                                                    <div class="col-md-2"><label>Address 2: </label></div>
                                                                    <div class="col-md-2">{{$customer->address2}}</div>
                                                                </div>
                                                            </div>
                                                            <div class="spacer"></div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-2"><label>City: </label></div>
                                                                    <div class="col-md-2">{{$customer->city}}</div>
                                                                    <div class="col-md-2"><label>State: </label></div>
                                                                    <div class="col-md-2">{{$customer->state}}</div>
                                                                    <div class="col-md-2"><label>Company: </label></div>
                                                                    <div class="col-md-2">{{$customer->conpany}}</div>
                                                                </div>
                                                            </div>
                                                            <div class="spacer"></div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-2"><label>Zip Code: </label></div>
                                                                    <div class="col-md-2">{{$customer->zip_code}}</div>
                                                                    <div class="col-md-2"><label>Balance: </label></div>
                                                                    <div class="col-md-2">{{$customer->balance}}</div>
                                                                    <div class="col-md-2"><label>Created Date: </label></div>
                                                                    <div class="col-md-2">{{$customer->created_at}}</div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4">
                                                                            <label>Show Invoice Button</label>
                                                                            {!! Form::checkbox('gallery_invoice_btn',1,$customer->gallery_invoice_btn, array('id'=>'gallery_invoice_btn','class' => 'flat-red')) !!}
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <label for="exampleInputEmail1">Show Order Button</label>
                                                                            {!! Form::checkbox('gallery_order_btn',1,$customer->gallery_order_btn, array('id'=>'gallery_order_btn','class' => 'flat-red')) !!}
                                                                        </div>

                                                                        <div class="col-sm-4">
                                                                            <label for="exampleInputEmail1">Show Customer Menus</label>
                                                                            {!! Form::checkbox('client_menu',1,$customer->client_menu, array('id'=>'client_menu','class' => 'flat-red')) !!}
                                                                        </div>           

                                                                    </div>  
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4">
                                                                            <label for="exampleInputEmail1">Show Unit Price</label>
                                                                            {!! Form::checkbox('show_unit_price',1,$customer->show_unit_price, array('id'=>'show_unit_price','class' => 'flat-red')) !!}
                                                                        </div> 

                                                                        <div class="col-sm-4">
                                                                            <label for="exampleInputEmail1">Show Image</label>
                                                                            {!! Form::checkbox('show_image',1,$customer->show_image, array('id'=>'show_image','class' => 'flat-red')) !!}
                                                                        </div> 

                                                                        <div class="col-sm-4">
                                                                            <label for="exampleInputEmail1">Show Serial</label>
                                                                            {!! Form::checkbox('show_serial',1,$customer->show_serial, array('id'=>'show_serial','class' => 'flat-red')) !!}
                                                                        </div> 

                                                                    </div> 

                                                                    <div class="form-group">
                                                                        <div class="col-sm-4">
                                                                            <label for="exampleInputEmail1">Show Package</label>
                                                                            {!! Form::checkbox('show_package_id',1,$customer->show_package_id, array('id'=>'show_package_id','class' => 'flat-red')) !!}
                                                                        </div>
                                                                        <div class="col-sm-4" style="display: block;">
                                                                            <label for="exampleInputEmail1">Sync Option</label>
                                                                            {!! Form::checkbox('is_order_sync',1,$customer->is_order_sync, array('id'=>'is_order_sync','class' => 'flat-red')) !!}
                                                                        </div> 

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <a href="{{ url('payment/customer/create/'.$customer->id) }}" class="btn btn-primary">$$</i></a>
                                                                <a href="{{ url('customer/edit/'.$customer->u_id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                                <a href="{{ url('customer/delete/'.$customer->u_id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                                <div class="btn">
                                                                    {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer-view','files' => true, 'method' => 'post')) !!}
                                                                    <input type="hidden"  name="user_id" value="{{ $customer->user_id }}">
                                                                    <input type="submit" name="submit" class="btn btn-primary" value="Login">
                                                                    {!! Form::close() !!}
                                                                </div>

                                                            </div>
                                                            <!-- /.box-body -->
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- /.tab-pane -->
                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.nav-tabs-custom -->
                                    </div>

                                    </section>
                                    @include('front/common/dataTable_js')
                                    <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
                                    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js')}}"></script>	
                                    <script>

                                                                        //Flat red color scheme for iCheck
                                                                        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                                                                        checkboxClass: 'icheckbox_flat-purple',
                                                                                radioClass: 'iradio_flat-purple'
                                                                        });
                                    </script>
                                    <script>
                                        $(document).ready(function() {
                                            
                                        $("#filter_row").css("display", "block");
                                        $("#customer_dropdown").css("display", "none");
                                        $("#transection_type").css("display", "none");
                                        $("#type01").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        $("#deliver_option").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;

                                        var valnew = '{{ $activeTab }}';
                                        if (valnew == ''){
                                        valnew = 'info';
                                        }
                                        // console.log(valnew)
                                        var res = valnew.toLowerCase();
                                        $("#tab0").val(res);
                                        document.getElementById("gallery_invoice_btn").disabled = true;
                                        document.getElementById("gallery_order_btn").disabled = true;
                                        document.getElementById("client_menu").disabled = true;
                                        document.getElementById("show_unit_price").disabled = true;
                                        document.getElementById("show_image").disabled = true;
                                        document.getElementById("show_serial").disabled = true;
                                        document.getElementById("show_package_id").disabled = true;
                                        document.getElementById("is_order_sync").disabled = true;
                                        // var valnew = $(".nav-tabs").find(".active a").html();

                                        var selected = '<?php echo $selected_report_type; ?>'
                                                if (selected == 'delivered' && res != 'invoices'){
                                        document.getElementById('type011').selectedIndex = - 1;
                                        $("#type011").val("");
                                        }

                                        if (valnew == 'transactions') {
                                        $("#filter_row").css("display", "block");
                                        $("#customer_dropdown").css("display", "block");
                                        $("#transection_type").css("display", "block");
                                        $("#type01").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        $("#deliver_option").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if (valnew == 'invoices'){
                                        $("#filter_row").css("display", "block");
                                        $("#type01").css("display", "block");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        $("#deliver_option").css("display", "block");
                                        $("#transection_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if (valnew == 'payments'){
                                        $("#filter_row").css("display", "block");
                                        $("#type01").css("display", "block");
                                        $("#account_type").css("display", "block");
                                        $("#payment_source_type").css("display", "block");
                                        $("#deliver_option").css("display", "none");
                                        $("#transection_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if (valnew == 'info'){
                                        $("#transection_type").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = true;
                                        document.getElementById("mybutton").disabled = true;
                                        // $("#filter_row").css("display", "hide");
                                        }else if (valnew == 'statement'){
                                        $("#transection_type").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        // $("#filter_row").css("display", "hide");
                                        }else if (valnew == 'settings'){
                                        $("#transection_type").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        // $("#filter_row").css("display", "hide");
                                        } else{
                                        $("#filter_row").css("display", "block");
                                        $("#customer_dropdown").css("display", "none");
                                        $("#transection_type").css("display", "none");
                                        $("#type01").css("display", "block");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        $("#deliver_option").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        }
                                        
                                        $("ul.nav-tabs > li > a").click(function() {

                                        var selected1 = '<?php echo $selected_report_type; ?>'
                                                if (selected1 == 'delivered' && $(this).attr("href").replace("#", "") != 'invoices'){
                                        document.getElementById('type011').selectedIndex = - 1;
                                        }

                                        $("#tab0").val($(this).attr("href").replace("#", ""));
                                        if ($(this).attr("href").replace("#", "") == 'transactions'){
                                        $("#filter_row").css("display", "block");
                                        $("#customer_dropdown").css("display", "none");
                                        $("#transection_type").css("display", "block");
                                        $("#deliver_option").css("display", "none");
                                        $("#type01").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if ($(this).attr("href").replace("#", "") == 'invoices'){
                                        $("#filter_row").css("display", "block");
                                        $("#deliver_option").css("display", "block");
                                        $("#type01").css("display", "block");
                                        $("#transection_type").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if ($(this).attr("href").replace("#", "") == 'payments'){
                                        $("#filter_row").css("display", "block");
                                        $("#deliver_option").css("display", "none");
                                        $("#type01").css("display", "block");
                                        $("#transection_type").css("display", "none");
                                        $("#account_type").css("display", "block");
                                        $("#payment_source_type").css("display", "block");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if ($(this).attr("href").replace("#", "") == 'statement'){
                                        $("#filter_row").css("display", "block");
                                        $("#type01").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        $("#transection_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if ($(this).attr("href").replace("#", "") == 'settings'){
                                        $("#filter_row").css("display", "block");
                                        $("#type01").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        $("#transection_type").css("display", "none");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        } else if ($(this).attr("href").replace("#", "") == 'info'){
                                        $("#type01").css("display", "none");
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        // $("#filter_row").css("display", "none");
                                        document.getElementById("mybutton2").disabled = true;
                                        document.getElementById("mybutton").disabled = true;
                                        $("#transection_type").css("display", "none");
                                        } else{
                                        $("#account_type").css("display", "none");
                                        $("#payment_source_type").css("display", "none");
                                        $("#deliver_option").css("display", "none");
                                        $("#customer_dropdown").css("display", "none");
                                        $("#transection_type").css("display", "none");
                                        $("#type01").css("display", "block");
                                        $("#filter_row").css("display", "block");
                                        document.getElementById("mybutton2").disabled = false;
                                        document.getElementById("mybutton").disabled = false;
                                        }
                                        console.log(
                                                $(this).attr("href").replace("#", "")
                                                )
                                        });
                                        });
                                        $(function () {
                                        $('#vendors1').DataTable({
                                        "paging": true,
                                                "lengthChange": false,
                                                "searching": false,
                                                "ordering": false,
                                                "info": false,
                                                "pageLength": {{Config::get('params.default_list_length')}},
                                                "autoWidth": false,
                                                dom: 'Bfrtip',
                                                buttons: [
                                                {
                                                extend: 'excelHtml5',
                                                        text: 'Export To Excel',
                                                        title: 'Report',
                                                },
                                                {
                                                extend: 'pdfHtml5',
                                                        text: 'Export To PDF',
                                                        title: 'Report',
                                                }
                                                ]
                                        });
                                        });

                                        $(function () {
                                        $('#vendors2').DataTable({
                                        "paging": true,
                                                "lengthChange": false,
                                                "searching": false,
                                                "ordering": false,
                                                "info": false,
                                                "pageLength": {{Config::get('params.default_list_length')}},
                                                "autoWidth": false,
//                                                dom: 'Bfrtip',
                                                // buttons: [
                                                // {
                                                // extend: 'excelHtml5',
                                                //         text: 'Export To Excel',
                                                //         title: 'Report',
                                                // },
                                                // {
                                                // extend: 'pdfHtml5',
                                                //         text: 'Export To PDF',
                                                //         title: 'Report',
                                                // }
                                                // ]
                                        });
                                        });

                                        $(function () {
                                        $('#statement1').DataTable({
                                        "paging": true,
                                                "lengthChange": false,
                                                "searching": false,
                                                "ordering": false,
                                                "info": false,
                                                "pageLength": {{Config::get('params.default_list_length')}},
                                                "autoWidth": false,
                                                dom: 'Bfrtip',
                                                buttons: [
                                                {
                                                extend: 'excelHtml5',
                                                        text: 'Export To Excel',
                                                        title: 'Report',
                                                },
                                                {
                                                extend: 'pdfHtml5',
                                                        text: 'Export To PDF',
                                                        title: 'Report',
                                                }
                                                ]
                                        });
                                        });


                                    </script>

                                    @endsection