@extends('customer_template')

@section('content')
@if(1==2)
<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
@endif
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>
            {!! Form::open(array( 'class' => '','url' => 'customers/', 'method' => 'get')) !!}
            <div class="box-body">
                <div class="row">

                   
                    <div class="form-group col-sm-3">
                    <select class ='form-control' name="status" >
                    <option value="" >Select Status</option> 
                    <option value="1"  @if(!isset($_GET['status'])) selected @endif @if(isset($_GET['status']) && ($_GET['status']==1))  selected @endif>Active</option>
                    <option value="0" @if(isset($_GET['status']) && ($_GET['status']==0))  selected @endif>Inactive </option>
                 
                    </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <select class ='form-control' name="pricestatus" >
                            <option value="" @if(isset($pricestatus) && empty($pricestatus)) selected  @endif>Select price status</option> 
                            <option value="increase" <?php if(isset($pricestatus) && $pricestatus=="increase"){ ?> selected  <?php }?>>Increase</option>
                            <option value="decrease"<?php if(isset($pricestatus) && $pricestatus=="decrease"){ ?> selected  <?php }?>>Decrease </option>
                            </select>
                    </div> 

                    <div class="clearfix"></div>
                    <input type="hidden" class="form-control" name="page" id="page" value="1">
                    <div class="clearfix"></div>
                    <div class=" form-group col-sm-3">
                    <a href="{{ url('customers') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                    </div>
                    <div class=" form-group col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Search">
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </div>
</div>
<div class = "table table-responsive" >
<section id="form" class="mt30 mb30 col-sm-12 p0">
    <div class="row">

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        
         @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Customers Listing</h3>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="">
                <b style="
                color: green;
                ">Total Active: {{ $active  }}</b>   <b style="
                color: red;
                "> Total Inactive: {{ $inactive  }}</b>
                </span>
            
                <a href="{{ url('customer/create/') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create New Customer</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="vendors" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                           
                            <th>Customer Name</th>
                            <th>Username</th>
                            <th>Price Template</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>Balance</th>
                            <th>Created Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($model) >0)
                        @foreach($model as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->username}}</td>
                            <td><a  href="{{ url('item-price-template/'.$item->selling_price_template) }}">{{$item->template_name}}</a></td>
                            <td>{{$item->phone}}</td>
                             <td>{{$item->address1.' '.$item->address2.' '.$item->city.', '.$item->state.' '.$item->zip_code}}</td>
                            <td>{{$item->balance}}</td>
                            <td><?php echo date("d M Y", strtotime($item->created_at)); ?></td>
                            <td>
                                <a href="{{ url('payment/customer/create/'.$item->id) }}" class="btn btn-primary">$$</i></a>
                                <a href="{{ url('customer/edit/'.$item->u_id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <!--<a href="{{ url('customer/detail/'.$item->u_id) }}" class="btn btn-alert"><i class="fa fa-edit"></i></a>-->
                                <a href="{{ url('customer/delete/'.$item->u_id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <a href="{{ url('customer/detail/'.$item->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                <div class="btn">
                                {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer-view','files' => true, 'method' => 'post')) !!}
                                <input type="hidden"  name="user_id" value="{{ $item->user_id }}">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Login">
                                {!! Form::close() !!}
                                </div>

                            </td>
                        </tr>
                        @endforeach

                        @endif


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>			
</section>
 </div>
@include('front/common/dataTable_js')
<script>
   $(function () {
        $('#vendors').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "pageLength": {{Config::get('params.default_list_length')}},
            "autoWidth": false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export To Excel',
                title: 'Customers List',
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'Export To PDF',
                title: 'Customers List',
               exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
            }
        ]
        });
    });
</script>
@endsection



