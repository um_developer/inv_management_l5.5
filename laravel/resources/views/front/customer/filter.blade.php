        <div class="row" id="filter_row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        
                        <h3 class="box-title">Filter by Date</h3>
                        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                        </div>
                    </div>

                    {!! Form::open(array( 'class' => '','url' => 'customer/detail/'.Request::route('id'), 'method' => 'post')) !!}

                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-sm-4">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i></div>
                                        <?php if (isset($filter)) { ?>
                                        
                                    <input type="text" name ="date_range" value="{{ $date_range }}" class="form-control pull-right" id="reservation">
                                <?php }else{ ?>
                                    <input type="text" name ="date_range" value="{{ $date }}" class="form-control pull-right" id="reservation" required="required">
                                    <?php } ?>                           
                                    </div>
                            </div>
                            <div class="form-group col-sm-4" id="type01" style="display: none;">

                            <select id="type011" name="type" class="form-control">
                                <option value="" <?php echo ($selected_report_type == '') ? 'selected' : ''; ?>>Select Status</option>        
                                <option value="pending" <?php echo ($selected_report_type == 'pending') ? 'selected' : ''; ?>>Pending</option>
                                <option value="approved" <?php echo ($selected_report_type == 'approved') ? 'selected' : ''; ?>>Approved</option>
                                 <option value="processing" <?php echo ($selected_report_type == 'processing') ? 'selected' : ''; ?>>Processing</option>
                                <option id="deliver_option" style="display: none;" value="delivered" <?php echo ($selected_report_type == 'delivered') ? 'selected' : ''; ?>>Delivered</option>
                            </select>
                            </div>
                            <div class="form-group col-sm-4" id="transection_type" style="display: none;">
                                {!!  Form::select('report_type_id', $report_type, Request::input('report_type_id'), array('class' => 'form-control','id' => 'report_type_id' ))  !!}
                            </div>
                            <div class="form-group col-sm-4" id="payment_source_type" style="display: none;">
                                {!!  Form::select('payment_source_id', $payment_sources, Request::input('payment_source_id'), array('class' => 'form-control','id' => 'payment_source_id' ))  !!}
                            </div>
                            <div class="form-group col-sm-4" id="account_type" style="display: none;">
                                {!!  Form::select('account_id', $accounts, Request::input('account_id'), array('class' => 'form-control','id' => 'account_id' ))  !!}
                            </div>
                            <link rel="stylesheet" href="{{asset('adminlte/plugins/daterangepicker/daterangepicker-bs3.css')}}">
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                            <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>

                            <script>
  $(function () {
    $('#reservation').daterangepicker({format: 'MM/DD/YYYY'});
  });
                            </script>

<input type="hidden" name="tab" id="tab0">
                            <div class="clearfix"></div>
                            <input type="hidden" class="form-control" name="filter" id="filter" value="1">
                            <div class="clearfix"></div>
                            <div class=" form-group col-sm-3">

                                <a href="{{ URL::to('customer/detail/'.Request::route('id')) }}" id="mybutton2"  class="btn btn-danger btn-block btn-flat">Clear Search</a>

                            </div>
                            <div class=" form-group col-sm-3">
                                <input type="submit" class="btn btn-primary btn-block btn-flat" id="mybutton" name="submit" value="Search">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
