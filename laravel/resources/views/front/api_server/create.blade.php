@extends('customer_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Create API Server</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'api-server/create', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                   <div class="form-group">
                <div class="col-sm-12">
                       <label for="exampleInputEmail1">Title</label>
                     {!! Form::text('title', Request::input('title') , array('placeholder'=>"Server Title *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>

                  <div class="form-group">
                <div class="col-sm-12">
                       <label for="exampleInputEmail1">User Name</label>
                     {!! Form::text('username', Request::input('username') , array('placeholder'=>"User Name",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                <div class="col-sm-12">
                       <label for="exampleInputEmail1">Password</label>
                       {!! Form::input('password', 'password', Request::input('password'),array('placeholder'=>"Password",'maxlength'  => 100,'autocomplete'=>"new-password",'class' => 'form-control',$required)) !!}
                     
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">URL</label>
                     {!! Form::URL('url', Request::input('url') , array('placeholder'=>"Server URL *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>
                  
                   
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Create">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
        			
</section>

@endsection