<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>&nbsp</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link href="{{ asset('adminlte/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('front/data_table/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/data_table/ionicons.min.css')}}">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        
        <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/skin-purple.min.css') }}">
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/select2.css') }}">
       
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/treegrid/treegrid.css')}}">
        <script src="{{ asset('adminlte/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
        <script src="{{ asset('adminlte/bootstrap/js/bootstrap.min.js')}}"></script>
         <link href="{{ asset('front/css/new_style.css')}}" rel="stylesheet" type="text/css" />
        
 <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
 
  <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
  
   <link rel="stylesheet" href="{{asset('front/css/jquery-ui.css')}}">
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  
<!--          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        -->
<script src="{{asset('front/js/moment.min.js')}}"></script>
    <script src="{{asset('front/data_table/bootstrap-datetimepicker.min.js')}}"></script>
        
    <link rel="stylesheet" href="{{asset('front/data_table/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/data_table/bootstrap-datetimepicker.min.css')}}">
    <script src="{{asset('front/data_table/bootstrap.min.js')}}"></script>

        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script> -->
<!--         <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css"> -->
        
        <link href="{{asset('front/css/print.css')}}" rel="stylesheet" media="print" type="text/css" />

        

    </head>
    <body class="hold-transition skin-purple sidebar-mini">
        <div class="wrapper">

            <!-- Header -->
            @include('front/admin/header')

            <!-- Sidebar -->
            @include('front/admin/sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">

                    <!-- Your Page Content Here -->
                    @yield('content')

                </section>
                <!-- /.content -->
            </div>


            <div class="control-sidebar-bg"></div>
        </div>

        <script src="{{ asset('adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
        <script src="{{ asset('adminlte/dist/js/app.min.js')}}"></script>
              <script src="{{ asset('adminlte/plugins/select2/select2.js')}}"></script>
        <script>
            $(document).ready(function () {
                var url = window.location;
                $('.sidebar-menu a[href="' + url + '"]').parent().addClass('active');
                $('.sidebar-menu a[href="' + url + '"]').closest('.treeview').addClass('active');
                $('.sidebar-menu a[href="' + url + '"]').closest('.treeview-menu').css("display", "block");
                $('.sidebar-menu a').filter(function () {
                    return this.href === url;
                }).parent().addClass('active');
            });
        </script>
    </body>
</html>
