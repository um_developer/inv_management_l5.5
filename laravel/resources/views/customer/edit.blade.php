@extends('customer_c_template_2')

@section('content')
<?php
$required = 'required';
?>
<section id="form">
    
              <div class="box box-primary">
            <div class="box-header with-border text-center">
              <h3 class="box-title">Update Profile</h3>
            </div>
            <!-- /.box-header -->
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif

                     @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                   @if (count($errors->form) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->form->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
                 {!! Form::open(array( 'class' => 'form-horizontal','url' => 'customer/profile/update', 'method' => 'post')) !!}
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
               <div class="form-group">
                  <div class="col-sm-12">
                      {!! Form::hidden('u_id', $model->u_id , array('maxlength' => 20,$required) ) !!}
                       {!! Form::hidden('id', $model->id , array('maxlength' => 20,$required) ) !!}
                       <label for="exampleInputEmail1">Customer Name</label>
                     {!! Form::text('name', $model->name , array('placeholder'=>"Customer Name *",'maxlength' => 100,'class' => 'form-control',$required) ) !!}
                  </div>
                   </div>
                  @if(1==2)
                     <div class="form-group">
                <div class="col-sm-12">
                       <label for="exampleInputEmail1">User Name</label>
                     {!! Form::text('username', $model->username , array('placeholder'=>"User Name",'maxlength' => 100,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                <div class="col-sm-12">
                       <label for="exampleInputEmail1">Password</label>
                     {!! Form::text('password', $model->password , array('placeholder'=>"Password",'maxlength' => 100,'class' => 'form-control') ) !!}
                  </div>
                </div>
                @endif
 <div class="form-group">

                </div>
                  
                     <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Phone</label>
                     {!! Form::text('phone', $model->phone , array('placeholder'=>"Phone",'maxlength' => 20,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Email</label>
                     {!! Form::text('email', $model->email , array('placeholder'=>"Email *",'maxlength' => 30,'class' => 'form-control',$required) ) !!}
                  </div>
                </div>
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Address Line 1</label>
                     {!! Form::text('address1', $model->address1 , array('placeholder'=>"Address Line 1",'maxlength' => 400,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                   <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Address Line 2</label>
                     {!! Form::text('address2', $model->address2 , array('placeholder'=>"Address Line 2",'maxlength' => 400,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-6">
                       <label for="exampleInputEmail1">City</label>
                     {!! Form::text('city', $model->city , array('placeholder'=>"City",'maxlength' => 30,'class' => 'form-control') ) !!}
                  </div>
                      
                       <div class="col-sm-6">
                       <label for="exampleInputEmail1">State</label>
                  
                     {!!   Form::select('state', $states, $model->state, array('class' => 'form-control' ))  !!}
                  </div>
                </div>
               <div class="form-group">
                  <div class="col-sm-6">
                       <label for="exampleInputEmail1">Company</label>
                     {!! Form::text('company', $model->company , array('placeholder'=>"Company",'maxlength' => 250,'class' => 'form-control') ) !!}
                  </div>
                    <div class="col-sm-6">
                       <label for="exampleInputEmail1">Zip Code</label>
                     {!! Form::text('zip_code', $model->zip_code , array('placeholder'=>"Zip Code",'maxlength' => 20,'class' => 'form-control') ) !!}
                  </div>
                </div>
                  
                  <div class="form-group">
                  <div class="col-sm-12">
                       <label for="exampleInputEmail1">Note</label>
                     {!! Form::textarea('note', $model->note , ['class'=>'form-control', 'rows' => 5, 'cols' => 10] ) !!}
                    
                  </div>
                </div>
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Update">
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!} 
          </div>
    
        			
</section>

@endsection