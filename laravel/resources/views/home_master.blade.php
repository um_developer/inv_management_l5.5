<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{Config::get('params.site_name')}}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link href="{{ asset('adminlte/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('front/data_table/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('front/data_table/ionicons.min.css')}}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        
        <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/skin-purple.min.css') }}">
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/select2.css') }}">
       
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/treegrid/treegrid.css')}}">
        <script src="{{ asset('adminlte/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
        <script src="{{ asset('adminlte/bootstrap/js/bootstrap.min.js')}}"></script>
         <link href="{{ asset('front/css/new_style.css')}}" rel="stylesheet" type="text/css" />
        
 <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
 
  <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
  
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
   <link rel="stylesheet" href="{{asset('front/css/jquery-ui.css')}}">
<!--          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css"> -->
    <link href="{{asset('front/js/moment.min.js')}}">
    <link href="{{asset('front/data_table/bootstrap-datetimepicker.min.js')}}">
        
    <link rel="stylesheet" href="{{asset('front/data_table/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/data_table/bootstrap-datetimepicker.min.css')}}">



    <script src="{{asset('front/vendor/swiper/swiper.jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('front/vendor/swiper/swiper.min.css')}}">
        
    </head>
    <body class="hold-transition skin-purple sidebar-mini home">
        <div class="wrapper">

            <!-- Header -->
            <header class="main-header">

  

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      
      <a href="{{ url('/dashboard') }}" class="logo">
        <i class="fa fa-home"></i>
        HOME
      </a>

      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
  
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            @if(isset(Auth::user()->id))
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="{{ asset('front/images/logo.png')}}" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->firstName }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-footer">
                <div class="pull-right">
                    <a href="{{ url('changepassword') }}" class="btn btn-primary btn-flat">Change Password</a>
                  <a href="{{ url('auth/logout') }}" class="btn btn-danger btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
            @else
             <a href="{{ url('/login') }}" class="dropdown-toggle">Sign In</a>
            @endif
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>



            <!-- Content Wrapper. Contains page content -->
            <div class="login-page1">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">

                    <!-- Your Page Content Here -->
                    @yield('content')
<footer class="main-footer">
    <!-- To the right -->
    <div class="d">
      {{ Config('params.site_name') }}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y');?> {{ Config('params.site_name') }}.</strong> All rights reserved. 
  </footer>
                </section>
                <!-- /.content -->
            </div>
            
            <!-- /.content-wrapper -->

            <!-- Footer -->
           

            <div class="control-sidebar-bg"></div>
        </div>

        <script src="{{ asset('adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
        <script src="{{ asset('adminlte/dist/js/app.min.js')}}"></script>
        <script type="module" src="{{ asset('front/js/main.js')}}"></script>


        <script type="module" src="{{ asset('front/js/main.js')}}"></script>

      <script src="{{ asset('adminlte/plugins/select2/select2.js')}}"></script>
      <script type="module" src="{{ asset('front/js/main.js')}}"></script>
        <script>
            $(document).ready(function () {
                var url = window.location;
                $('.sidebar-menu a[href="' + url + '"]').parent().addClass('active');
                $('.sidebar-menu a[href="' + url + '"]').closest('.treeview').addClass('active');
                $('.sidebar-menu a[href="' + url + '"]').closest('.treeview-menu').css("display", "block");
                $('.sidebar-menu a').filter(function () {
                    return this.href === url;
                }).parent().addClass('active');
            });
        </script>
    </body>
</html>
