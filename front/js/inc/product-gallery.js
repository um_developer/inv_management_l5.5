export function productModalGallery() {
 
  
    $(window).on('shown.bs.modal', function() { 
       // $('#imgModal').modal('show');
        setTimeout(function () { 

            var galleryTop = new Swiper('.Gallery', {
                direction: 'vertical',
                spaceBetween: 10
            });
    
            var galleryThumbs = new Swiper('.Thumbs', {
                direction: 'vertical',
                spaceBetween: 10,
                centeredSlides: true,
                slidesPerView: 9,
                touchRatio: 1,
                slideToClickedSlide: true,
                navigation: {
                    nextEl: '.tswiper-button-next',
                    prevEl: '.tswiper-button-prev',
                },
            });
    
            galleryTop.params.control = galleryThumbs;
            galleryThumbs.params.control = galleryTop;
    
    
        }, 500);
       // alert('shown');
    });
}   